var homeURL = $("#homeURL").val();
function pad(numb) {
    return (numb < 10 ? '0' : '') + numb;
}
function renderList(n) {
    var t = $("#fromDate").datepicker("getDate"),
        i = $("#toDate").datepicker("getDate");
    console.log(t);
    var date     = new Date(t);
    var year  = pad(date.getFullYear());
    var month = pad(date.getMonth() + 1);
    var day   = pad(date.getDate());
    var t = year +'-'+ month +'-'+ day;

    var date     = new Date(i);
    var year  = pad(date.getFullYear());
    var month = pad(date.getMonth() + 1);
    var day   = pad(date.getDate());

    var i = year +'-'+ month +'-'+ day;

    $.ajax({
        type: "GET",
        url: homeURL+'/mobile/checkreportdata/getReportData',
        contentType: "application/json; charset=utf-8",
        data: {
            userId: n,
            fromDate: t,
            toDate: i
        },
        dataType: "json",
        success: function(n) {

            if(n.status == 'success'){
                if(n.main == 'yes'){
                    $(".mainData").show();
                    $(".dateDisplay").html(t);
                }else{
                    $(".mainData").hide();
                }
                
                $("#dataReport").html(n.html);
            }else{
                $("#dataReport").html(n.msg);
            }
            
        },
        error: function() {
            alert("Error")
        }
    });
}

$(document).ready(function() {
    var t = new Date,
        n;
    t.setDate(t.getDate() - 1);
    
    n = new Date;
    n.setDate(n.getDate());
    $("#fromDate").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", t);

    $("#toDate").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", n);

    $("#clientId").on("change", function() {
        renderList($("#clientId").val(), !1);
        $("#isPayment").prop("checked", !1)
    });
    $("#toDate").on("change", function() {
        renderList($("#clientId").val(), !1);
    });
    $("#fromDate").on("change", function() {
        renderList($("#clientId").val(), !1);
    });
    $("#isPayment").on("change", function() {
        $("tr.no-transac").toggle()
    });
    $("#clientId").change()
});

