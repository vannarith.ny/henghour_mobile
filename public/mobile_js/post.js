var homeURL = $("#homeURL").val();

function check_all() {
	var n = $("input:checkbox");
	$.each(n, function (n, t) {
		t.checked || ($("#" + t.id).prop("checked", !0), $("#" + t.id).change())
	})
}

function uncheck_all() {
	var n = $("input:checkbox");
	$.each(n, function (n, t) {
		t.checked && ($("#" + t.id).prop("checked", !1), $("#" + t.id).change())
	})
}

function check_all_defult() {
	var n = $("#loginUserId").val();
	copyFromOtherUser(n)
}

function sameAs() {
	var n = $("#base_user_id").val();
	copyFromOtherUser(n)
}



function getPostVote() {
	var clientId = $("#clientId").val();
	var timeId = $("#timeId").val();
	var type_lottery = $("#type_lottery").val();
	$.ajax({
		type: "Get",
		url: homeURL+'/mobile/getpostbyuser',
		contentType: "application/json; charset=utf-8",
		data: {
			clientId: clientId,
			timeId: timeId,
			type_lottery: type_lottery
		},
		dataType: "json",
		success: function (data) {
			if(data.status == 'success'){
				$("#td_users tbody.stylecontent").html(data.data);
			}
		},
		error: function () {
			alert("Error")
		}
	})
}


function copyFromOtherUser(n) {
	uncheck_all();
	var clientId = $("#clientId").val();
	$.ajax({
		type: "Get",
		url: homeURL+"/mobile/GetPostVoteIdsClient",
		contentType: "application/json; charset=utf-8",
		data: {
			clientId: clientId,
			clientIdFollow : n
		},
		dataType: "json",
		success: function (n) {
			if(n.status == 'success'){
				getPostVote();
			}else{
				alert(n.msg);
			}
		},
		error: function () {}
	})
}

function saveChange(n,time,thisID) {
	if($(thisID).is(":checked")){
		var checkValue = true; 
	}else{
		var checkValue = false; 
	}
	var t = {
		clientId: $("#clientId").val(),
		postVoteId: n,
		checkValue: checkValue,
		time_id: time
	};
	$.ajax({
		type: "GET",
		url: homeURL+"/mobile/UpdateClientPostVote",
		contentType: "application/json; charset=utf-8",
		data: t,
		dataType: "json",
		success: function (n) {
			console.log(n);
			if(n.status == 'success'){
				getPostVote();
			}else{
				alert(n.msg);
			}
		},
		error: function () {}
	})
}

function fetchPostVoteByTime(n) {
	var t = $("#isAdmin").val();
	$.ajax({
		type: "Get",
		url: "/PostVote/PostVoteByLotteryTimeId",
		contentType: "application/json; charset=utf-8",
		data: {
			lotteryTimeId: n
		},
		dataType: "json",
		success: function (n) {
			var i, t, r;
			AjaxError(n) == !1 && (i = '<table cellpadding="2" cellspacing="0"><tbody><tr><th class="ctd" width="60">បង្ហាញ<\/th><th class="classtd1">ឈ្មោះប៉ុស្ត៏បង្ហាញ<\/th><th class="classtd1" colspan="2">ឈ្មោះប៉ុស្ត៏ដើម<\/th> <th class="classtd1">2លេខ<\/th><th class="classtd1">3លេខ<\/th><th class="classtd1">4លេខ<\/th>', i += "<\/tr>", t = "", $.each(n.PostVotes, function (n, i) {
				t += '<td class="classtd2" align="center"><input type="checkbox" id="' + i.Id + '"  onchange="saveChange(' + i.Id + ');"><\/td><td class="classtd2"><input size="10" class="input" value="' + i.Name + '"/><\/td><td class="classtd">' + i.DisplayName + '<\/td><td class="classtd">' + i.DisplayName + '<\/td><td class="classtd">x' + i.TwoDigit + '<\/td><td class="classtd">x' + i.ThreeDigit + '<\/td><td class="classtd">x' + i.FourDigit + "<\/td>";
				t += "<\/tr>"
			}), r = i + t + "<\/tbody><\/table>", $("#PostVote").html(r), $("#clientId").change())
		},
		error: function () {
			alert("Error")
		}
	})
}

function UpdateVoteName(checkValue,postVoteId,clientId,namePost){
	var t = {
		clientId: clientId,
		postVoteId: postVoteId,
		namePost: namePost,
		checkValue: checkValue
	};
	$.ajax({
		type: "GET",
		url: homeURL+"/mobile/UpdateNameVoteVote",
		contentType: "application/json; charset=utf-8",
		data: t,
		dataType: "json",
		success: function (n) {
			if(n.status == 'success'){
				getPostVote();
			}else{
				alert(n.msg);
			}
		},
		error: function () {}
	})
}

function LockPostVote(n) {
	$.ajax({
		type: "POST",
		url: "/PostVote/LockPostVote",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			postVoteId: n
		}),
		dataType: "json",
		success: function (n) {
			AjaxError(n) == !1 && $("#lotteryTime").change()
		},
		error: function () {}
	})
}

function UnlockPostVote(n) {
	$.ajax({
		type: "POST",
		url: "/PostVote/UnlockPostVote",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			postVoteId: n
		}),
		dataType: "json",
		success: function (n) {
			AjaxError(n) == !1 && $("#lotteryTime").change()
		},
		error: function () {}
	})
}

function AjaxError(n) {
	let t = n.IsError;
	if (t == !0) switch (n.ErrorCode) {
	case 100:
		window.location = "/Account/Logout/";
		break;
	case 101:
		alert("មានគេចូលគណនីរបស់អ្នក។ ចូលប្រើម្តងទៀត");
		window.location = "/Account/Logout/";
		break;
	default:
		alert(n.ErrorMessage);
		return
	}
	return t
}
$(document).ready(function () {
	getPostVote();

	$("#base_user_id").on("change", function () {
		var n = $(this).val();
		if(n != ''){
			copyFromOtherUser(n);	
		}
		
	});

	$('.stylecontent').on('change', '.checkUpdateName' , function() {
		// alert("ok");
		var namePost = $(this).val();
		var n = $(this).attr('idPost');
		var clientId = $('#clientId').val();
		var IDData = $(".classtd"+n).find('#'+n)
		if($(IDData).is(":checked")){
			var checkValue = true; 
		}else{
			var checkValue = false; 
		}
		console.log(checkValue);
		if(n != '' && clientId!= '' && namePost != ''){
			UpdateVoteName(checkValue,n,clientId,namePost);	
		}
		
	});
	// $("#lotteryTime").change();
	// $("#clientId").on("change", function () {
	// 	$("input:checkbox").prop("checked", !1);
	// 	$.ajax({
	// 		type: "Get",
	// 		url: "/PostVote/GetPostVoteIdsClient",
	// 		contentType: "application/json; charset=utf-8",
	// 		data: {
	// 			clientId: parseInt($("#clientId").val())
	// 		},
	// 		dataType: "json",
	// 		success: function (n) {
	// 			$.each(n, function (n, t) {
	// 				$("#" + t).prop("checked", !0)
	// 			})
	// 		},
	// 		error: function () {}
	// 	})
	// })
});