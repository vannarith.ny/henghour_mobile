var homeURL = $("#homeURL").val();
function elarger(n) {
	var t = $(n).val();
	$.ajax({
		type: "Get",
		url: "/Sary/GetElarger",
		contentType: "application/json; charset=utf-8",
		data: {
			value: t
		},
		success: function (n) {
			AjaxError(n) == !1 && (document.getElementById("number2").value = n.Digit2, document.getElementById("number3").value = n.Digit3, document.getElementById("number4").value = n.Digit4)
		},
		error: function () {
			alert("Error")
		}
	})
}

function DisplaySary() {
	DisplayEveningResult(2);
}
function DisplayEveningResult(n) {
	var t = GetResultDate(n);
	console.log(t);
	$.ajax({
		type: "Get",
		url: homeURL+"/mobile/GetResultSary",
		contentType: "application/json; charset=utf-8",
		data: {
			date: t.toISOString()
		},
		success: function (data) {
			
			$("#displaySaryKH1").html(data.htmlKH1);
			$("#displaySaryKH2").html(data.htmlKH2);
			$("#displaySaryKH3").html(data.htmlKH3);
			$("#displaySaryKH4").html(data.htmlKH4);
			$("#displaySaryKH5").html(data.htmlKH5);
			$("#displaySaryKH6").html(data.htmlKH6);

			$("#displaySaryAfternoom").html(data.htmlAfternoon);
			$("#displaySaryEvening").html(data.htmlEvening);
		},
		error: function () {},
		complete: function () {}
	})
}

// function DisplayNightResultResult(n) {
// 	var t = GetResultDate(n);
// 	$.ajax({
// 		type: "Get",
// 		url: "/Sary/GetNightSary/",
// 		contentType: "application/json; charset=utf-8",
// 		data: {
// 			date: t.toISOString()
// 		},
// 		success: function (n) {
// 			$("#NightSary").html(n)
// 		},
// 		error: function () {}
// 	})
// }

// function DisplayKhmerSary1045(n) {
// 	var t = GetResultDate(n);
// 	$.ajax({
// 		type: "Get",
// 		url: "/Sary/GetKhmerSary1045/",
// 		contentType: "application/json; charset=utf-8",
// 		data: {
// 			date: t.toISOString()
// 		},
// 		success: function (n) {
// 			$("#khmer1045").html(n)
// 		},
// 		error: function () {}
// 	})
// }

// function DisplayKhmerSary1300(n) {
// 	var t = GetResultDate(n);
// 	$.ajax({
// 		type: "Get",
// 		url: "/Sary/GetKhmerSary1300",
// 		contentType: "application/json; charset=utf-8",
// 		data: {
// 			date: t.toISOString()
// 		},
// 		success: function (n) {
// 			$("#khmer1300").html(n)
// 		},
// 		error: function () {}
// 	})
// }

// function DisplayKhmerSary1545(n) {
// 	var t = GetResultDate(n);
// 	$.ajax({
// 		type: "Get",
// 		url: "/Sary/GetKhmerSary1545/",
// 		contentType: "application/json; charset=utf-8",
// 		data: {
// 			date: t.toISOString()
// 		},
// 		success: function (n) {
// 			$("#khmer1545").html(n)
// 		},
// 		error: function () {}
// 	})
// }

// function DisplayKhmerSary1800(n) {
// 	var t = GetResultDate(n);
// 	$.ajax({
// 		type: "Get",
// 		url: "/Sary/GetKhmerSary1800/",
// 		contentType: "application/json; charset=utf-8",
// 		data: {
// 			date: t.toISOString()
// 		},
// 		success: function (n) {
// 			$(document).find("#khmer1800").html(n)
// 		},
// 		error: function () {}
// 	})
// }

// function DisplayKhmerSary1945(n) {
// 	var t = GetResultDate(n);
// 	$.ajax({
// 		type: "Get",
// 		url: "/Sary/GetKhmerSary1945",
// 		contentType: "application/json; charset=utf-8",
// 		data: {
// 			date: t.toISOString()
// 		},
// 		success: function (n) {
// 			$("#khmer1945").html(n)
// 		},
// 		error: function () {}
// 	})
// }

function PrintSary(n, t, i) {
	i || (i = "Normal");
	$.ajax({
		type: "POST",
		url: "/Print/PrintSary",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify({
			resultDate: t,
			lotteryTimeId: n,
			printType: i
		}),
		success: function (n) {
			if (AjaxError(n) == !1) {
				var t = n.Html.replace(/\n|\r/g, "");
				console.log(t);
				window.location = t
			}
		},
		error: function () {}
	})
}

function GetResultDate() {
	return $("#resultDate").datepicker("getDate")
}

function ProcessWinCount() {
	$.ajax({
		type: "POST",
		url: "/List/ProcessWinCount",
		success: function (n) {
			n.IsError == !1 ? swal("ស្វែងរកលេខត្រូវ!", "លទ្ធផលត្រូវបានគណនា", "success") : swal("ស្វែងរកលេខត្រុវ!", n.ErrorMessage, "error")
		},
		error: function () {
			swal("ស្វែងរកលេខត្រុវ!", "Error", "error")
		}
	})
}

function ProcessEOD() {
	$.ajax({
		type: "POST",
		url: "/List/ProcessEOD/",
		success: function (n) {
			n.IsError == !1 ? swal("បិទបញ្ចី!", "លទ្ធផលត្រូវបានគណនា", "success") : swal("បិទបញ្ចី!", n.ErrorMessage, "error")
		},
		error: function () {
			swal("បិទបញ្ចី!", "Error", "error")
		}
	})
}

function AjaxError(n) {
	let t = n.IsError;
	if (t == !0) switch (n.ErrorCode) {
	case 100:
		window.location = "/Account/Logout/";
		break;
	case 101:
		alert("មានគេចូលគណនីរបស់អ្នក។ ចូលប្រើម្តងទៀត");
		window.location = "/Account/Logout/";
		break;
	default:
		alert(n.ErrorMessage);
		return
	}
	return t
}
var objDate = [];
objDate.push({
	Day: 0,
	DateName: "អាទិត្យ",
	Title1: "T.GIANG",
	Title2: "Kiên Giang",
	Title3: "Kiên Giang"
});
objDate.push({
	Day: 1,
	DateName: "ច័ន្ទ",
	Title1: "TP HCM",
	Title2: "Đồng Tháp",
	Title3: "Đồng Tháp"
});
objDate.push({
	Day: 2,
	DateName: "អង្គារ",
	Title1: "B-TRE",
	Title2: "Vung Tàu",
	Title3: "Bạc Liêu"
});
objDate.push({
	Day: 3,
	DateName: "ពុធ",
	Title1: "Ð.NAI",
	Title2: "Sóc Trang",
	Title3: "Cần Thơ"
});
objDate.push({
	Day: 4,
	DateName: "ព្រហស្បតិ៍",
	Title1: "T.NINH",
	Title2: "Bình Thuận",
	Title3: "An Giang"
});
objDate.push({
	Day: 5,
	DateName: "សុក្រ",
	Title1: "V LONG",
	Title2: "V LONG",
	Title3: "V LONG"
});
objDate.push({
	Day: 6,
	DateName: "សៅរ៍",
	Title1: "TP.HCM",
	Title2: "Long An",
	Title3: "Long An"
});
$(document).ready(function () {
	$("#resultDate").datepicker({
		setDate: new Date,
		changeMonth: !1,
		changeYear: !1,
		showButtonPanel: !1,
		dateFormat: "yy-mm-dd"
	}).datepicker("setDate", new Date);
	$("#prev").click(function () {
		var n = $("#resultDate").datepicker("getDate");
		n.setDate(n.getDate() - 1);
		$("#resultDate").datepicker("setDate", n);
		$("#resultDate").change()
	});
	$("#next").click(function () {
		var t = new Date,
			n = $("#resultDate").datepicker("getDate");
		n.setDate(n.getDate() + 1);
		$("#resultDate").datepicker("setDate", n);
		$("#resultDate").change()
	});
	$("#resultDate").change(function () {
		DisplaySary()
	});
	DisplaySary()
});