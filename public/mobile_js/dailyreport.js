var homeURL = $("#homeURL").val();
function ReEodOverTime() {
	var n = $("#mainListDate").val(),
		t = [];
	$("#clientId option").each(function () {
		t.push(this.value)
	});
	$.ajax({
		type: "POST",
		url: "/Result/ReEdoResult",
		data: {
			date: n,
			userIds: t
		},
		success: function (t) {
			AjaxError(t) ? alert(t.ErrorMessage) : (GetAmountList(), alert("ក្បាលបញ្ចីត្រូវបានគណនាចាប់ពីថ្ងៃ : " + n))
		},
		error: function (n) {
			alert(n.ErrorMessage)
		}
	})
}

function GetAmountList() {
	// return false;
	var dateData = $("#mainListDate").val();
    var userID = $("#clientId").val();
    var sheetID = $("#lotteryTimeId").val();
    var stcType = $("#stc_type").val();
        
	$.ajax({
		type: "GET",
		url: homeURL+'/mobile/dailyReportGet',
		data: {
			userID: userID,
			dateData: dateData,
			sheetID: sheetID,
			stcType: stcType
		},
		success: function (n) {
            console.log(n);
			$("#main_table").html(n);
		},
		error: function () {}
	})
}

function clear_all_checked() {
	for (i = 6; i < document.frm.elements.length - 2; i++) document.frm.elements[i].checked = !1
}

function preview_only() {
	if (document.frm.client.value != 0) window.open("antent/prints/result3.php?client[]=" + document.frm.client.value + "&date=" + document.frm.date.value + "&sd=" + document.frm.sd.value + "&groupe=" + document.frm.groupe.value, "mywindow");
	else {
		var n = "",
			t = "";
		for (i = 6; i < document.frm.elements.length - 2; i++) document.frm.elements[i].checked && (n += t + document.frm.elements[i].value, t = "&client[]=");
		n != "" && window.open("antent/prints/result_prev.php?client[]=" + n + "&date=" + document.frm.date.value + "&sd=" + document.frm.sd.value + "&groupe=" + document.frm.groupe.value, "mywindow")
	}
}

function changeCalendar() {}

function print_bt_click() {
	if (document.frm.client.value != 0) window.open("antent/prints/result3.php?client[]=" + document.frm.client.value + "&date=" + document.frm.date.value + "&digit=" + document.frm.digit.value + "&sd=" + document.frm.sd.value + "&groupe=" + document.frm.groupe.value, "mywindow");
	else {
		var n = "",
			t = "";
		for (i = 6; i < document.frm.elements.length - 2; i++) document.frm.elements[i].checked && (n += t + document.frm.elements[i].value, t = "&client[]=");
		n != "" && window.open("antent/prints/result3.php?client[]=" + n + "&date=" + document.frm.date.value + "&digit=" + document.frm.digit.value + "&sd=" + document.frm.sd.value + "&groupe=" + document.frm.groupe.value, "mywindow")
	}
}

function AjaxError(n) {
	let t = n.IsError;
	if (t == !0) switch (n.ErrorCode) {
	case 100:
		window.location = "/Account/Logout/";
		break;
	case 101:
		alert("មានគេចូលគណនីរបស់អ្នក។ ចូលប្រើម្តងទៀត");
		window.location = "/Account/Logout/";
		break;
	default:
		alert(n.ErrorMessage);
		return
	}
	return t
}

function fillthescreen() {
	winH = windowHeight();
	heightNeeded = winH - 155;
	document.getElementById("div_result") && (document.getElementById("div_result").style.height = heightNeeded + "px")
}

function windowWidth() {
	var n = 0;
	return typeof innerWidth == "number" ? n = window.innerWidth : document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight) ? n = document.documentElement.clientWidth : document.body && (document.body.clientWidth || document.body.clientWidht) && (n = document.body.clientWidth), n
}

function windowHeight() {
	var n = 0;
	return typeof innerWidth == "number" ? n = window.innerHeight : document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight) ? n = document.documentElement.clientHeight : document.body && (document.body.clientWidth || document.body.clientHeight) && (n = document.body.clientHeight), n
}
$(document).ready(function () {
	$("#listDate").datepicker({
		setDate: new Date,
		changeMonth: !1,
		changeYear: !1,
		showButtonPanel: !1,
		dateFormat: "yy-mm-dd"
	}).datepicker("setDate", new Date);
	$("#mainListDate").datepicker({
		setDate: new Date,
		changeMonth: !1,
		changeYear: !1,
		showButtonPanel: !1,
		dateFormat: "yy-mm-dd"
	}).datepicker("setDate", new Date);
	$("#prev").click(function () {
		var n = $("#mainListDate").datepicker("getDate");
		n.setDate(n.getDate() - 1);
		$("#mainListDate").datepicker("setDate", n);
		$("#mainListDate").change()
	});
	$("#next").click(function () {
		var t = new Date,
			n = $("#mainListDate").datepicker("getDate");
		n.setDate(n.getDate() + 1);
		$("#mainListDate").datepicker("setDate", n);
		$("#mainListDate").change()
	});
	$("#mainListDate").change(function () {
		GetAmountList()
	});
	$("#mainListDate").change();
	$("#clientId").change(function () {
		GetAmountList()
	});
	$("#lotteryTimeId,#show_type").change(function () {
		GetAmountList()
	})
});
window.onresize = function () {
	fillthescreen()
};
window.onload = fillthescreen;