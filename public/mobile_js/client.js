
var Clients = new Array();
var Times = new Array();
var DefaultUserWaters = new Array();
var VoteLotteryWaters = new Array();
var homeURL = $("#homeURL").val();
$(function () {


    GetWaterList();
    GetList();

    $('body').on('change', '#select_time', function () {
        var id = $(this).val();
        $('#frm_user').find('tr.per_td').hide();
        $('#frm_user').find('tr#per_td_' + id).show();
        $('body').on('change', '#select_time', function () {
            var id = $(this).val();
            $('#frm_user').find('tr.per_td').hide();
            $('#frm_user').find('tr#per_td_' + id).show();
        })
    })



    $('body').on('click', '.tooltip-bikay', function () {
        $(this).remove();
    });


    $('body').on('click', '#btn_save', function () {
        $frm = $('#frm_user');
        var userLotteryWaters = new Array();
        var user = {
            Id: $frm.find('input[name="userId"]').val(),
            UserName: $frm.find('input[name="name"]').val(),
            NickName: $frm.find('input[name="id"]').val(),
            PreBalance: $frm.find('input[name="pre_balance"]').val(),
            PhoneNumber: $frm.find('input[name="phone"]').val(),
            LimitTwoDigit: $frm.find('input[name="LimitTwoDigit"]').val(),
            LimitThreeDigit: $frm.find('input[name="LimitThreeDigit"]').val(),
            LimitFourDigit: $frm.find('input[name="LimitFourDigit"]').val(),
            Limit2DLast5: $frm.find('input[name="Limit2DLast5"]').val(),
            Limit2DLast10: $frm.find('input[name="Limit2DLast10"]').val(),
            Limit3DLast5: $frm.find('input[name="Limit3DLast5"]').val(),
            Limit3DLast10: $frm.find('input[name="Limit3DLast10"]').val(),
            Limit4DLast5: $frm.find('input[name="Limit4DLast5"]').val(),
            Limit4DLast10: $frm.find('input[name="Limit4DLast10"]').val(),
            IsHeadListNoZero: $frm.find('input[name="HeaderList"]').prop('checked'),
            UserType: 0
        }

        if (user.UserName == '' || user.NickName == '' || user.PhoneNumber == '' || user.PhoneNumber.length < 8) {
            alert('ចូរបំពាញគ្រប់កន្លែងដែលមាន​ *');
            return;
        }

        $.each(Times, function (i, w) {
            var $tr = $frm.find('tr#per_td_' + w.Id);
            var water = {
                LotteryTimeId: w.Id,
                TwoDigit: $tr.find('input[name="TwoDigit"]').val(),
                ThreeDigit: $tr.find('input[name="ThreeDigit"]').val(),
                PayTwoDigit: $tr.find('input[name="PayTwoDigit"]').val(),
                PayThreeDigit: $tr.find('input[name="PayThreeDigit"]').val(),
                PayFourDigit: $tr.find('input[name="PayFourDigit"]').val(),
                PayFiveDigit: $tr.find('input[name="PayFiveDigit"]').val(),
                Percentage: $tr.find('input[name="Percentage"]').val()
            }
            userLotteryWaters.push(water);
        })
        $.ajax({
            type: "POST",
            url: user.Id > 0 ? '/Client/Update/' : '/Client/Save/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ user: user, userLotteryWaters: userLotteryWaters }),
            success: function (data) {
                if (AjaxError(data) == false) {
                    $frm.hide().parent().hide();
                    GetList();
                }
            },
            error: function (data) { }

        });
    })

    $('body').on('blur', '.printerNo', function () {
        var n = $(this).val();
        var userId = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: '/Client/SetPrinter/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userId: userId, printerNo: n }),
            success: function (data) {
                console.log(data);
                if (AjaxError(data) == false) {

                }
            },
            error: function (data) { }

        });
    })

});



validate_form = function(id, info) {
        var required;
        if (info == null) {
          info = '';
        }
        required = '<div class="tooltip-bikay"><div class="tooltip-bikay-arrow"></div><div class="tooltip-bikay-inner">' + info + '.</div></div>';
        return $(id).before(required);
};

function validateForm(formID){
    var num;
        num = 0;
    $(formID + " .required").each(function(index, element) {
        var id, info, value;
        value = $(element).val();
        id = $(element).attr('id');
          // alert(id);
      if (value === '') {
        $('#' + id).prev('.tooltip-bikay').remove();
        validate_form('#' + id, info = 'required');

        if(id == 'ct_ec_id'){
          $('#' + id).prev('.tooltip-bikay').addClass('newtop');
        }

        return num = num + 1;
      } else {
        
          return $('#' + id).prev('.tooltip-bikay').remove();
      }
    });
    if (num > 0) {
      return 1;
    } else {
      return 0;
    }
}

function GetWaterList(callback) {
    $.ajax({
        type: "get",
        url: homeURL+'/mobile/me/getwater',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.users);
                // DefaultUserWaters = data.DefaultUserWaters;
                // VoteLotteryWaters = data.VoteLotteryWaters;
                // $.each(data.LotteryTimes, function (i, t) {
                //     Times.push(t);
                // });
                if(data.users.s_dollar == 1){
                    $("input[name='isUseDollar']").prop( "checked", true );
                }
                if(data.users.s_print_cut == 1){
                    $("input[name='isPrintKatTek']").prop( "checked", true );
                }
                

                VoteLotteryWaters = data.water;
                // console.log(VoteLotteryWaters,'VoteLotteryWaters');
                TableWaters(VoteLotteryWaters);
                // SelectHtml();

                if (typeof callback === 'function') {
                    callback();
                }
        },
        error: function (data) { }

    });
}



function GetList() {
    var homeUrl = $("#homeURL").val();
    console.log($('meta[name="csrf-token"]').attr('content'));

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type:'POST',
        url:homeURL+'/mobile/getlist',
        data:{userType:0},

        success:function(data){

          Clients = data.Clients;
          TableClients(data.Clients);

        }

    });
}

function TableClients(data) {
    var tr = '';
    $.each(data, function (index, d) {
        tr += '<tr data-id="'+d.s_id+'">';
        tr += '<td class="classtd2">' + (index + 1) + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + d.s_id + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + d.s_name + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + d.s_phone + '</td>';
        tr += '<td class="classtd btn-hand"  onclick="SaveUpdate(' + d.s_id + ', event);"><div>VN</div>2លេខ x ' + d.s_two_digit_charge + '<br>3លេខ x ' + d.s_three_digit_charge + '<div>KH</div>2លេខ x ' + d.s_two_digit_kh_charge + '<br>3លេខ x ' + d.s_three_digit_kh_charge + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);" >' + (d.Active == 1 ? "ផ្អាក" : "តំណើការ") + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + GetSCode(d.SCode) + '</td>';
        if(d.Active == 0){
            tr += '<td class="classtd" onclick="ActiveClient(' + d.s_id + ', ' + d.Active + ')"><i style="color:green;" class="fa fa-check-square-o" aria-hidden="true"></i></td>';
        }else{
            // tr += '<td class="classtd" ></td>';
            tr += '<td class="classtd" onclick="ActiveClient(' + d.s_id + ', ' + d.Active + ')"><i style="color:green;" class="fa fa-square-o" aria-hidden="true"></i></td>';
        }
        
        tr += '</tr>';


    });
    console.log(tr);

    $('#list_user table>tbody').html(tr);
}

function TableChilds(data) {
    var tr = '';
    $.each(data, function (index, d) {
        tr += '<tr data-id="' + d.Id + '">';
        tr += '<td class="classtd">' + d.NickName + '</td>';
        tr += '<td class="classtd">' + d.UserName + '</td>';
        tr += '<td class="classtd">' + d.PhoneNumber + '</td>';
        tr += '<td class="classtd"><button class="btn-show-child"><i class="fa fa-list" aria-hidden="true"></i></button></td>';
        tr += '<td class="classtd btn-hand" title="' + GetUserWaters(d.UserLotteryWaters) + '" onclick="ViewUserWaters(' + d.Id + ', event)"></td>';
        tr += '<td class="classtd text-center"><input  onclick="ChangeLimitMoney(' + d.Id + ')" type="checkbox" ' + (d.IsLimitMoney == true ? "checked=checked" : "") + '></td>';
        tr += '<td class="classtd btn-reset" onclick="ResetPassword(' + d.Id + ')"></td>';
        tr += '<td class="classtd text-center"><input onclick="ChangeUseDollar(' + d.Id + ')" type="checkbox" ' + (d.HasDollar == true ? "checked=checked" : "") + '></td>';
        tr += '<td class="classtd text-center"><input onclick="ChangeCanCreateChild(' + d.Id + ')" type="checkbox" ' + (d.CanCreateChild == true ? "checked=checked" : "") + '></td>';
        tr += '<td class="classtd" onclick="ActiveClient(' + d.Id + ', ' + d.Active + ')">' + (d.Active == 1 ? "ផ្អាក" : "តំណើការ") + '</td>';
        tr += '<td class="classtd" onclick="ActiveClient(' + d.s_id + ', event)"><i style="color:green;" class="fa fa-check-square-o" aria-hidden="true"></i></td>';
        tr += '</tr>';
    });

    return tr;
}

function TableWaters(data) {
    var tr = '';
    $.each(data, function (index, d) {
        tr += '<tr time-id=' + d.w_id + ' class="txt">';
        if(d.time <= 6){
            tr += '<td class="typeLottery">VN '+d.pav_value.slice(1);+'</td>';
        }else{
            tr += '<td class="typeLottery">KH '+d.pav_value.slice(1);+'</td>';
        }
        

        // if(d.time == 5){
        //     tr += '<td>ល្ងាច</td>';
        // }else if(d.time == 6){
        //     tr += '<td>យប់</td>';
        // }else if(d.time == 23){
        //     tr += '<td>ព្រឹក</td>';
        // }else if(d.time == 24){
        //     tr += '<td>ថ្ងៃ</td>';
        // }else{
        //     tr += '<td></td>';
        // }
        tr += '<td><input style="width:100px" onKeypress="KeyValidate(event)" maxlength="5" class="input" type="text" pattern="[0-9]*" size="4" name="sell2" id="idsell2_1" value="' + d.w_2digit + '">%</td>';
        tr += '<td><input style="width:100px"  onKeypress="KeyValidate(event)" maxlength="5" class="input" type="text" pattern="[0-9]*" size="4" name="sell3" id="idsell2_1" value="' + d.w_3digit + '">%</td>';
        tr += '</tr>';
    });
    $('#tr-water').after(tr);
}

function GetUserWaters(userWaters) {
    var title = '';
    $.each(userWaters, function (i, w) {
        title += GetTimeNameById(w.LotteryTimeId) + '          2លេខ x ' + w.TwoDigit + ' 3លេខ x' + w.ThreeDigit + '\n';
    })
    return title;
}

function SelectHtml() {
    var options = '';
    $.each(Times, function (index, d) {
        options += '<option value=' + d.Id + '>' + d.Name + '</tr>';
    });
    $('#select_time').html(options);
}

function GetTimeNameById(id) {
    var t = Times.filter(function (obj) {
        return obj.Id == id;
    })[0];
    if (t != undefined) return t.Name;
    return '';
}

function ResetPassword(userId) {
    if (window.confirm('កំណត់លេខសំងាត់ និង លេខសុវត្តិភាព ឡើងវិញ')) {
        $.ajax({
            type: "POST",
            url: '/Client/ResetPassword/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userId: userId }),
            success: function (data) {
                if (AjaxError(data) == false) {

                }
            },
            error: function (data) { }

        });
    }
}

function GetVoteLotteryWaters(userId) {
        $.ajax({
            type: "GET",
            url: '/Client/GetVoteLotteryWater/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { userId: userId },
            success: function (data) {
                if (AjaxError(data) == false) {
                    $.each(data.VoteLotteryWaters, function (i, w) {
                        $('#per_td_' + w.LotteryTimeId).find('input[name="IsUseLottery"]').prop('checked', w.IsUsed);
                    })
                }
            },
            error: function (data) { }

        });
    
}
function DeleteUser() {
        if (window.confirm('ភ្នាក់ងារឈប់លក់ហើយ?')) {
            var userId = $("#s_id").val();
            if(s_id != ''){
                $.ajax({
                    type: "GET",
                    url: homeURL+'/mobile/me/deleteuser',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: { userId: userId },
                    success: function (data){
                        if (data.status == 'success') {

                            alert(data.msg);
                            GetList();
                            ClearForm('#waitingShowInfo');
                            $("#update_info_user").hide();
                            ShowClient();

                        }else{
                            alert(data.msg);
                        }
                    },
                    error: function (data) { }
                });
            }else{
                alert("don't have ID");
            }
            
        }
}

function ActiveClient(userId, active) {
    if(active == 0){
        var message = 'ផ្អាកឱ្យលក់?';
    }else{
        var message = 'ឱ្យលក់បាន​?';
    }
    
    // if (active == false) {
    //     message = 'ឱ្យតំណើការលក់បាន​?';
    // }
    // alert(userId);
    if (window.confirm(message)) {
        $.ajax({
            type: "get",
            url: homeURL+'/mobile/me/activeclient',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { userId: userId },
            success: function (data) {
                if (data.status == 'success') {
                    GetList();
                }
            },
            error: function (data) { }

        });
    }
}

function ViewUserWaters(userId, e) {
    var user = Clients.filter(function (obj) {
        return obj.Id == userId
    })[0];
    if (user != undefined) {
        // var tr = '';
        // tr += '<tr><td>' + GetTimeNameById(user.s_id) + '</td><td>2លេខ x ' + user.s_two_digit_charge + '</td><td>3លេខ x ' + user.s_three_digit_charge + '</td></tr>';

        // $('#frm_user').hide();
        // $('#showWaters').show().find('table tbody').html(tr);
        // var pageY = e.pageY - e.clientY;
        // if (pageY < 100)
        //     pageY = 100;
        // var pageX = e.clientX - 100;
        // xstooltip_show('div1', '', pageX, pageY);
    }
}

function ClearForm(main){
    var mainList = $(main);

    mainList.find('#idUser').html();
    mainList.find('#s_id').val("");
    mainList.find('#s_name').val("");
    mainList.find('#s_phone').val("");
    mainList.find('#LimitMoneyAmount').val("");
    mainList.find('#SCode').val("");
    mainList.find('#s_two_digit_charge').val("");
    mainList.find('#s_three_digit_charge').val("");
    mainList.find('#s_two_digit_paid').val("");
    mainList.find('#s_three_digit_paid').val("");

    mainList.find('#CanCreateChild').prop( "checked", false );
    mainList.find('#s_dollar').prop( "checked", false );
    mainList.find('#Active').prop( "checked", false );
}

function SaveUpdate(clientId, e) {
    return false;
    $(".hideall").hide();
    $(".btncheck").hide();
    $("#update_info_user").show();
    $("#update_info_user").find(".update").show();
    

    var user = Clients.filter(function (obj) {
        return obj.s_id == clientId
    })[0];

    console.log(user);

    var mainList = $("#waitingShowInfo");

    mainList.find('#idUser').html(user.s_id);
    mainList.find('#s_id').val(user.s_id);
    mainList.find('#s_name').val(user.s_name);
    mainList.find('#s_phone').val(user.s_phone);
    mainList.find('#LimitMoneyAmount').val(user.LimitMoneyAmount);
    mainList.find('#SCode').val(user.SCode);
    mainList.find('#s_two_digit_charge').val(user.s_two_digit_charge);
    mainList.find('#s_three_digit_charge').val(user.s_three_digit_charge);
    mainList.find('#s_two_digit_paid').val(user.s_two_digit_paid);
    mainList.find('#s_three_digit_paid').val(user.s_three_digit_paid);

    mainList.find('#s_phone_login').val(user.s_phone_login);
    mainList.find('#s_password').val(user.s_password);

    if(user.CanCreateChild == 1){
        mainList.find('#CanCreateChild').prop( "checked", true );
    }

    if(user.s_dollar == 1){
        mainList.find('#s_dollar').prop( "checked", true );
    }

    if(user.Active == 1){
        mainList.find('#Active').prop( "checked", true );
    }
}

function AddClient() {
    
    $(".hideall").hide();
    $(".btncheck").hide();
    ClearForm('#waitingShowInfo');
    $("#update_info_user").show();
    $("#update_info_user").find(".create").show();
    


    var mainList = $("#waitingShowInfo");
    mainList.find('#idUser').html("និងបង្កេីត");
}

function SetForm(client, isNew) {
    $frm = $('#frm_user');
    $frm.find('input[name="userId"]').val(isNew == true ? '' : client.Id);
    $frm.find('input[name="name"]').val(isNew == true ? '' : client.UserName);
    $frm.find('input[name="id"]').val(isNew == true ? '' : client.NickName);
    $frm.find('input[name="phone"]').val(isNew == true ? '' : client.PhoneNumber);
    $frm.find('input[name="LimitTwoDigit"]').val(isNew == true ? '6000' : client.LimitTwoDigit);
    $frm.find('input[name="LimitThreeDigit"]').val(isNew == true ? '1000' : client.LimitThreeDigit);
    $frm.find('input[name="LimitFourDigit"]').val(isNew == true ? '100' : client.LimitFourDigit);
    $frm.find('input[name="Limit2DLast5"]').val(isNew == true ? '0' : client.Limit2DLast5);
    $frm.find('input[name="Limit2DLast10"]').val(isNew == true ? '0' : client.Limit2DLast10);
    $frm.find('input[name="Limit3DLast5"]').val(isNew == true ? '0' : client.Limit3DLast5);
    $frm.find('input[name="Limit3DLast10"]').val(isNew == true ? '0' : client.Limit3DLast10);
    $frm.find('input[name="Limit4DLast5"]').val(isNew == true ? '0' : client.Limit4DLast5);
    $frm.find('input[name="Limit4DLast10"]').val(isNew == true ? '0' : client.Limit4DLast10);
    $frm.find('input[name="HeaderList"]').prop('checked', isNew == true ? false : client.IsHeadListNoZero);
    $frm.find('input[name="pre_balance"]').val(isNew == true ? '0' : client.PreBalance);
    $frm.find('#btn_save').val(isNew == true ? 'បញ្ចូលក្នុងកម្មវិធី' : 'កែប្រែ');
    $.each((isNew == true ? DefaultUserWaters : client.UserLotteryWaters), function (i, w) {
        var $tr = $frm.find('#per_td_' + w.LotteryTimeId);
        $tr.find('input[name="TwoDigit"]').val(w.TwoDigit);
        $tr.find('input[name="ThreeDigit"]').val(w.ThreeDigit);
        $tr.find('input[name="PayTwoDigit"]').val(w.PayTwoDigit);
        $tr.find('input[name="PayThreeDigit"]').val(w.PayThreeDigit);
        $tr.find('input[name="PayFourDigit"]').val(w.PayFourDigit);
        $tr.find('input[name="PayFiveDigit"]').val(w.PayFiveDigit);
        $tr.find('input[name="Percentage"]').val(w.Percentage);
    });

    $.each(VoteLotteryWaters, function (i, v) {
        if (isNew == true || v.IsUsed == false) {
            $('#per_td_' + v.LotteryTimeId).find('.IsUseLottery').hide();
        } else {
            $('#per_td_' + v.LotteryTimeId).find('.IsUseLottery').show();
        }
    });
}

function CreateDataClient(){

    var mainList = $("#waitingShowInfo");

    var CanCreateChild = mainList.find('input[name="CanCreateChild"]').prop('checked');
    var s_dollar = mainList.find('input[name="s_dollar"]').prop('checked');
    var Active = mainList.find('input[name="Active"]').prop('checked');

    // var s_id = mainList.find('input[name="s_id"]').val();
    var s_name = mainList.find('input[name="s_name"]').val();
    var s_phone = mainList.find('input[name="s_phone"]').val();

    var LimitMoneyAmount2R = mainList.find('input[name="LimitMoneyAmount2R"]').val();
    var LimitMoneyAmount3R = mainList.find('input[name="LimitMoneyAmount3R"]').val();
    var LimitMoneyAmount2D = mainList.find('input[name="LimitMoneyAmount2D"]').val();
    var LimitMoneyAmount3D = mainList.find('input[name="LimitMoneyAmount3D"]').val();

    var SCode = mainList.find('input[name="SCode"]').val();
    var s_two_digit_charge = mainList.find('input[name="s_two_digit_charge"]').val();
    var s_three_digit_charge = mainList.find('input[name="s_three_digit_charge"]').val();

    var s_two_digit_paid = mainList.find('input[name="s_two_digit_paid"]').val();
    var s_three_digit_paid = mainList.find('input[name="s_three_digit_paid"]').val();

    var s_phone_login = mainList.find('input[name="s_phone_login"]').val();
    var s_password = mainList.find('input[name="s_password"]').val();

    if(validateForm('#waitingShowInfo') == 0){
        if(s_two_digit_paid > 90){
            alert('សង2លេខមិនអាចធំជាង90');
            return false;
        }
        if(s_three_digit_paid > 860){
            alert('សង3លេខមិនអាចធំជាង860');
            return false;
        }
        
        $.ajax({
            type: "GET",
            url: homeURL+'/mobile/me/createuserinfo',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { CanCreateChild: CanCreateChild, s_dollar: s_dollar, Active: Active, s_name:s_name, s_phone:s_phone, LimitMoneyAmount2R:LimitMoneyAmount2R, LimitMoneyAmount3R:LimitMoneyAmount3R, LimitMoneyAmount2D:LimitMoneyAmount2D, LimitMoneyAmount3D:LimitMoneyAmount3D, SCode:SCode, s_two_digit_charge:s_two_digit_charge, s_three_digit_charge:s_three_digit_charge, s_two_digit_paid:s_two_digit_paid, s_three_digit_paid:s_three_digit_paid, s_phone_login:s_phone_login, s_password:s_password},
            success: function (data) {

                if(data.status == 'success'){
                    GetList();
                    ClearForm('#waitingShowInfo');

                    $("#update_info_user").hide();
                    ShowClient();

                    alert("រក្សាទុកបានសំរេច​!!!");
                }else{
                    alert(data.msg);
                }
                
            },
            error: function (data) { }

        });
    }

    
        
}

function SaveDataClient(){
    var mainList = $("#waitingShowInfo");

    var CanCreateChild = mainList.find('input[name="CanCreateChild"]').prop('checked');
    var s_dollar = mainList.find('input[name="s_dollar"]').prop('checked');
    var Active = mainList.find('input[name="Active"]').prop('checked');
    var s_id = mainList.find('input[name="s_id"]').val();
    var s_name = mainList.find('input[name="s_name"]').val();
    var s_phone = mainList.find('input[name="s_phone"]').val();
    
    var LimitMoneyAmount2R = mainList.find('input[name="LimitMoneyAmount2R"]').val();
    var LimitMoneyAmount3R = mainList.find('input[name="LimitMoneyAmount3R"]').val();
    var LimitMoneyAmount2D = mainList.find('input[name="LimitMoneyAmount2D"]').val();
    var LimitMoneyAmount3D = mainList.find('input[name="LimitMoneyAmount3D"]').val();

    var SCode = mainList.find('input[name="SCode"]').val();
    var s_two_digit_charge = mainList.find('input[name="s_two_digit_charge"]').val();
    var s_three_digit_charge = mainList.find('input[name="s_three_digit_charge"]').val();

    var s_two_digit_paid = mainList.find('input[name="s_two_digit_paid"]').val();
    var s_three_digit_paid = mainList.find('input[name="s_three_digit_paid"]').val();

    var s_phone_login = mainList.find('input[name="s_phone_login"]').val();
    var s_password = mainList.find('input[name="s_password"]').val();

    if(validateForm('#waitingShowInfo') == 0){
        // console.log(s_two_digit_paid);
        // return false;
        if(s_two_digit_paid > 90){
            alert('សង2លេខមិនអាចធំជាង90');
            return false;
        }
        if(s_three_digit_paid > 860){
            alert('សង3លេខមិនអាចធំជាង860');
            return false;
        }
        $.ajax({
        type: "GET",
        url: homeURL+'/mobile/me/saveuserinfo',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { CanCreateChild: CanCreateChild, s_dollar: s_dollar, Active: Active, s_id:s_id, s_name:s_name, s_phone:s_phone, LimitMoneyAmount2R:LimitMoneyAmount2R, LimitMoneyAmount3R:LimitMoneyAmount3R, LimitMoneyAmount2D:LimitMoneyAmount2D, LimitMoneyAmount3D:LimitMoneyAmount3D, SCode:SCode, s_two_digit_charge:s_two_digit_charge, s_three_digit_charge:s_three_digit_charge, s_two_digit_paid:s_two_digit_paid, s_three_digit_paid:s_three_digit_paid, s_phone_login:s_phone_login, s_password:s_password},
        success: function (data) {

            if(data.status == 'success'){
                GetList();
                ClearForm('#waitingShowInfo');

                $("#update_info_user").hide();
                ShowClient();

                alert("រក្សាទុកបានសំរេច​!!!");
            }else{
                alert(data.msg);
            }


            
        },
        error: function (data) { }

    });
    }
    
    
}

function SaveSellPercent() {
    var VoteLotteryWaters = new Array();
    var isUseDollar = $('#sell_percent').find('input[name="isUseDollar"]').prop('checked');
    var isPrintKatTek = $('#sell_percent').find('input[name="isPrintKatTek"]').prop('checked');
    var trs = $('#sell_percent').find('tr.txt');
    $.each(trs, function (i, tr) {
        var water = {
            LotteryTimeId: $(tr).attr('time-id'),
            TwoDigit: $(tr).find('input[name="sell2"]').val(),
            ThreeDigit: $(tr).find('input[name="sell3"]').val(),
        }
        VoteLotteryWaters.push(water);
    })
    // alert("abd");

    $.ajax({
        type: "GET",
        url: homeURL+'/mobile/me/savewater',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { voteLotteryWaters: VoteLotteryWaters, isUseDollar: isUseDollar, isPrintKatTek: isPrintKatTek },
        success: function (data) {
            console.log(data);
            alert("រក្សាទុកបានសំរេច​!!!");
        },
        error: function (data) { }

    });
}

function ChangeUseDollar(id) {
    $.ajax({
        type: "POST",
        url: '/Client/ChangeUseDollar/',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ userId: id }),
        success: function (data) {
            console.log(data);
            if (AjaxError(data) == false) {

            }
        },
        error: function (data) { }

    });
}

function ChangeCanCreateChild(id) {
        $.ajax({
            type: "POST",
            url: '/Client/ChangeCanCreateChild/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userId: id }),
            success: function (data) {
                console.log(data);
                if (AjaxError(data) == false) {

                }
            },
            error: function (data) { }

        });
    
}

function ChangeLimitMoney(id) {
    $.ajax({
        type: "POST",
        url: '/Client/ChangeLimitMoney/',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ userId: id }),
        success: function (data) {
            console.log(data);
            if (AjaxError(data) == false) {

            }
        },
        error: function (data) { }

    });
}



function IsUseLotteryTime(timeId) { 
    var userId = $("#frm_user").find('input[name="userId"]').val();
    var isUsed = $('#per_td_' + timeId).find('input[name="IsUseLottery"]').is(':checked');
    $.ajax({
        type: "POST",
        url: '/Client/IsUserLotteryTime/',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ userId: userId, lotteryTimeId: timeId, isUsed: isUsed }),
        success: function (data) { 
        },
        error: function (data) { }

    });
}


function xstooltip_show(tooltipId, parentId, posX, posY) {
    it = document.getElementById(tooltipId);
    it.style.top = posY + 'px';
    it.style.left = posX + 'px';
    it.style.visibility = 'visible';
    it.style.display = 'block';
}

function AjaxError(data) {
    let isError = data.IsError;
    if (isError == true) {
        switch (data.ErrorCode) {
            case 100:
                window.location = '/Account/Logout/';
                break;
            case 101:
                alert('មានគេចូលគណនីរបស់អ្នក។ ចូលប្រើម្តងទៀត');
                window.location = '/Account/Logout/';
                break;
            default:
                alert(data.ErrorMessage);
                return;
                break;
        }
    }

    return isError;
}

function KeyValidate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function GetSCode(sc) {

    if (sc != undefined && sc.length > 3) {
        return '..' + sc.substr(sc.length - 4);
    }

    return sc;
}

function ShowClient() {
    $('#td_users').show();
    $('#sell_percent').hide();
    $('#update_info_user').hide();
    $("#voteWater").removeClass('active');
    $("#ShowClientList").addClass('active');
}
function ShowVoteWater() {
    $('#td_users').hide();
    $('#update_info_user').hide();
    $('#sell_percent').show();
    $("#voteWater").addClass('active');
    $("#ShowClientList").removeClass('active');
}

$("body").on('click', '.btn-show-child', function () {
    var tr = $(this).closest('tr'); 
    if (tr.attr('ready') != '1') {
        var userId = tr.attr('data-id');
        $.ajax({
            type: "GET",
            url: '/Client/GetAllClients/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { userId: userId },
            success: function (data) {
                if (data.Clients!=null && data.Clients.length > 0) {
                    $.each(data.Clients, function (i, v) {
                        Clients.push(v);
                    })
                    console.log(Clients);
                    var html = '<tr><td colspan="14" class="td-child"><table width="100%">';
                    html += '<thead align="center">';
                    html += '<tr align = "center" >';
                    html += '<th class="classtd1">ឈ្មោះហៅ</th>';
                    html += '<th class="classtd1">ឈ្មោះពិត</th>';
                    html += '<th class="classtd1">លេខទូរស័ព្ទ</th>'; 
                    html += '<th class="classtd1">កូន</th>';
                    html += '<th class="classtd1">គុណទឹក</th>';
                    html += '<th class="classtd1">កំហិតលុយ</th>';
                    html += '<th class="classtd1">PWD</th>';
                    html += '<th class="classtd1">មាន $</th>';
                    html += ' <th class="classtd1">បង្កើតកូន</th>';
                    html += '<th class="classtd1">ផ្អាក</th>';
                    html += '<th class="classtd1">នៅលក់</th>';
                    html += '</tr >';
                    html += '</thead >';
                    html += TableChilds(data.Clients);
                    html += '<table></td></tr>';
                    tr.after(html);
                }
            },
            error: function (data) { },
        })
    } else {
        var n = tr.next();
        if (n.attr('data-id') == undefined){
            if (n.css('display') == 'none') {
                n.show();
            } else {
                n.hide();
            }
        }
    }
    tr.attr('ready','1');
});