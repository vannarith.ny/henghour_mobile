
var Clients = new Array();
var Times = new Array();
var DefaultUserWaters = new Array();
var VoteLotteryWaters = new Array();
var homeURL = $("#homeURL").val();
$(function () {


    // GetWaterList();
    // GetList();

    $('body').on('change', '#select_time', function () {
        var id = $(this).val();
        $('#frm_user').find('tr.per_td').hide();
        $('#frm_user').find('tr#per_td_' + id).show();
        $('body').on('change', '#select_time', function () {
            var id = $(this).val();
            $('#frm_user').find('tr.per_td').hide();
            $('#frm_user').find('tr#per_td_' + id).show();
        })
    })



    $('body').on('click', '.tooltip-bikay', function () {
        $(this).remove();
    });


    $('body').on('click', '#btn_save', function () {
        $frm = $('#frm_user');
        var userLotteryWaters = new Array();
        var user = {
            Id: $frm.find('input[name="userId"]').val(),
            UserName: $frm.find('input[name="name"]').val(),
            NickName: $frm.find('input[name="id"]').val(),
            PreBalance: $frm.find('input[name="pre_balance"]').val(),
            PhoneNumber: $frm.find('input[name="phone"]').val(),
            LimitTwoDigit: $frm.find('input[name="LimitTwoDigit"]').val(),
            LimitThreeDigit: $frm.find('input[name="LimitThreeDigit"]').val(),
            LimitFourDigit: $frm.find('input[name="LimitFourDigit"]').val(),
            Limit2DLast5: $frm.find('input[name="Limit2DLast5"]').val(),
            Limit2DLast10: $frm.find('input[name="Limit2DLast10"]').val(),
            Limit3DLast5: $frm.find('input[name="Limit3DLast5"]').val(),
            Limit3DLast10: $frm.find('input[name="Limit3DLast10"]').val(),
            Limit4DLast5: $frm.find('input[name="Limit4DLast5"]').val(),
            Limit4DLast10: $frm.find('input[name="Limit4DLast10"]').val(),
            IsHeadListNoZero: $frm.find('input[name="HeaderList"]').prop('checked'),
            UserType: 0
        }

        if (user.UserName == '' || user.NickName == '' || user.PhoneNumber == '' || user.PhoneNumber.length < 8) {
            alert('ចូរបំពាញគ្រប់កន្លែងដែលមាន​ *');
            return;
        }

        $.each(Times, function (i, w) {
            var $tr = $frm.find('tr#per_td_' + w.Id);
            var water = {
                LotteryTimeId: w.Id,
                TwoDigit: $tr.find('input[name="TwoDigit"]').val(),
                ThreeDigit: $tr.find('input[name="ThreeDigit"]').val(),
                PayTwoDigit: $tr.find('input[name="PayTwoDigit"]').val(),
                PayThreeDigit: $tr.find('input[name="PayThreeDigit"]').val(),
                PayFourDigit: $tr.find('input[name="PayFourDigit"]').val(),
                PayFiveDigit: $tr.find('input[name="PayFiveDigit"]').val(),
                Percentage: $tr.find('input[name="Percentage"]').val()
            }
            userLotteryWaters.push(water);
        })
        $.ajax({
            type: "POST",
            url: user.Id > 0 ? '/Client/Update/' : '/Client/Save/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ user: user, userLotteryWaters: userLotteryWaters }),
            success: function (data) {
                if (AjaxError(data) == false) {
                    $frm.hide().parent().hide();
                    GetList();
                }
            },
            error: function (data) { }

        });
    })

    $('body').on('blur', '.printerNo', function () {
        var n = $(this).val();
        var userId = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: '/Client/SetPrinter/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userId: userId, printerNo: n }),
            success: function (data) {
                console.log(data);
                if (AjaxError(data) == false) {

                }
            },
            error: function (data) { }

        });
    })

});



validate_form = function(id, info) {
        var required;
        if (info == null) {
          info = '';
        }
        required = '<div class="tooltip-bikay"><div class="tooltip-bikay-arrow"></div><div class="tooltip-bikay-inner">' + info + '.</div></div>';
        return $(id).before(required);
};

function validateForm(formID){
    var num;
        num = 0;
    $(formID + " .required").each(function(index, element) {
        var id, info, value;
        value = $(element).val();
        id = $(element).attr('id');
          // alert(id);
      if (value === '') {
        $('#' + id).prev('.tooltip-bikay').remove();
        validate_form('#' + id, info = 'required');

        if(id == 'ct_ec_id'){
          $('#' + id).prev('.tooltip-bikay').addClass('newtop');
        }

        return num = num + 1;
      } else {
        
          return $('#' + id).prev('.tooltip-bikay').remove();
      }
    });
    if (num > 0) {
      return 1;
    } else {
      return 0;
    }
}

function GetWaterList(callback) {
    $.ajax({
        type: "get",
        url: homeURL+'/mobile/me/getwater',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.users);
                // DefaultUserWaters = data.DefaultUserWaters;
                // VoteLotteryWaters = data.VoteLotteryWaters;
                // $.each(data.LotteryTimes, function (i, t) {
                //     Times.push(t);
                // });
                if(data.users.s_dollar == 1){
                    $("input[name='isUseDollar']").prop( "checked", true );
                }
                if(data.users.s_print_cut == 1){
                    $("input[name='isPrintKatTek']").prop( "checked", true );
                }
                

                VoteLotteryWaters = data.water;
                TableWaters(VoteLotteryWaters);
                // SelectHtml();

                if (typeof callback === 'function') {
                    callback();
                }
        },
        error: function (data) { }

    });
}


function pad(numb) {
    return (numb < 10 ? '0' : '') + numb;
}

function GetList() {
    // var homeUrl = $("#homeURL").val();
    // console.log($('meta[name="csrf-token"]').attr('content'));
    var n = $("#clientId").val();
    var t = $("#fromDate").datepicker("getDate"),
        i = $("#toDate").datepicker("getDate");
    
    var date     = new Date(t);
    var year  = pad(date.getFullYear());
    var month = pad(date.getMonth() + 1);
    var day   = pad(date.getDate());
    var t = year +'-'+ month +'-'+ day;

    var date     = new Date(i);
    var year  = pad(date.getFullYear());
    var month = pad(date.getMonth() + 1);
    var day   = pad(date.getDate());

    var i = year +'-'+ month +'-'+ day;

    $.ajax({
        
        type:'GET',
        url:homeURL+'/mobile/transctionstaff/getTransctionList',
        data:{
            userId: n,
            fromDate: t,
            toDate: i
        },

        success:function(n){
            // console.log(data);
            if(n.status == 'success'){
                Clients = n.Clients;
                $("#dataList").html(n.Clients);
            }else{
                $("#dataList").html(n.msg);
            }

        }

    });
}

function TableClients(data) {
    var tr = '';
    $.each(data, function (index, d) {
        tr += '<tr data-id="'+d.s_id+'">';
        tr += '<td class="classtd2">' + (index + 1) + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + d.s_id + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + d.s_name + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + d.s_phone + '</td>';
        tr += '<td class="classtd btn-hand"  onclick="SaveUpdate(' + d.s_id + ', event);">2លេខ x ' + d.s_two_digit_charge + '<br>3លេខ x ' + d.s_three_digit_charge + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);" >' + (d.Active == 1 ? "ផ្អាក" : "តំណើការ") + '</td>';
        tr += '<td class="classtd" onclick="SaveUpdate(' + d.s_id + ', event);">' + GetSCode(d.SCode) + '</td>';
        if(d.Active == 0){
            tr += '<td class="classtd" onclick="ActiveClient(' + d.s_id + ', ' + d.Active + ')"><i style="color:green;" class="fa fa-check-square-o" aria-hidden="true"></i></td>';
        }else{
            tr += '<td class="classtd" ></td>';
        }
        
        tr += '</tr>';


    });
    console.log(tr);

    $('#list_user table>tbody').html(tr);
}



function GetUserWaters(userWaters) {
    var title = '';
    $.each(userWaters, function (i, w) {
        title += GetTimeNameById(w.LotteryTimeId) + '          2លេខ x ' + w.TwoDigit + ' 3លេខ x' + w.ThreeDigit + '\n';
    })
    return title;
}

function SelectHtml() {
    var options = '';
    $.each(Times, function (index, d) {
        options += '<option value=' + d.Id + '>' + d.Name + '</tr>';
    });
    $('#select_time').html(options);
}

function GetTimeNameById(id) {
    var t = Times.filter(function (obj) {
        return obj.Id == id;
    })[0];
    if (t != undefined) return t.Name;
    return '';
}

function ResetPassword(userId) {
    if (window.confirm('កំណត់លេខសំងាត់ និង លេខសុវត្តិភាព ឡើងវិញ')) {
        $.ajax({
            type: "POST",
            url: '/Client/ResetPassword/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userId: userId }),
            success: function (data) {
                if (AjaxError(data) == false) {

                }
            },
            error: function (data) { }

        });
    }
}

function GetVoteLotteryWaters(userId) {
        $.ajax({
            type: "GET",
            url: '/Client/GetVoteLotteryWater/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { userId: userId },
            success: function (data) {
                if (AjaxError(data) == false) {
                    $.each(data.VoteLotteryWaters, function (i, w) {
                        $('#per_td_' + w.LotteryTimeId).find('input[name="IsUseLottery"]').prop('checked', w.IsUsed);
                    })
                }
            },
            error: function (data) { }

        });
    
}
function DeleteUser() {
        if (window.confirm('ភ្នាក់ងារឈប់លក់ហើយ?')) {
            var userId = $("#idhide").val();
            if(userId != ''){
                $.ajax({
                    type: "GET",
                    url: homeURL+'/mobile/transctionstaff/delete/id/transction',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: { userId: userId },
                    success: function (data){
                        if (data.status == 'success') {

                            alert(data.msg);
                            GetList();
                            ClearForm('#waitingShowInfo');
                            $("#update_info_user").hide();
                            ShowClient();

                        }else{
                            alert(data.msg);
                        }
                    },
                    error: function (data) { }
                });
            }else{
                alert("don't have ID");
            }
            
        }
}

function ActiveClient(userId, active) {
    var message = 'ផ្អាកឱ្យលក់បាន​?';
    // if (active == false) {
    //     message = 'ឱ្យតំណើការលក់បាន​?';
    // }
    // alert(userId);
    if (window.confirm(message)) {
        $.ajax({
            type: "get",
            url: homeURL+'/mobile/me/activeclient',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { userId: userId },
            success: function (data) {
                if (data.status == 'success') {
                    GetList();
                }
            },
            error: function (data) { }

        });
    }
}

function ViewUserWaters(userId, e) {
    var user = Clients.filter(function (obj) {
        return obj.Id == userId
    })[0];
    if (user != undefined) {
        // var tr = '';
        // tr += '<tr><td>' + GetTimeNameById(user.s_id) + '</td><td>2លេខ x ' + user.s_two_digit_charge + '</td><td>3លេខ x ' + user.s_three_digit_charge + '</td></tr>';

        // $('#frm_user').hide();
        // $('#showWaters').show().find('table tbody').html(tr);
        // var pageY = e.pageY - e.clientY;
        // if (pageY < 100)
        //     pageY = 100;
        // var pageX = e.clientX - 100;
        // xstooltip_show('div1', '', pageX, pageY);
    }
}

function ClearForm(main){
    var mainList = $(main);

    mainList.find('#idUser').html();
    mainList.find('#s_id').val("");
    mainList.find('#s_name').val("");
    mainList.find('#s_phone').val("");
    mainList.find('#LimitMoneyAmount').val("");
    mainList.find('#SCode').val("");
    mainList.find('#s_two_digit_charge').val("");
    mainList.find('#s_three_digit_charge').val("");

    mainList.find('#CanCreateChild').prop( "checked", false );
    mainList.find('#s_dollar').prop( "checked", false );
    mainList.find('#Active').prop( "checked", false );
}

function SaveUpdate(clientId, e) {
    
    
    
    $.ajax({
        type: "get",
        url: homeURL+'/mobile/transctionstaff/gettransctionone/id',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { clientId: clientId },
        success: function (data) {
            if (data.status == 'success') {
                var user = data.Clients;
                var mainList = $("#waitingShowInfo");
                mainList.find('#child_id').val(user.s_id);
                mainList.find('#moneyR').val(user.st_price_r);
                mainList.find('#moneyD').val(user.st_price_d);
                mainList.find('#s_type').val(user.st_type);
                mainList.find('#date_in').val(user.st_date_diposit);
                mainList.find('#idhide').val(user.st_id);
                
            }
        },
        error: function (data) { }

    });

    $(".hideall").hide();
    $(".btncheck").hide();
    $("#update_info_user").show();
    $("#update_info_user").find(".update").show();

    
}

function AddClient() {
    
    $(".hideall").hide();
    $(".btncheck").hide();
    ClearForm('#waitingShowInfo');
    $("#update_info_user").show();
    $("#update_info_user").find(".create").show();
    


    var mainList = $("#waitingShowInfo");
    mainList.find('#idUser').html("និងបង្កេីត");
}

function SetForm(client, isNew) {
    $frm = $('#frm_user');
    $frm.find('input[name="userId"]').val(isNew == true ? '' : client.Id);
    $frm.find('input[name="name"]').val(isNew == true ? '' : client.UserName);
    $frm.find('input[name="id"]').val(isNew == true ? '' : client.NickName);
    $frm.find('input[name="phone"]').val(isNew == true ? '' : client.PhoneNumber);
    $frm.find('input[name="LimitTwoDigit"]').val(isNew == true ? '6000' : client.LimitTwoDigit);
    $frm.find('input[name="LimitThreeDigit"]').val(isNew == true ? '1000' : client.LimitThreeDigit);
    $frm.find('input[name="LimitFourDigit"]').val(isNew == true ? '100' : client.LimitFourDigit);
    $frm.find('input[name="Limit2DLast5"]').val(isNew == true ? '0' : client.Limit2DLast5);
    $frm.find('input[name="Limit2DLast10"]').val(isNew == true ? '0' : client.Limit2DLast10);
    $frm.find('input[name="Limit3DLast5"]').val(isNew == true ? '0' : client.Limit3DLast5);
    $frm.find('input[name="Limit3DLast10"]').val(isNew == true ? '0' : client.Limit3DLast10);
    $frm.find('input[name="Limit4DLast5"]').val(isNew == true ? '0' : client.Limit4DLast5);
    $frm.find('input[name="Limit4DLast10"]').val(isNew == true ? '0' : client.Limit4DLast10);
    $frm.find('input[name="HeaderList"]').prop('checked', isNew == true ? false : client.IsHeadListNoZero);
    $frm.find('input[name="pre_balance"]').val(isNew == true ? '0' : client.PreBalance);
    $frm.find('#btn_save').val(isNew == true ? 'បញ្ចូលក្នុងកម្មវិធី' : 'កែប្រែ');
    $.each((isNew == true ? DefaultUserWaters : client.UserLotteryWaters), function (i, w) {
        var $tr = $frm.find('#per_td_' + w.LotteryTimeId);
        $tr.find('input[name="TwoDigit"]').val(w.TwoDigit);
        $tr.find('input[name="ThreeDigit"]').val(w.ThreeDigit);
        $tr.find('input[name="PayTwoDigit"]').val(w.PayTwoDigit);
        $tr.find('input[name="PayThreeDigit"]').val(w.PayThreeDigit);
        $tr.find('input[name="PayFourDigit"]').val(w.PayFourDigit);
        $tr.find('input[name="PayFiveDigit"]').val(w.PayFiveDigit);
        $tr.find('input[name="Percentage"]').val(w.Percentage);
    });

    $.each(VoteLotteryWaters, function (i, v) {
        if (isNew == true || v.IsUsed == false) {
            $('#per_td_' + v.LotteryTimeId).find('.IsUseLottery').hide();
        } else {
            $('#per_td_' + v.LotteryTimeId).find('.IsUseLottery').show();
        }
    });
}

function CreateDataClient(){

    var mainList = $("#waitingShowInfo");

    

    var s_id = mainList.find('select[name="child_id"]').val();

    var moneyR = mainList.find('input[name="moneyR"]').val();
    var moneyD = mainList.find('input[name="moneyD"]').val();
    var s_type = mainList.find('select[name="s_type"]').val();
    var date_in = mainList.find('input[name="date_in"]').val();
    

    if(validateForm('#waitingShowInfo') == 0){
        $.ajax({
            type: "GET",
            url: homeURL+'/mobile/transctionstaff/saveTransction',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { s_id: s_id, moneyR: moneyR, moneyD: moneyD, s_type:s_type, date_in:date_in},
            success: function (data) {

                if(data.status == 'success'){
                    GetList();
                    ClearForm('#waitingShowInfo');

                    $("#update_info_user").hide();
                    ShowClient();

                    alert("រក្សាទុកបានសំរេច​!!!");
                }else{
                    alert(data.msg);
                }
                
            },
            error: function (data) { }

        });
    }

    
        
}

function SaveDataClient(){
    var mainList = $("#waitingShowInfo");
    var idData = mainList.find('input[name="idhide"]').val();
    var s_id = mainList.find('select[name="child_id"]').val();

    var moneyR = mainList.find('input[name="moneyR"]').val();
    var moneyD = mainList.find('input[name="moneyD"]').val();
    var s_type = mainList.find('select[name="s_type"]').val();
    var date_in = mainList.find('input[name="date_in"]').val();

    if(validateForm('#waitingShowInfo') == 0){
        $.ajax({
        type: "GET",
        url: homeURL+'/mobile/transctionstaff/updateData/id/update',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: {  idData:idData, s_id: s_id, moneyR: moneyR, moneyD: moneyD, s_type:s_type, date_in:date_in},
        success: function (data) {

            if(data.status == 'success'){
                GetList();
                ClearForm('#waitingShowInfo');

                $("#update_info_user").hide();
                ShowClient();

                alert("រក្សាទុកបានសំរេច​!!!");
            }else{
                alert(data.msg);
            }
            
        },
        error: function (data) { }

    });
    }
    
    
}

function SaveSellPercent() {
    var VoteLotteryWaters = new Array();
    var isUseDollar = $('#sell_percent').find('input[name="isUseDollar"]').prop('checked');
    var isPrintKatTek = $('#sell_percent').find('input[name="isPrintKatTek"]').prop('checked');
    var trs = $('#sell_percent').find('tr.txt');
    $.each(trs, function (i, tr) {
        var water = {
            LotteryTimeId: $(tr).attr('time-id'),
            TwoDigit: $(tr).find('input[name="sell2"]').val(),
            ThreeDigit: $(tr).find('input[name="sell3"]').val(),
        }
        VoteLotteryWaters.push(water);
    })
    // alert("abd");

    $.ajax({
        type: "GET",
        url: homeURL+'/mobile/me/savewater',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { voteLotteryWaters: VoteLotteryWaters, isUseDollar: isUseDollar, isPrintKatTek: isPrintKatTek },
        success: function (data) {
            console.log(data);
            alert("រក្សាទុកបានសំរេច​!!!");
        },
        error: function (data) { }

    });
}

function ChangeUseDollar(id) {
    $.ajax({
        type: "POST",
        url: '/Client/ChangeUseDollar/',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ userId: id }),
        success: function (data) {
            console.log(data);
            if (AjaxError(data) == false) {

            }
        },
        error: function (data) { }

    });
}

function ChangeCanCreateChild(id) {
        $.ajax({
            type: "POST",
            url: '/Client/ChangeCanCreateChild/',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userId: id }),
            success: function (data) {
                console.log(data);
                if (AjaxError(data) == false) {

                }
            },
            error: function (data) { }

        });
    
}

function ChangeLimitMoney(id) {
    $.ajax({
        type: "POST",
        url: '/Client/ChangeLimitMoney/',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ userId: id }),
        success: function (data) {
            console.log(data);
            if (AjaxError(data) == false) {

            }
        },
        error: function (data) { }

    });
}



// function IsUseLotteryTime(timeId) { 
//     var userId = $("#frm_user").find('input[name="userId"]').val();
//     var isUsed = $('#per_td_' + timeId).find('input[name="IsUseLottery"]').is(':checked');
//     $.ajax({
//         type: "POST",
//         url: '/Client/IsUserLotteryTime/',
//         contentType: "application/json; charset=utf-8",
//         dataType: "json",
//         data: JSON.stringify({ userId: userId, lotteryTimeId: timeId, isUsed: isUsed }),
//         success: function (data) { 
//         },
//         error: function (data) { }

//     });
// }


// function xstooltip_show(tooltipId, parentId, posX, posY) {
//     it = document.getElementById(tooltipId);
//     it.style.top = posY + 'px';
//     it.style.left = posX + 'px';
//     it.style.visibility = 'visible';
//     it.style.display = 'block';
// }

// function AjaxError(data) {
//     let isError = data.IsError;
//     if (isError == true) {
//         switch (data.ErrorCode) {
//             case 100:
//                 window.location = '/Account/Logout/';
//                 break;
//             case 101:
//                 alert('មានគេចូលគណនីរបស់អ្នក។ ចូលប្រើម្តងទៀត');
//                 window.location = '/Account/Logout/';
//                 break;
//             default:
//                 alert(data.ErrorMessage);
//                 return;
//                 break;
//         }
//     }

//     return isError;
// }

// function KeyValidate(evt) {
//     var theEvent = evt || window.event;
//     var key = theEvent.keyCode || theEvent.which;
//     key = String.fromCharCode(key);
//     var regex = /[0-9]|\./;
//     if (!regex.test(key)) {
//         theEvent.returnValue = false;
//         if (theEvent.preventDefault) theEvent.preventDefault();
//     }
// }

// function GetSCode(sc) {

//     if (sc != undefined && sc.length > 3) {
//         return '..' + sc.substr(sc.length - 4);
//     }

//     return sc;
// }

function ShowClient() {
    $('#list_user').show();
    $("#list_menu").show();
    $('#sell_percent').hide();
    $('#update_info_user').hide();
    // $("#voteWater").removeClass('active');
    // $("#ShowClientList").addClass('active');
}
// function ShowVoteWater() {
//     $('#td_users').hide();
//     $('#update_info_user').hide();
//     $('#sell_percent').show();
//     $("#voteWater").addClass('active');
//     $("#ShowClientList").removeClass('active');
// }

$(document).ready(function() {
    var t = new Date,
        n;
    t.setDate(t.getDate() - 10);
    n = new Date;
    n.setDate(n.getDate());
    $("#fromDate").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", t);

    $("#toDate").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", n);

    $("#date_in").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", n);

    

    $("#clientId").on("change", function() {
        GetList();
        $("#isPayment").prop("checked", !1)
    });
    $("#toDate").on("change", function() {
        GetList();
    });
    $("#fromDate").on("change", function() {
        GetList();
    });
    $("#isPayment").on("change", function() {
        $("tr.no-transac").toggle()
    });
    $("#clientId").change();
});

// $("body").on('click', '.btn-show-child', function () {
//     var tr = $(this).closest('tr'); 
//     if (tr.attr('ready') != '1') {
//         var userId = tr.attr('data-id');
//         $.ajax({
//             type: "GET",
//             url: '/Client/GetAllClients/',
//             contentType: "application/json; charset=utf-8",
//             dataType: "json",
//             data: { userId: userId },
//             success: function (data) {
//                 if (data.Clients!=null && data.Clients.length > 0) {
//                     $.each(data.Clients, function (i, v) {
//                         Clients.push(v);
//                     })
//                     console.log(Clients);
//                     var html = '<tr><td colspan="14" class="td-child"><table width="100%">';
//                     html += '<thead align="center">';
//                     html += '<tr align = "center" >';
//                     html += '<th class="classtd1">ឈ្មោះហៅ</th>';
//                     html += '<th class="classtd1">ឈ្មោះពិត</th>';
//                     html += '<th class="classtd1">លេខទូរស័ព្ទ</th>'; 
//                     html += '<th class="classtd1">កូន</th>';
//                     html += '<th class="classtd1">គុណទឹក</th>';
//                     html += '<th class="classtd1">កំហិតលុយ</th>';
//                     html += '<th class="classtd1">PWD</th>';
//                     html += '<th class="classtd1">មាន $</th>';
//                     html += ' <th class="classtd1">បង្កើតកូន</th>';
//                     html += '<th class="classtd1">ផ្អាក</th>';
//                     html += '<th class="classtd1">នៅលក់</th>';
//                     html += '</tr >';
//                     html += '</thead >';
//                     html += TableChilds(data.Clients);
//                     html += '<table></td></tr>';
//                     tr.after(html);
//                 }
//             },
//             error: function (data) { },
//         })
//     } else {
//         var n = tr.next();
//         if (n.attr('data-id') == undefined){
//             if (n.css('display') == 'none') {
//                 n.show();
//             } else {
//                 n.hide();
//             }
//         }
//     }
//     tr.attr('ready','1');
// });