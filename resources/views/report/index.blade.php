@extends('master')
<?php
use App\Http\Controllers\SaleController;

// $lottery_pay = DB::('tbl_staff_charge')->where('')
?>
@section('title')
<title>របាយការណ៏</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			/*background-color: #dff3f5;*/
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 16px;
		}
		table.summary_result_per_day td{
			font-size: 14px;
			font-weight: bold;
		}
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 4px 2px !important;
		}

		.totalByGroup{
			/*width: 100%;*/
			/*background: #;*/
			text-align: center;
			font-size: 13px;
			font-weight: bold;
			color: #FF9033;
			border: 1px #FF9033 solid; 
		}

		.bee_highlight.background th{
			background: #e8e8e8;
		}
		.hidetableTD{
			display:none !important;
		}

		.fc-border-separate thead tr, .table thead tr{
			background-image:none !important;
			background: #5698d2;
		}
		.table-bordered>thead>tr>th{
			border: 1px solid #8c9dad;
		}


	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'reportFilter', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_page))
							 <?php $page_filter = $var_page; ?>
							 <?php $attr_page = $var_page; ?>
						 @else
							 <?php $page_filter = null; ?>
							 <?php $attr_page = 0; ?>
						 @endif

						 @if(isset($var_sheet))
							 <?php $sheet_filter = $var_sheet; ?>
							 <?php $attr_sheet = $var_page; ?>
						 @else
							 <?php $sheet_filter = null; ?>
							 <?php $attr_sheet = 0; ?>
						 @endif

						 @if(isset($var_ownner))
							 <?php $ownner_filter = $var_ownner; ?>
						 @else
							 <?php $ownner_filter = null; ?>
						 @endif
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

							 <section class="col col-1">
								 <label class="select">
									 {{ Form::select('type_lottery', ([
                                        '' => trans('result.chooseTypeLottery') ]+$type_lottery),$var_type_lottery,['class' => 'required ','id'=>'type_lottery','sms'=> trans('result.pleaseChooseTypeLottery') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('staff', ([
                                        '' => trans('result.chooseStaff') ]+$staff),$staff_filter,['class' => 'required ','id'=>'staff','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('sheet', ([
                                        '' => trans('result.chooseShift') ]+$sheets),$sheet_filter,['class' => ' ','id'=>'sheet','sms'=> trans('result.pleaseChooseShift') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('ownner', ([
                                        '' => 'ម្ចាស់ក្រដាស់' ]+$ownner),$ownner_filter,['class' => ' ','id'=>'ownner','sms'=> 'ម្ចាស់ក្រដាស់' ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-1">
								 <label class="select">
									 {{ Form::select('page', ([
                                        '' => trans('result.choosePage') ]+$pages),$page_filter,['class' => ' ','id'=>'pages','sms'=> trans('result.pleaseChoosePage') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 

							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}
										
					 @if(isset($report))

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 <?php if($staff_boss==1){ ?>
							 .paperStyle{
								 display: none;
							 }
							 .paperStyle:last-child{
								 display: block !important;
							 }
							 .staff_name{
								 display: none;
							 }
							 <?php }?>
							 
						 </style>



						 <div class="widget-body no-padding control_width">

							 <div class="">
								 <div class="">
									 <?php
									 	$page_id = 0;

									 	 $sum_total_onedigit_r = 0;
										 $sum_total_onedigit_s = 0;

										 $sum_total_twodigit_r = 0;
										 $sum_total_twodigit_s = 0;
										 $sum_total_threedigit_r = 0;
										 $sum_total_threedigit_s = 0;
										 $controlTotal = count($report)-1;

										 $sum_price_right_twodigit_r = 0;
										 $sum_price_right_twodigit_s = 0;
										 $sum_price_right_threedigit_r = 0;
										 $sum_price_right_threedigit_s = 0;

							 			
									 ?>
									 @foreach($report as $key => $rowlist)

										 @if($page_id != $rowlist->p_id)

												 {{--#display total amount by page--}}
											 	@if($key > 0)
											 		<div class="col-md-4 display_result">
														 អ៊ួរ (សរុប) :
														 <b><span class="val_r">{{number_format($sum_total_twodigit_r+$sum_total_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_twodigit_s+$sum_total_threedigit_s,2)}} $</span></b>
													 </div>
													 <div class="col-md-4 display_result">
														 {{trans('label.total_price_number_2_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_twodigit_s,2)}} $</span></b>
													 </div>
													 <div class="col-md-4 display_result">
														 {{trans('label.total_price_number_3_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_threedigit_s,2)}} $</span></b>
													 </div>
													 

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_2_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_twodigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_3_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_threedigit_s,2)}} $</span></b>
													 </div>



													<input type="hidden" id="page_{{$key}}" class="report_total"
														   id_staff = "{{$rowlist->s_id}}"
														   time="{{$rowlist->pav_value}}"
														   staff="{{$rowlist->s_name}}"
														   page="{{$rowlist->p_number}}" 
														   page_id="{{$rowlist->p_id}}"

														   total2digitR="{{$sum_total_twodigit_r}}"
														   total2digitS="{{$sum_total_twodigit_s}}"
														   total3digitR="{{$sum_total_threedigit_r}}"
														   total3digitS="{{$sum_total_threedigit_s}}"
														   total2digitRright="{{$sum_price_right_twodigit_r}}"
														   total2digitSright="{{$sum_price_right_twodigit_s}}"
														   total3digitRright="{{$sum_price_right_threedigit_r}}"
														   total3digitSright="{{$sum_price_right_threedigit_s}}" 

														   total1digitRright="{{$sum_price_right_onedigit_r}}"
														   total1digitSright="{{$sum_price_right_onedigit_s}}"
														   >

												 @endif

												 <?php
												 $sum_total_twodigit_r = 0;
												 $sum_total_twodigit_s = 0;
												 $sum_total_threedigit_r = 0;
												 $sum_total_threedigit_s = 0;

												 $sum_price_right_onedigit_r = 0;
												 $sum_price_right_onedigit_s = 0;
												 $sum_price_right_twodigit_r = 0;
												 $sum_price_right_twodigit_s = 0;
												 $sum_price_right_threedigit_r = 0;
												 $sum_price_right_threedigit_s = 0;
												 ?>


												 </div>
							 				</div>
							 					{{--<button class="btn_print" attr_id="print_page_{{$key}}" attr_date="{{$var_dateStart}}" attr_staff="{{$var_staff}}" attr_sheet="{{$attr_sheet}}" attr_page="{{$attr_page}}">{{trans('label.download_this')}}</button>--}}

											 <div id="print_page_{{$key}}" class="paperStyle" >

													 <div class="col-md-3">
														 {{trans('label.staff_name')}} :
														 <b>{{$rowlist->s_name}} {{$rowlist->p_id}}</b>
													 </div>

													 <div class="col-md-2">
														 {{trans('label.date')}} :
														 <b>{{$rowlist->p_date}}</b>
													 </div>

													 <div class="col-md-2">
														 {{trans('label.number_paper')}} :
														 <b>{{$rowlist->p_number}}</b>
													 </div>

													 <div class="col-md-2">
														 {{trans('label.time')}} :
														 <b>
														 	<?php 
														 		$find = array("0","1","2","3","4","5","8:5"," 8:");
																$replace = array("");
																echo str_replace($find,$replace,$rowlist->pav_value);
														 	?>
														 </b>
													 </div>
													 <div class="col-md-2">
														 កែប្រែ :
														 <a href="{{URL::to('/')}}/sale/{{$rowlist->p_id}}/edit" target="_blank">Click</a>
													 </div>

												 <?php 
												 		$find = array("0","1","2","3","4","5","8:5"," 8:");
														$replace = array("");
														$timeDisplay =  str_replace($find,$replace,$rowlist->pav_value);
												 ?>
												 <input type="hidden" id="page_info_{{$key}}" class="report_total_info"
														time="{{$timeDisplay}}"
														staff="{{$rowlist->s_name}}"
														id_staff="{{$rowlist->s_id}}"
														page="{{$rowlist->p_number}}"
														page_id="{{$rowlist->p_id}}"
														page_code="{{$rowlist->p_code}}"
														page_code_p="{{$rowlist->p_code_p}}"
														page_id="{{$rowlist->p_id}}"
														 >

												 <div class="clearfix"></div>
												 <br>
												 <div class="row boder controlColume">
											 <?php
											 	$page_id = $rowlist->p_id;
											 ?>
										 @endif

										 <div class="colume_style" id="Remove_{{$rowlist->r_id}}">
											 <?php
											 $total_twodigit_r = 0;
											 $total_threedigit_r = 0;
											 $total_twodigit_s = 0;
											 $total_threedigit_s = 0;

											 $price_right_onedigit_s = 0;
											 $price_right_onedigit_r = 0;
											 $price_right_twodigit_s = 0;
											 $price_right_twodigit_r = 0;
											 $price_right_threedigit_s = 0;
											 $price_right_threedigit_r = 0;

											 $totalRealLoop = 0;
											 $totalDollaLoop = 0;

											 $checklast = 0;
											 ?>

											 <!-- block list number lottery -->
											 <?php
											 $numberLotterys = DB::table('tbl_number_mobile')
													 ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
													 ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
													 ->where('tbl_number_mobile.r_id',$rowlist->r_id)
													 ->orderBy('tbl_number_mobile.num_sort','ASC')
													 ->orderBy('tbl_number_mobile.num_id','ASC')
													 ->get();

											 $checkGroup = DB::table('tbl_number_mobile')
											 		 ->select('tbl_number_mobile.num_sort', DB::raw('count(*) as total'))
													 ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
													 ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
													 ->where('tbl_number_mobile.r_id',$rowlist->r_id)
													 ->groupBy('tbl_number_mobile.num_sort')->get();

											$numGroup = count($checkGroup);
											// var_dump($checkGroup);

											 $storeOrder = 0;
											 $countGroupLoop = 0;
											 $loopDynamic = 0;
											 ?>
											@foreach($numberLotterys as $number)
												@php
													$loopDynamic++;
												@endphp
											 	
											 	
												@if($storeOrder != $number->num_sort)
												 	@if( ($totalRealLoop > 0 ) || ($totalDollaLoop > 0))
													 	<div class="totalByGroup">
													 		@if( $totalRealLoop > 0 && $totalDollaLoop > 0)
													 			{{number_format($totalRealLoop)}} / {{$totalDollaLoop}}$
													 		@elseif($totalRealLoop > 0 && $totalDollaLoop == 0)
													 			{{number_format($totalRealLoop)}}
													 		@else
													 			{{$totalDollaLoop}}$
													 		@endif
													 	</div>
													@endif
														 <?php

														 	$storeOrder = $number->num_sort;
														 	
														 	$totalRealLoop = 0;
												 			$totalDollaLoop = 0;

												 			$loopDynamic = 1;
												 			$countGroupLoop = $countGroupLoop + 1;
														 ?>
														 <div class="pos_style" id="pos_style_{{$number->num_sort}}">{{$number->g_name}}</div>
												@endif







											 <?php
												 $numberDisplay = '';  //display number to fromte
												 $displayCurrency = '';

												//	Resulte price by row
												 $price_result = App\Model\Report::calculate( $number->num_number,$number->num_end_number,$number->num_sym,$number->num_reverse,$number->num_price,$number->num_currency,$number->g_id,$rowlist->p_time,$rowlist->p_date,$number->num_type);
												 $val_price = explode("-", $price_result);
												 $num_right = "";
												 if($val_price[2] > 0){

													 $actived = 'actived';
													 
													 if($val_price[2]>1){
													 	$num_right = '<div class="right_num"><b>* '.$val_price[2].'</b></div>';
													 }

												 }else{
													 $actived = '';
												 }

												 if($number->num_currency == 2){
													 $currencySym = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
													 $displayCurrency = ' '.$currencySym->pav_value;
													 if($val_price[1]=='2'){
														 $total_twodigit_s = $total_twodigit_s + $val_price['0'];
														 $price_right_twodigit_s = $price_right_twodigit_s + ($number->num_price * $val_price[2]);

														 $totalDollaLoop = $totalDollaLoop + $val_price['0'];

													 }elseif($val_price[1]=='1'){
													 	$total_twodigit_s = $total_twodigit_s + $val_price['0'];
														 $price_right_onedigit_s = $price_right_onedigit_s + ($number->num_price * $val_price[2]);

														 $totalDollaLoop = $totalDollaLoop + $val_price['0'];

													 }else{
														 $total_threedigit_s = $total_threedigit_s + $val_price['0'];
														 $price_right_threedigit_s = $price_right_threedigit_s + ($number->num_price * $val_price[2]);

														 $totalDollaLoop = $totalDollaLoop + $val_price['0'];
													 }

												 }else{

													 if($val_price[1]=='2'){
														 $total_twodigit_r = $total_twodigit_r + $val_price['0'];
														 $price_right_twodigit_r = $price_right_twodigit_r + ($number->num_price * $val_price[2]);

														 $totalRealLoop = $totalRealLoop + $val_price['0'];

													 }elseif($val_price[1]=='1'){

													 	$total_twodigit_r = $total_twodigit_r + $val_price['0'];
														 $price_right_onedigit_r = $price_right_onedigit_r + ($number->num_price * $val_price[2]);
														 $totalRealLoop = $totalRealLoop + $val_price['0'];

													 }else{
														 $total_threedigit_r = $total_threedigit_r + $val_price['0'];
														 $price_right_threedigit_r = $price_right_threedigit_r + ($number->num_price * $val_price[2]);

														 $totalRealLoop = $totalRealLoop + $val_price['0'];
													 }


												 }



												 if($number->num_sym == 7){ //check sym is -
													 $numberDisplay .='
													<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

														<div class="number_lot lot_int">'.$number->num_number.'</div>';
													 if($number->num_reverse == 1){
														 $numberDisplay .= '<div class="symbol_lot lot_int">x</div>';
													 }else{
														 $numberDisplay .= '<div class="symbol_lot lot_int">-</div>';
													 }

													 $numberDisplay .= '
														<div class="amount_lot lot_int">'.round($number->num_price,2).$displayCurrency.'</div>
														<div class="clear"></div>
														'.$num_right.'
													</div>
													';
												 }else if($number->num_sym == 8){

													 if($number->num_reverse == 1){
														 $end_number_new = '';
														 if($number->num_end_number == ''){
															 $end_number_new = substr($number->num_number, 0, -1).'9';
														 }else{
															 $end_number_new = $number->num_end_number;
														 }
														 $numberDisplay .='
														<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

															<div class="number_lot lot_int">'.$number->num_number.'</div>
															<div class="symbol_lot lot_int">x</div>
															<div class="clear"></div>
															<div class="display_total_number">'.SaleController::calculate_number($number->num_number,$end_number_new,1).'</div>
															<div class="number_lot lot_int clear_margin">
																|
																<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
															</div>
															<div class="clear"></div>
															<div class="number_lot lot_int">'.$end_number_new.'</div>
															<div class="symbol_lot lot_int">x</div>
															<div class="clear"></div>
															'.$num_right.'
														</div>
														';
													 }else{
														 $check = substr($number->num_number, -1);
														 if($check == '0' && $number->num_end_number == ''){
															 $numberDisplay .='
																<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																	<div class="number_lot lot_int">'.$number->num_number.'</div>
																	<div class="symbol_lot lot_int"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class="amount_lot lot_int">'.round($number->num_price,2).$displayCurrency.'</div>
																	<div class="clear"></div>
																	'.$num_right.'
																</div>
																';
														 }else{
															 $end_number_new = '';
															 if($number->num_end_number == ''){
																 $end_number_new = substr($number->num_number, 0, -1).'9';
															 }else{
																 $end_number_new = $number->num_end_number;
															 }
															 $numberDisplay .='
															<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																<div class="number_lot lot_int">'.$number->num_number.'</div>
																<div class="clear"></div>
																<div class="display_total_number">'.SaleController::calculate_number($number->num_number,$end_number_new,0).'</div>
																<div class="number_lot lot_int clear_margin">
																	|
																	<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
																</div>
																<div class="clear"></div>
																<div class="number_lot lot_int">'.$end_number_new.'</div>
																<div class="clear"></div>
																'.$num_right.'
															</div>
															';
														 }

													 }
												}else if($number->num_sym == 10){ //check sym is alot digit to 3 digit
									                $numberDisplay .='
									                <div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">
									                    
									                    <div class="number_lot lot_int newdigitalot">'.$number->num_number.'</div>';
									                    if($number->num_reverse == 1){
									                        $numberDisplay .= '<div class="symbol_lot lot_int ">x</div>';
									                    }else{
									                        $numberDisplay .= '<div class="symbol_lot lot_int ">-</div>';
									                    }
									                    
									                $numberDisplay .= '
									                    <div class="amount_lot lot_int newdigitalot_price">'.round($number->num_price,2).$displayCurrency.'</div>
									                    <div class="clear"></div>
									                    '.$num_right.'
									                </div>
									                '; 
									            }else if($number->num_sym == 11){ //check sym is alot digit to 3 digit
									            	$numberDisplay .='
									                <div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">';
									                $explotData = explode("/", $number->num_number);
									                $leftData = '';
									                $centerData = '';
									                $rightData = '';
									                if(isset($explotData[0])){
									                	$explotData_sub = explode(",", $explotData[0]);
									                	foreach ($explotData_sub as $key0 => $value_sub) {
									                		if($value_sub != ''){
									                			$leftData .='<li>'.$value_sub.'</li>';
									                		}
									                	}
									                }
								                	
									                if(isset($explotData[1])){
									                	$explotData_sub = explode(",", $explotData[1]);
									                	foreach ($explotData_sub as $key1 => $value_sub) {
									                		if($value_sub != ''){
									                			$centerData .='<li>'.$value_sub.'</li>';
									                		}
									                		
									                	}
									                }

									                if(isset($explotData[2])){
									                	$explotData_sub = explode(",", $explotData[2]);
									                	foreach ($explotData_sub as $key2 => $value_sub) {
									                		if($value_sub != ''){
									                			$rightData .='<li>'.$value_sub.'</li>';
									                		}
									                	}
									                }

									                if($leftData == ''){
												        $leftData = $centerData;
												        $centerData = $rightData;
												        $rightData = '';
												    }else if($centerData == ''){
												        $centerData = $rightData;
												        $rightData = '';
												    }

												    if($number->num_type == 1){
												    	$labelTypeNum = 'ដកគូ';
												    }else if($number->num_type == 2){
												    	$labelTypeNum = 'ដកសេស';
												    }else{
												    	$labelTypeNum = '';
												    }

									                $numberDisplay .= '
									                	<div class="row_lottery_list_new">
										                    <div class="txt_left">
										                        <ul>
										                        	'.$leftData.'
										                        </ul>
										                    </div>
										                    <div class="txt_center">
										                        <ul>
										                        	'.$centerData.'
										                        </ul>
										                    </div>
										                    <div class="txt_right">
										                       <ul>
										                        	'.$rightData.'
										                        </ul>
										                    </div>
										                    <div class="clear"></div>
										                    <div class="labelnumtype">'.$labelTypeNum.'</div>
										                </div>
									                ';
									                   
									                    
									                $numberDisplay .= '
									                    <div class="number_lot lot_int newdigitalot_price">'.SaleController::calculate_totalnumber_new($number->num_number,$number->num_type).'</div>
									                    <div class="symbol_lot lot_int ">x</div>
									                    <div class="amount_lot lot_int newdigitalot_price">'.round($number->num_price,2).$displayCurrency.'</div>
									                    <div class="clear"></div>
									                    '.$num_right.'
									                </div>
									                '; 

												 }else{
													 if($number->num_reverse == 1){
														 $numberDisplay .='
															<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																<div class="number_lot lot_int">'.$number->num_number.'</div>
																<div class="symbol_lot lot_int">x</div>
																<div class="clear"></div>
																<div class="number_lot lot_int clear_margin">
																	|
																	<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
																</div>
																<div class="clear"></div>
																<div class="number_lot lot_int">'.$number->num_end_number.'</div>
																<div class="symbol_lot lot_int">x</div>
																<div class="clear"></div>
																'.$num_right.'
															</div>
															';
													 }else{
														 $numberDisplay .='
															<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																<div class="number_lot lot_int">'.$number->num_number.'</div>
																<div class="clear"></div>
																<div class="number_lot lot_int clear_margin">
																	|
																	<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
																</div>
																<div class="clear"></div>
																<div class="number_lot lot_int">'.$number->num_end_number.'</div>
																<div class="clear"></div>
																'.$num_right.'
															</div>
															';
													 }
												 }
												 echo $numberDisplay;
												 $numGroupDynamic = $checkGroup[$countGroupLoop-1]->total;
												 // var_dump($numGroupDynamic);
												 // var_dump($loopDynamic);
												 ?>

												@if( ($countGroupLoop == $numGroup) && ($loopDynamic == $numGroupDynamic) )
													 @if( ($totalRealLoop > 0 ) || ($totalDollaLoop > 0))
													 	<div class="totalByGroup">
													 		@if( $totalRealLoop > 0 && $totalDollaLoop > 0)
													 			{{number_format($totalRealLoop)}} / {{$totalDollaLoop}}$
													 		@elseif($totalRealLoop > 0 && $totalDollaLoop == 0)
													 			{{number_format($totalRealLoop)}}
													 		@else
													 			{{$totalDollaLoop}}$
													 		@endif
													 	</div>
													@endif
												@endif

												 
											 @endforeach
											 <div class="clearfix"></div>

											 <?php 
											 	$sumamount_r = $total_twodigit_r + $total_threedigit_r;
											 	$sumamount_s = $total_twodigit_s + $total_threedigit_s;
											 ?>

											 <div class="result_price_top" id="row_result_2_{{$rowlist->r_id}}">					{{number_format($total_twodigit_r)}} ៛  
											 	<span class="pull-right">{{round($total_twodigit_s,2)}} $</span>
											 </div>

											 <div class="result_price" id="row_result_3_{{$rowlist->r_id}}">{{number_format($total_threedigit_r)}} ៛  <span class="pull-right">{{round($total_threedigit_s,2)}} $</span></div>

											 <div class="result_price_main" id="row_result_3_{{$rowlist->r_id}}">{{number_format($sumamount_r)}} ៛  <span class="pull-right">{{round($sumamount_s,2)}} $</span></div>


										 </div>
										 		 <?php $sum_total_twodigit_r = $sum_total_twodigit_r + $total_twodigit_r;?>
												 <?php $sum_total_twodigit_s = $sum_total_twodigit_s + $total_twodigit_s;?>
												 <?php $sum_total_threedigit_r = $sum_total_threedigit_r + $total_threedigit_r;?>
												 <?php $sum_total_threedigit_s = $sum_total_threedigit_s + $total_threedigit_s;?>

												 <?php $sum_price_right_onedigit_r = $sum_price_right_onedigit_r + $price_right_onedigit_r;?>
												 <?php $sum_price_right_onedigit_s = $sum_price_right_onedigit_s + $price_right_onedigit_s;?>

												 <?php $sum_price_right_twodigit_r = $sum_price_right_twodigit_r + $price_right_twodigit_r;?>
												 <?php $sum_price_right_twodigit_s = $sum_price_right_twodigit_s + $price_right_twodigit_s;?>
												 <?php $sum_price_right_threedigit_r = $sum_price_right_threedigit_r + $price_right_threedigit_r;?>
												 <?php $sum_price_right_threedigit_s = $sum_price_right_threedigit_s + $price_right_threedigit_s;?>



												 {{--#display total amount by page--}}
												 @if($controlTotal == $key)

												 	<div class="col-md-4 display_result">
														 អ៊ួរ (សរុប) :
														 <b><span class="val_r">{{number_format($sum_total_twodigit_r+$sum_total_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_twodigit_s+$sum_total_threedigit_s,2)}} $</span></b>
													 </div>
													 <div class="col-md-4 display_result">
														 {{trans('label.total_price_number_2_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_twodigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-4 display_result">
														 {{trans('label.total_price_number_3_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_threedigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-4 display_result">
														 ចំនួន​លុយសរុប ១លេខ (ត្រូវ) :
														 <b><span class="val_r">{{number_format($sum_price_right_onedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_onedigit_s,2)}} $</span></b>
													 </div>


													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_2_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_twodigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_3_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_threedigit_s,2)}} $</span></b>
													 </div>

													 <input type="hidden" id="page_{{$key}}" class="report_total"
													 		id_staff = "{{$rowlist->s_id}}"
													 		time="{{$rowlist->pav_value}}"
														    staff="{{$rowlist->s_name}}"
														    page="{{$rowlist->p_number}}"
														    page_id="{{$rowlist->p_id}}"

															total2digitR="{{$sum_total_twodigit_r}}"
															total2digitS="{{$sum_total_twodigit_s}}"
															total3digitR="{{$sum_total_threedigit_r}}"
															total3digitS="{{$sum_total_threedigit_s}}"

															total1digitRright="{{$sum_price_right_onedigit_r}}"
															total1digitSright="{{$sum_price_right_onedigit_s}}"

															total2digitRright="{{$sum_price_right_twodigit_r}}"
															total2digitSright="{{$sum_price_right_twodigit_s}}"
															total3digitRright="{{$sum_price_right_threedigit_r}}"
															total3digitSright="{{$sum_price_right_threedigit_s}}">


												 @endif

									 @endforeach

								 </div>

							 </div>

							 <?php
							 $twodigit_charge = 0;
							 $threedigit_charge = 0;
							 

							 if($var_type_lottery >=5){
								$typeStaffLott = 1;
							 }else{
								$typeStaffLott = $var_type_lottery;
							 }

							 $getStaffCharge = DB::table('tbl_staff')
									 // ->select('stc_id','stc_three_digit_charge','stc_two_digit_charge','stc_pay_two_digit','stc_pay_there_digit')
									 ->where('s_id',$var_staff)
									 // ->where('stc_type',$typeStaffLott)
									 // ->where('stc_date','<=',$var_dateStart)
									 // ->orderBy('stc_date','DESC')
									 ->first();
							// dd($staffInfo->use_code);

							if($var_type_lottery == 1){
								if($getStaffCharge->s_two_digit_charge > 0){
									$win_two_digit = $getStaffCharge->s_two_digit_paid;
								}else{
									$win_two_digit = 90;
								}

								if($getStaffCharge->s_three_digit_charge > 0){
									$win_three_digit = $getStaffCharge->s_three_digit_paid;
								}else{
									$win_three_digit = 800;
								}

							}else{

								if($getStaffCharge->s_two_digit_kh_charge > 0){
									$win_two_digit = $getStaffCharge->s_two_digit_kh_paid;
								}else{
									$win_two_digit = 90;
								}

								if($getStaffCharge->s_three_digit_kh_charge > 0){
									$win_three_digit = $getStaffCharge->s_three_digit_kh_paid;
								}else{
									$win_three_digit = 800;
								}

							}

								 // var_dump($getStaffCharge);
							 if(isset($getStaffCharge)){

								 $twodigit_charge = $getStaffCharge->s_two_digit_charge;
								 $threedigit_charge = $getStaffCharge->s_three_digit_charge;

								 if($var_type_lottery == 2){

								 	$twodigit_charge = $getStaffCharge->s_two_digit_kh_charge;
								 	$threedigit_charge = $getStaffCharge->s_three_digit_kh_charge;

								 }
								 if($twodigit_charge == $threedigit_charge){
								 	// $hidedata = 'hidetwothree_digit';
								 	// $sumHide = '';
								 	// $colspan = 1;
								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }else{
								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }

							 }

							 ?>
							 @if($var_sheet=='' && $var_page=='' && $var_ownner=='')
							 	@if(Session::get('roleLot') >= 0)
								 <div id="html-content-holder" class="widget-body paperStyle">
								 	@if(isset($rowlist))
									 <h1 style="display:none;">{{trans('label.summary_result_per_day')}} 
									 	@if($var_type_lottery == 1)
									 		VN
									 	@elseif($var_type_lottery == 2)
											KH
									 	@else
									 		VN
									 	@endif
									 </h1><br>
									 <div class="col-md-3 staff_name">
										 {{trans('label.staff_name')}} :
										 <b>{{$rowlist->s_name}} </b>
									 </div>

									 <div class="col-md-2">
										 <b>Lottery</b>
									 </div>

									 <div class="col-md-3">
										 {{trans('label.date')}} :
										 <b><?php echo date("d-m-Y", strtotime($rowlist->p_date));?></b>
									 </div>
									 @endif
									 <br><br>
									 <table id="" class="table table-bordered summary_result_per_day" style="width:100%;" >
										 <thead>
										 <tr>
										 	 <th>
										 	 	@if($var_type_lottery == 1)
											 		VN
											 	@elseif($var_type_lottery == 2)
													KH
											 	@else
											 		VN
											 	@endif
											  </th>
											 <th colspan="3" class="pOwnerMan">
											 	ថ្ងៃទី 
										 		<b><?php echo date("d-m-Y", strtotime($rowlist->p_date));?></b>
											 </th>
											 <th colspan="{{$colspan}}">KHR</th>
											 <th class="1digit">ត្រូវ </th>
											 <th class="win2DR" >ត្រូវ </th>
											 <th class="win3DR">ត្រូវ </th>
											 <th colspan="{{$colspan}}" class="moneyAllDD">USD</th>
											 <th class="1digit">ត្រូវ</th>
											 <th class="win2DD">ត្រូវ</th>
											 <th class="win3DD">ត្រូវ</th>
										 </tr>
										 <tr>
											 <th style="width:10%;">{{trans('label.staff')}}</th>
											 <th style="width:5%;">វេន</th>
											 <th colspan="1" style="width:10%;" class="pOwner">
							 					ម្ចាស់ក្រដាស់
											 </th>
											 <th style="width:5%;">ល.រ</th>
											 <th class="{{$hidedata}}" style="width:10%;">2D</th>
											 <th class="{{$hidedata}}" style="width:10%;">3D</th>
											 <th class="{{$sumHide}}" style="width:10%;">លុយ</th>
											 <th class="1digit">1D</th>
											 <th class="win2DR" style="width:10%;">2D</th>
											 <th class="win3DR" style="width:10%;">3D</th>
											 <th class="{{$hidedata}} money2DD" style="width:10%;">2D</th>
											 <th class="{{$hidedata}} money3DD" style="width:10%;">3D</th>
											 <th class="{{$sumHide}} moneyAllDD" style="width:10%;">លុយ</th>
											 <th class="1digit">1D</th>
											 <th class="win2DD" style="width:10%;">2D</th>
											 <th class="win3DD" style="width:10%;">3D</th>
										 </tr>
										 </thead>
										 <tbody class="display_total_result" >
										 </tbody>
									 </table>
								</div>
								@endif

							 @endif
						 </div>




					 @endif



		    
		         </div>
		         <!-- end widget content -->
							
		        </div>
		        <!-- end widget div -->
				<div style="text-align: center;">
				<input type='button' id='btnPrintDiv' value='Print'  style="float:none;">
				</div>
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		$(document).off('change', '#type_lottery').on('change', '#type_lottery', function(e){
			
			var id = $(this).val();
			$("#staff").prop( "disabled", true );
			$("#sheet").prop( "disabled", true );
            if(id != ''){
            	$.ajax({
				   url: 'getstaffbylotterytype',
			       type: 'GET',
			       data: {type:id},
			       success: function(data) {
			           if(data.status=="success"){

			           		$("#staff").html(data.msg);
			           		$("#sheet").html(data.msg1);

			           		$("#staff").prop( "disabled", false );
			           		$("#sheet").prop( "disabled", false );

			      			console.log(data.msg);

			           }else{
			           }
			        }
			     });
            }else{

            	$("#staff").html('<option value="" selected="selected">រើសបុគ្គលិក</option>');
			    $("#sheet").html('<option value="" selected="selected">ជ្រើសរើសពេល</option>');
            	$("#staff").prop( "disabled", false );
			    $("#sheet").prop( "disabled", false );
            }
			

		});


		$(document).off('submit', '#filter').on('submit', '#filter', function(e){
			if (validate_form_main('#filter') == 0) {
				return true;
			}
			return false;
		});

@if(isset($report))

		function commaSeparateNumber(val){
			while (/(\d+)(\d{3})/.test(val.toString())){
				val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
			}
			return val;
		}

		function addCommas(nStr)
		{
		    nStr += '';
		    x = nStr.split('.');
		    x1 = x[0];
		    if(x[1]){
		    	xdot = x[1].toString().substring(0, 2)
		    }else{
		    	xdot = null;
		    }
		    x2 = x.length > 1 ? '.' + xdot : '';
		    var rgx = /(\d+)(\d{3})/;
		    while (rgx.test(x1)) {
		        x1 = x1.replace(rgx, '$1' + ',' + '$2');
		    }
		    // console.log(x[1]);
		    return x1 + x2;
		}



		$(document).ready(function() {

			var oneDPay = 8;
			var total2digitr = '';
			var total2digits = '';
			var total3digitr = '';
			var total3digits = '';
			var total2digitrright = '';
			var total2digitsright = '';
			var total3digitrright = '';
			var total3digitsright = '';

			var total1digitrright = '';
			var total1digitsright = '';

			var time = '';
			var staff_list = '';
			var page = '';
			var tr = '';
			var sum_total2digitrright = 0;
			var sum_total2digitsright = 0;
			var sum_total3digitrright = 0;
			var sum_total3digitsright = 0;

			var sum_total1digitrright = 0;
			var sum_total1digitsright = 0;

			var num_total2digitrright = 0;
			var num_total2digitsright = 0;
			var num_total3digitrright = 0;
			var num_total3digitsright = 0;


			var twodigit_charge = <?php echo $twodigit_charge;?>;
			var threedigit_charge = <?php echo $threedigit_charge;?>;

			var sum_total2digitr = 0;
			var sum_total2digits = 0;
			var sum_total3digitr = 0;
			var sum_total3digits = 0;

			var data_array = new Array();
			$(".report_total").each(function(index, element) {
				
				var data_list = {};

				var id_staff = $(this).parent().parent().find('.report_total_info').attr('id_staff');

				var time_value = $(this).parent().parent().find('.report_total_info').attr('time');
				var staff_value = $(this).parent().parent().find('.report_total_info').attr('staff');
				var page_value = $(this).parent().parent().find('.report_total_info').attr('page');

				var page_code = $(this).parent().parent().find('.report_total_info').attr('page_code');

				var page_code_p = $(this).parent().parent().find('.report_total_info').attr('page_code_p');

				var page_id = $(this).parent().parent().find('.report_total_info').attr('page_id');

				total2digitr = $(this).attr('total2digitr');
				total2digits = $(this).attr('total2digits');
				total3digitr = $(this).attr('total3digitr');
				total3digits = $(this).attr('total3digits');
				total2digitrright = $(this).attr('total2digitrright');
				total2digitsright = $(this).attr('total2digitsright');
				total3digitrright = $(this).attr('total3digitrright');
				total3digitsright = $(this).attr('total3digitsright');

				total1digitrright = $(this).attr('total1digitrright');
				total1digitsright = $(this).attr('total1digitsright');

				var item = new Array();
				item.push({'total2digitr':total2digitr,
					       'total2digits':total2digits,
					       'total3digitr':total3digitr,
					       'total3digits':total3digits,
					       'total2digitrright':total2digitrright,
					       'total2digitsright':total2digitsright,
					       'total3digitrright':total3digitrright,
					       'total3digitsright':total3digitsright,
					       'total1digitrright':total1digitrright,
					       'total1digitsright':total1digitsright,
					       'time_value':time_value,
					       'staff_value':staff_value,
					   	   'page_value':page_value,
					   	   'page_id':page_id});
				

				data_list['id']= id_staff;
				data_list['item']= item;

				data_array.push(data_list);
			});

			data_array = data_array.sort(function (a, b) {
			    return a.id.localeCompare( b.id );
			});
			// console.log(data_array);

			// $.each( data_array, function( index, element ) {
			// 	total2digitr = element.item[0].total2digitr;
			// 	total2digits = element.item[0].total2digits;
			// 	total3digitr = element.item[0].total3digitr;
			// 	total3digits = element.item[0].total3digits;
			// 	total2digitrright = element.item[0].total2digitrright;
			// 	total2digitsright = element.item[0].total2digitsright;
			// 	total3digitrright = element.item[0].total3digitrright;
			// 	total3digitsright = element.item[0].total3digitsright;

			// 	time_value = element.item[0].time_value;
			// 	staff_value = element.item[0].staff_value;
			// 	page_value = element.item[0].page_value;
			// 	page_id = element.item[0].page_id;

			// 	if(total2digitr==0){
			// 		numtotal2digitr = 0;
			// 		total2digitr = '';
			// 	}else{
			// 		numtotal2digitr = total2digitr;
			// 		total2digitr = total2digitr;
			// 	}

			// 	if(total2digits==0){
			// 		numtotal2digits = 0;
			// 		total2digits = '';
			// 	}else{
			// 		numtotal2digits = total2digits;
			// 		total2digits = total2digits;
			// 	}

			// 	if(total3digitr==0){
			// 		numtotal3digitr = 0;
			// 		total3digitr = '';
			// 	}else{
			// 		numtotal3digitr = total3digitr;
			// 		total3digitr = total3digitr;
			// 	}

			// 	if(total3digits==0){
			// 		numtotal3digits = 0;
			// 		total3digits = '';
			// 	}else{
			// 		numtotal3digits = total3digits;
			// 		total3digits = total3digits;
			// 	}

			// 	if(total2digitrright==0){
			// 		num_total2digitrright = 0;
			// 		total2digitrright = '';
			// 	}else{
			// 		num_total2digitrright = total2digitrright;
			// 		total2digitrright = total2digitrright;

			// 	}
			// 	if(total2digitsright==0){
			// 		num_total2digitsright = 0;
			// 		total2digitsright = '';
			// 	}else{
			// 		num_total2digitsright = total2digitsright;
			// 		total2digitsright = total2digitsright;
			// 	}
			// 	if(total3digitrright==0){
			// 		num_total3digitrright = 0;
			// 		total3digitrright = '';
			// 	}else{
			// 		num_total3digitrright = total3digitrright;
			// 		total3digitrright = total3digitrright;
			// 	}
			// 	if(total3digitsright==0){
			// 		num_total3digitsright = 0;
			// 		total3digitsright = '';
			// 	}else{
			// 		num_total3digitsright = total3digitsright;
			// 		total3digitsright = total3digitsright;
			// 	}

			// 	sum_total2digitr = sum_total2digitr + parseInt(numtotal2digitr);
			// 	sum_total2digits = sum_total2digits + parseInt(numtotal2digits);
			// 	sum_total3digitr = sum_total3digitr + parseInt(numtotal3digitr);
			// 	sum_total3digits = sum_total3digits + parseInt(numtotal3digits);

			// 	sum_total2digitrright = sum_total2digitrright + parseInt(num_total2digitrright);
			// 	sum_total2digitsright = sum_total2digitsright + parseInt(num_total2digitsright);
			// 	sum_total3digitrright = sum_total3digitrright + parseInt(num_total3digitrright);
			// 	sum_total3digitsright = sum_total3digitsright + parseInt(num_total3digitsright);

			// 	// tr = '<tr><td id="td_time_'+index+'">'+time+'</td><td id="td_page_'+index+'">'+page+'</td><td>'+commaSeparateNumber(total2digitr)+'</td><td>'+commaSeparateNumber(total2digits)+'</td><td>'+commaSeparateNumber(total3digitr)+'</td><td>'+commaSeparateNumber(total3digits)+'</td><td class="result_right">'+commaSeparateNumber(total2digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total2digitsright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitsright)+'</td><tr>';

			// 	tr = '<tr><th id="td_staff_com_'+index+'">'+staff_value+'</th><th id="td_time_com_'+index+'">'+time_value+'</th><th id="td_page_com_'+index+'">'+page_value+'</th><td>'+total2digitr+'</td><td>'+total3digitr+'</td><td>'+total2digitrright+'</td><td>'+total3digitrright+'</td><td>'+total2digits+'</td><td>'+total3digits+'</td><td>'+total2digitsright+'</td><td>'+total3digitsright+'</td><tr>';

			// 	$('.display_total_result').append(tr);
			// });
			var total_page_sum = 0;
			var check_staff = '';
			var check_time = '';
			var row_data = '';var row_data2 = '';
			var date_new = '';
			var type_lottery = '';
			var staff_new = '';
			var total_page = $(".report_total").length;

			
			var win2DR = 0;
			var win3DR = 0;

			var dolarFooter = 0;
			var money2DD = 0;
			var money3DD = 0;
			var moneyAllDD = 0;
			var win2DD = 0;
			var win3DD = 0;
			var pOwner = 0;

			var moliCut = 1;

			$(".report_total").each(function(index, element) {
				total_page_sum = total_page_sum + 1;
				var id_staff = $(this).parent().parent().find('.report_total_info').attr('id_staff');

				var time_value = $(this).parent().parent().find('.report_total_info').attr('time');
				var staff_value = $(this).parent().parent().find('.report_total_info').attr('staff');
				var page_value = $(this).parent().parent().find('.report_total_info').attr('page');

				var page_code = $(this).parent().parent().find('.report_total_info').attr('page_code');
				var page_code_p = $(this).parent().parent().find('.report_total_info').attr('page_code_p');

				var page_id = $(this).parent().parent().find('.report_total_info').attr('page_id');
				
				total2digitr = $(this).attr('total2digitr') * moliCut;
				total2digits = $(this).attr('total2digits');
				total3digitr = $(this).attr('total3digitr') * moliCut;
				total3digits = $(this).attr('total3digits');
				total2digitrright = $(this).attr('total2digitrright') * moliCut;
				total2digitsright = $(this).attr('total2digitsright');
				total3digitrright = $(this).attr('total3digitrright') * moliCut;
				total3digitsright = $(this).attr('total3digitsright');

				total1digitrright = $(this).attr('total1digitrright');
				total1digitsright = $(this).attr('total1digitsright');

				if(total2digitr==0){
					numtotal2digitr = 0;
					total2digitr = '';
				}else{
					numtotal2digitr = total2digitr;
					total2digitr = total2digitr;
				}

				if(total2digits==0){
					numtotal2digits = 0;
					total2digits = '';
				}else{
					numtotal2digits = total2digits;
					total2digits = total2digits;
				}

				if(total3digitr==0){
					numtotal3digitr = 0;
					total3digitr = '';
				}else{
					numtotal3digitr = total3digitr;
					total3digitr = total3digitr;
				}

				var totalsum = parseInt(numtotal2digitr) + parseInt(numtotal3digitr);
				if(totalsum==0){
					totalsum = '';
				}

				if(total3digits==0){
					numtotal3digits = 0;
					total3digits = '';
				}else{
					numtotal3digits = total3digits;
					total3digits = total3digits;
				}

				if(total2digitrright==0){
					num_total2digitrright = 0;
					total2digitrright = '';
				}else{
					num_total2digitrright = total2digitrright;
					total2digitrright = total2digitrright;

				}
				if(total2digitsright==0){
					num_total2digitsright = 0;
					total2digitsright = '';
				}else{
					num_total2digitsright = total2digitsright;
					total2digitsright = total2digitsright;
				}
				if(total3digitrright==0){
					num_total3digitrright = 0;
					total3digitrright = '';
				}else{
					num_total3digitrright = total3digitrright;
					total3digitrright = total3digitrright;
				}
				if(total3digitsright==0){
					num_total3digitsright = 0;
					total3digitsright = '';
				}else{
					num_total3digitsright = total3digitsright;
					total3digitsright = total3digitsright;
				}

				if(total1digitrright==0){
					num_total1digitrright = 0;
					total1digitrright = '';
				}else{
					num_total1digitrright = total1digitrright;
					total1digitrright = total1digitrright;
				}
				if(total1digitsright==0){
					num_total1digitsright = 0;
					total1digitsright = '';
				}else{
					num_total1digitsright = total1digitsright;
					total1digitsright = total1digitsright;
				}

				var totalsum_dolla = parseFloat(numtotal2digits) + parseFloat(numtotal3digits);
				if(totalsum_dolla==0){
					totalsum_dolla = '';
				}

				sum_total2digitr = sum_total2digitr + parseInt(numtotal2digitr);
				sum_total2digits = sum_total2digits + parseFloat(numtotal2digits);
				sum_total3digitr = sum_total3digitr + parseInt(numtotal3digitr);
				sum_total3digits = sum_total3digits + parseFloat(numtotal3digits);

				sum_total2digitrright = sum_total2digitrright + parseInt(num_total2digitrright);
				sum_total2digitsright = sum_total2digitsright + parseFloat(num_total2digitsright);
				sum_total3digitrright = sum_total3digitrright + parseInt(num_total3digitrright);
				sum_total3digitsright = sum_total3digitsright + parseFloat(num_total3digitsright);

				sum_total1digitrright = sum_total1digitrright + parseInt(num_total1digitrright);
				sum_total1digitsright = sum_total1digitsright + parseFloat(num_total1digitsright);

				// tr = '<tr><td id="td_time_'+index+'">'+time+'</td><td id="td_page_'+index+'">'+page+'</td><td>'+commaSeparateNumber(total2digitr)+'</td><td>'+commaSeparateNumber(total2digits)+'</td><td>'+commaSeparateNumber(total3digitr)+'</td><td>'+commaSeparateNumber(total3digits)+'</td><td class="result_right">'+commaSeparateNumber(total2digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total2digitsright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitsright)+'</td><tr>';
					
				@if($var_dateStart!='' &&  $var_staff!='' )
					var date_note = '<?php echo date("d-m-Y", strtotime($rowlist->p_date));?>';
					var date_db = '<?php echo date("Y-m-d", strtotime($rowlist->p_date));?>';
					var type_lottery = '<?php echo $var_type_lottery;?>';
					// console.log(time_value);
					// console.log(page_value);
					// return false;
					// console.log(total_page_sum);
					var sheet_id = '';
				@if($var_type_lottery == 1 || $var_type_lottery == 5 || $var_type_lottery == 7)

					if(time_value == 'ថ្ងៃ'){
						var new_time = '1';
						sheet_id = 5;
					}else if(time_value == 'ល្ងាច'){
						var new_time = '2';
						sheet_id = 5;
					}else if(time_value == 'យប់'){
						var new_time = '3';
						sheet_id = 6;
					}else if(time_value == 'ព្រឹក'){
						var new_time = '0';
						sheet_id = 5;
					}else{
						var new_time = '';
						sheet_id = 5;
					}

				@else
					console.log(time_value,'time_value');
					var time_value = time_value.replace(":", "").replace("5", "").replace("6", "").replace(" ", "").replace("7:", "").replace(" 7: ", "").replace("7", "");
					if(time_value == 'ថ្ងៃ'){
						var new_time = '2';
						sheet_id = 12;
					}else if(time_value == 'រសៀល'){
						var new_time = '3';
						sheet_id = 13;
					}else if(time_value == 'ល្ងាច'){
						var new_time = '4';
						sheet_id = 24;
					}else if(time_value == 'យប់'){
						var new_time = '5';
						sheet_id = 14;
					}else if(time_value == 'ព្រឹក'){
						var new_time = '1';
						sheet_id = 17;
					}else if(time_value == 'ព្រលឹម'){
						var new_time = '0';
						sheet_id = 36;
					}else{
						var new_time = '';
						sheet_id = 36;
					}
				@endif

					row_data = row_data + new_time+time_value+","+page_value+","+total2digitr+","+total3digitr+","+totalsum+","+total2digitrright+","+total3digitrright+","+total2digits+","+total3digits+","+totalsum_dolla+","+total2digitsright+","+total3digitsright+","+total_page_sum+","+page_code+","+page_id+","+page_code_p+","+total1digitrright+","+total1digitsright+";";

					row_data2 = row_data2 +sheet_id+","+page_value+","+total2digitr+","+total3digitr+","+totalsum+","+total2digitrright+","+total3digitrright+","+total2digits+","+total3digits+","+totalsum_dolla+","+total2digitsright+","+total3digitsright+","+total_page_sum+","+page_code+","+page_id+","+page_code_p+","+total1digitrright+","+total1digitsright+";";

					date_new = date_note;
					type_lottery = type_lottery;
					staff_new = staff_value;
					id_staff = id_staff;
					/* page_code_data = page_code; */
					if(total_page_sum == total_page){
						$.ajaxSetup({
					        headers: {
					            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					        }
					    });
						$.ajax({
							// url: '/report/summary_lottery_page',
							url:"{{ route('summary_lottery_page.post') }}",
							type: 'POST',
							data: {row_data:row_data,date_new:date_new,type_lottery:type_lottery,staff_new:staff_new,id_staff:id_staff},
							success: function(data) {
								if(data.status=="success"){
									console.log(data.msg, 'summary_lottery_page');
								}else{
		//							console.log(data.msg);
								}
							}
						});
						$.ajax({
							// url: 'summary_lottery_page_by_page',
							url:"{{ route('summary_lottery_page_by_page.post') }}",
							type: 'POST',
							data: {row_data:row_data2,date_new:date_db,type_lottery:type_lottery,id_staff:id_staff},
							success: function(data) {
								if(data.status=="success"){
									console.log(data.msg,'summary_lottery_page_by_page');
								}else{
		//							console.log(data.msg);
								}
							}
						});
					}
					

				@endif


				if(check_staff != staff_value){
					check_staff = staff_value;
					var new_staff = staff_value;
					check_time = '';
				}else{
					var new_staff = '';
					
				}

				if(check_time != time_value){
					check_time = time_value;
					var new_time = time_value;
				}else{
					
						var new_time = '';
					
				}

				/* check condition col */

				if(total2digitrright > 0){
					win2DR = win2DR + 1;
				}
				if(total3digitrright > 0){
					win3DR = win3DR + 1;
				}

				if(total2digits > 0){
					money2DD = money2DD + 1;
				}
				if(total3digits > 0){
					money3DD = money3DD + 1;
				}
				if(totalsum_dolla > 0){
					moneyAllDD = moneyAllDD + 1;
				}

				if(total2digitsright > 0){
					win2DD = win2DD + 1;
				}
				if(total3digitsright > 0){
					win3DD = win3DD + 1;
				}
				if(page_code != ''){
					pOwner = pOwner + 1;
				}

				var newTimeStr = new_time.replace(":", "").replace("5", "");
				// tr = '<tr><th id="td_staff_'+index+'">'+staff_value+'</th><th id="td_time_'+index+'">'+time_value+'</th><th id="td_page_'+index+'">'+page_value+'</th><td​​ class="{{$hidedata}}">'+total2digitr+'</td><td class="{{$hidedata}}">'+total3digitr+'</td><td class="{{$sumHide}}">'+totalsum+'</td><td>'+total2digitrright+'</td><td>'+total3digitrright+'</td><td class="{{$hidedata}}">'+total2digits+'</td><td class="{{$hidedata}}">'+total3digits+'</td><td class="{{$sumHide}}">'+totalsum_dolla+'</td><td>'+total2digitsright+'</td><td>'+total3digitsright+'</td><tr>';
				tr = '<tr><th id="td_staff_'+index+'">'+new_staff+'</th><th id="td_time_'+index+'">'+newTimeStr+'</th><th id="td_staff_code_'+index+'" class="pOwner">'+page_code+'</th><th id="td_page_'+index+'">'+page_value+' <span class="numSumP">('+total_page_sum+')</span></th><td class="{{$hidedata}}">'+total2digitr+'</td><td class="{{$hidedata}}">'+total3digitr+'</td><td class="{{$sumHide}}">'+totalsum+'</td><td class="1digit">'+addCommas(total1digitrright)+'</td><td class="win2DR">'+total2digitrright+'</td><td class="win3DR">'+total3digitrright+'</td><td class="{{$hidedata}} money2DD">'+total2digits+'</td><td class="{{$hidedata}} money3DD">'+total3digits+'</td><td class="{{$sumHide}} moneyAllDD">'+totalsum_dolla+'</td><td class="1digit">'+addCommas(total1digitsright)+'</td><td class="win2DD">'+total2digitsright+'</td><td class="win3DD">'+total3digitsright+'</td></tr>';
				console.log(newTimeStr,'time');
				$('.display_total_result').append(tr);

				


			});

			

			var total_income_expense_riel = 0;
			var total_income_expense_dollar = 0;
			<?php 




			if($var_type_lottery == 1){
			?>
				var income_riel = parseInt(Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) + parseInt(sum_total1digitrright * oneDPay));
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>) + parseInt(sum_total1digitsright * oneDPay));
			<?php }else{?>
				var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
			<?php }?>
			var total_income_expense_riel = income_riel - expense_riel;
			var check_total = total_income_expense_riel.toString().substr(-2);
            // console.log(total_income_expense_riel);
            // console.log(check_total);
            var staff_id = "<?php echo $staff_filter;?>";

        if(staff_id == 492 || staff_id == 491 || staff_id == 490 || staff_id == 363 || staff_id == 266 || staff_id == 274 || staff_id == 276 || staff_id == 275 || staff_id == 271 || staff_id == 316 || staff_id == 315 || staff_id == 269 || staff_id == 277 || staff_id == 307 || staff_id == 309 || staff_id == 534 || staff_id == 564 || staff_id == 564 || staff_id == 597 || staff_id == 599 || staff_id == 604 || staff_id == 603 || staff_id == 602 || staff_id == 605 || staff_id == 606 || staff_id == 611 || staff_id == 609 || staff_id == 618 || staff_id == 619 || staff_id == 624 || staff_id == 623 || staff_id == 630 || staff_id == 633 || staff_id == 634 || staff_id == 658 || staff_id == 659 || staff_id == 619 || staff_id == 637 || staff_id == 597 || staff_id == 636 || staff_id == 668 || staff_id == 672 || staff_id == 750 || staff_id == 760 || staff_id == 767){

        }else{
			/* if(check_total >= 50){
                // console.log('1');
                if(total_income_expense_riel > 0 ) {

                    total_income_expense_riel = total_income_expense_riel + ( 100 - parseInt(check_total));
                }else{
                    total_income_expense_riel = total_income_expense_riel - ( 100 - parseInt(check_total));
                }
			}else{
                // console.log('2');
                if(total_income_expense_riel > 0 ){
                    total_income_expense_riel = parseInt(total_income_expense_riel) - parseInt(check_total);
                }else{
                    total_income_expense_riel = parseInt(total_income_expense_riel) + parseInt(check_total);
                }

			} */
		}

			
			if(total_income_expense_riel>0){
				total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = total_income_expense_riel;
                
            }else{
            	total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = '('+total_income_expense_riel+')';
                
            }



			var total_income_expense_dollar = income_dollar - expense_dollar;
			var total_income_expense_dollar_noStr = income_dollar - expense_dollar;
            if(total_income_expense_dollar>0){
                total_income_expense_dollar = total_income_expense_dollar.toFixed(2);
            }else{
                total_income_expense_dollar = '('+total_income_expense_dollar.toFixed(2)+')';
            }

            var sum_totalall = sum_total2digitr + sum_total3digitr;
            var sum_totalall_dolla = sum_total2digits + sum_total3digits;
	
			tr = '<tr class="bee_highlight background"><th>{{trans('label.total')}}</th><th></th><th class="pOwner"></th><th >'+total_page_sum+'</th><th class="{{$hidedata}}">'+sum_total2digitr+'</th><th class="{{$hidedata}}">'+sum_total3digitr+'</th><th class="{{$sumHide}}">'+sum_totalall+'</th><th class="1digit">'+addCommas(sum_total1digitrright)+'</th><th class="win2DR">'+sum_total2digitrright+'</th><th class="win3DR">'+sum_total3digitrright+'</th><th class="{{$hidedata}} money2DD">$ '+sum_total2digits+'</th><th class="{{$hidedata}} money3DD">$ '+sum_total3digits+'</th><th class="{{$sumHide}} moneyAllDD">$ '+sum_totalall_dolla+'</th><th class="1digit">$ '+addCommas(sum_total1digitsright)+'</th><th class="win2DD">$ '+sum_total2digitsright+'</th><th class="win3DD">$ '+sum_total3digitsright+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class="bee_highlight"><td></td><td>{{trans('label.total_charge')}}</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)))+'</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitrright * 70)+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitrright * 600)+'</td><td>'+commaSeparateNumber(((parseInt(sum_total2digits) * parseInt(twodigit_charge)) / 100).toFixed(2))+'</td><td>'+commaSeparateNumber(((parseInt(sum_total3digits) * parseInt(threedigit_charge)) / 100).toFixed(2))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitsright * 70 )+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitsright * 600)+'</td><tr>';
			// $('.display_total_result').append(tr);
            var total_rate_thesame = parseInt(sum_total2digitr) + parseInt(sum_total3digitr);
            var total_rate_thesame_dolla = parseFloat(sum_total2digits) + parseFloat(sum_total3digits); 
			tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result').append(tr);
			// 2D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th colspan="3" class="pOwnerMan" rowspan="2" style="vertical-align: middle; text-align: center;">{{$rowlist->s_name}}</th><th>2D</th><th>'+sum_total2digitr+'</th><th>x '+twodigit_charge+'%</th><th>'+Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100))+'</th><th class="win3DR"></th><th class="footerDollar">$ '+sum_total2digits+'</th><th class="footerDollar">x '+twodigit_charge+'%</th><th class="footerDollar">$ '+((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100).toFixed(2)+'</th><th class="footerDollar"></th></tr>';
			$('.display_total_result').append(tr);
			// 3D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th>3D</th><th>'+sum_total3digitr+'</th><th>x '+threedigit_charge+'%</th><th>'+Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100))+'</th><th class="win3DR"></th><th class="footerDollar">$ '+sum_total3digits+'</th><th class="footerDollar">x '+threedigit_charge+'%</th><th class="footerDollar">$ '+((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100).toFixed(2)+'</th><th class="footerDollar"></th></tr>';
			$('.display_total_result').append(tr);

			tr = '<tr class="bee_highlight {{$sumHide}}"><th colspan="2" style="vertical-align: middle; text-align: center;">{{$rowlist->s_name}}</th><th>Total</th><th>'+total_rate_thesame+'</th><th>x '+twodigit_charge+'%</th><th>'+Math.round(((parseFloat(total_rate_thesame) * parseFloat(threedigit_charge)) / 100))+'</th><th class="win3DR"></th><th class="footerDollar">$ '+total_rate_thesame_dolla+'</th><th class="footerDollar">x '+twodigit_charge+'%</th><th class="footerDollar">$ '+((parseFloat(total_rate_thesame_dolla) * parseFloat(threedigit_charge)) / 100).toFixed(2)+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);




			<?php 
			if($var_type_lottery == 1){
			?>
				

				// 2D(T)
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}} pOwnerMan" colspan="3" rowspan="3" style="vertical-align: middle; text-align: center;">សង</th><th class="{{$sumHide}}" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th>x<?php echo $win_two_digit;?></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th class="win3DR"></th><th class="footerDollar">'+sum_total2digitsright+'</th><th class="footerDollar">x<?php echo $win_two_digit;?></th><th class="footerDollar">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				tr = '<tr class="bee_highlight"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th>x<?php echo $win_three_digit;?></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th class="win3DR"></th><th class="footerDollar">'+sum_total3digitsright+'</th><th class="footerDollar">x<?php echo $win_three_digit;?></th><th class="footerDollar">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);

				// 1D(T)
				tr = '<tr class="bee_highlight"><th>1D(T)</th><th>'+sum_total1digitrright+'</th><th>x'+oneDPay+'</th><th>'+sum_total1digitrright * oneDPay+'</th><th class="win3DR"></th><th class="footerDollar">'+sum_total1digitsright+'</th><th class="footerDollar">x'+oneDPay+'</th><th class="footerDollar">'+sum_total1digitsright * oneDPay+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);

			<?php }else if($var_type_lottery == 2 || $var_type_lottery == 3 || $var_type_lottery == 4){?>
				// 2D(T)
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}} pOwnerMan" colspan="3" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th class="{{$sumHide}}" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th class="win3DR"></th><th class="footerDollar">'+sum_total2digitsright+'</th><th class="footerDollar"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th class="footerDollar">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				tr = '<tr class="bee_highlight"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th class="win3DR"></th><th class="footerDollar">'+sum_total3digitsright+'</th><th class="footerDollar"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th class="footerDollar">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);
			<?php }else{?>
				
				// 2D(T)
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}} pOwnerMan" colspan="3" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th colspan="2" class="{{$sumHide}}" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th>x<?php echo $win_two_digit;?></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th class="win3DR"></th><th class="footerDollar">'+sum_total2digitsright+'</th><th class="footerDollar">x<?php echo $win_two_digit;?></th><th class="footerDollar">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				tr = '<tr class="bee_highlight"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th>x<?php echo $win_three_digit;?></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th class="win3DR"></th><th class="footerDollar">'+sum_total3digitsright+'</th><th class="footerDollar">x<?php echo $win_three_digit;?></th><th class="footerDollar">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);

			<?php }?>




			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);


			// tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th></th><th></th><th class="result_right_total">'+total_income_expense_riel+' </th><th></th><th></th><th></th><th class="result_right_total">$ '+total_income_expense_dollar+'</th><th class="{{$hidedata}}"></th></tr>';
			// $('.display_total_result').append(tr);

			/* tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result').append(tr); */



			// paid or borrow
			var totalRealBefore = total_income_expense_riel_noStr;
			var totalDollaBefore = total_income_expense_dollar_noStr;
			var redColor = '';
			var redColorDolar = '';

			if(totalRealBefore < 0){
				var labelBefore_r = 'មេខាត';
				var redColor = 'color: #ff0000;';
			}else{
				var labelBefore_r = 'មេសុី';
			}

			if(totalDollaBefore < 0){
				var labelBefore_d = 'មេខាត';
				var redColorDolar = 'color: #ff0000;';
			}else{
				var labelBefore_d = 'មេសុី';
			}	

			// price before percent
			tr = '<tr class="bee_highlight"><th class="" colspan="{{$colspan}}"></th><th colspan="4" class="pOwnerManFour" style="text-align:right !important;'+redColor+'">'+labelBefore_r+'</th><th class="result_right_total" style="text-align:center !important; '+redColor+'">'+addCommas(totalRealBefore)+' </th><th class="win3DR"></th><th class="footerDollar"></th><th class="footerDollar" style="'+redColorDolar+'">'+labelBefore_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+totalDollaBefore.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
			$('.display_total_result').append(tr);

			@if($percentStaff > 0)

				var percent = 100 - parseFloat('{{$percentStaff}}');

				var totalReal = parseFloat(totalRealBefore * percent)/100;
				var totalDolla = parseFloat(totalDollaBefore * percent)/100;

				totalRealBefore = totalReal;
				totalDollaBefore = totalDolla;

				var redColor = '';
				if(totalReal < 0){
					var redColor = 'color: #ff0000;';
				}

				// price after percent
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th colspan="3" class="pOwnerMan"></th><th colspan="2" style="text-align:right !important; '+redColor+'">%</th><th class="result_right_total" style="text-align:center !important; '+redColor+'">'+addCommas(totalReal)+' </th><th class="win3DR"></th><th class="footerDollar"></th><th class="footerDollar">%</th><th class="result_right_total footerDollar" style="text-align:left !important;">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);

			@else

				var totalReal = totalRealBefore;
				var totalDolla = totalDollaBefore;

			@endif

@if($var_type_lottery == 1 || $var_type_lottery == 2 || $var_type_lottery == 5 || $var_type_lottery == 6 || $var_type_lottery == 7 )
			
			console.log("{{$var_type_lottery}}", 'type lottery');
			var displayMainTotal = 0;
			<?php 
				if (!empty($money_paid) && ($checkinView == 1 && $var_type_lottery == 1 || $var_type_lottery == 5 || $var_type_lottery == 6 || $var_type_lottery == 7) ) {
					foreach($money_paid as $key => $value) {

						if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23){
							$redColor = "";
							if($value->st_type == 21 && $value->st_price_r < 0 ){
								$label_r = 'ខាតចាស់';
								$redColor = "color: #ff0000;";
							}else{
								$label_r = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_d < 0 ){
								$label_d = 'ខាតចាស់';
								$redColor = 'color: #ff0000;';
							}else{
								$label_d = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_r > 0 && $value->st_price_d > 0 ){
								$label_r = $value->pav_value;
								$label_d = '';
							}
							if($value->st_type == 21 && $value->st_price_r < 0 && $value->st_price_d < 0 ){
								$label_r = 'ខាតចាស់';
								$label_d = '';
								$redColor = 'color: #ff0000;';
							}

			?>	
							// console.log(totalDolla);
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							if(subPrice_r != ''){
								totalReal = totalReal + parseFloat(subPrice_r);
							}

							if(subPrice_d != ''){
								totalDolla = totalDolla + parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							var displayPriceR = parseFloat('{{$value->st_price_r}}');
							var displayPriceD = parseFloat('{{$value->st_price_d}}');
							tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th colspan="3" class="pOwnerMan"></th><th colspan="2" style="text-align:right !important;">{{ $label_r }}</th><th class="result_right_total" style="text-align:center !important; {{$redColor}}">'+addCommas(displayPriceR)+'</th><th class="win3DR"></th><th class="footerDollar"></th><th class="footerDollar">{{ $label_d }}</th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}else{
			?>	
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							if(subPrice_r > 0){
								totalReal = totalReal - parseFloat(subPrice_r);
							}
							if(subPrice_d > 0){
								totalDolla = totalDolla - parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							
							var displayPriceR = parseFloat('{{$value->st_price_r * -1}}');
							var displayPriceD = parseFloat('{{$value->st_price_d * -1}}');
							tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th colspan="3" class="pOwnerMan"></th><th colspan="2" style="text-align:right !important;">{{ $value->pav_value }}</th><th class="result_right_total" style="text-align:center !important;">'+addCommas(displayPriceR)+'</th><th class="win3DR"></th><th class="footerDollar"></th><th class="footerDollar"></th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}
					}
				}else if (!empty($money_paid) && ($checkinViewKh == 1 && $var_type_lottery == 2) ) {
					foreach($money_paid as $key => $value) {
						if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23){
							if($value->st_type == 21 && $value->st_price_r < 0 ){
								$label_r = 'ខាត';
							}else{
								$label_r = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_d < 0 ){
								$label_d = 'ខាត';
							}else{
								$label_d = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_r > 0 && $value->st_price_d > 0 ){
								$label_r = $value->pav_value;
								$label_d = '';
							}
							if($value->st_type == 21 && $value->st_price_r < 0 && $value->st_price_d < 0 ){
								$label_r = 'ខាត';
								$label_d = '';
							}

			?>	
							// console.log(totalDolla);
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							if(subPrice_r != ''){
								totalReal = totalReal + parseFloat(subPrice_r);
							}

							if(subPrice_d != ''){
								totalDolla = totalDolla + parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							var displayPriceR = parseFloat('{{$value->st_price_r}}');
							var displayPriceD = parseFloat('{{$value->st_price_d}}');
							tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th colspan="3" class="pOwnerMan"></th><th colspan="2" style="text-align:right !important;">{{ $label_r }}</th><th class="result_right_total" style="text-align:center !important;">'+addCommas(displayPriceR)+'</th><th class="win3DR"></th><th class="footerDollar"></th><th class="footerDollar">{{ $label_d }}</th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}else{
			?>	
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							if(subPrice_r > 0){
								totalReal = totalReal - parseFloat(subPrice_r);
							}
							if(subPrice_d > 0){
								totalDolla = totalDolla - parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							
							var displayPriceR = parseFloat('{{$value->st_price_r * -1}}');
							var displayPriceD = parseFloat('{{$value->st_price_d * -1}}');
							tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th colspan="3" class="pOwnerMan"></th><th colspan="2" style="text-align:right !important;">{{ $value->pav_value }}</th><th class="result_right_total" style="text-align:center !important;">'+addCommas(displayPriceR)+'</th><th class="win3DR"></th><th class="footerDollar"></th><th class="footerDollar"></th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}
					}
				}
			?>
			// end paid or borrow
			if(displayMainTotal > 0 ){
				// total price
				tr = '<tr class=""><td colspan="11"></td></tr>';
				$('.display_total_result').append(tr);

				var redColor = '';
				var redColorDolar = '';

				if(totalReal > 0){
					var labelTotal = 'មេសុី'; 
				}else{
					var labelTotal = 'មេខាត';
					var redColor = 'color: #ff0000;';
				}

				if(totalDolla > 0){
					var labelTotal_d = 'មេសុី'; 
				}else{
					var labelTotal_d = 'មេខាត';
					var redColorDolar = 'color: #ff0000;';
				}

				if(totalReal > 0 && totalDolla > 0){
					var labelTotal = 'មេសុី'; 
					var labelTotal_d = ''; 
				}

				if(totalReal < 0 && totalDolla < 0){
					var labelTotal = 'មេខាត'; 
					var labelTotal_d = ''; 
					var redColor = 'color: #ff0000;';
				}

				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th colspan="3" class="pOwnerMan"></th><th colspan="2" style="text-align:right !important; '+redColor+'">'+labelTotal+'</th><th class="result_right_total" style="text-align:center !important; '+redColor+'">'+addCommas(totalReal)+'</th><th class="win3DR"></th><th class="footerDollar"></th><th style="'+redColorDolar+'" class="footerDollar">'+labelTotal_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);
			}

			

			// $(".report_total_info").each(function(index, element) {

			// 	staff_list = $(this).attr('staff');
			// 	id_staff = $(this).attr('id_staff');

			// 	time = $(this).attr('time');
			// 	page = $(this).attr('page');
			// 	$('.display_total_result').find('#td_staff_com_'+index).text(staff_list);
			// 	$('.display_total_result').find('#td_time_com_'+index).text(time);
			// 	$('.display_total_result').find('#td_page_com_'+index).text(page);

			// });

			@if($var_dateStart!='' &&  $var_staff!='' && $var_page=='' && $var_sheet=='' && $var_ownner=='')
				var date = '{{$var_dateStart}}';
				var staff = '{{$var_staff}}';
				var var_type_lottery = '{{$var_type_lottery}}';
				var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				<?php 
				if($var_type_lottery == 1){
				?>
					var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
					var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				<?php }else if($var_type_lottery == 2){?>
					var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
					var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				<?php }else if($var_type_lottery >= 5){?>
					var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
					var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				<?php }?>
//				console.log(income_riel);
//				console.log(income_dollar);
//				console.log(expense_riel);
//				console.log(expense_dollar);
				<?php 
				if($checkinView == 1 && ($var_type_lottery == 1 || $var_type_lottery >= 5) ){
				?>
	// 				$.ajax({
	// 					url: '../addmoney',
	// 					type: 'GET',
	// 					data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla,totalRealBefore:totalRealBefore,totalDollaBefore:totalDollaBefore},
	// 					success: function(data) {
	// 						if(data.status=="success"){
	// //							console.log(data.msg);
	// 						}else{
	// //							console.log(data.msg);
	// 						}
	// 					}
	// 				});
				<?php 
				}
				?>

				<?php 
				if($checkinViewKh == 1 && $var_type_lottery == 2){
				?>	
					console.log("{{$checkinViewKh}}");
	// 				$.ajax({
	// 					url: '../addmoney',
	// 					type: 'GET',
	// 					data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla,totalRealBefore:totalRealBefore,totalDollaBefore:totalDollaBefore},
	// 					success: function(data) {
	// 						if(data.status=="success"){
	// //							console.log(data.msg);
	// 						}else{
	// //							console.log(data.msg);
	// 						}
	// 					}
	// 				});
				<?php 
				}
				?>

// 				$.ajax({
// 					url: 'summary_lottery',
// 					type: 'GET',
// 					data: {date:date,staff:staff,income_riel:income_riel,income_dollar:income_dollar,expense_riel:expense_riel,expense_dollar:expense_dollar,var_type_lottery:var_type_lottery},
// 					success: function(data) {
// 						if(data.status=="success"){
// //							console.log(data.msg);
// 						}else{
// //							console.log(data.msg);
// 						}
// 					}
// 				});
				
// 				$.ajax({
// 					url: 'summary_lottery_total',
// 					type: 'GET',
// 					data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:var_type_lottery},
// 					success: function(data) {
// 						if(data.status=="success"){
// //							console.log(data.msg);
// 						}else{
// //							console.log(data.msg);
// 						}
// 					}
// 				});

// 				$.ajax({
// 					url: 'addtotalmoney',
// 					type: 'GET',
// 					data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:var_type_lottery},
// 					success: function(data) {
// 						if(data.status=="success"){
// //							console.log(data.msg);
// 						}else{
// //							console.log(data.msg);
// 						}
// 					}
// 				});
		    @endif

			

@endif

			/* console.log(win2DR,' win2DR'); */
			/* console.log(win3DR,' win3DR'); */
			/* console.log(moneyAllDD,' moneyAllDD'); */
			
			if(win3DR == 0){
				$(".win3DR").addClass('hidetableTD');
			}

			if(moneyAllDD == 0){
				console.log(moneyAllDD,' moneyAllDD');
				console.log(money2DD,' money2DD');
				console.log(money3DD,' money3DD');
				console.log(win2DD,' money2DD');
				console.log(win3DD,' money3DD');
				$(".moneyAllDD").addClass('hidetableTD');
				$(".money2DD").addClass('hidetableTD');
				$(".money3DD").addClass('hidetableTD');
				$(".win2DD").addClass('hidetableTD');
				$(".win3DD").addClass('hidetableTD');
				$("th.footerDollar").addClass('hidetableTD');
			}

			if(pOwner == 0){
				$(".pOwner").addClass('hidetableTD');
				$(".pOwnerMan").attr('colspan',2);
				$(".pOwnerManFour").attr('colspan',3);
				$(".numSumP").hide();
			}

		});
@else

	$(document).ready(function() {
		var new_time = $("#sheet option:selected").text();
		var date_new = $('#dateStart').val();
		var page_value = $('#pages').val();
		var type_lottery = $("#type_lottery").val();
		var staff_new = $('#staff option:selected').text();
		var id_staff = $('#staff').val();
		var ownner = $("#ownner").val();

		row_data = '';
		
					

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
		$.ajax({
			// url: '/report/summary_lottery_page',
			url:"{{ route('summary_lottery_page.post') }}",
			type: 'POST',
			data: {row_data:row_data,date_new:date_new,type_lottery:type_lottery,staff_new:staff_new,id_staff:id_staff,page_value:page_value, new_time:new_time, ownner:ownner},
			success: function(data) {
				console.log(data);
				if(data.status=="success"){
					// console.log(data.msg);
				}else{
//							console.log(data.msg);
				}
			}
		});
	});

@endif
	$(document).ready(function() {
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
		});
	});
	$(document).ready(function(){

		$('.btn_print').click(function(){
			var attr_id = $(this).attr('attr_id');
			var attr_date = $(this).attr('attr_date');
			var attr_staff = $(this).attr('attr_staff');
			var attr_sheet = $(this).attr('attr_sheet');
			if(attr_sheet==''){
				attr_sheet = 0;
			}
			var attr_page = $(this).attr('attr_page');
			if(attr_page==''){
				attr_page = 0;
			}
			var form_upload = $('#frmupload');
			var fram_upload = $('<iframe style="border:0px; width:0px; height:0px; opacity: 0px;" src="http://188.166.234.165/lottery/screenshot/download.php?attr_id='+attr_id+'&attr_date='+attr_date+'&attr_staff='+attr_staff+'&attr_sheet='+attr_sheet+'&attr_page='+attr_page+'" id="framupload" name="framupload"></iframe>');

			$('body').append(fram_upload);

		});
	});


	document.getElementById('btnPrintDiv').addEventListener ("click", print)
	function print() {
		printJS({
			printable: 'html-content-holder',
			type: 'html',
			targetStyles: ['*']
		})
	}
	// Print.js
	!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define("print-js",[],t):"object"==typeof exports?exports["print-js"]=t():e["print-js"]=t()}(this,function(){return function(e){function t(i){if(n[i])return n[i].exports;var o=n[i]={i:i,l:!1,exports:{}};return e[i].call(o.exports,o,o.exports,t),o.l=!0,o.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,i){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:i})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p="./",t(t.s=10)}([function(e,t,n){"use strict";function i(e,t){if(e.focus(),r.a.isEdge()||r.a.isIE())try{e.contentWindow.document.execCommand("print",!1,null)}catch(t){e.contentWindow.print()}r.a.isIE()||r.a.isEdge()||e.contentWindow.print(),r.a.isIE()&&"pdf"===t.type&&setTimeout(function(){e.parentNode.removeChild(e)},2e3),t.showModal&&a.a.close(),t.onLoadingEnd&&t.onLoadingEnd()}function o(e,t,n){void 0===e.naturalWidth||0===e.naturalWidth?setTimeout(function(){o(e,t,n)},500):i(t,n)}var r=n(1),a=n(3),d={send:function(e,t){document.getElementsByTagName("body")[0].appendChild(t);var n=document.getElementById(e.frameId);"pdf"===e.type&&(r.a.isIE()||r.a.isEdge())?n.setAttribute("onload",i(n,e)):t.onload=function(){if("pdf"===e.type)i(n,e);else{var t=n.contentWindow||n.contentDocument;t.document&&(t=t.document),t.body.innerHTML=e.htmlData,"image"===e.type?o(t.getElementById("printableImage"),n,e):i(n,e)}}}};t.a=d},function(e,t,n){"use strict";var i={isFirefox:function(){return"undefined"!=typeof InstallTrigger},isIE:function(){return-1!==navigator.userAgent.indexOf("MSIE")||!!document.documentMode},isEdge:function(){return!i.isIE()&&!!window.StyleMedia},isChrome:function(){return!!window.chrome&&!!window.chrome.webstore},isSafari:function(){return Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor")>0||-1!==navigator.userAgent.toLowerCase().indexOf("safari")}};t.a=i},function(e,t,n){"use strict";function i(e,t){return'<div style="font-family:'+t.font+" !important; font-size: "+t.font_size+' !important; width:100%;">'+e+"</div>"}function o(e){return e.charAt(0).toUpperCase()+e.slice(1)}function r(e,t){var n=document.defaultView||window,i=[],o="";if(n.getComputedStyle){i=n.getComputedStyle(e,"");var r=t.targetStyles||["border","box","break","text-decoration"],a=t.targetStyle||["clear","display","width","min-width","height","min-height","max-height"];t.honorMarginPadding&&r.push("margin","padding"),t.honorColor&&r.push("color");for(var d=0;d<i.length;d++)for(var l=0;l<r.length;l++)"*"!==r[l]&&-1===i[d].indexOf(r[l])&&-1===a.indexOf(i[d])||(o+=i[d]+":"+i.getPropertyValue(i[d])+";")}else if(e.currentStyle){i=e.currentStyle;for(var s in i)-1!==i.indexOf("border")&&-1!==i.indexOf("color")&&(o+=s+":"+i[s]+";")}return o+="max-width: "+t.maxWidth+"px !important;"+t.font_size+" !important;"}function a(e,t){for(var n=0;n<e.length;n++){var i=e[n],o=i.tagName;if("INPUT"===o||"TEXTAREA"===o||"SELECT"===o){var d=r(i,t),l=i.parentNode,s="SELECT"===o?document.createTextNode(i.options[i.selectedIndex].text):document.createTextNode(i.value),c=document.createElement("div");c.appendChild(s),c.setAttribute("style",d),l.appendChild(c),l.removeChild(i)}else i.setAttribute("style",r(i,t));var p=i.children;p&&p.length&&a(p,t)}}function d(e,t,n){var i=document.createElement("h1"),o=document.createTextNode(t);i.appendChild(o),i.setAttribute("style",n),e.insertBefore(i,e.childNodes[0])}t.a=i,t.b=o,t.c=r,t.d=a,t.e=d},function(e,t,n){"use strict";var i={show:function(e){var t=document.createElement("div");t.setAttribute("style","font-family:sans-serif; display:table; text-align:center; font-weight:300; font-size:30px; left:0; top:0;position:fixed; z-index: 9990;color: #0460B5; width: 100%; height: 100%; background-color:rgba(255,255,255,.9);transition: opacity .3s ease;"),t.setAttribute("id","printJS-Modal");var n=document.createElement("div");n.setAttribute("style","display:table-cell; vertical-align:middle; padding-bottom:100px;");var o=document.createElement("div");o.setAttribute("class","printClose"),o.setAttribute("id","printClose"),n.appendChild(o);var r=document.createElement("span");r.setAttribute("class","printSpinner"),n.appendChild(r);var a=document.createTextNode(e.modalMessage);n.appendChild(a),t.appendChild(n),document.getElementsByTagName("body")[0].appendChild(t),document.getElementById("printClose").addEventListener("click",function(){i.close()})},close:function(){var e=document.getElementById("printJS-Modal");e.parentNode.removeChild(e)}};t.a=i},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=n(7),o=i.a.init;"undefined"!=typeof window&&(window.printJS=o),t.default=o},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.getElementById(e.printable);if(!r)return window.console.error("Invalid HTML element id: "+e.printable),!1;var a=document.createElement("div");a.appendChild(r.cloneNode(!0)),a.setAttribute("style","display:none;"),a.setAttribute("id","printJS-html"),r.parentNode.appendChild(a),a=document.getElementById("printJS-html"),a.setAttribute("style",n.i(i.c)(a,e)+"margin:0 !important;");var d=a.children;n.i(i.d)(d,e),e.header&&n.i(i.e)(a,e.header,e.headerStyle),a.parentNode.removeChild(a),e.htmlData=n.i(i.a)(a.innerHTML,e),o.a.send(e,t)}}},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.createElement("img");r.src=e.printable,r.onload=function(){r.setAttribute("style","width:100%;"),r.setAttribute("id","printableImage");var a=document.createElement("div");a.setAttribute("style","width:100%"),a.appendChild(r),e.header&&n.i(i.e)(a,e.header,e.headerStyle),e.htmlData=a.outerHTML,o.a.send(e,t)}}}},function(e,t,n){"use strict";var i=n(1),o=n(3),r=n(9),a=n(5),d=n(6),l=n(8),s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},c=["pdf","html","image","json"];t.a={init:function(){var e={printable:null,fallbackPrintable:null,type:"pdf",header:null,headerStyle:"font-weight: 300;",maxWidth:800,font:"TimesNewRoman",font_size:"12pt",honorMarginPadding:!0,honorColor:!1,properties:null,gridHeaderStyle:"font-weight: bold;",gridStyle:"border: 1px solid lightgray; margin-bottom: -1px;",showModal:!1,onLoadingStart:null,onLoadingEnd:null,modalMessage:"Retrieving Document...",frameId:"printJS",htmlData:"",documentTitle:"Document",targetStyle:null,targetStyles:null},t=arguments[0];if(void 0===t)throw new Error("printJS expects at least 1 attribute.");switch(void 0===t?"undefined":s(t)){case"string":e.printable=encodeURI(t),e.fallbackPrintable=e.printable,e.type=arguments[1]||e.type;break;case"object":e.printable=t.printable,e.fallbackPrintable=void 0!==t.fallbackPrintable?t.fallbackPrintable:e.printable,e.type=void 0!==t.type?t.type:e.type,e.frameId=void 0!==t.frameId?t.frameId:e.frameId,e.header=void 0!==t.header?t.header:e.header,e.headerStyle=void 0!==t.headerStyle?t.headerStyle:e.headerStyle,e.maxWidth=void 0!==t.maxWidth?t.maxWidth:e.maxWidth,e.font=void 0!==t.font?t.font:e.font,e.font_size=void 0!==t.font_size?t.font_size:e.font_size,e.honorMarginPadding=void 0!==t.honorMarginPadding?t.honorMarginPadding:e.honorMarginPadding,e.properties=void 0!==t.properties?t.properties:e.properties,e.gridHeaderStyle=void 0!==t.gridHeaderStyle?t.gridHeaderStyle:e.gridHeaderStyle,e.gridStyle=void 0!==t.gridStyle?t.gridStyle:e.gridStyle,e.showModal=void 0!==t.showModal?t.showModal:e.showModal,e.onLoadingStart=void 0!==t.onLoadingStart?t.onLoadingStart:e.onLoadingStart,e.onLoadingEnd=void 0!==t.onLoadingEnd?t.onLoadingEnd:e.onLoadingEnd,e.modalMessage=void 0!==t.modalMessage?t.modalMessage:e.modalMessage,e.documentTitle=void 0!==t.documentTitle?t.documentTitle:e.documentTitle,e.targetStyle=void 0!==t.targetStyle?t.targetStyle:e.targetStyle,e.targetStyles=void 0!==t.targetStyles?t.targetStyles:e.targetStyles;break;default:throw new Error('Unexpected argument type! Expected "string" or "object", got '+(void 0===t?"undefined":s(t)))}if(!e.printable)throw new Error("Missing printable information.");if(!e.type||"string"!=typeof e.type||-1===c.indexOf(e.type.toLowerCase()))throw new Error("Invalid print type. Available types are: pdf, html, image and json.");e.showModal&&o.a.show(e),e.onLoadingStart&&e.onLoadingStart();var n=document.getElementById(e.frameId);n&&n.parentNode.removeChild(n);var p=void 0;switch(p=document.createElement("iframe"),p.setAttribute("style","display:none;"),p.setAttribute("id",e.frameId),"pdf"!==e.type&&(p.srcdoc="<html><head><title>"+e.documentTitle+"</title></head><body></body></html>"),e.type){case"pdf":if(i.a.isFirefox()||i.a.isEdge()||i.a.isIE()){window.open(e.fallbackPrintable,"_blank").focus(),e.showModal&&o.a.close(),e.onLoadingEnd&&e.onLoadingEnd()}else r.a.print(e,p);break;case"image":d.a.print(e,p);break;case"html":a.a.print(e,p);break;case"json":l.a.print(e,p);break;default:throw new Error("Invalid print type. Available types are: pdf, html, image and json.")}}}},function(e,t,n){"use strict";function i(e){var t=e.printable,i=e.properties,r='<div style="display:flex; flex-direction: column;">';r+='<div style="flex:1 1 auto; display:flex;">';for(var a=0;a<i.length;a++)r+='<div style="flex:1; padding:5px;'+e.gridHeaderStyle+'">'+n.i(o.b)(i[a])+"</div>";r+="</div>";for(var d=0;d<t.length;d++){r+='<div style="flex:1 1 auto; display:flex;">';for(var l=0;l<i.length;l++){var s=t[d],c=i[l].split(".");if(c.length>1)for(var p=0;p<c.length;p++)s=s[c[p]];else s=s[i[l]];r+='<div style="flex:1; padding:5px;'+e.gridStyle+'">'+s+"</div>"}r+="</div>"}return r+="</div>"}var o=n(2),r=n(0),a="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};t.a={print:function(e,t){if("object"!==a(e.printable))throw new Error("Invalid javascript data object (JSON).");if(!e.properties||"object"!==a(e.properties))throw new Error("Invalid properties array for your JSON data.");var d="";e.header&&(d+='<h1 style="'+e.headerStyle+'">'+e.header+"</h1>"),d+=i(e),e.htmlData=n.i(o.a)(d,e),r.a.send(e,t)}}},function(e,t,n){"use strict";function i(e,t){t.setAttribute("src",e.printable),r.a.send(e,t)}var o=n(1),r=n(0);t.a={print:function(e,t){if(e.showModal||e.onLoadingStart||o.a.isIE()){var n=new window.XMLHttpRequest;n.addEventListener("load",i(e,t)),n.open("GET",window.location.origin+"/"+e.printable,!0),n.send()}else i(e,t)}}},function(e,t,n){e.exports=n(4)}])});
	

	</script>
@stop