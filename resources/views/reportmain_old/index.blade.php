@extends('master')
@section('title')
<title>មេីលបញ្ជីសរុប</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}
		.backgroundSubTotal{
			background: #eaeaea;
		}

		table.table-bordered.summary_result_per_day{
            border: 1px #fff solid !important;
        }

        .table-bordered>tbody>tr{
            border:none !important;
        }

        .table-bordered>thead th{
            background-image:none !important;
			background: #5698d2;
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td{
            border: 1px #000 solid !important;
            border-bottom: none !important;
            text-align: left;
            width: 150px;
        }
        .table-bordered>tbody>tr>th, .table-bordered>thead>tr>th{
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td.clearBorder,.table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }
        .table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }

        .table-bordered>tbody>tr>th.result_right_total{
            border: none !important;
            border-bottom: 1px #000 solid !important;
        }

        * {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }
        *:before,
        *:after {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }
        .table-bordered>tbody>tr>td.lr_clear{
            border-left: none !important;
            border-right: none !important;
        }
        .table-bordered>tbody>tr>th.bottom_clear{
            border-bottom: none !important;
        }

		thead.footer th{
			background: none !important;
            /*border: 1px #000 solid !important;*/
		}
		thead.footer th.backgroundSubTotal{
			background: #5698d2 !important;
            
		}

		.display_total_result tr:nth-child(even) {background: #CCC}
		.display_total_result tr:nth-child(odd) {background: #FFF}
		th.text-right{
			text-align:right !important;
		}
		tbody>tr>td, tbody>tr>td>span{
			text-align: left !important;
		}
		tbody>tr>td>div{
			text-align: left !important;
			width: 100%;
			height: 100%;
		}

		input.txteditor{
			border: none;
			background: none;
			width: 90%;
			height: 100%;
			display: none;
		}

		.display_total_result tr:hover td{
			background: #c1dbf3 !important;
		}


	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ ប្រតិបត្តិការបុគ្គលិក</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>មេីលបញ្ជីសរុប</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'filterMain', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_staff_chail))
							 <?php $staff_chail_filter = $var_staff_chail; ?>
						 @else
							 <?php $staff_chail_filter = null; ?>
						 @endif

						 

						 @if(isset($var_dateEnd))
							 <?php $endDate_filter = $var_dateEnd; ?>
						 @else
							 <?php $endDate_filter = null; ?>
						 @endif

						
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>
							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateEnd", $value = $endDate_filter, $attributes = array('class' => 'form-control ', 'id' => 'dateEnd','placeholder'=> trans('result.dateEnd'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>


							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('s_id_man', ([
                                        '' => 'ឈ្មួញកណ្តាល' ]+$staffMain),$staff_filter,['class' => ' ','id'=>'s_id_man','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>
							 <section class="col col-2">
								 <label class="select">
									@if($staff_filter != '')
										{{ Form::select('s_id', ([
                                        '' => 'កូនក្រុម' ]+$chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@else
										{{ Form::select('s_id', ($chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@endif
									 
									 <i></i>
								 </label>
							 </section>


							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					<?php 
						

						if($staff_chail_filter){
							$loopStaff = DB::table('tbl_staff')
								                ->where('parent_id', $var_staff_chail)
								                ->where('Active', 0)
								                ->orderBy('tbl_staff.s_name', 'ASC')
								                ->get();
							if(count($loopStaff) == 0){
								// dd($var_staff_chail, 'in');
								$loopStaff = DB::table('tbl_staff')
								                ->where('s_id', $var_staff_chail)
								                ->where('Active', 0)
								                ->orderBy('tbl_staff.s_name', 'ASC')
								                ->get();
							}
							// dd($loopStaff);
						}else if($staff_filter == 'all'){
							$loopStaff = DB::table('tbl_staff')
												->where('parent_id','<>', 0)
												->where('Active', 0)
								                ->orderBy('tbl_staff.s_name', 'ASC')
								                ->get();
						}else if($staff_filter){

							$loopStaff = DB::table('tbl_staff')
								                ->where('parent_id', $var_staff)
								                ->where('Active', 0)
								                ->orderBy('tbl_staff.s_name', 'ASC')
								                ->get();
						}
					?>

					@if(isset($loopStaff))
					<div id="printElement">
                        <style>
                            table.displayReport{
                                font-size: 12px;
								border-collapse: collapse;
                            }
                            table.displayReport tr th{
                                font-size: 12px;
                                vertical-align: top;
                                text-align:center !important;
								padding: 10px 5px;
								background: #5698d2 !important;
								border: 1px #000 solid !important;
                            }
							table.displayReport tr td{
								padding: 8px 3px;
								border: 1px #000 solid !important;
								
							}
							table.displayReport thead{

							}
							table.displayReport thead.footer th{
								background:none !important;
							}
							table.displayReport thead.footer th.backgroundSubTotal{
								background: #5698d2 !important;
								
							}
							table.displayReport thead.footer th.clearBor{
								border:none !important;
								border-bottom:none !important;
							}
                        </style>
                        <div style="padding-left: 20px; padding-bottom: 20px; font-size: 14px;">
                        	@if($mainNameStaffInfo)
							<span style="">ឈ្មោះកូន: {{$mainNameStaffInfo->s_name}}</span>
							@else
							<span style="">ឈ្មោះកូន: All</span>
							@endif

							<span style="padding-left: 50px;">ថ្ងៃខែ: {{$startDate_filter}}</span>

                        </div>

						 <table  class="table-bordered-new displayReport" border="0" cellpadding="0" cellspacing="0" width="1280">
                            <thead>
                                <tr>
									<th style="width: 80px !important;" rowspan="2" v-align="top">ឈ្មោះ</th>
                                    <th colspan="2" v-align="top">លទ្ធផល</th>
                                    <th colspan="2" >លុយទទួលបាន</th>
                                    <th colspan="2">លុយអោយកូន</th>
                                    <th colspan="2">លុយតវ៉ា</th>
                                    <th colspan="2" >លុយសល់ចាស់</th>
                                    <th colspan="2" class="background">សរុបលុយចុងគ្រា</th>
                                </tr>
                                <tr>
                                    <th style="width: 120px !important;">រៀល(៛) </th>
                                    <th style="width: 80px !important;">ដុល្លារ($)</th>

                                    <th style="width: 120px !important;">រៀល(៛) </th>
                                    <th style="width: 80px !important;">ដុល្លារ($)</th>

                                    <th style="width: 120px !important;">រៀល(៛) </th>
                                    <th style="width: 80px !important;">ដុល្លារ($)</th>

									<th style="width: 120px !important;">រៀល(៛) </th>
                                    <th style="width: 80px !important;">ដុល្លារ($)</th>

                                    <th style="width: 120px !important;" class="">រៀល(៛) </th>
                                    <th style="width: 80px !important;" class="">ដុល្លារ($)</th>

                                    <th style="width: 120px !important;" class="background">រៀល(៛) </th>
                                    <th style="width: 80px !important;" class="background">ដុល្លារ($)</th>
                                </tr>
                            </thead>
                            <tbody class="display_total_result">

									<?php 						

										$endDate_filter = date('Y-m-d', strtotime($endDate_filter. ' +1 days'));


										$begin = new DateTime($startDate_filter);
										$end = new DateTime($endDate_filter);
										$interval = DateInterval::createFromDateString('1 day');
										$period = new DatePeriod($begin, $interval, $end);

										$totalMoneyFirst = 0;
										$totalMoneyFirstD = 0;
										$totalMoneyFromStaff = 0;
										$totalMoneyFromStaffD = 0;
										$totalMoneyToStaff = 0;
										$totalMoneyToStaffD = 0;
										$totalMoneyCheckagain = 0;
										$totalMoneyCheckagainD = 0;
										$totalMoneyOld = 0;
										$totalMoneyOldD = 0;
										$totalFinnal = 0;
										$totalFinnalD = 0;

										foreach ($loopStaff as $sLoop) {

											// get first money
											$dateDB = $startDate_filter;
											// calculate moneyFinal
											$finalPayment = \App\Http\Controllers\TransctionController::calculatemoney($dateDB, $sLoop->s_id);

											// var_dump($finalPayment[0],$finalPayment[1]);
											$payments = DB::table('tbl_staff_transction')
													->where('s_id',$sLoop->s_id)
													->where('st_date_search',$dateDB)
													->where('st_type', 21)->first();

											$moneyFirst = null;
											$moneyFirstD = null;
											if($payments){
												$moneyFirst = $payments->price_one_day_r;
												$moneyFirstD = $payments->price_one_day_d;
											}
											
											

											$totalMoneyFirst = $totalMoneyFirst + $moneyFirst;
											$totalMoneyFirstD = $totalMoneyFirstD + $moneyFirstD;


											// get money from staff
											$moneyFroStaff = DB::table('tbl_staff_transction')
														->where('s_id',$sLoop->s_id)
														->where('st_date_diposit',$dateDB)
														->where('st_type', 3)->first();
											$moneyFromSt = null;
											$moneyFromStD = null;
											if($moneyFroStaff){
												$moneyFromSt = $moneyFroStaff->st_price_r;
												$moneyFromStD = $moneyFroStaff->st_price_d;
											}

											$totalMoneyFromStaff = $totalMoneyFromStaff + $moneyFromSt;
											$totalMoneyFromStaffD = $totalMoneyFromStaffD + $moneyFromStD;

											// get money to staff
											$moneyToStaff = DB::table('tbl_staff_transction')
														->where('s_id',$sLoop->s_id)
														->where('st_date_diposit',$dateDB)
														->where('st_type', 4)->first();
											$moneyToSt = null;
											$moneyToStD = null;
											if($moneyToStaff){
												$moneyToSt = $moneyToStaff->st_price_r;
												$moneyToStD = $moneyToStaff->st_price_d;
											}

											$totalMoneyToStaff = $totalMoneyToStaff + $moneyToSt;
											$totalMoneyToStaffD = $totalMoneyToStaffD + $moneyToStD;

											// get money តវ៉ា staff
											$moneyCheckS = DB::table('tbl_staff_transction')
														->where('s_id',$sLoop->s_id)
														->where('st_date_diposit',$dateDB)
														->where('st_type', 20)->first();
											$moneyCheckAgain = null;
											$moneyCheckAgainD = null;
											if($moneyCheckS){
												$moneyCheckAgain = $moneyCheckS->st_price_r;
												$moneyCheckAgainD = $moneyCheckS->st_price_d;
											}

											$totalMoneyCheckagain = $totalMoneyCheckagain + $moneyCheckAgain;
											$totalMoneyCheckagainD = $totalMoneyCheckagainD + $moneyCheckAgainD;

											// get money old
											$moneyOld = DB::table('tbl_staff_transction')
														->where('s_id',$sLoop->s_id)
														->where('st_date_search',date('Y-m-d', strtotime($dateDB. ' -1 days')))
														->where('st_type', 21)->first();
											$moneyOldYes = null;
											$moneyOldYesD = null;
											if($moneyOld){
												$moneyOldYes = $moneyOld->st_price_r;
												$moneyOldYesD = $moneyOld->st_price_d;
											}

											$totalMoneyOld = $totalMoneyOld + $moneyOldYes;
											$totalMoneyOldD = $totalMoneyOldD + $moneyOldYesD;

											// get final money by day 
											$moneyFinal = DB::table('tbl_staff_transction')
														->where('s_id',$sLoop->s_id)
														->where('st_date_search',$dateDB)
														->where('st_type', 21)->first();
											$moneyFinalData = null;
											$moneyFinalDataD = null;
											if($moneyFinal){
												$moneyFinalData = $moneyFinal->st_price_r;
												$moneyFinalDataD = $moneyFinal->st_price_d;
											}

											$totalFinnal = $totalFinnal + $moneyFinalData;
											$totalFinnalD = $totalFinnalD + $moneyFinalDataD;


									?>
											<tr>
												<td class="date">{{$sLoop->s_name}}</td>
												<td>
													<span id="firstamount_{{$sLoop->s_id}}">{{number_format($moneyFirst)}}</span>

												</td>
												<td>
													<span id="firstamountD_{{$sLoop->s_id}}">{{number_format($moneyFirstD,2)}}</span>$
												</td>


												<!-- get money from staff -->
												<td class="">
													<div id="amountFromStaff_label_{{$sLoop->s_id}}">{{number_format($moneyFromSt)}}</div>
													
												</td>
												<td class="">
													<div id="amountFromStaffD_label_{{$sLoop->s_id}}">{{number_format($moneyFromStD,2)}}$</div>
												</td>


												<!-- get money to staff -->
												<td class="">
													<div id="amountToStaff_label_{{$sLoop->s_id}}">{{number_format($moneyToSt)}}</div>
													
												</td>
												<td class="editable">
													<div id="amountToStaffD_label_{{$sLoop->s_id}}">{{number_format($moneyToStD,2)}}$</div>
													
												</td>

												<!-- get money តវ៉ា staff -->
												<td class="editable">
													<div id="amountCheckAgain_label_{{$sLoop->s_id}}">{{number_format($moneyCheckAgain)}}</div>
													
												</td>
												<td class="editable">
													<div id="amountCheckAgainD_label_{{$sLoop->s_id}}">{{number_format($moneyCheckAgainD)}}$</div>
													
												</td>
												<td>{{number_format($moneyOldYes)}}</td>
												<td>{{number_format($moneyOldYesD,2)}}$</td>
												<td><span id="finalPayment_{{$sLoop->s_id}}">{{$finalPayment[0]}}</span></td>
												<td><span id="finalPaymentD_{{$sLoop->s_id}}">{{$finalPayment[1]}}</span>$</td>
											</tr>
									<?php
										}
									?>

							</tbody>
							<thead class="footer">

								<tr>
									<th>សរុប</th>
                                    <th style="text-align: left !important;" id="totalFirstMoney">{{number_format($totalMoneyFirst)}} </th>
                                    <th style="text-align: left !important;" id="totalFirstMoneyD">{{number_format($totalMoneyFirstD)}}$</th>

                                    <th style="text-align: left !important;" id="totalMoneyFromStaff">{{number_format($totalMoneyFromStaff)}} </th>
                                    <th style="text-align: left !important;" id="totalMoneyFromStaffD">{{number_format($totalMoneyFromStaffD)}}$</th>

                                    <th style="text-align: left !important;" id="totalMoneyToStaff">{{number_format($totalMoneyToStaff)}} </th>
                                    <th style="text-align: left !important;" id="totalMoneyToStaffD">{{number_format($totalMoneyToStaffD)}}$</th>

									<th style="text-align: left !important;" id="totalMoneyCheckagain">{{number_format($totalMoneyCheckagain)}} </th>
                                    <th style="text-align: left !important;" id="totalMoneyCheckagainD">{{number_format($totalMoneyCheckagainD)}}$</th>

                                    <th style="text-align: left !important;" id="totalMoneyOld"></th>
                                    <th style="text-align: left !important;" id="totalMoneyOldD"></th>


                                    <th class="background" id="totalFinnal"></th>
                                    <th class="background" id="totalFinnalD"></th>
                                </tr>
								
                            </thead>
                         </table>
                        <style>
                            .background{
                                background: #c7c7c7;
                            }
                            .blueColor{
                                color: #0000ff !important;
                                font-size: 12px !important;
                            }
                            .redColor{
                                color: #ff0000 !important;
                                font-size: 12px !important;
                            }
                        </style>
                    
					@endif

		    
		         </div>
		         <!-- end widget content -->
				 </div>
		        </div>
		        <!-- end widget div -->
				<input type='button' id='btnPrintDiv' value='Print'  style="float:right;">
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

	function printDiv() 
	{

		var divToPrint=document.getElementById('displayReportPrint');

		var newWin=window.open('','Print-Window');

		newWin.document.open();

		newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

		newWin.document.close();

		setTimeout(function(){newWin.close();},10);

	}

	$(document).ready(function() {

		
		
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
        });
        
        $(document).off('submit', '#filter').on('submit', '#filter', function(e){
			if (validate_form_main('#filter') == 0) {
				return true;
			}
			return false;
		});

		
		$(document).off('click', 'td.editable').on('click', 'td.editable', function(e){
			// alert('ok');
			$(this).find('.txteditor').show().focus();
			$(this).find('div').hide();
		});
		$(document).off('focusout', '.txteditor').on('focusout', '.txteditor', function(e){
			$(this).hide();
			$(this).parent().find('div').show();
		});

		$(document).off('change', '.txteditor').on('change', '.txteditor', function(e){
			var mainID = $(this);
			var value = $(this).val();
			var typeID = $(this).attr('type_id');
			var typeDate = $(this).attr('type_date');
			var crcy = $(this).attr('currency');
			var valDateID = $(this).attr('valDateID');
			var staffID = $(this).attr('staffID');

			var id = $(this).attr('id');
			var idLabel = $(this).parent().find('div').attr('id');
			console.log(id,idLabel);
			// return false;
			$.ajax({
				url: '/transction/updatemoneydata',
				type: 'GET',
				data: {value:value, typeID:typeID, typeDate:typeDate, crcy:crcy, valDateID:valDateID, staffID:staffID},
				success: function(data) {
					if(data.status=="success"){
						$("#"+id).attr('value',value);
						$("#"+idLabel).text(data.valueDisplay);
						$("#finalPayment_"+valDateID).text(data.finalPayment[0]);
						$("#finalPaymentD_"+valDateID).text(data.finalPayment[1]);

						
						
						console.log(data);

					}else{
					}
				}
			});


		});


		$(document).off('change', '#s_id_man').on('change', '#s_id_man', function(e){
			
			var id = $(this).val();
			$("#s_id").prop( "disabled", true );
			$.ajax({
				url: '/reportmainstaff/getstaffchild',
				type: 'GET',
				data: {s_id:id},
				success: function(data) {
					if(data.status=="success"){

						$("#s_id").html(data.msg);

						$("#s_id").prop( "disabled", false );

						console.log(data.msg);

					}else{
					}
				}
			});

		});

		$( "#btnPrintDiv" ).on( "click", function() {
			var divID = 'printElement';
			console.log('okokok');
			printDiv(divID);
		});

	@if(isset($reports))
		var date = '{{$var_dateStart}}';
		var staff = '{{$var_staff}}';
		var totalReal = {{$total2}};
		var totalDolla = {{$total3}};
			
		$.ajax({
			url: '../addmoney_block',
			type: 'GET',
			data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla},
			success: function(data) {
				console.log(data);
				if(data.status=="success"){
//							console.log(data.msg);
				}else{
//							console.log(data.msg);
				}
			}
		});
	@endif

	});


	//document.getElementById('btnPrintDiv').addEventListener ("click", print)
	/*function print() {
		printJS({
			printable: 'printElement',
			type: 'html',
			targetStyles: ['*']
		})
	}*/
	// Print.js
	//!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define("print-js",[],t):"object"==typeof exports?exports["print-js"]=t():e["print-js"]=t()}(this,function(){return function(e){function t(i){if(n[i])return n[i].exports;var o=n[i]={i:i,l:!1,exports:{}};return e[i].call(o.exports,o,o.exports,t),o.l=!0,o.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,i){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:i})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p="./",t(t.s=10)}([function(e,t,n){"use strict";function i(e,t){if(e.focus(),r.a.isEdge()||r.a.isIE())try{e.contentWindow.document.execCommand("print",!1,null)}catch(t){e.contentWindow.print()}r.a.isIE()||r.a.isEdge()||e.contentWindow.print(),r.a.isIE()&&"pdf"===t.type&&setTimeout(function(){e.parentNode.removeChild(e)},2e3),t.showModal&&a.a.close(),t.onLoadingEnd&&t.onLoadingEnd()}function o(e,t,n){void 0===e.naturalWidth||0===e.naturalWidth?setTimeout(function(){o(e,t,n)},500):i(t,n)}var r=n(1),a=n(3),d={send:function(e,t){document.getElementsByTagName("body")[0].appendChild(t);var n=document.getElementById(e.frameId);"pdf"===e.type&&(r.a.isIE()||r.a.isEdge())?n.setAttribute("onload",i(n,e)):t.onload=function(){if("pdf"===e.type)i(n,e);else{var t=n.contentWindow||n.contentDocument;t.document&&(t=t.document),t.body.innerHTML=e.htmlData,"image"===e.type?o(t.getElementById("printableImage"),n,e):i(n,e)}}}};t.a=d},function(e,t,n){"use strict";var i={isFirefox:function(){return"undefined"!=typeof InstallTrigger},isIE:function(){return-1!==navigator.userAgent.indexOf("MSIE")||!!document.documentMode},isEdge:function(){return!i.isIE()&&!!window.StyleMedia},isChrome:function(){return!!window.chrome&&!!window.chrome.webstore},isSafari:function(){return Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor")>0||-1!==navigator.userAgent.toLowerCase().indexOf("safari")}};t.a=i},function(e,t,n){"use strict";function i(e,t){return'<div style="font-family:'+t.font+" !important; font-size: "+t.font_size+' !important; width:100%;">'+e+"</div>"}function o(e){return e.charAt(0).toUpperCase()+e.slice(1)}function r(e,t){var n=document.defaultView||window,i=[],o="";if(n.getComputedStyle){i=n.getComputedStyle(e,"");var r=t.targetStyles||["border","box","break","text-decoration"],a=t.targetStyle||["clear","display","width","min-width","height","min-height","max-height"];t.honorMarginPadding&&r.push("margin","padding"),t.honorColor&&r.push("color");for(var d=0;d<i.length;d++)for(var l=0;l<r.length;l++)"*"!==r[l]&&-1===i[d].indexOf(r[l])&&-1===a.indexOf(i[d])||(o+=i[d]+":"+i.getPropertyValue(i[d])+";")}else if(e.currentStyle){i=e.currentStyle;for(var s in i)-1!==i.indexOf("border")&&-1!==i.indexOf("color")&&(o+=s+":"+i[s]+";")}return o+="max-width: "+t.maxWidth+"px !important;"+t.font_size+" !important;"}function a(e,t){for(var n=0;n<e.length;n++){var i=e[n],o=i.tagName;if("INPUT"===o||"TEXTAREA"===o||"SELECT"===o){var d=r(i,t),l=i.parentNode,s="SELECT"===o?document.createTextNode(i.options[i.selectedIndex].text):document.createTextNode(i.value),c=document.createElement("div");c.appendChild(s),c.setAttribute("style",d),l.appendChild(c),l.removeChild(i)}else i.setAttribute("style",r(i,t));var p=i.children;p&&p.length&&a(p,t)}}function d(e,t,n){var i=document.createElement("h1"),o=document.createTextNode(t);i.appendChild(o),i.setAttribute("style",n),e.insertBefore(i,e.childNodes[0])}t.a=i,t.b=o,t.c=r,t.d=a,t.e=d},function(e,t,n){"use strict";var i={show:function(e){var t=document.createElement("div");t.setAttribute("style","font-family:sans-serif; display:table; text-align:center; font-weight:300; font-size:30px; left:0; top:0;position:fixed; z-index: 9990;color: #0460B5; width: 100%; height: 100%; background-color:rgba(255,255,255,.9);transition: opacity .3s ease;"),t.setAttribute("id","printJS-Modal");var n=document.createElement("div");n.setAttribute("style","display:table-cell; vertical-align:middle; padding-bottom:100px;");var o=document.createElement("div");o.setAttribute("class","printClose"),o.setAttribute("id","printClose"),n.appendChild(o);var r=document.createElement("span");r.setAttribute("class","printSpinner"),n.appendChild(r);var a=document.createTextNode(e.modalMessage);n.appendChild(a),t.appendChild(n),document.getElementsByTagName("body")[0].appendChild(t),document.getElementById("printClose").addEventListener("click",function(){i.close()})},close:function(){var e=document.getElementById("printJS-Modal");e.parentNode.removeChild(e)}};t.a=i},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=n(7),o=i.a.init;"undefined"!=typeof window&&(window.printJS=o),t.default=o},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.getElementById(e.printable);if(!r)return window.console.error("Invalid HTML element id: "+e.printable),!1;var a=document.createElement("div");a.appendChild(r.cloneNode(!0)),a.setAttribute("style","display:none;"),a.setAttribute("id","printJS-html"),r.parentNode.appendChild(a),a=document.getElementById("printJS-html"),a.setAttribute("style",n.i(i.c)(a,e)+"margin:0 !important;");var d=a.children;n.i(i.d)(d,e),e.header&&n.i(i.e)(a,e.header,e.headerStyle),a.parentNode.removeChild(a),e.htmlData=n.i(i.a)(a.innerHTML,e),o.a.send(e,t)}}},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.createElement("img");r.src=e.printable,r.onload=function(){r.setAttribute("style","width:100%;"),r.setAttribute("id","printableImage");var a=document.createElement("div");a.setAttribute("style","width:100%"),a.appendChild(r),e.header&&n.i(i.e)(a,e.header,e.headerStyle),e.htmlData=a.outerHTML,o.a.send(e,t)}}}},function(e,t,n){"use strict";var i=n(1),o=n(3),r=n(9),a=n(5),d=n(6),l=n(8),s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},c=["pdf","html","image","json"];t.a={init:function(){var e={printable:null,fallbackPrintable:null,type:"pdf",header:null,headerStyle:"font-weight: 300;",maxWidth:800,font:"TimesNewRoman",font_size:"12pt",honorMarginPadding:!0,honorColor:!1,properties:null,gridHeaderStyle:"font-weight: bold;",gridStyle:"border: 1px solid lightgray; margin-bottom: -1px;",showModal:!1,onLoadingStart:null,onLoadingEnd:null,modalMessage:"Retrieving Document...",frameId:"printJS",htmlData:"",documentTitle:"Document",targetStyle:null,targetStyles:null},t=arguments[0];if(void 0===t)throw new Error("printJS expects at least 1 attribute.");switch(void 0===t?"undefined":s(t)){case"string":e.printable=encodeURI(t),e.fallbackPrintable=e.printable,e.type=arguments[1]||e.type;break;case"object":e.printable=t.printable,e.fallbackPrintable=void 0!==t.fallbackPrintable?t.fallbackPrintable:e.printable,e.type=void 0!==t.type?t.type:e.type,e.frameId=void 0!==t.frameId?t.frameId:e.frameId,e.header=void 0!==t.header?t.header:e.header,e.headerStyle=void 0!==t.headerStyle?t.headerStyle:e.headerStyle,e.maxWidth=void 0!==t.maxWidth?t.maxWidth:e.maxWidth,e.font=void 0!==t.font?t.font:e.font,e.font_size=void 0!==t.font_size?t.font_size:e.font_size,e.honorMarginPadding=void 0!==t.honorMarginPadding?t.honorMarginPadding:e.honorMarginPadding,e.properties=void 0!==t.properties?t.properties:e.properties,e.gridHeaderStyle=void 0!==t.gridHeaderStyle?t.gridHeaderStyle:e.gridHeaderStyle,e.gridStyle=void 0!==t.gridStyle?t.gridStyle:e.gridStyle,e.showModal=void 0!==t.showModal?t.showModal:e.showModal,e.onLoadingStart=void 0!==t.onLoadingStart?t.onLoadingStart:e.onLoadingStart,e.onLoadingEnd=void 0!==t.onLoadingEnd?t.onLoadingEnd:e.onLoadingEnd,e.modalMessage=void 0!==t.modalMessage?t.modalMessage:e.modalMessage,e.documentTitle=void 0!==t.documentTitle?t.documentTitle:e.documentTitle,e.targetStyle=void 0!==t.targetStyle?t.targetStyle:e.targetStyle,e.targetStyles=void 0!==t.targetStyles?t.targetStyles:e.targetStyles;break;default:throw new Error('Unexpected argument type! Expected "string" or "object", got '+(void 0===t?"undefined":s(t)))}if(!e.printable)throw new Error("Missing printable information.");if(!e.type||"string"!=typeof e.type||-1===c.indexOf(e.type.toLowerCase()))throw new Error("Invalid print type. Available types are: pdf, html, image and json.");e.showModal&&o.a.show(e),e.onLoadingStart&&e.onLoadingStart();var n=document.getElementById(e.frameId);n&&n.parentNode.removeChild(n);var p=void 0;switch(p=document.createElement("iframe"),p.setAttribute("style","display:none;"),p.setAttribute("id",e.frameId),"pdf"!==e.type&&(p.srcdoc="<html><head><title>"+e.documentTitle+"</title></head><body></body></html>"),e.type){case"pdf":if(i.a.isFirefox()||i.a.isEdge()||i.a.isIE()){window.open(e.fallbackPrintable,"_blank").focus(),e.showModal&&o.a.close(),e.onLoadingEnd&&e.onLoadingEnd()}else r.a.print(e,p);break;case"image":d.a.print(e,p);break;case"html":a.a.print(e,p);break;case"json":l.a.print(e,p);break;default:throw new Error("Invalid print type. Available types are: pdf, html, image and json.")}}}},function(e,t,n){"use strict";function i(e){var t=e.printable,i=e.properties,r='<div style="display:flex; flex-direction: column;">';r+='<div style="flex:1 1 auto; display:flex;">';for(var a=0;a<i.length;a++)r+='<div style="flex:1; padding:5px;'+e.gridHeaderStyle+'">'+n.i(o.b)(i[a])+"</div>";r+="</div>";for(var d=0;d<t.length;d++){r+='<div style="flex:1 1 auto; display:flex;">';for(var l=0;l<i.length;l++){var s=t[d],c=i[l].split(".");if(c.length>1)for(var p=0;p<c.length;p++)s=s[c[p]];else s=s[i[l]];r+='<div style="flex:1; padding:5px;'+e.gridStyle+'">'+s+"</div>"}r+="</div>"}return r+="</div>"}var o=n(2),r=n(0),a="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};t.a={print:function(e,t){if("object"!==a(e.printable))throw new Error("Invalid javascript data object (JSON).");if(!e.properties||"object"!==a(e.properties))throw new Error("Invalid properties array for your JSON data.");var d="";e.header&&(d+='<h1 style="'+e.headerStyle+'">'+e.header+"</h1>"),d+=i(e),e.htmlData=n.i(o.a)(d,e),r.a.send(e,t)}}},function(e,t,n){"use strict";function i(e,t){t.setAttribute("src",e.printable),r.a.send(e,t)}var o=n(1),r=n(0);t.a={print:function(e,t){if(e.showModal||e.onLoadingStart||o.a.isIE()){var n=new window.XMLHttpRequest;n.addEventListener("load",i(e,t)),n.open("GET",window.location.origin+"/"+e.printable,!0),n.send()}else i(e,t)}}},function(e,t,n){e.exports=n(4)}])});
	
	function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
		console.log(divElements,'in');
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;
        //Reset the page's HTML with div's HTML only
        document.body.innerHTML = 
          "<html><head><title></title></head><body>" + 
          divElements + "</body>";
        //Print Page
        window.print();
        //Restore orignal HTML
        document.body.innerHTML = oldPage;

    }
	</script>
@stop