@extends('master')
<?php
use App\Http\Controllers\SaleController;
?>
@section('title')
<title>របាយការណ៏</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:25px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:18px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			/*background-color: #dff3f5;*/
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: center;
		}

		.display_total_result tr td:nth-child(1) .cssInput{
			text-align: center;
		}

		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}

		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 4px 2px !important;
		}

		.bee_highlight.background th{
			background: #ddd;
		}
		.hidetableTD{
			display:none !important;
		}
        table.table-bordered.summary_result_per_day{
            border: 1px #fff solid !important;
        }

        .table-bordered>tbody>tr{
            border:none !important;
        }

        .table-bordered>thead th{
            background-image:none !important;
			background: #5698d2;
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td{
            border: 1px #000 solid !important;
            border-bottom: none !important;
        }
        .table-bordered>tbody>tr>th{
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td.clearBorder,.table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }
        .table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }

        .table-bordered>tbody>tr>th.result_right_total{
            border: none !important;
            border-bottom: 1px #000 solid !important;
        }

        /** {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }
        *:before,
        *:after {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }*/
        .table-bordered>tbody>tr>td.lr_clear{
            border-left: none !important;
            border-right: none !important;
        }
        .table-bordered>tbody>tr>th.bottom_clear{
            border-bottom: none !important;
        }
        
        .customFormpCode{
        	width: 80% !important;
        	height: auto !important;
        	text-align: center;
        }
        .customnumForm{
        	width: 40px !important;
        	height: auto !important;
        	text-align: center !important;
        	float: left;
        	padding: 5px 5px;

        }
        span.numSumP{
        	/*float: left;*/
        	/*padding-left: 25px;*/
        }

        .cssInput{
        	border-color: #FFF;
        	font-size: 16px;
        	padding: 0px 2px;
        }
        input.cssInput:focus{
        	border: 1px solid #ccc;
        }

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		
		    
		<!-- widget grid -->
		<section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div role="content">
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding" style="">


                 {!! Form::open(['route' => 'filterMain', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_staff_chail))
							 <?php $staff_chail_filter = $var_staff_chail; ?>
						 @else
							 <?php $staff_chail_filter = null; ?>
						 @endif

						 

						 @if(isset($var_dateEnd))
							 <?php $endDate_filter = $var_dateEnd; ?>
						 @else
							 <?php $endDate_filter = null; ?>
						 @endif

						
						 <div class="row">

							 <section class="col col-3">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>
							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateEnd", $value = $endDate_filter, $attributes = array('class' => 'form-control ', 'id' => 'dateEnd','placeholder'=> trans('result.dateEnd'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>


							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('s_id_man', ([
                                        '' => 'ឈ្មួញកណ្តាល' ]+$staffMain),$staff_filter,['class' => ' ','id'=>'s_id_man','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>
							 <section class="col col-2">
								 <label class="select">
								 	@if($staff_filter != '')
										{{ Form::select('s_id', ([
                                        '' => 'កូនក្រុម' ]+$chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@else
										{{ Form::select('s_id', ($chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@endif
									 <i></i>
								 </label>
							 </section>


							 <section class="col col-3">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
								 @if(Session::get('roleLot') != 100)
								 <label class="tesxt">
									 <button type="button" id="printAll" name="printAll" class="btn btn-primary btn-sm btn-filter">Print</button>
								 </label>
								 @endif
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 @if(isset($boss_data))

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 
							 .paperStyle{
								 display: none;
							 }
							 .paperStyle:last-child{
								 display: block !important;
							 }
							 .staff_name{
								 display: none;
							 }
							 
						 </style>



						 <div class="widget-body control_width">

							

							 <?php
							 $twodigit_charge = 0;
							 $threedigit_charge = 0;
							

							 $getStaffCharge = DB::table('tbl_staff_charge')
									 ->select('stc_id','stc_three_digit_charge','stc_two_digit_charge','stc_pay_two_digit','stc_pay_there_digit')
									 ->where('s_id',$var_staff_chail)
									 ->where('stc_date','<=',$var_dateStart)
									 ->orderBy('stc_date','DESC')
									 ->first();
								 //dd($getStaffCharge);
							 if(isset($getStaffCharge)){
								 $twodigit_charge = $getStaffCharge->stc_two_digit_charge;
								 $threedigit_charge = $getStaffCharge->stc_three_digit_charge;
								 if($twodigit_charge == $threedigit_charge){
								 	// $hidedata = 'hidetwothree_digit';
								 	// $sumHide = '';
								 	// $colspan = 1;

								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }else{
								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }

								 

                                if($getStaffCharge->stc_pay_two_digit > 0){
                                    $win_two_digit = $getStaffCharge->stc_pay_two_digit;
                                }else{
                                    $win_two_digit = 90;
                                }

                                if($getStaffCharge->stc_pay_there_digit > 0){
                                    $win_three_digit = $getStaffCharge->stc_pay_there_digit;
                                }else{
                                    $win_three_digit = 800;
                                }

							 }else{
							 	
                                $win_two_digit = 90;
                                $win_three_digit = 800;
							 }





							 ?>
							 
								 <div id="html-content-holder" class="widget-body">
								 	
									 
									 <div class="col-sm-3 col-md-3">
										 {{trans('label.staff_name')}} :
										 <b>{{$mainNameStaffInfo->s_name}} </b>
									 </div>

									 <div class="col-sm-6 col-md-6">
										 {{trans('label.date')}} :
										 <b><?php echo date("d-m-Y", strtotime($var_dateStart));?></b>
									 </div>
									 <br><br>
									 <table id="" class="table table-bordered summary_result_per_day" style="width:90%;">
										 <thead>
										 <tr>
										 	 <th>
										 	 	Lottery
										 	 </th>
											 <th colspan="3" class="pOwnerMan">
											 	ថ្ងៃទី 
										 		<b><?php echo date("d-m-Y", strtotime($var_dateStart));?></b>
											 </th>
											 <th colspan="{{$colspan}}">KHR</th>
											 <th class="win2DR" >ត្រូវ </th>
											 <th class="win3DR">ត្រូវ </th>
											 <th colspan="{{$colspan}}" class="moneyAllDD">USD</th>
											 <th class="win2DD">ត្រូវ</th>
											 <th class="win3DD">ត្រូវ</th>
										 </tr>
										 <tr>
											 <th style="width:10%;">{{trans('label.staff')}}</th>
											 <th style="width:5%;">វេន</th>
											 <th colspan="1" style="width:10%;" class="pOwner">
							 					ម្ចាស់ក្រដាស់
											 </th>
											 <th style="width:5%;">ល.រ</th>
											 <th class="{{$hidedata}}" style="width:10%;">2D</th>
											 <th class="{{$hidedata}}" style="width:10%;">3D</th>
											 <th class="{{$sumHide}}" style="width:10%;">លុយ</th>
											 <th class="win2DR" style="width:10%;">2D</th>
											 <th class="win3DR" style="width:10%;">3D</th>
											 <th class="{{$hidedata}} money2DD" style="width:10%;">2D</th>
											 <th class="{{$hidedata}} money3DD" style="width:10%;">3D</th>
											 <th class="{{$sumHide}} moneyAllDD" style="width:10%;">លុយ</th>
											 <th class="win2DD" style="width:10%;">2D</th>
											 <th class="win3DD" style="width:10%;">3D</th>
										 </tr>
										 
										 </thead>
										 <tbody class="display_total_result">
										 	<?php 
										 		$staff_n = '';
										 		$time_n = '';
										 		$checkTimePa = '';
										 		$countPaper = 0;
										 	?>
										 	@foreach($boss_data as $key => $data)
										 		<?php 

										 			if($key == 0){
										 				$checkTimePa = $data->time_value;
										 			}

							 						/* check % */
													 $staffCheck = DB::table('tbl_staff')->where('s_id', $data->id_staff)->first();
							 						/* var_dump($staffCheck->percent_data); */
											 		if($staff_n != $data->staff_value){
											 			$name_staff_display = $data->staff_value;
											 			$staff_n = $data->staff_value;

											 			$time_n = '';
											 		}else{
											 			$name_staff_display = '';
											 		}

											 		if($time_n != $data->time_value){
											 			$time_display = $data->time_value;

											 			$find = array("0","1","2","3");
														$replace = array("");
														$time_display = str_replace($find,$replace,$time_display);

											 			$time_n = $data->time_value;
											 		}else{
											 			$time_display = '';
											 		}
											 		// var_dump(number_format($data->total2digitr));
											 		// dd($main_new);
											 		
                                                    $cal_2_digit = $twodigit_charge;
                                                    $cal_3_digit = $threedigit_charge;


										 		?>
										 	@if($data->time_value != $checkTimePa )
										 		<?php 
										 			$find = array("0","1","2","3");
													$replace = array("");
													$timeFinal = str_replace($find,$replace,$checkTimePa);
													$displayPaperNum = $countPaper;
													$countPaper = 0;
										 		?>
										 		<tr>
										 			<td colspan="3" style="text-align:right !important; border-right:none !important; border-left:none !important;"></td>
										 			<td colspan="1" style="text-align:center !important;  border-left:none !important;  border-right:none !important;">{{$timeFinal}} {{$displayPaperNum}}</td>
										 			<td colspan="10" style="text-align:right !important; border-left:none !important; border-right:none !important;"></td>
										 		</tr>
										 	@endif
										 		<tr class="many_row">
										 			
										 			<td id="td_staff_{{$key}}" id_staff="{{$data->id_staff}}" two="{{$cal_2_digit}}" three="{{$cal_3_digit}}" percent="{{$staffCheck->percent_data}}" p_code="{{$data->p_code}}" align="center">
										 			
										 				@if(Session::get('roleLot') == 100)
															<span>{{$data->p_code_p}}</span>
														@else
															<input id="p_code_p{{$data->id}}" class="form-control cssInput customFormpCodep" placeholder="Code" name="p_code_p" type="text" value="{{$data->p_code_p}}" pageID="{{$data->id}}" date="{{$var_dateStart}}" oldData="{{$data->p_code_p}}" p_id="{{$data->p_id}}">
														@endif

										 			</td>
													<td id="td_time_'+index+'">{{$time_display}}</td>
													<td colspan="1" style="width:10%;" class="pOwner">
														@if(Session::get('roleLot') == 100)
															<span>{{$data->p_code}}</span>
														@else
															<input id="p_owner{{$data->id}}" class="form-control cssInput customFormpCode" placeholder="owner" name="p_owner" type="text" value="{{$data->p_code}}" pageID="{{$data->id}}" date="{{$var_dateStart}}" oldData="{{$data->p_code}}" p_id="{{$data->p_id}}">
														@endif
														
														
													</td>
													@if(Session::get('roleLot') == 100)
														<td id="td_page_'+index+'" style="width:5%; text-align: center !important;">
															<span>{{$data->page_value}}</span>
															<span class="numSumP">({{$key+1}})</span>
														</td>
													@else
													
													<td id="td_page_'+index+'" style="width:5%; text-align: left !important;">
															<input id="page_value{{$data->id}}" class="form-control customnumForm cssInput" placeholder="number" name="page_value" type="number" value="{{$data->page_value}}" oldData="{{$data->page_value}}" pageID="{{$data->id}}" date="{{$var_dateStart}}" p_id="{{$data->p_id}}">
															<span class="numSumP">({{$key+1}})</span>
													</td>
													@endif
													
										 			<td class="{{$hidedata}} total2digitr " >@if($data->total2digitr != 0) {!! floatval($data->total2digitr) !!} @endif</td>
										 			<td class="{{$hidedata}} total3digitr " >@if($data->total3digitr != 0) {!! floatval($data->total3digitr) !!} @endif</td>
										 			<td class="{{$sumHide}} totalsum " >@if($data->totalsum != 0) {{ floatval($data->totalsum) }} @endif</td>
										 			<td class="total2digitrright win2DR" style="width:300px;">@if($data->total2digitrright != 0) {!! floatval($data->total2digitrright) !!} @endif</td>
										 			<td class="total3digitrright win3DR">@if($data->total3digitrright != 0) {{ floatval($data->total3digitrright) }} @endif</td>
										 			<td class="{{$hidedata}} total2digits money2DD">@if($data->total2digits != 0) {{ floatval($data->total2digits) }} @endif</td>
										 			<td class="{{$hidedata}} total3digits money3DD">@if($data->total3digits != 0) {{ floatval($data->total3digits) }} @endif</td>
										 			<td class="{{$sumHide}} totalsum_dolla moneyAllDD">@if($data->totalsum_dolla != 0) {{ floatval($data->totalsum_dolla) }} @endif</td>
										 			<td class="total2digitsright win2DD">@if($data->total2digitsright != 0) {{ floatval($data->total2digitsright) }} @endif</td>
										 			<td class="total3digitsright win3DD">@if($data->total3digitsright != 0) {{ floatval($data->total3digitsright) }} @endif</td>

										 		</tr>
										 	

										 		<?php 
										 			$checkTimePa = $data->time_value;
										 			$countPaper++;
										 		?>

										 	@endforeach

										 		<?php 
										 			$find = array("0","1","2","3");
													$replace = array("");
													$timeFinal = str_replace($find,$replace,$checkTimePa);
										 		?>
										 		<tr>
										 			<td colspan="3" style="text-align:right !important; border-right:none !important; border-left:none !important;"></td>
										 			<td colspan="1" style="text-align:center !important;  border-left:none !important;  border-right:none !important;">{{$timeFinal}} {{$countPaper}}</td>
										 			<td colspan="10" style="text-align:right !important; border-left:none !important; border-right:none !important;"></td>
										 		</tr>
										 </tbody>
									 </table>
								</div>
						 </div>




					 @endif



		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
				<div style="text-align: center;">
				<input type='button' id='btnPrintDiv' value='Print'  style="float:none;">
				</div>
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	
	<script type="text/javascript">
	$(function () {
	    $("#btnPrintDiv").click(function () {
	    	var dateStart = $("#dateStart").val();
	    	var dateEnd = $("#dateEnd").val();
	    	var s_id_man = $("#s_id_man").val();
	    	var s_id = $("#s_id").val();
	    	var mainUrl = "{{URL::to('/')}}";
	    	// console.log(mainUrl);
	        var urlLoad = mainUrl+'/reportmainstaff/printdata?dateStart='+dateStart+'&dateEnd='+dateEnd+'&s_id_man='+s_id_man+'&s_id='+s_id;
	        window.open(
				  urlLoad,
				  '_blank' // <- This is what makes it open in a new window.
				);
	    });

	    $("#printAll").click(function () {
	    	var dateStart = $("#dateStart").val();
	    	var dateEnd = $("#dateEnd").val();
	    	var s_id_man = $("#s_id_man").val();
	    	var s_id = $("#s_id").val();
	    	var mainUrl = "{{URL::to('/')}}";
	    	// console.log(mainUrl);
	    	if(dateStart == '' && s_id_man == ''){
	    		alert("សូមបញ្ចូលថ្ងៃចាប់ផ្តើមនិងឈ្មោះកូនមុននិង Print");
	    	}else{
	    		var urlLoad = mainUrl+'/reportmainstaff/printdataall?dateStart='+dateStart+'&dateEnd='+dateEnd+'&s_id_man='+s_id_man+'&s_id='+s_id;
	        window.open(
				  urlLoad,
				  '_blank' // <- This is what makes it open in a new window.
				);
	    	}
	        
	    });
	});
	</script>

    


	<script type="text/javascript">

		


		$(document).off('submit', '#filter').on('submit', '#filter', function(e){
			if (validate_form_main('#filter') == 0) {
				return true;
			}
			return false;
		});

@if(isset($boss_data))

		function commaSeparateNumber(val){
			while (/(\d+)(\d{3})/.test(val.toString())){
				val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
			}
			return val;
		}

		function addCommas(nStr)
		{
		    nStr += '';
		    x = nStr.split('.');
		    x1 = x[0];
		    if(x[1]){
		    	xdot = x[1].toString().substring(0, 2)
		    }else{
		    	xdot = null;
		    }
		    x2 = x.length > 1 ? '.' + xdot : '';
		    var rgx = /(\d+)(\d{3})/;
		    while (rgx.test(x1)) {
		        x1 = x1.replace(rgx, '$1' + ',' + '$2');
		    }
		    return x1 + x2;
		}



		$(document).ready(function() {
			var total2digitr = '';
			var total2digits = '';
			var total3digitr = '';
			var total3digits = '';
			var total2digitrright = '';
			var total2digitsright = '';
			var total3digitrright = '';
			var total3digitsright = '';
			var time = '';
			var staff_list = '';
			var page = '';
			var tr = '';
			
			var sum_total2digitrright = 0;
			var sum_total2digitsright = 0;
			var sum_total3digitrright = 0;
			var sum_total3digitsright = 0;

			var num_total2digitrright = 0;
			var num_total2digitsright = 0;
			var num_total3digitrright = 0;
			var num_total3digitsright = 0;


			var twodigit_charge = <?php echo $twodigit_charge;?>;
			var threedigit_charge = <?php echo $threedigit_charge;?>;

			var sum_total2digitr = 0;
			var sum_total2digits = 0;
			var sum_total3digitr = 0;
			var sum_total3digits = 0;


			var sum_total2digitr_new = 0;
			var sum_total2digits_new = 0;
			var sum_total3digitr_new = 0;
			var sum_total3digits_new = 0;

			var sum2DigitBoosReal = 0;
			var sum3DigitBoosReal = 0;
			var sum2DigitBoosDolla = 0;
			var sum3DigitBoosDolla = 0;

			var sumPerMoney = 0;
			var sumPerMoney_D = 0;

			var data_array = new Array();

			var win2DR = 0;
			var win3DR = 0;

			var dolarFooter = 0;
			var money2DD = 0;
			var money3DD = 0;
			var moneyAllDD = 0;
			var win2DD = 0;
			var win3DD = 0;
			var pOwner = 0;

			var total_page_sum = 0;
			$(".many_row").each(function(index, element) {
				total_page_sum = total_page_sum + 1;

				

				total2digitr = $(this).find('.total2digitr').text();
				total2digits = $(this).find('.total2digits').text();
				total3digitr = $(this).find('.total3digitr').text();
				total3digits = $(this).find('.total3digits').text();
				total2digitrright = $(this).find('.total2digitrright').text();
				total2digitsright = $(this).find('.total2digitsright').text();
				total3digitrright = $(this).find('.total3digitrright').text();
				total3digitsright = $(this).find('.total3digitsright').text();

				var percent = $(this).find('#td_staff_'+index).attr('percent');
				var page_code = $(this).find('#td_staff_'+index).attr('p_code');

				var id_staff = $(this).find('#td_staff_'+index).attr('id_staff');
				var twodigit_charge = $(this).find('#td_staff_'+index).attr('two');
				var threedigit_charge = $(this).find('#td_staff_'+index).attr('three');
					// alert(id_staff);

				if(total2digitr==0){
					numtotal2digitr = 0;
					total2digitr = '';
				}else{
					numtotal2digitr = total2digitr;
					total2digitr = total2digitr;
				}

				if(total2digits==0){
					numtotal2digits = 0;
					total2digits = '';
				}else{
					numtotal2digits = total2digits;
					total2digits = total2digits;
				}

				if(total3digitr==0){
					numtotal3digitr = 0;
					total3digitr = '';
				}else{
					numtotal3digitr = total3digitr;
					total3digitr = total3digitr;
				}

				var totalsum = parseFloat(numtotal2digitr) + parseFloat(numtotal3digitr);
				if(totalsum==0){
					totalsum = '';
				}

				if(total3digits==0){
					numtotal3digits = 0;
					total3digits = '';
				}else{
					numtotal3digits = total3digits;
					total3digits = total3digits;
				}

				if(total2digitrright==0){
					num_total2digitrright = 0;
					total2digitrright = '';
				}else{
					num_total2digitrright = total2digitrright;
					total2digitrright = total2digitrright;

				}
				if(total2digitsright==0){
					num_total2digitsright = 0;
					total2digitsright = '';
				}else{
					num_total2digitsright = total2digitsright;
					total2digitsright = total2digitsright;
				}
				if(total3digitrright==0){
					num_total3digitrright = 0;
					total3digitrright = '';
				}else{
					num_total3digitrright = total3digitrright;
					total3digitrright = total3digitrright;
				}
				if(total3digitsright==0){
					num_total3digitsright = 0;
					total3digitsright = '';
				}else{
					num_total3digitsright = total3digitsright;
					total3digitsright = total3digitsright;
				}

				var totalsum_dolla = parseFloat(numtotal2digits) + parseFloat(numtotal3digits);
				if(totalsum_dolla==0){
					totalsum_dolla = '';
				}

				sum_total2digitr = sum_total2digitr + parseFloat(numtotal2digitr);
				sum_total2digits = sum_total2digits + parseFloat(numtotal2digits);
				sum_total3digitr = sum_total3digitr + parseFloat(numtotal3digitr);
				sum_total3digits = sum_total3digits + parseFloat(numtotal3digits);

				sum_total2digitrright = sum_total2digitrright + parseFloat(num_total2digitrright);
				sum_total2digitsright = sum_total2digitsright + parseFloat(num_total2digitsright);
				sum_total3digitrright = sum_total3digitrright + parseFloat(num_total3digitrright);
				sum_total3digitsright = sum_total3digitsright + parseFloat(num_total3digitsright);

				sum_total2digitr_new = sum_total2digitr_new + (
																Math.round(

																		((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100)
																	
																)
															);


				sum_total2digits_new = sum_total2digits_new + 

															(
																

																		((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100)
																	
																
															);


				sum2DigitBoosReal = sum2DigitBoosReal + (((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100));

				sum3DigitBoosReal = sum3DigitBoosReal + (((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100));

				sum2DigitBoosDolla = sum2DigitBoosDolla + (((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100));

				sum3DigitBoosDolla = sum3DigitBoosDolla + (((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100));
				
                
				if(percent > 0){
					var totalRIncom = (((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100)) + (((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100));
					var totalRExpan = (parseInt(num_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(num_total3digitrright * <?php echo $win_three_digit;?>) );
					var moneyData = ((totalRIncom-totalRExpan) *  percent)/100;
					console.log(totalRIncom,' Income Real');
					console.log(totalRExpan, 'Expan');
					
					sumPerMoney = sumPerMoney + moneyData;
					console.log(percent, 'percent');
					console.log(moneyData, 'Money');

					/* dolla */
					var totalDIncom = (((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100)) + (((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100));
					var totalDExpan = (parseInt(num_total2digitsright * <?php echo $win_two_digit;?>) + parseInt(num_total3digitsright * <?php echo $win_three_digit;?>) );
					var moneyData_D = ((totalDIncom-totalDExpan) *  percent)/100;
					sumPerMoney_D = sumPerMoney_D + moneyData_D;
				}
				
				/* check condition col */

				if(total2digitrright > 0){
					win2DR = win2DR + 1;
				}
				if(total3digitrright > 0){
					win3DR = win3DR + 1;
				}

				if(total2digits > 0){
					money2DD = money2DD + 1;
				}
				if(total3digits > 0){
					money3DD = money3DD + 1;
				}
				if(totalsum_dolla > 0){
					moneyAllDD = moneyAllDD + 1;
				}

				if(total2digitsright > 0){
					win2DD = win2DD + 1;
				}
				if(total3digitsright > 0){
					win3DD = win3DD + 1;
				}
				if(page_code != ''){
					pOwner = pOwner + 1;
				}
				
			});


			var total_income_expense_riel = 0;
			var total_income_expense_dollar = 0;
			
				
				
                var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
                var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
			



				// alert(sum_total2digitr_new);

				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				
			
			 // var a = Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100));
			 // var b = Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100));
			 // var aaaa = a + b;
			// alert( income_riel );
			var staff_id = "<?php echo $var_staff_chail;?>";
			
			
            var new_price = income_riel;
            var new_price_s = income_dollar;

			var total_income_expense_riel = income_riel - expense_riel;
			var check_total = total_income_expense_riel.toString().substr(-2);
            // console.log(total_income_expense_riel);
            // console.log(check_total);

            
         

			if(total_income_expense_riel>0){
				total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = total_income_expense_riel;
            }else{
            	total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = '('+total_income_expense_riel+')';
            }

			var total_income_expense_dollar = income_dollar - expense_dollar;
			var total_income_expense_dollar_noStr = total_income_expense_dollar;
            if(total_income_expense_dollar>0){
                total_income_expense_dollar = total_income_expense_dollar.toFixed(2);
            }else{
                total_income_expense_dollar = '('+total_income_expense_dollar.toFixed(2)+')';
            }

            var sum_totalall = sum_total2digitr + sum_total3digitr;
            var sum_totalall_dolla = sum_total2digits + sum_total3digits;


           

			<?php 
				$totalCal = $colspan + 1;
			?>

			

			tr = '<tr class="bee_highlight background"><th>{{trans('label.total')}}</th><th></th><th class="pOwner"></th><th >'+total_page_sum+'</th><th class="{{$hidedata}}">'+sum_total2digitr+'</th><th class="{{$hidedata}}">'+sum_total3digitr+'</th><th class="{{$sumHide}}">'+sum_totalall+'</th><th class="win2DR">'+sum_total2digitrright+'</th><th class="win3DR">'+sum_total3digitrright+'</th><th class="{{$hidedata}} money2DD">$ '+sum_total2digits+'</th><th class="{{$hidedata}} money3DD">$ '+sum_total3digits+'</th><th class="{{$sumHide}} moneyAllDD">$ '+sum_totalall_dolla+'</th><th class="win2DD">$ '+sum_total2digitsright+'</th><th class="win3DD">$ '+sum_total3digitsright+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class="bee_highlight"><td></td><td>{{trans('label.total_charge')}}</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)))+'</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitrright * 70)+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitrright * 600)+'</td><td>'+commaSeparateNumber(((parseInt(sum_total2digits) * parseInt(twodigit_charge)) / 100).toFixed(2))+'</td><td>'+commaSeparateNumber(((parseInt(sum_total3digits) * parseInt(threedigit_charge)) / 100).toFixed(2))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitsright * 70 )+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitsright * 600)+'</td><tr>';
			// $('.display_total_result').append(tr);
            var total_rate_thesame = parseInt(sum_total2digitr) + parseInt(sum_total3digitr);
            var total_rate_thesame_dolla = parseFloat(sum_total2digits) + parseFloat(sum_total3digits); 
			tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result').append(tr);

            var cospan23D = 3;
            if(win3DR>0){
                cospan23D = 4;
                
            }
			
			// 2D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th colspan="'+cospan23D+'" class="pOwnerManFooter" rowspan="2" style="vertical-align: middle; text-align: center;">ទឹក</th><th>2D</th><th>'+sum_total2digitr+'</th><th> => </th><th>'+Math.round(sum2DigitBoosReal)+'</th><th class="win3DR footer3D"></th><th class="footerDollar">$ '+sum_total2digits+'</th><th class="footerDollar"> => </th><th class="footerDollar">$ '+sum2DigitBoosDolla.toFixed(2)+'</th><th class="footerDollar"></th></tr>';
			$('.display_total_result').append(tr);
			// 3D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th>3D</th><th>'+sum_total3digitr+'</th><th> => </th><th>'+Math.round(sum3DigitBoosReal)+'</th><th class="win3DR footer3D"></th><th class="footerDollar">$ '+sum_total3digits+'</th><th class="footerDollar"> => </th><th class="footerDollar">$ '+sum3DigitBoosDolla.toFixed(2)+'</th><th class="footerDollar"></th></tr>';
			$('.display_total_result').append(tr);

			tr = '<tr class="bee_highlight {{$sumHide}}"><th colspan="'+(cospan23D-1)+'"  style="vertical-align: middle; text-align: center;">ទឹក</th><th>សរុប</th><th>'+total_rate_thesame+'</th><th> => </span></th><th>'+Math.round(sum_total2digitr_new)+'</th><th class="win3DR"></th><th class="footerDollar">$ '+total_rate_thesame_dolla+'</th><th class="footerDollar"> => </th><th class="footerDollar">$ '+sum_total2digits_new.toFixed(2)+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);



			
			
            // 2D(T)
            tr = '<tr class="bee_highlight"><th class="{{$hidedata}} pOwnerManFooter" colspan="'+cospan23D+'" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th class="{{$sumHide}}" colspan="2" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th>x<?php echo $win_two_digit;?></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th class="win3DR footer3D"></th><th class="footerDollar">'+sum_total2digitsright+'</th><th class="footerDollar">x<?php echo $win_two_digit;?></th><th class="footerDollar">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
            $('.display_total_result').append(tr);
            // 3D(T)
            tr = '<tr class="bee_highlight"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th>x<?php echo $win_three_digit;?></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th class="win3DR footer3D"></th><th class="footerDollar">'+sum_total3digitsright+'</th><th class="footerDollar">x<?php echo $win_three_digit;?></th><th class="footerDollar">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
            $('.display_total_result').append(tr);

			tr = '<tr class=""><td colspan="12" class="lr_clear"></td></tr>';
			$('.display_total_result').append(tr);


			// paid or borrow


			var totalRealBefore = total_income_expense_riel_noStr;
			var totalDollaBefore = total_income_expense_dollar_noStr;

			console.log(totalRealBefore,'Before R');
			console.log(sumPerMoney,'ok % R');

			console.log(totalDollaBefore,'Before D');
			console.log(sumPerMoney_D,' % D');

			// totalRealBefore = totalRealBefore - sumPerMoney;
			// totalDollaBefore = totalDollaBefore - sumPerMoney_D;
			
			console.log(totalRealBefore,'Ready R');
			console.log(totalDollaBefore,'Ready D');


			
			
			
			

			if(totalRealBefore < 0){
				var labelBefore_r = 'ខាត';
				var redColor = 'color: red;';
			}else{
				var labelBefore_r = 'សុី';
			}

			if(totalDollaBefore < 0){
				var labelBefore_d = 'ខាត';
				var redColorDolar = 'color: red;';
			}else{
				var labelBefore_d = 'សុី';
			}	

            var cospanTotal = 4;
            if(win3DR>0){
                cospanTotal = 5;
                
            }

            var newth = ''
            if(pOwner == 0){
				newth = '<th class="clearBorder"></th>';
			}


			// price before percent
			tr = '<tr class="bee_highlight ">'+newth+'<th class="clearBorder" colspan="{{$colspan}}"></th><th colspan="'+cospanTotal+'" class="pOwnerManFooter clearBorder" style="text-align:right !important; '+redColor+'">'+labelBefore_r+'</th><th class="result_right_total" style="text-align:center !important; '+redColor+'">'+addCommas(totalRealBefore)+' </th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th style="'+redColorDolar+'" class="footerDollar">'+labelBefore_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+addCommas(totalDollaBefore.toFixed(2))+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
			$('.display_total_result').append(tr);





			@if($percentStaff > 0)
				var percent_sub = parseFloat('{{$percentStaff}}');
				var percent = 100 - percent_sub;

				var totalReal = parseFloat(totalRealBefore * percent)/100;
				var totalDolla = parseFloat(totalDollaBefore * percent)/100;

				

				console.log(totalReal,'Testing');

				var redColorPer = '';
				if(totalReal < 0){
					var redColorPer = 'color: red;';
				}

				var redColorPerD = '';
				if(totalDolla < 0){
					var redColorPerD = 'color: red;';
				}



				// price after percent 
				// tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th class="clearBorder"></th><th class="clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:left !important;">'+percent_sub+'%</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(totalReal)+' </th><th></th><th></th><th>%</th><th class="result_right_total" style="text-align:left !important;">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColorPer+'">'+percent+'%</th><th class="result_right_total" style="text-align:center !important; '+redColorPer+'">'+addCommas(totalReal)+' </th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar">%</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorPerD+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
                $('.display_total_result').append(tr);

				// chail_id
				var totalReal_main = parseFloat(totalRealBefore * percent_sub)/100;
				var totalDolla_main = parseFloat(totalDollaBefore * percent_sub)/100;

				tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColorPer+'">'+percent_sub+'%</th><th class="result_right_total" style="text-align:center !important; '+redColorPer+'">'+addCommas(totalReal_main)+' </th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar">%</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorPerD+'">$ '+totalDolla_main.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
                $('.display_total_result').append(tr);

                totalRealBefore = totalReal;
				totalDollaBefore = totalDolla;


			@else

				var totalReal = totalRealBefore;
				var totalDolla = totalDollaBefore;

			@endif


			var displayMainTotal = 0;
			<?php 
				if (!empty($money_paid)) {
					foreach($money_paid as $key => $value) {
						if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23){
							$redColor = "";

                            if($value->st_type == 21 && $value->st_price_r < 0 ){
								$label_r = 'ខាតចាស់';
                                $redColor = "color: #ff0000;";
							}else{
								$label_r = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_d < 0 ){
								$label_d = 'ខាតចាស់';
                                $redColor = 'color: #ff0000;';
							}else{
								$label_d = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_r > 0 && $value->st_price_d > 0 ){
								$label_r = $value->pav_value;
								$label_d = '';
							}
							if($value->st_type == 21 && $value->st_price_r < 0 && $value->st_price_d < 0 ){
								$label_r = 'ខាតចាស់';
								$label_d = '';
                                $redColor = 'color: #ff0000;';
							}

			?>	
							// console.log(totalDolla);

							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							console.log(totalReal,subPrice_r,'new case');
							if(subPrice_r != ''){
								totalReal = totalReal + parseFloat(subPrice_r);
							}
							console.log(totalReal,'new case');

							if(subPrice_d != ''){
								totalDolla = totalDolla + parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							var displayPriceR = parseFloat('{{$value->st_price_r}}');
							var displayPriceD = parseFloat('{{$value->st_price_d}}');
							//tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">{{ $label_r }}</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(displayPriceR)+'</th><th></th><th></th><th>{{ $label_d }}</th><th class="result_right_total" style="text-align:left !important;" >'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
							//$('.display_total_result').append(tr);
                            tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important;">{{ $label_r }}</th><th class="result_right_total" style="text-align:center !important; {{$redColor}}">'+addCommas(displayPriceR)+'</th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar">{{ $label_d }}</th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}else{
			?>	
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';

							if(subPrice_r > 0){
								totalReal = totalReal - parseFloat(subPrice_r);
							}
							if(subPrice_d > 0){
								totalDolla = totalDolla - parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							
							var displayPriceR = parseFloat('{{$value->st_price_r * -1}}');
							var displayPriceD = parseFloat('{{$value->st_price_d * -1}}');
							//tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">{{ $value->pav_value }}</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(displayPriceR)+'</th><th></th><th></th><th></th><th class="result_right_total" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
							//$('.display_total_result').append(tr);
                            tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important;">{{ $value->pav_value }}</th><th class="result_right_total" style="text-align:center !important;">'+addCommas(displayPriceR)+'</th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar"></th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}
					}
				}
			?>
			// end paid or borrow
			if(displayMainTotal > 0){
				// total price
				
				tr = '<tr class=""><td colspan="11" class="clearBorder"></td></tr>';
				$('.display_total_result').append(tr);

				var redColor = '';
				var redColorDolar = '';

				if(totalReal > 0){
					var labelTotal = 'សុីសរុប'; 
				}else{
					var labelTotal = 'ខាតសរុប';
					var redColor = 'color: red;';
				}

				if(totalDolla > 0){
					var labelTotal_d = 'សុីសរុប'; 
				}else{
					var labelTotal_d = 'ខាតសរុប';
					var redColorDolar = 'color: red;';
				}

				if(totalReal >= 0 && totalDolla >= 0){
					var labelTotal = 'សុីសរុប'; 
					var labelTotal_d = ''; 
				}

				if(totalReal < 0 && totalDolla < 0){
					var labelTotal = 'ខាតសរុប'; 
					var labelTotal_d = ''; 
					var redColor = 'color: red;';
				}

				//tr = '<tr class="bee_highlight"><th class="{{$hidedata}}" ></th><th></th><th></th><th colspan="2" style="text-align:left !important; '+redColor+'">'+labelTotal+'</th><th class="result_right_total" style="text-align:left !important; '+redColor+'">'+addCommas(totalReal)+'</th><th></th><th></th><th style="'+redColorDolar+'">'+labelTotal_d+'</th><th class="result_right_total" style="text-align:left !important; '+redColorDolar+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
				//$('.display_total_result').append(tr);
				

                tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColor+'"><span class='+redColor+'>'+labelTotal+'</span></th><th class="result_right_total bottom_clear" style="text-align:center !important; '+redColor+'"><span class='+redColor+'>'+addCommas(totalReal)+'</span></th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th style="'+redColorDolar+'" class="footerDollar">'+labelTotal_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result').append(tr);


				// console.log(moneyAllDD,totalDolla,'test dollar');
				if(moneyAllDD == 0 && (totalDolla>0 || totalDolla<0)){

					
					tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColorDolar+'"><span class='+redColorDolar+'>'+labelTotal_d+'</span></th><th class="result_right_total bottom_clear" style="text-align:center !important; '+redColorDolar+'"><span class='+redColorDolar+'>$ '+totalDolla.toFixed(2)+'</span></th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th style="'+redColorDolar+'" class="footerDollar">'+labelTotal_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
					$('.display_total_result').append(tr);
				}

			}

			// $(".report_total_info").each(function(index, element) {

			// 	staff_list = $(this).attr('staff');
			// 	id_staff = $(this).attr('id_staff');

			// 	time = $(this).attr('time');
			// 	page = $(this).attr('page');
			// 	$('.display_total_result').find('#td_staff_com_'+index).text(staff_list);
			// 	$('.display_total_result').find('#td_time_com_'+index).text(time);
			// 	$('.display_total_result').find('#td_page_com_'+index).text(page);

			// });

			var date = '{{$var_dateStart}}';
			var staff = '{{$var_staff_chail}}';
			<?php 
			if($checkinView == 1 && Session::get('roleLot') != 100){
			?>
			$.ajax({
				url: '../addmoney',
				type: 'GET',
				data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla,totalRealBefore:totalRealBefore, totalDollaBefore:totalDollaBefore},
				success: function(data) {
					console.log(data);
					if(data.status=="success"){
//							console.log(data.msg);
					}else{
//							console.log(data.msg);
					}
				}
			});

			
			
			<?php 
			}
			?>

			<?php 
			if($checkinViewKh == 1 && Session::get('roleLot') != 100){
			?>	
				// console.log("{{$checkinViewKh}}");
				$.ajax({
					url: '../addmoney',
					type: 'GET',
					data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla,totalRealBefore:totalRealBefore, totalDollaBefore:totalDollaBefore},
					success: function(data) {
						if(data.status=="success"){
//							console.log(data.msg);
						}else{
//							console.log(data.msg);
						}
					}
				});

				
			<?php 
			}
			?>

			<?php 
				if(Session::get('roleLot') != 100){
			?>

				$.ajax({
					url: '../../report/summary_lottery',
					type: 'GET',
					data: {date:date,staff:staff,income_riel:income_riel,income_dollar:income_dollar,expense_riel:expense_riel,expense_dollar:expense_dollar,var_type_lottery:1},
					success: function(data) {
						if(data.status=="success"){
							console.log(data.msg,'summary_lottery');
						}else{
//							console.log(data.msg);
						}
					}
				});

				$.ajax({
					url: '../../report/summary_lottery_total',
					type: 'GET',
					data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:1},
					success: function(data) {
						if(data.status=="success"){
							console.log(data.msg,'summary_lottery_total');
						}else{
//							console.log(data.msg);
						}
					}
				});

				$.ajax({
					url: '../../report/addtotalmoney',
					type: 'GET',
					data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:1},
					success: function(data) {
						if(data.status=="success"){
								console.log(data.msg,'addtotalmoney');
						}else{
	//							console.log(data.msg);
						}
					}
				});

			<?php 
			}
			?>


			if(win3DR == 0){
				$(".win3DR").addClass('hidetableTD');
			}else{
                $(".win3DR.footer3D").addClass('hidetableTD');
            }

			if(moneyAllDD == 0){
				console.log(moneyAllDD,' moneyAllDD');
				console.log(money2DD,' money2DD');
				console.log(money3DD,' money3DD');
				console.log(win2DD,' money2DD');
				console.log(win3DD,' money3DD');
				$(".moneyAllDD").addClass('hidetableTD');
				$(".money2DD").addClass('hidetableTD');
				$(".money3DD").addClass('hidetableTD');
				$(".win2DD").addClass('hidetableTD');
				$(".win3DD").addClass('hidetableTD');
				$("th.footerDollar").addClass('hidetableTD');
			}

			if(pOwner == 0){

				var numSpan = 3;
				var numSpanTotal = 4;
				if(win3DR == 0 && win2DR == 0){
					numSpan = 2;
					numSpanTotal = 3;
				}else if(win2DR > 0 && win3DR > 0){

	            }else if(win2DR > 0){
	            	numSpan = 2;
	            }
	            console.log(win2DR,'win2DR');

				$(".pOwner").addClass('hidetableTD');
				$(".pOwnerMan").attr('colspan',2);
				$(".pOwnerManFour").attr('colspan',3);
				$(".pOwnerManFooter").attr('colspan',numSpan);
				
				$(".numSumP").hide();
			}





		});

@endif
       
	$(document).ready(function() {
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
		});
	});
	$(document).ready(function(){

        $(document).off('change', '.customFormpCodep').on('change', '.customFormpCodep', function(e){
			
			var codeVal = $(this).val();
			var pIDList = $(this).attr('pageID');
			var pDate = $(this).attr('date');
			var thisID = $(this);
			var oldData = $(this).attr('oldData');
			var p_id = $(this).attr('p_id');
			
			if(codeVal != ''){
				$.ajax({
					url: '/reportmainstaff/updatecodep',
					type: 'GET',
					data: {codeVal:codeVal, pIDList:pIDList, pDate:pDate, p_id:p_id},
					success: function(data) {
						if(data.status=="success"){

							thisID.val(data.data);
							thisID.attr('oldData',data.data);


							console.log(data.msg);

						}else{
						}
					}
				});
			}else{
				thisID.val(oldData);
			}
			
		});

		$(document).off('change', '.customFormpCode').on('change', '.customFormpCode', function(e){
			
			var codeVal = $(this).val();
			var pIDList = $(this).attr('pageID');
			var pDate = $(this).attr('date');
			var thisID = $(this);
			var oldData = $(this).attr('oldData');
			var p_id = $(this).attr('p_id');
			
			if(codeVal != ''){
				$.ajax({
					url: '/reportmainstaff/updatecode',
					type: 'GET',
					data: {codeVal:codeVal, pIDList:pIDList, pDate:pDate, p_id:p_id},
					success: function(data) {
						if(data.status=="success"){

							thisID.val(data.data);
							thisID.attr('oldData',data.data);


							console.log(data.msg);

						}else{
						}
					}
				});
			}else{
				thisID.val(oldData);
			}
			
		});

		$(document).off('change', '.customnumForm').on('change', '.customnumForm', function(e){
			
			var codeVal = $(this).val();
			var pIDList = $(this).attr('pageID');
			var pDate = $(this).attr('date');
			var thisID = $(this);
			var oldData = $(this).attr('oldData');
			var p_id = $(this).attr('p_id');

			if(codeVal != ''){
				$.ajax({
					url: '/reportmainstaff/updatenumber',
					type: 'GET',
					data: {codeVal:codeVal, pIDList:pIDList, pDate:pDate, p_id:p_id},
					success: function(data) {
						if(data.status=="success"){

							thisID.val(data.data);
							thisID.attr('oldData',data.data);


							console.log(data.msg);

						}else{
						}
					}
				});
			}else{
				thisID.val(oldData);
			}
			
		});

		$(document).off('change', '#s_id_man').on('change', '#s_id_man', function(e){
			
			var id = $(this).val();
			$("#s_id").prop( "disabled", true );
			$.ajax({
				url: '/reportmainstaff/getstaffchild',
				type: 'GET',
				data: {s_id:id},
				success: function(data) {
					if(data.status=="success"){

						$("#s_id").html(data.msg);

						$("#s_id").prop( "disabled", false );

						console.log(data.msg);

					}else{
					}
				}
			});
            
			

		});

		$('.btn_print').click(function(){
			var attr_id = $(this).attr('attr_id');
			var attr_date = $(this).attr('attr_date');
			var attr_staff = $(this).attr('attr_staff');
			var attr_sheet = $(this).attr('attr_sheet');
			if(attr_sheet==''){
				attr_sheet = 0;
			}
			var attr_page = $(this).attr('attr_page');
			if(attr_page==''){
				attr_page = 0;
			}
			var form_upload = $('#frmupload');
			var fram_upload = $('<iframe style="border:0px; width:0px; height:0px; opacity: 0px;" src="http://188.166.234.165/lottery/screenshot/download.php?attr_id='+attr_id+'&attr_date='+attr_date+'&attr_staff='+attr_staff+'&attr_sheet='+attr_sheet+'&attr_page='+attr_page+'" id="framupload" name="framupload"></iframe>');

			$('body').append(fram_upload);

		});
	});

	// document.getElementById('btnPrintDiv').addEventListener ("click", print)
	// function print() {
	// 	printJS({
	// 		printable: 'html-content-holder',
	// 		type: 'html',
	// 		targetStyles: ['*']
	// 	})
	// }
	// Print.js
	!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define("print-js",[],t):"object"==typeof exports?exports["print-js"]=t():e["print-js"]=t()}(this,function(){return function(e){function t(i){if(n[i])return n[i].exports;var o=n[i]={i:i,l:!1,exports:{}};return e[i].call(o.exports,o,o.exports,t),o.l=!0,o.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,i){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:i})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p="./",t(t.s=10)}([function(e,t,n){"use strict";function i(e,t){if(e.focus(),r.a.isEdge()||r.a.isIE())try{e.contentWindow.document.execCommand("print",!1,null)}catch(t){e.contentWindow.print()}r.a.isIE()||r.a.isEdge()||e.contentWindow.print(),r.a.isIE()&&"pdf"===t.type&&setTimeout(function(){e.parentNode.removeChild(e)},2e3),t.showModal&&a.a.close(),t.onLoadingEnd&&t.onLoadingEnd()}function o(e,t,n){void 0===e.naturalWidth||0===e.naturalWidth?setTimeout(function(){o(e,t,n)},500):i(t,n)}var r=n(1),a=n(3),d={send:function(e,t){document.getElementsByTagName("body")[0].appendChild(t);var n=document.getElementById(e.frameId);"pdf"===e.type&&(r.a.isIE()||r.a.isEdge())?n.setAttribute("onload",i(n,e)):t.onload=function(){if("pdf"===e.type)i(n,e);else{var t=n.contentWindow||n.contentDocument;t.document&&(t=t.document),t.body.innerHTML=e.htmlData,"image"===e.type?o(t.getElementById("printableImage"),n,e):i(n,e)}}}};t.a=d},function(e,t,n){"use strict";var i={isFirefox:function(){return"undefined"!=typeof InstallTrigger},isIE:function(){return-1!==navigator.userAgent.indexOf("MSIE")||!!document.documentMode},isEdge:function(){return!i.isIE()&&!!window.StyleMedia},isChrome:function(){return!!window.chrome&&!!window.chrome.webstore},isSafari:function(){return Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor")>0||-1!==navigator.userAgent.toLowerCase().indexOf("safari")}};t.a=i},function(e,t,n){"use strict";function i(e,t){return'<div style="font-family:'+t.font+" !important; font-size: "+t.font_size+' !important; width:100%;">'+e+"</div>"}function o(e){return e.charAt(0).toUpperCase()+e.slice(1)}function r(e,t){var n=document.defaultView||window,i=[],o="";if(n.getComputedStyle){i=n.getComputedStyle(e,"");var r=t.targetStyles||["border","box","break","text-decoration"],a=t.targetStyle||["clear","display","width","min-width","height","min-height","max-height"];t.honorMarginPadding&&r.push("margin","padding"),t.honorColor&&r.push("color");for(var d=0;d<i.length;d++)for(var l=0;l<r.length;l++)"*"!==r[l]&&-1===i[d].indexOf(r[l])&&-1===a.indexOf(i[d])||(o+=i[d]+":"+i.getPropertyValue(i[d])+";")}else if(e.currentStyle){i=e.currentStyle;for(var s in i)-1!==i.indexOf("border")&&-1!==i.indexOf("color")&&(o+=s+":"+i[s]+";")}return o+="max-width: "+t.maxWidth+"px !important;"+t.font_size+" !important;"}function a(e,t){for(var n=0;n<e.length;n++){var i=e[n],o=i.tagName;if("INPUT"===o||"TEXTAREA"===o||"SELECT"===o){var d=r(i,t),l=i.parentNode,s="SELECT"===o?document.createTextNode(i.options[i.selectedIndex].text):document.createTextNode(i.value),c=document.createElement("div");c.appendChild(s),c.setAttribute("style",d),l.appendChild(c),l.removeChild(i)}else i.setAttribute("style",r(i,t));var p=i.children;p&&p.length&&a(p,t)}}function d(e,t,n){var i=document.createElement("h1"),o=document.createTextNode(t);i.appendChild(o),i.setAttribute("style",n),e.insertBefore(i,e.childNodes[0])}t.a=i,t.b=o,t.c=r,t.d=a,t.e=d},function(e,t,n){"use strict";var i={show:function(e){var t=document.createElement("div");t.setAttribute("style","font-family:sans-serif; display:table; text-align:center; font-weight:300; font-size:30px; left:0; top:0;position:fixed; z-index: 9990;color: #0460B5; width: 100%; height: 100%; background-color:rgba(255,255,255,.9);transition: opacity .3s ease;"),t.setAttribute("id","printJS-Modal");var n=document.createElement("div");n.setAttribute("style","display:table-cell; vertical-align:middle; padding-bottom:100px;");var o=document.createElement("div");o.setAttribute("class","printClose"),o.setAttribute("id","printClose"),n.appendChild(o);var r=document.createElement("span");r.setAttribute("class","printSpinner"),n.appendChild(r);var a=document.createTextNode(e.modalMessage);n.appendChild(a),t.appendChild(n),document.getElementsByTagName("body")[0].appendChild(t),document.getElementById("printClose").addEventListener("click",function(){i.close()})},close:function(){var e=document.getElementById("printJS-Modal");e.parentNode.removeChild(e)}};t.a=i},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=n(7),o=i.a.init;"undefined"!=typeof window&&(window.printJS=o),t.default=o},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.getElementById(e.printable);if(!r)return window.console.error("Invalid HTML element id: "+e.printable),!1;var a=document.createElement("div");a.appendChild(r.cloneNode(!0)),a.setAttribute("style","display:none;"),a.setAttribute("id","printJS-html"),r.parentNode.appendChild(a),a=document.getElementById("printJS-html"),a.setAttribute("style",n.i(i.c)(a,e)+"margin:0 !important;");var d=a.children;n.i(i.d)(d,e),e.header&&n.i(i.e)(a,e.header,e.headerStyle),a.parentNode.removeChild(a),e.htmlData=n.i(i.a)(a.innerHTML,e),o.a.send(e,t)}}},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.createElement("img");r.src=e.printable,r.onload=function(){r.setAttribute("style","width:100%;"),r.setAttribute("id","printableImage");var a=document.createElement("div");a.setAttribute("style","width:100%"),a.appendChild(r),e.header&&n.i(i.e)(a,e.header,e.headerStyle),e.htmlData=a.outerHTML,o.a.send(e,t)}}}},function(e,t,n){"use strict";var i=n(1),o=n(3),r=n(9),a=n(5),d=n(6),l=n(8),s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},c=["pdf","html","image","json"];t.a={init:function(){var e={printable:null,fallbackPrintable:null,type:"pdf",header:null,headerStyle:"font-weight: 300;",maxWidth:800,font:"TimesNewRoman",font_size:"12pt",honorMarginPadding:!0,honorColor:!1,properties:null,gridHeaderStyle:"font-weight: bold;",gridStyle:"border: 1px solid lightgray; margin-bottom: -1px;",showModal:!1,onLoadingStart:null,onLoadingEnd:null,modalMessage:"Retrieving Document...",frameId:"printJS",htmlData:"",documentTitle:"Document",targetStyle:null,targetStyles:null},t=arguments[0];if(void 0===t)throw new Error("printJS expects at least 1 attribute.");switch(void 0===t?"undefined":s(t)){case"string":e.printable=encodeURI(t),e.fallbackPrintable=e.printable,e.type=arguments[1]||e.type;break;case"object":e.printable=t.printable,e.fallbackPrintable=void 0!==t.fallbackPrintable?t.fallbackPrintable:e.printable,e.type=void 0!==t.type?t.type:e.type,e.frameId=void 0!==t.frameId?t.frameId:e.frameId,e.header=void 0!==t.header?t.header:e.header,e.headerStyle=void 0!==t.headerStyle?t.headerStyle:e.headerStyle,e.maxWidth=void 0!==t.maxWidth?t.maxWidth:e.maxWidth,e.font=void 0!==t.font?t.font:e.font,e.font_size=void 0!==t.font_size?t.font_size:e.font_size,e.honorMarginPadding=void 0!==t.honorMarginPadding?t.honorMarginPadding:e.honorMarginPadding,e.properties=void 0!==t.properties?t.properties:e.properties,e.gridHeaderStyle=void 0!==t.gridHeaderStyle?t.gridHeaderStyle:e.gridHeaderStyle,e.gridStyle=void 0!==t.gridStyle?t.gridStyle:e.gridStyle,e.showModal=void 0!==t.showModal?t.showModal:e.showModal,e.onLoadingStart=void 0!==t.onLoadingStart?t.onLoadingStart:e.onLoadingStart,e.onLoadingEnd=void 0!==t.onLoadingEnd?t.onLoadingEnd:e.onLoadingEnd,e.modalMessage=void 0!==t.modalMessage?t.modalMessage:e.modalMessage,e.documentTitle=void 0!==t.documentTitle?t.documentTitle:e.documentTitle,e.targetStyle=void 0!==t.targetStyle?t.targetStyle:e.targetStyle,e.targetStyles=void 0!==t.targetStyles?t.targetStyles:e.targetStyles;break;default:throw new Error('Unexpected argument type! Expected "string" or "object", got '+(void 0===t?"undefined":s(t)))}if(!e.printable)throw new Error("Missing printable information.");if(!e.type||"string"!=typeof e.type||-1===c.indexOf(e.type.toLowerCase()))throw new Error("Invalid print type. Available types are: pdf, html, image and json.");e.showModal&&o.a.show(e),e.onLoadingStart&&e.onLoadingStart();var n=document.getElementById(e.frameId);n&&n.parentNode.removeChild(n);var p=void 0;switch(p=document.createElement("iframe"),p.setAttribute("style","display:none;"),p.setAttribute("id",e.frameId),"pdf"!==e.type&&(p.srcdoc="<html><head><title>"+e.documentTitle+"</title></head><body></body></html>"),e.type){case"pdf":if(i.a.isFirefox()||i.a.isEdge()||i.a.isIE()){window.open(e.fallbackPrintable,"_blank").focus(),e.showModal&&o.a.close(),e.onLoadingEnd&&e.onLoadingEnd()}else r.a.print(e,p);break;case"image":d.a.print(e,p);break;case"html":a.a.print(e,p);break;case"json":l.a.print(e,p);break;default:throw new Error("Invalid print type. Available types are: pdf, html, image and json.")}}}},function(e,t,n){"use strict";function i(e){var t=e.printable,i=e.properties,r='<div style="display:flex; flex-direction: column;">';r+='<div style="flex:1 1 auto; display:flex;">';for(var a=0;a<i.length;a++)r+='<div style="flex:1; padding:5px;'+e.gridHeaderStyle+'">'+n.i(o.b)(i[a])+"</div>";r+="</div>";for(var d=0;d<t.length;d++){r+='<div style="flex:1 1 auto; display:flex;">';for(var l=0;l<i.length;l++){var s=t[d],c=i[l].split(".");if(c.length>1)for(var p=0;p<c.length;p++)s=s[c[p]];else s=s[i[l]];r+='<div style="flex:1; padding:5px;'+e.gridStyle+'">'+s+"</div>"}r+="</div>"}return r+="</div>"}var o=n(2),r=n(0),a="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};t.a={print:function(e,t){if("object"!==a(e.printable))throw new Error("Invalid javascript data object (JSON).");if(!e.properties||"object"!==a(e.properties))throw new Error("Invalid properties array for your JSON data.");var d="";e.header&&(d+='<h1 style="'+e.headerStyle+'">'+e.header+"</h1>"),d+=i(e),e.htmlData=n.i(o.a)(d,e),r.a.send(e,t)}}},function(e,t,n){"use strict";function i(e,t){t.setAttribute("src",e.printable),r.a.send(e,t)}var o=n(1),r=n(0);t.a={print:function(e,t){if(e.showModal||e.onLoadingStart||o.a.isIE()){var n=new window.XMLHttpRequest;n.addEventListener("load",i(e,t)),n.open("GET",window.location.origin+"/"+e.printable,!0),n.send()}else i(e,t)}}},function(e,t,n){e.exports=n(4)}])});
	
	</script>
@stop