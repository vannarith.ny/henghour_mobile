<?php
use App\Http\Controllers\SaleController;

// dd($chilStaffs);
?>

<!DOCTYPE html>
<html lang="en-us">
	<head>
		
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo asset("css/bootstrap.min.css"); ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo asset("css/stylePrint.css"); ?>">
		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="<?php echo asset("js/libs/jquery-2.0.2.min.js"); ?>"></script>

		<script src="{{ asset('/') }}js/custom.js"></script>
		<script type="text/javascript">
			function commaSeparateNumber(val){
				while (/(\d+)(\d{3})/.test(val.toString())){
					val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
				}
				return val;
			}

			function addCommas(nStr)
			{
			    nStr += '';
			    x = nStr.split('.');
			    x1 = x[0];
			    if(x[1]){
			    	xdot = x[1].toString().substring(0, 2)
			    }else{
			    	xdot = null;
			    }
			    x2 = x.length > 1 ? '.' + xdot : '';
			    var rgx = /(\d+)(\d{3})/;
			    while (rgx.test(x1)) {
			        x1 = x1.replace(rgx, '$1' + ',' + '$2');
			    }
			    return x1 + x2;
			}
		</script>
		

	</head>

	

<body class="">
		    
<div id="html-content-holder-main" class="">
@foreach($chilStaffs as $key => $staffRow)
@if($staffRow->s_id)
	<?php 
	// var_dump($staffRow);

	$boss_data = DB::table('summary_lottery_page')
                    // ->select(DB::raw('p_code, CAST(p_code as SIGNED) AS casted_column, id,type_lottery,date_lottery,staff_value,id_staff,time_value,page_value,total2digitr,total3digitr,totalsum,total2digitrright,total3digitrright,total2digits,total3digits,totalsum_dolla,total2digitsright,total3digitsright,total1digitrright,total4digitrright,total1digitsright,total4digitsright,p_id'))
                    ->where('date_lottery',date("d-m-Y", strtotime($var_dateStart)))
                    ->whereRaw('id_staff IN ('.$staffRow->s_id.')')
                    // ->orderBy('id_staff' ,'ASC')
                    ->orderBy('staff_value' ,'ASC')
                    ->orderBy('time_value' ,'ASC')
                    // ->orderBy('casted_column' ,'ASC')
                    ->orderByRaw('LENGTH(p_code)')
                    ->orderBy('p_code')
                    ->orderByRaw('LENGTH(page_value)')
                    ->orderBy('page_value','ASC')
                    ->get(); 
    // var_dump(count($boss_data));

    if(count($boss_data)>0){

    	$var_staff_chail = $staffRow->s_id;
    	$mainNameStaffInfo = DB::table('tbl_staff')
                    ->where('s_id', $var_staff_chail)
                    ->first();
                // dd($var_staff_chail);
        // check paid or borrow and old price
        $parameterMoney =array();
        $checkreport = DB::table('tbl_staff')->where('s_id',$var_staff_chail)->first();
        $checkinView = $checkreport->payment;
        $checkinViewKh = $checkreport->paymentkh;

        // get percent
        $percentStaff = $checkreport->percent_data;
        $staffInfo = $checkreport;

        // dd($checkreport->payment);
        if($checkreport->payment == 1 || $checkreport->paymentkh == 1){

            $take_last = DB::table('tbl_staff_transction')
            ->where('s_id',$var_staff_chail)
            ->where('st_date_search','<',$var_dateStart)
            ->orderBy('st_date_search','DESC')->first();

            // dd($take_last->st_date_search);
            if($take_last){
                $money_paid = DB::table('tbl_staff_transction')
                        ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                        ->where('s_id',$var_staff_chail)
                        ->where('st_date_search','=',$take_last->st_date_search)
                        ->where('st_date_search','<',$var_dateStart)
                        // ->where('st_type','<>','21')
                        ->get();
                // dd($money_paid);
            }else{
            $money_paid = []; 
            }
            // dd($take_last->st_date_diposit);
            // transction money
            // dd($money_paid);


            $old_money = DB::table('tbl_story_payment')->where('s_id',$var_staff_chail)->orderBy('id', 'desc')->first();

        }else{
            $money_paid = [];
            $old_money = [];
        }

    }

	?>
		<!-- widget content -->
		@if(isset($boss_data) && count($boss_data) > 0)

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 
							 .paperStyle{
								 display: none;
							 }
							 .paperStyle:last-child{
								 display: block !important;
							 }
							 .staff_name{
								 display: none;
							 }
							 
						 </style>

						 <div class="" style="">

							

							 <?php
							 $twodigit_charge = 0;
							 $threedigit_charge = 0;
							

							 $getStaffCharge = DB::table('tbl_staff_charge')
									 ->select('stc_id','stc_three_digit_charge','stc_two_digit_charge','stc_pay_two_digit','stc_pay_there_digit')
									 ->where('s_id',$var_staff_chail)
									 ->where('stc_date','<=',$var_dateStart)
									 ->orderBy('stc_date','DESC')
									 ->first();
								 // dd($getStaffCharge);
							 if(isset($getStaffCharge)){
								 $twodigit_charge = $getStaffCharge->stc_two_digit_charge;
								 $threedigit_charge = $getStaffCharge->stc_three_digit_charge;
								 if($twodigit_charge == $threedigit_charge){
								 	// $hidedata = 'hidetwothree_digit';
								 	// $sumHide = '';
								 	// $colspan = 1;

								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }else{
								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }

								 

                                if($getStaffCharge->stc_pay_two_digit > 0){
                                    $win_two_digit = $getStaffCharge->stc_pay_two_digit;
                                }else{
                                    $win_two_digit = 90;
                                }

                                if($getStaffCharge->stc_pay_there_digit > 0){
                                    $win_three_digit = $getStaffCharge->stc_pay_there_digit;
                                }else{
                                    $win_three_digit = 800;
                                }

							 }else{
							 	
                                $win_two_digit = 90;
                                $win_three_digit = 800;
							 }





							 ?>
							 
								 	
									 
									 <div class="col-md-12">
										 {{trans('label.staff_name')}} :
										 <b>{{$mainNameStaffInfo->s_name}} </b>
										 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 {{trans('label.date')}} :
										 <b><?php echo date("d-m-Y", strtotime($var_dateStart));?></b>
									 </div>

									 <br><br>
									 <table id="summary_result_per_day_{{$var_staff_chail}}" class="table table-bordered summary_result_per_day" style="width:100%;">
										 <!-- <thead> -->
										 <tr class="thead">
										 	 <th>
										 	 	Lottery
										 	 </th>
											 <th colspan="3" class="pOwnerMan">
											 	ថ្ងៃទី 
										 		<b><?php echo date("d-m-Y", strtotime($var_dateStart));?></b>
											 </th>
											 <th colspan="{{$colspan}}">KHR</th>
											 <th class="win2DR" >ត្រូវ </th>
											 <th class="win3DR">ត្រូវ </th>
											 <th colspan="{{$colspan}}" class="moneyAllDD">USD</th>
											 <th class="win2DD">ត្រូវ</th>
											 <th class="win3DD">ត្រូវ</th>
										 </tr>
										 <tr class="thead">
											 <th style="width:10%;">{{trans('label.staff')}}</th>
											 <th style="width:5%;">វេន</th>
											 <th colspan="1" style="width:10%;" class="pOwner">
							 					ម្ចាស់ក្រដាស់
											 </th>
											 <th style="width:5%;">ល.រ</th>
											 <th class="{{$hidedata}}" style="width:10%;">2D</th>
											 <th class="{{$hidedata}}" style="width:10%;">3D</th>
											 <th class="{{$sumHide}}" style="width:10%;">លុយ</th>
											 <th class="win2DR" style="width:10%;">2D</th>
											 <th class="win3DR" style="width:10%;">3D</th>
											 <th class="{{$hidedata}} money2DD" style="width:10%;">2D</th>
											 <th class="{{$hidedata}} money3DD" style="width:10%;">3D</th>
											 <th class="{{$sumHide}} moneyAllDD" style="width:10%;">លុយ</th>
											 <th class="win2DD" style="width:10%;">2D</th>
											 <th class="win3DD" style="width:10%;">3D</th>
										 </tr>
										 
										 <!-- </thead> -->
										 <tbody class="display_total_result_{{$var_staff_chail}}">
										 	<?php 
										 		$staff_n = '';
										 		$time_n = '';
										 		$checkTimePa = '';
										 		$countPaper = 0;
										 	?>
										 	@foreach($boss_data as $key => $data)
										 		<?php 

										 			if($key == 0){
										 				$checkTimePa = $data->time_value;
										 			}

							 						/* check % */
													 $staffCheck = DB::table('tbl_staff')->where('s_id', $data->id_staff)->first();
							 						/* var_dump($staffCheck->percent_data); */
											 		if($staff_n != $data->staff_value){
											 			$name_staff_display = $data->staff_value;
											 			$staff_n = $data->staff_value;

											 			$time_n = '';
											 		}else{
											 			$name_staff_display = '';
											 		}

											 		if($time_n != $data->time_value){
											 			$time_display = $data->time_value;

											 			$find = array("0","1","2","3");
														$replace = array("");
														$time_display = str_replace($find,$replace,$time_display);

											 			$time_n = $data->time_value;
											 		}else{
											 			$time_display = '';
											 		}
											 		// var_dump(number_format($data->total2digitr));
											 		// dd($main_new);
											 		
                                                    $cal_2_digit = $twodigit_charge;
                                                    $cal_3_digit = $threedigit_charge;


										 		?>
										 	@if($data->time_value != $checkTimePa )
										 		<?php 
										 			$find = array("0","1","2","3");
													$replace = array("");
													$timeFinal = str_replace($find,$replace,$checkTimePa);
													$displayPaperNum = $countPaper;
													$countPaper = 0;
										 		?>
										 		<tr>
										 			<td colspan="3" style="text-align:right !important; border-right:none !important; border-left:none !important;"></td>
										 			<td colspan="1" style="text-align:center !important;  border-left:none !important;  border-right:none !important;">{{$timeFinal}} {{$displayPaperNum}}</td>
										 			<td colspan="10" style="text-align:right !important; border-left:none !important; border-right:none !important;"></td>
										 		</tr>
										 	@endif
										 		<tr class="many_row_{{$var_staff_chail}}">
										 			
										 			<td id="td_staff_{{$key}}" id_staff="{{$data->id_staff}}" two="{{$cal_2_digit}}" three="{{$cal_3_digit}}" percent="{{$staffCheck->percent_data}}" p_code="{{$data->p_code}}">
										 				{{$data->p_code_p}}
										 			</td>
													<td id="td_time_'+index+'">{{$time_display}}</td>
													<td colspan="1" style="width:10%;" class="pOwner">
														{{$data->p_code}}
														
													</td>
													<td id="td_page_'+index+'" style="width:5%; text-align: center !important;">
														{{$data->page_value}} ({{$key+1}})
													</td>
													
										 			<td class="{{$hidedata}} total2digitr " >@if($data->total2digitr != 0) {!! floatval($data->total2digitr) !!} @endif</td>
										 			<td class="{{$hidedata}} total3digitr " >@if($data->total3digitr != 0) {!! floatval($data->total3digitr) !!} @endif</td>
										 			<td class="{{$sumHide}} totalsum " >@if($data->totalsum != 0) {{ floatval($data->totalsum) }} @endif</td>
										 			<td class="total2digitrright win2DR" style="width:300px;">@if($data->total2digitrright != 0) {!! floatval($data->total2digitrright) !!} @endif</td>
										 			<td class="total3digitrright win3DR">@if($data->total3digitrright != 0) {{ floatval($data->total3digitrright) }} @endif</td>
										 			<td class="{{$hidedata}} total2digits money2DD">@if($data->total2digits != 0) {{ floatval($data->total2digits) }} @endif</td>
										 			<td class="{{$hidedata}} total3digits money3DD">@if($data->total3digits != 0) {{ floatval($data->total3digits) }} @endif</td>
										 			<td class="{{$sumHide}} totalsum_dolla moneyAllDD">@if($data->totalsum_dolla != 0) {{ floatval($data->totalsum_dolla) }} @endif</td>
										 			<td class="total2digitsright win2DD">@if($data->total2digitsright != 0) {{ floatval($data->total2digitsright) }} @endif</td>
										 			<td class="total3digitsright win3DD">@if($data->total3digitsright != 0) {{ floatval($data->total3digitsright) }} @endif</td>

										 		</tr>
										 	

										 		<?php 
										 			$checkTimePa = $data->time_value;
										 			$countPaper++;
										 		?>

										 	@endforeach

										 		<?php 
										 			$find = array("0","1","2","3");
													$replace = array("");
													$timeFinal = str_replace($find,$replace,$checkTimePa);
										 		?>
										 		<tr>
										 			<td colspan="3" style="text-align:right !important; border-right:none !important; border-left:none !important;"></td>
										 			<td colspan="1" style="text-align:center !important;  border-left:none !important;  border-right:none !important;">{{$timeFinal}} {{$countPaper}}</td>
										 			<td colspan="10" style="text-align:right !important; border-left:none !important; border-right:none !important;"></td>
										 		</tr>
										 </tbody>
									 </table>
								</div>
								<div class="footnotes"></div>




		@endif






	
	

    


<script type="text/javascript">

	@if(isset($boss_data) && count($boss_data) > 0)

		$(document).ready(function() {
			var total2digitr = '';
			var total2digits = '';
			var total3digitr = '';
			var total3digits = '';
			var total2digitrright = '';
			var total2digitsright = '';
			var total3digitrright = '';
			var total3digitsright = '';
			var time = '';
			var staff_list = '';
			var page = '';
			var tr = '';
			
			var sum_total2digitrright = 0;
			var sum_total2digitsright = 0;
			var sum_total3digitrright = 0;
			var sum_total3digitsright = 0;

			var num_total2digitrright = 0;
			var num_total2digitsright = 0;
			var num_total3digitrright = 0;
			var num_total3digitsright = 0;


			var twodigit_charge = <?php echo $twodigit_charge;?>;
			var threedigit_charge = <?php echo $threedigit_charge;?>;

			var sum_total2digitr = 0;
			var sum_total2digits = 0;
			var sum_total3digitr = 0;
			var sum_total3digits = 0;


			var sum_total2digitr_new = 0;
			var sum_total2digits_new = 0;
			var sum_total3digitr_new = 0;
			var sum_total3digits_new = 0;

			var sum2DigitBoosReal = 0;
			var sum3DigitBoosReal = 0;
			var sum2DigitBoosDolla = 0;
			var sum3DigitBoosDolla = 0;

			var sumPerMoney = 0;
			var sumPerMoney_D = 0;

			var data_array = new Array();

			var win2DR = 0;
			var win3DR = 0;

			var dolarFooter = 0;
			var money2DD = 0;
			var money3DD = 0;
			var moneyAllDD = 0;
			var win2DD = 0;
			var win3DD = 0;
			var pOwner = 0;

			var total_page_sum = 0;
			$(".many_row_{{$var_staff_chail}}").each(function(index, element) {
				total_page_sum = total_page_sum + 1;

				

				total2digitr = $(this).find('.total2digitr').text();
				total2digits = $(this).find('.total2digits').text();
				total3digitr = $(this).find('.total3digitr').text();
				total3digits = $(this).find('.total3digits').text();
				total2digitrright = $(this).find('.total2digitrright').text();
				total2digitsright = $(this).find('.total2digitsright').text();
				total3digitrright = $(this).find('.total3digitrright').text();
				total3digitsright = $(this).find('.total3digitsright').text();

				var percent = $(this).find('#td_staff_'+index).attr('percent');
				var page_code = $(this).find('#td_staff_'+index).attr('p_code');

				var id_staff = $(this).find('#td_staff_'+index).attr('id_staff');
				var twodigit_charge = $(this).find('#td_staff_'+index).attr('two');
				var threedigit_charge = $(this).find('#td_staff_'+index).attr('three');
					// alert(id_staff);

				if(total2digitr==0){
					numtotal2digitr = 0;
					total2digitr = '';
				}else{
					numtotal2digitr = total2digitr;
					total2digitr = total2digitr;
				}

				if(total2digits==0){
					numtotal2digits = 0;
					total2digits = '';
				}else{
					numtotal2digits = total2digits;
					total2digits = total2digits;
				}

				if(total3digitr==0){
					numtotal3digitr = 0;
					total3digitr = '';
				}else{
					numtotal3digitr = total3digitr;
					total3digitr = total3digitr;
				}

				var totalsum = parseFloat(numtotal2digitr) + parseFloat(numtotal3digitr);
				if(totalsum==0){
					totalsum = '';
				}

				if(total3digits==0){
					numtotal3digits = 0;
					total3digits = '';
				}else{
					numtotal3digits = total3digits;
					total3digits = total3digits;
				}

				if(total2digitrright==0){
					num_total2digitrright = 0;
					total2digitrright = '';
				}else{
					num_total2digitrright = total2digitrright;
					total2digitrright = total2digitrright;

				}
				if(total2digitsright==0){
					num_total2digitsright = 0;
					total2digitsright = '';
				}else{
					num_total2digitsright = total2digitsright;
					total2digitsright = total2digitsright;
				}
				if(total3digitrright==0){
					num_total3digitrright = 0;
					total3digitrright = '';
				}else{
					num_total3digitrright = total3digitrright;
					total3digitrright = total3digitrright;
				}
				if(total3digitsright==0){
					num_total3digitsright = 0;
					total3digitsright = '';
				}else{
					num_total3digitsright = total3digitsright;
					total3digitsright = total3digitsright;
				}

				var totalsum_dolla = parseFloat(numtotal2digits) + parseFloat(numtotal3digits);
				if(totalsum_dolla==0){
					totalsum_dolla = '';
				}

				sum_total2digitr = sum_total2digitr + parseFloat(numtotal2digitr);
				sum_total2digits = sum_total2digits + parseFloat(numtotal2digits);
				sum_total3digitr = sum_total3digitr + parseFloat(numtotal3digitr);
				sum_total3digits = sum_total3digits + parseFloat(numtotal3digits);

				sum_total2digitrright = sum_total2digitrright + parseFloat(num_total2digitrright);
				sum_total2digitsright = sum_total2digitsright + parseFloat(num_total2digitsright);
				sum_total3digitrright = sum_total3digitrright + parseFloat(num_total3digitrright);
				sum_total3digitsright = sum_total3digitsright + parseFloat(num_total3digitsright);

				sum_total2digitr_new = sum_total2digitr_new + (
																Math.round(

																		((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100)
																	
																)
															);


				sum_total2digits_new = sum_total2digits_new + 

															(
																

																		((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100)
																	
																
															);


				sum2DigitBoosReal = sum2DigitBoosReal + (((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100));

				sum3DigitBoosReal = sum3DigitBoosReal + (((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100));

				sum2DigitBoosDolla = sum2DigitBoosDolla + (((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100));

				sum3DigitBoosDolla = sum3DigitBoosDolla + (((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100));
				
                
				if(percent > 0){
					var totalRIncom = (((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100)) + (((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100));
					var totalRExpan = (parseInt(num_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(num_total3digitrright * <?php echo $win_three_digit;?>) );
					var moneyData = ((totalRIncom-totalRExpan) *  percent)/100;
					console.log(totalRIncom,' Income Real');
					console.log(totalRExpan, 'Expan');
					
					sumPerMoney = sumPerMoney + moneyData;
					console.log(percent, 'percent');
					console.log(moneyData, 'Money');

					/* dolla */
					var totalDIncom = (((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100)) + (((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100));
					var totalDExpan = (parseInt(num_total2digitsright * <?php echo $win_two_digit;?>) + parseInt(num_total3digitsright * <?php echo $win_three_digit;?>) );
					var moneyData_D = ((totalDIncom-totalDExpan) *  percent)/100;
					sumPerMoney_D = sumPerMoney_D + moneyData_D;
				}
				
				/* check condition col */

				if(total2digitrright > 0){
					win2DR = win2DR + 1;
				}
				if(total3digitrright > 0){
					win3DR = win3DR + 1;
				}

				if(total2digits > 0){
					money2DD = money2DD + 1;
				}
				if(total3digits > 0){
					money3DD = money3DD + 1;
				}
				if(totalsum_dolla > 0){
					moneyAllDD = moneyAllDD + 1;
				}

				if(total2digitsright > 0){
					win2DD = win2DD + 1;
				}
				if(total3digitsright > 0){
					win3DD = win3DD + 1;
				}
				if(page_code != ''){
					pOwner = pOwner + 1;
				}
				
			});


			var total_income_expense_riel = 0;
			var total_income_expense_dollar = 0;
			
				
				
                var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
                var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
			



				// alert(sum_total2digitr_new);

				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				
			
			 // var a = Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100));
			 // var b = Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100));
			 // var aaaa = a + b;
			// alert( income_riel );
			var staff_id = "<?php echo $var_staff_chail;?>";
			
			
            var new_price = income_riel;
            var new_price_s = income_dollar;

			var total_income_expense_riel = income_riel - expense_riel;
			var check_total = total_income_expense_riel.toString().substr(-2);
            // console.log(total_income_expense_riel);
            // console.log(check_total);

            
         

			if(total_income_expense_riel>0){
				total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = total_income_expense_riel;
            }else{
            	total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = '('+total_income_expense_riel+')';
            }

			var total_income_expense_dollar = income_dollar - expense_dollar;
			var total_income_expense_dollar_noStr = total_income_expense_dollar;
            if(total_income_expense_dollar>0){
                total_income_expense_dollar = total_income_expense_dollar.toFixed(2);
            }else{
                total_income_expense_dollar = '('+total_income_expense_dollar.toFixed(2)+')';
            }

            var sum_totalall = sum_total2digitr + sum_total3digitr;
            var sum_totalall_dolla = sum_total2digits + sum_total3digits;


           

			<?php 
				$totalCal = $colspan + 1;
			?>

			

			tr = '<tr class="bee_highlight background"><th>{{trans('label.total')}}</th><th></th><th class="pOwner"></th><th >'+total_page_sum+'</th><th class="{{$hidedata}}">'+sum_total2digitr+'</th><th class="{{$hidedata}}">'+sum_total3digitr+'</th><th class="{{$sumHide}}">'+sum_totalall+'</th><th class="win2DR">'+sum_total2digitrright+'</th><th class="win3DR">'+sum_total3digitrright+'</th><th class="{{$hidedata}} money2DD">$ '+sum_total2digits+'</th><th class="{{$hidedata}} money3DD">$ '+sum_total3digits+'</th><th class="{{$sumHide}} moneyAllDD">$ '+sum_totalall_dolla+'</th><th class="win2DD">$ '+sum_total2digitsright+'</th><th class="win3DD">$ '+sum_total3digitsright+'</th></tr>';
			$('.display_total_result_{{$var_staff_chail}}').append(tr);

			// tr = '<tr class="bee_highlight"><td></td><td>{{trans('label.total_charge')}}</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)))+'</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitrright * 70)+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitrright * 600)+'</td><td>'+commaSeparateNumber(((parseInt(sum_total2digits) * parseInt(twodigit_charge)) / 100).toFixed(2))+'</td><td>'+commaSeparateNumber(((parseInt(sum_total3digits) * parseInt(threedigit_charge)) / 100).toFixed(2))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitsright * 70 )+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitsright * 600)+'</td><tr>';
			// $('.display_total_result').append(tr);
            var total_rate_thesame = parseInt(sum_total2digitr) + parseInt(sum_total3digitr);
            var total_rate_thesame_dolla = parseFloat(sum_total2digits) + parseFloat(sum_total3digits); 
			tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result_{{$var_staff_chail}}').append(tr);

            var cospan23D = 3;

            if(win3DR == 0 && win2DR == 0){
            	cospan23D = 2;
            }

            if(win3DR>0){
                cospan23D = 4;
                
            }

            console.log(cospan23D,'okok');
			
			// 2D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th colspan="'+cospan23D+'" class="pOwnerManFooter" rowspan="2" style="vertical-align: middle; text-align: center;">ទឹក</th><th>2D</th><th>'+sum_total2digitr+'</th><th> => </th><th>'+Math.round(sum2DigitBoosReal)+'</th><th class="win3DR footer3D"></th><th class="footerDollar">$ '+sum_total2digits+'</th><th class="footerDollar"> => </th><th class="footerDollar">$ '+sum2DigitBoosDolla.toFixed(2)+'</th><th class="footerDollar"></th></tr>';
			$('.display_total_result_{{$var_staff_chail}}').append(tr);
			// 3D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th>3D</th><th>'+sum_total3digitr+'</th><th> => </th><th>'+Math.round(sum3DigitBoosReal)+'</th><th class="win3DR footer3D"></th><th class="footerDollar">$ '+sum_total3digits+'</th><th class="footerDollar"> => </th><th class="footerDollar">$ '+sum3DigitBoosDolla.toFixed(2)+'</th><th class="footerDollar"></th></tr>';
			$('.display_total_result_{{$var_staff_chail}}').append(tr);

			tr = '<tr class="bee_highlight {{$sumHide}}"><th colspan="'+(cospan23D-1)+'"  style="vertical-align: middle; text-align: center;">ទឹក</th><th>សរុប</th><th>'+total_rate_thesame+'</th><th> => </span></th><th>'+Math.round(sum_total2digitr_new)+'</th><th class="win3DR"></th><th class="footerDollar">$ '+total_rate_thesame_dolla+'</th><th class="footerDollar"> => </th><th class="footerDollar">$ '+sum_total2digits_new.toFixed(2)+'</th></tr>';
			$('.display_total_result_{{$var_staff_chail}}').append(tr);

			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);



			
			
            // 2D(T)
            tr = '<tr class="bee_highlight"><th class="{{$hidedata}} pOwnerManFooter" colspan="'+cospan23D+'" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th class="{{$sumHide}}" colspan="2" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th>x<?php echo $win_two_digit;?></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th class="win3DR footer3D"></th><th class="footerDollar">'+sum_total2digitsright+'</th><th class="footerDollar">x<?php echo $win_two_digit;?></th><th class="footerDollar">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
            $('.display_total_result_{{$var_staff_chail}}').append(tr);
            // 3D(T)
            tr = '<tr class="bee_highlight"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th>x<?php echo $win_three_digit;?></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th class="win3DR footer3D"></th><th class="footerDollar">'+sum_total3digitsright+'</th><th class="footerDollar">x<?php echo $win_three_digit;?></th><th class="footerDollar">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
            $('.display_total_result_{{$var_staff_chail}}').append(tr);

			tr = '<tr class=""><td colspan="12" class="lr_clear"></td></tr>';
			$('.display_total_result_{{$var_staff_chail}}').append(tr);


			// paid or borrow


			var totalRealBefore = total_income_expense_riel_noStr;
			var totalDollaBefore = total_income_expense_dollar_noStr;

			console.log(totalRealBefore,'Before R');
			console.log(sumPerMoney,'ok % R');

			console.log(totalDollaBefore,'Before D');
			console.log(sumPerMoney_D,' % D');

			// totalRealBefore = totalRealBefore - sumPerMoney;
			// totalDollaBefore = totalDollaBefore - sumPerMoney_D;
			
			console.log(totalRealBefore,'Ready R');
			console.log(totalDollaBefore,'Ready D');


			
			
			
			

			if(totalRealBefore < 0){
				var labelBefore_r = 'ខាត';
				var redColor = 'color: red !important;';
			}else{
				var labelBefore_r = 'សុី';
			}

			if(totalDollaBefore < 0){
				var labelBefore_d = 'ខាត';
				var redColorDolar = 'color: red !important;';
			}else{
				var labelBefore_d = 'សុី';
			}	

            var cospanTotal = 4;
            if(win3DR>0){
                cospanTotal = 5;
                
            }
			// price before percent
			tr = '<tr class="bee_highlight "><th class="clearBorder" colspan="{{$colspan}}"></th><th colspan="'+cospanTotal+'" class="pOwnerManFour clearBorder" style="text-align:right !important; '+redColor+'">'+labelBefore_r+'</th><th class="result_right_total" style="text-align:center !important; '+redColor+'">'+addCommas(totalRealBefore)+' </th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th style="'+redColorDolar+'" class="footerDollar">'+labelBefore_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+addCommas(totalDollaBefore.toFixed(2))+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
			$('.display_total_result_{{$var_staff_chail}}').append(tr);



			@if($percentStaff > 0)
				var percent_sub = parseFloat('{{$percentStaff}}');
				var percent = 100 - percent_sub;

				var totalReal = parseFloat(totalRealBefore * percent)/100;
				var totalDolla = parseFloat(totalDollaBefore * percent)/100;

				var redColorPer = '';
				if(totalReal < 0){
					var redColorPer = 'color: red;';
				}

				var redColorPerD = '';
				if(totalDolla < 0){
					var redColorPerD = 'color: red;';
				}



				// price after percent 
				// tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th class="clearBorder"></th><th class="clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:left !important;">'+percent_sub+'%</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(totalReal)+' </th><th></th><th></th><th>%</th><th class="result_right_total" style="text-align:left !important;">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColorPer+'">'+percent+'%</th><th class="result_right_total" style="text-align:center !important; '+redColorPer+'">'+addCommas(totalReal)+' </th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar">%</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorPerD+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
                $('.display_total_result_{{$var_staff_chail}}').append(tr);

				// chail_id
				var totalReal_main = parseFloat(totalRealBefore * percent_sub)/100;
				var totalDolla_main = parseFloat(totalDollaBefore * percent_sub)/100;

				tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColorPer+'">'+percent_sub+'%</th><th class="result_right_total" style="text-align:center !important; '+redColorPer+'">'+addCommas(totalReal_main)+' </th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar">%</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorPerD+'">$ '+totalDolla_main.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
                $('.display_total_result_{{$var_staff_chail}}').append(tr);


			@else

				var totalReal = totalRealBefore;
				var totalDolla = totalDollaBefore;

			@endif


			var displayMainTotal = 0;
			<?php 
				if (!empty($money_paid)) {
					foreach($money_paid as $key => $value) {
						if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23){
							$redColor = "";

                            if($value->st_type == 21 && $value->st_price_r < 0 ){
								$label_r = 'ខាតចាស់';
                                $redColor = "color: #ff0000;";
							}else{
								$label_r = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_d < 0 ){
								$label_d = 'ខាតចាស់';
                                $redColor = 'color: #ff0000;';
							}else{
								$label_d = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_r > 0 && $value->st_price_d > 0 ){
								$label_r = $value->pav_value;
								$label_d = '';
							}
							if($value->st_type == 21 && $value->st_price_r < 0 && $value->st_price_d < 0 ){
								$label_r = 'ខាតចាស់';
								$label_d = '';
                                $redColor = 'color: #ff0000;';
							}

			?>	
							// console.log(totalDolla);

							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							console.log(totalReal,subPrice_r,'new case');
							if(subPrice_r != ''){
								totalReal = totalReal + parseFloat(subPrice_r);
							}
							console.log(totalReal,'new case');

							if(subPrice_d != ''){
								totalDolla = totalDolla + parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							var displayPriceR = parseFloat('{{$value->st_price_r}}');
							var displayPriceD = parseFloat('{{$value->st_price_d}}');
							//tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">{{ $label_r }}</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(displayPriceR)+'</th><th></th><th></th><th>{{ $label_d }}</th><th class="result_right_total" style="text-align:left !important;" >'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
							//$('.display_total_result').append(tr);
                            tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important;">{{ $label_r }}</th><th class="result_right_total" style="text-align:center !important; {{$redColor}}">'+addCommas(displayPriceR)+'</th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar">{{ $label_d }}</th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result_{{$var_staff_chail}}').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}else{
			?>	
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';

							if(subPrice_r > 0){
								totalReal = totalReal - parseFloat(subPrice_r);
							}
							if(subPrice_d > 0){
								totalDolla = totalDolla - parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							
							var displayPriceR = parseFloat('{{$value->st_price_r * -1}}');
							var displayPriceD = parseFloat('{{$value->st_price_d * -1}}');
							//tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">{{ $value->pav_value }}</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(displayPriceR)+'</th><th></th><th></th><th></th><th class="result_right_total" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
							//$('.display_total_result').append(tr);
                            tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important;">{{ $value->pav_value }}</th><th class="result_right_total" style="text-align:center !important;">'+addCommas(displayPriceR)+'</th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th class="footerDollar"></th><th class="result_right_total footerDollar" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
							$('.display_total_result_{{$var_staff_chail}}').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}
					}
				}
			?>
			// end paid or borrow
			if(displayMainTotal > 0){
				// total price
				
				tr = '<tr class=""><td colspan="11" class="clearBorder"></td></tr>';
				$('.display_total_result_{{$var_staff_chail}}').append(tr);

				var redColor = '';
				var redColorDolar = '';

				if(totalReal > 0){
					var labelTotal = 'សុីសរុប'; 
				}else{
					var labelTotal = 'ខាតសរុប';
					var redColor = 'color: red;';
				}

				if(totalDolla > 0){
					var labelTotal_d = 'សុីសរុប'; 
				}else{
					var labelTotal_d = 'ខាតសរុប';
					var redColorDolar = 'color: red;';
				}

				if(totalReal >= 0 && totalDolla >= 0){
					var labelTotal = 'សុីសរុប'; 
					var labelTotal_d = ''; 
				}

				if(totalReal < 0 && totalDolla < 0){
					var labelTotal = 'ខាតសរុប'; 
					var labelTotal_d = ''; 
					var redColor = 'color: red;';
				}

				//tr = '<tr class="bee_highlight"><th class="{{$hidedata}}" ></th><th></th><th></th><th colspan="2" style="text-align:left !important; '+redColor+'">'+labelTotal+'</th><th class="result_right_total" style="text-align:left !important; '+redColor+'">'+addCommas(totalReal)+'</th><th></th><th></th><th style="'+redColorDolar+'">'+labelTotal_d+'</th><th class="result_right_total" style="text-align:left !important; '+redColorDolar+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
				//$('.display_total_result_{{$var_staff_chail}}').append(tr);
                tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColor+'"><span class='+redColor+'>'+labelTotal+'</span></th><th class="result_right_total bottom_clear" style="text-align:center !important; '+redColor+'"><span class='+redColor+'>'+addCommas(totalReal)+'</span></th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th style="'+redColorDolar+'" class="footerDollar">'+labelTotal_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
				$('.display_total_result_{{$var_staff_chail}}').append(tr);

				// console.log(moneyAllDD,totalDolla,'test dollar');
				if(moneyAllDD == 0 && (totalDolla>0 || totalDolla<0)){

					// console.log(moneyAllDD,totalDolla,'okokok');
					tr = '<tr class="bee_highlight"><th class="{{$hidedata}} clearBorder"></th><th colspan="'+cospan23D+'" class="pOwnerManFooter clearBorder"></th><th colspan="2" class="clearBorder" style="text-align:right !important; '+redColorDolar+'"><span class='+redColorDolar+'>'+labelTotal_d+'</span></th><th class="result_right_total bottom_clear" style="text-align:center !important; '+redColorDolar+'"><span class='+redColorDolar+'>$ '+totalDolla.toFixed(2)+'</span></th><th class="win3DR footer3D"></th><th class="footerDollar"></th><th style="'+redColorDolar+'" class="footerDollar">'+labelTotal_d+'</th><th class="result_right_total footerDollar" style="text-align:left !important; '+redColorDolar+'">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} footerDollar"></th></tr>';
					$('.display_total_result_{{$var_staff_chail}}').append(tr);
				}
			}

			// $(".report_total_info").each(function(index, element) {

			// 	staff_list = $(this).attr('staff');
			// 	id_staff = $(this).attr('id_staff');

			// 	time = $(this).attr('time');
			// 	page = $(this).attr('page');
			// 	$('.display_total_result').find('#td_staff_com_'+index).text(staff_list);
			// 	$('.display_total_result').find('#td_time_com_'+index).text(time);
			// 	$('.display_total_result').find('#td_page_com_'+index).text(page);

			// });

			var date = '{{$var_dateStart}}';
			var staff = '{{$var_staff_chail}}';
			

			

			if(win3DR == 0){
				$("#summary_result_per_day_{{$var_staff_chail}} .win3DR").addClass('hidetableTD');
			}else{
                $("#summary_result_per_day_{{$var_staff_chail}} .win3DR.footer3D").addClass('hidetableTD');
            }

			if(moneyAllDD == 0){
				console.log(moneyAllDD,' moneyAllDD');
				console.log(money2DD,' money2DD');
				console.log(money3DD,' money3DD');
				console.log(win2DD,' money2DD');
				console.log(win3DD,' money3DD');
				$("#summary_result_per_day_{{$var_staff_chail}} .moneyAllDD").addClass('hidetableTD');
				$("#summary_result_per_day_{{$var_staff_chail}} .money2DD").addClass('hidetableTD');
				$("#summary_result_per_day_{{$var_staff_chail}} .money3DD").addClass('hidetableTD');
				$("#summary_result_per_day_{{$var_staff_chail}} .win2DD").addClass('hidetableTD');
				$("#summary_result_per_day_{{$var_staff_chail}} .win3DD").addClass('hidetableTD');
				$("#summary_result_per_day_{{$var_staff_chail}} th.footerDollar").addClass('hidetableTD');
			}

			// console.log(pOwner,'pOwner');

			if(pOwner == 0){
				$("#summary_result_per_day_{{$var_staff_chail}} .pOwner").addClass('hidetableTD');
				$("#summary_result_per_day_{{$var_staff_chail}} .pOwnerMan").attr('colspan',2);
				
				var numSpan = 3;
				var numSpanTotal = 4;
				// if(win3DR == 0 && win2DR == 0){
				// 	numSpan = 2;
				// 	numSpanTotal = 3;
	   //          }

	            if(win3DR == 0 && win2DR == 0){
					numSpan = 2;
					numSpanTotal = 3;
				}else if(win2DR > 0 && win3DR > 0){
					
	            }else if(win2DR > 0){
	            	numSpan = 2;
	            	numSpanTotal = 3;
	            }


	            $("#summary_result_per_day_{{$var_staff_chail}} .pOwnerManFour").attr('colspan',numSpanTotal);
				$("#summary_result_per_day_{{$var_staff_chail}} .pOwnerManFooter").attr('colspan',numSpan);
				$("#summary_result_per_day_{{$var_staff_chail}} .numSumP").hide();
			}





		});


       
	
		
	@endif
</script>




@endif
@endforeach

</div>
<script type="text/javascript">
	

	$(document).ready(function() {
    // $("#btnPrintDiv").click(function () {
        var contents = $("#html-content-holder-main").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>Print Data</title>');
        //Append the external CSS file.
        frameDoc.document.write('<link href="<?php echo asset("css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="<?php echo asset("css/stylePrint.css"); ?>" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('</head><body>');
        //Append the DIV contents.
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    // });
	});
</script>

</body>

</html>