<!-- User info -->
<?php 
	function getUserIP()
	{
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}
	$user_ip = getUserIP();

	
?>
<style>
	.newBuild a .menu-item-parent{
		color: green;
	}
</style>

			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						
						<span>
							{{Session::get('nameLot')}}
						</span>
						<!-- <i class="fa fa-angle-down"></i> -->
					</a> 
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive

			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->
			<nav>
				<!-- NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

				<ul>
					@if(Session::get('roleLot') == 1)
					<li>
						<a href="{{URL::to('/')}}/dasboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">{{trans('label.dashboard')}}</span></a>
					</li>

					<li class="newBuild">
						<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('label.sale') }} Phone</span></a>
						<ul>
							<li @if ($page == 'salephone') class="active" @endif>
								<a href="{{URL::to('/')}}/salephone?type=1">{{ trans('label.list_paper') }} (VN) Phone</a>
							</li>
							<li @if ($page == 'salephone') class="active" @endif>
								<a href="{{URL::to('/')}}/salephone?type=2">{{ trans('label.list_paper') }} (KH) Phone</a>
							</li>
							<!-- <li @if ($page == 'max2digit') class="active" @endif>
								<a href="{{URL::to('/')}}/maxpricetwo">ឆែកទិន្នន័យឆ្នោត2លេខ</a>
							</li>
							<li @if ($page == 'max3digit') class="active" @endif>
								<a href="{{URL::to('/')}}/maxpricethree">ឆែកទិន្នន័យឆ្នោត3លេខ</a>
							</li> -->
						</ul>
					</li>
					<li class="newBuild">
						<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">លេខបិទ</span></a>
						<ul>
							<li @if ($page == 'close_number') class="active" @endif>
								<a href="{{URL::to('/')}}/closenumber?type=1">បង្ហាញលេខបិទ</a>
							</li>
							<li @if ($page == 'add_close_number') class="active" @endif>
								<a href="{{URL::to('/')}}/closenumber/create">បញ្ចូលលេខបិទ</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{trans('label.sale')}}</span></a>
						<ul>
							<li>
								<a href="#">VN</a>
								<ul>
									<li @if ($page == 'sale') class="active" @endif>
										<a href="{{URL::to('/')}}/sale?type=1">{{trans('label.list_paper')}}</a>
									</li>
									<li @if ($page == 'add_sale') class="active" @endif>
										<a href="{{URL::to('/')}}/sale/create?type=1">{{trans('label.add_new_lottery')}}</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">KH</a>
								<ul>
									<li @if ($page == 'sale_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/sale?type=2">{{trans('label.list_paper')}}</a>
									</li>
									<li @if ($page == 'add_sale_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/sale/create?type=2">{{trans('label.add_new_lottery')}}</a>
									</li>
								</ul>
							</li>
							
						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">មើលតារាងលេខ</span></a>
						<ul>
							
									<li @if ($page == 'reportnumber') class="active" @endif>
										<a href="{{URL::to('/')}}/reportnumber">{{ trans('result.resultDisplay') }}</a>
									</li>
							
						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('result.result') }}</span></a>
						<ul>
							
									<!-- <li @if ($page == 'result') class="active" @endif>
										<a href="{{URL::to('/')}}/result?stc_type=1">{{ trans('result.resultDisplay') }}</a>
									</li>
									<li @if ($page == 'add_result') class="active" @endif>
										<a href="{{URL::to('/')}}/result/create?stc_type=1">{{ trans('result.addResult') }}</a>
									</li> -->
								<li>
									<a href="#">VN</a>
									<ul>
										<li @if ($page == 'result') class="active" @endif>
											<a href="{{URL::to('/')}}/result?stc_type=1">{{ trans('result.resultDisplay') }}</a>
										</li>
										<li @if ($page == 'add_result') class="active" @endif>
											<a href="{{URL::to('/')}}/result/create?stc_type=1">{{ trans('result.addResult') }}</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">KH</a>
									<ul>
										<li @if ($page == 'result_kh') class="active" @endif>
											<a href="{{URL::to('/')}}/result?stc_type=2">{{ trans('result.resultDisplay') }}</a>
										</li>
										<li @if ($page == 'add_result_kh') class="active" @endif>
											<a href="{{URL::to('/')}}/result/create?stc_type=2">{{ trans('result.addResult') }}</a>
										</li>
									</ul>
								</li>
							
						</ul>

					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('report.report_result') }}</span></a>
						<ul>
							<li @if ($page == 'report') class="active" @endif>
								<a href="{{URL::to('/')}}/report">{{ trans('sidebar.reportDisplay') }}</a>
							</li>
							<li @if ($page == 'reportMain') class="active" @endif>
								<a href="{{URL::to('/')}}/reportmainstaff">មេីលបញ្ចី</a>
							</li>
							
							<!-- <li @if ($page == 'daily_report') class="active" @endif>
								<a href="{{URL::to('/')}}/dailyreport">បញ្ចីប្រចាំថ្ងៃ</a>
							</li>
							<li @if ($page == 'total_daily_report') class="active" @endif>
								<a href="{{URL::to('/')}}/totaldailyreport">បញ្ចីលុយកូនសរុបប្រចាំថ្ងៃ</a>
							</li>
							<li @if ($page == 'money_total_report') class="active" @endif>
								<a href="{{URL::to('/')}}/moneytotalreport">បញ្ចីទឹកលុយសរុប</a>
							</li> -->
							
						</ul>
					</li>

					<li class="newBuild" style="display: none;">
						<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('report.report_result') }} Phone</span></a>
						<ul>
							<!-- <li @if ($page == 'reportphone') class="active" @endif>
								<a href="{{URL::to('/')}}/reportphone">មេីលបញ្ជី Phone Detail</a>
							</li> -->
							<li @if ($page == 'checkreportphone') class="active" @endif>
								<a href="{{URL::to('/')}}/reportphone/checkReportPhone">{{ trans('sidebar.reportDisplay') }} Phone</a>
							</li>
							<li @if ($page == 'reportdetail') class="active" @endif>
								<a href="{{URL::to('/')}}/reportdetail">មេីលបញ្ជីលម្អិត</a>
							</li>
							<!-- <li @if ($page == 'reportMain') class="active" @endif>
								<a href="{{URL::to('/')}}/reportmainstaff">មេីលបញ្ជីសរុប</a>
							</li> -->
							
							<!-- <li @if ($page == 'daily_report') class="active" @endif>
								<a href="{{URL::to('/')}}/dailyreport">បញ្ចីប្រចាំថ្ងៃ Phone</a>
							</li>
							<li @if ($page == 'total_daily_report') class="active" @endif>
								<a href="{{URL::to('/')}}/totaldailyreport">បញ្ចីលុយកូនសរុបប្រចាំថ្ងៃ Phone</a>
							</li>
							<li @if ($page == 'money_total_report') class="active" @endif>
								<a href="{{URL::to('/')}}/moneytotalreport">បញ្ចីទឹកលុយសរុប Phone</a>
							</li> -->
							
						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">{{trans('label.staff_transaction')}}</span></a>
						<ul>
							<!-- <li @if ($page == 'reportstaff') class="active" @endif>
								<a href="{{URL::to('/')}}/stafftransction/reportstaff">របាយការណ៏ លក់បុគ្គលិក</a>
							</li> -->
							<li @if ($page == 'transction') class="active" @endif>
								<a href="{{URL::to('/')}}/transction">{{trans('label.view_staff_transaction')}}</a>
							</li>
							<!-- <li @if ($page == 'add_staff_transction') class="active" @endif>
								<a href="{{URL::to('/')}}/stafftransction/create">{{trans('label.add_new')}}</a>
							</li> -->
						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">មេក្រុម</span></a>
						<ul>
							<li @if ($page == 'staff') class="active" @endif>
								<a href="{{URL::to('/')}}/staff">បង្ហាញមេក្រុម</a>
							</li>
							<li @if ($page == 'add_staff') class="active" @endif>
								<a href="{{URL::to('/')}}/staff/create">{{trans('label.add_new')}}</a>
							</li>
						</ul>
					</li>
					<li class="newBuild">
						<a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">{{trans('label.staff')}} Phone</span></a>
						<ul>
							<li @if ($page == 'staff_mobile' || $page == 'time_close') class="active" @endif>
								<a href="{{URL::to('/')}}/staffmobile">{{trans('label.view_staff')}} Phone</a>
							</li>
							<li @if ($page == 'add_staff_mobile') class="active" @endif>
								<a href="{{URL::to('/')}}/staffmobile/create">{{trans('label.add_new')}} Phone</a>
							</li>
						</ul>
					</li>

					
					<!-- <li>
						<a href="#"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Report</span></a>
						<ul>
							<li @if ($page == 'profit-loss') class="active" @endif>
								<a href="{{URL::to('/')}}/profit-loss">Profit_loss by staff</a>
							</li>							
						</ul>
					</li> -->

					

					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-folder-open"></i> <span class="menu-item-parent">{{trans('label.pos')}}</span></a>
						<ul>
							<li>
								<a href="#">VN</a>
								<ul>
									<li @if ($page == 'pos') class="active" @endif>
										<a href="{{URL::to('/')}}/pos?stc_type=1">{{trans('label.view_pos')}}</a>
									</li>
									<li @if ($page == 'add_pos') class="active" @endif>
										<a href="{{URL::to('/')}}/pos/create?stc_type=1">{{trans('label.add_new')}}</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">KH</a>
								<ul>
									<li @if ($page == 'pos_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/pos?stc_type=2">{{trans('label.view_pos')}}</a>
									</li>
									<li @if ($page == 'add_pos_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/pos/create?stc_type=2">{{trans('label.add_new')}}</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">TH</a>
								<ul>
									<li @if ($page == 'pos_th') class="active" @endif>
										<a href="{{URL::to('/')}}/pos?stc_type=3">{{trans('label.view_pos')}}</a>
									</li>
									<li @if ($page == 'add_pos_th') class="active" @endif>
										<a href="{{URL::to('/')}}/pos/create?stc_type=3">{{trans('label.add_new')}}</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">វត្តុ</a>
								<ul>
									<li @if ($page == 'pos_lo') class="active" @endif>
										<a href="{{URL::to('/')}}/pos?stc_type=4">{{trans('label.view_pos')}}</a>
									</li>
									<li @if ($page == 'add_pos_lo') class="active" @endif>
										<a href="{{URL::to('/')}}/pos/create?stc_type=4">{{trans('label.add_new')}}</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-folder-open"></i> <span class="menu-item-parent">{{trans('label.group')}}</span></a>
						<ul>
							<li>
								<a href="#">VN</a>
								<ul>
									<li @if ($page == 'pos_group') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup?stc_type=1">{{trans('label.view_pos_group')}}</a>
									</li>
									<li @if ($page == 'add_pos_group') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup/create?stc_type=1">{{trans('label.add_pos_group')}}</a>
									</li>
									<li @if ($page == 'group') class="active" @endif>
										<a href="{{URL::to('/')}}/group?stc_type=1">{{trans('label.view_group')}}</a>
									</li>
									<li @if ($page == 'add_group') class="active" @endif>
										<a href="{{URL::to('/')}}/group/create?stc_type=1">{{trans('label.add_group')}}</a>
									</li>
								</ul>
							</li>

							<li>
								<a href="#">KH</a>
								<ul>
									<li @if ($page == 'pos_group_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup?stc_type=2">{{trans('label.view_pos_group')}}</a>
									</li>
									<li @if ($page == 'add_pos_group_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup/create?stc_type=2">{{trans('label.add_pos_group')}}</a>
									</li>
									<li @if ($page == 'group_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/group?stc_type=2">{{trans('label.view_group')}}</a>
									</li>
									<li @if ($page == 'add_group_kh') class="active" @endif>
										<a href="{{URL::to('/')}}/group/create?stc_type=2">{{trans('label.add_group')}}</a>
									</li>
								</ul>
							</li>

							<li>
								<a href="#">TH</a>
								<ul>
									<li @if ($page == 'pos_group_th') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup?stc_type=3">{{trans('label.view_pos_group')}}</a>
									</li>
									<li @if ($page == 'add_pos_group_th') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup/create?stc_type=3">{{trans('label.add_pos_group')}}</a>
									</li>
									<li @if ($page == 'group_th') class="active" @endif>
										<a href="{{URL::to('/')}}/group?stc_type=3">{{trans('label.view_group')}}</a>
									</li>
									<li @if ($page == 'add_group_th') class="active" @endif>
										<a href="{{URL::to('/')}}/group/create?stc_type=3">{{trans('label.add_group')}}</a>
									</li>
								</ul>
							</li>

							<li>
								<a href="#">វត្តុ</a>
								<ul>
									<li @if ($page == 'pos_group_lo') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup?stc_type=4">{{trans('label.view_pos_group')}}</a>
									</li>
									<li @if ($page == 'add_pos_group_lo') class="active" @endif>
										<a href="{{URL::to('/')}}/posgroup/create?stc_type=4">{{trans('label.add_pos_group')}}</a>
									</li>
									<li @if ($page == 'group_lo') class="active" @endif>
										<a href="{{URL::to('/')}}/group?stc_type=4">{{trans('label.view_group')}}</a>
									</li>
									<li @if ($page == 'add_group_lo') class="active" @endif>
										<a href="{{URL::to('/')}}/group/create?stc_type=4">{{trans('label.add_group')}}</a>
									</li>
								</ul>
							</li>

						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">តំបន់</span></a>
						<ul>
							<li @if ($page == 'location') class="active" @endif>
								<a href="{{URL::to('/')}}/location">បង្ហាញតំបន់</a>
							</li>
							<li @if ($page == 'add_location') class="active" @endif>
								<a href="{{URL::to('/')}}/location/create">បង្កេីតតំបន់</a>
							</li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">{{trans('label.user')}}</span></a>
						<ul>
							<li @if ($page == 'user') class="active" @endif>
								<a href="{{URL::to('/')}}/user">{{trans('label.list_user')}}</a>
							</li>
							<li @if ($page == 'add_user') class="active" @endif>
								<a href="{{URL::to('/')}}/user/create">{{trans('label.new_user')}}</a>
							</li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Data Error</span></a>
						<ul>
							<li @if ($page == 'baddata') class="active" @endif>
								<a href="{{URL::to('/')}}/baddata">Bad Data</a>
							</li>
							<li @if ($page == 'reporterror') class="active" @endif>
								<a href="{{URL::to('/')}}/reporterror">Report Error</a>
							</li>
						</ul>
					</li>
					@elseif(Session::get('roleLot') == 2)
						
						<li class="newBuild">
							<a href="{{URL::to('/')}}/dasboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
						</li>


						<li class="newBuild">
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('label.sale') }} Phone</span></a>
							<ul>
								<li @if ($page == 'salephone') class="active" @endif>
									<a href="{{URL::to('/')}}/salephone?type=1">{{ trans('label.list_paper') }} (VN) Phone</a>
								</li>
								<li @if ($page == 'salephone') class="active" @endif>
									<a href="{{URL::to('/')}}/salephone?type=2">{{ trans('label.list_paper') }} (KH) Phone</a>
								</li>
								<!-- <li @if ($page == 'max2digit') class="active" @endif>
									<a href="{{URL::to('/')}}/maxpricetwo">ឆែកទិន្នន័យឆ្នោត2លេខ</a>
								</li>
								<li @if ($page == 'max3digit') class="active" @endif>
									<a href="{{URL::to('/')}}/maxpricethree">ឆែកទិន្នន័យឆ្នោត3លេខ</a>
								</li> -->
							</ul>
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{trans('label.sale')}}</span></a>
							<ul>
								<li>
									<a href="#">VN</a>
									<ul>
										<li @if ($page == 'sale') class="active" @endif>
											<a href="{{URL::to('/')}}/sale?type=1">{{trans('label.list_paper')}}</a>
										</li>
										<li @if ($page == 'add_sale') class="active" @endif>
											<a href="{{URL::to('/')}}/sale/create?type=1">{{trans('label.add_new_lottery')}}</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">KH</a>
									<ul>
										<li @if ($page == 'sale_kh') class="active" @endif>
											<a href="{{URL::to('/')}}/sale?type=2">{{trans('label.list_paper')}}</a>
										</li>
										<li @if ($page == 'add_sale_kh') class="active" @endif>
											<a href="{{URL::to('/')}}/sale/create?type=2">{{trans('label.add_new_lottery')}}</a>
										</li>
									</ul>
								</li>
								
							</ul>
						</li>
						<!-- <li class="newBuild">
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">លេខបិទ</span></a>
							<ul>
								<li @if ($page == 'close_number') class="active" @endif>
									<a href="{{URL::to('/')}}/closenumber?type=1">បង្ហាញលេខបិទ</a>
								</li>
								<li @if ($page == 'add_close_number') class="active" @endif>
									<a href="{{URL::to('/')}}/closenumber/create">បញ្ចូលលេខបិទ</a>
								</li>
							</ul>
						</li> -->
						<!-- <li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{trans('label.sale')}}</span></a>
							<ul>
										<li @if ($page == 'sale') class="active" @endif>
											<a href="{{URL::to('/')}}/sale?type=1">{{trans('label.list_paper')}}</a>
										</li>
										<li @if ($page == 'add_sale') class="active" @endif>
											<a href="{{URL::to('/')}}/sale/create?type=1">{{trans('label.add_new_lottery')}}</a>
										</li>
								
							</ul>
						</li> -->
						<li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">មើលតារាងលេខ</span></a>
							<ul>
								<li @if ($page == 'reportnumber') class="active" @endif>
											<a href="{{URL::to('/')}}/reportnumber">{{ trans('result.resultDisplay') }}</a>
								</li>
								
							</ul>
						</li>

						<!-- <li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('result.result') }}</span></a>
							<ul>
										<li @if ($page == 'result') class="active" @endif>
											<a href="{{URL::to('/')}}/result?stc_type=1">{{ trans('result.resultDisplay') }}</a>
										</li>
										<li @if ($page == 'add_result') class="active" @endif>
											<a href="{{URL::to('/')}}/result/create?stc_type=1">{{ trans('result.addResult') }}</a>
										</li>
							</ul>
						</li> -->

						<li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('report.report_result') }}</span></a>
							<ul>
								<li @if ($page == 'report') class="active" @endif>
									<a href="{{URL::to('/')}}/report">{{ trans('sidebar.reportDisplay') }}</a>
								</li>
								<li @if ($page == 'reportMain') class="active" @endif>
									<a href="{{URL::to('/')}}/reportmainstaff">មេីលបញ្ចី</a>
								</li>
								
								<!-- <li @if ($page == 'daily_report') class="active" @endif>
									<a href="{{URL::to('/')}}/dailyreport">បញ្ចីប្រចាំថ្ងៃ</a>
								</li>
								<li @if ($page == 'total_daily_report') class="active" @endif>
									<a href="{{URL::to('/')}}/totaldailyreport">បញ្ចីលុយកូនសរុបប្រចាំថ្ងៃ</a>
								</li>
								<li @if ($page == 'money_total_report') class="active" @endif>
									<a href="{{URL::to('/')}}/moneytotalreport">បញ្ចីទឹកលុយសរុប</a>
								</li> -->
								
							</ul>
						</li>

						

						<!-- <li>
							<a href="#"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">{{trans('label.staff_transaction')}}</span></a>
							<ul>
								<li @if ($page == 'reportstaff') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction/reportstaff">របាយការណ៏ លក់បុគ្គលិក</a>
								</li>
								<li @if ($page == 'staff_transction') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction">{{trans('label.view_staff_transaction')}}</a>
								</li>
								<li @if ($page == 'add_staff_transction') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction/create">{{trans('label.add_new')}}</a>
								</li>
							</ul>
						</li> -->
					@elseif(Session::get('roleLot') == 100)
						
						<li class="newBuild">
							<a href="{{URL::to('/')}}/dasboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
						</li>


						<li class="newBuild">
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('label.sale') }} Phone</span></a>
							<ul>
								<li @if ($page == 'salephone') class="active" @endif>
									<a href="{{URL::to('/')}}/salephone?type=1">{{ trans('label.list_paper') }} (VN) Phone</a>
								</li>
								<li @if ($page == 'salephone') class="active" @endif>
									<a href="{{URL::to('/')}}/salephone?type=2">{{ trans('label.list_paper') }} (KH) Phone</a>
								</li>
								<!-- <li @if ($page == 'max2digit') class="active" @endif>
									<a href="{{URL::to('/')}}/maxpricetwo">ឆែកទិន្នន័យឆ្នោត2លេខ</a>
								</li>
								<li @if ($page == 'max3digit') class="active" @endif>
									<a href="{{URL::to('/')}}/maxpricethree">ឆែកទិន្នន័យឆ្នោត3លេខ</a>
								</li> -->
							</ul>
						</li>
						
						<!-- <li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">មើលតារាងលេខ</span></a>
							<ul>
								<li @if ($page == 'reportnumber') class="active" @endif>
											<a href="{{URL::to('/')}}/reportnumber">{{ trans('result.resultDisplay') }}</a>
								</li>
								
							</ul>
						</li> -->

						<!-- <li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('result.result') }}</span></a>
							<ul>
										<li @if ($page == 'result') class="active" @endif>
											<a href="{{URL::to('/')}}/result?stc_type=1">{{ trans('result.resultDisplay') }}</a>
										</li>
										<li @if ($page == 'add_result') class="active" @endif>
											<a href="{{URL::to('/')}}/result/create?stc_type=1">{{ trans('result.addResult') }}</a>
										</li>
							</ul>
						</li> -->

						<li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('report.report_result') }}</span></a>
							<ul>
								
								<li @if ($page == 'reportMain') class="active" @endif>
									<a href="{{URL::to('/')}}/reportmainstaff">មេីលបញ្ចី</a>
								</li>
								
								<!-- <li @if ($page == 'daily_report') class="active" @endif>
									<a href="{{URL::to('/')}}/dailyreport">បញ្ចីប្រចាំថ្ងៃ</a>
								</li>
								<li @if ($page == 'total_daily_report') class="active" @endif>
									<a href="{{URL::to('/')}}/totaldailyreport">បញ្ចីលុយកូនសរុបប្រចាំថ្ងៃ</a>
								</li>
								<li @if ($page == 'money_total_report') class="active" @endif>
									<a href="{{URL::to('/')}}/moneytotalreport">បញ្ចីទឹកលុយសរុប</a>
								</li> -->
								
							</ul>
						</li>
						<!-- <li>
							<a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">មេក្រុម</span></a>
							<ul>
								<li @if ($page == 'staff') class="active" @endif>
									<a href="{{URL::to('/')}}/staff">បង្ហាញមេក្រុម</a>
								</li>
								<li @if ($page == 'add_staff') class="active" @endif>
									<a href="{{URL::to('/')}}/staff/create">{{trans('label.add_new')}}</a>
								</li>
							</ul>
						</li>
						<li class="newBuild">
							<a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">{{trans('label.staff')}} Phone</span></a>
							<ul>
								<li @if ($page == 'staff_mobile' || $page == 'time_close') class="active" @endif>
									<a href="{{URL::to('/')}}/staffmobile">{{trans('label.view_staff')}} Phone</a>
								</li>
								<li @if ($page == 'add_staff_mobile') class="active" @endif>
									<a href="{{URL::to('/')}}/staffmobile/create">{{trans('label.add_new')}} Phone</a>
								</li>
							</ul>
						</li> -->

						

						<!-- <li>
							<a href="#"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">{{trans('label.staff_transaction')}}</span></a>
							<ul>
								<li @if ($page == 'reportstaff') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction/reportstaff">របាយការណ៏ លក់បុគ្គលិក</a>
								</li>
								<li @if ($page == 'staff_transction') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction">{{trans('label.view_staff_transaction')}}</a>
								</li>
								<li @if ($page == 'add_staff_transction') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction/create">{{trans('label.add_new')}}</a>
								</li>
							</ul>
						</li> -->
					@elseif(Session::get('roleLot') == 3)
						<li >
							<a href="{{URL::to('/')}}/dasboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
						</li>
						<li class="newBuild">
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('label.sale') }} Phone</span></a>
							<ul>
								<li @if ($page == 'salephone') class="active" @endif>
									<a href="{{URL::to('/')}}/salephone?type=1">{{ trans('label.list_paper') }} (VN) Phone</a>
								</li>
								<li @if ($page == 'salephone') class="active" @endif>
									<a href="{{URL::to('/')}}/salephone?type=2">{{ trans('label.list_paper') }} (KH) Phone</a>
								</li>
								<!-- <li @if ($page == 'max2digit') class="active" @endif>
									<a href="{{URL::to('/')}}/maxpricetwo">ឆែកទិន្នន័យឆ្នោត2លេខ</a>
								</li>
								<li @if ($page == 'max3digit') class="active" @endif>
									<a href="{{URL::to('/')}}/maxpricethree">ឆែកទិន្នន័យឆ្នោត3លេខ</a>
								</li> -->
							</ul>
						</li>
						<li class="newBuild">
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">លេខបិទ</span></a>
							<ul>
								<li @if ($page == 'close_number') class="active" @endif>
									<a href="{{URL::to('/')}}/closenumber?type=1">បង្ហាញលេខបិទ</a>
								</li>
								<li @if ($page == 'add_close_number') class="active" @endif>
									<a href="{{URL::to('/')}}/closenumber/create">បញ្ចូលលេខបិទ</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">មើលតារាងលេខ</span></a>
							<ul>
								<li @if ($page == 'reportnumber') class="active" @endif>
											<a href="{{URL::to('/')}}/reportnumber">{{ trans('result.resultDisplay') }}</a>
								</li>
								
							</ul>
						</li>

						<li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{ trans('report.report_result') }}</span></a>
							<ul>
								<li @if ($page == 'report') class="active" @endif>
									<a href="{{URL::to('/')}}/report">{{ trans('sidebar.reportDisplay') }}</a>
								</li>
								<li @if ($page == 'reportMain') class="active" @endif>
									<a href="{{URL::to('/')}}/reportmainstaff">មេីលបញ្ចី</a>
								</li>
								
								<!-- <li @if ($page == 'daily_report') class="active" @endif>
									<a href="{{URL::to('/')}}/dailyreport">បញ្ចីប្រចាំថ្ងៃ</a>
								</li>
								<li @if ($page == 'total_daily_report') class="active" @endif>
									<a href="{{URL::to('/')}}/totaldailyreport">បញ្ចីលុយកូនសរុបប្រចាំថ្ងៃ</a>
								</li>
								<li @if ($page == 'money_total_report') class="active" @endif>
									<a href="{{URL::to('/')}}/moneytotalreport">បញ្ចីទឹកលុយសរុប</a>
								</li> -->
								
							</ul>
						</li>
						
						
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">{{trans('label.staff_transaction')}}</span></a>
							<ul>
								<!-- <li @if ($page == 'reportstaff') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction/reportstaff">របាយការណ៏ លក់បុគ្គលិក</a>
								</li> -->
								<li @if ($page == 'staff_transction') class="active" @endif>
									<a href="{{URL::to('/')}}/transction">{{trans('label.view_staff_transaction')}}</a>
								</li>
								<!-- <li @if ($page == 'add_staff_transction') class="active" @endif>
									<a href="{{URL::to('/')}}/stafftransction/create">{{trans('label.add_new')}}</a>
								</li> -->
							</ul>
						</li>

						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">មេក្រុម</span></a>
							<ul>
								<li @if ($page == 'staff') class="active" @endif>
									<a href="{{URL::to('/')}}/staff">បង្ហាញមេក្រុម</a>
								</li>
								<li @if ($page == 'add_staff') class="active" @endif>
									<a href="{{URL::to('/')}}/staff/create">{{trans('label.add_new')}}</a>
								</li>
							</ul>
						</li>
						<li class="newBuild">
							<a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">{{trans('label.staff')}} Phone</span></a>
							<ul>
								<li @if ($page == 'staff_mobile' || $page == 'time_close') class="active" @endif>
									<a href="{{URL::to('/')}}/staffmobile">{{trans('label.view_staff')}} Phone</a>
								</li>
								<li @if ($page == 'add_staff_mobile') class="active" @endif>
									<a href="{{URL::to('/')}}/staffmobile/create">{{trans('label.add_new')}} Phone</a>
								</li>
							</ul>
						</li>
					@else 
						<li>
							<a href="#"><i class="fa fa-lg fa-sort-numeric-asc"></i> <span class="menu-item-parent">{{trans('label.sale')}}</span></a>
							<ul>
										<li @if ($page == 'sale') class="active" @endif>
											<a href="{{URL::to('/')}}/sale?type=1">{{trans('label.list_paper')}}</a>
										</li>
										<li @if ($page == 'add_sale') class="active" @endif>
											<a href="{{URL::to('/')}}/sale/create?type=1">{{trans('label.add_new_lottery')}}</a>
										</li>
								
							</ul>
						</li>
					
					@endif
					
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> 
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>
	