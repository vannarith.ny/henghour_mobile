@extends('master')
@section('title')
<title>របាយការណ៏ ឈ្មួញកណ្តាល</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}
		.backgroundSubTotal{
			background: #eaeaea;
		}

		table.table-bordered.summary_result_per_day{
            border: 1px #fff solid !important;
        }

        .table-bordered>tbody>tr{
            border:none !important;
        }

        .table-bordered>thead th{
            background-image:none !important;
			background: #5698d2;
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td{
            border: 1px #000 solid !important;
            border-bottom: none !important;
        }
        .table-bordered>tbody>tr>th, .table-bordered>thead>tr>th{
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td.clearBorder,.table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }
        .table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }

        .table-bordered>tbody>tr>th.result_right_total{
            border: none !important;
            border-bottom: 1px #000 solid !important;
        }

        /** {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }
        *:before,
        *:after {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }*/
        .table-bordered>tbody>tr>td.lr_clear{
            border-left: none !important;
            border-right: none !important;
        }
        .table-bordered>tbody>tr>th.bottom_clear{
            border-bottom: none !important;
        }

		thead.footer th{
			background: none !important;
            /*border: 1px #000 solid !important;*/
		}
		thead.footer th.backgroundSubTotal{
			background: #5698d2 !important;
            
		}

		.display_total_result tr:nth-child(even) {background: #CCC}
		.display_total_result tr:nth-child(odd) {background: #FFF}
		th.text-right{
			text-align:right !important;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ ឈ្មួញកណ្តាល</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>មេីលបញ្ចី</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body ">


					 {!! Form::open(['route' => 'filterMain', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_staff_chail))
							 <?php $staff_chail_filter = $var_staff_chail; ?>
						 @else
							 <?php $staff_chail_filter = null; ?>
						 @endif

						 

						 @if(isset($var_dateEnd))
							 <?php $endDate_filter = $var_dateEnd; ?>
						 @else
							 <?php $endDate_filter = null; ?>
						 @endif

						
						 <div class="row">

							 <section class="col col-3">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>
							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateEnd", $value = $endDate_filter, $attributes = array('class' => 'form-control ', 'id' => 'dateEnd','placeholder'=> trans('result.dateEnd'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>


							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('s_id_man', ([
                                        '' => 'ឈ្មួញកណ្តាល' ]+$staffMain),$staff_filter,['class' => ' ','id'=>'s_id_man','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>
							 <section class="col col-2">
								 <label class="select">
									@if($staff_filter != '')
										{{ Form::select('s_id', ([
                                        '' => 'កូនក្រុម' ]+$chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@else
										{{ Form::select('s_id', ($chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@endif
									 
									 <i></i>
								 </label>
							 </section>


							 <section class="col col-3">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
								 @if(Session::get('roleLot') != 100)
								 <label class="tesxt">
									 <button type="button" id="printAll" name="printAll" class="btn btn-primary btn-sm btn-filter">Print</button>
								 </label>
								 @endif
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 @if(isset($reports))
					<div id="printElement">
                        <style>
                            table.displayReport{
                                font-size: 12px;
								border-collapse: collapse;
                            }
                            table.displayReport tr th{
                                font-size: 12px;
                                vertical-align: top;
                                text-align:center !important;
								padding: 5px 5px;
								background: #5698d2 !important;
								border: 1px #000 solid !important;
                            }
							table.displayReport tr td{
								padding: 8px 5px;
								border: 1px #000 solid !important;
								
							}
							table.displayReport thead{

							}
							table.displayReport thead.footer th{
								background:none !important;
							}
							table.displayReport thead.footer th.backgroundSubTotal{
								background: #5698d2 !important;
								
							}
							table.displayReport thead.footer th.clearBor{
								border:none !important;
								border-bottom:none !important;
							}
                        </style>
                        <div style="padding-left: 20px; padding-bottom: 20px; font-size: 14px;">
							<span style="padding-left: 50px;">ឈ្មោះកូន: {{$mainNameStaffInfo->s_name}}</span>
							<span style="padding-left: 50px;">កាលបរិច្ឆេទ: {{$var_dateStart}}</span>
                        </div>
						 <table  class="table-bordered-new displayReport" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th rowspan="2" v-align="top">កូន</th>
                                    <th colspan="2" >លទ្ធផលមូយថ្ងៃ</th>
                                    <th colspan="2">លុយទឹក</th>
                                    <th colspan="2">ទឹកលុយសរុប 100%</th>
                                    <th colspan="2" >ភាគរយ ឈ្មួញកណ្ដាល</th>
                                    <th colspan="2" class="background">សរុបចុងក្រោយ</th>
                                </tr>
                                <tr>
                                    <th>រៀល(៛) </th>
                                    <th>ដុល្លា($)</th>

                                    <th>រៀល(៛) </th>
                                    <th>ដុល្លា($)</th>

                                    <th>រៀល(៛) </th>
                                    <th>ដុល្លា($)</th>

                                    <th>រៀល(៛) </th>
                                    <th>ដុល្លា($)</th>

                                    <th class="">រៀល(៛) </th>
                                    <th class="">ដុល្លា($)</th>
                                </tr>
                            </thead>
                            <tbody class="display_total_result">
                            <?php
                                $checkStaff = '';
                                $checkTime = '';
                                $count = 0;

                                $totalMoneyR = 0;
                                $totalMoneyD = 0;

                                $totalcomissionR = 0;
                                $totalcomissionD = 0;

                                $totalRealR = 0;
                                $totalRealD = 0;

                                $totalmidMR = 0;
                                $totalmidMD = 0;

                                $totalMoneyFinalR = 0;
                                $totalMoneyFinalD = 0;

                                
                                $dateGroup = $reports->groupBy('date');
                                //dd($dateGroup);
                            ?>
                            @foreach ($dateGroup as $mainKey => $dataDate)
                                <?php 
                                    $priceR = 0;
                                    $priceD = 0;

                                    $comissionR = 0;
                                    $comissionD = 0;

                                    $realMoneyR = 0;
                                    $realMoneyD = 0;

                                    $percentMainR = 0;
                                    $percentMainD = 0;

                                    $moneyFinalR = 0;
                                    $moneyFinalD = 0;
                                    
                                    foreach($dataDate as $key => $row){

                                    	

                                        //check commission
                                    	// var_dump($row->date);
                                        $data = DB::table("tbl_sum_by_paper")
                                                ->select(DB::raw('sum(price2digit_r) as totalprice2R, sum(price2digit_d) as totalprice2D, sum(price3digit_r) as totalprice3R, sum(price3digit_d) as totalprice3D'))
                                                ->where('s_id', $row->s_id)
                                                ->where('date', $row->date)
												->first();
												
										
                                        $getStaffCharge = DB::table('tbl_staff_charge')
														 ->where('s_id',$row->s_id)
														 ->where('stc_date','<=',$row->date)
														 ->orderBy('stc_date','DESC')
														 ->first();

										$staffBoos = DB::table('tbl_staff_charge_main')
														 ->where('chail_id',$row->s_id)
														 ->where('stc_date','<=',$row->date)
														 ->orderBy('stc_date','DESC')
														 ->first();

										$staffchail = DB::table('tbl_staff')->where('s_id',$row->s_id)->first();



										$totalAmount2R = $row->oneday_price_r;
                                    	$totalAmount2D = $row->oneday_price_d;
                                    	if($staffchail->percent_data > 0){
                                    		$totalAmount2R = $totalAmount2R * (100-$staffchail->percent_data)/100;
                                    		$totalAmount2D = $totalAmount2D * (100-$staffchail->percent_data)/100;
                                    	}

                                        $priceR = $priceR + $totalAmount2R;
                                        $priceD = $priceD + $totalAmount2D;



										// var_dump($getStaffCharge->stc_three_digit_charge,$staffBoos->stc_three_digit_charge,$row->s_id);

                                        if($data){
                                            $totalWater2 = (float) ($getStaffCharge->stc_two_digit_charge - $staffBoos->stc_two_digit_charge);
                                            // var_dump($totalWater2);
                                            $totalWater3 = (float) ($getStaffCharge->stc_three_digit_charge - $staffBoos->stc_three_digit_charge);
                                            // var_dump($data->totalprice2R);

                                            // $amount2R = $data->totalprice2R * ($totalWater2/100);
                                            
                                            // $amount2D = $data->totalprice2D * ($totalWater2/100);

                                            // $amount3R = $data->totalprice3R * ($totalWater3/100);
                                            // $amount3D = $data->totalprice3D * ($totalWater3/100);

                                            $amount2DR = $data->totalprice2R;
	                                        $amount2DD = $data->totalprice2D;
	                                        $amount3DR = $data->totalprice3R;
	                                        $amount3DD = $data->totalprice3D;
	                                        if($staffchail->percent_data > 0){
	                                        	$amount2DR = $amount2DR * (100-$staffchail->percent_data)/100;
	                                        	$amount2DD = $amount2DD * (100-$staffchail->percent_data)/100;
	                                        	$amount3DR = $amount3DR * (100-$staffchail->percent_data)/100;
	                                        	$amount3DD = $amount3DD * (100-$staffchail->percent_data)/100;
	                                        }

	                                        $amount2R = $amount2DR * ($totalWater2/100);
                                            
                                            $amount2D = $amount2DD * ($totalWater2/100);

                                            $amount3R = $amount3DR * ($totalWater3/100);
                                            $amount3D = $amount3DD * ($totalWater3/100);



                                            // var_dump($amount2R);
                                            // var_dump($amount3R);
                                            $totalReal = $amount2R + $amount3R;
                                            $totalDollar = $amount2D + $amount3D;
                                        }else{
                                            $totalReal = 0;
                                            $totalDollar = 0;
                                        }

                                        // $checkPrice = substr($totalReal, -2);
                                        // if($checkPrice <= 50 ){
                                        //     $totalReal = $totalReal - $checkPrice;
                                        // }else{
                                        //     $totalReal = $totalReal + (100 - $checkPrice);
                                        // }

                                        $totalReal = $totalReal * -1;
									    $totalDollar = $totalDollar * -1;

                                        $comissionR = $comissionR + $totalReal;
                                        $comissionD = $comissionD + $totalDollar;

                                        $mainPaymentReal = (float) ($totalAmount2R + $totalReal);
										$mainPaymentDollar = (float) ($totalAmount2D + $totalDollar);

										$realMoneyR = $realMoneyR + $mainPaymentReal;
                                        $realMoneyD = $realMoneyD + $mainPaymentDollar;
                                        
                                        $mainPaymentPercentReal = (float) ($mainPaymentReal * ($staffBoos->percent/100));
										$mainPaymentPercentDollar = (float) ($mainPaymentDollar * ($staffBoos->percent/100));
										$percentMainR = $percentMainR + $mainPaymentPercentReal;
                                        $percentMainD = $percentMainD + $mainPaymentPercentDollar;
                                        
                                        $bossPaymentReal = $mainPaymentReal - $mainPaymentPercentReal;
                                        $bossPaymentDollar = $mainPaymentDollar - $mainPaymentPercentDollar;
										
										$moneyFinalR = $moneyFinalR + $bossPaymentReal;
										$moneyFinalD = $moneyFinalD + $bossPaymentDollar;

                                    
                                    }
                                    
                                ?>

                                @php 
                                    $count = $count + 1;
                                @endphp
                                
                                <tr>
                                    <td align="left" style="text-align:left !important; width: 10%;">{{$mainKey}}</td>
                                    
                                    <!-- // total amount child water -->
                                    <td class=" " align="right" style="width: 5% !important; text-align:right !important;">{{number_format($priceR)}}</td>
                                    <td class=" " align="right" style="width: 7% !important;">{{number_format($priceD,2)}}</td>
									
                                    <!-- // totam amount main water -->
                                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($comissionR) }}</td>
                                    <td class=" " align="right" style="width: 7% !important;">{{ number_format($comissionD,2) }}$</td>
                                    
                                    <!-- get data main payment -->
                                    
                                    <td class=" " align="right" style="width: 5% !important;">{{number_format($realMoneyR)}}</td>
                                    <td class=" " align="right" style="width: 7% !important;">{{number_format($realMoneyD,2)}}$</td>

                                    <!-- percent main  -->
                                    
                                    <td class=" " align="right" style="width: 5% !important;">{{number_format($percentMainR)}}</td>
                                    <td class=" " align="right" style="width: 7% !important;">{{number_format($percentMainD,2)}}$</td>

                                    <!-- percent boss  -->
                                    
                                    <td class="" align="right" style="width: 7% !important;">{{number_format($moneyFinalR)}}</td>
                                    <td class="" style="width: 7% !important;" colspan="2" align="right">{{number_format($moneyFinalD)}}$</td>
                                </tr>

                                <?php 
                                    //block calculate footer total
                                    $totalMoneyR = $totalMoneyR + $priceR;
                                    $totalMoneyD = $totalMoneyD + $priceD;

                                    $totalcomissionR = $totalcomissionR + $comissionR;
                                    $totalcomissionD = $totalcomissionD + $comissionD;

                                    $totalRealR = $totalRealR + $realMoneyR;
                                    $totalRealD = $totalRealD + $realMoneyD;

                                    $totalmidMR = $totalmidMR + $percentMainR;
                                    $totalmidMD = $totalmidMD + $percentMainD;

                                    $totalMoneyFinalR = $totalMoneyFinalR + $moneyFinalR;
                                    $totalMoneyFinalD = $totalMoneyFinalD + $moneyFinalD;
                                ?>
                            @endforeach
							</tbody>
							<thead class="footer">
								<tr>
                                    <th colspan="1" align="right" class="backgroundSubTotal" style="text-align: right !important;">សរុប</th>

                                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalMoneyR) }}</th>
                                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalMoneyD, 2) }}$</th>
									<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalcomissionR) }}</th>
                                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalcomissionD,2) }}$</th>
									<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRealR) }}</th>
                                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRealD, 2) }}$</th>
									<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalmidMR) }}</th>
                                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalmidMD,2) }}$</th>
									<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalMoneyFinalR) }}</th>
                                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalMoneyFinalD, 2) }}$</th>
                                </tr>

								

                                
                            </thead>
                         </table>
                        <style>
                            .background{
                                background: #c7c7c7;
                            }
                            .blueColor{
                                color: #0000ff !important;
                                font-size: 12px !important;
                            }
                            .redColor{
                                color: #ff0000 !important;
                                font-size: 12px !important;
                            }
                        </style>

					 @endif

		    
		         </div>
		         <!-- end widget content -->
				 </div>
		        </div>
		        <!-- end widget div -->
				<input type='button' id='btnPrintDiv' value='Print'  style="float:right;">
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

	function printDiv() 
	{

		var divToPrint=document.getElementById('displayReportPrint');

		var newWin=window.open('','Print-Window');

		newWin.document.open();

		newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

		newWin.document.close();

		setTimeout(function(){newWin.close();},10);

	}

	$(document).ready(function() {
		
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
        });
        
        $(document).off('submit', '#filter').on('submit', '#filter', function(e){
			if (validate_form_main('#filter') == 0) {
				return true;
			}
			return false;
		});

		$(document).off('change', '#s_id_man').on('change', '#s_id_man', function(e){
			
			var id = $(this).val();
			$("#s_id").prop( "disabled", true );
			$.ajax({
				url: '/reportmainstaff/getstaffchild',
				type: 'GET',
				data: {s_id:id},
				success: function(data) {
					if(data.status=="success"){

						$("#s_id").html(data.msg);

						$("#s_id").prop( "disabled", false );

						console.log(data.msg);

					}else{
					}
				}
			});

		});

		$( "#btnPrintDiv" ).on( "click", function() {
			var divID = 'printElement';
			console.log('okokok');
			printDiv(divID);
		});

	

	});


	//document.getElementById('btnPrintDiv').addEventListener ("click", print)
	/*function print() {
		printJS({
			printable: 'printElement',
			type: 'html',
			targetStyles: ['*']
		})
	}*/
	// Print.js
	//!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define("print-js",[],t):"object"==typeof exports?exports["print-js"]=t():e["print-js"]=t()}(this,function(){return function(e){function t(i){if(n[i])return n[i].exports;var o=n[i]={i:i,l:!1,exports:{}};return e[i].call(o.exports,o,o.exports,t),o.l=!0,o.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,i){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:i})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p="./",t(t.s=10)}([function(e,t,n){"use strict";function i(e,t){if(e.focus(),r.a.isEdge()||r.a.isIE())try{e.contentWindow.document.execCommand("print",!1,null)}catch(t){e.contentWindow.print()}r.a.isIE()||r.a.isEdge()||e.contentWindow.print(),r.a.isIE()&&"pdf"===t.type&&setTimeout(function(){e.parentNode.removeChild(e)},2e3),t.showModal&&a.a.close(),t.onLoadingEnd&&t.onLoadingEnd()}function o(e,t,n){void 0===e.naturalWidth||0===e.naturalWidth?setTimeout(function(){o(e,t,n)},500):i(t,n)}var r=n(1),a=n(3),d={send:function(e,t){document.getElementsByTagName("body")[0].appendChild(t);var n=document.getElementById(e.frameId);"pdf"===e.type&&(r.a.isIE()||r.a.isEdge())?n.setAttribute("onload",i(n,e)):t.onload=function(){if("pdf"===e.type)i(n,e);else{var t=n.contentWindow||n.contentDocument;t.document&&(t=t.document),t.body.innerHTML=e.htmlData,"image"===e.type?o(t.getElementById("printableImage"),n,e):i(n,e)}}}};t.a=d},function(e,t,n){"use strict";var i={isFirefox:function(){return"undefined"!=typeof InstallTrigger},isIE:function(){return-1!==navigator.userAgent.indexOf("MSIE")||!!document.documentMode},isEdge:function(){return!i.isIE()&&!!window.StyleMedia},isChrome:function(){return!!window.chrome&&!!window.chrome.webstore},isSafari:function(){return Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor")>0||-1!==navigator.userAgent.toLowerCase().indexOf("safari")}};t.a=i},function(e,t,n){"use strict";function i(e,t){return'<div style="font-family:'+t.font+" !important; font-size: "+t.font_size+' !important; width:100%;">'+e+"</div>"}function o(e){return e.charAt(0).toUpperCase()+e.slice(1)}function r(e,t){var n=document.defaultView||window,i=[],o="";if(n.getComputedStyle){i=n.getComputedStyle(e,"");var r=t.targetStyles||["border","box","break","text-decoration"],a=t.targetStyle||["clear","display","width","min-width","height","min-height","max-height"];t.honorMarginPadding&&r.push("margin","padding"),t.honorColor&&r.push("color");for(var d=0;d<i.length;d++)for(var l=0;l<r.length;l++)"*"!==r[l]&&-1===i[d].indexOf(r[l])&&-1===a.indexOf(i[d])||(o+=i[d]+":"+i.getPropertyValue(i[d])+";")}else if(e.currentStyle){i=e.currentStyle;for(var s in i)-1!==i.indexOf("border")&&-1!==i.indexOf("color")&&(o+=s+":"+i[s]+";")}return o+="max-width: "+t.maxWidth+"px !important;"+t.font_size+" !important;"}function a(e,t){for(var n=0;n<e.length;n++){var i=e[n],o=i.tagName;if("INPUT"===o||"TEXTAREA"===o||"SELECT"===o){var d=r(i,t),l=i.parentNode,s="SELECT"===o?document.createTextNode(i.options[i.selectedIndex].text):document.createTextNode(i.value),c=document.createElement("div");c.appendChild(s),c.setAttribute("style",d),l.appendChild(c),l.removeChild(i)}else i.setAttribute("style",r(i,t));var p=i.children;p&&p.length&&a(p,t)}}function d(e,t,n){var i=document.createElement("h1"),o=document.createTextNode(t);i.appendChild(o),i.setAttribute("style",n),e.insertBefore(i,e.childNodes[0])}t.a=i,t.b=o,t.c=r,t.d=a,t.e=d},function(e,t,n){"use strict";var i={show:function(e){var t=document.createElement("div");t.setAttribute("style","font-family:sans-serif; display:table; text-align:center; font-weight:300; font-size:30px; left:0; top:0;position:fixed; z-index: 9990;color: #0460B5; width: 100%; height: 100%; background-color:rgba(255,255,255,.9);transition: opacity .3s ease;"),t.setAttribute("id","printJS-Modal");var n=document.createElement("div");n.setAttribute("style","display:table-cell; vertical-align:middle; padding-bottom:100px;");var o=document.createElement("div");o.setAttribute("class","printClose"),o.setAttribute("id","printClose"),n.appendChild(o);var r=document.createElement("span");r.setAttribute("class","printSpinner"),n.appendChild(r);var a=document.createTextNode(e.modalMessage);n.appendChild(a),t.appendChild(n),document.getElementsByTagName("body")[0].appendChild(t),document.getElementById("printClose").addEventListener("click",function(){i.close()})},close:function(){var e=document.getElementById("printJS-Modal");e.parentNode.removeChild(e)}};t.a=i},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=n(7),o=i.a.init;"undefined"!=typeof window&&(window.printJS=o),t.default=o},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.getElementById(e.printable);if(!r)return window.console.error("Invalid HTML element id: "+e.printable),!1;var a=document.createElement("div");a.appendChild(r.cloneNode(!0)),a.setAttribute("style","display:none;"),a.setAttribute("id","printJS-html"),r.parentNode.appendChild(a),a=document.getElementById("printJS-html"),a.setAttribute("style",n.i(i.c)(a,e)+"margin:0 !important;");var d=a.children;n.i(i.d)(d,e),e.header&&n.i(i.e)(a,e.header,e.headerStyle),a.parentNode.removeChild(a),e.htmlData=n.i(i.a)(a.innerHTML,e),o.a.send(e,t)}}},function(e,t,n){"use strict";var i=n(2),o=n(0);t.a={print:function(e,t){var r=document.createElement("img");r.src=e.printable,r.onload=function(){r.setAttribute("style","width:100%;"),r.setAttribute("id","printableImage");var a=document.createElement("div");a.setAttribute("style","width:100%"),a.appendChild(r),e.header&&n.i(i.e)(a,e.header,e.headerStyle),e.htmlData=a.outerHTML,o.a.send(e,t)}}}},function(e,t,n){"use strict";var i=n(1),o=n(3),r=n(9),a=n(5),d=n(6),l=n(8),s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},c=["pdf","html","image","json"];t.a={init:function(){var e={printable:null,fallbackPrintable:null,type:"pdf",header:null,headerStyle:"font-weight: 300;",maxWidth:800,font:"TimesNewRoman",font_size:"12pt",honorMarginPadding:!0,honorColor:!1,properties:null,gridHeaderStyle:"font-weight: bold;",gridStyle:"border: 1px solid lightgray; margin-bottom: -1px;",showModal:!1,onLoadingStart:null,onLoadingEnd:null,modalMessage:"Retrieving Document...",frameId:"printJS",htmlData:"",documentTitle:"Document",targetStyle:null,targetStyles:null},t=arguments[0];if(void 0===t)throw new Error("printJS expects at least 1 attribute.");switch(void 0===t?"undefined":s(t)){case"string":e.printable=encodeURI(t),e.fallbackPrintable=e.printable,e.type=arguments[1]||e.type;break;case"object":e.printable=t.printable,e.fallbackPrintable=void 0!==t.fallbackPrintable?t.fallbackPrintable:e.printable,e.type=void 0!==t.type?t.type:e.type,e.frameId=void 0!==t.frameId?t.frameId:e.frameId,e.header=void 0!==t.header?t.header:e.header,e.headerStyle=void 0!==t.headerStyle?t.headerStyle:e.headerStyle,e.maxWidth=void 0!==t.maxWidth?t.maxWidth:e.maxWidth,e.font=void 0!==t.font?t.font:e.font,e.font_size=void 0!==t.font_size?t.font_size:e.font_size,e.honorMarginPadding=void 0!==t.honorMarginPadding?t.honorMarginPadding:e.honorMarginPadding,e.properties=void 0!==t.properties?t.properties:e.properties,e.gridHeaderStyle=void 0!==t.gridHeaderStyle?t.gridHeaderStyle:e.gridHeaderStyle,e.gridStyle=void 0!==t.gridStyle?t.gridStyle:e.gridStyle,e.showModal=void 0!==t.showModal?t.showModal:e.showModal,e.onLoadingStart=void 0!==t.onLoadingStart?t.onLoadingStart:e.onLoadingStart,e.onLoadingEnd=void 0!==t.onLoadingEnd?t.onLoadingEnd:e.onLoadingEnd,e.modalMessage=void 0!==t.modalMessage?t.modalMessage:e.modalMessage,e.documentTitle=void 0!==t.documentTitle?t.documentTitle:e.documentTitle,e.targetStyle=void 0!==t.targetStyle?t.targetStyle:e.targetStyle,e.targetStyles=void 0!==t.targetStyles?t.targetStyles:e.targetStyles;break;default:throw new Error('Unexpected argument type! Expected "string" or "object", got '+(void 0===t?"undefined":s(t)))}if(!e.printable)throw new Error("Missing printable information.");if(!e.type||"string"!=typeof e.type||-1===c.indexOf(e.type.toLowerCase()))throw new Error("Invalid print type. Available types are: pdf, html, image and json.");e.showModal&&o.a.show(e),e.onLoadingStart&&e.onLoadingStart();var n=document.getElementById(e.frameId);n&&n.parentNode.removeChild(n);var p=void 0;switch(p=document.createElement("iframe"),p.setAttribute("style","display:none;"),p.setAttribute("id",e.frameId),"pdf"!==e.type&&(p.srcdoc="<html><head><title>"+e.documentTitle+"</title></head><body></body></html>"),e.type){case"pdf":if(i.a.isFirefox()||i.a.isEdge()||i.a.isIE()){window.open(e.fallbackPrintable,"_blank").focus(),e.showModal&&o.a.close(),e.onLoadingEnd&&e.onLoadingEnd()}else r.a.print(e,p);break;case"image":d.a.print(e,p);break;case"html":a.a.print(e,p);break;case"json":l.a.print(e,p);break;default:throw new Error("Invalid print type. Available types are: pdf, html, image and json.")}}}},function(e,t,n){"use strict";function i(e){var t=e.printable,i=e.properties,r='<div style="display:flex; flex-direction: column;">';r+='<div style="flex:1 1 auto; display:flex;">';for(var a=0;a<i.length;a++)r+='<div style="flex:1; padding:5px;'+e.gridHeaderStyle+'">'+n.i(o.b)(i[a])+"</div>";r+="</div>";for(var d=0;d<t.length;d++){r+='<div style="flex:1 1 auto; display:flex;">';for(var l=0;l<i.length;l++){var s=t[d],c=i[l].split(".");if(c.length>1)for(var p=0;p<c.length;p++)s=s[c[p]];else s=s[i[l]];r+='<div style="flex:1; padding:5px;'+e.gridStyle+'">'+s+"</div>"}r+="</div>"}return r+="</div>"}var o=n(2),r=n(0),a="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};t.a={print:function(e,t){if("object"!==a(e.printable))throw new Error("Invalid javascript data object (JSON).");if(!e.properties||"object"!==a(e.properties))throw new Error("Invalid properties array for your JSON data.");var d="";e.header&&(d+='<h1 style="'+e.headerStyle+'">'+e.header+"</h1>"),d+=i(e),e.htmlData=n.i(o.a)(d,e),r.a.send(e,t)}}},function(e,t,n){"use strict";function i(e,t){t.setAttribute("src",e.printable),r.a.send(e,t)}var o=n(1),r=n(0);t.a={print:function(e,t){if(e.showModal||e.onLoadingStart||o.a.isIE()){var n=new window.XMLHttpRequest;n.addEventListener("load",i(e,t)),n.open("GET",window.location.origin+"/"+e.printable,!0),n.send()}else i(e,t)}}},function(e,t,n){e.exports=n(4)}])});
	
	function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
		console.log(divElements,'in');
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;
        //Reset the page's HTML with div's HTML only
        document.body.innerHTML = 
          "<html><head><title></title></head><body>" + 
          divElements + "</body>";
        //Print Page
        window.print();
        //Restore orignal HTML
        document.body.innerHTML = oldPage;

    }
	</script>
@stop