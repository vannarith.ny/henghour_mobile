@extends('master')
<?php
use App\Http\Controllers\SaleController;
?>
@section('title')
<title>របាយការណ៏</title>
@stop

@section('cssStyle')
	<style type="text/css">
		

	</style>

	<style type="text/css">

		@font-face {
		    font-family: 'KhmerOSSiemreap';
		    src: url('fonts/KhmerOSSiemreap.eot?#iefix') format('embedded-opentype'),  url('fonts/KhmerOSSiemreap.woff') format('woff'), url('fonts/KhmerOSSiemreap.ttf')  format('truetype'), url('fonts/KhmerOSSiemreap.svg#KhmerOSSiemreap') format('svg');
		    font-weight: bold;
		    font-style: normal;
		 }

		@font-face {
		  font-family: 'Battambang';
		  src: url('../fonts/Battambang.eot');
		  src: url('../fonts/Battambang.eot?#iefix') format('embedded-opentype'),
		      url('../fonts/Battambang.woff2') format('woff2'),
		      url('../fonts/Battambang.woff') format('woff'),
		      url('../fonts/Battambang.ttf') format('truetype'),
		      url('../fonts/Battambang.svg#Battambang') format('svg');
		  font-weight: bold;
		  font-style: normal;
		}


		.colume_style{
			padding-bottom:25px;
			padding-top:15px;

			font-family: 'KhmerOSSiemreap';
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:18px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color: #f5f5f5;
			font-size: 28px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:black;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		/*#td_staff_0{
			font-size: 20px;
		}*/
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
			font-family: 'KhmerOSSiemreap';
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 24px;
			font-family: 'KhmerOSSiemreap';
		}
		table.summary_result_per_day td{
			/*font-size: 18px;*/
			font-size: 20px;
			font-weight: bold;

		}
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 4px 6px !important;
			font-family: 'KhmerOSSiemreap';
		}

		tr.bee_highlight th:last-child{
			width: 20% !important;
		}

		.jarviswidget>div{
			font-size: 24px;
		}

		.blueColor{
			color: black !important;
		}
		.redColor{
			color: red !important;
		}

		.backgroundNew th{
			background-color: #c3c9f1;
			border: 1px solid #ddd !important;
			border-bottom-width: 1px !important;
			font-family: 'KhmerOSSiemreap';
		}
		table.table-bordered tbody th{
			border: 1px solid #ddd !important;
			border-bottom-width: 1px !important;
			font-family: 'KhmerOSSiemreap';
		}

		.paperStyle{
			width: 1200px;
		}

		.bee_highlight.needRed th, td.needRed, th.needRed{
			color: red;
		}

		.headerreport{
			background-color: #f5f5f5;
		    /*background-image: -webkit-gradient(linear,0 0,0 100%,from(#f2f2f2),to(#fafafa));
		    background-image: -webkit-linear-gradient(top,#f2f2f2 0,#fafafa 100%);*/
		    padding-top: 10px;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		
		    
		<!-- widget grid -->
		<section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div role="content">
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding" style="">


                 {!! Form::open(['route' => 'filterMain', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_staff_chail))
							 <?php $staff_chail_filter = $var_staff_chail; ?>
						 @else
							 <?php $staff_chail_filter = null; ?>
						 @endif

						 

						 @if(isset($var_dateEnd))
							 <?php $endDate_filter = $var_dateEnd; ?>
						 @else
							 <?php $endDate_filter = null; ?>
						 @endif

						
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>
							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateEnd", $value = $endDate_filter, $attributes = array('class' => 'form-control ', 'id' => 'dateEnd','placeholder'=> trans('result.dateEnd'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>


							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('s_id_man', ([
                                        '' => 'ឈ្មួញកណ្តាល' ]+$staffMain),$staff_filter,['class' => ' ','id'=>'s_id_man','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>
							 <section class="col col-2">
								 <label class="select">
								 	@if($staff_filter != '')
										{{ Form::select('s_id', ([
                                        '' => 'កូនក្រុម' ]+$chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@else
										{{ Form::select('s_id', ($chilStaffs),$staff_chail_filter,['class' => ' ','id'=>'s_id','sms'=> trans('result.pleaseChooseStaff') ]
										) }}
									@endif
									 <i></i>
								 </label>
							 </section>
							 <section class="col col-1">
								 <label class="select">
									 {{ Form::select('type_lottery', ([
                                        '' => trans('result.chooseTypeLottery') ]+$type_lottery),$var_type_lottery,['class' => ' ','id'=>'type_lottery','sms'=> trans('result.pleaseChooseTypeLottery') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>


							 <section class="col col-3">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
								 @if(Session::get('roleLot') != 100)
								 <label class="tesxt">
									 <button type="button" id="printAll" name="printAll" class="btn btn-primary btn-sm btn-filter">Print</button>
								 </label>
								 @endif
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 


					<!-- Block display report -->
						<?php 
							 $countDolla = 0;

							 $count1DRWin = 0;
							 $count2DRWin = 0;
							 $count3DRWin = 0;
							 $count4DRWin = 0;

							 $count1DDWin = 0;
							 $count4DDWin = 0;

							 $win_one_digit = 8;
							 $win_four_digit = 4000;

							 $totalCSpanR = 3;
							 $totalCSpanD = 3;

							 $countRowSP = 0;
							 $totalCountSpan = 0;

							 $colspanDollar = 2;
							 $coltwoD = 4;
						?>
					@if(isset($boss_data))

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 

							 .paperStyle{
								 display: none;
							 }
							 .paperStyle:last-child{
								 display: block !important;
							 }
							 .staff_name{
								 display: none;
							 }
							 
						 </style>



						 <div class="widget-body no-padding ">

							

							 <?php
							 $twodigit_charge = 0;
							 $threedigit_charge = 0;
							 // $var_type_lottery = 1;
							 $colspan = 3;
							 $hidedata = '';
							 $sumHide = '';
						 	$colspan = 3;
						 	$spaceRow = 10;
							 // var_dump($var_dateStart);
							 $getStaffCharge = DB::table('tbl_staff_charge')
									 ->select('stc_id','stc_three_digit_charge','stc_two_digit_charge','stc_pay_two_digit','stc_pay_there_digit','stc_type')
									 ->where('s_id',$staffInfo->s_id)
									 ->where('stc_type',$var_type_lottery)
									 ->where('stc_date','<=',$var_dateStart)
									 ->orderBy('stc_date','DESC')
									 ->first();
								 // dd($getStaffCharge);
							 if(isset($getStaffCharge)){
								 $twodigit_charge = $getStaffCharge->stc_two_digit_charge;
								 $threedigit_charge = $getStaffCharge->stc_three_digit_charge;
								 // if($twodigit_charge == $threedigit_charge){
								 // 	$hidedata = 'hidetwothree_digit';
								 // 	$sumHide = '';
								 // 	$colspan = 1;
								 // 	$spaceRow = 9;
								 // }else{
								 // 	$hidedata = '';
								 // 	$sumHide = 'hidetwothree_digit';
								 // 	$colspan = 2;
								 // 	$spaceRow = 10;
								 // }

								$hidedata = '';
							 	$sumHide = '';
							 	$colspan = 3;
							 	$spaceRow = 10;

							 	$var_type_lottery = $getStaffCharge->stc_type;



								 if($var_type_lottery == 1 ){
									if($getStaffCharge->stc_pay_two_digit > 0){
										$win_two_digit = $getStaffCharge->stc_pay_two_digit;
									}else{
										$win_two_digit = 70;
									}

									if($getStaffCharge->stc_pay_there_digit > 0){
										$win_three_digit = $getStaffCharge->stc_pay_there_digit;
									}else{
										$win_three_digit = 600;
									}
								 }else if($var_type_lottery == 2 || $var_type_lottery == 3 || $var_type_lottery == 4){

									if($getStaffCharge->stc_pay_two_digit > 0){
										$win_two_digit = $getStaffCharge->stc_pay_two_digit;
									}else{
										$win_two_digit = 90;
									}

									if($getStaffCharge->stc_pay_there_digit > 0){
										$win_three_digit = $getStaffCharge->stc_pay_there_digit;
									}else{
										$win_three_digit = 800;
									}

								 }


							 }else{


							 	 
								 // if($twodigit_charge == $threedigit_charge){
								 // 	$hidedata = 'hidetwothree_digit';
								 // 	$sumHide = '';
								 // 	$colspan = 1;
								 // 	$spaceRow = 9;
								 // }else{
								 // 	$hidedata = '';
								 // 	$sumHide = 'hidetwothree_digit';
								 // 	$colspan = 2;
								 // 	$spaceRow = 10;
								 // }

								$hidedata = '';
							 	$sumHide = '';
							 	$colspan = 3;
							 	$spaceRow = 10;;



								if($var_type_lottery == 1 ){

								 	$twodigit_charge = $mainNameStaffInfo->s_two_digit_charge;
								 	$threedigit_charge = $mainNameStaffInfo->s_three_digit_charge;

									if($mainNameStaffInfo->s_two_digit_charge > 0){
										$win_two_digit = $mainNameStaffInfo->s_two_digit_paid;
									}else{
										$win_two_digit = 70;
									}

									if($mainNameStaffInfo->s_three_digit_paid > 0){
										$win_three_digit = $mainNameStaffInfo->s_three_digit_paid;
									}else{
										$win_three_digit = 600;
									}
								}else if($var_type_lottery == 2){

								 	$twodigit_charge = $mainNameStaffInfo->s_two_digit_kh_charge;
								 	$threedigit_charge = $mainNameStaffInfo->s_three_digit_kh_charge;

									if($mainNameStaffInfo->s_two_digit_kh_paid > 0){
										$win_two_digit = $mainNameStaffInfo->s_two_digit_kh_paid;
									}else{
										$win_two_digit = 90;
									}

									if($mainNameStaffInfo->s_three_digit_kh_paid > 0){
										$win_three_digit = $mainNameStaffInfo->s_three_digit_kh_paid;
									}else{
										$win_three_digit = 800;
									}

								}

							 	

							 }






							 ?>
								 <div id="html-content-holder" class="widget-body paperStyle">
								 	@if(isset($staffInfo))
									 <h1>
									 	{{trans('label.summary_result_per_day')}}
									 	<b>
									 	@if($var_type_lottery == 1)
									 		VN
									 	@elseif($var_type_lottery == 2)
									 		KH
									 	@else
									 		TH
									 	@endif
									 	</b>
									 </h1><br>
									 <div class="col-md-12 headerreport" style="text-align: center;">
										 {{trans('label.date')}} :
										 <b><?php echo date("d-m-Y", strtotime($var_dateStart));?></b>
									 </div>

									 <div class="col-md-12 headerreport" style="text-align: center;">
										 <b>{{$staffInfo->s_name}} </b>
									 </div>


									 
									 @endif
									 <br><br>
									 <table id="" class="table table-bordered summary_result_per_day" style="width:100%;">
										 <thead>
										 <tr>
										 	 <th>

										 	 	@if($var_type_lottery == 1)
											 		VN
											 	@elseif($var_type_lottery == 2)
											 		KH
											 	@else
											 		TH
											 	@endif
										 	 </th>
											 <th colspan="2">
											 	
											 </th>
											 <th colspan="{{$colspan}}">KHR</th>
											 <th colspan="4" class="winReal">លេខត្រូវ </th>
											 <th colspan="{{$colspan}}" class="columDollar">USD</th>
											 <th class="winDollar columDollar" colspan="4">លេខត្រូវ</th>
											 <!-- <th style="font-size:16px !important; ">ពិនិត្យ</th> -->
										 </tr>
										 <tr>
										 	 <th>ឈ្មោះ</th>
											 <th>ពេល</th>
											 <th>សន្លឹក</th>
											 <th class="{{$sumHide}}">សរុប</th>
											 <th class="{{$hidedata}}">2D</th>
											 <th class="{{$hidedata}}">3D</th>
											 
											 <th class="1DigitR">1D</th>
											 <th class="2DigitR">2D</th>
											 <th class="3DigitR">3D</th>
											 <th class="4DigitR">4D</th>
											 <th class="{{$sumHide}} columDollar">សរុប</th>
											 <th class="{{$hidedata}} columDollar">2D</th>
											 <th class="{{$hidedata}} columDollar">3D</th>
											 
											 
											 <th class="columDollar oneDD">1D</th>
											 <th class="columDollar">2D</th>
											 <th class="columDollar">3D</th>
											 <th class="columDollar fourDD">4D</th>
											 <!-- <th></th> -->
										 </tr>
										 </thead>
										 <tbody class="display_total_result">
										 	<?php 
										 		$staff_n = '';
										 		$time_n = '';

										 		
										 	?>
										 	@foreach($boss_data as $key => $data)
										 		<?php 
											 		if($staff_n != $data->staff_value){
											 			$name_staff_display = $data->staff_value;
											 			$staff_n = $data->staff_value;

											 			$time_n = '';
											 		}else{
											 			$name_staff_display = '';
											 		}

											 		if($time_n != $data->time_value){
											 			$time_display = $data->time_value;

											 			$find = array("0","1","2","3","4","5");
														$replace = array("");
														$time_display = str_replace($find,$replace,$time_display);

											 			$time_n = $data->time_value;
											 		}else{
											 			$time_display = '';
											 		}
											 		// var_dump(number_format($data->total2digitr));
											 		// dd($main_new);
											 		if($main_new == 1 ){
											 			$price_calculate = DB::table('tbl_staff_charge_main')
											 								->where('s_id',$staffInfo->s_id)
											 								->where('chail_id',$data->id_staff)
											 								->orderBy('stc_date','DESC')
											 								->take(1)
											 								->get();
											 			foreach ($price_calculate as $keyss => $data_ss) {
											 				$cal_2_digit = $data_ss->stc_two_digit_charge;
											 				$cal_3_digit = $data_ss->stc_three_digit_charge;
											 			}
											 			
											 		}else{
											 			$cal_2_digit = $twodigit_charge;
											 			$cal_3_digit = $threedigit_charge;
											 		}

											 	// dd($data);
										 		?>
										 		<tr class="many_row">
										 			<td id="td_staff_{{$key}}" id_staff="{{$data->id_staff}}" two="{{$cal_2_digit}}" three="{{$cal_3_digit}}">
										 				@if($main_new == 0)
										 					{{$data->p_code_p}}
										 				@else
										 					{{$name_staff_display}}
										 				@endif
										 			</td>
										 			<td id="td_time_'+index+'">{{$time_display}}</td>
										 			<td id="td_page_'+index+'">{{$data->page_value}}</td>

										 			<td class="{{$sumHide}} totalsum" >@if($data->totalsum != 0) {{ floatval($data->totalsum) }} @endif</td>
										 			<td class="{{$hidedata}} total2digitr" >@if($data->total2digitr != 0) {!! floatval($data->total2digitr) !!} @endif</td>
										 			<td class="{{$hidedata}} total3digitr" >@if($data->total3digitr != 0) {!! floatval($data->total3digitr) !!} @endif</td>
										 			

										 			<td class="total1digitrright needRed" style="">@if($data->total1digitrright != 0) {!! floatval($data->total1digitrright) !!} @endif</td>

										 			<td class="total2digitrright needRed">@if($data->total2digitrright != 0) {{ floatval($data->total2digitrright) }} @endif</td>

										 			<td class="total3digitrright needRed" style="">@if($data->total3digitrright != 0) {!! floatval($data->total3digitrright) !!} @endif</td>

										 			<td class="total4digitrright needRed">@if($data->total4digitrright != 0) {{ floatval($data->total4digitrright) }} @endif</td>

										 			
										 			<td class="{{$sumHide}} totalsum_dolla columDollar">@if($data->totalsum_dolla != 0) {{ floatval($data->totalsum_dolla) }} @endif</td>
										 			<td class="{{$hidedata}} total2digits columDollar">@if($data->total2digits != 0) {{ floatval($data->total2digits) }} @endif</td>
										 			<td class="{{$hidedata}} total3digits columDollar">@if($data->total3digits != 0) {{ floatval($data->total3digits) }} @endif</td>

										 			

										 			<td class="total1digitsright columDollar oneDD needRed">@if($data->total1digitsright != 0) {{ floatval($data->total1digitsright) }} @endif</td>
										 			
										 			<td class="total2digitsright columDollar needRed">@if($data->total2digitsright != 0) {{ floatval($data->total2digitsright) }} @endif</td>

										 			<td class="total3digitsright columDollar needRed">@if($data->total3digitsright != 0) {{ floatval($data->total3digitsright) }} @endif</td>

										 			<td class="total4digitsright columDollar fourDD needRed">@if($data->total4digitsright != 0) {{ floatval($data->total4digitsright) }} @endif</td>

										 			<!-- <td class="status_report">
										 				@if($data->completed == 1)
										 					<img src="<?php echo asset("img/correct.png"); ?>" alt="បូកផ្ទៀងត្រឹមត្រូវ" style="width: 20px;">
										 				@else
										 					<img src="<?php echo asset("img/cross.png"); ?>" alt="បូកផ្ទៀងមិនត្រឹមត្រូវ" style="width: 20px;">
										 				@endif
										 				

										 			</td> -->

										 		</tr>

										 		<?php 
										 			$countDolla = $countDolla + ($data->total2digits+$data->total3digits);

										 			$count1DRWin = $count1DRWin + $data->total1digitrright;
										 			$count2DRWin = $count2DRWin + $data->total2digitrright;
										 			$count3DRWin = $count3DRWin + $data->total3digitrright;
										 			$count4DRWin = $count4DRWin + $data->total4digitrright;

										 			$count1DDWin = $count1DDWin + $data->total1digitsright;
													$count4DDWin = $count4DDWin + $data->total4digitsright;
										 		?>
										 	@endforeach
										 </tbody>
									 </table>
								</div>
						 </div> <!-- widget-body no-padding -->

						 



					@endif
					<?php 
						 if($countDolla == 0){
						 	$totalCountSpan = $totalCountSpan - 2;
		 					$totalCSpanD = 0;
						 }

						 if($count1DRWin > 0){
						 	$countRowSP = $countRowSP + 1;
						 }
						 if($count2DRWin >= 0){
						 	$countRowSP = $countRowSP + 1;
						 }
						 if($count3DRWin >= 0){
						 	$countRowSP = $countRowSP + 1;
						 }
						 if($count4DRWin > 0){
						 	$countRowSP = $countRowSP + 1;
						 }

						 if($count1DDWin > 0){
						 	$colspanDollar++;
						 }
						 if($count4DDWin > 0){
						 	$colspanDollar++;
						 }
					 // var_dump($count1DDWin);
					?>






		    		<!-- end Block display report -->
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
				<div style="text-align: center;">
				<!-- <input type='button' id='btnPrintDiv' value='Print'  style="float:none;"> -->
				</div>
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>

	<style type="text/css">
		@if($countDolla == 0)
		.columDollar{
			display: none !important;
		}
		@endif

		@if($count1DDWin == 0)
	 		.oneDD{
				display: none !important;
			}
			<?php 
			$coltwoD =  $coltwoD - 1;
			?>
	 	@endif
	 	@if($count4DDWin == 0)
	 		.fourDD{
				display: none !important;
			}
			<?php 
			$coltwoD =  $coltwoD - 1;
			?>
	 	@endif


	</style>


<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
<script type="text/javascript">

	

	$(document).off('submit', '#filter').on('submit', '#filter', function(e){
		if (validate_form_main('#filter') == 0) {
			return true;
		}
		return false;
	});


       
	$(document).ready(function() {
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
		});
	});
	$(document).ready(function(){


        $(document).off('change', '.customFormpCodep').on('change', '.customFormpCodep', function(e){
			
			var codeVal = $(this).val();
			var pIDList = $(this).attr('pageID');
			var pDate = $(this).attr('date');
			var thisID = $(this);
			var oldData = $(this).attr('oldData');
			var p_id = $(this).attr('p_id');
			
			// if(codeVal != ''){
				$.ajax({
					url: '/reportmainstaff/updatecodep',
					type: 'GET',
					data: {codeVal:codeVal, pIDList:pIDList, pDate:pDate, p_id:p_id},
					success: function(data) {
						if(data.status=="success"){

							thisID.val(data.data);
							thisID.attr('oldData',data.data);


							console.log(data.msg);

						}else{
						}
					}
				});
			// }else{
			// 	thisID.val(oldData);
			// }
			
		});

		$(document).off('change', '.customFormpCode').on('change', '.customFormpCode', function(e){
			
			var codeVal = $(this).val();
			var pIDList = $(this).attr('pageID');
			var pDate = $(this).attr('date');
			var thisID = $(this);
			var oldData = $(this).attr('oldData');
			var p_id = $(this).attr('p_id');
			
			// if(codeVal != ''){
				$.ajax({
					url: '/reportmainstaff/updatecode',
					type: 'GET',
					data: {codeVal:codeVal, pIDList:pIDList, pDate:pDate, p_id:p_id},
					success: function(data) {
						if(data.status=="success"){

							thisID.val(data.data);
							thisID.attr('oldData',data.data);


							console.log(data.msg);

						}else{
						}
					}
				});
			// }else{
			// 	thisID.val(oldData);
			// }
			
		});

		$(document).off('change', '.customnumForm').on('change', '.customnumForm', function(e){
			
			var codeVal = $(this).val();
			var pIDList = $(this).attr('pageID');
			var pDate = $(this).attr('date');
			var thisID = $(this);
			var oldData = $(this).attr('oldData');
			var p_id = $(this).attr('p_id');

			var ownnerValue = $('#p_owner'+pIDList).val();
			// console.log(codeVal);

			if(codeVal != ''){
				var dataCheck = codeVal.split('-');
				// console.log(dataCheck);
				// return false;
				if(dataCheck[1]){

					if(dataCheck[0]){
						if(ownnerValue != ''){
							
							if($.isNumeric(dataCheck[0]) && $.isNumeric(dataCheck[1])){
								var numStart = parseInt(dataCheck[0]);
								var numEnd = parseInt(dataCheck[1]);

								if(numEnd > numStart){
									console.log(numStart,'=>',numEnd);
									var parentID = thisID.parents('.many_row').attr('num');
									console.log(parentID,'indexdata');

									var numindex = parseInt(parentID);
									thisID.val(numStart);
									// console.log(numStart,numEnd);
									for (var i = numStart; i <= numEnd; i++) {
										
										var mainID = $('#main_'+numindex);

										// block number save
										var codeVal_sub = i;
										var pIDList_sub = mainID.find('.customnumForm').attr('pageID');
										var pDate_sub = mainID.find('.customnumForm').attr('date');
										var thisID_sub = mainID.find('.customnumForm');
										var oldData_sub = mainID.find('.customnumForm').attr('oldData');
										var p_id_sub = mainID.find('.customnumForm').attr('p_id');
										console.log(codeVal_sub,pIDList_sub,pDate_sub,oldData_sub,p_id_sub,'=> correct data');

										$.ajax({
											url: '/reportmainstaff/updatenumber',
											type: 'GET',
											data: {codeVal:codeVal_sub, pIDList:pIDList_sub, pDate:pDate_sub, p_id:p_id_sub},
											success: function(data) {
												console.log(data);
												if(data.status=="success"){
													var mainDa = $('#page_value'+data.msg)
													mainDa.val(data.data);
													mainDa.attr('oldData',data.data);


													console.log(data.msg);

												}else{
												}
											}
										});


										// block ownner
										var codeVal_name = ownnerValue;
										$.ajax({
											url: '/reportmainstaff/updatecode',
											type: 'GET',
											data: {codeVal:codeVal_name, pIDList:pIDList_sub, pDate:pDate_sub, p_id:p_id_sub},
											success: function(data) {
												if(data.status=="success"){
													var mainDa = $('#p_owner'+data.msg)
													mainDa.val(data.data);
													mainDa.attr('oldData',data.data);


													console.log(data.msg);

												}else{
												}
											}
										});



										numindex = numindex + 1;
									}



								}else{
									alert('លេខមិនត្រឹមត្រូវ');
									thisID.val(oldData);
								}
								
								
							}else{
								alert('លេខមិនត្រឹមត្រូវ');
								thisID.val(oldData);
							}


						}else{
							alert('លេខមិនត្រឹមត្រូវ');
							thisID.val(oldData);
						}
					}else{
						alert('លេខមិនត្រឹមត្រូវ');
						thisID.val(oldData);
					}
				}else{
					// console.log($.isNumeric(codeVal));
					// return false;
					if($.isNumeric(codeVal)){

						$.ajax({
							url: '/reportmainstaff/updatenumber',
							type: 'GET',
							data: {codeVal:codeVal, pIDList:pIDList, pDate:pDate, p_id:p_id},
							success: function(data) {
								if(data.status=="success"){

									thisID.val(data.data);
									thisID.attr('oldData',data.data);


									console.log(data.msg);

								}else{
								}
							}
						});

					}else{

						console.log(codeVal,'not Data');
						alert('លេខមិនត្រឹមត្រូវ');
						thisID.val(oldData);
						
					}
					
				}
				
			}else{
				thisID.val(oldData);
			}
			
		});

		$(document).off('change', '#s_id_man').on('change', '#s_id_man', function(e){
			
			var id = $(this).val();
			$("#s_id").prop( "disabled", true );
			$.ajax({
				url: '/reportmainstaff/getstaffchild',
				type: 'GET',
				data: {s_id:id},
				success: function(data) {
					if(data.status=="success"){

						$("#s_id").html(data.msg);

						$("#s_id").prop( "disabled", false );

						console.log(data.msg);

					}else{
					}
				}
			});
            
			

		});
	});

	

@if(isset($boss_data))
	function commaSeparateNumber(val){
		while (/(\d+)(\d{3})/.test(val.toString())){
			val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
		}
		return val;
	}

	function addCommas(nStr)
	{
	    nStr += '';
	    x = nStr.split('.');
	    x1 = x[0];
	    if(x[1]){
	    	xdot = x[1].toString().substring(0, 2)
	    }else{
	    	xdot = null;
	    }
	    x2 = x.length > 1 ? '.' + xdot : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + ',' + '$2');
	    }
	    return x1 + x2;
	}

	$(document).ready(function() {

			var conRowTop = 4;
			@if($count1DRWin == 0)
				conRowTop = conRowTop - 1;
			@endif
			@if($count4DRWin == 0)
				conRowTop = conRowTop - 1;
			@endif


			var total2digitr = '';
			var total2digits = '';
			var total3digitr = '';
			var total3digits = '';
			var total2digitrright = '';
			var total2digitsright = '';
			var total3digitrright = '';
			var total3digitsright = '';

			var total1digitrright = '';
			var total1digitsright = '';
			var total4digitrright = '';
			var total4digitsright = '';

			var time = '';
			var staff_list = '';
			var page = '';
			var tr = '';
			var sum_total2digitrright = 0;
			var sum_total2digitsright = 0;
			var sum_total3digitrright = 0;
			var sum_total3digitsright = 0;


			var num_total2digitrright = 0;
			var num_total2digitsright = 0;
			var num_total3digitrright = 0;
			var num_total3digitsright = 0;

			var sum_total1digitrright = 0;
			var sum_total1digitsright = 0;
			var sum_total4digitrright = 0;
			var sum_total4digitsright = 0;

			var num_total1digitrright = 0;
			var num_total1digitsright = 0;
			var num_total4digitrright = 0;
			var num_total4digitsright = 0;


			var twodigit_charge = <?php echo $twodigit_charge;?>;
			var threedigit_charge = <?php echo $threedigit_charge;?>;

			var sum_total2digitr = 0;
			var sum_total2digits = 0;
			var sum_total3digitr = 0;
			var sum_total3digits = 0;

			var sum_total1digitr = 0;
			var sum_total1digits = 0;
			var sum_total4digitr = 0;
			var sum_total4digits = 0;
				


			var sum_total2digitr_new = 0;
			var sum_total2digits_new = 0;
			var sum_total3digitr_new = 0;
			var sum_total3digits_new = 0;

			var sum2DigitBoosReal = 0;
			var sum3DigitBoosReal = 0;
			var sum2DigitBoosDolla = 0;
			var sum3DigitBoosDolla = 0;

			var data_array = new Array();

			var total_page_sum = 0;
			$(".many_row").each(function(index, element) {
				total_page_sum = total_page_sum + 1;

				total2digitr = $(this).find('.total2digitr').text();
				total2digits = $(this).find('.total2digits').text();
				total3digitr = $(this).find('.total3digitr').text();
				total3digits = $(this).find('.total3digits').text();
				total2digitrright = $(this).find('.total2digitrright').text();
				total2digitsright = $(this).find('.total2digitsright').text();
				total3digitrright = $(this).find('.total3digitrright').text();
				total3digitsright = $(this).find('.total3digitsright').text();

				total1digitrright = $(this).find('.total1digitrright').text();
				total1digitsright = $(this).find('.total1digitsright').text();
				total4digitrright = $(this).find('.total4digitrright').text();
				total4digitsright = $(this).find('.total4digitsright').text();

				var id_staff = $(this).find('#td_staff_'+index).attr('id_staff');
				var twodigit_charge = $(this).find('#td_staff_'+index).attr('two');
				var threedigit_charge = $(this).find('#td_staff_'+index).attr('three');
					// alert(id_staff);

				if(total2digitr==0){
					numtotal2digitr = 0;
					total2digitr = '';
				}else{
					numtotal2digitr = total2digitr;
					total2digitr = total2digitr;
				}

				if(total2digits==0){
					numtotal2digits = 0;
					total2digits = '';
				}else{
					numtotal2digits = total2digits;
					total2digits = total2digits;
				}

				if(total3digitr==0){
					numtotal3digitr = 0;
					total3digitr = '';
				}else{
					numtotal3digitr = total3digitr;
					total3digitr = total3digitr;
				}

				var totalsum = parseFloat(numtotal2digitr) + parseFloat(numtotal3digitr);
				if(totalsum==0){
					totalsum = '';
				}

				if(total3digits==0){
					numtotal3digits = 0;
					total3digits = '';
				}else{
					numtotal3digits = total3digits;
					total3digits = total3digits;
				}

				if(total2digitrright==0){
					num_total2digitrright = 0;
					total2digitrright = '';
				}else{
					num_total2digitrright = total2digitrright;
					total2digitrright = total2digitrright;

				}
				if(total2digitsright==0){
					num_total2digitsright = 0;
					total2digitsright = '';
				}else{
					num_total2digitsright = total2digitsright;
					total2digitsright = total2digitsright;
				}
				if(total3digitrright==0){
					num_total3digitrright = 0;
					total3digitrright = '';
				}else{
					num_total3digitrright = total3digitrright;
					total3digitrright = total3digitrright;
				}
				if(total3digitsright==0){
					num_total3digitsright = 0;
					total3digitsright = '';
				}else{
					num_total3digitsright = total3digitsright;
					total3digitsright = total3digitsright;
				}

				// block add more digit
				if(total1digitrright==0){
					num_total1digitrright = 0;
					total1digitrright = '';
				}else{
					num_total1digitrright = total1digitrright;
					total1digitrright = total1digitrright;

				}
				if(total1digitsright==0){
					num_total1digitsright = 0;
					total1digitsright = '';
				}else{
					num_total1digitsright = total1digitsright;
					total1digitsright = total1digitsright;
				}

				if(total4digitrright==0){
					num_total4digitrright = 0;
					total4digitrright = '';
				}else{
					num_total4digitrright = total4digitrright;
					total4digitrright = total4digitrright;
				}
				if(total4digitsright==0){
					num_total4digitsright = 0;
					total4digitsright = '';
				}else{
					num_total4digitsright = total4digitsright;
					total4digitsright = total4digitsright;
				}

				var totalsum_dolla = parseFloat(numtotal2digits) + parseFloat(numtotal3digits);
				if(totalsum_dolla==0){
					totalsum_dolla = '';
				}

				sum_total2digitr = sum_total2digitr + parseFloat(numtotal2digitr);
				sum_total2digits = sum_total2digits + parseFloat(numtotal2digits);
				sum_total3digitr = sum_total3digitr + parseFloat(numtotal3digitr);
				sum_total3digits = sum_total3digits + parseFloat(numtotal3digits);

				

				sum_total2digitrright = sum_total2digitrright + parseFloat(num_total2digitrright);
				sum_total2digitsright = sum_total2digitsright + parseFloat(num_total2digitsright);
				sum_total3digitrright = sum_total3digitrright + parseFloat(num_total3digitrright);
				sum_total3digitsright = sum_total3digitsright + parseFloat(num_total3digitsright);

				sum_total1digitrright = sum_total1digitrright + parseFloat(num_total1digitrright);
				sum_total1digitsright = sum_total1digitsright + parseFloat(num_total1digitsright);
				sum_total4digitrright = sum_total4digitrright + parseFloat(num_total4digitrright);
				sum_total4digitsright = sum_total4digitsright + parseFloat(num_total4digitsright);

				sum_total2digitr_new = sum_total2digitr_new + (
																Math.round(

																		((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100)
																	
																)
															);


				sum_total2digits_new = sum_total2digits_new + 

															(
																

																		((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100)
																	
																
															);


				sum2DigitBoosReal = sum2DigitBoosReal + (((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100));

				sum3DigitBoosReal = sum3DigitBoosReal + (((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100));

				sum2DigitBoosDolla = sum2DigitBoosDolla + (((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100));

				sum3DigitBoosDolla = sum3DigitBoosDolla + (((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100));


			});


			var total_income_expense_riel = 0;
			var total_income_expense_dollar = 0;
			<?php 
			if($var_type_lottery == 1 || $var_type_lottery == 2){
			?>
				
				@if($main_new == 0 )
					var income_riel = sum_total2digitr_new;
					var income_dollar = sum_total2digits_new

				@else
					var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
					var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				@endif



				// alert(sum_total2digitr_new);

				var expense_riel = (parseInt(sum_total1digitrright * <?php echo $win_one_digit;?>) + parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>)+ parseInt(sum_total4digitrright * <?php echo $win_four_digit;?>) );


				var expense_dollar = (parseFloat(sum_total1digitsright * <?php echo $win_one_digit;?>) + parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>) + parseFloat(sum_total4digitsright * <?php echo $win_four_digit;?>));
				
			<?php }else{?>
				var income_riel = parseFloat(Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
			<?php }?>
			 // var a = Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100));
			 // var b = Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100));
			 // var aaaa = a + b;
			// alert( income_riel );
			var staff_id = "<?php echo $staffInfo->s_id;?>";
			
			if(staff_id == 658 || staff_id == 659){
					var income_riel = parseFloat(Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));

					var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
					// alert(income_riel);
					var new_price = sum_total2digitr_new;
					var new_price_s = sum_total2digits_new.toFixed(2);
			}else{
				var new_price = income_riel;
				var new_price_s = income_dollar;
			}

			// console.log(income_riel,'income_riel');

			var total_income_expense_riel = income_riel - expense_riel;
			var check_total = total_income_expense_riel.toString().substr(-2);
            // console.log(total_income_expense_riel);
            // console.log(check_total);

            
            
					




		
			// if(check_total >= 10){
   //              // console.log('1');
   //              if(total_income_expense_riel > 0 ) {
   //                  total_income_expense_riel = total_income_expense_riel + ( 10 - parseInt(check_total));
   //              }else{
   //                  total_income_expense_riel = total_income_expense_riel - ( 10 - parseInt(check_total));
   //              }
			// }else{
   //              // console.log('2');
   //              if(total_income_expense_riel > 0 ){
   //                  total_income_expense_riel = parseInt(total_income_expense_riel) - parseInt(check_total);
   //              }else{
   //                  total_income_expense_riel = parseInt(total_income_expense_riel) + parseInt(check_total);
   //              }

			// }

			// console.log(total_income_expense_riel,'total_income_expense_riel_noStr');

			if(total_income_expense_riel>0){
				total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = total_income_expense_riel;
            }else{
            	total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = '('+total_income_expense_riel+')';
            }

			var total_income_expense_dollar = income_dollar - expense_dollar;
			// console.log(income_dollar);
			// console.log(income_dollar);
			var total_income_expense_dollar_noStr = total_income_expense_dollar;

            if(total_income_expense_dollar>0){
                total_income_expense_dollar = total_income_expense_dollar.toFixed(2);
            }else{
                total_income_expense_dollar = '('+total_income_expense_dollar.toFixed(2)+')';
            }

            var sum_totalall = sum_total2digitr + sum_total3digitr;
            var sum_totalall_dolla = sum_total2digits + sum_total3digits;

           	

			<?php 
				$totalCal = $colspan + 1;
			?>


			tr = '<tr class="bee_highlight backgroundNew"><th>{{trans('label.total')}}</th><th></th><th >'+total_page_sum+'</th><th class="{{$sumHide}}">'+sum_totalall+'</th><th class="{{$hidedata}}">'+sum_total2digitr+'</th><th class="{{$hidedata}}">'+sum_total3digitr+'</th><th class="totalReal1 needRed">'+sum_total1digitrright+'</th><th class="totalReal2 needRed">'+sum_total2digitrright+'</th><th class="totalReal3 needRed">'+sum_total3digitrright+'</th><th class="totalReal4 needRed">'+sum_total4digitrright+'</th><th class="{{$sumHide}} columDollar">$ '+sum_totalall_dolla+'</th><th class="{{$hidedata}} columDollar">$ '+sum_total2digits+'</th><th class="{{$hidedata}} columDollar">$ '+sum_total3digits+'</th><th class="columDollar oneDD needRed">$ '+sum_total1digitsright+'</th><th class="columDollar needRed">$ '+sum_total2digitsright+'</th><th class="columDollar needRed">$ '+sum_total3digitsright+'</th><th class="columDollar fourDD needRed">$ '+sum_total4digitsright+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class="bee_highlight"><td></td><td>{{trans('label.total_charge')}}</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)))+'</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitrright * 70)+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitrright * 600)+'</td><td>'+commaSeparateNumber(((parseInt(sum_total2digits) * parseInt(twodigit_charge)) / 100).toFixed(2))+'</td><td>'+commaSeparateNumber(((parseInt(sum_total3digits) * parseInt(threedigit_charge)) / 100).toFixed(2))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitsright * 70 )+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitsright * 600)+'</td><tr>';
			// $('.display_total_result').append(tr);
            var total_rate_thesame = parseInt(sum_total2digitr) + parseInt(sum_total3digitr);
            var total_rate_thesame_dolla = parseFloat(sum_total2digits) + parseFloat(sum_total3digits); 
			tr = '<tr class=""><td colspan="<?php echo $spaceRow;?>"></td></tr>';
			$('.display_total_result').append(tr);

			
			// 2D
			// tr = '<tr class="bee_highlight {{$hidedata}}"><th colspan="2" rowspan="2" style="vertical-align: middle; text-align: center;">កាត់ទឹក</th><th>2D</th><th>'+sum_total2digitr+'</th><th>x '+twodigit_charge+'%</th><th>'+Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100))+'</th><th></th><th>$ '+sum_total2digits+'</th><th>x '+twodigit_charge+'%</th><th>$ '+((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100).toFixed(2)+'</th><th></th></tr>';

			tr = '<tr class="bee_highlight {{$hidedata}}"><th colspan="2" rowspan="2" style="vertical-align: middle; text-align: center;">កាត់ទឹក</th><th>2D</th><th>'+sum_total2digitr+'</th><th>  x'+twodigit_charge+'%  </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+Math.round(sum2DigitBoosReal)+'</th><th class="spaceReal"></th><th class="columDollar">$ '+sum_total2digits+'</th><th class="columDollar"> x'+twodigit_charge+'% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">$ '+sum2DigitBoosDolla.toFixed(2)+'</th><th class="columDollar fourDD"></th></tr>';
			$('.display_total_result').append(tr);
			// 3D
			// tr = '<tr class="bee_highlight {{$hidedata}}"><th>3D</th><th>'+sum_total3digitr+'</th><th>x '+threedigit_charge+'%</th><th>'+Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100))+'</th><th></th><th>$ '+sum_total3digits+'</th><th>x '+threedigit_charge+'%</th><th>$ '+((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100).toFixed(2)+'</th><th></th></tr>';

			tr = '<tr class="bee_highlight {{$hidedata}}"><th>3D</th><th>'+sum_total3digitr+'</th><th> x'+threedigit_charge+'% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+Math.round(sum3DigitBoosReal)+'</th><th class="spaceReal"></th><th class="columDollar">$ '+sum_total3digits+'</th><th class="columDollar"> x'+threedigit_charge+'% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">$ '+sum3DigitBoosDolla.toFixed(2)+'</th><th class="columDollar fourDD"></th></tr>';
			$('.display_total_result').append(tr);

			tr = '<tr class="bee_highlight {{$sumHide}}"><th colspan="2"  style="vertical-align: middle; text-align: center;">កាត់ទឹក</th><th>Total</th><th>'+total_rate_thesame+'</th><th> => </span></th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+Math.round(sum_total2digitr_new)+'</th><th class="spaceReal"></th><th class="columDollar">$ '+total_rate_thesame_dolla+'</th><th class="columDollar"> => </th><th colspan="{{$totalCSpanD}}"  class="columDollar " style="text-align:left !important;">$ '+sum_total2digits_new.toFixed(2)+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);




			<?php 
			if($var_type_lottery == 1){
			?>
				
				@if($count1DRWin > 0)
					// 1D(T)
					tr = '<tr class="bee_highlight needRed"><th class="{{$hidedata}}" colspan="2" rowspan="{{$countRowSP}}" style="vertical-align: middle; text-align: center;">លេខត្រូវ</th><th>1D(T)</th><th>'+sum_total1digitrright+'</th><th>x<?php echo $win_one_digit;?>% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+sum_total1digitrright * <?php echo $win_one_digit;?>+' </th><th class="spaceReal"></th><th class="columDollar">'+sum_total1digitsright+'</th><th class="columDollar"> x<?php echo $win_one_digit;?>% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">'+sum_total1digitsright * <?php echo $win_one_digit;?>+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
					$('.display_total_result').append(tr);

				@else
					// 2D(T)
					tr = '<tr class="bee_highlight needRed"><th class="{{$hidedata}}" colspan="2" rowspan="{{$countRowSP}}" style="vertical-align: middle; text-align: center;">លេខត្រូវ</th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th> x<?php echo $win_two_digit;?>% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th class="spaceReal"></th><th class="columDollar">'+sum_total2digitsright+'</th><th class="columDollar"> x<?php echo $win_two_digit;?>% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
					$('.display_total_result').append(tr);
				@endif

				@if($count1DRWin > 0)
					// 2D(T)
					tr = '<tr class="bee_highlight needRed"><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th> x<?php echo $win_two_digit;?>% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+sum_total2digitrright * <?php echo $win_two_digit;?>+'</th><th class="spaceReal"></th><th class="columDollar">'+sum_total2digitsright+'</th><th class="columDollar"> x<?php echo $win_two_digit;?>% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
					$('.display_total_result').append(tr);

				@endif

				// 3D(T)
				tr = '<tr class="bee_highlight needRed"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th> x<?php echo $win_three_digit;?>% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th class="spaceReal"></th><th class="columDollar">'+sum_total3digitsright+'</th><th class="columDollar"> x<?php echo $win_three_digit;?>% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
				$('.display_total_result').append(tr);

				@if($count4DRWin > 0)

					// 4D(T)
					tr = '<tr class="bee_highlight needRed"><th>4D(T)</th><th>'+sum_total4digitrright+'</th><th> x<?php echo $win_four_digit;?>% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+sum_total4digitrright * <?php echo $win_four_digit;?>+'</th><th class="spaceReal"></th><th class="columDollar">'+sum_total4digitsright+'</th><th class="columDollar"> x<?php echo $win_four_digit;?>% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">'+sum_total4digitsright * <?php echo $win_four_digit;?>+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
					$('.display_total_result').append(tr);

				@endif
			<?php }else if($var_type_lottery == 2 || $var_type_lottery == 3 || $var_type_lottery == 4){?>
				// 2D(T)
				// tr = '<tr class="bee_highlight needRed"><th class="{{$hidedata}}" colspan="2" rowspan="2" style="vertical-align: middle; text-align: center;">លេខត្រូវ</th><th class="{{$sumHide}}" rowspan="2" style="vertical-align: middle; text-align: center;"></th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th> => </th><th style="text-align:left !important;">'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th></th><th class="columDollar">'+sum_total2digitsright+'</th><th class="columDollar"> => </th><th class="columDollar">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} columDollar"></th></tr>';
				tr = '<tr class="bee_highlight needRed"><th class="{{$hidedata}}" colspan="2" rowspan="{{$countRowSP}}" style="vertical-align: middle; text-align: center;">លេខត្រូវ</th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th> x<?php echo $win_two_digit;?>% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th class="spaceReal"></th><th class="columDollar">'+sum_total2digitsright+'</th><th class="columDollar"> x<?php echo $win_two_digit;?>% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				// tr = '<tr class="bee_highlight"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th> => </th><th style="text-align:left !important;">'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th></th><th class="columDollar">'+sum_total3digitsright+'</th><th class="columDollar"> => </th><th class="columDollar">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} columDollar"></th></tr>';
				tr = '<tr class="bee_highlight needRed"><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th> x<?php echo $win_three_digit;?>% </th><th colspan="{{$totalCSpanR}}" style="text-align:left !important;">'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th class="spaceReal"></th><th class="columDollar">'+sum_total3digitsright+'</th><th class="columDollar"> x<?php echo $win_three_digit;?>% </th><th colspan="{{$totalCSpanD}}" class="columDollar" style="text-align:left !important;">'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
				$('.display_total_result').append(tr);
			<?php }?>


			



			tr = '<tr class=""><td colspan="<?php echo $spaceRow;?>"></td></tr>';
			$('.display_total_result').append(tr);


			// tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th></th><th></th><th class="result_right_total">'+total_income_expense_riel+' </th><th></th><th></th><th></th><th class="result_right_total">$ '+total_income_expense_dollar+'</th><th class="{{$hidedata}}"></th></tr>';
			// $('.display_total_result').append(tr);


			// paid or borrow


			var totalRealBefore = total_income_expense_riel_noStr;
			var totalDollaBefore = total_income_expense_dollar_noStr;



			console.log(totalRealBefore);

			if(totalRealBefore < 0){
				var labelBefore_r = 'ខាត';
				var redColor = 'color: red;';
			}else{
				var labelBefore_r = 'សុី';
				var redColor = '';
			}


			if(totalDollaBefore < 0){
				var labelBefore_d = 'ខាត';
				var redColorDolar = 'color: red;';
			}else{
				var labelBefore_d = 'សុី';
				var redColorDolar = '';
			}	

			// price before percent
			tr = '<tr class="bee_highlight backgroundNew"><th class="" colspan="{{$colspan}}"></th><th></th><th colspan="1" style="text-align:left !important; '+redColor+'" class="result_right_total">'+labelBefore_r+'</th><th class="result_right_total" style="text-align:left !important; '+redColor+'" colspan="{{$totalCSpanR}}" >'+addCommas(totalRealBefore)+' </th><th class="spaceReal"></th><th style="'+redColorDolar+' text-align:left !important;"  class="result_right_total columDollar" colspan="2">'+labelBefore_d+'</th><th class="result_right_total columDollar" style="text-align:left !important; '+redColorDolar+'" colspan="{{$totalCSpanD}}" >$ '+addCommas(totalDollaBefore.toFixed(2))+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
			$('.display_total_result').append(tr);



			@if($percentStaff > 0)
				var percent_sub = parseFloat('{{$percentStaff}}');
				var percent = 100 - percent_sub;

				var totalReal = parseFloat(totalRealBefore * percent_sub)/100;
				var totalDolla = parseFloat(totalDollaBefore * percent_sub)/100;

				if(totalReal < 0){
					var redColor = 'color: red;';
				}else{
					var redColor = '';
				}

				if(totalDolla < 0){
					var redColorDolar = 'color: red;';
				}else{
					var redColorDolar = '';
				}

				// price after percent 
				tr = '<tr class="bee_highlight backgroundNew"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important; '+redColor+'">'+percent_sub+'%</th><th class="result_right_total" style="text-align:left !important; '+redColor+'" colspan="{{$totalCSpanR}}">'+addCommas(totalReal)+' </th><th class="spaceReal"></th><th class="columDollar" style="'+redColorDolar+'">%</th><th colspan="2" class="result_right_total columDollar" style="text-align:left !important; '+redColorDolar+'" colspan="{{$totalCSpanD}}">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
				$('.display_total_result').append(tr);

				// chail_id
				var totalReal_main = parseFloat(totalRealBefore * percent)/100;
				var totalDolla_main = parseFloat(totalDollaBefore * percent)/100;

				tr = '<tr class="bee_highlight backgroundNew"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important; '+redColor+'">'+percent+'%</th><th class="result_right_total" style="text-align:left !important; '+redColor+'" colspan="{{$totalCSpanR}}">'+addCommas(totalReal_main)+' </th><th class="spaceReal"></th><th class="columDollar" colspan="2">%</th><th class="result_right_total columDollar" style="text-align:left !important; '+redColorDolar+'" colspan="{{$totalCSpanD}}">$ '+totalDolla_main.toFixed(2)+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
				$('.display_total_result').append(tr);

				// tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">'+percent+'%</th><th class="result_right_total" style="text-align:left !important;" colspan="{{$totalCSpanD}}">'+addCommas(totalReal_main)+' </th><th class="spaceReal"></th><th class="spaceReal"></th><th class="columDollar">%</th><th class="result_right_total columDollar" style="text-align:left !important;" colspan="{{$totalCSpanD}}">$ '+totalDolla_main.toFixed(2)+'</th><th class="{{$hidedata}} columDollar "></th></tr>';
				// $('.display_total_result').append(tr);

				totalRealBefore = totalReal;
				totalDollaBefore = totalDolla;


			@else

				var totalReal = totalRealBefore;
				var totalDolla = totalDollaBefore;

			@endif



@if($var_type_lottery == 1 || $var_type_lottery == 2)
			var displayMainTotal = 0;
			<?php 
				if (!empty($money_paid) && ($var_type_lottery == 1 || $var_type_lottery == 2) ) {
					foreach($money_paid as $key => $value) {

						if($value->st_price_r == 0 && $value->st_price_d == 0){

						}else{

							if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23){
								if($value->st_type == 21 && $value->st_price_r < 0 ){
									$label_r = 'ខាត';
								}else{
									$label_r = $value->pav_value;
								}

								if($value->st_type == 21 && $value->st_price_d < 0 ){
									$label_d = 'ខាត';
								}else{
									$label_d = $value->pav_value;
								}

								if($value->st_type == 21 && $value->st_price_r > 0 && $value->st_price_d > 0 ){
									$label_r = $value->pav_value;
									$label_d = '';
								}
								if($value->st_type == 21 && $value->st_price_r < 0 && $value->st_price_d < 0 ){
									$label_r = 'ខាត';
									$label_d = '';
								}

								

				?>	
								// console.log(totalDolla);
								var subPrice_r = '{{ $value->st_price_r }}';
								var subPrice_d = '{{ $value->st_price_d }}';
								if(subPrice_r != ''){
									totalReal = totalReal + parseFloat(subPrice_r);
								}

								if(subPrice_d != ''){
									totalDolla = totalDolla + parseFloat(subPrice_d);
								}

								

								<?php 
									$colorPrice = 'blueColor';
									$colorPriceD = 'blueColor';
									if($label_r == 'ខ្វះ' || $label_r == 'ខាត'){
										if($value->st_price_r >= 0){
											$label_r = 'លុយចាស់';
											$colorPrice = 'blueColor';
										}else{
											$label_r = 'លុយខាតចាស់';
											$colorPrice = 'redColor';
										}

										if($value->st_price_d >= 0){
											$label_d = 'លុយចាស់';
											$colorPriceD = 'blueColor';
										}else{
											$label_d = 'លុយខាតចាស់';
											$colorPriceD = 'redColor';
										}
									}
								?>
								
								

								// console.log(parseFloat(totalDolla));
								var displayPriceR = parseFloat('{{$value->st_price_r}}');
								var displayPriceD = parseFloat('{{$value->st_price_d}}');
								@if($countDolla == 10000)
									tr = '<tr class="bee_highlight "><th class="{{$hidedata}} {{$colorPrice}}" style="text-align:left !important;">{{$label_d}}</th><th class="{{$colorPriceD}}" style="text-align:left !important;">$ '+displayPriceD.toFixed(2)+'</th><th></th><th colspan="2" style="text-align:left !important;" class="{{$colorPrice}}">{{$label_r}}</th><th class="result_right_total {{$colorPrice}}" style="text-align:left !important;" colspan="{{$totalCSpanR}}">'+addCommas(displayPriceR)+'</th><th class="spaceReal"></th><th class="{{$colorPriceD}} columDollar"  style="text-align:left !important;" colspan="2">{{$label_d}}</th><th class="result_right_total {{$colorPriceD}} columDollar" style="text-align:left !important;" colspan="{{$totalCSpanD}}">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';

								@else
									tr = '<tr class="bee_highlight "><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;" class="{{$colorPrice}}">{{$label_r}}</th><th class="result_right_total {{$colorPrice}}" style="text-align:left !important;" colspan="{{$totalCSpanR}}">'+addCommas(displayPriceR)+'</th><th class="spaceReal"></th><th class="{{$colorPriceD}} columDollar" style="text-align:left !important;" colspan="2">{{$label_d}}</th><th class="result_right_total {{$colorPriceD}} columDollar" style="text-align:left !important;" colspan="{{$totalCSpanD}}">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
								@endif



								$('.display_total_result').append(tr);
								displayMainTotal = displayMainTotal + 1;
				<?php 
							}else{
				?>	
								var subPrice_r = '{{ $value->st_price_r }}';
								var subPrice_d = '{{ $value->st_price_d }}';
								if(subPrice_r > 0){
									totalReal = totalReal - parseFloat(subPrice_r);
								}
								if(subPrice_d > 0){
									totalDolla = totalDolla - parseFloat(subPrice_d);
								}
								// console.log(parseFloat(totalDolla));

								<?php 
								if($value->pav_value == 'តវ៉ា'){

									
									$colorPrice = 'redColor';
									$colorPriceD = 'redColor';
								}

								if($value->pav_value == 'កូនអោយ'){

									
									$colorPrice = 'redColor';
									$colorPriceD = 'redColor';

								}
								?>
								
								var displayPriceR = parseFloat('{{$value->st_price_r * -1}}');
								var displayPriceD = parseFloat('{{$value->st_price_d * -1}}');

								tr = '<tr class="bee_highlight "><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;" class="{{$colorPrice}}">{{ $value->pav_value }}</th><th class="result_right_total {{$colorPrice}}" style="text-align:left !important;" colspan="{{$totalCSpanR}}">'+addCommas(displayPriceR)+'</th><th class="spaceReal"></th><th class="{{$colorPriceD}} columDollar"  style="text-align:left !important;" colspan="2">{{ $value->pav_value }}</th><th class="result_right_total {{$colorPriceD}} columDollar" style="text-align:left !important;" colspan="{{$totalCSpanD}}">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';

								// tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;" class="{{$colorPrice}}">{{ $value->pav_value }}</th><th class="result_right_total {{$colorPrice}}" style="text-align:left !important;" colspan="{{$totalCSpanR}}">'+addCommas(displayPriceR)+'</th><th class="spaceReal"></th><th class="result_right_total columDollar {{$colorPriceD}}" style="text-align:left !important;" colspan="{{$totalCSpanD}}">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}} columDollar"></th></tr>';
								$('.display_total_result').append(tr);
								displayMainTotal = displayMainTotal + 1;
			<?php 
							}
						} //end if value 0
					}
				}
			?>
			// end paid or borrow
			if(displayMainTotal > 0){
				// total price
				
				tr = '<tr class=""><td colspan="{{$totalCal}}"></td></tr>';
				$('.display_total_result').append(tr);

				var redColor = '';
				var redColorDolar = '';

				if(totalReal > 0){
					var labelTotal = 'សុីសរុប'; 
				}else{
					var labelTotal = 'ខាតសរុប';
					var redColor = 'color: red;';
				}

				if(totalDolla >= 0){
					var labelTotal_d = 'សុីសរុប'; 
				}else{
					var labelTotal_d = 'ខាតសរុប';
					var redColorDolar = 'color: red;';
				}

				if(totalReal >= 0 && totalDolla >= 0){
					var labelTotal = 'សុីសរុប'; 
					var labelTotal_d = 'សុីសរុប'; 
				}

				if(totalReal < 0 && totalDolla < 0){
					var labelTotal = 'ខាតសរុប'; 
					var labelTotal_d = 'ខាតសរុប'; 
					var redColor = 'color: red;';
				}

				@if($countDolla == 0)

					if(totalDolla == 0){
						totalDollaNew = '';
						labelTotal_dNew = '';
					}else{
						totalDollaNew = '$ '+totalDolla.toFixed(2);
						labelTotal_dNew = labelTotal_d;
					}
					tr = '<tr class="bee_highlight backgroundNew"><th class="{{$hidedata}} result_right_total" style="text-align:left !important; '+redColorDolar+'">'+labelTotal_dNew+'</th><th style="text-align:left !important; '+redColorDolar+'" class="result_right_total">'+totalDollaNew+'</th><th></th><th colspan="2" style="text-align:left !important; '+redColor+'" class="result_right_total">'+labelTotal+'</th><th class="result_right_total" style="text-align:left !important; '+redColor+'" colspan="{{$totalCSpanR}}">'+addCommas(totalReal)+'</th><th class="spaceReal"></th><th  style="'+redColorDolar+' text-align:left !important;" class="result_right_total columDollar" colspan="2">'+labelTotal_d+'</th><th class="result_right_total columDollar" style="text-align:left !important; '+redColorDolar+'" colspan="{{$totalCSpanD}}">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
								$('.display_total_result').append(tr);
				@else

					tr = '<tr class="bee_highlight backgroundNew"><th class="{{$hidedata}}" ></th><th></th><th></th><th colspan="2" style="text-align:left !important; '+redColor+'" class="result_right_total">'+labelTotal+'</th><th class="result_right_total" style="text-align:left !important; '+redColor+'" colspan="{{$totalCSpanR}}">'+addCommas(totalReal)+'</th><th class="spaceReal"></th><th style="'+redColorDolar+' text-align:left !important;" class="result_right_total columDollar" colspan="2">'+labelTotal_d+'</th><th class="result_right_total columDollar" style="text-align:left !important; '+redColorDolar+'" colspan="{{$totalCSpanD}}">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}} columDollar fourDD"></th></tr>';
								$('.display_total_result').append(tr);

				@endif
			}

			// $(".report_total_info").each(function(index, element) {

			// 	staff_list = $(this).attr('staff');
			// 	id_staff = $(this).attr('id_staff');

			// 	time = $(this).attr('time');
			// 	page = $(this).attr('page');
			// 	$('.display_total_result').find('#td_staff_com_'+index).text(staff_list);
			// 	$('.display_total_result').find('#td_time_com_'+index).text(time);
			// 	$('.display_total_result').find('#td_page_com_'+index).text(page);

			// });

			var date = '{{$var_dateStart}}';
			var staff = '{{$staffInfo->s_id}}';
			// console.log('{{$checkinView}}', '{{$checkinViewKh}}','VN And KH');
			<?php 
			if($checkinView == 1 && $var_type_lottery == 1){
			?>
			$.ajax({
				url: '../addmoney',
				type: 'GET',
				data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla,totalRealBefore:totalRealBefore,totalDollaBefore:totalDollaBefore},
				success: function(data) {
					console.log(data);
					if(data.status=="success"){
//							console.log(data.msg);
					}else{
//							console.log(data.msg);
					}
				}
			});
			
			<?php 
			}
			?>

			<?php 
			if($checkinViewKh == 1 && $var_type_lottery == 2){
			?>	
				// console.log("{{$checkinViewKh}}");
				$.ajax({
					url: '../addmoney',
					type: 'GET',
					data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla,totalRealBefore:totalRealBefore,totalDollaBefore:totalDollaBefore},
					success: function(data) {
						if(data.status=="success"){
//							console.log(data.msg);
						}else{
//							console.log(data.msg);
						}
					}
				});
			<?php 
			}
			?>

			var var_type_lottery = '{{$var_type_lottery}}';

			$.ajax({
				url: 'summary_lottery_total',
				type: 'GET',
				data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:var_type_lottery},
				success: function(data) {
					if(data.status=="success"){
//							console.log(data.msg);
					}else{
//							console.log(data.msg);
					}
				}
			});


			<?php 
				if(Session::get('roleLot') != 100){
			?>

				var var_type_lottery = '{{$var_type_lottery}}';

				$.ajax({
					url: '../../report/summary_lottery',
					type: 'GET',
					data: {date:date,staff:staff,income_riel:income_riel,income_dollar:income_dollar,expense_riel:expense_riel,expense_dollar:expense_dollar,var_type_lottery:var_type_lottery},
					success: function(data) {
						if(data.status=="success"){
							console.log(data.msg,'summary_lottery new form');
						}else{
//							console.log(data.msg);
						}
					}
				});

				$.ajax({
					url: '../../report/summary_lottery_total',
					type: 'GET',
					data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:var_type_lottery},
					success: function(data) {
						if(data.status=="success"){
							console.log(data.msg,'summary_lottery_total new form');
						}else{
//							console.log(data.msg);
						}
					}
				});

				$.ajax({
					url: '../../report/addtotalmoney',
					type: 'GET',
					data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:var_type_lottery},
					success: function(data) {
						if(data.status=="success"){
								console.log(data.msg,'addtotalmoney new form');
						}else{
	//							console.log(data.msg);
						}
					}
				});

			<?php 
			}
			?>

	@endif

	});

	$(document).ready(function(){

		


		var conRowTop = 4;
		@if($count1DRWin == 0)
			$(".1DigitR").hide();
			$(".total1digitrright").hide();
			$(".totalReal1").hide();
			conRowTop = conRowTop - 1;
		@endif
		@if($count4DRWin == 0)
			$(".4DigitR").hide();
			$(".total4digitrright").hide();
			$(".totalReal4").hide();
			conRowTop = conRowTop - 1;
		@endif

		

	 	$(".winDollar").attr('colspan', '{{$colspanDollar}}');

		$(".winReal").attr('colspan', conRowTop);
		if(conRowTop == 2 || conRowTop == 3){
			$(".spaceReal").hide();
		}
	});

@endif
	
</script>
	
@stop