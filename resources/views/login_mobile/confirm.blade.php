
<!-- saved from url=(0034)http://13.250.35.236/Account/Login -->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Login</title>

		<!-- <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5, user-scalable=no;user-scalable=0;"> -->

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link href="{{ asset('/') }}/mobile_css/khmer-font.css" rel="stylesheet" />

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<link href="{{ asset('/') }}/mobile_css/font-awesome.css" rel="stylesheet" />
		<link href="{{ asset('/') }}/mobile_css/khmer-font.css" rel="stylesheet" />
		<link href="{{ asset('/') }}/mobile_css/login.css" rel="stylesheet" />
		<link href="{{ asset('/') }}/Login_files/style_login_phone.css" rel="stylesheet">

		<style> 
			/* * { 
				-webkit-touch-callout: none; 
				-webkit-user-select: none; 
				-khtml-user-select: none; 
				-moz-user-select: none; 
				-ms-user-select: none; 
				user-select: none; 
			}  */
			table td{
				font-family: fontBattambong;
				font-size: 22px;
			}

			.login-box{
			    width: 310px;
			    margin: 40px auto;
			    border: 1px solid #ccc;
			    padding: 25px 25px 0;


			    background: #67afe2;
				background-image: -webkit-linear-gradient(top, #67afe2, #0aa0f3);
				background-image: -moz-linear-gradient(top, #67afe2, #0aa0f3);
				background-image: -ms-linear-gradient(top, #67afe2, #0aa0f3);
				background-image: -o-linear-gradient(top, #67afe2, #0aa0f3);
				background-image: linear-gradient(to bottom, #67afe2, #0aa0f3);
				-webkit-border-radius: 10;
				-moz-border-radius: 10;
				border-radius: 10px;
			}
			.btn, .btn-large{
				background-color: #ee6e73;
			}
		</style>
	</head>
	<body class="login-page">
		
		<div class="login-box">
			@include('flash::message')
			<h6 class="font-M1" style="font-size:20px;"> លេខសុវត្ថិភាព</h6>
			<div class="login-box-body">
				{!! Form::open(['route' => 'confirmlogin', 'files' => true , 'novalidate' => 'validate', 'id' => 'confrm_login', 'name' => 'confrm_login', 'class'=>'']) !!}
					<input name="id_verify" type="hidden" value="{{$id}}" />
					<div class="form-group has-feedback">
						<input class="form-control" id="VerifyCode" maxlength="64" name="VerifyCode" placeholder="បញ្វូលលេខ" type="text" value="{{old('VerifyCode')}}" />
					</div>
					<div class="row">
						<button class="waves-effect waves-light btn">ចូល</button>
					</div>
				</form>
				<div class="social-auth-links text-center">
					<p style="font-size:14px;">* សូមបញ្ចូលលេខទូរសព័្ទដែលអ្នកចូលគណនី *</p>
				</div>
				<div class="row">
					<div class="pull-right"></div>
				</div>
			</div>
		</div>

		<script> 
			var current_focus = 0; 
		</script>
		<!-- <script src="{{ asset('/') }}/Login_files/login.min.js"></script> -->
		<!-- <script src="{{ asset('/') }}/Login_files/inspect.min.js"></script> -->
		<script> 
			
		</script>
	</body>
</html>