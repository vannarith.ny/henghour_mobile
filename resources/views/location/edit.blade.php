@extends('master')

@section('title')
<title>កែប្រែ តំបន់</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.form_add input{
			padding: 5px;
		}
		.form_add_kh input{
			padding: 5px;
		}
		.form_add_th input{
			padding: 5px;
		}
		.form_add_lo input{
			padding: 5px;
		}
		.show_input_add,.show_input_add_main,.show_input_add_main_kh,.show_input_add_main_th,
		.show_input_add_kh,.show_input_add_th,.show_input_add_lo{
			display: none;
		}
		.staff_charge_lo .alert-success,.staff_charge_lo .alert-danger,.staff_charge_th .alert-success,.staff_charge_th .alert-danger,.staff_charge_kh .alert-success,.staff_charge_kh .alert-danger{
			display: none;
		}
		.staff_charge .alert-success,.staff_charge .alert-danger{
			display: none;
		}
		.staff_charge_main .alert-success,.staff_charge_main .alert-danger{
			display: none;
		}
		.addmore_width{
			width: 150px;
		}

		.staff_charge_main_kh .alert-success,.staff_charge_main_kh .alert-danger{
			display: none;
		}
		.staff_charge_main_th .alert-success,.staff_charge_main_th .alert-danger{
			display: none;
		}
	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li>តំបន់</li><li>កែប្រែ</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> 
		        កែប្រែ តំបន់
		      </h1>
		     </div>
		</div>

		
    	<section id="widget-grid" class="">
    		@include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" aria-label="Close" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> {{ trans('label.validationAlert') }}</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>
		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-103" data-widget-editbutton="false">        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
		         <h2>កែប្រែ តំបន់</h2>    
		         
		        </header>

		        <!-- widget div-->
		        <div>
		         
		         <!-- widget edit box -->
		         <div class="jarviswidget-editbox">
		          <!-- This area used as dropdown edit box -->
		          
		         </div>
		         <!-- end widget edit box -->
		         
		         <!-- widget content -->
		         <div class="widget-body no-padding">            
		              {!! Form::model($location, ['route' => ['location.update', $location->id] ,'method' => 'PATCH',  'class' => 'smart-form user-form', 'novalidate' => 'validate', 'id' => 'checkout-form' ,"autocomplete"=>"false"]) !!}
		           <fieldset>

			            <div class="row">
				             <section class="col col-6">
				              {{ Form::label('name', trans('label.name'), array('class' => 'label')) }}
				              <label class="input">
				               {!! Form::text("name", $value = null, $attributes = array( 'id' => 'name', 'placeholder'=>trans('label.name'))) !!}
				              </label>
				             </section>
							 <section class="col col-3">
							 	{{ Form::label('s_id_main', 'ឈ្មួនកណ្តាល', array('class' => 'label')) }}
								<label class="select">
									 {{ Form::select('s_id', ([
                                        '' => 'Select' ]+$staffMains),$value = null,['class' => 'required ','id'=>'s_id_main','sms'=> 'ឈ្មួនកណ្តាល' ]
                                     ) }}
									 <i></i>
								</label>
							 </section>
							 <section class="col col-3">
				              {{ Form::label('percent', 'Share', array('class' => 'label')) }}
				              <label class="input">
				               {!! Form::text("percent", $value = null, $attributes = array( 'id' => 'percent', 'placeholder'=>'Share %')) !!}
				              </label>
				             </section>
				        </div>
			            
		           	</fieldset>
		           <footer>
		            <button type="submit" name="submit" class="btn btn-primary">{{trans('label.save')}}</button>   
		            <button type="button" class="btn btn-warning" onclick="btnCancel()">{{trans('label.cancel')}}</button>          
		           </footer>

		           <div class="message">
		            <i class="fa fa-check fa-lg"></i>
		            <p>
		             Your comment was successfully added!
		            </p>
		           </div>
		          {{ Form::close() }}
		          
		         </div>
		         <!-- end widget content -->
		         
		        </div>
		        <!-- end widget div -->
		        
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->


		      <article class="col-sm-12 col-md-12 col-lg-12">
		      	
			       	<!-- Widget ID (each widget will need unique ID)-->
			       	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-108" data-widget-editbutton="false">        
				        <header>
				         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				         <h2>បញ្ចីបុគ្គលិកក្នុងតំបន់ </h2>
				        </header>
				        <div class="staff_charge">
				        	<div class="alert alert-success " style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
				           <div class="alert alert-danger "  style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
					        <div class="widget-body no-padding">          
					          	<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
					           
						           <thead>           
							            <tr>
								             <th>{{ trans('label.id') }}</th>
								             <th>ឈ្មោះកូន</th>
								             <th>{{ trans('label.action') }}</th>
							            </tr>
						           </thead>
						           <tbody>
						           		
						           		<tr class="form_add smart-form ">
								             <td><button id="btn_add_staff_charge_main" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>
								             
								             <td class="show_input_add_main">
								             	<label class="select">
												   {{ Form::select('s_id',(['0' => trans('result.chooseStaff') ]+$staffs) , null, ['class' => 'select2 addmore_width required','id'=>'s_id']) }}
											   </label>
								             </td>

								             <td class="show_input_add_main">
								             	<button id="btn_new_staff_charge_main" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
								             </td>
								       	</tr>
								       

						           	<?php $i=0;?>
			           				@foreach($staffs_location as $staff_location)
			           					<?php $i++;?>
							            <tr class="staff_charge-{{$staff_location->id}}">
								             <td>{{$i}}</td>
								             <td>{{$staff_location->s_name}}</td>
								             <td>
												 
								             		 <button id="{{$staff_location->id}}" class="padding-button EditStaffCharge_main btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
								             		 <button id="{{$staff_location->id}}" class="padding-button deleteStaffCharge_main btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
								             </td>
								       	</tr>        
						            @endforeach 
						           </tbody>
					          	</table>
					    
					         </div>
					    </div>
					</div>
		      </article>
		      <!-- end control main  -->

		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script src="{{ asset('/') }}js/plugin/jquery-form/jquery-form.min.js"></script>
	<script type="text/javascript">
	
		function validate_form_main(class_div){
			var num = 0;
			$(class_div + " .required").each(function(index, element) {
	          var id, info, value, number;
	          value = $(element).val();
	          id = $(element).attr('id');
	          number = $(element).attr('number');
	          info = $(element).attr('sms');

	          // pattern_number = /^\d+$/;
	          pattern_number = /^(?:[1-9]\d*|0)?(?:\.\d+)?$/;

	          alert(id);
	          if (value === '') { //check value empty
	          	validate_by_vannarith(id,info,'add');
	          	num = num + 1;
	          }else if(number === 'true' && !pattern_number.test(value)){ //check value number only
	          	 validate_by_vannarith(id,'សូមបញ្ចូលតំលៃជាលេខ','add');
	          		num = num + 1;
	          }else{ //else value good data
	          	validate_by_vannarith(id,'','remove');
	          }
	        });
	        return num;
		}


		$(document).ready(function() {
		   pageSetUp();

		   var $checkoutForm = $('#checkout-form').validate({
		   // Rules for form validation
		    rules : {
		     
		     name : {
		      required : true
		     }
		    },
		  
		    // Messages for form validation
		    messages : {
		     name : {
		      required : 'សូមបញ្ចូលឈ្មោះថ្មី'
		     }
		    },
		  
		    // Do not change code below
		    errorPlacement : function(error, element) {
		     error.insertAfter(element.parent());
		    }
		   });

		   

		   // btn controll show hide form insert data
		   $(document).off('click', '#btn_add_staff_charge_main').on('click', '#btn_add_staff_charge_main', function(e){
				
				if($('.show_input_add_main').css('display') == 'none'){
					$('.show_input_add_main').show();
					$("#btn_new_staff_charge_main").attr( "type", "add" );
					$("#btn_new_staff_charge_main").attr( "idData", '' );
					$(this).text("{{ trans('label.delete') }}");
				}else{
					
					$('.show_input_add_main').hide();
					$(this).text("{{ trans('label.add') }}");
				}

			});




		   // insert data to database
		   $(document).off('click', '#btn_new_staff_charge_main').on('click', '#btn_new_staff_charge_main', function(e){
				$(".staff_charge .alert-success").hide();
				$(".staff_charge .alert-danger").hide();
				var s_id = $("#s_id").val();
				if(s_id != 0){
					var s_id = $("#s_id").val();

					var location_id = '{{ $location->id }}';
					var iddata = $(this).attr('iddata');
					var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
					var checkType = $(this).attr('type');
					// console.log(location_id);
					// console.log(s_id);
					// console.log(checkType);
					// return false;
					$.ajax({
			              url: '{{URL::to('/')}}/storestafflocation',
			              type: 'GET',
			              data: {s_id:s_id,
			              		 location_id:location_id,
			              		 checkType:checkType,
			              		 iddata:iddata,
			              		 idDataTable:idDataTable
			              },
			              success: function(data) {
			                if(data.status=="success"){
			                  $('tr.form_add.smart-form').after(data.msg);
			                  $(".staff_charge .alert-success").show();
			                  $(".staff_charge .alert-success").find('span').text("{{trans('message.add_success')}}");
			                  $(".staff_charge select").val("");
			                  $("#btn_add_staff_charge").text("{{trans('label.delete')}}");
			                  $("#btn_new_staff_charge").attr('type','add').attr('iddata');
			                }else if(data.status=="updatesuccess"){
			                	$(".staff_charge .alert-success").show();
			                  	$(".staff_charge .alert-success").find('span').text("{{trans('message.update_success')}}");
			                  	$(".staff_charge select").val("");
			                  	$(".staff_charge-"+iddata).html(data.msg);
			                  	$("#btn_add_staff_charge").text("{{trans('label.delete')}}");
			                  	$("#btn_new_staff_charge").attr('type','add').attr('iddata','');
			                }else{
			                	$(".staff_charge .alert-danger").show();
			                  	$(".staff_charge .alert-danger").find('span').text(data.msg);
			                	// alert();
			                  // callPopupLogin();
			                }        
			              }
			        });
				}
				
			});


			$(document).off('click', '.EditStaffCharge_main').on('click', '.EditStaffCharge_main', function(e){
				var id = $(this).attr('id');
				var table_main = 'tbl_block_staff';
				$.ajax({
		              url: '{{URL::to('/')}}/location/getCharge',
		              type: 'GET',
		              data: {id:id,table_main:table_main},
		              success: function(data) {
		                if(data.status=="success"){

		                	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
							// alert(data.msg.chail_id);
							// $("#parent_id_main").val(data.msg.chail_id);
							$("#s_id").select2().select2('val',data.msg.s_id);

							$("#stc_currency_add_main").val(data.msg.stc_currency).trigger("change");
							$('.show_input_add_main').show();
							$("#btn_new_staff_charge_main").attr( "type", "modify" );
							$("#btn_new_staff_charge_main").attr( "idData", data.msg.id );
							
		                }else{
		                	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
		                	$("#btn_new_staff_charge_main").attr( "type", "add" );
		                	$("#btn_new_staff_charge_main").attr( "idData", '' );
		                	$('.show_input_add_main').hide();
		                  // callPopupLogin();
		                }        
		              }
		        });
			});

			$(document).off('click', '.deleteStaffCharge_main').on('click', '.deleteStaffCharge_main', function(e){
				
				var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
	    		if (r == true) {
	    			var id = $(this).attr('id');
	    			$.ajax({
			              url: '{{action("LocationController@deleteStaffCharge_main",[""])}}',
			              type: 'GET',
			              data: {id:id},
			              success: function(data) {
			                if(data.status=="success"){
			                  $(".staff_charge-"+id).remove();
			                }else{
			                	alert(data.msg);
			                }        
			              }
			        });
	    		}
			});




		 
		
		});
		function btnCancel(){
	   		var r = confirm("តើលោកអ្នកចង់ត្រលប់ក្រោយឬទេ?");
		    if (r == true) {
		        document.location.href='{{URL::to('/')}}/staff';
		    }
	    }
	</script>
@stop