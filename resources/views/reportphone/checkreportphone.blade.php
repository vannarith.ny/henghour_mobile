@extends('master')
<?php
use App\Http\Controllers\SaleController;

// $lottery_pay = DB::('tbl_staff_charge')->where('')
?>
@section('title')
<title>របាយការណ៏ Phone</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ Phone</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏ Phone</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'reportphoneFilter', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_page))
							 <?php $page_filter = $var_page; ?>
							 <?php $attr_page = $var_page; ?>
						 @else
							 <?php $page_filter = null; ?>
							 <?php $attr_page = 0; ?>
						 @endif

						 @if(isset($var_sheet))
							 <?php $sheet_filter = $var_sheet; ?>
							 <?php $attr_sheet = $var_page; ?>
						 @else
							 <?php $sheet_filter = null; ?>
							 <?php $attr_sheet = 0; ?>
						 @endif
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

							 <!-- <section class="col col-2">
								 <label class="select">
									 {{ Form::select('type_lottery', ([
                                        '' => trans('result.chooseTypeLottery') ]+$type_lottery),$var_type_lottery,['class' => 'required ','id'=>'type_lottery','sms'=> trans('result.pleaseChooseTypeLottery') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section> -->

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('staff', ([
                                        '' => trans('result.chooseStaff') ]+$staff),$staff_filter,['class' => 'required ','id'=>'staff','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('sheet', ([
                                        '' => trans('result.chooseShift') ]+$sheets),$sheet_filter,['class' => ' ','id'=>'sheet','sms'=> trans('result.pleaseChooseShift') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>



							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('page', ([
                                        '' => trans('result.choosePage') ]+$pages),$page_filter,['class' => ' ','id'=>'pages','sms'=> trans('result.pleaseChoosePage') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" id="submitData" name="submit"  class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					
					 <div class="widget-body no-padding">
						<div class="col-12" style="width:90%; margin:0px auto;" id="main_table">
							
						</div>
					 </div>

						 


		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		$(document).ready(function() {
			// START AND FINISH DATE
			$('#dateStart').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#dateEnd').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#dateEnd').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#dateStart').datepicker('option', 'mixDate', selectedDate);
				}
			});


			function GetAmountList() {
				var dateData = $("#dateStart").val();
				var userID = $("#staff").val();
				var sheetID = $("#sheet").val();
				// console.log(userID);
				// return false;
				$.ajax({
					type: "GET",
					url: '{{URL::to('/')}}/reportphone/dailyReportGet',
					data: {
						userID: userID,
						dateData: dateData,
						sheetID: sheetID
					},
					success: function (n) {
						console.log(n);
						$("#main_table").html(n);
					},
					error: function () {}
				})
				return false;
			}

			$("#submitData").click(function () {
				GetAmountList();
				return false;
			});


		});
	
	</script>
@stop