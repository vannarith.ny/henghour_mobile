<?php 
	if(count($data_array) > 0 ){
		foreach ($data_array as $key => $value) {



			$date_new = $value['date_lottery'];
	        $type_lottery = $value['stc_type'];
	        $staff_new = $value['staff_value'];
	        $id_staff = $value['s_id'];
	        $page_id = $value['page_id'];



            $check = DB::table('summary_lottery_page')
                ->where('p_id',$page_id)
                ->first();
            if($check){
                DB::table('summary_lottery_page')->where('p_id',$page_id)->update(
                    [
                        'type_lottery' => $type_lottery,
                        'date_lottery' => $date_new,
                        'staff_value' => $staff_new,
                        'id_staff' => $id_staff,
                        'time_value' => $value['time_value'],
                        'page_value' => $value['page_num'],
                        'total2digitr' => $value['total2digitr'],
                        'total3digitr' => $value['total3digitr'],
                        'totalsum' => $value['totalsum'],
                        'total2digitrright' => $value['total2digitrright'],
                        'total3digitrright' => $value['total3digitrright'],
                        'total2digits' => $value['total2digits'],
                        'total3digits' => $value['total3digits'],
                        'totalsum_dolla' => $value['totalsum_dolla'],
                        'total2digitsright' => $value['total2digitsright'],
                        'total3digitsright' => $value['total3digitsright'],
                        // 'p_code' => '',
                        // 'p_id' => '',
                        // 'p_code_p' => '',
                        'id_order' => $key+1
                    ]
                );
            }else{
                DB::table('summary_lottery_page')->insert(
                    [
                        'type_lottery' => $type_lottery,
                        'date_lottery' => $date_new,
                        'staff_value' => $staff_new,
                        'id_staff' => $id_staff,
                        'time_value' => $value['time_value'],
                        'page_value' => $value['page_num'],
                        'total2digitr' => $value['total2digitr'],
                        'total3digitr' => $value['total3digitr'],
                        'totalsum' => $value['totalsum'],
                        'total2digitrright' => $value['total2digitrright'],
                        'total3digitrright' => $value['total3digitrright'],
                        'total2digits' => $value['total2digits'],
                        'total3digits' => $value['total3digits'],
                        'totalsum_dolla' => $value['totalsum_dolla'],
                        'total2digitsright' => $value['total2digitsright'],
                        'total3digitsright' => $value['total3digitsright'],
                        // 'p_code' => '',
                        'p_id' => $value['page_id'],
                        // 'p_code_p' => '',
                        'id_order' => $key+1
                    ]
                );
            }



			
		} //end foreach
	} //endif check array has
?>