<?php
use App\Http\Controllers\SaleController;

// $lottery_pay = DB::('tbl_staff_charge')->where('')
?>
@if(isset($report))
	<div class="widget-body no-padding control_width">

		 <div class="">
			 <div class="">
				 <?php
				 	 $page_id = 0;
					 $sum_total_twodigit_r = 0;
					 $sum_total_twodigit_s = 0;
					 $sum_total_threedigit_r = 0;
					 $sum_total_threedigit_s = 0;
					 $controlTotal = count($report)-1;

					 $sum_price_right_twodigit_r = 0;
					 $sum_price_right_twodigit_s = 0;
					 $sum_price_right_threedigit_r = 0;
					 $sum_price_right_threedigit_s = 0;

					 $data_array=array();

		 			
				 ?>
				 @foreach($report as $key => $rowlist)

					 @if($page_id != $rowlist->p_id)

							 {{--#display total amount by page--}}
						 	@if($key > 0)
						 		<div class="col-md-4 display_result">
									 អ៊ួរ (សរុប) :
									 <b><span class="val_r">{{number_format($sum_total_twodigit_r+$sum_total_threedigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_total_twodigit_s+$sum_total_threedigit_s,2)}} $</span></b>
								 </div>
								 <div class="col-md-4 display_result">
									 {{trans('label.total_price_number_2_digit')}} :
									 <b><span class="val_r">{{number_format($sum_total_twodigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_total_twodigit_s,2)}} $</span></b>
								 </div>
								 <div class="col-md-4 display_result">
									 {{trans('label.total_price_number_3_digit')}} :
									 <b><span class="val_r">{{number_format($sum_total_threedigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_total_threedigit_s,2)}} $</span></b>
								 </div>
								 

								 <div class="col-md-6 display_result">
									 {{trans('label.total_price_number_2_digit_right')}} :
									 <b><span class="val_r">{{number_format($sum_price_right_twodigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_price_right_twodigit_s,2)}} $</span></b>
								 </div>

								 <div class="col-md-6 display_result">
									 {{trans('label.total_price_number_3_digit_right')}} :
									 <b><span class="val_r">{{number_format($sum_price_right_threedigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_price_right_threedigit_s,2)}} $</span></b>
								 </div>



								<input type="hidden" id="page_{{$key}}" class="report_total"
									   id_staff = "{{$rowlist->s_id}}"
									   time="{{$rowlist->pav_value}}"
									   staff="{{$rowlist->s_name}}"
									   page="{{$rowlist->p_number}}" 
									   page_id="{{$rowlist->p_id}}"

									   total2digitR="{{$sum_total_twodigit_r}}"
									   total2digitS="{{$sum_total_twodigit_s}}"
									   total3digitR="{{$sum_total_threedigit_r}}"
									   total3digitS="{{$sum_total_threedigit_s}}"
									   total2digitRright="{{$sum_price_right_twodigit_r}}"
									   total2digitSright="{{$sum_price_right_twodigit_s}}"
									   total3digitRright="{{$sum_price_right_threedigit_r}}"
									   total3digitSright="{{$sum_price_right_threedigit_s}}" >




									<?php 
										// $item = Array();
										$item['total2digitr'] = $sum_total_twodigit_r;
										$item['total2digits'] = $sum_total_twodigit_s;
										$item['total3digitr'] = $sum_total_threedigit_r;
										$item['total3digits'] = $sum_total_threedigit_s;
										$item['total2digitrright'] = $sum_price_right_twodigit_r;
										$item['total2digitsright'] = $sum_price_right_twodigit_s;
										$item['total3digitrright'] = $sum_price_right_threedigit_r;
										$item['total3digitsright'] = $sum_price_right_threedigit_s;

										$item['totalsum'] = $sum_total_twodigit_r+$sum_total_threedigit_r;
										$item['totalsum_dolla'] = $sum_total_twodigit_s+$sum_total_threedigit_s;

										// $item['time_value'] = $rowlist->pav_value;
										// $item['staff_value'] = $rowlist->s_name;
										// $item['page_value'] = $rowlist->pav_value;
										// $item['page_id'] = $rowlist->p_id;

										// $data_list = Array();
										// $data_list['id']= $rowlist->s_id;
										// $data_list['item']= $item;

										array_push($data_array,$item);


									?>

							 @endif

							 <?php
							 $sum_total_twodigit_r = 0;
							 $sum_total_twodigit_s = 0;
							 $sum_total_threedigit_r = 0;
							 $sum_total_threedigit_s = 0;

							 $sum_price_right_twodigit_r = 0;
							 $sum_price_right_twodigit_s = 0;
							 $sum_price_right_threedigit_r = 0;
							 $sum_price_right_threedigit_s = 0;


							 ?>


							 </div>
		 				</div>
		 					{{--<button class="btn_print" attr_id="print_page_{{$key}}" attr_date="{{$var_dateStart}}" attr_staff="{{$var_staff}}" attr_sheet="{{$attr_sheet}}" attr_page="{{$attr_page}}">{{trans('label.download_this')}}</button>--}}

						 <div id="print_page_{{$key}}" class="paperStyle" >

								 <div class="col-md-3">
									 {{trans('label.staff_name')}} :
									 <b>{{$rowlist->s_name}} {{$rowlist->p_id}}</b>
								 </div>

								 <div class="col-md-2">
									 {{trans('label.date')}} :
									 <b>{{$rowlist->p_date}}</b>
								 </div>

								 <div class="col-md-2">
									 {{trans('label.number_paper')}} :
									 <b>{{$rowlist->p_number}}</b>
								 </div>

								 <div class="col-md-2">
									 {{trans('label.time')}} :
									 <b>
									 	<?php 
									 		$find = array("0","1","2","3","4","5","8:5"," 8:");
											$replace = array("");
											echo str_replace($find,$replace,$rowlist->pav_value);
									 	?>
									 </b>
								 </div>
								 <div class="col-md-2">
									 @if($rowlist->stc_type == 1)
									 	VN
									 @else
									 	KH
									 @endif
								 </div>

							 <?php 
							 		$find = array("0","1","2","3","4","5","8:5"," 8:");
									$replace = array("");
									$timeDisplay =  str_replace($find,$replace,$rowlist->pav_value);
							 ?>
							 <input type="hidden" id="page_info_{{$key}}" class="report_total_info"
									time="{{$timeDisplay}}"
									staff="{{$rowlist->s_name}}"
									id_staff="{{$rowlist->s_id}}"
									page="{{$rowlist->p_number}}"
									page_id="{{$rowlist->p_id}}"
									page_code="{{$rowlist->p_code}}"
									page_code_p="{{$rowlist->p_code_p}}"
									page_id="{{$rowlist->p_id}}"
									 >

							<?php 
								 $item = Array();
								 // $data_list = Array();
								 $searchVal = array("08:45","10:35","1:00","3:45","6:00","7:45");
								 $replaceVal = array("", "", "","", "", "");
								 $time_value = str_replace($searchVal, $replaceVal, $rowlist->pav_value);
								 $item['time_value'] = trim($time_value);
								 $item['staff_value'] = $rowlist->s_name;
								 $item['page_value'] = $rowlist->p_number;
								 $item['page_id'] = $rowlist->p_id;
								 $item['date_lottery'] = date("d-m-Y", strtotime($rowlist->p_date));
								 $item['date_lottery_db'] = $rowlist->p_date;
								 $item['s_id'] = $rowlist->s_id;
								 $item['stc_type'] = $rowlist->stc_type;
								 $item['page_num'] = $rowlist->p_number;


								 // $data_list['id']= $rowlist->s_id;
							?>

							 <div class="clearfix"></div>
							 <br>
							 <div class="row boder controlColume">
						 <?php
						 	$page_id = $rowlist->p_id;
						 ?>
					 @endif

					 <div class="colume_style" id="Remove_{{$rowlist->r_id}}">
						 <?php
						 $total_twodigit_r = 0;
						 $total_threedigit_r = 0;
						 $total_twodigit_s = 0;
						 $total_threedigit_s = 0;

						 $price_right_twodigit_s = 0;
						 $price_right_twodigit_r = 0;
						 $price_right_threedigit_s = 0;
						 $price_right_threedigit_r = 0;

						 $totalRealLoop = 0;
						 $totalDollaLoop = 0;

						 $checklast = 0;
						 ?>

						 <!-- block list number lottery -->
						 <?php
						 $numberLotterys = DB::table('tbl_number_mobile')
								 ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
								 ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
								 ->where('tbl_number_mobile.r_id',$rowlist->r_id)
								 ->orderBy('tbl_number_mobile.num_sort','ASC')
								 ->orderBy('tbl_number_mobile.num_id','ASC')
								 ->get();

						 $checkGroup = DB::table('tbl_number_mobile')
						 		 ->select('tbl_number_mobile.num_sort', DB::raw('count(*) as total'))
								 ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
								 ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
								 ->where('tbl_number_mobile.r_id',$rowlist->r_id)
								 ->groupBy('tbl_number_mobile.num_sort')->get();

						$numGroup = count($checkGroup);
						// var_dump($checkGroup);

						 $storeOrder = 0;
						 $countGroupLoop = 0;
						 $loopDynamic = 0;
						 ?>
						@foreach($numberLotterys as $number)
							@php
								$loopDynamic++;
							@endphp
						 	
						 	
							@if($storeOrder != $number->num_sort)
							 	@if( ($totalRealLoop > 0 ) || ($totalDollaLoop > 0))
								 	<div class="totalByGroup">
								 		@if( $totalRealLoop > 0 && $totalDollaLoop > 0)
								 			{{number_format($totalRealLoop)}} / {{$totalDollaLoop}}$
								 		@elseif($totalRealLoop > 0 && $totalDollaLoop == 0)
								 			{{number_format($totalRealLoop)}}
								 		@else
								 			{{$totalDollaLoop}}$
								 		@endif
								 	</div>
								@endif
									 <?php

									 	$storeOrder = $number->num_sort;
									 	
									 	$totalRealLoop = 0;
							 			$totalDollaLoop = 0;

							 			$loopDynamic = 1;
							 			$countGroupLoop = $countGroupLoop + 1;
									 ?>
									 <div class="pos_style" id="pos_style_{{$number->num_sort}}">{{$number->g_name}}</div>
							@endif







						 <?php
							 $numberDisplay = '';  //display number to fromte
							 $displayCurrency = '';

							//	Resulte price by row
							 $price_result = App\Model\Report::calculate( $number->num_number,$number->num_end_number,$number->num_sym,$number->num_reverse,$number->num_price,$number->num_currency,$number->g_id,$rowlist->p_time,$rowlist->p_date,$number->num_type);
							 $val_price = explode("-", $price_result);
							 $num_right = "";
							 if($val_price[2] > 0){

								 $actived = 'actived';
								 
								 if($val_price[2]>1){
								 	$num_right = '<div class="right_num"><b>* '.$val_price[2].'</b></div>';
								 }

							 }else{
								 $actived = '';
							 }

							 if($number->num_currency == 2){
								 $currencySym = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
								 $displayCurrency = ' '.$currencySym->pav_value;
								 if($val_price[1]=='2'){
									 $total_twodigit_s = $total_twodigit_s + $val_price['0'];
									 $price_right_twodigit_s = $price_right_twodigit_s + ($number->num_price * $val_price[2]);

									 $totalDollaLoop = $totalDollaLoop + $val_price['0'];
								 }else{
									 $total_threedigit_s = $total_threedigit_s + $val_price['0'];
									 $price_right_threedigit_s = $price_right_threedigit_s + ($number->num_price * $val_price[2]);

									 $totalDollaLoop = $totalDollaLoop + $val_price['0'];
								 }

							 }else{

								 if($val_price[1]=='2'){
									 $total_twodigit_r = $total_twodigit_r + $val_price['0'];
									 $price_right_twodigit_r = $price_right_twodigit_r + ($number->num_price * $val_price[2]);

									 $totalRealLoop = $totalRealLoop + $val_price['0'];
								 }else{
									 $total_threedigit_r = $total_threedigit_r + $val_price['0'];
									 $price_right_threedigit_r = $price_right_threedigit_r + ($number->num_price * $val_price[2]);

									 $totalRealLoop = $totalRealLoop + $val_price['0'];
								 }


							 }



							 if($number->num_sym == 7){ //check sym is -
								 $numberDisplay .='
								<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

									<div class="number_lot lot_int">'.$number->num_number.'</div>';
								 if($number->num_reverse == 1){
									 $numberDisplay .= '<div class="symbol_lot lot_int">x</div>';
								 }else{
									 $numberDisplay .= '<div class="symbol_lot lot_int">-</div>';
								 }

								 $numberDisplay .= '
									<div class="amount_lot lot_int">'.round($number->num_price,2).$displayCurrency.'</div>
									<div class="clear"></div>
									'.$num_right.'
								</div>
								';
							 }else if($number->num_sym == 8){

								 if($number->num_reverse == 1){
									 $end_number_new = '';
									 if($number->num_end_number == ''){
										 $end_number_new = substr($number->num_number, 0, -1).'9';
									 }else{
										 $end_number_new = $number->num_end_number;
									 }
									 $numberDisplay .='
									<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

										<div class="number_lot lot_int">'.$number->num_number.'</div>
										<div class="symbol_lot lot_int">x</div>
										<div class="clear"></div>
										<div class="display_total_number">'.SaleController::calculate_number($number->num_number,$end_number_new,1).'</div>
										<div class="number_lot lot_int clear_margin">
											|
											<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
											<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
										</div>
										<div class="clear"></div>
										<div class="number_lot lot_int">'.$end_number_new.'</div>
										<div class="symbol_lot lot_int">x</div>
										<div class="clear"></div>
										'.$num_right.'
									</div>
									';
								 }else{
									 $check = substr($number->num_number, -1);
									 if($check == '0' && $number->num_end_number == ''){
										 $numberDisplay .='
											<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

												<div class="number_lot lot_int">'.$number->num_number.'</div>
												<div class="symbol_lot lot_int"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
												<div class="amount_lot lot_int">'.round($number->num_price,2).$displayCurrency.'</div>
												<div class="clear"></div>
												'.$num_right.'
											</div>
											';
									 }else{
										 $end_number_new = '';
										 if($number->num_end_number == ''){
											 $end_number_new = substr($number->num_number, 0, -1).'9';
										 }else{
											 $end_number_new = $number->num_end_number;
										 }
										 $numberDisplay .='
										<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

											<div class="number_lot lot_int">'.$number->num_number.'</div>
											<div class="clear"></div>
											<div class="display_total_number">'.SaleController::calculate_number($number->num_number,$end_number_new,0).'</div>
											<div class="number_lot lot_int clear_margin">
												|
												<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
												<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
											</div>
											<div class="clear"></div>
											<div class="number_lot lot_int">'.$end_number_new.'</div>
											<div class="clear"></div>
											'.$num_right.'
										</div>
										';
									 }

								 }
							}else if($number->num_sym == 10){ //check sym is alot digit to 3 digit
				                $numberDisplay .='
				                <div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">
				                    
				                    <div class="number_lot lot_int newdigitalot">'.$number->num_number.'</div>';
				                    if($number->num_reverse == 1){
				                        $numberDisplay .= '<div class="symbol_lot lot_int ">x</div>';
				                    }else{
				                        $numberDisplay .= '<div class="symbol_lot lot_int ">-</div>';
				                    }
				                    
				                $numberDisplay .= '
				                    <div class="amount_lot lot_int newdigitalot_price">'.round($number->num_price,2).$displayCurrency.'</div>
				                    <div class="clear"></div>
				                    '.$num_right.'
				                </div>
				                '; 
				            }else if($number->num_sym == 11){ //check sym is alot digit to 3 digit
				            	$numberDisplay .='
				                <div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">';
				                $explotData = explode("/", $number->num_number);
				                $leftData = '';
				                $centerData = '';
				                $rightData = '';
				                if(isset($explotData[0])){
				                	$explotData_sub = explode(",", $explotData[0]);
				                	foreach ($explotData_sub as $key0 => $value_sub) {
				                		if($value_sub != ''){
				                			$leftData .='<li>'.$value_sub.'</li>';
				                		}
				                	}
				                }
			                	
				                if(isset($explotData[1])){
				                	$explotData_sub = explode(",", $explotData[1]);
				                	foreach ($explotData_sub as $key1 => $value_sub) {
				                		if($value_sub != ''){
				                			$centerData .='<li>'.$value_sub.'</li>';
				                		}
				                		
				                	}
				                }

				                if(isset($explotData[2])){
				                	$explotData_sub = explode(",", $explotData[2]);
				                	foreach ($explotData_sub as $key2 => $value_sub) {
				                		if($value_sub != ''){
				                			$rightData .='<li>'.$value_sub.'</li>';
				                		}
				                	}
				                }

				                if($leftData == ''){
							        $leftData = $centerData;
							        $centerData = $rightData;
							        $rightData = '';
							    }else if($centerData == ''){
							        $centerData = $rightData;
							        $rightData = '';
							    }

							    if($number->num_type == 1){
							    	$labelTypeNum = 'ដកគូ';
							    }else if($number->num_type == 2){
							    	$labelTypeNum = 'ដកសេស';
							    }else{
							    	$labelTypeNum = '';
							    }

				                $numberDisplay .= '
				                	<div class="row_lottery_list_new">
					                    <div class="txt_left">
					                        <ul>
					                        	'.$leftData.'
					                        </ul>
					                    </div>
					                    <div class="txt_center">
					                        <ul>
					                        	'.$centerData.'
					                        </ul>
					                    </div>
					                    <div class="txt_right">
					                       <ul>
					                        	'.$rightData.'
					                        </ul>
					                    </div>
					                    <div class="clear"></div>
					                    <div class="labelnumtype">'.$labelTypeNum.'</div>
					                </div>
				                ';
				                   
				                    
				                $numberDisplay .= '
				                    <div class="number_lot lot_int newdigitalot_price">'.SaleController::calculate_totalnumber_new($number->num_number,$number->num_type).'</div>
				                    <div class="symbol_lot lot_int ">x</div>
				                    <div class="amount_lot lot_int newdigitalot_price">'.round($number->num_price,2).$displayCurrency.'</div>
				                    <div class="clear"></div>
				                    '.$num_right.'
				                </div>
				                '; 

							 }else{
								 if($number->num_reverse == 1){
									 $numberDisplay .='
										<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

											<div class="number_lot lot_int">'.$number->num_number.'</div>
											<div class="symbol_lot lot_int">x</div>
											<div class="clear"></div>
											<div class="number_lot lot_int clear_margin">
												|
												<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
												<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
											</div>
											<div class="clear"></div>
											<div class="number_lot lot_int">'.$number->num_end_number.'</div>
											<div class="symbol_lot lot_int">x</div>
											<div class="clear"></div>
											'.$num_right.'
										</div>
										';
								 }else{
									 $numberDisplay .='
										<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

											<div class="number_lot lot_int">'.$number->num_number.'</div>
											<div class="clear"></div>
											<div class="number_lot lot_int clear_margin">
												|
												<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
												<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
											</div>
											<div class="clear"></div>
											<div class="number_lot lot_int">'.$number->num_end_number.'</div>
											<div class="clear"></div>
											'.$num_right.'
										</div>
										';
								 }
							 }
							 echo $numberDisplay;
							 $numGroupDynamic = $checkGroup[$countGroupLoop-1]->total;
							 // var_dump($numGroupDynamic);
							 // var_dump($loopDynamic);
							 ?>

							@if( ($countGroupLoop == $numGroup) && ($loopDynamic == $numGroupDynamic) )
								 @if( ($totalRealLoop > 0 ) || ($totalDollaLoop > 0))
								 	<div class="totalByGroup">
								 		@if( $totalRealLoop > 0 && $totalDollaLoop > 0)
								 			{{number_format($totalRealLoop)}} / {{$totalDollaLoop}}$
								 		@elseif($totalRealLoop > 0 && $totalDollaLoop == 0)
								 			{{number_format($totalRealLoop)}}
								 		@else
								 			{{$totalDollaLoop}}$
								 		@endif
								 	</div>
								@endif
							@endif

							 
						 @endforeach
						 <div class="clearfix"></div>

						 <?php 
						 	$sumamount_r = $total_twodigit_r + $total_threedigit_r;
						 	$sumamount_s = $total_twodigit_s + $total_threedigit_s;
						 ?>

						 <div class="result_price_top" id="row_result_2_{{$rowlist->r_id}}">					{{number_format($total_twodigit_r)}} ៛  
						 	<span class="pull-right">{{round($total_twodigit_s,2)}} $</span>
						 </div>

						 <div class="result_price" id="row_result_3_{{$rowlist->r_id}}">{{number_format($total_threedigit_r)}} ៛  <span class="pull-right">{{round($total_threedigit_s,2)}} $</span></div>

						 <div class="result_price_main" id="row_result_3_{{$rowlist->r_id}}">{{number_format($sumamount_r)}} ៛  <span class="pull-right">{{round($sumamount_s,2)}} $</span></div>


					 </div>
					 		 <?php $sum_total_twodigit_r = $sum_total_twodigit_r + $total_twodigit_r;?>
							 <?php $sum_total_twodigit_s = $sum_total_twodigit_s + $total_twodigit_s;?>
							 <?php $sum_total_threedigit_r = $sum_total_threedigit_r + $total_threedigit_r;?>
							 <?php $sum_total_threedigit_s = $sum_total_threedigit_s + $total_threedigit_s;?>

							 <?php $sum_price_right_twodigit_r = $sum_price_right_twodigit_r + $price_right_twodigit_r;?>
							 <?php $sum_price_right_twodigit_s = $sum_price_right_twodigit_s + $price_right_twodigit_s;?>
							 <?php $sum_price_right_threedigit_r = $sum_price_right_threedigit_r + $price_right_threedigit_r;?>
							 <?php $sum_price_right_threedigit_s = $sum_price_right_threedigit_s + $price_right_threedigit_s;?>



							 {{--#display total amount by page--}}
							 @if($controlTotal == $key)

							 	<div class="col-md-4 display_result">
									 អ៊ួរ (សរុប) :
									 <b><span class="val_r">{{number_format($sum_total_twodigit_r+$sum_total_threedigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_total_twodigit_s+$sum_total_threedigit_s,2)}} $</span></b>
								 </div>
								 <div class="col-md-4 display_result">
									 {{trans('label.total_price_number_2_digit')}} :
									 <b><span class="val_r">{{number_format($sum_total_twodigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_total_twodigit_s,2)}} $</span></b>
								 </div>

								 <div class="col-md-4 display_result">
									 {{trans('label.total_price_number_3_digit')}} :
									 <b><span class="val_r">{{number_format($sum_total_threedigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_total_threedigit_s,2)}} $</span></b>
								 </div>


								 <div class="col-md-6 display_result">
									 {{trans('label.total_price_number_2_digit_right')}} :
									 <b><span class="val_r">{{number_format($sum_price_right_twodigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_price_right_twodigit_s,2)}} $</span></b>
								 </div>

								 <div class="col-md-6 display_result">
									 {{trans('label.total_price_number_3_digit_right')}} :
									 <b><span class="val_r">{{number_format($sum_price_right_threedigit_r)}} ៛</span></b>
									 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 <b><span class="val_s">{{round($sum_price_right_threedigit_s,2)}} $</span></b>
								 </div>

								 <input type="hidden" id="page_{{$key}}" class="report_total"
								 		id_staff = "{{$rowlist->s_id}}"
								 		time="{{$rowlist->pav_value}}"
									    staff="{{$rowlist->s_name}}"
									    page="{{$rowlist->p_number}}"
									    page_id="{{$rowlist->p_id}}"

										total2digitR="{{$sum_total_twodigit_r}}"
										total2digitS="{{$sum_total_twodigit_s}}"
										total3digitR="{{$sum_total_threedigit_r}}"
										total3digitS="{{$sum_total_threedigit_s}}"
										total2digitRright="{{$sum_price_right_twodigit_r}}"
										total2digitSright="{{$sum_price_right_twodigit_s}}"
										total3digitRright="{{$sum_price_right_threedigit_r}}"
										total3digitSright="{{$sum_price_right_threedigit_s}}">


									<?php 
										
										$item['total2digitr'] = $sum_total_twodigit_r;
										$item['total2digits'] = $sum_total_twodigit_s;
										$item['total3digitr'] = $sum_total_threedigit_r;
										$item['total3digits'] = $sum_total_threedigit_s;
										$item['total2digitrright'] = $sum_price_right_twodigit_r;
										$item['total2digitsright'] = $sum_price_right_twodigit_s;
										$item['total3digitrright'] = $sum_price_right_threedigit_r;
										$item['total3digitsright'] = $sum_price_right_threedigit_s;

										$item['totalsum'] = $sum_total_twodigit_r+$sum_total_threedigit_r;
										$item['totalsum_dolla'] = $sum_total_twodigit_s+$sum_total_threedigit_s;

										// $item['time_value'] = $rowlist->pav_value;
										// $item['staff_value'] = $rowlist->s_name;
										// $item['page_value'] = $rowlist->pav_value;
										// $item['page_id'] = $rowlist->p_id;

										// $data_list = Array();
										
										// $data_list['item']= $item;

										array_push($data_array,$item);

										$item = Array();
								 		// $data_list = Array();


									?>


							 @endif


						

				 @endforeach
				 <?php 
				 	var_dump($data_array);
				 ?>
				 @include('reportautoeveryday/summarylotterypage')
				 @include('reportautoeveryday/summary_lottery_page_by_page')
				 

			 </div>

		 </div>

 @endif