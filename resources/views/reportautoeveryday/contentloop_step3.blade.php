@if(isset($reports))
	<div id="printElement">
        <style>
            table.displayReport{
                font-size: 12px;
				border-collapse: collapse;
            }
            table.displayReport tr th{
                font-size: 12px;
                vertical-align: top;
                text-align:center !important;
				padding: 10px 5px;
				background: #5698d2 !important;
				border: 1px #000 solid !important;
            }
			table.displayReport tr td{
				padding: 8px 3px;
				border: 1px #000 solid !important;
				
			}
			table.displayReport thead{

			}
			table.displayReport thead.footer th{
				background:none !important;
			}
			table.displayReport thead.footer th.backgroundSubTotal{
				background: #5698d2 !important;
				
			}
			table.displayReport thead.footer th.clearBor{
				border:none !important;
				border-bottom:none !important;
			}
        </style>
        <div style="padding-left: 20px; padding-bottom: 20px; font-size: 14px;">
			<span style="padding-left: 50px;">ឈ្មោះកូន: {{$mainNameStaffInfo->s_name}}</span>
			<span style="padding-left: 50px;">កាលបរិច្ឆេទ: {{$dateCall}}</span>
        </div>
		 <table  class="table-bordered-new  displayReport" border="0" cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <tr>
					<th rowspan="2" v-align="top">ល.រ</th>
                    <th rowspan="2" v-align="top">កូន</th>
                    <th colspan="2" >លទ្ធផលមូយថ្ងៃ</th>
                    <th colspan="2">លុយទឹក</th>
                    <th colspan="2">ទឹកលុយសរុប 100%</th>
                    <th colspan="3" >ភាគរយ ឈ្មួញកណ្ដាល %</th>
                    <th colspan="3" class="background">សរុបចុងក្រោយ</th>
                </tr>
                <tr>
                    <th>រៀល(៛) </th>
                    <th>ដុល្លា($)</th>

                    <th>រៀល(៛) </th>
                    <th>ដុល្លា($)</th>

                    <th>រៀល(៛) </th>
                    <th>ដុល្លា($)</th>
					<th>ភាគរយ(%) </th>
                    <th>រៀល(៛) </th>
                    <th>ដុល្លា($)</th>

                    <th class="">រៀល(៛) </th>
                    <th class="">ដុល្លា($)</th>
                </tr>
            </thead>
            <tbody class="display_total_result">
            <?php
                $checkStaff = '';
                $checkTime = '';
                $count = 0;

                $total2 = 0;
                $total3 = 0;

                $total2DD = 0;
                $total3DD = 0;

                $Wtotal2DR = 0;
                $Wtotal3DR = 0;

                $Wtotal2DD = 0;
				$Wtotal3DD = 0;
				
				$totalRow1 = 0;
				$totalRow2 = 0;
				$totalRow3 = 0;
				$totalRow4 = 0;
				$totalRow5 = 0;
				$totalRow6 = 0;
				$totalRow7 = 0;
				$totalRow8 = 0;
				$totalRow9 = 0;
				$totalRow10 = 0;
				// dd($staffMain);
            ?>
            @foreach ($reports as $key => $row)
                @php 
                    $count = $count + 1;
                @endphp
                <?php 
                	$data = DB::table("tbl_sum_by_paper")
                                ->select(DB::raw('sum(price2digit_r) as totalprice2R, sum(price2digit_d) as totalprice2D, sum(price3digit_r) as totalprice3R, sum(price3digit_d) as totalprice3D'))
                                ->where('s_id', $row->s_id)
                                ->where('date', $row->date)
								->first();
								
						if($row->stc_type == 1){
							$water2D =  $mainNameStaffInfo->s_two_digit_charge;
							$water3D =  $mainNameStaffInfo->s_three_digit_charge;
							$winner2D =  $mainNameStaffInfo->s_two_digit_paid;
							$winner3D =  $mainNameStaffInfo->s_three_digit_paid;
						}else{
							$water2D =  $mainNameStaffInfo->s_two_digit_kh_charge;
							$water3D =  $mainNameStaffInfo->s_three_digit_kh_charge;
							$winner2D =  $mainNameStaffInfo->s_two_digit_kh_paid;
							$winner3D =  $mainNameStaffInfo->s_three_digit_kh_paid;
						}
                        // $getStaffCharge = DB::table('tbl_staff_charge')
						// 				 ->where('s_id',$row->s_id)
						// 				 ->where('stc_date','<=',$row->date)
						// 				 ->orderBy('stc_date','DESC')
						// 				 ->first();

						// $staffBoos = DB::table('tbl_staff_charge_main')
						// 				 ->where('chail_id',$row->s_id)
						// 				 ->where('stc_date','<=',$row->date)
						// 				 ->orderBy('stc_date','DESC')
						// 				 ->first();

						$staffchail = DB::table('tbl_staff')->where('s_id',$row->s_id)->first();
						// var_dump($staffchail->percent_data);
                ?>
                <tr>
					<td style="text-align:center !important; width: 3%;">{{$count}}</td>
                    <td align="left" style="text-align:left !important; width: 10%;">{{$row->s_name}}
                    	@if($row->stc_type == 1)
                    		VN
                    	@else
                    		KH
                    	@endif
                    </td>
                    
                    <!-- // total amount child water -->
                    <?php 
                    	$totalAmount2R = $row->oneday_price_r;
                    	$totalAmount2D = $row->oneday_price_d;
                    	if($staffchail->percent_data > 0){
                    		// $totalAmount2R = $totalAmount2R * (100-$staffchail->percent_data)/100;
                    		// $totalAmount2D = $totalAmount2D * (100-$staffchail->percent_data)/100;
                    	}
                    ?>
                    <td class=" " align="right" style="width: 5% !important;">{{number_format($totalAmount2R)}}</td>
                    <td class=" " align="right" style="width: 7% !important;">{{number_format($totalAmount2D,2)}}</td>
					
                    <!-- // totam amount main water -->
                    <?php 
                    	// var_dump($row->date);
                        

						// var_dump($getStaffCharge->stc_three_digit_charge,$staffBoos->stc_three_digit_charge,$row->s_id);

                    if($data){
                        $totalWater2 = (float) ($water2D - $water2D);
                        // var_dump($totalWater2);
                        $totalWater3 = (float) ($water3D - $water3D);
                        // var_dump($totalWater3);
                        $amount2DR = $data->totalprice2R;
                        $amount2DD = $data->totalprice2D;
                        $amount3DR = $data->totalprice3R;
                        $amount3DD = $data->totalprice3D;
                        if($staffchail->percent_data > 0){
                        	$amount2DR = $amount2DR * (100-$staffchail->percent_data)/100;
                        	$amount2DD = $amount2DD * (100-$staffchail->percent_data)/100;
                        	$amount3DR = $amount3DR * (100-$staffchail->percent_data)/100;
                        	$amount3DD = $amount3DD * (100-$staffchail->percent_data)/100;
                        }
                        // var_dump($staffBoos->percent_data);

                        $amount2R = $amount2DR * ($totalWater2/100);
                        
                        $amount2D = $amount2DD * ($totalWater2/100);

                        $amount3R = $amount3DR * ($totalWater3/100);
                        $amount3D = $amount3DD * ($totalWater3/100);
                        // var_dump($amount2R);
                        // var_dump($amount3R);
                        $totalReal = $amount2R + $amount3R;
                        $totalDollar = $amount2D + $amount3D;
                    }else{
                        $totalReal = 0;
                        $totalDollar = 0;
					}

					// $checkPrice = substr($totalReal, -2);
					// if($checkPrice <= 50 ){
					// 	$totalReal = $totalReal - $checkPrice;
					// }else{
					// 	$totalReal = $totalReal + (100 - $checkPrice);
					// }

					// var_dump($totalReal);
					$totalRow1 = $totalRow1 + $totalAmount2R;
					$totalRow2 = $totalRow2 + $totalAmount2D;

                    $totalReal = $totalReal * -1;
					$totalDollar = $totalDollar * -1;
					
					$totalRow3 = $totalRow3 + $totalReal;
					$totalRow4 = $totalRow4 + $totalDollar;

                    ?>
                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($totalReal) }}</td>
                    <td class=" " align="right" style="width: 7% !important;">{{ number_format($totalDollar,2) }}</td>
                    
                    <!-- get data main payment -->
                    <?php 
                        $mainPaymentReal = (float) ($totalAmount2R + $totalReal);
						$mainPaymentDollar = (float) ($totalAmount2D + $totalDollar);

						$totalRow5 = $totalRow5 + $mainPaymentReal;
						$totalRow6 = $totalRow6 + $mainPaymentDollar;
						
                    ?>
                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($mainPaymentReal) }}</td>
                    <td class=" " align="right" style="width: 7% !important;">{{ number_format($mainPaymentDollar,2) }} </td>

                    <!-- percent main  -->
                    <?php 
                        // var_dump($staffBoos->percent);
                        $mainPaymentPercentReal = (float) ($mainPaymentReal * ($staffchail->percent_data/100));
						$mainPaymentPercentDollar = (float) ($mainPaymentDollar * ($staffchail->percent_data/100));
						$totalRow7 = $totalRow7 + $mainPaymentPercentReal;
						$totalRow8 = $totalRow8 + $mainPaymentPercentDollar;
                    ?>
					<td class=" " align="right" style="width: 5% !important;">{{ $staffchail->percent_data }} %</td>
                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($mainPaymentPercentReal) }}</td>
                    <td class=" " align="right" style="width: 7% !important;">{{ number_format($mainPaymentPercentDollar,2) }}$</td>

                    <!-- percent boss  -->
                    <?php 
                        $bossPaymentReal = $mainPaymentReal - $mainPaymentPercentReal;
                        $bossPaymentDollar = $mainPaymentDollar - $mainPaymentPercentDollar;
                        
                        $total2 = $total2 + round($bossPaymentReal);
						$total3 = $total3 + round($bossPaymentDollar,2);
						// var_dump($total3.'=> bossPaymentDollar');
						$totalRow9 = $totalRow9 + round($bossPaymentReal);
						$totalRow10 = $totalRow10 + round($bossPaymentDollar,2);
                    ?>
                    <td class="" align="right" style="width: 7% !important;">{{ number_format($bossPaymentReal) }}</td>
                    <td class="" style="width: 7% !important;" colspan="2" align="right">{{ number_format($bossPaymentDollar,2) }}$</td>
                </tr>

            @endforeach
			</tbody>
			<thead class="footer">
				<tr>
                    <th colspan="2" align="right" class="backgroundSubTotal" style="text-align: right !important;">សរុប</th>

                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow1) }}</th>
                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow2, 2) }} $</th>
					<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow3) }}</th>
                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow4,2) }} $</th>
					<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow5) }}</th>
                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow6, 2) }} $</th>
					<th class="backgroundSubTotal" style="text-align: right !important;"></td>
					<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow7) }}</th>
                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow8,2) }} $</th>
					<th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow9) }}</th>
                    <th class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow10,2) }} $</th>
                </tr>

				@if($money_paid)
				<?php 
					foreach($money_paid as $key => $value) {
						if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23){
						
							$total2 = $total2 + $value->st_price_r;
							$total3 = $total3 + $value->st_price_d;

						}else{
							$total2 = $total2 - $value->st_price_r;
							$total3 = $total3 - $value->st_price_d;
						}

						if($value->st_price_r < 0){
							$class_totalR = 'redColor';
						}else{
							$class_totalR = 'blueColor';
						}
						if($value->st_price_d < 0){
							$class_totalD = 'redColor'; 
						}else{
							$class_totalD = 'blueColor'; 
						}

						$result = DB::table('tbl_parameter_value')->where('pav_id','=',$value->st_type)->first();
										
						$labelTran = $result->pav_value;

						if($value->st_type == 21 && $value->st_price_r < 0){
							$labelTran = 'ខាតចាស់'; 
						}else if($value->st_type == 21 && $value->st_price_r >= 0){
							$labelTran = 'សុីចាស់'; 
						}
						

						if($value->st_type == 3 || $value->st_type ==20){
							$class_totalR = 'redColor';
							$class_totalD = 'redColor';
						}
						

				?>
				<tr align="right">
                    <th class="text-right clearBor" align="right" colspan="11" style="text-align: right !important;">{{$labelTran}}</th>
                    <th class="text-right" style="text-align: right !important;"><span class="{{$class_totalR}}">{{number_format($value->st_price_r)}}</span></th>
                    <th class="text-right" style="text-align: right !important;"><span class="{{$class_totalD}}">{{ number_format($value->st_price_d, 2) }} $</span></th>
                </tr>
				<?php 
					}
				?>
				@endif

                <?php 
                    if($total2 > 0){
                        $labelR = '<span class="blueColor">មេសុី</span>';
                        $class_totalR = 'blueColor';
                    }else{
                        $labelR = '<span class="redColor">មេខាត</span>';
                        $class_totalR = 'redColor';
                    }

                    if($total3 >= 0){
                        $labelD = '<span class="blueColor">មេសុី</span>';
                        $class_totalD = 'blueColor';
                    }else{
                        $labelD = '<span class="redColor">មេខាត</span>';
                        $class_totalD = 'redColor';
                    }
                ?>
                <tr>
                    <th class="clearBor" colspan="11" align="right" style="text-align: right !important;">{!! $labelR !!}</th>
                    <th class="CheckData" style="text-align: right !important;"><span class="{{$class_totalR}}">{{number_format($total2)}}</span></th>
                    <th class="" style="text-align: right !important;"><span class="{{$class_totalD}}">{{ number_format($total3, 2) }} $</span></th>
                </tr>
            </thead>
         </table>
        <style>
            .background{
                background: #c7c7c7;
            }
            .blueColor{
                color: #0000ff !important;
                font-size: 12px !important;
            }
            .redColor{
                color: #ff0000 !important;
                font-size: 12px !important;
            }
        </style>

        <!-- block addmoney_block  database-->
        <?php 
           $totalReal = $total2;
	       $totalDolla = $total3;
	       $totalRealBefore = $totalRow9;
	       $totalDollaBefore = $totalRow10;
	       $staff_id = $staffID;
	       $date_new = $dateCall;

	        $check = DB::table('tbl_staff_transction')->where('s_id',$staff_id)->where('st_type','21')->where('st_date_search',$date_new)->first();
	        if($check){

	          DB::table('tbl_staff_transction')->where('s_id',$staff_id)->where('st_type','21')->where('st_date_search',$date_new)->update([
	                          'price_one_day_r' => $totalRealBefore,
	                          'price_one_day_d' => $totalDollaBefore,
	                          'st_price_r' => $totalReal,
	                          'st_price_d' => $totalDolla,
	                          'st_remark' => "Update From System (staff auto loop one day)",
	                          'st_by' => Session::get('iduserlot')
	                          ]
	                      );

	        }else{

	          $id_s = 1 ;
	          $draft =  DB::table('tbl_staff_transction')->orderBy('st_id', 'DESC')->take(1)->get();
	          if ($draft){
	              foreach ($draft as $id) {
	                      $id_s = $id->st_id + 1;
	              }
	          }
	          DB::table('tbl_staff_transction')->where('s_id',$staff_id)->where('st_type','<>','21')->update([
	                          'paid' => 1
	                          ]
	                      );
	          DB::table('tbl_staff_transction')->insert([
	                          'st_id' => $id_s,
	                          'price_one_day_r' => $totalRealBefore,
	                          'price_one_day_d' => $totalDollaBefore,
	                          'st_price_r' => $totalReal,
	                          'st_price_d' => $totalDolla,
	                          'st_type' => '21',
	                          'st_date_diposit' => $date_new,
	                          'st_date_search' => $date_new,
	                        'st_remark' => "Add From System (staff auto loop one day)",
	                          's_id' => $staff_id,
	                          'st_by' => Session::get('iduserlot')
	                          ]
	                      );
	        }

        ?>
@endif