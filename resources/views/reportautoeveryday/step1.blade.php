@extends('master')
<?php
use App\Http\Controllers\SaleController;

// $lottery_pay = DB::('tbl_staff_charge')->where('')
?>
@section('title')
<title>របាយការណ៏ Auto</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			/*background-color: #dff3f5;*/
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 16px;
		}
		table.summary_result_per_day td{
			font-size: 14px;
			font-weight: bold;
		}
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 4px 2px !important;
		}

		.totalByGroup{
			/*width: 100%;*/
			/*background: #;*/
			text-align: center;
			font-size: 13px;
			font-weight: bold;
			color: #FF9033;
			border: 1px #FF9033 solid; 
		}

		.bee_highlight.background th{
			background: #e8e8e8;
		}
		.hidetableTD{
			display:none !important;
		}

		.fc-border-separate thead tr, .table thead tr{
			background-image:none !important;
			background: #5698d2;
		}
		.table-bordered>thead>tr>th{
			border: 1px solid #8c9dad;
		}


	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ Auto</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏ Auto</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">

		         	<!-- // block call report data -->
		         	@for($typeLottery=1; $typeLottery <= 2; $typeLottery++)
					 	<?php
					 		$staffs = DB::table('tbl_paper_mobile')
							            ->select('s_id')
							            ->where('p_date',$dateCall)
							            ->where('stc_type',$typeLottery)
							            ->groupBy('s_id')
							            ->pluck('s_id')
							            ->all();
							 // var_dump($staffs);
					 	?>
					 	@if(count($staffs) > 0)
					 		@foreach($staffs as $s => $staff)

					 			<?php 
					 				
					 				$conditions = '';
					 				$conditions .= " tbl_paper_mobile.s_id = '$staff' AND ";
					 				$conditions .= " tbl_paper_mobile.stc_type = '$typeLottery' AND ";
					 				$conditions .= " tbl_paper_mobile.p_date = '$dateCall'";
					 				$report = DB::table('tbl_paper_mobile')
								                ->join('tbl_row_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
								                ->leftjoin('tbl_staff','tbl_paper_mobile.s_id','=','tbl_staff.s_id')
								                ->leftjoin('tbl_parameter_value','tbl_paper_mobile.p_time','=','tbl_parameter_value.pav_id')
								                ->whereRaw($conditions)
								                ->orderBy('tbl_paper_mobile.s_id' ,'ASC')
								                ->orderBy('tbl_parameter_value.pav_value' ,'ASC')
								                ->orderByRaw('LENGTH(tbl_paper_mobile.p_code)')
								                ->orderBy('tbl_paper_mobile.p_code' ,'ASC')
								                ->orderByRaw('LENGTH(tbl_paper_mobile.p_number)')
								                ->orderBy('tbl_paper_mobile.p_number','ASC')
								                ->orderBy('tbl_paper_mobile.p_id' ,'ASC')
								                ->get();
								?>
								   	
								   	@include('reportautoeveryday/contentloop')

					 		@endforeach
					 	@endif

								
					@endfor
					<!-- // end block call report data -->


		    
		         </div>
		         <!-- end widget content -->
							
		        </div>
		        <!-- end widget div -->
				
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

		    <?php 
		    	// need send email  step 1
                Session::put('email_to', 'tongdarasmile168@gmail.com');
                $data_message = 'Success step 1 '.$dateCall;

                Mail::send('emails.debug_step1',
                    array(
                        'title' => $data_message

                    ), function($message)
                {
                    $message->from("lynococosexyboy@gmail.com");
                    $message->to(Session::get('email_to'), 'Auto run report step1')->subject('Cron auto run report log step 1 from BongSrey');
                    $message->cc(['penhsok96@gmail.com']);
                });

                Session::forget('email_to');
            	// end email send 

		    ?>

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		
	
	</script>
@stop