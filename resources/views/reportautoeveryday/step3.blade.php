@extends('master')
<?php
use App\Http\Controllers\SaleController;
?>
@section('title')
<title>របាយការណ៏ Auto step3</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:25px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:18px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			/*background-color: #dff3f5;*/
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: center;
		}

		.display_total_result tr td:nth-child(1) .cssInput{
			text-align: center;
		}

		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}

		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 4px 2px !important;
		}

		.bee_highlight.background th{
			background: #ddd;
		}
		.hidetableTD{
			display:none !important;
		}
        table.table-bordered.summary_result_per_day{
            border: 1px #fff solid !important;
        }

        .table-bordered>tbody>tr{
            border:none !important;
        }

        .table-bordered>thead th{
            background-image:none !important;
			background: #5698d2;
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td{
            border: 1px #000 solid !important;
            border-bottom: none !important;
        }
        .table-bordered>tbody>tr>th{
            border: 1px #000 solid !important;
        }

        .table-bordered>tbody>tr>td.clearBorder,.table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }
        .table-bordered>tbody>tr>th.clearBorder{
            border: none !important;
        }

        .table-bordered>tbody>tr>th.result_right_total{
            border: none !important;
            border-bottom: 1px #000 solid !important;
        }

        /** {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }
        *:before,
        *:after {
        -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
                box-sizing: content-box;
        }*/
        .table-bordered>tbody>tr>td.lr_clear{
            border-left: none !important;
            border-right: none !important;
        }
        .table-bordered>tbody>tr>th.bottom_clear{
            border-bottom: none !important;
        }
        
        .customFormpCode{
        	width: 80% !important;
        	height: auto !important;
        	text-align: center;
        }
        .customnumForm{
        	width: 60px !important;
        	height: auto !important;
        	text-align: center !important;
        	float: left;
        	padding: 5px 5px;

        }
        span.numSumP{
        	/*float: left;*/
        	/*padding-left: 25px;*/
        }

        .cssInput{
        	border-color: #FFF;
        	font-size: 16px;
        	padding: 0px 2px;
        }
        input.cssInput:focus{
        	border: 1px solid #ccc;
        }

        .highlightdata{
        	background-color: #ccc;
        }

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ Auto step3</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">
		
		    
		<!-- widget grid -->
		<section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏ Auto step2</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div role="content">
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding" style="">


	         			<?php
				 		// $staffs = DB::table('tbl_paper_mobile')
						//             ->select('s_id')
						//             ->where('p_date',$dateCall)
						//             ->groupBy('s_id')
						//             ->pluck('s_id')
						//             ->all();
						$staffs = DB::table('tbl_staff')
						            ->select('s_id')
						            ->where('parent_id',0)
						            ->where('s_role',1)
						            ->pluck('s_id')
						            ->all();
						 var_dump($staffs);
					 	?>

					 	@if(count($staffs) > 0)
					 		@foreach($staffs as $s => $staffID)

					 			<?php 
					 				$mainNameStaffInfo = DB::table('tbl_staff')
					                    ->where('s_id', $staffID)
					                    ->first();
					 				$reports = DB::table('tbl_total_everyday')
						                        ->leftjoin('tbl_staff', 'tbl_total_everyday.s_id','=','tbl_staff.s_id')
						                        ->where('tbl_total_everyday.s_id', $staffID)
						                        ->where('tbl_total_everyday.date',$dateCall)
						                        ->where('tbl_total_everyday.stc_type','<>',0)
						                        // ->orderBy('tbl_total_everyday.s_id', 'ASC')
						                        // ->orderByRaw('LENGTH(tbl_staff.s_name)', 'asc')
						                        ->orderBy('tbl_staff.s_name', 'asc')
						                        ->orderByRaw('LENGTH(tbl_staff.s_name)')
						                        ->get();

						            // dd($reports);
				                    $take_last = DB::table('tbl_staff_transction')
				                    ->where('s_id',$staffID)
				                    ->where('st_date_search','<',$dateCall)
				                    ->orderBy('st_date_search','DESC')->first();
				                    if($take_last){
				                        $money_paid = DB::table('tbl_staff_transction')
				                                    ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
				                                    ->where('s_id',$staffID)
				                                    ->where('st_date_search','=',$take_last->st_date_search)
				                                    ->where('st_date_search','<',$dateCall)
				                                    // ->where('st_type','<>','21')
				                                    ->get();
				                    }else{
				                        $money_paid = []; 
				                    }
					 			?>

					 			@include('reportautoeveryday/contentloop_step3')

					 		@endforeach
					 	@endif

                

					



		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
				<div style="text-align: center;">
				<!-- <input type='button' id='btnPrintDiv' value='Print'  style="float:none;"> -->
				</div>
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

		    <?php 
		    	// need send email  step 3
                Session::put('email_to', 'tongdarasmile168@gmail.com');
                $data_message = 'Success step 3 '.$dateCall;

                Mail::send('emails.debug_step1',
                    array(
                        'title' => $data_message

                    ), function($message)
                {
                    $message->from("lynococosexyboy@gmail.com");
                    $message->to(Session::get('email_to'), 'Auto run report step3')->subject('Cron auto run report log step 3 from BongSrey');
                    $message->cc(['penhsok96@gmail.com']);
                });

                Session::forget('email_to');
            	// end email send 

		    ?>

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		
       
	

	
	</script>
@stop