@if(isset($boss_data))

		 <style type="text/css">
			 .smart-form select.formlottery_select{
				 box-sizing: border-box !important;
			 }
			 
			 .paperStyle{
				 display: none;
			 }
			 .paperStyle:last-child{
				 display: block !important;
			 }
			 .staff_name{
				 display: none;
			 }
			 
		 </style>



		 <div class="widget-body control_width">

			

			 <?php
			 $twodigit_charge = 0;
			 $threedigit_charge = 0;

			 $data_array=array();

			

			 $getStaffCharge = DB::table('tbl_staff')
					 ->where('s_id',$staffID)
					 ->first();
				 //dd($getStaffCharge);
			 if(isset($getStaffCharge)){
			 	if($typeLottery == 1){
			 		$twodigit_charge = $getStaffCharge->s_two_digit_charge;
				 	$threedigit_charge = $getStaffCharge->s_three_digit_charge;

				 	$win_two_digit = $getStaffCharge->s_two_digit_paid;
				 	$win_three_digit = $getStaffCharge->s_three_digit_paid;
			 	}else{
			 		$twodigit_charge = $getStaffCharge->s_two_digit_kh_charge;
				 	$threedigit_charge = $getStaffCharge->s_three_digit_kh_charge;

				 	$win_two_digit = $getStaffCharge->s_two_digit_paid;
				 	$win_three_digit = $getStaffCharge->s_three_digit_paid;

			 	}
				 
				 if($twodigit_charge == $threedigit_charge){
				 	// $hidedata = 'hidetwothree_digit';
				 	// $sumHide = '';
				 	// $colspan = 1;

				 	$hidedata = '';
				 	$sumHide = 'hidetwothree_digit';
				 	$colspan = 3;
				 }else{
				 	$hidedata = '';
				 	$sumHide = 'hidetwothree_digit';
				 	$colspan = 3;
				 }

				 

                

			 }else{
			 	
                $win_two_digit = 90;
                $win_three_digit = 800;
			 }





			 ?>
			 
				 <div id="html-content-holder" class="widget-body">
				 	
					 
					 <div class="col-sm-3 col-md-3">
						 {{trans('label.staff_name')}} :
						 <b>{{$getStaffCharge->s_name}} </b>
					 </div>

					 <div class="col-sm-6 col-md-6">
						 {{trans('label.date')}} :
						 <b><?php echo date("d-m-Y", strtotime($dateCall));?></b>
					 </div>
					 <br><br>
					 <table id="" class="table table-bordered summary_result_per_day" style="width:90%;">
						 <thead>
						 <tr>
						 	 <th>
						 	 	@if($typeLottery == 1)
						 	 		VN
						 	 	@else
						 	 		KH
						 	 	@endif
						 	 </th>
							 <th colspan="3" class="pOwnerMan">
							 	ថ្ងៃទី 
						 		<b><?php echo date("d-m-Y", strtotime($dateCall));?></b>
							 </th>
							 <th colspan="{{$colspan}}">KHR</th>
							 <th class="win2DR" >ត្រូវ </th>
							 <th class="win3DR">ត្រូវ </th>
							 <th colspan="{{$colspan}}" class="moneyAllDD">USD</th>
							 <th class="win2DD">ត្រូវ</th>
							 <th class="win3DD">ត្រូវ</th>
						 </tr>
						 <tr>
							 <th style="width:10%;">{{trans('label.staff')}}</th>
							 <th style="width:5%;">វេន</th>
							 <th colspan="1" style="width:10%;" class="pOwner">
			 					ម្ចាស់ក្រដាស់
							 </th>
							 <th style="width:5%;">ល.រ</th>
							 <th class="{{$hidedata}}" style="width:10%;">សរុប</th>
							 <th class="{{$hidedata}}" style="width:10%;">2D</th>
							 <th class="{{$hidedata}}" style="width:10%;">3D</th>
							 <th class="{{$sumHide}}" style="width:10%;">លុយ</th>
							 <th class="win2DR" style="width:10%;">2D</th>
							 <th class="win3DR" style="width:10%;">3D</th>
							 <th class="{{$hidedata}} money2DD" style="width:10%;">សរុប</th>
							 <th class="{{$hidedata}} money2DD" style="width:10%;">2D</th>
							 <th class="{{$hidedata}} money3DD" style="width:10%;">3D</th>
							 <th class="{{$sumHide}} moneyAllDD" style="width:10%;">លុយ</th>
							 <th class="win2DD" style="width:10%;">2D</th>
							 <th class="win3DD" style="width:10%;">3D</th>
						 </tr>
						 
						 </thead>
						 <tbody class="display_total_result">
						 	<?php 
						 		$staff_n = '';
						 		$time_n = '';
						 		$checkTimePa = '';
						 		$countPaper = 0;

						 		$fullAmountR = 0;
						 		$fullAmount2DR = 0;
						 		$fullAmount3DR = 0;

						 		$fullAmountD = 0;
						 		$fullAmount2DD = 0;
						 		$fullAmount3DD = 0;

						 		$fullWin2DR = 0;
						 		$fullWin3DR = 0;
						 		$fullWin2DD = 0;
						 		$fullWin3DD = 0;

						 		$sumPaper = count($boss_data);
						 	?>
						 	@foreach($boss_data as $key => $data)
						 		<?php 

						 			if($key == 0){
						 				$checkTimePa = $data->time_value;
						 			}

			 						/* check % */
									 $staffCheck = DB::table('tbl_staff')->where('s_id', $data->id_staff)->first();
			 						/* var_dump($staffCheck->percent_data); */
							 		if($staff_n != $data->staff_value){
							 			$name_staff_display = $data->staff_value;
							 			$staff_n = $data->staff_value;

							 			$time_n = '';
							 		}else{
							 			$name_staff_display = '';
							 		}

							 		if($time_n != $data->time_value){
							 			$time_display = $data->time_value;

							 			$find = array("0","1","2","3",'8','75');
										$replace = array("");
										$time_display = str_replace($find,$replace,$time_display);

							 			$time_n = $data->time_value;
							 		}else{
							 			$time_display = '';
							 		}
							 		// var_dump(number_format($data->total2digitr));
							 		// dd($main_new);
							 		
                                    $cal_2_digit = $twodigit_charge;
                                    $cal_3_digit = $threedigit_charge;


						 		?>
						 	@if($data->time_value != $checkTimePa )
						 		<?php 
						 			$find = array("0","1","2","3" ,'8','75');
									$replace = array("");
									$timeFinal = str_replace($find,$replace,$checkTimePa);
									$displayPaperNum = $countPaper;
									$countPaper = 0;
						 		?>
						 		<tr>
						 			<td colspan="3" style="text-align:right !important; border-right:none !important; border-left:none !important;"></td>
						 			<td colspan="1" style="text-align:center !important;  border-left:none !important;  border-right:none !important;">{{$timeFinal}} {{$displayPaperNum}}</td>
						 			<td colspan="10" style="text-align:right !important; border-left:none !important; border-right:none !important;"></td>
						 		</tr>
						 	@endif
						 		<tr class="many_row" num="{{$key}}" id="main_{{$key}}">
						 			
						 			<td id="td_staff_{{$key}}" id_staff="{{$data->id_staff}}" two="{{$cal_2_digit}}" three="{{$cal_3_digit}}" percent="{{$staffCheck->percent_data}}" p_code="{{$data->p_code}}" align="center">
						 			
						 				@if(Session::get('roleLot') == 100)
											<span>{{$data->p_code_p}}</span>
										@else
											<input id="p_code_p{{$data->id}}" class="form-control cssInput customFormpCodep" placeholder="Code" name="p_code_p" type="text" value="{{$data->p_code_p}}" pageID="{{$data->id}}" date="{{$dateCall}}" oldData="{{$data->p_code_p}}" p_id="{{$data->p_id}}">
										@endif

						 			</td>
									<td id="td_time_'+index+'">{{$time_display}}</td>
									<td colspan="1" style="width:10%;" class="pOwner">
										@if(Session::get('roleLot') == 100)
											<span>{{$data->p_code}}</span>
										@else
											<input id="p_owner{{$data->id}}" class="form-control cssInput customFormpCode" placeholder="owner" name="p_owner" type="text" value="{{$data->p_code}}" pageID="{{$data->id}}" date="{{$dateCall}}" oldData="{{$data->p_code}}" p_id="{{$data->p_id}}">
										@endif
										
										
									</td>
									@if(Session::get('roleLot') == 100)
										<td id="td_page_'+index+'" style="width:5%; text-align: center !important;">
											<span>{{$data->page_value}}</span>
											<span class="numSumP">({{$key+1}})</span>
										</td>
									@else
									
									<td id="td_page_'+index+'" style="width:5%; text-align: left !important;">
											<input id="page_value{{$data->id}}" class="form-control customnumForm cssInput" placeholder="number" name="page_value" type="text" value="{{$data->page_value}}" oldData="{{$data->page_value}}" pageID="{{$data->id}}" date="{{$dateCall}}" p_id="{{$data->p_id}}">
											<span class="numSumP">({{$key+1}})</span>
									</td>
									@endif

									<?php 

									$amount2DTotal = $data->total2digitr;
									$amount3DTotal = $data->total3digitr;
									$totalfullamount = $amount2DTotal + $amount3DTotal;

									$amount2DTotalD = $data->total2digits;
									$amount3DTotalD = $data->total3digits;
									$totalfullamountD = $amount2DTotalD + $amount3DTotalD;

									//sum amount
									$fullAmountR = $fullAmountR + $totalfullamount;
									$fullAmount2DR = $fullAmount2DR + $amount2DTotal;
									$fullAmount3DR = $fullAmount3DR + $amount3DTotal;


									$fullAmountD = $fullAmountD + $totalfullamountD;
									$fullAmount2DD = $fullAmount2DD + $amount2DTotalD;
									$fullAmount3DD = $fullAmount3DD + $amount3DTotalD;

									$fullWin2DR = $fullWin2DR + $data->total2digitrright;
									$fullWin3DR = $fullWin3DR + $data->total3digitrright;
									$fullWin2DD = $fullWin2DD + $data->total2digitsright;
									$fullWin3DD = $fullWin3DD + $data->total3digitsright;

									?>

									<td class="{{$hidedata}} " >@if($totalfullamount != 0) {!! floatval($totalfullamount) !!} @endif</td>

						 			<td class="{{$hidedata}} total2digitr " >@if($data->total2digitr != 0) {!! floatval($data->total2digitr) !!} @endif</td>
						 			<td class="{{$hidedata}} total3digitr " >@if($data->total3digitr != 0) {!! floatval($data->total3digitr) !!} @endif</td>
						 			<td class="{{$sumHide}} totalsum " >@if($data->totalsum != 0) {{ floatval($data->totalsum) }} @endif</td>

						 			<td class="total2digitrright win2DR" style="width:300px;">@if($data->total2digitrright != 0) {!! floatval($data->total2digitrright) !!} @endif</td>
						 			<td class="total3digitrright win3DR">@if($data->total3digitrright != 0) {{ floatval($data->total3digitrright) }} @endif</td>

						 			<td class="{{$hidedata}} totalfulldigits money2DD">@if($totalfullamountD != 0) {{ floatval($totalfullamountD) }} @endif</td>
						 			<td class="{{$hidedata}} total2digits money2DD">@if($data->total2digits != 0) {{ floatval($data->total2digits) }} @endif</td>
						 			<td class="{{$hidedata}} total3digits money3DD">@if($data->total3digits != 0) {{ floatval($data->total3digits) }} @endif</td>
						 			<td class="{{$sumHide}} totalsum_dolla moneyAllDD">@if($data->totalsum_dolla != 0) {{ floatval($data->totalsum_dolla) }} @endif</td>

						 			<td class="total2digitsright win2DD">@if($data->total2digitsright != 0) {{ floatval($data->total2digitsright) }} @endif</td>
						 			<td class="total3digitsright win3DD">@if($data->total3digitsright != 0) {{ floatval($data->total3digitsright) }} @endif</td>

						 		</tr>
						 	

						 		<?php 
						 			$checkTimePa = $data->time_value;
						 			$countPaper++;
						 		?>

						 	@endforeach

						 		<?php 
						 			$find = array("0","1","2","3",'8','75');
									$replace = array("");
									$timeFinal = str_replace($find,$replace,$checkTimePa);
						 		?>
						 		<tr>
						 			<td colspan="3" style="text-align:right !important; border-right:none !important; border-left:none !important;"></td>
						 			<td colspan="1" style="text-align:center !important;  border-left:none !important;  border-right:none !important;">{{$timeFinal}} {{$countPaper}}</td>
						 			<td colspan="10" style="text-align:right !important; border-left:none !important; border-right:none !important;"></td>
						 		</tr>
						 		<tr class="bee_highlight background">
						 			<th>{{trans('label.total')}}</th>
						 			<th></th>
						 			<th class="pOwner"></th>
						 			<th >{{$sumPaper}}</th>
						 			<th class="{{$hidedata}}">{{$fullAmountR}}</th>
						 			<th class="{{$hidedata}}">{{$fullAmount2DR}}</th>
						 			<th class="{{$hidedata}}">{{$fullAmount3DR}}</th>
						 			<th class="{{$sumHide}}">{{$fullAmountR}}</th>

						 			<th class="win2DR">{{$fullWin2DR}}</th>
						 			<th class="win3DR">{{$fullWin3DR}}</th>

						 			<th class="{{$hidedata}} money2DD">$ {{$fullAmountD}}</th>
						 			<th class="{{$hidedata}} money2DD">$ {{$fullAmount2DD}}</th>
						 			<th class="{{$hidedata}} money3DD">$ {{$fullAmount3DD}}</th>
						 			<th class="{{$sumHide}} moneyAllDD">$ {{$fullAmountD}}</th>

						 			<th class="win2DD">$ {{$fullWin2DD}}</th>
						 			<th class="win3DD">$ {{$fullWin3DD}}</th>
						 		</tr>
						 		<tr class=""><td colspan="12"></td></tr>

						 		<?php 
						 			$sum2DigitBoosReal = ($fullAmount2DR * $twodigit_charge)/100;
									$sum3DigitBoosReal = ($fullAmount3DR * $threedigit_charge)/100;

									$sum2DigitBoosDolla = ($fullAmount2DD * $twodigit_charge)/100;
									$sum3DigitBoosDolla = ($fullAmount3DD * $threedigit_charge)/100;

									$sumtotalAfterWaterR = $sum2DigitBoosReal + $sum3DigitBoosReal;
									$sumtotalAfterWaterD = $sum2DigitBoosDolla + $sum3DigitBoosDolla;


						 		?>

						 		<tr class="bee_highlight">
						 			<th colspan="3" class="pOwnerManFooter blockwater" rowspan="3" style="vertical-align: middle; text-align: center;">ទឹក</th>
						 			<th>2D</th>
						 			<th>{{$fullAmount2DR}}</th>
						 			<th> => </th>
						 			<th>{{$sum2DigitBoosReal}}</th>
						 			<th class="win3DR footer3D"></th>
						 			<th class="footerDollar">$ {{$fullAmount2DD}}</th>
						 			<th class="footerDollar"> => </th>
						 			<th class="footerDollar">$ {{$sum2DigitBoosDolla}}</th>
						 			<th class="footerDollar"></th>
						 		</tr>

						 		<tr class="bee_highlight">
						 			<th>3D</th>
						 			<th>{{$fullAmount3DR}}</th>
						 			<th> => </th>
						 			<th>{{$sum3DigitBoosReal}}</th>
						 			<th class="win3DR footer3D"></th>
						 			<th class="footerDollar">$ {{$fullAmount3DD}}</th>
						 			<th class="footerDollar"> => </th>
						 			<th class="footerDollar">$ {{$sum3DigitBoosDolla}}</th>
						 			<th class="footerDollar"></th>
						 		</tr>

						 		<tr class="bee_highlight highlightdata">
						 			<th>Total</th>
						 			<th>{{$fullAmountR}}</th>
						 			<th> => </th>
						 			<th>{{$sumtotalAfterWaterR}}</th>
						 			<th class="win3DR footer3D"></th>
						 			<th class="footerDollar">$ {{$fullAmountD}}</th>
						 			<th class="footerDollar"> => </th>
						 			<th class="footerDollar">$ {{$sumtotalAfterWaterD}}</th>
						 			<th class="footerDollar"></th>
						 		</tr>

						 		<?php 
						 			$total_r_2x = $fullWin2DR * $win_two_digit;
									$total_r_3x = $fullWin3DR * $win_three_digit;

									$total_d_2x = $fullWin2DD * $win_two_digit;
									$total_d_3x = $fullWin3DD * $win_three_digit;
						 		?>
						 		<tr class="bee_highlight">
						 			<th class="{{$hidedata}} pOwnerManFooter blockwater" colspan="3" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th>
						 			<th class="{{$sumHide}}" colspan="2" rowspan="2" style="vertical-align: middle; text-align: center;">សង</th>
						 			<th>2D(T)</th>
						 			<th>{{$fullWin2DR}}</th>
						 			<th>x{{$win_two_digit}}</th>
						 			<th>{{$total_r_2x}}</th>
						 			<th class="win3DR footer3D"></th>
						 			<th class="footerDollar">{{$fullWin2DD}}</th>
						 			<th class="footerDollar">x{{$win_two_digit}}</th>
						 			<th class="footerDollar">{{$total_d_2x}}</th>
						 			<th class="{{$hidedata}} footerDollar"></th>
						 		</tr>
						 		<tr class="bee_highlight">
						 			<th>3D(T)</th>
						 			<th>{{$fullWin3DR}}</th>
						 			<th>x{{$win_three_digit}}</th>
						 			<th>{{$total_r_3x}}</th>
						 			<th class="win3DR footer3D"></th>
						 			<th class="footerDollar">{{$fullWin3DD}}</th>
						 			<th class="footerDollar">x{{$win_three_digit}}</th>
						 			<th class="footerDollar">{{$total_d_3x}}</th>
						 			<th class="{{$hidedata}} footerDollar"></th>
						 		</tr>

						 		<?php 
						 			$total_income_expense_riel = $sumtotalAfterWaterR - $total_r_2x - $total_r_3x;
						 			$total_income_expense_dollar = $sumtotalAfterWaterD - $total_d_2x - $total_d_3x;

						 			if($total_income_expense_riel < 0){
										$labelBefore_r = 'ខាត';
										$redColor = 'color: red;';
									}else{
										$labelBefore_r = 'សុី';
										$redColor = 'color: #000;';
									}

									if($total_income_expense_dollar < 0){
										$labelBefore_d = 'ខាត';
										$redColorDolar = 'color: red;';
									}else{
										$labelBefore_d = 'សុី';
										$redColorDolar = 'color: #000;';
									}	

						           
						 		?>
						 		<tr class="bee_highlight ">
						 			<th class="clearBorder"></th>
						 			<th class="clearBorder" colspan="2"></th>
						 			<th colspan="3" class="pOwnerManFooter clearBorder" style="text-align:right !important; {!! $redColor !!}">{{$labelBefore_r}}</th>
						 			<th class="result_right_total" style="text-align:center !important; {!!$redColor!!}">{{$total_income_expense_riel}}</th>
						 			<th class="win3DR footer3D"></th>
						 			<th class="footerDollar result_right_total"></th>
						 			<th style="{!!$redColorDolar!!}" class="footerDollar result_right_total">{{$labelBefore_d}}</th>
						 			<th class="result_right_total footerDollar" colspan="2" style="text-align:left !important; {!!$redColorDolar!!}">$ {{$total_income_expense_dollar}}</th>
						 		</tr>


						 		<?php 
						 			if($getStaffCharge->percent_data > 0){
						 				$percent = 100 - $getStaffCharge->percent_data;

						 				$totalReal = ($total_income_expense_riel * $percent)/100;
										$totalDolla = ($total_income_expense_dollar * $percent)/100;
								?>
									<tr class="bee_highlight">
							 			<th class="clearBorder"></th>
							 			<th colspan="2" class="pOwnerManFooter clearBorder"></th>
							 			<th colspan="3" class="clearBorder" style="text-align:right !important;">{{$percent}}%</th>
							 			<th class="result_right_total" style="text-align:center !important;">{{$totalReal}}</th>
							 			<th class="win3DR footer3D"></th>
							 			<th class="footerDollar result_right_total"></th>
							 			<th class="footerDollar result_right_total">%</th>
							 			<th class="result_right_total footerDollar" colspan="2" style="text-align:left !important; ">$ {{$totalDolla}}</th>
							 		</tr>
								<?php

						 			}else{
						 				$percent = 100;
						 				$totalReal = $total_income_expense_riel;
										$totalDolla = $total_income_expense_dollar;
						 			}
						 		?>


						 		<!-- block add money to db -->
						 		<?php 


							        $staff_id = $staffID;
							        $date_new = $dateCall;
							        $var_type_lottery = $typeLottery;
							  
							        $check = DB::table('tbl_total_everyday')->where('s_id',$staff_id)->where('stc_type',$var_type_lottery)->where('date',$date_new)->first();
							        if($check){
							  
							            DB::table('tbl_total_everyday')->where('s_id',$staff_id)->where('stc_type',$var_type_lottery)->where('date',$date_new)->update([
							                                'price_r' => $totalReal,
							                                'price_d' => $totalDolla,
							                                'c_price_r' => $totalReal,
							                                'c_price_d' => $totalDolla,
							                                'oneday_price_r' => $totalReal,
							                                'oneday_price_d' => $totalDolla,
							                                'stc_type' => $var_type_lottery
							                            ]
							                        );
							  
							        }else{
							  
							            
							            DB::table('tbl_total_everyday')->insert([
							                            'price_r' => $totalReal,
							                            'price_d' => $totalDolla,
							                            'c_price_r' => $totalReal,
							                            'c_price_d' => $totalDolla,
							                            'oneday_price_r' => $totalReal,
							                            'oneday_price_d' => $totalDolla,
							                            'date' => $date_new,
							                            's_id' => $staff_id,
							                            'stc_type' => $var_type_lottery

							                            ]
							                        );
							        }

						 		?>
						 		

						 		

						 </tbody>
					 </table>
				</div>
		 </div>




	 @endif