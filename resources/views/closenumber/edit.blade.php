@extends('master')

@section('title')
<title>បញ្ចូលលេខបិត</title>
@stop

@section('cssStyle')
	<style type="text/css">

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li>លេខបិត</li><li>កែប្រែលេខបិត</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-money fa-fw "></i> 
               កែប្រែលេខបិត
		      </h1>
		     </div>
		</div>

		
    	<section id="widget-grid" class="">
    		@include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>
		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
		         <h2>កែប្រែលេខបិត</h2>    
		         
		        </header>

		        <!-- widget div-->
		        <div>
		         
		         <!-- widget edit box -->
		         <div class="jarviswidget-editbox">
		          <!-- This area used as dropdown edit box -->
		          
		         </div>
		         <!-- end widget edit box -->
		         
		         <!-- widget content -->
		         <div class="widget-body no-padding">      
                     {!! Form::model($closeNumber, ['route' => ['closenumber.update', $closeNumber->c_id] ,'method' => 'PATCH',  'class' => 'smart-form user-form', 'novalidate' => 'validate', 'id' => 'checkout-form' ,"autocomplete"=>"false"]) !!}
		           <fieldset>

			            <div class="row">
				             
                             <section class="col col-2">
								 <label class="select">
									 {{ Form::select('sheet_id', ([
                                        '' => "ពេល" ]+$times),$closeNumber->sheet_id,['class' => 'required ','id'=>'sheet_id','sms'=> "ពេល" ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('post_id', ([
                                        '' => 'ប៉ុស' ]+$posts),$closeNumber->post_id,['class' => 'required ','id'=>'post_id','sms'=> 'ប៉ុស' ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

                             <section class="col col-2">
				              <label class="input"> 
				               {!! Form::text("c_number", $closeNumber->c_number, $attributes = array( 'id' => 'c_number','placeholder'=>'លេខបិទ', 'class'=>'form-control ')) !!}
				              </label>
				             </section>

							 <section class="col col-2">
				              <label class="input"> 
				               {!! Form::text("price_limit_r", $closeNumber->price_limit_r, $attributes = array( 'id' => 'price_limit_r','placeholder'=>'ចំនួនទឹកលុយដែលកំណត់បិទ ៛', 'class'=>'form-control ')) !!}
				              </label>
				             </section>
							 <section class="col col-2">
				              <label class="input"> 
				               {!! Form::text("price_limit_d", $closeNumber->price_limit_d, $attributes = array( 'id' => 'price_limit_d','placeholder'=>'ចំនួនទឹកលុយដែលកំណត់បិទ $', 'class'=>'form-control ')) !!}
				              </label>
				             </section>
				        </div>
			        	


			           
					
		           	</fieldset>
		           

		           <footer>
		            <button type="submit" name="submit" class="btn btn-primary">{{trans('label.save')}}</button>
		            <button type="button" class="btn btn-warning" onclick="btnCancel()">{{trans('label.cancel')}}</button>                        
		           </footer>

		           <div class="message">
		            <i class="fa fa-check fa-lg"></i>
		            <p>
		             Your comment was successfully added!
		            </p>
		           </div>
		          {{ Form::close() }}
		          
		         </div>
		         <!-- end widget content -->
		         
		        </div>
		        <!-- end widget div -->
		        
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->
		    
		    

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script src="{{ asset('/') }}js/plugin/jquery-form/jquery-form.min.js"></script>
	<script type="text/javascript">


		$(document).ready(function() {
		   pageSetUp();

           $(document).off('change', '#sheet_id').on('change', '#sheet_id', function(e){
			
                var id = $(this).val();
                $("#post_id").prop( "disabled", true );
                if(id != ''){
                    $.ajax({
                    url: "{{URL::to('/')}}/closenumber/getpost",
                    type: 'GET',
                    data: {type:id},
                    success: function(data) {
                        if(data.status=="success"){

                                $("#post_id").html(data.msg);

                                $("#post_id").prop( "disabled", false );

                                console.log(data.msg);

                        }else{
                        }
                        }
                    });
                }else{

                    $("#post_id").prop( "disabled", false );
                }
                

            });

		   var $checkoutForm = $('#checkout-form').validate({
		   // Rules for form validation
		    rules : {
                 sheet_id : {
			      required : true
			     },
			     post_id : {
			      required : true,
			      number: true
			     },
			     c_number : {
			      required : true
			     }
		    },
		  
		    // Messages for form validation
		    messages : {
                sheet_id : {
			      required : "សូមជ្រើសរើសពេល"
			     },
			     post_id : {
			      required : "សូមជ្រើសរើសប៉ុស"
			     },
			     c_number : {
			      required : "សូមបញ្ចូលលេខបិទ",
                  number : "សូមបញ្ចូលលេខសុទ្ធ"
			     }
			},
		  
		    // Do not change code below
		    errorPlacement : function(error, element) {
		     error.insertAfter(element.parent());
		    }

		   });

		   
		 
		   // START AND FINISH DATE
		   $('#st_date_diposit').datepicker({
			    dateFormat : 'yy-mm-dd',
			    prevText : '<i class="fa fa-chevron-left"></i>',
			    nextText : '<i class="fa fa-chevron-right"></i>',
			    // onSelect : function(selectedDate) {
			    //  $('#dob').datepicker('option', 'minDate', selectedDate);
			    // }
			});

		   // $(document).off('keypress', '#st_price').on('keypress', '#st_price', function() {
		   		
		   // 		var val = $(this).val();
		   // 		var newVal = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		   // 		$(this).val(newVal);

		   // });
		   
		   $("#st_price").keyup(function(e){
		   		$(this).val(currencyFormat($(this).val()));
		   });
		   
		});

		function btnCancel(){
	   		var r = confirm("តើលោកអ្នកចង់ត្រលប់ក្រោយឬទេ?");
		    if (r == true) {
		        document.location.href='{{URL::to('/')}}/closenumber';
		    }
	   }
	</script>
@stop