@extends('master')

@section('title')
<title>Welcome to lottery</title>
@stop

@section('cssStyle')
     <style type="text/css">
        .needBoder{
            /*border-top: 1px #ccc solid;
            border-left: 1px #ccc solid;*/
            border: 1px #ccc solid;
        }
        /*.needBoderFull{
            border-top: 1px #ccc solid;
            border-left: 1px #ccc solid;
            border-right: 1px #ccc solid;
        }*/
        .tableSrey{
            text-align: center;
            width: 100%;
        }
        .tableSrey td{
            border: 1px #ccc solid;
            padding: 2px;
            font-size: 14px;
        }
    </style>
@stop


@section('content')

<!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>{{trans('label.dashboard')}}</li>
        </ol>
        <!-- end breadcrumb -->


    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>


                            <!-- widget content -->
                            <article class="widget-body " style="width: 1000px;">
                                    <fieldset>
                                        
                                        <div class="row" >
                                            @if(Session::get('roleLot') != 100 && Session::get('roleLot') != 0)
                                            <article class="col-sm-12 col-md-12 col-lg-12 " style="padding-bottom: 20px;">
                                                <div id="btn_show">
                                                    <a href="{{URL::to('/')}}/reportautoeveryday/step1?date={{$dateVar}}" target="_blank" class="btn btn-primary">Run Report Step 1</a>
                                                    <a href="{{URL::to('/')}}/reportautoeveryday/step2?date={{$dateVar}}" target="_blank" class="btn btn-primary">Run Report Step 2</a>
                                                    <a href="{{URL::to('/')}}/reportautoeveryday/step3?date={{$dateVar}}" target="_blank" class="btn btn-primary">Run Report Step 3</a>
                                                    <a href="{{URL::to('/')}}/reportautoeveryday/step4?date={{$dateVar}}" target="_blank" class="btn btn-primary">Run Report Step 4</a>
                                                </div>
                                                
                                            </article>
                                            @endif
                                            <article class="col-sm-12 col-md-3 col-lg-3 " style="padding-bottom: 20px;">
                                                <div id="caledar"></div>
                                            </article>
                                            <article class="col-sm-3 col-md-3 col-lg-3 ">
            <?php 
                // dd($resultsMorning);
                $resultsMorning = [];
            ?>
            @if(isset($resultsMorning) and count($resultsMorning) > 0 )
                                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                                    <tr>
                                                        <td colspan="2">
                                                            <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនព្រឹក</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <b>Date : {{$date}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                             <b style="color: red; font-size: 14px;">1:30 PM</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>A</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[0]->re_num_result}}</b><br>
                                                             <b>{{$resultsMorning[1]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                    @if(isset($saryAfternoon))
                                        @php 
                                            $data = $saryAfternoon->info;
                                            $resultLot = explode(",", $data);
                                        @endphp
                                                    <tr>
                                                        <td>
                                                             <b>Giải sáu</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[1]}}</b><br>
                                                             <b>{{$resultLot[2]}}</b><br>
                                                             <b>{{$resultLot[3]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>Giải năm</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             
                                                             <b>{{$resultLot[4]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>Giải tư</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[11]}}</b><br>
                                                             <b>{{$resultLot[10]}}</b><br>
                                                             <b>{{$resultLot[9]}}</b><br>
                                                             <b>{{$resultLot[8]}}</b><br>
                                                             <b>{{$resultLot[7]}}</b><br>
                                                             <b>{{$resultLot[6]}}</b><br>
                                                             <b>{{$resultLot[5]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>Giải nhất</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             
                                                             <b>{{$resultLot[12]}}</b>
                                                        </td>
                                                    </tr>
                                                    
                                    @endif
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>B</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[2]->re_num_result}}</b> - <b>{{$resultsMorning[3]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>C</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[4]->re_num_result}}</b> - <b>{{$resultsMorning[5]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>D</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[6]->re_num_result}}</b> - <b>{{$resultsMorning[7]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>F</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[8]->re_num_result}}</b> - <b>{{$resultsMorning[9]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>I</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[10]->re_num_result}}</b> - <b>{{$resultsMorning[11]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>N</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[12]->re_num_result}}</b> - <b>{{$resultsMorning[13]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>K</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsMorning[14]->re_num_result}}</b> - <b>{{$resultsMorning[15]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                </table>
            @endif
                                               
                                            </article>

                                            <article class="col-sm-3 col-md-3 col-lg-3 ">
            <?php 
                // dd($resultsAfternoon);
            ?>
            @if(isset($resultsAfternoon) and count($resultsAfternoon) > 0 )
                                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                                    <tr>
                                                        <td colspan="2">
                                                            <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនថ្ងៃ</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <b>Date : {{$date}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                             <b style="color: red; font-size: 14px;">4:30 PM</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>A</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[0]->re_num_result}}</b><br>
                                                             <b>{{$resultsAfternoon[1]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                    @if(isset($saryEvening))
                                        @php 
                                            $data = $saryEvening->info;
                                            $resultLot = explode(",", $data);
                                        @endphp
                                                    <tr>
                                                        <td>
                                                             <b>200N</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[0]}}</b><br>
                                                             <b>{{$resultLot[1]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>500N</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[2]}}</b><br>
                                                             <b>{{$resultLot[3]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>1,5Tr</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[4]}}</b><br>
                                                             <b>{{$resultLot[5]}}</b><br>
                                                             <b>{{$resultLot[6]}}</b><br>
                                                             <b>{{$resultLot[7]}}</b><br>
                                                             <b>{{$resultLot[8]}}</b><br>
                                                             <b>{{$resultLot[9]}}</b><br>
                                                             <b>{{$resultLot[10]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>5Tr</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[11]}}</b><br>
                                                             <b>{{$resultLot[12]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>8Tr</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[13]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <b>12Tr</b>
                                                        </td>
                                                        <td style="text-align: right;">
                                                             <b>{{$resultLot[14]}}</b>
                                                        </td>
                                                    </tr>
                                    @endif
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>B</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[2]->re_num_result}}</b> - <b>{{$resultsAfternoon[3]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>C</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[4]->re_num_result}}</b> - <b>{{$resultsAfternoon[5]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>D</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[6]->re_num_result}}</b> - <b>{{$resultsAfternoon[7]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>F</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[8]->re_num_result}}</b> - <b>{{$resultsAfternoon[9]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>I</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[10]->re_num_result}}</b> - <b>{{$resultsAfternoon[11]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>N</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[12]->re_num_result}}</b> - <b>{{$resultsAfternoon[13]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>K</b>
                                                        </td>
                                                        <td style="text-align: right; font-size: 20px !important;">
                                                             <b>{{$resultsAfternoon[14]->re_num_result}}</b> - <b>{{$resultsAfternoon[15]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                </table>
            @endif
                                               
                                            </article>
                                            <article class="col-sm-3 col-md-3 col-lg-3">

            @if(isset($resultsEvening) and count($resultsEvening) > 0 )
                                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                                    <tr>
                                                        <td colspan="3">
                                                            <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនយប់</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <b>Date : {{$date}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                             <b style="color: red; font-size: 14px;">6:30 PM</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>A</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[0]->re_num_result}}</b><br>
                                                             <b>{{$resultsEvening[1]->re_num_result}}</b><br>
                                                             <b>{{$resultsEvening[2]->re_num_result}}</b><br>
                                                             <b>{{$resultsEvening[3]->re_num_result}}</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[4]->re_num_result}}</b><br>
                                                             <b>{{$resultsEvening[5]->re_num_result}}</b><br>
                                                             <b>{{$resultsEvening[6]->re_num_result}}</b>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>B</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[7]->re_num_result}}</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[8]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>C</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[9]->re_num_result}}</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[10]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px !important;">
                                                             <b>D</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[11]->re_num_result}}</b>
                                                        </td>
                                                        <td style="text-align: center; font-size: 20px !important;">
                                                             <b>{{$resultsEvening[12]->re_num_result}}</b>
                                                        </td>
                                                    </tr>
                            @if(isset($saryNight))
                                        @php 
                                            $data = $saryNight->info;
                                            $resultLotEve = explode(",", $data);
                                        @endphp
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             19
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[0]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             18
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[1]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             17
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[2]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             16
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[3]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             15
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[4]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             14
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[5]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             13
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[6]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             12
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[7]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             11
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[8]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             10
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[9]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             9
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[10]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             8
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[11]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             7
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[12]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             6
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[13]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             5
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[14]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             4
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[15]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             3
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[16]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             2
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[17]}}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                             1
                                                        </td>
                                                        <td style="text-align: center;">
                                                             <b>{{$resultLotEve[18]}}</b>
                                                        </td>
                                                    </tr>
                            @endif
                                                    
                                                    
                                                </table>
            @endif
                                            </article>
                                        </div>
                                    </fieldset>
                        </div>
                        <!-- end widget content -->
             
        
    </div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            pageSetUp();

            var responsiveHelper_datatable_tabletools = undefined;
    
            var breakpointDefinition = {
             tablet : 1024,
             phone : 480
            };
            $('#datatable_tabletools').dataTable({
    
                
                "autoWidth" : true,
                "preDrawCallback" : function() {
                 // Initialize the responsive datatables helper once.
                 if (!responsiveHelper_datatable_tabletools) {
                  responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
                 }
                },
                "rowCallback" : function(nRow) {
                 responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                 responsiveHelper_datatable_tabletools.respond();
                }
            });

            //WIDGET SUMMARY  REAL LOTTERY TRANSACTION

            $('#datatable-summary-real-lottery-tran').dataTable({
    
                
                "autoWidth" : true,
                "preDrawCallback" : function() {
                 // Initialize the responsive datatables helper once.
                 if (!responsiveHelper_datatable_tabletools) {
                  responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable-summary-real-lottery-tran'), breakpointDefinition);
                 }
                },
                "rowCallback" : function(nRow) {
                 responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                 responsiveHelper_datatable_tabletools.respond();
                }
            });


            $('#datatable_tabletools1').dataTable({  
                
                "autoWidth" : true,
                "preDrawCallback" : function() {
                 // Initialize the responsive datatables helper once.
                 if (!responsiveHelper_datatable_tabletools) {
                  responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools1'), breakpointDefinition);
                 }
                },
                "rowCallback" : function(nRow) {
                 responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                 responsiveHelper_datatable_tabletools.respond();
                }
            });

            $('#datatable_tabletools11').dataTable({  
                
                "autoWidth" : true,
                "preDrawCallback" : function() {
                 // Initialize the responsive datatables helper once.
                 if (!responsiveHelper_datatable_tabletools) {
                  responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools1'), breakpointDefinition);
                 }
                },
                "rowCallback" : function(nRow) {
                 responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                 responsiveHelper_datatable_tabletools.respond();
                }
            });



            
        });
    </script>
    <script type="text/javascript">
    
        $( document ).ready(function() {
          $("#fullscreen a").click();
            // var myDate = new Date(2021,11,25) 
            $('#caledar').datepicker({
                dateFormat : 'yy-mm-dd',
                maxDate: new Date(),
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                   // alert(selectedDate);
                   //  var date2 = $('#p_date').datepicker('getDate');
                   //  date2.setDate(date2.getDate());
                   //  var month = date2.getMonth() + 1;
                   //  if(month < 10){
                   //      month = '0'+month;
                   //  }
                   //  var current_date = date2.getDate() + '-' + month + '-' + date2.getFullYear();
                   // alert(current_date);
                    window.open("{{url('/dasboard?date=')}}"+selectedDate,'_self');
                    
                }
            });
            $('#caledar').datepicker('setDate', '{{$date}}');
        });
    </script>
@stop