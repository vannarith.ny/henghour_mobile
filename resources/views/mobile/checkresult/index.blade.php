@extends('mobile.master')

@section('mobile_title')
    <title> ផ្ទៀងលេខត្រូវ </title>
@stop

@section('mobile_cssStyle')
    <link href="{{ asset('/') }}/mobile_css/main_android1.min.css" rel="stylesheet" />
	<link href="{{ asset('/') }}/mobile_css/mobile.min.css?v=2" rel="stylesheet" />
	<link href="{{ asset('/') }}/mobile_css/jquery-ui.min.css" rel="stylesheet" />
	<meta name="viewport" content="user-scalable=no" />
	<style>
		* { -webkit-touch-callout: none; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; } #tr_post td.posts_font2 { font-size: 62px !important; font-weight: bold; } #menu_tr_id table td { font-size: 40px; font-family: fontHanuman; font-weight: bold; background-image: -webkit-linear-gradient(top, #f9f7f7, #AAA); } #menu_tr_id table td a { text-decoration: none; display: block; color: #000; padding: 25px 50px; } td.deletetd { cursor: pointer; width: 48px; min-width: 100px; background: url(/Content/assets/img/delete48.png) center top no-repeat; } td.btntds, td.btntd, td.posts_font2, td.c_post { cursor: pointer; } .div_post_vote { border-bottom: 1px solid #ccc; }


		#main_keyboard td{touch-action: manipulation !important; }
		
		#main {
		    height: auto !important; 
		    min-height: 100%;
		    background: #f4f3e8 !important;
		}
		#main_div{
			border: none;
		}
		.backgroundMenu{
			background-color: #f4f3e8;
		}
		.bodermenu{
			border-bottom: 2px #ccc solid;
			padding-bottom: 20px;
		}
		/*.backgroundMenu td{
			margin-right: 5px;
		}*/
		.btntds{
			margin-right: 10px;
		}
		.paddingleftright{
			padding-right: 15px;
			padding-left: 15px;
		}
		input.inputb{
			width: 100%;
		}
		.total{
			border:none;
		}

		.display_chink {
		    position: absolute;
		    right: 0px;
		    top: 50px;
		    /* padding: 2px; */
		    font-size: 25px !important;
		    border: 1px #990012 solid;
		    color: #990012;
		    width: 85px;
		    height: 50px;
		    text-align: center;
		     padding-top: 4px; 
		    border-radius: 50% 50% 50% 50%;
		    -moz-border-radius: 50% 50% 50% 50%;
		    -webkit-border-radius: 50% 50% 50% 50%;
		}



		/*new css*/
		.mainShowLottery{
			position: relative;
			width: 100%;
			height: 865px !important;
			display: block;
			border-right: 1px #ccc solid;
			overflow: auto;
		}


		.colume_style{
		    
		    width: 280px;
		    height: auto !important;
		    float: left;
		    position: relative !important;
		}


		.rowview{
			width: 240px;
			padding-left: 10px;
			padding-right: 10px;
			float: left;
		}
		.rowview .eventDeletenumberAny{
			display: none;
		}

		.row_main.actived {
		    background-color: #990012 !important;
		    border-top: 1px solid #fff !important;
		}
		.row_main.actived.lugjit{
		    background-color:#a57c0b !important;
		    border-top:1px solid #fff !important;
		}
		.right_num {
		    position: absolute;
		    right: 5px;
		    top: 2px;
		    color: yellow;
		}

		.right_num_lug{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}

		.row_main.actived .lot_int {
		    color: #fff !important;
		}

		.row_main.actived ul li {
		    color: #fff !important;
		}

		.row_main.payed {
		    background-color: #505050 !important;
		    border-top: 1px solid #fff !important;
		}
		
		.row_main.payed .lot_int {
		    color: #fff !important;
		}

		.row_main.payed ul li {
		    color: #fff !important;
		}

		

		.pos_style{
		    border-bottom: 1px #000 solid;
		    width: 100%;
		    margin: 0 auto;
		    /*cursor: pointer;*/

		    font-family: "Arial", Gadget, sans-serif;
		    font-size: 40px;
		    font-weight: bold;
		    color: #000000;
		    text-align: center;
		    padding-bottom: 2px;
		    margin-bottom: 2px;
		    padding-top: 20px;
		    position: relative;

		}

		.optionNumber{
			/*display: none;*/
			position: absolute;
			right: -20px;
			top: -5px;
		}
		.optionNumberMain{
			/*display: none;*/
			position: absolute;
			right: 15px;
			top: 10px;
			font-size: 60px !important;
		}

		.txt-color-red{
			color: red;
			font-size: 50px;
		}

		.optionNumberMain i.txt-color-red{
			font-size: 60px;
		}

		.row_main{
		    width: 100%;
		    padding: 5px 10px;
		    position: relative;
		    
		}
		.clear {
		    clear: both;
		}
		.lot_int{
		    float: left;
		    font-family: "Arial", Gadget, sans-serif;
		    font-size: 30px;
		    color: #000000;
		    text-align: left;
		}
		.number_lot, .amount_lot,.symbol_lot{
		    width: 30%;
		    position: relative;
		}
		.newdigitalot{
		    width: 35%;
		    position: relative;
		}



		.newdigitalot_price{
		    width: 20% !important;
		}
		.symbol_lot{
		    width: 30%;
		    font-size: 30px;
		    margin-top: -1px;
		    text-align: center;

		}

		.symbol_lot i{
		    font-size: 50px;
		    margin-top: -2px !important;
		}
		.amount_lot{
		    width: 35%;
		    /*background-color: red;*/
		}

		.symbol_lot{
		    padding-left: 5px;
		    padding-right: 5px;
		}

		.newsymbolStyle{
		    width: 38% !important;
		}
		.newstylebtn{
		    float: right;
		    width: 50% !important;
		}
		.newDesignAddLottery{
		    padding-bottom: 10px;
		}


	.clear_margin{
	    margin: 0px !important;
	    padding: 0px !important;
	    line-height: 10px;
	    padding-left: 5px !important;
	}
	.sym_absolube{
	    position: absolute;
	    left: 95px;
	    top: -27px;
	    font-size: 60px;
	}
	.sym_absolube_amount{
	    position: absolute;
	    left: 135px;
	    top: 0px;
	}

	.display_total_number {
	    position: absolute;
	    left: 40px;
	    top: 30px;
	    /* padding: 2px; */
	    font-size: 25px !important;
	    border: 1px #990012 solid;
	    color: #990012;
	    width: 36px;
	    height: 36px;
	    text-align: center;
	    /* padding-top: 2px; */
	    border-radius: 50% 50% 50% 50%;
	    -moz-border-radius: 50% 50% 50% 50%;
	    -webkit-border-radius: 50% 50% 50% 50%;
	}

	.showempty{
		position: absolute;
		width: 100%;
		text-align: center;
		height: 20px;
		top: calc(50% - 10px);
		font-size: 22px;
		color: red;
	}

	.totalprice{
		width: 100%;
		font-size: 35px;
		color: #000;
		text-align: left;
		border: 2px #000 solid;
		padding: 5px;
	}

	.totalprice.needPadding{
		margin-top: 5px;
		margin-bottom: 10px;
	}

	.labeltitle{
		width: 40%;
		float: left;
		/*padding-top: 5px;
		padding-bottom: 5px;*/
	}
	.valuedisplay{
		width: 60%;
		float: right;
		/*padding-top: 5px;
		padding-bottom: 5px;*/
		font-weight: bold;
		/*font-size: 40px;*/
		font-style: italic;
	}
	.clearboth{
		clear: both;
	}

	.rowtxt{
		padding-top: 5px;
		padding-bottom: 5px;
	}
	.rowtxt{
		display: none;
	}
	.formtxt{
		width: 31%;
		float: left;

		padding: 8px 5px 2px 5px;
		border-radius: 2px;
	    margin: 0 0 0 0;
	    border: 1px solid #ccc;
	    margin-right: 1%;
	    box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
	    font-size: 56px;
	    line-height: 50px;
	    height: 56px;
	    text-align: center;
	    letter-spacing: -3px;
	    background-color: #ccc;
	}

	.row_lottery_list_new{
	    width: 100%;
	    position: relative;
	    padding-bottom: 5px;
	    border-bottom: 1px #ccc solid;
	}
	.txt_left,.txt_center,.txt_right{
	    width: 30%;
	    float: left;
	    text-align: left;
	}

	.row_lottery_list_new ul{
	    list-style: none;
	    margin: 0px;
	    padding: 0px;
	}
	.row_lottery_list_new ul li{
	    padding: 2px 0px;
	}

	.hidetwothree_digit{
	    display: none !important;
	}


	.totalfooter{
		width: 100%;
		padding-top: 50px;
		padding-bottom: 20px;
		font-size: 28px;
		text-align: left;
	}
	.totalfooter .rowLeft{
		float: left;
		padding-right: 40px;
	}
	.winCSS{
		color: red;
	}

	.rowview .pos_style{
		font-size: 25px;
	}
	.rowview .lot_int{
		font-size: 25px;
	}
	.rowview .row_main ul li, .rowview .totalprice{
		font-size: 25px;
	}
	.rowview .labeltitle{
		width: 25%;
	}
	.rowview .valuedisplay{
		width: 70%;
	}

	input.inputpr{
		width: 210px;
	}

	#ShowResult table tr td{
		border: 1px #ccc solid;
	}

	</style>
@stop


@section('mobile_content')

	@include('mobile.checkresult.header')
	<div class="container-fluid" >
		<input id="loginUserId" type="hidden" value="{{Session::get('iduserlotMobileSec')}}" />
		


		<table width="100%" cellpadding="0" cellspacing="0" id="basic_table" style="min-height: 700px;">
			<tbody>
				<tr valign="top" id="id_add">
					<td align="center" colspan="2">
						<form name="frm">
							<table align="center" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr id="menu_tr_id" style="display:none">
										<td colspan="2">
											@include('mobile.checkresult.main_menu')
										</td>
									</tr>


									<tr id="tr_post" style="display:none">
										<td colspan="4" valign="top" align="center" id="posts">
										</td>
									</tr>
									<tr id="id_add_dis" >
										<td align="left" class="backgroundMenu" style="border-right: 2px #ccc solid;">
											<!-- block list data lottery -->
											
											<div class="mainShowLottery" >
												<div id="main_div" class="colume_style"  style="">
													


												</div>
											</div>
											<!-- end block list data lottery -->
										</td>
										<td align="right" width="53%">
										</td>
									</tr>
									<tr id="tr_keyboard" valign="bottom">
										<td valign="bottom" align="right" colspan="2" style="vertical-align:bottom">
											<!-- block keyboard -->

										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</td>
				</tr>
				<tr id="id_show" style="display:none" valign="top" >
					<td bgcolor="#EBEBEB" style="width: 60%;">
						<div id="main_table" onclick="if(default_view_size){ default_view_size=!default_view_size; this.style.fontSize=&#39;84px&#39;; this.style.lineHeight=&#39;80px&#39;; } else{ default_view_size=!default_view_size; this.style.fontSize=&#39;35px&#39;; this.style.lineHeight=&#39;35px&#39;; }">waiting...</div>
					</td>
					<td id="ShowResult" style="width: 35%; background-color: #FFF; padding-left: 20px; ">
						waiting...
					</td>
				</tr>
			</tbody>
		</table>
		<div class="dragclass" id="div1"></div>
		

		
				
	</div>


    	

       
      




@endsection


@section('mobile_javascript')

	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>

	<script src="{{ asset('/') }}/mobile_js/moment.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/functions.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup.min.js"></script>
	<!-- <script src="{{ asset('/') }}/mobile_js/vote_function.min.js?v=7"></script> -->
	<script src="{{ asset('/') }}/mobile_js/checkresult.js?v=7"></script>



    <script type="text/javascript">
    	$(document).ready(function () {
			
		});
        $(document).ready(function() {
            
   //      	var doubleTouchStartTimestamp = 0;
			// $(document).bind("touchstart", function(event){
			//     var now = +(new Date());
			//     console.log(now);
			//     if (doubleTouchStartTimestamp + 500 > now){
			//         $(this).off();
			//     };
			//     doubleTouchStartTimestamp = now;
			// });
        });
    </script>
@stop