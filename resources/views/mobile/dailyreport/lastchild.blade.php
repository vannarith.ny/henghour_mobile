<td align="left" id="">
    <table align="center" cellpadding="0" cellspacing="0" width="90%" border="1">
        <tbody>
            <tr align="right" style="">
                <td align='center' rowspan="2">ពេល</td>
                <td align='center' colspan="2">ចំនួនលុយរៀល</td>
                <td align='center' colspan="2">លេខត្រូវរៀល</td>

                <td align='center' colspan="2">ចំនួនលុយដុល្លា</td>
                <td align='center' colspan="2">លេខត្រូវដុល្លា</td>
                <td align="center" rowspan="2">លុយសរុប</td>
            </tr>
            <tr>
                <td align="center" width="">2លេខ</td>
                <td align="center" width="">3លេខ</td>
                <td align="center" width="">2លេខ</td>
                <td align="center" width="">3លេខ</td>

                <td align="center" width="30">2លេខ</td>
                <td align="center" width="30">3លេខ</td>
                <td align="center" width="30">2លេខ</td>
                <td align="center" width="30">3លេខ</td>
            </tr>
            <tr align="center" style="">
                <td align="left" width="" colspan="10" class="time">ល្ងាច</td>

                
            </tr>
            <tr>
                <td class="">(1)</td>
                <td class=" " align="right">94000</td>
                <td class=" " align="right">67900</td>
                <td class=" " align="right">3600</td>
                <td class=" " align="right">700</td>

                <td class=" " align="right"></td>
                <td class=" " align="right"></td>
                <td class=" " align="right"></td>
                <td class=" " align="right"></td>
                <td class=" " align="right">161900</td>
            </tr>
            <tr align="center" style="">
                <td align="left" width="" colspan="10" class="time">ល្ងាច</td>
            </tr>
            <tr>
                <td class="">(1)</td>

                <td class="" align="right">151000</td>
                <td class="" align="right">119200</td>
                <td class="" align="right">2000</td>
                <td class="" align="right"></td>

                <td class="" align="right"></td>
                <td class="" align="right"></td>
                <td class="" align="right"></td>
                <td class="" align="right"></td>

                <td class="" align="right">270200</td>
            </tr>
            <tr>
                <td colspan="10">
                    <table border="0" cellpadding="0" cellspacing="5" width="100%" class="subtotal">
                        <tbody>
                            <tr>
                                <td class="titleCurrentcy" colspan="4" align="left">រៀល</td>
                                <td class="titleCurrentcy" colspan="4" align="left">ដុល្លា</td>
                            </tr>
                            <tr align="center" style="line-height:20px;">

                                <td align="left" width="70">2លេខ</td>
                                <td align="right" width="135px" style="font-weight:bold;">245000</td>
                                <td align="center" width="5px">=</td>
                                <td align="left" class="win" width="150px"
                                    ><strong>171500</strong>
                                </td>

                                <td align="left" width="70">2លេខ</td>
                                <td align="right" width="135px" style="font-weight:bold;">245000</td>
                                <td align="center" width="5px">=</td>
                                <td align="left" class="win" width="150px"
                                    ><strong>171500</strong>
                                </td>

                            </tr>
                            <tr align="center" style="line-height:20px">
                                <td align="left" width="70">3លេខ</td>
                                <td align="right" width="135" style="font-weight:bold;">187100</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="win"><strong>127228</strong>
                                </td>
                                
                                <td align="left" width="70">3លេខ</td>
                                <td align="right" width="135" style="font-weight:bold;">187100</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="win"><strong>127228</strong>
                                </td>
                            </tr>
                            <tr style="line-height:20px;">
                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">5600x70</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-392000</strong>
                                </td>

                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">5600x70</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-392000</strong>
                                </td>
                            </tr>
                            <tr style="line-height:20px;">
                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">700x600</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-420000</strong>
                                </td>

                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">700x600</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-420000</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom:3px solid; line-height:5px; height:5px;" colspan="8"></td>
                            </tr>
                            <tr>
                                <td>សរុប១ថ្ងៃ</td>
                                <td align="right" colspan="3" class="totalpricereal">
                                    <h3 class="lose"> ខាត: (513300) </h3>
                                </td>
                                <td><br></td>
                                <td align="left" colspan='4' class="totalpricedollar">
                                    <h3 class="win"> ស៊ី: 0$ </h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom:1px solid; line-height:5px; height:5px;" colspan="8"></td>
                            </tr>
                            <tr align="center">
                                <td style="text-align:left;line-height:28px">លុយចាស់ :</td>
                                <td align="right" colspan="3" class="calculate">
                                    <span class="win" style="font-weight:bold;">1241300</span> 
                                </td>
                                <td  align="right" style="line-height:28px">លុយចាស់ :</td>
                                <td align="right" colspan="3" class="calculate">
                                    <span class="win" style="font-weight:bold;">269.9$</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="line-height:5px; height:5px;">
                                    <hr>
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td colspan="2" width="320"></td>
                                <td width="180" colspan="2" align="left">
                                    <h3 class="win"> ស៊ី : 728000 </h3>
                                </td>
                                <td colspan="2"></td>
                                <td align="left" width="150">
                                    <h4 class="win"> ស៊ី : 269.9$ </h4>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</td>