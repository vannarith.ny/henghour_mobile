@if(count($data) > 0)
<td align="left" id="">
    <style>
        .mainTotal .lose, .mainTotal .win{
            line-height: 45px;
        }
    </style>
    <table align="center" cellpadding="0" cellspacing="0" width="90%" border="1">
        <tbody>
            <tr align="right" style="">
                <td align='center' rowspan="2">កូន</td>
                <td align='center' rowspan="2">ពេល</td>
                <td align='center' colspan="2">លុយរៀល</td>
                <td align='center' colspan="3">ត្រូវរៀល</td>

                <td align='center' colspan="2" class="dDollar">លុយដុល្លា</td>
                <td align='center' colspan="3" class="dDollar">ត្រូវដុល្លា</td>
                <td align="center" colspan="2">លុយសរុប</td>
            </tr>
            <tr>
                <td align="center" width="">2D</td>
                <td align="center" width="">3D</td>
                <td align="center" width="">1D</td>
                <td align="center" width="">2D</td>
                <td align="center" width="">3D</td>

                <td align="center" width="30" class="dDollar">2D</td>
                <td align="center" width="30" class="dDollar">3D</td>
                <td align="center" width="30" class="dDollar">1D</td>
                <td align="center" width="30" class="dDollar">2D</td>
                <td align="center" width="30" class="dDollar">3D</td>

                <td align="center" >រៀល</td>
                <td align="center" >ដុល្លា</td>
            </tr>
            <?php

                $lugPay = 5;
                $oneDPay = 8;

                $checkStaff = '';
                $checkTime = '';
                $count = 0;

                $total2DR = 0;
                $total3DR = 0;

                $total2DD = 0;
                $total3DD = 0;

                $Wtotal1DR = 0;
                $Wtotal2DR = 0;
                $Wtotal3DR = 0;

                $Wtotal1DD = 0;
                $Wtotal2DD = 0;
                $Wtotal3DD = 0;

                $WtotalLugR = 0;
                $WtotalLugD = 0;

                $countPaper = 0;

                $checkDollarHas = 0;

            ?>
            @foreach($data as $row)
                @php 
                    $count = $count + 1;
                    $countPaper = $countPaper + 1;
                @endphp
                <!-- check time and staff name -->
                @if($checkTime != $row->sheet_id || $checkStaff != $row->s_id)
                    <tr align="center" style="">
                        <td align="center" width="">
                            @if($checkStaff != $row->s_id)
                                {{$row->s_name}}
                                @php 
                                    $checkStaff = $row->s_id;
                                @endphp
                            @endif
                        </td>
                        <td align="left" width="" colspan="9" class="time">
                            @if($row->sheet_id == 5)
                                ល្ងាច
                            @elseif($row->sheet_id == 6)
                                យប់
                            @elseif($row->sheet_id == 36)
                                ព្រលឹម
                            @elseif($row->sheet_id == 17)
                                ព្រឹក
                            @elseif($row->sheet_id == 12)
                                ថ្ងៃ
                            @elseif($row->sheet_id == 13)
                                រសៀល
                            @elseif($row->sheet_id == 24)
                                ល្ងាច
                            @elseif($row->sheet_id == 14)
                                យប់
                            @else
                                យប់
                            @endif
                        </td>

                        
                    </tr>
                    @php 
                        $checkTime = $row->sheet_id;
                        $count = 1;
                    @endphp
                @endif

                @php 
                    $totalReal =  $row->price2digit_r + $row->price3digit_r;
                    $totalDollar =  $row->price2digit_d + $row->price3digit_d;

                    $total2DR = $total2DR + $row->price2digit_r;
                    $total3DR = $total3DR + $row->price3digit_r;

                    $total2DD = $total2DD + $row->price2digit_d;
                    $total3DD = $total3DD + $row->price3digit_d;

                    $Wtotal1DR = $Wtotal1DR + $row->win1digit_r;
                    $Wtotal2DR = $Wtotal2DR + $row->win2digit_r;
                    $Wtotal3DR = $Wtotal3DR + $row->win3digit_r;

                    $Wtotal1DD = $Wtotal1DD + $row->win1digit_d;
                    $Wtotal2DD = $Wtotal2DD + $row->win2digit_d;
                    $Wtotal3DD = $Wtotal3DD + $row->win3digit_d;

                    $WtotalLugR = $WtotalLugR + $row->lug_r;
                    $WtotalLugD = $WtotalLugD + $row->lug_s;

                    if($row->price2digit_d > 0 || $row->price3digit_d >0 || $row->win2digit_d >0 || $row->win3digit_d >0){
                        $checkDollarHas = $checkDollarHas + 1;
                    }
                @endphp

                <tr>
                    <td></td>
                    <td class="">{{$row->page_value}}</td>
                    <td class=" " align="right">{{number_format($row->price2digit_r)}}</td>
                    <td class=" " align="right">{{number_format($row->price3digit_r)}}</td>
                    <td class=" " align="right">{{number_format($row->win1digit_r)}}</td>
                    <td class=" " align="right">{{number_format($row->win2digit_r)}}</td>
                    <td class=" " align="right">{{number_format($row->win3digit_r)}}</td>

                    <td align="right" class="dDollar">{{$row->price2digit_d}}</td>
                    <td align="right" class="dDollar">{{$row->price3digit_d}}</td>
                    <td align="right" class="dDollar">{{$row->win1digit_r}}</td>
                    <td align="right" class="dDollar">{{$row->win2digit_d}}</td>
                    <td align="right" class="dDollar">{{$row->win3digit_d}}</td>

                    <td class=" " align="right">{{number_format($totalReal)}}</td>
                    <td class=" " align="right">{{$totalDollar}}</td>
                </tr>

            @endforeach
                
            
            <!-- <tr align="center" style="">
                <td></td>
                <td align="left" width="" colspan="11" class="time">ល្ងាច</td>
            </tr>
            <tr>
                <td></td>
                <td class="">(1)</td>

                <td class="" align="right">151000</td>
                <td class="" align="right">119200</td>
                <td class="" align="right">2000</td>
                <td class="" align="right"></td>

                <td class="" align="right"></td>
                <td class="" align="right"></td>
                <td class="" align="right"></td>
                <td class="" align="right"></td>

                <td class="" align="right">270200</td>
            </tr>
            <tr align="center" style="">
                <td align="center" width="">ThyDa</td>
                <td align="left" width="" colspan="10" class="time">ល្ងាច</td>

                
            </tr>
            <tr>
                <td></td>
                <td class="">(1)</td>
                <td class=" " align="right">94000</td>
                <td class=" " align="right">67900</td>
                <td class=" " align="right">3600</td>
                <td class=" " align="right">700</td>

                <td class=" " align="right"></td>
                <td class=" " align="right"></td>
                <td class=" " align="right"></td>
                <td class=" " align="right"></td>
                <td class=" " align="right">161900</td>
            </tr>
            <tr align="center" style="">
                <td></td>
                <td align="left" width="" colspan="11" class="time">ល្ងាច</td>
            </tr>
            <tr>
                <td></td>
                <td class="">(1)</td>

                <td class="" align="right">151000</td>
                <td class="" align="right">119200</td>
                <td class="" align="right">2000</td>
                <td class="" align="right"></td>

                <td class="" align="right"></td>
                <td class="" align="right"></td>
                <td class="" align="right"></td>
                <td class="" align="right"></td>

                <td class="" align="right">270200</td>
            </tr> -->
            <tr>
                <td colspan="14">
                    <table border="0" cellpadding="0" cellspacing="5" width="100%" class="subtotal">
                        <tbody>
                            <tr>
                                <td>សរុបក្រដាស់ : </td>
                                <td style="text-align:left !important;" colspan="7" align="left">{{$countPaper}}</td>
                            </tr>
                            <tr>
                                <td class="titleCurrentcy" colspan="4" align="left">រៀល</td>
                                <td class="titleCurrentcy" colspan="4" align="left">ដុល្លា</td>
                            </tr>
                            <tr align="center" style="line-height:20px;">

                                <td align="left" width="70">2លេខ</td>
                                <td align="right" width="135px" style="font-weight:bold;">{{$total2DR}}</td>
                                <td align="center" width="5px">=</td>
                                <td align="left" class="win" width="150px"
                                    ><strong>
                                        <?php 
                                            $afterCut2 = number_format( ($total2DR * $userInfo->s_two_digit_charge)/100 );
                                            $realafterCut2 = ($total2DR * $userInfo->s_two_digit_charge)/100;
                                        ?>
                                        {{$afterCut2}}
                                    </strong>
                                </td>

                                <td align="left" width="70">2លេខ</td>
                                <td align="right" width="135px" style="font-weight:bold;">{{$total2DD}}</td>
                                <td align="center" width="5px">=</td>
                                <td align="left" class="win" width="150px"
                                    ><strong>
                                        <?php 
                                            $afterCut2D = round( (($total2DD * $userInfo->s_two_digit_charge)/100 ), 2);
                                            $realafterCut2D = (($total2DD * $userInfo->s_two_digit_charge)/100 );
                                        ?> 
                                        {{$afterCut2D}}
                                    </strong>
                                </td>

                            </tr>
                            <tr align="center" style="line-height:20px">
                                <td align="left" width="70">3លេខ</td>
                                <td align="right" width="135" style="font-weight:bold;">{{$total3DR}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="win">
                                <strong>
                                    <?php 
                                        $afterCut3 = number_format( ($total3DR * $userInfo->s_three_digit_charge)/100 );
                                        $realafterCut3 = ($total3DR * $userInfo->s_three_digit_charge)/100;
                                    ?>
                                    {{$afterCut3}}
                                    
                                </strong>
                                </td>
                                
                                <td align="left" width="70">3លេខ</td>
                                <td align="right" width="135" style="font-weight:bold;">{{$total3DD}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="win">
                                <strong>
                                    <?php 
                                        $afterCut3D = round( (($total3DD * $userInfo->s_three_digit_charge)/100 ), 2);
                                        $realafterCut3D = (($total3DD * $userInfo->s_three_digit_charge)/100 );
                                    ?>    
                                    {{$afterCut3D}}
                                </strong>
                                </td>
                            </tr>

                            <?php 
                                $sum2And3 = $total2DR + $total3DR;
                                $sum2and3Cut = ($total2DR * $water2D)/100 + ($total3DR * $water3D)/100;

                                $sum2And3D = $total2DD + $total3DD;
                                $sum2and3CutD = $afterCut2D + $afterCut3D;
                            ?>

                            <tr align="center" style="line-height:20px; background: #ddd;">
                                <td align="left" width="70">សរុប</td>
                                <td align="right" width="135" style="font-weight:bold;">{{number_format($sum2And3)}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="win">
                                <strong>
                                    
                                    {{number_format($sum2and3Cut)}}
                                    
                                </strong>
                                </td>
                                
                                <td align="left" width="70">3លេខ</td>
                                <td align="right" width="135" style="font-weight:bold;">{{$sum2And3D}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="win">
                                <strong>
                                    {{$sum2and3CutD}}
                                </strong>
                                </td>
                            </tr>

                            <tr style="line-height:20px;">
                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$Wtotal1DR}}x{{$oneDPay}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{number_format($Wtotal1DR*$oneDPay)}}</strong>
                                </td>

                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$Wtotal1DD}}x{{$oneDPay}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{$Wtotal1DD*$oneDPay}}</strong>
                                </td>
                            </tr>
                            <tr style="line-height:20px;">
                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$Wtotal2DR}}x{{$userInfo->s_two_digit_paid}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{number_format($Wtotal2DR*$userInfo->s_two_digit_paid)}}</strong>
                                </td>

                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$Wtotal2DD}}x{{$userInfo->s_two_digit_paid}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{$Wtotal2DD*$userInfo->s_two_digit_paid}}</strong>
                                </td>
                            </tr>
                            <tr style="line-height:20px;">
                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$Wtotal3DR}}x{{$userInfo->s_three_digit_paid}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{number_format($Wtotal3DR*$userInfo->s_three_digit_paid)}}</strong>
                                </td>

                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$Wtotal3DD}}x{{$userInfo->s_three_digit_paid}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{$Wtotal3DD*$userInfo->s_three_digit_paid}}</strong>
                                </td>
                            </tr>


                            <!-- <tr style="line-height:20px;">
                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$WtotalLugR}}x{{$lugPay}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{number_format($WtotalLugR*$lugPay)}}</strong>
                                </td>

                                <td width="70"></td>
                                <td align="right" width="135" style="font-weight:bold;">{{$WtotalLugD}}x{{$lugPay}}</td>
                                <td align="center" width="5">=</td>
                                <td align="left" width="150" class="lose"><strong>-{{$WtotalLugD*$lugPay}}</strong>
                                </td>
                            </tr> -->

                            <tr>
                                <td style="border-bottom:3px solid; line-height:5px; height:5px;" colspan="8"></td>
                            </tr>
                            <tr>
                                <td align="right">សរុប១ថ្ងៃ</td>
                                <td align="right" colspan="3" class="totalpricereal">
                                    <?php 
                                        $totalPrice = (float)$realafterCut2 + (float)$realafterCut3;
                                        $totalPriceD = (float)$realafterCut2D + (float)$realafterCut3D;

                                        $priceRW = ((float)$Wtotal1DR*$oneDPay) + ((float)$Wtotal2DR*$userInfo->s_two_digit_paid) + ((float)$Wtotal3DR*$userInfo->s_three_digit_paid);
                                        $priceDW = ((float)$Wtotal2DD*$userInfo->s_two_digit_paid) + ((float)$Wtotal3DD*$userInfo->s_three_digit_paid);

                                        $priceLugR = ((float)$WtotalLugR*$lugPay);
                                        $priceLugD = ((float)$WtotalLugD*$lugPay);
                                        
                                        $totalWinR = $totalPrice - $priceRW - $priceLugR;
                                        if($totalWinR < 0){
                                            $titleR = 'ខាត';
                                            $classR = 'lose';
                                        }else{
                                            $titleR = 'ស៊ី';
                                            $classR = 'win';
                                        }

                                        $totalWinD = $totalPriceD - $priceDW - $priceLugD;
                                        if($totalWinD < 0){
                                            $titleD = 'ខាត';
                                            $classD = 'lose';
                                        }else{
                                            $titleD = 'ស៊ី';
                                            $classD = 'win';
                                        }

                                        // check last price in kh
                                        $checklastPrice = substr($totalWinR, -2);
                                        // if($checklastPrice >= 50){
                                        //     $count = 100 - $checklastPrice;
                                        //     $totalWinR = $totalWinR + $count;
                                        // }

                                        
                                    ?>
                                    <h3 class="{{$classR}}"> {{$titleR}}: {{number_format($totalWinR)}} </h3>
                                </td>
                                <td><br></td>
                                <td align="center" colspan='4' class="totalpricedollar">
                                    <h3 class="{{$classD}}"> {{$titleD}}: {{$totalWinD}}$ </h3>
                                </td>
                            </tr>
                            
                            <?php 

                            $realPriceR =  $totalWinR;
                            $realPriceD =  $totalWinD;



                                $check = DB::table('tbl_total_everyday')
                                    ->where('s_id',$userInfo->s_id)
                                    ->where('stc_type',$stc_type)
                                    ->where('date',$date)->first();
                                if($check){
                                    DB::table('tbl_total_everyday')
                                    ->where('s_id',$userInfo->s_id)
                                    ->where('stc_type',$stc_type)
                                    ->where('date',$date)
                                    ->update([
                                        'price_r' => $realPriceR,
                                        'price_d' => $realPriceD,
                                    ]);
                                }else{
                                    DB::table('tbl_total_everyday')->insert([
                                    's_id' => $userInfo->s_id,
                                    'price_r' => $realPriceR,
                                    'stc_type' => $stc_type,
                                    'price_d' => $realPriceD,
                                    'date' => $date
                                    ]);
                                }

                                

                            // $oldPrice = DB::table('tbl_total_everyday')
                            //             ->where('s_id',$userInfo->s_id)
                            //             ->where('date','<',$date)
                            //             ->orderBy('date','DESC')
                            //             ->first();

                            // if($oldPrice){
                            //     $realPriceR =  $oldPrice->c_price_r + $totalWinR;
                            //     $realPriceD =  $oldPrice->c_price_d + $totalWinD;
                            // }
                            ?>

                            @if(isset($oldPrice))
                                <tr>
                                    <td style="border-bottom:1px solid; line-height:5px; height:5px;" colspan="8"></td>
                                </tr>
                                <tr align="center" class="mainTotal">
                                    <td style="text-align:right;line-height:28px">លុយចាស់ :</td>
                                    <td align="center" colspan="3" class="calculate">
                                        <?php 
                                            $oldTitle = 'win';
                                            if($oldPrice->c_price_r < 0){
                                                $oldTitle = 'lose';
                                            }
                                            $oldTitleD = 'win';
                                            if($oldPrice->c_price_d < 0){
                                                $oldTitleD = 'lose';
                                            }
                                        ?>
                                        <span class="{{$oldTitle}}" style="font-weight:bold;">{{number_format($oldPrice->c_price_r)}}</span> 
                                    </td>
                                    <td  align="right" style="line-height:28px">លុយចាស់ :</td>
                                    <td align="center" colspan="3" class="calculate">
                                        <span class="{{$oldTitleD}}" style="font-weight:bold;">{{$oldPrice->c_price_d}}$</span>
                                    </td>
                                </tr>

                                
                            @endif

                            <?php 
                                // $yesturday = date('Y-m-d',strtotime($date . "-1 days"));
                                // $transction = DB::table("tbl_staff_transction")
                                //             ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                //             ->where('tbl_staff_transction.st_date_search',$yesturday)
                                //             ->where('tbl_staff_transction.s_id',$userInfo->s_id)
                                //             ->get();

                                
                            ?>
                            @if(isset($transction))
                                <!-- <tr>
                                    <td style="border-bottom:1px solid; line-height:5px; height:5px;" colspan="8"></td>
                                </tr> -->
                                @foreach($transction as $key=> $data)
                                
                                    <tr align="center" class="mainTotal">
                                        <td style="text-align:right;line-height:28px">{{$data->pav_value}} :</td>
                                        <td align="center" colspan="3" class="calculate">
                                            <?php 
                                                $oldTitle = 'win';
                                                $oldTitleD = 'win';
                                                if($data->pav_id == 20 || $data->pav_id == 3){
                                                    $oldTitle = 'lose';
                                                    $oldTitleD = 'lose';
                                                }
                                            ?>
                                            <span class="{{$oldTitle}}" style="font-weight:bold;">{{$data->st_price_r}}</span> 
                                        </td>
                                        <td  align="right" style="line-height:28px">{{$data->pav_value}} :</td>
                                        <td align="center" colspan="3" class="calculate">
                                            <span class="{{$oldTitleD}}" style="font-weight:bold;">{{$data->st_price_d}}$</span>
                                        </td>
                                    </tr>

                                    <?php 
                                        if($data->pav_id == 20 || $data->pav_id == 3){
                                            $realPriceR = $realPriceR - $data->st_price_r;
                                            $realPriceD = $realPriceD - $data->st_price_d;
                                        }else if($data->pav_id == 4 || $data->pav_id == 22){
                                            $realPriceR = $realPriceR + $data->st_price_r;
                                            $realPriceD = $realPriceD + $data->st_price_d;
                                        }
                                    ?>
                                @endforeach
                            @endif
                                <!-- <tr>
                                    <td style="border-bottom:1px solid; line-height:5px; height:15px;" colspan="8"></td>
                                </tr> -->
                                
                                <!-- <tr valign="middle" class="mainTotal">
                                    <?php 
                                            
                                        $data2DRisplay = '';
                                        if($realPriceR < 0){
                                            $data2DRisplay = 'ខាត : '.number_format($realPriceR);
                                            $newTitleR = 'lose';
                                        }else{
                                            $data2DRisplay = 'ស៊ី : '.number_format($realPriceR);
                                            $newTitleR = 'win';
                                        }

                                        $data2DDisplay = '';
                                        if($realPriceD < 0){
                                            $data2DDisplay = 'ខាត : '.$realPriceD.' $';
                                            $newTitleD = 'lose';
                                        }else{
                                            $data2DDisplay = 'ស៊ី : '.$realPriceD.' $';
                                            $newTitleD = 'win';
                                        }


                                    ?>
                                    <td colspan="2" width="320"></td>
                                    <td width="180" colspan="2" align="left">
                                        <h3 class="{{$newTitleR}}">{!! $data2DRisplay !!}</h3>
                                    </td>
                                    <td colspan="2"></td>
                                    <td align="right" width="150">
                                        <h4 class="{{$newTitleD}}">{!! $data2DDisplay !!}</h4>
                                    </td>
                                </tr> -->

                            <?php 

                            $check = DB::table('tbl_total_everyday')
                                    ->where('s_id',$userInfo->s_id)
                                    ->where('stc_type',$stc_type)
                                    ->where('date',$date)->first();
                            if($check){
                                DB::table('tbl_total_everyday')
                                    ->where('s_id',$userInfo->s_id)
                                    ->where('stc_type',$stc_type)
                                    ->where('date',$date)
                                    ->update([
                                        'c_price_r' => $realPriceR,
                                        'c_price_d' => $realPriceD,
                                    ]);
                            }else{
                                DB::table('tbl_total_everyday')->insert([
                                    's_id' => $userInfo->s_id,
                                    'c_price_r' => $realPriceR,
                                    'c_price_d' => $realPriceD,
                                    'stc_type' => $stc_type,
                                    'date' => $date
                                    ]);
                            }

                            ?>

                            </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</td>
<script>
    @if($checkDollarHas == 0)
        $('.dDollar').hide();
    @endif
</script>
@else
<td align="left" id="">
    <div style="color:red;">លោកអ្នកមិនមានបញ្ចីសំរាប់ថ្ងៃដែលបានស្វែងរកទេ.</div>
</td>
@endif