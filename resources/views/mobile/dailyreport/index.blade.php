@extends('mobile.master')

@section('mobile_title')
    <title>ក្បាលបញ្ជី</title>
@stop

@section('mobile_cssStyle')
    <style type="text/css">
    	*:focus {
		    outline: none;
		}

        * { 
                -webkit-touch-callout: none; 
                -webkit-user-select: none; 
                -khtml-user-select: none; 
                -moz-user-select: none; 
                -ms-user-select: none; 
                user-select: none; 
                touch-action: manipulation;
            } 

    	input.inputbt {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 24px;
			    padding: 15px 20px;
			    border: 2px #ccc solid;
			    line-height: 24px;
			    border-radius:10px;
    	}

    	input.inputbt.active{
    		background-color: #ccc;

    	}
    	#main_table{
    		padding-top: 20px;
        }
        #main_table td{
            text-align:center;
            
        }
        .titleCurrentcy{
	        font-family: fantasy !important;
	        padding-left: 10px;
	    }

    </style>

@stop


@section('mobile_content')
	<link href="{{ asset('/') }}/mobile_css/dailyreport.css" rel="stylesheet" />
	<style>
        
    </style>
		<br />
		<br />
		<br/>
		<table width="100%" border="0">
			<tbody>
				<tr>
					<td style="text-align: center; font-size: 30px;">
						ក្បាលបញ្ជី @if($stc_type == 2) KH @else VN @endif
						<input type="hidden" name="stc_type" id="stc_type" value="{{$stc_type}}">
					</td>
				</tr>
				<tr>
					<td>
						<table align="center" cellpadding="3" cellspacing="0">
							<tbody>
								<tr height="45">
									<td></td>
									<td>
										<img src="{{ asset('/') }}/img/sale/prev.png" id="prev" style="vertical-align: bottom">
									</td>
									<td class="title" nowrap="nowrap">
										<input type="text" name="date" id="mainListDate" style="width: 150px;">
									</td>
									<td>
										<img src="{{ asset('/') }}/img/sale/next.png" id="next" style="vertical-align: bottom">
									</td>
									<td>
										<select name="lotteryTimeId" id="lotteryTimeId" class="selectbar">
                                            <option selected value="0">សរុប១ថ្ងៃ</option>
                                            @foreach ($sheets as $sheet)
												<option value="{{$sheet->pav_id}}">{{$sheet->pav_value}}</option>
											@endforeach
										</select>
									</td>
									<td></td>
									<td style="display:none"></td>
									<td class="titleb">កូន</td>
									<td></td>
									<td id="list_client">
										<select name="clientId" id="clientId" class="selectbar">
                                            <option value="{{Session::get('iduserlotMobileSec')}}">{{Session::get('usernameLotMobileSec')}}</option>
                                        @foreach ($userList as $user)
                                            <option value="{{$user->s_id}}">{{$user->s_name}}</option>
                                        @endforeach
										</select>
									</td>
									<td align="left" id="display_post"></td>
									<td id="print"></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr valign="top" id="main_table">
                    <td align="left" id="">
                        Waiting...
                    </td>
                </tr>
				
			</tbody>
		</table>




@endsection


@section('mobile_javascript')
	<script src="{{ asset('/') }}/mobile_js/dailyreport.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop