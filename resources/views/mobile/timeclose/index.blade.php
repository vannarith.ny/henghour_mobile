@extends('mobile.master')

@section('mobile_title')
    <title>ម៉ោងបិទ</title>
@stop

@section('mobile_cssStyle')
    <style type="text/css">
    	*:focus {
		    outline: none;
		}
    	input.inputbt {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 24px;
			    padding: 15px 20px;
			    border: 2px #ccc solid;
			    line-height: 24px;
			    border-radius:10px;
    	}

    	input.inputbt.active{
    		background-color: #ccc;

    	}
    	#main_table{
    		padding-top: 20px;
    	}
    	input[type=checkbox], input[type=radio]{
    		zoom:2;
    		margin-top: 5px;
    	}
    	.stylecontent td{
    		font-family: Verdana, Geneva, sans-serif;
    		font-size: 20px;
    	}


    	.timeTableStyle tr:nth-child(even) {background: #CCC}
		.timeTableStyle tr:nth-child(odd) {background: #FFF}

    	.titleTime{
    		background: #006699 !important;
    		color: #FFF;
    		border-color: #FFF;
    	}
    	.titleTime.kh{
    		background: #eb5a5a !important;
    		color: #FFF;
    		border-color: #FFF;
    	}
    	.titleTime th{
    		padding-top: 10px;
    		padding-bottom: 10px;
    		font-size: 20px !important;
    	}
    	.timeTableStyle tbody td{
    		padding-top: 10px;
    		padding-bottom:10px;
    		font-weight: bold;
    		font-size: 18px !important;
    	}
    	.currentDay{
    		background: #FFC133;
    	}

    	


    </style>
@stop


@section('mobile_content')
	<div class="container-fluid">
		

		<div class="row pt-4">
			<div class="col-md-12 " id="td_users">
				<table cellpadding="0" cellspacing="0" width="100%" class="timeTableStyle">
					<!-- <thead align="center">
						<tr align="center" class="titleTime">
							<th class="classtd1">ព្រឹក</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
					</thead> -->
					<tbody class="">
						


						<!-- VN evening -->
						<tr align="center" class="titleTime">
							<th class="classtd1">VN ល្ងាច</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',5)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1">{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif">{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach

						<!-- night -->
						<tr align="center" class="titleTime">
							<th class="classtd1">VN យប់</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',6)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1">{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif">{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach





						<!-- start KH -->
						<tr > <td colspan="8" height="20px;"></td></tr>
						<!-- afternoon ព្រលឹម-->
						<tr align="center" class="titleTime kh">
							<th class="classtd1">KH ព្រលឹម 8:45</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',36)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $mainKey => $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1" >{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif" >{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach

						<!--  ព្រឹក 10:35-->
						<tr align="center" class="titleTime kh">
							<th class="classtd1">KH ព្រឹក 10:35</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',17)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1">{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif">{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach

						<!--  2ថ្ងៃ 1:00-->
						<tr align="center" class="titleTime kh">
							<th class="classtd1">KH ថ្ងៃ 1:00</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',12)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1">{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif">{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach

						<!--  3រសៀល 3:45-->
						<tr align="center" class="titleTime kh">
							<th class="classtd1">KH រសៀល 3:45</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',13)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1">{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif">{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach

						<!--  4ល្ងាច 6:00-->
						<tr align="center" class="titleTime kh">
							<th class="classtd1">KH ល្ងាច 6:00</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',24)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1">{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif">{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach

						<!--  5យប់ 7:45-->
						<tr align="center" class="titleTime kh">
							<th class="classtd1">KH យប់ 7:45</th>
							@foreach ($daly as $day)
								<th class="classtd1">{{$day}}</th>
							@endforeach

						</tr>
						@php 
							$queryAfter = $times->where('sheet_id',14)->groupBy('p_id');
						@endphp
						@foreach ($queryAfter as $querypos)
						<tr>
								
							@foreach ($querypos as $key => $data)
								@if($key == 0)
								<td class="classtd1">{{$data->pos_name}}</td>
								@endif
								<td class="classtd1 @if($key == $currentDay ) currentDay @endif">{{ date("H:i", strtotime($data->t_time)) }}</td>
							@endforeach
						</tr>
						@endforeach
						<tr > <td colspan="8" height="20px;"></td></tr>
					</tbody>
				</table>

				
			</div>
		</div>

		
				
	</div>


    	

       
      




@endsection


@section('mobile_javascript')
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/post.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop