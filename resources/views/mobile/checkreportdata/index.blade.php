@extends('mobile.master')

@section('mobile_title')
    <title>ពិនិត្យបញ្ជី</title>
@stop

@section('mobile_cssStyle')
    <style type="text/css">
    	*:focus {
		    outline: none;
		}

        * { 
                -webkit-touch-callout: none; 
                -webkit-user-select: none; 
                -khtml-user-select: none; 
                -moz-user-select: none; 
                -ms-user-select: none; 
                user-select: none; 
                touch-action: manipulation;
            } 

    	input.inputbt {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 24px;
			    padding: 15px 20px;
			    border: 2px #ccc solid;
			    line-height: 24px;
			    border-radius:10px;
    	}

    	input.inputbt.active{
    		background-color: #ccc;

    	}
    	#main_table{
    		padding-top: 20px;
        }
        #main_table td{
            text-align:center;
            
        }

        .container{

        }

        .formFiltter{
            display:inline-block;
        }
        .formFiltter label{
            font-family:"Times New Roman", Times, serif;
            font-weight:bold;
            padding-top:10px;
            padding-right:10px;
            padding-left:20px;
        }
        .formdate{
            width: 150px;
        }
        #clientId{
            border: 1px #ccc solid;
            min-width: 100px;
        }
        .container{
            text-align: center;
        }

        .ondaytotal{
        	background-color: #cfe7e9;
        }

        .lasttotal{
        	background-color: #b8b9ef;
        }
    </style>
    <style>
		.table-row .tr-row:nth-child(even) { background: #FFFFFF; } .table-row .tr-row:nth-child(odd) { background: #DCDDDE; } #accountingList tr.tr-row:hover{ background: #FF9966 !important; } .table-data { font-size: 13px; padding-left: 7px; padding-right: 7px; border-bottom: 1px solid rgb(183, 194, 208); border-right: 1px solid rgb(183, 194, 208); height: 30px; text-align: left; } .table-header { font-size: 13px; padding-left: 7px; padding-right: 7px; border-top: 1px solid rgb(183, 194, 208); border-bottom: 1px solid rgb(183, 194, 208); border-right: 1px solid rgb(183, 194, 208); }
	</style>

@stop


@section('mobile_content')

	<div class="container" style="overflow:scroll; max-width: 90% !important;">
		
			<div class="row formFiltter">
				<label>ថ្ងៃខែចាម់ផ្តើម</label>
				<input type="datetime" id="fromDate" class="formdate"/>
				<label>បញ្ចប់</label>
				<input type="datetime" id="toDate" class="formdate"/>
				<label>កូន</label>
				<select id="clientId">
                    <option value="{{Session::get('iduserlotMobileSec')}}">{{Session::get('usernameLotMobileSec')}}</option>
                    @foreach($userList as $child)
                        <option value="{{$child->s_id}}">{{$child->s_name}}</option>
                    @endforeach
					
				</select>
			</div>
			<div class="row" style="padding-top:15px;">
            	<div class="mainData" style="padding-left: 0px;">ថ្ងៃខែ: <span class="dateDisplay"></span></div>
				<table width="100%" class="table-row" align="center" cellspacing="0" cellpadding="0" id="dataReport">
					
                    
						
						
					
				</table>
			</div>
	</div>


@endsection


@section('mobile_javascript')
	<script src="{{ asset('/') }}/mobile_js/checkreportdata.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop