<table cellspacing="3" cellpadding="0" width="100%" id="main_keyboard">
	<tbody>
		<tr height="128" id="tr_mn_1" style="height: 190px;">
			<td class="btntds" width="16%" id="del_key">លុប</td>
			<td class="btntd" width="21%" id="keynum_7">7</td>
			<td class="btntd" width="21%" id="keynum_8">8</td>
			<td class="btntd" width="21%" id="keynum_9">9</td>
			<td class="btntd" width="21%" id="key_x">x</td>
		</tr>
		<tr height="128" id="tr_mn_2" style="height: 190px;">
			<td class="btntd" id="back_all">
				<img src="{{ asset('/') }}/img/sale/swype-b.png" width="80" />
			</td>
			<td class="btntd" id="keynum_4">4</td>
			<td class="btntd" id="keynum_5">5</td>
			<td class="btntd" id="keynum_6">6</td>
			<td rowspan="3">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr id="tr_mn_11" style="height: 285px;">
							<td class="btntd" id="plus_key">+</td>
						</tr>
						<tr id="tr_mn_12" style="height: 285px;">
							<td class="btntd" id="enter_key">↵</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr height="128" id="tr_mn_3" style="height: 190px;">
			<td class="btntds" id="back_last">
				<img src="{{ asset('/') }}/img/sale/back_arrow.png" width="80" />
			</td>
			<td class="btntd" id="keynum_1">1</td>
			<td class="btntd" id="keynum_2">2</td>
			<td class="btntd" id="keynum_3">3</td>
		</tr>
		<tr height="128" id="tr_mn_4" style="height: 190px;">
			<td class="btntds" id="add_break_key">វគ្គថ្មី</td>
			<td class="btntd" id="keynum_0" colspan="2">0</td>
			<td class="btntd" id="dot_key">.</td>
		</tr>
	</tbody>
</table>