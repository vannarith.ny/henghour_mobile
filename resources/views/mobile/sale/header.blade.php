<table cellpadding="0" cellspacing="0" width="100%" border="0" class="backgroundMenu bodermenu">
		<tbody>
			<tr class="">
				<td>
					<form name="add_frm" class="paddingleftright" style="padding-bottom: 5px;">
						<table cellpadding="0" cellspacing="2">
							<tbody>
								<tr style="height:80px">
									<td> <div class="btntds btn_show_menu">&nbsp;បញ្ជី&nbsp;</div></td>
									<td onclick="time_click(event);">
										<div class="btntds" style="width: 110px;">
											<span id="timeName" time_id="{{$sheet_id}}">{{substr($sheetName->pav_value,1)}}</span>
											<select class="select" name="time" style="display:none">
												@foreach ($sheets as $sheet)
												    <option value="{{$sheet->pav_id}}">{{substr($sheet->pav_value,1)}}</option>
												@endforeach
											</select>
										</div>
									</td>
									<td>
										<img style="vertical-align:middle" src="{{ asset('/') }}/img/sale/prev.png" onclick="Prev_Date(event)" width="60" />
									</td>
									<td>
										<input id="txtdate" class="inputdate" type="text" name="date" readonly="readonly" />
										<input id="txt_stc_type"  type="hidden" name="txt_stc_type" value="{{$stc_type}}" />
									</td>
									<td>
										<img style="vertical-align:middle" src="{{ asset('/') }}/img/sale/next.png" onclick="Next_Date(event)" width="60" />
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</td>
				<td>
					<form name="frm_top">
						<table align="left" width="100%">
							<tbody>
								<tr>
									<td id="td_page" style="display:none" nowrap="nowrap">សន្លឹក
										<input class="input" type="text" name="page" value="1" id="frm_page" readonly="readonly" />
									</td>
									<td id="col_tail">
										<table width="100%">
											<tbody>
												<tr>
													<td style="display:none"></td>
													<td style="font-size:46px;">វគ្គ:</td>
													<td nowrap="nowrap">
														<input type="text" onclick="this.value='';change_focus(8);" name="tail" style="width:150px; margin-top: 5px; height: 70px;" id="tail" class="input" readonly="readonly" />
														<a href="javascript:void(0);" onclick="print_bt();" style="color:#00F; text-decoration:none; line-height:12px;">
															<img src="{{ asset('/') }}/img/sale/Receipts-iPhone-App-Icon.png" height="70" style="margin-bottom:15px;" />
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td style="display:none">
										<input type="checkbox" name="sang" title="For Sang" onclick="document.frm.bnum.focus();" />
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</td>
				<td>
					<table style="display:none" align="right" id="right_sh_id" cellpadding="0" cellspacing="3">
						<tbody>
							<tr>
								<td width="90" class="btntds" id="btn_back_ch">ចាក់</td>
								<td width="60" id="btn_show_prev" class="btntds">←</td>
								<td width="60" id="btn_show_next" class="btntds">→</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>