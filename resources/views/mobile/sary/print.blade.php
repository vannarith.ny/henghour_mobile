	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="{{ asset('/') }}/mobile_css/jquery-ui.min.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/khmer-font.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/font-awesome.min.css" rel="stylesheet" />
	<link href="{{ asset('/') }}/mobile_css/sary.css" rel="stylesheet" />
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/post.js?v=2"></script>

	
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/sary.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>

    <style type="text/css">
    	*:focus {
		    outline: none;
		}
    	input#resultDate {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 70px;
			    border: 1px #ccc solid;
			    border-radius:10px;
			    margin-left: 70px;
			    margin-right: 70px;
    	}
		
    	#resultDate{
    		text-align: center;
    	}
    	.table_sary_afternoon td{
    		padding-left: 5px;
    		padding-top: 5px;
    	}
    	.dateTitlesary{
    		padding-left: 10px;
    	}
    	
    	.tableSrey{
    		background-color: #FFF;
    		text-align: center;
    		width: 600px;
    	}
    	.tableSrey td {
		    border: 1px #ccc solid;
		    padding: 2px 10px 2px 2px;
		    font-size: 55px;
		    
		}
		.width200{
			width: 180px;
		}


    </style>

	<div class="container-fluid">
		
		<?php 
               // var_dump($resultsMorningVone);
          ?>

		<div class="row pt-2 justify-content-md-center" id="saryContent">
				<div class="col-md-12 ">
					<div class="row justify-content-md-center">
                              <!-- start morning -->
                              @if(isset($resultsKH) and count($resultsKH) > 0 )
                                   @for ($i = 0; $i < 1; $i++)
                                   <article class="" >
                                   
                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                    <tr>
                                        <td colspan="3">
                                            <b style="color: red; font-size: 56px;">លទ្ធផលឆ្នោតខ្មែរ 
                                            @if($time == 36)
                                                ព្រលឹម
                                            @elseif($time == 17)
                                                ព្រឹក
                                            @elseif($time == 12)
                                                ថ្ងៃ
                                            @elseif($time == 13)
                                                រសៀល
                                            @elseif($time == 24)
                                                ល្ងាច
                                            @elseif($time == 14)
                                                យប់
                                            @else
                                                ព្រលឹម
                                            @endif
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b>Date : {{$date}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                             <b style="color: red; font-size: 64px;">
                                                 @if($time == 36)
                                                    08:45 AM
                                                @elseif($time == 17)
                                                    10:45 AM
                                                @elseif($time == 12)
                                                    01:00 PM
                                                @elseif($time == 13)
                                                    03:45 PM
                                                @elseif($time == 24)
                                                    06:00 PM
                                                @elseif($time == 14)
                                                    07:45 PM
                                                @else
                                                    08:45 AM
                                                @endif
                                             </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>A</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[0]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[1]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>B</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[2]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[3]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>C</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[4]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[5]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>D</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[6]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsKH[7]->re_num_result}}</b>
                                        </td>
                                    </tr>

                                  
                                    
                                </table>
                            </article>
                                   @endfor
                              @endif


                              <!-- start afternoon  -->
                              @if(isset($resultsmorning) and count($resultsmorning) > 0 )
                                   @for ($i = 0; $i < 1; $i++)
                                   <article class="" >
                                   
                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                    <tr>
                                        <td colspan="2">
                                            <b style="color: red; font-size: 56px;">លទ្ធផលឆ្នោតយួនថ្ងៃ</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <b>Date : {{$date}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <b style="color: red; font-size: 64px;">1:30 PM</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>A</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsmorning[0]->re_num_result}}</b><br>
                                             <b>{{$resultsmorning[1]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                   @if(isset($sary))
                                  @php 
                                      $data = $sary->info;
                                      $resultLot = explode(",", $data);
                                  @endphp
                                    <tr>
                                        <td>
                                             <b>Giải sáu</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[0]}}</b><br>
                                             <b>{{$resultLot[1]}}</b><br>
                                             <b>{{$resultLot[2]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải năm</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[3]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải tư</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[4]}}</b><br>
                                             <b>{{$resultLot[5]}}</b><br>
                                             <b>{{$resultLot[6]}}</b><br>
                                             <b>{{$resultLot[7]}}</b><br>
                                             <b>{{$resultLot[8]}}</b><br>
                                             <b>{{$resultLot[9]}}</b><br>
                                             <b>{{$resultLot[10]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải nhất</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[11]}}</b>
                                        </td>
                                    </tr>
                                    
                                   @endif
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>B</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsmorning[2]->re_num_result}}</b> - <b>{{$resultsmorning[3]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>C</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsmorning[4]->re_num_result}}</b> - <b>{{$resultsmorning[5]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>D</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsmorning[6]->re_num_result}}</b> - <b>{{$resultsmorning[7]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>F</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsmorning[8]->re_num_result}}</b> - <b>{{$resultsmorning[9]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>I</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsmorning[10]->re_num_result}}</b> - <b>{{$resultsmorning[11]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>N</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsmorning[12]->re_num_result}}</b> - <b>{{$resultsmorning[13]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                </table>
                            </article>
                                   @endfor
                              @endif


                              <!-- start evening -->

						@if(isset($resultsAfternoon) and count($resultsAfternoon) > 0 )
							@for ($i = 0; $i < 1; $i++)
		 					<article class="" >
	 						
                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                    <tr>
                                        <td colspan="2">
                                            <b style="color: red; font-size: 56px;">លទ្ធផលឆ្នោតយួនល្ងាច</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <b>Date : {{$date}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <b style="color: red; font-size: 64px;">4:30 PM</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>A</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsAfternoon[0]->re_num_result}}</b><br>
                                             <b>{{$resultsAfternoon[1]->re_num_result}}</b>
                                        </td>
                                    </tr>
                    			@if(isset($sary))
		                        @php 
		                            $data = $sary->info;
		                            $resultLot = explode(",", $data);
		                        @endphp
                                    <tr>
                                        <td>
                                             <b>Giải sáu</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[0]}}</b><br>
                                             <b>{{$resultLot[1]}}</b><br>
                                             <b>{{$resultLot[2]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải năm</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[3]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải tư</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[4]}}</b><br>
                                             <b>{{$resultLot[5]}}</b><br>
                                             <b>{{$resultLot[6]}}</b><br>
                                             <b>{{$resultLot[7]}}</b><br>
                                             <b>{{$resultLot[8]}}</b><br>
                                             <b>{{$resultLot[9]}}</b><br>
                                             <b>{{$resultLot[10]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải ba</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[11]}}</b><br>
                                             <b>{{$resultLot[12]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải nhì</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[13]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải nhất</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[14]}}</b>
                                        </td>
                                    </tr>
                    			@endif
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>B</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsAfternoon[2]->re_num_result}}</b> - <b>{{$resultsAfternoon[3]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>C</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsAfternoon[4]->re_num_result}}</b> - <b>{{$resultsAfternoon[5]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>D</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsAfternoon[6]->re_num_result}}</b> - <b>{{$resultsAfternoon[7]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>F</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsAfternoon[8]->re_num_result}}</b> - <b>{{$resultsAfternoon[9]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>I</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsAfternoon[10]->re_num_result}}</b> - <b>{{$resultsAfternoon[11]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>N</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsAfternoon[12]->re_num_result}}</b> - <b>{{$resultsAfternoon[13]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                </table>
                            </article>
	 						@endfor
						@endif

						@if(isset($resultsEvening) and count($resultsEvening) > 0 )
                                   @for ($i = 0; $i < 1; $i++)
                                   <article class="" >
                                   
                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                    <tr>
                                        <td colspan="2">
                                            <b style="color: red; font-size: 56px;">លទ្ធផលឆ្នោតយួនយប់</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <b>Date : {{$date}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <b style="color: red; font-size: 64px;">6:30 PM</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>A</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsEvening[0]->re_num_result}}</b><br>
                                             <b>{{$resultsEvening[1]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                   @if(isset($sary))
                                  @php 
                                      $data = $sary->info;
                                      $resultLot = explode(",", $data);
                                  @endphp
                                    <tr>
                                        <td>
                                             <b>Giải sáu</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[0]}}</b><br>
                                             <b>{{$resultLot[1]}}</b><br>
                                             <b>{{$resultLot[2]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải năm</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[3]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải tư</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[4]}}</b><br>
                                             <b>{{$resultLot[5]}}</b><br>
                                             <b>{{$resultLot[6]}}</b><br>
                                             <b>{{$resultLot[7]}}</b><br>
                                             <b>{{$resultLot[8]}}</b><br>
                                             <b>{{$resultLot[9]}}</b><br>
                                             <b>{{$resultLot[10]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải ba</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[11]}}</b><br>
                                             <b>{{$resultLot[12]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải nhì</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[13]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>Giải nhất</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[14]}}</b>
                                        </td>
                                    </tr>
                                   @endif
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>B</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsEvening[2]->re_num_result}}</b> - <b>{{$resultsEvening[3]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>C</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsEvening[4]->re_num_result}}</b> - <b>{{$resultsEvening[5]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 70px !important;">
                                             <b>D</b>
                                        </td>
                                        <td style="text-align: right; font-size: 70px !important;">
                                             <b>{{$resultsEvening[6]->re_num_result}}</b> - <b>{{$resultsEvening[7]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                </table>
                            </article>
                                   @endfor
                              @endif
	 					
					</div>
				</div>
				
				
				
			</div>
		</div>

		
				
	</div>


    	

       
      


	