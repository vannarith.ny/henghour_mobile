	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="{{ asset('/') }}/mobile_css/jquery-ui.min.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/khmer-font.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/font-awesome.min.css" rel="stylesheet" />
	<link href="{{ asset('/') }}/mobile_css/sary.css" rel="stylesheet" />
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/post.js?v=2"></script>

	
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/sary.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>

    <style type="text/css">
    	
    	.tableSrey td {
		    padding: 4px 10px 4px 10px;
		    
		}


    </style>

	<div class="container-fluid">
		
		

		<div class="row pt-2 justify-content-md-center" id="saryContent">
				<div class="col-md-12 ">
					<div class="row justify-content-md-center">
					@if($type == 1)
                        <!-- គុណកាត់យក 3 -->
                        <table align="center" class="tableSrey">
                            <tbody>
                                <tr height="50">
                                    <td colspan="3" align="center">គុណកាត់យក 3 ខ្ទង់</td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        <table cellspacing="10" style="border:1px solid #000000">
                                            <tbody>
                                                <tr>
                                                    <td>1112</td>
                                                    <td width="15">x</td>
                                                    <td align="right">4</td>
                                                </tr>
                                                <tr>
                                                    <td>1122</td>
                                                    <td width="15">x</td>
                                                    <td align="right">6</td>
                                                </tr>
                                                <tr>
                                                    <td>1123</td>
                                                    <td width="15">x</td>
                                                    <td align="right">12</td>
                                                </tr>
                                                <tr>
                                                    <td>1234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">24</td>
                                                </tr>
                                                <tr>
                                                    <td>11122</td>
                                                    <td width="15">x</td>
                                                    <td align="right">7</td>
                                                </tr>
                                                <tr>
                                                    <td>11123</td>
                                                    <td width="15">x</td>
                                                    <td align="right">13</td>
                                                </tr>
                                                <tr>
                                                    <td>11223</td>
                                                    <td width="15">x</td>
                                                    <td align="right">18</td>
                                                </tr>
                                                <tr>
                                                    <td>11234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">33</td>
                                                </tr>
                                                <tr>
                                                    <td>12345</td>
                                                    <td width="15">x</td>
                                                    <td align="right">60</td>
                                                </tr>
                                                <tr>
                                                    <td>111222</td>
                                                    <td width="15">x</td>
                                                    <td align="right">8</td>
                                                </tr>
                                                <tr>
                                                    <td>111223</td>
                                                    <td width="15">x</td>
                                                    <td align="right">19</td>
                                                </tr>
                                                <tr>
                                                    <td>112233</td>
                                                    <td width="15">x</td>
                                                    <td align="right">24</td>
                                                </tr>
                                                <tr>
                                                    <td>111234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">34</td>
                                                </tr>
                                                <tr>
                                                    <td>112234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">42</td>
                                                </tr>
                                                <tr>
                                                    <td>112345</td>
                                                    <td width="15">x</td>
                                                    <td align="right">72</td>
                                                </tr>
                                                <tr>
                                                    <td>123456</td>
                                                    <td width="15">x</td>
                                                    <td align="right">120</td>
                                                </tr>
                                                <tr>
                                                    <td>1112223
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">20</td>
                                                </tr>
                                                <tr>
                                                    <td>1112233
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">25</td>
                                                </tr>
                                                <tr>
                                                    <td>1112234
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">43</td>
                                                </tr>
                                                <tr>
                                                    <td>1112345
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">73</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">51</td>
                                                </tr>
                                                <tr>
                                                    <td>1122345
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">84</td>
                                                </tr>
                                                <tr>
                                                    <td>1123456
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">135</td>
                                                </tr>
                                                <tr>
                                                    <td>1234567
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">210</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50"></td>
                                    <td>
                                        <table cellspacing="10" style="border:1px solid #000000">
                                            <tbody>
                                                <tr>
                                                    <td>11223344</td>
                                                    <td width="15">x</td>
                                                    <td align="right">60</td>
                                                </tr>
                                                <tr>
                                                    <td>11223345</td>
                                                    <td width="15">x</td>
                                                    <td align="right">96</td>
                                                </tr>
                                                <tr>
                                                    <td>11223456</td>
                                                    <td width="15">x</td>
                                                    <td align="right">150</td>
                                                </tr>
                                                <tr>
                                                    <td>11234567</td>
                                                    <td width="15">x</td>
                                                    <td align="right">228</td>
                                                </tr>
                                                <tr>
                                                    <td>12345678</td>
                                                    <td width="15">x</td>
                                                    <td align="right">336</td>
                                                </tr>
                                                <tr>
                                                    <td>112233445</td>
                                                    <td width="15">x</td>
                                                    <td align="right">108</td>
                                                </tr>
                                                <tr>
                                                    <td>112233456</td>
                                                    <td width="15">x</td>
                                                    <td align="right">165</td>
                                                </tr>
                                                <tr>
                                                    <td>112234567</td>
                                                    <td width="15">x</td>
                                                    <td align="right">246</td>
                                                </tr>
                                                <tr>
                                                    <td>112345678</td>
                                                    <td width="15">x</td>
                                                    <td align="right">357</td>
                                                </tr>
                                                <tr>
                                                    <td>123456789</td>
                                                    <td width="15">x</td>
                                                    <td align="right">504</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334455
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">120</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334456
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">180</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334567
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">264</td>
                                                </tr>
                                                <tr>
                                                    <td>1122345678
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">378</td>
                                                </tr>
                                                <tr>
                                                    <td>1123456789
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">528</td>
                                                </tr>
                                                <tr>
                                                    <td>1234567890
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">720</td>
                                                </tr>
                                                <tr>
                                                    <td>333220154</td>
                                                    <td width="15">x</td>
                                                    <td align="right">151</td>
                                                </tr>
                                                <tr>
                                                    <td>112233445566
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">210</td>
                                                </tr>
                                                <tr>
                                                    <td>11223344556677
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">336</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334455667788
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">504</td>
                                                </tr>
                                                <tr>
                                                    <td>112233445678
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">420</td>
                                                </tr>
                                                <tr>
                                                    <td>123</td>
                                                    <td width="15">x</td>
                                                    <td align="right">6</td>
                                                </tr>
                                                <tr>
                                                    <td>112</td>
                                                    <td width="15">x</td>
                                                    <td align="right">3</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    @else
                        <!-- គុណកាត់យក 4 -->
                        <table align="center" class="tableSrey">
                            <tbody>
                                <tr height="50">
                                    <td colspan="3" align="center">គុណកាត់យក 4 ខ្ទង់</td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        <table cellspacing="10" style="border:1px solid #000000">
                                            <tbody>
                                                <tr>
                                                    <td>1112</td>
                                                    <td width="15">x</td>
                                                    <td align="right">4</td>
                                                </tr>
                                                <tr>
                                                    <td>1122</td>
                                                    <td width="15">x</td>
                                                    <td align="right">6</td>
                                                </tr>
                                                <tr>
                                                    <td>1123</td>
                                                    <td width="15">x</td>
                                                    <td align="right">12</td>
                                                </tr>
                                                <tr>
                                                    <td>1234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">24</td>
                                                </tr>
                                                <tr>
                                                    <td>11122</td>
                                                    <td width="15">x</td>
                                                    <td align="right">10</td>
                                                </tr>
                                                <tr>
                                                    <td>11123</td>
                                                    <td width="15">x</td>
                                                    <td align="right">20</td>
                                                </tr>
                                                <tr>
                                                    <td>11223</td>
                                                    <td width="15">x</td>
                                                    <td align="right">30</td>
                                                </tr>
                                                <tr>
                                                    <td>11234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">60</td>
                                                </tr>
                                                <tr>
                                                    <td>12345</td>
                                                    <td width="15">x</td>
                                                    <td align="right">120</td>
                                                </tr>
                                                <tr>
                                                    <td>111222</td>
                                                    <td width="15">x</td>
                                                    <td align="right">14</td>
                                                </tr>
                                                <tr>
                                                    <td>111223</td>
                                                    <td width="15">x</td>
                                                    <td align="right">38</td>
                                                </tr>
                                                <tr>
                                                    <td>112233</td>
                                                    <td width="15">x</td>
                                                    <td align="right">54</td>
                                                </tr>
                                                <tr>
                                                    <td>111234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">72</td>
                                                </tr>
                                                <tr>
                                                    <td>112234</td>
                                                    <td width="15">x</td>
                                                    <td align="right">102</td>
                                                </tr>
                                                <tr>
                                                    <td>112345</td>
                                                    <td width="15">x</td>
                                                    <td align="right">192</td>
                                                </tr>
                                                <tr>
                                                    <td>123456</td>
                                                    <td width="15">x</td>
                                                    <td align="right">360</td>
                                                </tr>
                                                <tr>
                                                    <td>1112223
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">46</td>
                                                </tr>
                                                <tr>
                                                    <td>1112233
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">62</td>
                                                </tr>
                                                <tr>
                                                    <td>1112234
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">114</td>
                                                </tr>
                                                <tr>
                                                    <td>1112345
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">208</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">150</td>
                                                </tr>
                                                <tr>
                                                    <td>1122345
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">270</td>
                                                </tr>
                                                <tr>
                                                    <td>1123456
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">480</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50"></td>
                                    <td>
                                        <table cellspacing="10" style="border:1px solid #000000">
                                            <tbody>
                                                <tr>
                                                    <td>1234567
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">840</td>
                                                </tr>
                                                <tr>
                                                    <td>11223344</td>
                                                    <td width="15">x</td>
                                                    <td align="right">204</td>
                                                </tr>
                                                <tr>
                                                    <td>11223345</td>
                                                    <td width="15">x</td>
                                                    <td align="right">354</td>
                                                </tr>
                                                <tr>
                                                    <td>11223456</td>
                                                    <td width="15">x</td>
                                                    <td align="right">606</td>
                                                </tr>
                                                <tr>
                                                    <td>11234567</td>
                                                    <td width="15">x</td>
                                                    <td align="right">1020</td>
                                                </tr>
                                                <tr>
                                                    <td>12345678</td>
                                                    <td width="15">x</td>
                                                    <td align="right">1680</td>
                                                </tr>
                                                <tr>
                                                    <td>112233445</td>
                                                    <td width="15">x</td>
                                                    <td align="right">444</td>
                                                </tr>
                                                <tr>
                                                    <td>112233456</td>
                                                    <td width="15">x</td>
                                                    <td align="right">738</td>
                                                </tr>
                                                <tr>
                                                    <td>112234567</td>
                                                    <td width="15">x</td>
                                                    <td align="right">1206</td>
                                                </tr>
                                                <tr>
                                                    <td>112345678</td>
                                                    <td width="15">x</td>
                                                    <td align="right">1932</td>
                                                </tr>
                                                <tr>
                                                    <td>123456789</td>
                                                    <td width="15">x</td>
                                                    <td align="right">3024</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334455
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">540</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334456
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">876</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334567
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">1398</td>
                                                </tr>
                                                <tr>
                                                    <td>1122345678
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">2190</td>
                                                </tr>
                                                <tr>
                                                    <td>1123456789
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">3360</td>
                                                </tr>
                                                <tr>
                                                    <td>1234567890
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">5040</td>
                                                </tr>
                                                <tr>
                                                    <td>333220154</td>
                                                    <td width="15">x</td>
                                                    <td align="right">626</td>
                                                </tr>
                                                <tr>
                                                    <td>112233445566
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">1170</td>
                                                </tr>
                                                <tr>
                                                    <td>11223344556677
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">2226</td>
                                                </tr>
                                                <tr>
                                                    <td>1122334455667788
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">3864</td>
                                                </tr>
                                                <tr>
                                                    <td>112233445678
                                                    </td>
                                                    <td width="15">x</td>
                                                    <td align="right">2724</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="right" style="font-size:12px"></td>
                                </tr>
                            </tbody>
                        </table>

                    @endif
	 					
					</div>
				</div>
				
				
				
			</div>
		</div>

		
				
	</div>


    	

       
      


	