<!-- 
<div class="container-fluid">
	<div class="nav">
		<a href="{{URL::to('/')}}/mobile/client" class=" @if($page == 'Client') active @endif ">ភ្នាក់ងារ</a>
		<a href="{{URL::to('/')}}/mobile/post" class=" @if($page == 'Post') active @endif ">ប៉ុស្ត៏</a>
		<a href="{{URL::to('/')}}/mobile/timeclose" class=" @if($page == 'time_close') active @endif ">ម៉ោងបិទ</a>
		<a href="{{URL::to('/')}}/mobile/checkresult" class="">ផ្ទៀងលេខត្រូវ</a>
		<a href="{{URL::to('/')}}/mobile/sary" class=" @if($page == 'sary') active @endif ">សារី</a>
		<a href="{{URL::to('/')}}/mobile/dailyReport" class=" @if($page == 'dailyreport') active @endif" >ក្បាលបញ្ជី</a>
		<a href="{{URL::to('/')}}/mobile/checkreportdata" class="@if($page == 'checkreportdata') active @endif">ពិនិត្យបញ្ជី</a>
		<a href="{{URL::to('/')}}/mobile/changePassword" class="">ប្ដូរពាក្យសំងាត់</a>
		<a href="{{URL::to('/')}}/mobile/transctionstaff" class="@if($page == 'TransctionStaff') active @endif">ស៊ីសង</a>
		<a href="{{URL::to('/')}}/mobile/sale" class=" @if($page == 'sale') active @endif ">ចាក់ឆ្នោត</a>
		<a href="{{URL::to('/')}}/mobile/howtoplay" class="">របៀបចាក់</a>
		<a href="{{URL::to('/')}}/mobile/logout">ចេញ</a>
	</div>
</div> -->

<div class="headerMenu row">
	<div class="col-4">
		<button type="button" id="header-menu" class="btn btn-lg btn-secondary ml-2 fontBig" data-toggle="button" aria-pressed="false" autocomplete="off">
		  មឺនុយ
		</button>
	</div>
	<div class="col-8 text-right pt-2 text-white">
		<h2>សូមស្វាគមន៏ <span class="text-white">{{Session::get('nameLotMobileSec')}}</span> មកកាន់ឡូតូ</h2>
	</div>
</div>
