@extends('master')

@section('title')
<title>{{trans('label.edit_user')}}</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.form_add input{
			padding: 5px;
		}
		.form_add_kh input{
			padding: 5px;
		}
		.form_add_th input{
			padding: 5px;
		}
		.form_add_lo input{
			padding: 5px;
		}
		.show_input_add,.show_input_add_main,.show_input_add_main_kh,.show_input_add_main_th,
		.show_input_add_kh,.show_input_add_th,.show_input_add_lo{
			display: none;
		}
		.staff_charge_lo .alert-success,.staff_charge_lo .alert-danger,.staff_charge_th .alert-success,.staff_charge_th .alert-danger,.staff_charge_kh .alert-success,.staff_charge_kh .alert-danger, .show_input_add_main_clear{
			display: none;
		}
		.staff_charge .alert-success,.staff_charge .alert-danger{
			display: none;
		}
		.staff_charge_main .alert-success,.staff_charge_main .alert-danger{
			display: none;
		}
		.addmore_width{
			width: 150px;
		}

		.staff_charge_main_kh .alert-success,.staff_charge_main_kh .alert-danger{
			display: none;
		}
		.staff_charge_main_th .alert-success,.staff_charge_main_th .alert-danger{
			display: none;
		}

		.staff_charge_main_clear .alert-success{
			display: none;
		}
		.staff_charge_main_clear .alert-danger{
			display: none;
		}
	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li>អ្នកប្រើប្រាស់</li><li>{{trans('label.edit_user')}}</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-user fa-fw "></i> 
		         
		      </h1>
		     </div>
		</div>

		
    	<section id="widget-grid" class="">
    		@include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>
		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
		         <h2>{{trans('label.edit_user')}}</h2>    
		         
		        </header>

		        <!-- widget div-->
		        <div>
		         
		         <!-- widget edit box -->
		         <div class="jarviswidget-editbox">
		          <!-- This area used as dropdown edit box -->
		          
		         </div>
		         <!-- end widget edit box -->
		         
		         <!-- widget content -->
		         <div class="widget-body no-padding">               
		             {!! Form::model($user, ['route' => ['user.update', $user->u_id] ,'method' => 'PATCH',  'class' => 'smart-form user-form', 'novalidate' => 'validate', 'id' => 'checkout-form' ,"autocomplete"=>"false"]) !!}
		           <fieldset>

			            <div class="row">
			             <section class="col col-6">
			              {{ Form::label('u_name', 'ឈ្មោះ', array('class' => 'label')) }}
			              <label class="input">
			               {!! Form::text("u_name", $value = null, $attributes = array( 'id' => 'u_name', 'placeholder'=>'Name')) !!}
			              </label>
			             </section>


			             <section class="col col-6">
			              {{ Form::label('u_phone', 'លេខទូរស័ព្ទ', array('class' => 'label')) }}
			              <label class="input"> <i class="icon-append fa fa-envelope-o"></i>
			               {!! Form::text("u_phone", $value = null, $attributes = array('class' => 'form-control', 'id' => 'u_phone', 'placeholder'=>'xxx-xxx-xxxx', 'data-mask'=>'(999) 999-999?9')) !!}
			              </label>
			             </section>

			            </div>


			            <div class="row">
			             
				             <section class="col col-6">
				              {{ Form::label('u_line', 'ID គណនីសង្គមLine', array('class' => 'label')) }}
				              <label class="input"> 
				               {!! Form::text("u_line", $value = null, $attributes = array( 'id' => 'u_line', 'placeholder'=>'Line ID')) !!}
				              </label>
				             </section>
							 <section class="col col-6">
							 	<div style="padding-right: 20px; padding-top:30px;">
						        	<input type="checkbox" name="u_unlock" class="checkboxRadio" id="u_unlock" style="" @if($user->u_unlock) checked @endif>
							        Unlock
							    </div>
				             </section>
			             
			            </div>
					
		           	</fieldset>
		            <fieldset>
		            	<legend>{{trans('label.user_register')}}</legend>
			            <div class="row">
			             <section class="col col-6">
			              {{ Form::label('role', trans('label.type_user'), array('class' => 'label')) }}
			              <label class="select">                              
			               <select name="role" id="role" class="form-control input-sm require"> 
			                                
			                <option value="1" @if($user->role == 1) selected @endif >Admin</option>
			                <option value="2" @if($user->role == 2) selected @endif >Checker1</option>
							<option value="3" @if($user->role == 3) selected @endif >Checker2</option>
			                <option value="0" @if($user->role == 0) selected @endif>Normal</option>
			                <option value="100" @if($user->role == 100) selected @endif>កូនកាតុង</option>                           
			              
			               </select>
			              </label>
			             </section>
		             
		            	</div>
			            <div class="row">
			             <section class="col col-6">
			              {{ Form::label('u_username', 'ឈ្មោះអ្នកប្រើប្រាស់', array('class' => 'label')) }}
			              <label class="input"> <i class="icon-append fa fa-user"></i>
			               {!! Form::text("u_username", $value = null, $attributes = array('class' => 'form-control', 'id' => 'u_username', 'placeholder'=>'Username')) !!}
			              </label>
			             </section>
			             <section class="col col-6">
			              {{ Form::label('new_password', 'លេខសម្ងាត់ថ្មី', array('class' => 'label')) }}
			              <label class="input"> <i class="icon-append fa fa-lock"></i>
			               {!! Form::text("new_password", null, $attributes = array('class' => 'form-control', 'id' => 'new_password', 'placeholder'=>'Password','autocomplete'=>'off')) !!}
			              </label>
			             </section>
			            </div>

		           </fieldset>


		           <footer>
		             <button type="submit" name="submit" class="btn btn-primary">រក្សាទុក</button>  
		             <button type="button" class="btn btn-warning" onclick="btnCancel()">{{trans('label.cancel')}}</button>                      
		           </footer>

		           <div class="message">
		            <i class="fa fa-check fa-lg"></i>
		            <p>
		             Your comment was successfully added!
		            </p>
		           </div>
		          {{ Form::close() }}
		          
		         </div>
		         <!-- end widget content -->
		         
		        </div>
		        <!-- end widget div -->
		        
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->

		      <article class="col-sm-12 col-md-12 col-lg-12">
		      	
			       	<!-- Widget ID (each widget will need unique ID)-->
			       	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-108" data-widget-editbutton="false">        
				        <header>
				         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				         <h2>បង្ហាញឈ្មោះកូនដែលត្រូវវាយ</h2>
				        </header>
				        <div class="staff_charge_main">
				        	<div class="alert alert-success " style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
				           <div class="alert alert-danger "  style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
					        <div class="widget-body no-padding">          
					          	<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
					           
						           <thead>           
							            <tr>
								             <th>{{ trans('label.id') }}</th>
								             <th>ឈ្មោះកូន</th>
								             <th>{{ trans('label.action') }}</th>
							            </tr>
						           </thead>
						           <tbody>
						           		
						           		<tr class="form_add_main smart-form">
								             <td><button id="btn_add_staff_charge_main" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>
								             
								             <td class="show_input_add_main">
								             	<label class="select">
												   {{ Form::select('parent_id_main',(['0' => trans('result.chooseStaff') ]+$staffs) , null, ['class' => 'select2 addmore_width ','id'=>'parent_id_main']) }}
											   </label>
								             </td>
								             
								             


								             <td class="show_input_add_main">
								             	<button id="btn_new_staff_charge_main" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
								             </td>
								       	</tr>
								       

						           	<?php $i=0;?>
			           				@foreach($user_staffs as $user_staff)
			           					<?php $i++;?>
							            <tr class="staff_charge_main-{{$user_staff->us_id}}">
								             <td>{{$i}}</td>
								             <td>{{$user_staff->s_name}}</td>
								             <td>
												 
								             		 <button id="{{$user_staff->us_id}}" class="padding-button EditStaffCharge_main btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
								             		 <button id="{{$user_staff->us_id}}" class="padding-button deleteStaffCharge_main btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
								             </td>
								       	</tr>        
						            @endforeach 
						           </tbody>
					          	</table>
					    
					         </div>
					    </div>
					</div>
		      </article>
		      <!-- end control main  -->




		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->
		    
		    

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">
		$(document).ready(function() {

			// btn controll show hide form insert data
		   $(document).off('click', '#btn_add_staff_charge_main').on('click', '#btn_add_staff_charge_main', function(e){
				
				if($('.show_input_add_main').css('display') == 'none'){
					$('#stc_date_add_main').val('');
					$('#stc_two_digit_charge_add_main').val('');
					$('#stc_three_digit_charge_add_main').val('');
					$('.show_input_add_main').show();
					$("#btn_new_staff_charge_main").attr( "type", "add" );
					$("#btn_new_staff_charge_main").attr( "idData", '' );
					$(this).text("{{ trans('label.delete') }}");
					$('#stc_currency_add_main').val($('#stc_currency_add_main option:first-child').val()).trigger('change');
				}else{
					$('#stc_date_add_main').val('');
					$('#stc_two_digit_charge_add_main').val('');
					$('#stc_three_digit_charge_add_main').val('');
					$('.show_input_add_main').hide();
					$(this).text("{{ trans('label.add') }}");
				}

			});

		    // insert data to database
		   $(document).off('click', '#btn_new_staff_charge_main').on('click', '#btn_new_staff_charge_main', function(e){
				$(".staff_charge_edit .alert-success").hide();
				$(".staff_charge_edit .alert-danger").hide();
				var parent_id_main = $("#parent_id_main").val();
				// console.log(parent_id_main);
				// return false;
				if(parent_id_main > 0){
					// var stc_date_add = $("#stc_date_add_main").val();
					// var stc_two_digit_charge_add = $("#stc_two_digit_charge_add_main").val();
					// var stc_three_digit_charge_add = $("#stc_three_digit_charge_add_main").val();
					
					var user_id = '<?php echo $id;?>';
					var checkType = $(this).attr('type');
					var iddata = $(this).attr('iddata');
					var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
					// var stc_type = 1;
					// alert(parent_id_main);
					// return false;
					$.ajax({
			              url: '{{URL::to('/')}}/user/storeAddStaffToUser',
			              type: 'GET',
			              data: {
			              		 checkType:checkType,
			              		 parent_id_main:parent_id_main,
			              		 user_id:user_id,
			              		 iddata:iddata,
			              		 idDataTable:idDataTable
			              },
			              success: function(data) {
			                if(data.status=="success"){
			                  $('tr.form_add_main.smart-form').after(data.msg);
			                  $(".staff_charge_main .alert-success").show();
			                  $(".staff_charge_main .alert-success").find('span').text("{{trans('message.add_success')}}");
			                  $("#parent_id_main").val("");
			                  $("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
			                  $("#btn_new_staff_charge_main").attr('type','add').attr('iddata');
			                }else if(data.status=="updatesuccess"){
			                	$(".staff_charge_main .alert-success").show();
			                  	$(".staff_charge_main .alert-success").find('span').text("{{trans('message.update_success')}}");
			                  	$("#parent_id_main").val("");
			                  	$(".staff_charge_main-"+iddata).html(data.msg);
			                  	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
			                  	$("#btn_new_staff_charge_main").attr('type','add').attr('iddata','');
			                }else{
			                	$(".staff_charge_main .alert-danger").show();
			                  	$(".staff_charge_main .alert-danger").find('span').text(data.msg);
			                	// alert();
			                  // callPopupLogin();
			                }        
			              }
			        });
				}else{
					alert("មេត្តាបញ្ចូលឈ្មោះសិន");
				}
				
			});

			$(document).off('click', '.EditStaffCharge_main').on('click', '.EditStaffCharge_main', function(e){
				var id = $(this).attr('id');
				var table_main = 'tbl_staff_charge_main';
				$.ajax({
		              url: '{{URL::to('/')}}/user/getUserStaff',
		              type: 'GET',
		              data: {id:id,table_main:table_main},
		              success: function(data) {
		                if(data.status=="success"){

		                	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
							// alert(data.msg.chail_id);
							// $("#parent_id_main").val(data.msg.chail_id);
							$("#parent_id_main").select2().select2('val',data.msg.s_id);
							$('.show_input_add_main').show();
							$("#btn_new_staff_charge_main").attr( "type", "modify" );
							$("#btn_new_staff_charge_main").attr( "idData", data.msg.us_id );
							
		                }else{
		                	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
		                	$("#btn_new_staff_charge_main").attr( "type", "add" );
		                	$("#btn_new_staff_charge_main").attr( "idData", '' );
		                	$('.show_input_add_main').hide();
		                  // callPopupLogin();
		                }        
		              }
		        });
			});

			$(document).off('click', '.deleteStaffCharge_main').on('click', '.deleteStaffCharge_main', function(e){
				
				var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
	    		if (r == true) {
	    			var id = $(this).attr('id');
	    			$.ajax({
			              url: '{{action("UserController@deleteUserStaff",[""])}}',
			              type: 'GET',
			              data: {id:id},
			              success: function(data) {
			                if(data.status=="success"){
			                  $(".staff_charge_main-"+id).remove();
			                }else{
			                	alert(data.msg);
			                }        
			              }
			        });
	    		}
			});


		   pageSetUp();

		   var $checkoutForm = $('#checkout-form').validate({
		   // Rules for form validation
		    rules : {
		     
		     u_name : {
		      required : true
		     },
		     u_phone : {
		      required : true
		     },
		     u_username : {
		      required : true
		     },
		     u_password : {
		      required : true,
		      minlength : 6,
		      maxlength : 15
		     }
		    },
		  
		    // Messages for form validation
		    messages : {
		     u_name : {
		      required : 'Please enter your name'
		     },
		     u_phone : {
		      required : 'Please enter your phone number'
		     },
		     u_username : {
		      required : 'Please enter your username'
		     },
		     u_password : {
		      required : 'Please enter your password'
		     }
		    },
		  
		    // Do not change code below
		    errorPlacement : function(error, element) {
		     error.insertAfter(element.parent());
		    }
		   });
		 
		   // START AND FINISH DATE
		   $('#dob').datepicker({
		    dateFormat : 'yy-mm-dd',
		    prevText : '<i class="fa fa-chevron-left"></i>',
		    nextText : '<i class="fa fa-chevron-right"></i>',
		    // onSelect : function(selectedDate) {
		    //  $('#dob').datepicker('option', 'minDate', selectedDate);
		    // }
		   });

		   // alert("ok");
   		//   $('input[type="password"]').val('');
		});
		function btnCancel(){
	   		var r = confirm("តើលោកអ្នកចង់ត្រលប់ក្រោយឬទេ?");
		    if (r == true) {
		        document.location.href='{{URL::to('/')}}/user';
		    }
	   }
	</script>
@stop