@extends('master')
<?php 
use App\Http\Controllers\MaxPriceTwoController;
?>
@section('title')
<title>List Max Price</title>
@stop

@section('cssStyle')
	<style type="text/css">
        .table-bordered.table-hover{
            width:90% !important;
            border: 1px #ccc solid !important;
            margin: 0px auto;
        }
        .table-bordered.table-hover td{
            text-align: center;
        }
        .numberData{
            font-size: 16px;
            background-color: #050830 !important;
            color: #FFF;
        }
        .dataList td.data{
            height: 80px !important;
        }
        .dataList td.data.active{
            color: #FFF !important;
            background-color: red !important;
            font-size: 20px;
        }
	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li>List Max Price</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-sort-numeric-asc fa-fw "></i> 
               List Max Price
		      </h1>
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>
		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <h2>{{trans('label.list_paper')}}</h2>
		    
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding"> 
		         	{!! Form::open(['route' => 'maxpricetwofilter', 'method' => 'POST' , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						
						 <div class="row">
                            @if(isset($post_value))
                                <?php $post_filter = $post_value; ?>
                            @else
                                <?php $post_filter = null; ?>
                            @endif

                            @if(isset($limitPrice_value))
                                <?php $limitPrice_filter = $limitPrice_value; ?>
                            @else
                                <?php $limitPrice_filter = null; ?>
                            @endif

                            @if(isset($limitPrice_value))
                                <?php $limitPriceDollar_filter = $limitPriceDollar_value; ?>
                            @else
                                <?php $limitPriceDollar_filter = null; ?>
                            @endif
							
							 <section class="col col-2">

				                {{ Form::label('pos_id', 'ជ្រើសរេីសប៉ុស' , array('class' => 'label')) }}
								 <label class="select">
									 {{ Form::select('pos_id', ([
                                        '' => 'ជ្រើសរេីសប៉ុស' ]+$posts),$post_filter,['class' => 'required ','id'=>'pos_id','sms'=> 'ជ្រើសរេីសប៉ុស' ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>
                             <section class="col col-2">
				              {{ Form::label('limitPrice', 'ចំនួនទឹកប្រាក់ Alert' , array('class' => 'label')) }}
				              <label class="input"> <i class="icon-append fa fa-money"></i>
				               {!! Form::text("limitPrice", $limitPrice_filter, $attributes = array( 'id' => 'limitPrice', 'class'=>'form-control ')) !!}
				              </label>
				             </section>
                             <section class="col col-2">
				              {{ Form::label('limitPriceDollar', 'ចំនួនទឹកប្រាក់ Alert' , array('class' => 'label')) }}
				              <label class="input"> <i class="icon-append fa fa-money"></i>
				               {!! Form::text("limitPriceDollar", $limitPriceDollar_filter, $attributes = array( 'id' => 'limitPriceDollar', 'class'=>'form-control ')) !!}
				              </label>
				             </section>
							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}
                
                @if(isset($groups))
		          <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="90%">
                    <!-- block number 00 to 19 -->
                    <tr class="titleNumber">
                    @for ($i = 0; $i <= 19; $i++)
                        @if($i < 10)
                        <td class="numberData">{{'0'.$i}}</td>
                        @else
                        <td class="numberData">{{$i}}</td>
                        @endif
                    @endfor
                    <tr>
                    <tr class="dataList">
                    @for ($i = 0; $i <= 19; $i++)
                        <?php 
                            $number = $i;
                            if($i < 10){
                                $number = '0'.$i;
                            }
                            $price = MaxPriceTwoController::getpriceOfNumber($allNumber,$number,$groups,$date,$sheet_id);
                            $active = '';
                            if(
                                ( (float)$price[0] >= (float)$limitPrice_filter ) || ( (float)$price[1] >= (float)$limitPriceDollar_filter )
                            ){
                                $active = 'active';
                            }
                        ?>
                        <td class="data {{$active}}">{{$price[0].' / '.$price[1].'$'}}</td>
                    @endfor
                    <tr>

                    <!-- block number 20 to 39 -->
                    <tr class="titleNumber">
                    @for ($i = 20; $i <= 39; $i++)
                        @if($i < 10)
                        <td class="numberData">{{'0'.$i}}</td>
                        @else
                        <td class="numberData">{{$i}}</td>
                        @endif
                    @endfor
                    <tr>
                    <tr class="dataList">
                    @for ($i = 20; $i <= 39; $i++)
                        <?php 
                            $number = $i;
                            if($i < 10){
                                $number = '0'.$i;
                            }
                            $price = MaxPriceTwoController::getpriceOfNumber($allNumber,$number,$groups,$date,$sheet_id);
                            $active = '';
                            if(
                                ( (float)$price[0] >= (float)$limitPrice_filter ) || ( (float)$price[1] >= (float)$limitPriceDollar_filter )
                            ){
                                $active = 'active';
                            }
                        ?>
                        <td class="data {{$active}}">{{$price[0].' / '.$price[1].'$'}}</td>
                    @endfor
                    <tr>

                    <!-- block number 40 to 59 -->
                    <tr class="titleNumber">
                    @for ($i = 40; $i <= 59; $i++)
                        @if($i < 10)
                        <td class="numberData">{{'0'.$i}}</td>
                        @else
                        <td class="numberData">{{$i}}</td>
                        @endif
                    @endfor
                    <tr>
                    <tr class="dataList">
                    @for ($i = 40; $i <= 59; $i++)
                        <?php 
                            $number = $i;
                            if($i < 10){
                                $number = '0'.$i;
                            }
                            $price = MaxPriceTwoController::getpriceOfNumber($allNumber,$number,$groups,$date,$sheet_id);
                            $active = '';
                            if(
                                ( (float)$price[0] >= (float)$limitPrice_filter ) || ( (float)$price[1] >= (float)$limitPriceDollar_filter )
                            ){
                                $active = 'active';
                            }
                        ?>
                        <td class="data {{$active}}">{{$price[0].' / '.$price[1].'$'}}</td>
                    @endfor
                    <tr>

                    <!-- block number 60 to 79 -->
                    <tr class="titleNumber">
                    @for ($i = 60; $i <= 79; $i++)
                        @if($i < 10)
                        <td class="numberData">{{'0'.$i}}</td>
                        @else
                        <td class="numberData">{{$i}}</td>
                        @endif
                    @endfor
                    <tr>
                    <tr class="dataList">
                    @for ($i = 60; $i <= 79; $i++)
                        <?php 
                            $number = $i;
                            if($i < 10){
                                $number = '0'.$i;
                            }
                            $price = MaxPriceTwoController::getpriceOfNumber($allNumber,$number,$groups,$date,$sheet_id);
                            $active = '';
                            if(
                                ( (float)$price[0] >= (float)$limitPrice_filter ) || ( (float)$price[1] >= (float)$limitPriceDollar_filter )
                            ){
                                $active = 'active';
                            }
                        ?>
                        <td class="data {{$active}}">{{$price[0].' / '.$price[1].'$'}}</td>
                    @endfor
                    <tr>

                    <!-- block number 80 to 99 -->
                    <tr class="titleNumber">
                    @for ($i = 80; $i <= 99; $i++)
                        @if($i < 10)
                        <td class="numberData">{{'0'.$i}}</td>
                        @else
                        <td class="numberData">{{$i}}</td>
                        @endif
                    @endfor
                    <tr>
                    <tr class="dataList">
                    @for ($i = 80; $i <= 99; $i++)
                        <?php 
                            $number = $i;
                            if($i < 10){
                                $number = '0'.$i;
                            }
                            $price = MaxPriceTwoController::getpriceOfNumber($allNumber,$number,$groups,$date,$sheet_id);
                            $active = '';
                            if(
                                ( (float)$price[0] >= (float)$limitPrice_filter ) || ( (float)$price[1] >= (float)$limitPriceDollar_filter )
                            ){
                                $active = 'active';
                            }
                        ?>
                        <td class="data {{$active}}">{{$price[0].' / '.$price[1].'$'}}</td>
                    @endfor
                    <tr>
		           
		          </table>
                @endif
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();

			$('#dateStart').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#dateEnd').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#dateEnd').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#dateStart').datepicker('option', 'mixDate', selectedDate);
				}
			});

			var responsiveHelper_datatable_tabletools = undefined;
    
		    var breakpointDefinition = {
		     tablet : 1024,
		     phone : 480
		    };
			$('#datatable_tabletools').dataTable({
    
			    
			    "autoWidth" : true,
			    "preDrawCallback" : function() {
			     // Initialize the responsive datatables helper once.
			     if (!responsiveHelper_datatable_tabletools) {
			      responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			     }
			    },
			    "rowCallback" : function(nRow) {
			     responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
			    },
			    "drawCallback" : function(oSettings) {
			     responsiveHelper_datatable_tabletools.respond();
			    }
			});


			$(document).off('click', '.deleteSale').on('click', '.deleteSale', function(e){
				var r = confirm("{{trans('message.are_you_sure')}}");
	    		if (r == true) {
	    			var id = $(this).attr('id');
	    			$.ajax({
			              url: 'salephone/deleteItem',
			              type: 'GET',
			              data: {id:id},
			              success: function(data) {
			              	console.log(data);
			                if(data.status=="success"){
			                  $(".sale-"+id).remove();
			                }else{
			                	alert(data.msg);
			                  // callPopupLogin();
			                }        
			              }
			        });
	    		}
	    	});
		});
	</script>
@stop