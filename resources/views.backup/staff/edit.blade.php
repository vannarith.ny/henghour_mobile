@extends('master')

@section('title')
<title>{{trans('label.editStaff')}}</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.form_add input{
			padding: 5px;
		}
		.form_add_kh input{
			padding: 5px;
		}
		.form_add_th input{
			padding: 5px;
		}
		.form_add_lo input{
			padding: 5px;
		}
		.show_input_add,.show_input_add_main,.show_input_add_main_kh,.show_input_add_main_th,
		.show_input_add_kh,.show_input_add_th,.show_input_add_lo{
			display: none;
		}
		.staff_charge_lo .alert-success,.staff_charge_lo .alert-danger,.staff_charge_th .alert-success,.staff_charge_th .alert-danger,.staff_charge_kh .alert-success,.staff_charge_kh .alert-danger{
			display: none;
		}
		.staff_charge .alert-success,.staff_charge .alert-danger{
			display: none;
		}
		.staff_charge_main .alert-success,.staff_charge_main .alert-danger{
			display: none;
		}
		.addmore_width{
			width: 150px;
		}

		.staff_charge_main_kh .alert-success,.staff_charge_main_kh .alert-danger{
			display: none;
		}
		.staff_charge_main_th .alert-success,.staff_charge_main_th .alert-danger{
			display: none;
		}
	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li>{{trans('label.staff')}}</li><li>{{trans('label.editStaff')}}</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> 
		        {{trans('label.editStaff')}}
		      </h1>
		     </div>
		</div>

		
    	<section id="widget-grid" class="">
    		@include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" aria-label="Close" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> {{ trans('label.validationAlert') }}</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>
		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
		         <h2>{{trans('label.editStaff')}}</h2>    
		         
		        </header>

		        <!-- widget div-->
		        <div>
		         
		         <!-- widget edit box -->
		         <div class="jarviswidget-editbox">
		          <!-- This area used as dropdown edit box -->
		          
		         </div>
		         <!-- end widget edit box -->
		         
		         <!-- widget content -->
		         <div class="widget-body no-padding">            
		              {!! Form::model($staff, ['route' => ['staff.update', $staff->s_id] ,'method' => 'PATCH',  'class' => 'smart-form user-form', 'novalidate' => 'validate', 'id' => 'checkout-form' ,"autocomplete"=>"false"]) !!}
		           <fieldset>

			            <div class="row">
				             <section class="col col-6">
				              {{ Form::label('s_name', trans('label.name'), array('class' => 'label')) }}
				              <label class="input">
				               {!! Form::text("s_name", $value = null, $attributes = array( 'id' => 's_name', 'placeholder'=>trans('label.name'))) !!}
				              </label>
				             </section>
				             <section class="col col-6">
				              {{ Form::label('s_phone', trans('label.phone'), array('class' => 'label')) }}
				              <label class="input"> <i class="icon-append fa fa-mobile"></i>
				               {!! Form::text("s_phone", $value = null, $attributes = array('class' => 'form-control', 'id' => 's_phone', 'placeholder'=>'xxx-xxx-xxxx', 'data-mask'=>'(999) 999-999?9')) !!}
				              </label>
				             </section>
				        </div>
			        	<div class="row">
				             <section class="col col-6">
				              {{ Form::label('s_line', trans('label.line'), array('class' => 'label')) }}
				              <label class="input"> 
				               {!! Form::text("s_line", $value = null, $attributes = array('class' => 'form-control', 'id' => 's_line', 'placeholder'=>trans('label.lineId'))) !!}
				              </label>
				             </section>
				             <section class="col col-6">
				              {{ Form::label('s_fb', trans('label.facebook'), array('class' => 'label')) }}
				              <label class="input"> 
				               {!! Form::text("s_fb", $value = null, $attributes = array('class' => 'form-control', 'id' => 's_fb', 'placeholder'=>trans('label.facebookName'))) !!}
				              </label>
				             </section>
			            </div>
			        	<div class="row">
				             <section class="col col-6">
				              {{ Form::label('s_address', trans('label.address'), array('class' => 'label')) }}
				              <label class="input"> 
				               {!! Form::text("s_address", $value = null, $attributes = array('class' => 'form-control', 'id' => 's_address', 'placeholder'=>trans('label.address'))) !!}
				              </label>
				             </section>
				             <section class="col col-6">
				              {{ Form::label('s_info', trans('label.detail'), array('class' => 'label')) }}
				              <label class="input"> <i class="icon-append fa fa-info"></i>
				               {!! Form::text("s_info", $value = null, $attributes = array('class' => 'form-control', 'id' => 's_info', 'placeholder'=> trans('label.detailOfStaff') )) !!}
				              </label>
				             </section>
			            </div>
			        	<div class="row">
				             <section class="col col-3">
					              {{ Form::label('s_start', trans('label.startDate') , array('class' => 'label')) }}
					              <label class="input"> <i class="icon-append fa fa-calendar"></i>
					               {!! Form::text("s_start", $value = null, $attributes = array( 'id' => 's_start', 'placeholder'=>'YYYY-MM--DD')) !!}
					              </label>
				             </section>
				             <section class="col col-3">
					              {{ Form::label('s_end', trans('label.endDate'), array('class' => 'label')) }}
					              <label class="input"> <i class="icon-append fa fa-calendar"></i>
					               {!! Form::text("s_end", $value = null, $attributes = array( 'id' => 's_end', 'placeholder'=>'YYYY-MM--DD')) !!}
					              </label>
				             </section>
							<section class="col col-6">
								{{ Form::label('parent_id', trans('label.staffSale'), array('class' => 'label')) }}
								<label class="select"> <i class="icon-append fa"></i>
									{{ Form::select('parent_id',(['0' => trans('result.chooseStaff') ]+$staffs) , null, ['class' => 'select2 ','id'=>'parent_id']) }}
								</label>
							</section>
			            </div>
		           	</fieldset>
		           <footer>
		            <button type="submit" name="submit" class="btn btn-primary">{{trans('label.save')}}</button>   
		            <button type="button" class="btn btn-warning" onclick="btnCancel()">{{trans('label.cancel')}}</button>          
		           </footer>

		           <div class="message">
		            <i class="fa fa-check fa-lg"></i>
		            <p>
		             Your comment was successfully added!
		            </p>
		           </div>
		          {{ Form::close() }}
		          
		         </div>
		         <!-- end widget content -->
		         
		        </div>
		        <!-- end widget div -->
		        
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->


		      <article class="col-sm-12 col-md-12 col-lg-12">
		      	
			       	<!-- Widget ID (each widget will need unique ID)-->
			       	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-108" data-widget-editbutton="false">        
				        <header>
				         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				         <h2>{{ trans('label.staffCharge') }} ឈ្មួញកណ្តាល​ ពីកូន VN </h2>
				        </header>
				        <div class="staff_charge_main">
				        	<div class="alert alert-success " style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
				           <div class="alert alert-danger "  style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
					        <div class="widget-body no-padding">          
					          	<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
					           
						           <thead>           
							            <tr>
								             <th>{{ trans('label.id') }}</th>
								             <th>{{ trans('label.date') }}</th>
								             <th>ឈ្មោះកូន</th>
								             <th>{{ trans('label.priceFor2Digits') }}</th>
								             <th>{{ trans('label.priceFor3Digits') }}</th>
								             <th>{{ trans('label.action') }}</th>
							            </tr>
						           </thead>
						           <tbody>
						           		
						           		<tr class="form_add_main smart-form">
								             <td><button id="btn_add_staff_charge_main" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>
								             <td class="show_input_add_main">
								             	<label class="input">
								             		{!! Form::text("stc_date_add_main", $value = null, $attributes = array( 'id' => 'stc_date_add_main', 'placeholder'=>'YYYY-MM-DD', 'class'=>'required', 'sms'=>'សូមបញ្ចូលកាលបរិច្ឆេទ')) !!}
								             	</label>
								             </td>
								             <td class="show_input_add_main">
								             	<label class="select">
												   {{ Form::select('parent_id_main',(['0' => trans('result.chooseStaff') ]+$staffs_main) , null, ['class' => 'select2 addmore_width','id'=>'parent_id_main']) }}
											   </label>
								             </td>
								             
								             <td class="show_input_add_main">
								             	<label class="input">
								             		{!! Form::text("stc_two_digit_charge_add_main", $value = null, $attributes = array( 'id' => 'stc_two_digit_charge_add_main','class'=>'required percentage', 'placeholder'=>trans('label.priceFor2Digits'),'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
								             	</label>

								             <td class="show_input_add_main">
								             	<label class="input">
								             		{!! Form::text("stc_three_digit_charge_add_main", $value = null, $attributes = array( 'id' => 'stc_three_digit_charge_add_main','class'=>'required percentage', 'placeholder'=>trans('label.priceFor3Digits') ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
								             	</label>
								             


								             <td class="show_input_add_main">
								             	<button id="btn_new_staff_charge_main" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
								             </td>
								       	</tr>
								       

						           	<?php $i=0;?>
			           				@foreach($staff_charges_main as $staff_charge_main)
			           					<?php $i++;?>
							            <tr class="staff_charge_main-{{$staff_charge_main->stc_id}}">
								             <td>{{$i}}</td>
								             <td>{{$staff_charge_main->stc_date}}</td>
								             <td>{{$staff_charge_main->s_name}}</td>
								             <td>{{$staff_charge_main->stc_two_digit_charge}}</td>
								             <td>{{$staff_charge_main->stc_three_digit_charge}}</td>
								             <td>
												 @if($paper_date <= $staff_charge_main->stc_date)
								             		 <button id="{{$staff_charge_main->stc_id}}" class="padding-button EditStaffCharge_main btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
								             		 <button id="{{$staff_charge_main->stc_id}}" class="padding-button deleteStaffCharge_main btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
												 @endif
								             </td>
								       	</tr>        
						            @endforeach 
						           </tbody>
					          	</table>
					    
					         </div>
					    </div>
					</div>
		      </article>
		      <!-- end control main  -->

		      <article class="col-sm-12 col-md-12 col-lg-12">
		      	
			       	<!-- Widget ID (each widget will need unique ID)-->
			       	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-109" data-widget-editbutton="false">        
				        <header>
				         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				         <h2>{{ trans('label.staffCharge') }} VN</h2>
				        </header>
				        <div class="staff_charge">
				        	<div class="alert alert-success " style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
				           <div class="alert alert-danger "  style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
					        <div class="widget-body no-padding">          
					          	<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
					           
						           <thead>           
							            <tr>
								             <th>{{ trans('label.id') }}</th>
								             <th>{{ trans('label.date') }}</th>
								             <th>{{ trans('label.priceFor2Digits') }}</th>
								             <th>{{ trans('label.priceFor3Digits') }}</th>
								             <th>លុយសង2ខ្ទង់</th>
								             <th>លុយសង3ខ្ទង់</th>
								             <th>{{ trans('label.action') }}</th>
							            </tr>
						           </thead>
						           <tbody>
						           		
						           		<tr class="form_add smart-form">
								             <td><button id="btn_add_staff_charge" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>

								             <td class="show_input_add">
								             	<label class="input">
								             		{!! Form::text("stc_date_add", $value = null, $attributes = array( 'id' => 'stc_date_add', 'placeholder'=>'YYYY-MM-DD', 'class'=>'required', 'sms'=>'សូមបញ្ចូលកាលបរិច្ឆេទ')) !!}
								             	</label>
								             </td>
								             <td class="show_input_add">
								             	<label class="input">
								             		{!! Form::text("stc_two_digit_charge_add", $value = null, $attributes = array( 'id' => 'stc_two_digit_charge_add','class'=>'required percentage', 'placeholder'=>trans('label.priceFor2Digits'),'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
								             	</label>
								             <td class="show_input_add">
								             	<label class="input">
								             		{!! Form::text("stc_three_digit_charge_add", $value = null, $attributes = array( 'id' => 'stc_three_digit_charge_add','class'=>'required percentage', 'placeholder'=>trans('label.priceFor3Digits') ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
								             	</label>
								             <td class="show_input_add">
								             	<label class="input">
								             		{!! Form::text("stc_pay_two_digit_charge_add", $value = null, $attributes = array( 'id' => 'stc_pay_two_digit_charge_add','class'=>'required num', 'placeholder'=>"លេខ",'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
								             	</label>
								             <td class="show_input_add">
								             	<label class="input">
								             		{!! Form::text("stc_pay_there_digit_charge_add", $value = null, $attributes = array( 'id' => 'stc_pay_there_digit_charge_add','class'=>'required num', 'placeholder'=>"លេខ" ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
								             	</label>

								             <td class="show_input_add">
								             	<button id="btn_new_staff_charge" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
								             </td>
								       	</tr>
								       

						           	<?php $i=0;?>
			           				@foreach($staff_charges as $staff_charge)
			           					<?php $i++;?>
							            <tr class="staff_charge-{{$staff_charge->stc_id}}">
								             <td>{{$i}}</td>
								             <td>{{$staff_charge->stc_date}}</td>
								             <td>{{$staff_charge->stc_two_digit_charge}}</td>
								             <td>{{$staff_charge->stc_three_digit_charge}}</td>
								             <td>{{$staff_charge->stc_pay_two_digit}}</td>
								             <td>{{$staff_charge->stc_pay_there_digit}}</td>
								             <td>
												 @if($paper_date <= $staff_charge->stc_date)
								             		 <button id="{{$staff_charge->stc_id}}" class="padding-button EditStaffCharge btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
								             		 <button id="{{$staff_charge->stc_id}}" class="padding-button deleteStaffCharge btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
												 @endif
								             </td>
								       	</tr>        
						            @endforeach 
						           </tbody>
					          	</table>
					    
					         </div>
					    </div>
					</div>
		     </article>


		     <article class="col-sm-12 col-md-12 col-lg-12">
		      	
			       	<!-- Widget ID (each widget will need unique ID)-->
			       	<div class="jarviswidget jarviswidget-color-red" id="wid-id-110" data-widget-editbutton="false">        
				        <header>
				         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				         <h2>{{ trans('label.staffCharge') }} ឈ្មួញកណ្តាល​ ពីកូន KH </h2>
				        </header>
				        <div class="staff_charge_main_kh">
				        	<div class="alert alert-success " style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
				           <div class="alert alert-danger "  style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
					        <div class="widget-body no-padding">          
					          	<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
					           
						           <thead>           
							            <tr>
								             <th>{{ trans('label.id') }}</th>
								             <th>{{ trans('label.date') }}</th>
								             <th>ឈ្មោះកូន</th>
								             <th>{{ trans('label.priceFor2Digits') }}</th>
								             <th>{{ trans('label.priceFor3Digits') }}</th>
								             <th>{{ trans('label.action') }}</th>
							            </tr>
						           </thead>
						           <tbody>
						           		
						           		<tr class="form_add_main_kh smart-form">
								             <td><button id="btn_add_staff_charge_main_kh" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>
								             <td class="show_input_add_main_kh">
								             	<label class="input">
								             		{!! Form::text("stc_date_add_main_kh", $value = null, $attributes = array( 'id' => 'stc_date_add_main_kh', 'placeholder'=>'YYYY-MM-DD', 'class'=>'required', 'sms'=>'សូមបញ្ចូលកាលបរិច្ឆេទ')) !!}
								             	</label>
								             </td>
								             <td class="show_input_add_main_kh">
								             	<label class="select">
												   {{ Form::select('parent_id_main_kh',(['0' => trans('result.chooseStaff') ]+$staffs_main) , null, ['class' => 'select2 addmore_width','id'=>'parent_id_main_kh']) }}
											   </label>
								             </td>
								             
								             <td class="show_input_add_main_kh">
								             	<label class="input">
								             		{!! Form::text("stc_two_digit_charge_add_main_kh", $value = null, $attributes = array( 'id' => 'stc_two_digit_charge_add_main_kh','class'=>'required percentage', 'placeholder'=>trans('label.priceFor2Digits'),'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
								             	</label>

								             <td class="show_input_add_main_kh">
								             	<label class="input">
								             		{!! Form::text("stc_three_digit_charge_add_main_kh", $value = null, $attributes = array( 'id' => 'stc_three_digit_charge_add_main_kh','class'=>'required percentage', 'placeholder'=>trans('label.priceFor3Digits') ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
								             	</label>
								             


								             <td class="show_input_add_main_kh">
								             	<button id="btn_new_staff_charge_main_kh" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
								             </td>
								       	</tr>
								       

						           	<?php $i=0;?>
			           				@foreach($staff_charges_main_kh as $staff_charge_main)
			           					<?php $i++;?>
							            <tr class="staff_charge_main_kh-{{$staff_charge_main->stc_id}}">
								             <td>{{$i}}</td>
								             <td>{{$staff_charge_main->stc_date}}</td>
								             <td>{{$staff_charge_main->s_name}}</td>
								             <td>{{$staff_charge_main->stc_two_digit_charge}}</td>
								             <td>{{$staff_charge_main->stc_three_digit_charge}}</td>
								             <td>
												 @if($paper_date <= $staff_charge_main->stc_date)
								             		 <button id="{{$staff_charge_main->stc_id}}" class="padding-button EditStaffCharge_main_kh btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
								             		 <button id="{{$staff_charge_main->stc_id}}" class="padding-button deleteStaffCharge_main_kh btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
												 @endif
								             </td>
								       	</tr>        
						            @endforeach 
						           </tbody>
					          	</table>
					    
					         </div>
					    </div>
					</div>
		      </article>
		      <!-- end control main  -->




			 <article class="col-sm-12 col-md-12 col-lg-12">

				 <!-- Widget ID (each widget will need unique ID)-->
				 <div class="jarviswidget jarviswidget-color-red" id="wid-id-111" data-widget-editbutton="false">
					 <header>
						 <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
						 <h2>{{ trans('label.staffCharge') }} KH</h2>
					 </header>
					 <div class="staff_charge_kh">
						 <div class="alert alert-success " style="margin-top:10px;">
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
							 <span aria-hidden="true"></span>
						 </div>
						 <div class="alert alert-danger "  style="margin-top:10px;">
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
							 <span aria-hidden="true"></span>
						 </div>
						 <div class="widget-body no-padding">
							 <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">

								 <thead>
								 <tr>
									 <th>{{ trans('label.id') }}</th>
									 <th>{{ trans('label.date') }}</th>
									 <th>{{ trans('label.priceFor2Digits') }}</th>
									 <th>{{ trans('label.priceFor3Digits') }}</th>
									 <th>លុយសង2ខ្ទង់</th>
								     <th>លុយសង3ខ្ទង់</th>
									 <th>{{ trans('label.action') }}</th>
								 </tr>
								 </thead>
								 <tbody>

								 <tr class="form_add_kh smart-form">
									 <td><button id="btn_add_staff_charge_kh" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>

									 <td class="show_input_add_kh">
										 <label class="input">
											 {!! Form::text("stc_date_add_kh", $value = null, $attributes = array( 'id' => 'stc_date_add_kh', 'placeholder'=>'YYYY-MM-DD', 'class'=>'required', 'sms'=>'សូមបញ្ចូលកាលបរិច្ឆេទ')) !!}
										 </label>
									 </td>
									 <td class="show_input_add_kh">
										 <label class="input">
										 {!! Form::text("stc_two_digit_charge_add_kh", $value = null, $attributes = array( 'id' => 'stc_two_digit_charge_add_kh','class'=>'required percentage', 'placeholder'=>trans('label.priceFor2Digits'),'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
									 </label>
									 <td class="show_input_add_kh">
										 <label class="input">
										 {!! Form::text("stc_three_digit_charge_add_kh", $value = null, $attributes = array( 'id' => 'stc_three_digit_charge_add_kh','class'=>'required percentage', 'placeholder'=>trans('label.priceFor3Digits') ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
									 </label>
									 <td class="show_input_add_kh">
						             	<label class="input">
						             		{!! Form::text("stc_pay_two_digit_charge_add_kh", $value = null, $attributes = array( 'id' => 'stc_pay_two_digit_charge_add_kh','class'=>'required num', 'placeholder'=>"លេខ",'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
						             	</label>
						             <td class="show_input_add_kh">
						             	<label class="input">
						             		{!! Form::text("stc_pay_there_digit_charge_add_kh", $value = null, $attributes = array( 'id' => 'stc_pay_there_digit_charge_add_kh','class'=>'required num', 'placeholder'=>"លេខ" ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
						             	</label>

									 <td class="show_input_add_kh">
										 <button id="btn_new_staff_charge_kh" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
									 </td>
								 </tr>


								 <?php $i=0;?>
								 @foreach($staff_charges_kh as $staff_charge)
									 <?php $i++;?>
									 <tr class="staff_charge-{{$staff_charge->stc_id}}">
										 <td>{{$i}}</td>
										 <td>{{$staff_charge->stc_date}}</td>
										 <td>{{$staff_charge->stc_two_digit_charge}}</td>
										 <td>{{$staff_charge->stc_three_digit_charge}}</td>
										 <td>{{$staff_charge->stc_pay_two_digit}}</td>
										 <td>{{$staff_charge->stc_pay_there_digit}}</td>
										 <td>
											 @if($paper_date <= $staff_charge->stc_date)
												 <button id="{{$staff_charge->stc_id}}" class="padding-button EditStaffCharge_kh btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
												 <button id="{{$staff_charge->stc_id}}" class="padding-button deleteStaffCharge_kh btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
											 @endif
										 </td>
									 </tr>
								 @endforeach
								 </tbody>
							 </table>

						 </div>
					 </div>
				 </div>

			 </article>

			 <!-- thailand -->
			 <article class="col-sm-12 col-md-12 col-lg-12">
		      	
			       	<!-- Widget ID (each widget will need unique ID)-->
			       	<div class="jarviswidget jarviswidget-color-blue" id="wid-id-112" data-widget-editbutton="false">        
				        <header>
				         <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				         <h2>{{ trans('label.staffCharge') }} ឈ្មួញកណ្តាល​ ពីកូន TH </h2>
				        </header>
				        <div class="staff_charge_main_th">
				        	<div class="alert alert-success " style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
				           <div class="alert alert-danger "  style="margin-top:10px;">
				               <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
				               <span aria-hidden="true"></span>
				           </div>
					        <div class="widget-body no-padding">          
					          	<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
					           
						           <thead>           
							            <tr>
								             <th>{{ trans('label.id') }}</th>
								             <th>{{ trans('label.date') }}</th>
								             <th>ឈ្មោះកូន</th>
								             <th>{{ trans('label.priceFor2Digits') }}</th>
								             <th>{{ trans('label.priceFor3Digits') }}</th>
								             <th>{{ trans('label.action') }}</th>
							            </tr>
						           </thead>
						           <tbody>
						           		
						           		<tr class="form_add_main_th smart-form">
								             <td><button id="btn_add_staff_charge_main_th" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>
								             <td class="show_input_add_main_th">
								             	<label class="input">
								             		{!! Form::text("stc_date_add_main_th", $value = null, $attributes = array( 'id' => 'stc_date_add_main_th', 'placeholder'=>'YYYY-MM-DD', 'class'=>'required', 'sms'=>'សូមបញ្ចូលកាលបរិច្ឆេទ')) !!}
								             	</label>
								             </td>
								             <td class="show_input_add_main_th">
								             	<label class="select">
												   {{ Form::select('parent_id_main_th',(['0' => trans('result.chooseStaff') ]+$staffs_main) , null, ['class' => 'select2 addmore_width','id'=>'parent_id_main_th']) }}
											   </label>
								             </td>
								             
								             <td class="show_input_add_main_th">
								             	<label class="input">
								             		{!! Form::text("stc_two_digit_charge_add_main_th", $value = null, $attributes = array( 'id' => 'stc_two_digit_charge_add_main_th','class'=>'required percentage', 'placeholder'=>trans('label.priceFor2Digits'),'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
								             	</label>

								             <td class="show_input_add_main_th">
								             	<label class="input">
								             		{!! Form::text("stc_three_digit_charge_add_main_th", $value = null, $attributes = array( 'id' => 'stc_three_digit_charge_add_main_th','class'=>'required percentage', 'placeholder'=>trans('label.priceFor3Digits') ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
								             	</label>
								             


								             <td class="show_input_add_main_th">
								             	<button id="btn_new_staff_charge_main_th" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
								             </td>
								       	</tr>
								       

						           	<?php $i=0;?>
			           				@foreach($staff_charges_main_th as $staff_charge_main_th)
			           					<?php $i++;?>
							            <tr class="staff_charge_main-{{$staff_charge_main_th->stc_id}}">
								             <td>{{$i}}</td>
								             <td>{{$staff_charge_main_th->stc_date}}</td>
								             <td>{{$staff_charge_main_th->s_name}}</td>
								             <td>{{$staff_charge_main_th->stc_two_digit_charge}}</td>
								             <td>{{$staff_charge_main_th->stc_three_digit_charge}}</td>
								             <td>
												 @if($paper_date <= $staff_charge_main_th->stc_date)
								             		 <button id="{{$staff_charge_main_th->stc_id}}" class="padding-button EditStaffCharge_main_th btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
								             		 <button id="{{$staff_charge_main_th->stc_id}}" class="padding-button deleteStaffCharge_main btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
												 @endif
								             </td>
								       	</tr>        
						            @endforeach 
						           </tbody>
					          	</table>
					    
					         </div>
					    </div>
					</div>
		      </article>
		      <!-- end control main  -->


			 <article class="col-sm-12 col-md-12 col-lg-12">

				 <!-- Widget ID (each widget will need unique ID)-->
				 <div class="jarviswidget jarviswidget-color-blue" id="wid-id-113" data-widget-editbutton="false">
					 <header>
						 <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
						 <h2>{{ trans('label.staffCharge') }} TH</h2>
					 </header>
					 <div class="staff_charge_th">
						 <div class="alert alert-success " style="margin-top:10px;">
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
							 <span aria-hidden="true"></span>
						 </div>
						 <div class="alert alert-danger "  style="margin-top:10px;">
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
							 <span aria-hidden="true"></span>
						 </div>
						 <div class="widget-body no-padding">
							 <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">

								 <thead>
								 <tr>
									 <th>{{ trans('label.id') }}</th>
									 <th>{{ trans('label.date') }}</th>
									 <th>{{ trans('label.priceFor2Digits') }}</th>
									 <th>{{ trans('label.priceFor3Digits') }}</th>
									 <th>លុយសង2ខ្ទង់</th>
								     <th>លុយសង3ខ្ទង់</th>
									 <th>{{ trans('label.action') }}</th>
								 </tr>
								 </thead>
								 <tbody>

								 <tr class="form_add_th smart-form">
									 <td><button id="btn_add_staff_charge_th" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>

									 <td class="show_input_add_th">
										 <label class="input">
											 {!! Form::text("stc_date_add_th", $value = null, $attributes = array( 'id' => 'stc_date_add_th', 'placeholder'=>'YYYY-MM-DD', 'class'=>'required', 'sms'=>'សូមបញ្ចូលកាលបរិច្ឆេទ')) !!}
										 </label>
									 </td>
									 <td class="show_input_add_th">
										 <label class="input">
										 {!! Form::text("stc_two_digit_charge_add_th", $value = null, $attributes = array( 'id' => 'stc_two_digit_charge_add_th','class'=>'required percentage', 'placeholder'=>trans('label.priceFor2Digits'),'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
									 </label>
									 <td class="show_input_add_th">
										 <label class="input">
										 {!! Form::text("stc_three_digit_charge_add_th", $value = null, $attributes = array( 'id' => 'stc_three_digit_charge_add_th','class'=>'required percentage', 'placeholder'=>trans('label.priceFor3Digits') ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
									 </label>
									 <td class="show_input_add_th">
						             	<label class="input">
						             		{!! Form::text("stc_pay_two_digit_charge_add_th", $value = null, $attributes = array( 'id' => 'stc_pay_two_digit_charge_add_th','class'=>'required percentage', 'placeholder'=>"លេខ",'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
						             	</label>
						             <td class="show_input_add_th">
						             	<label class="input">
						             		{!! Form::text("stc_pay_there_digit_charge_add_th", $value = null, $attributes = array( 'id' => 'stc_pay_there_digit_charge_add_th','class'=>'required percentage', 'placeholder'=>"លេខ" ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
						             	</label>

									 <td class="show_input_add_th">
										 <button id="btn_new_staff_charge_th" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
									 </td>
								 </tr>


								 <?php $i=0;?>
								 @foreach($staff_charges_th as $staff_charge)
									 <?php $i++;?>
									 <tr class="staff_charge-{{$staff_charge->stc_id}}">
										 <td>{{$i}}</td>
										 <td>{{$staff_charge->stc_date}}</td>
										 <td>{{$staff_charge->stc_two_digit_charge}}</td>
										 <td>{{$staff_charge->stc_three_digit_charge}}</td>
										 <td>{{$staff_charge->stc_pay_two_digit}}</td>
										 <td>{{$staff_charge->stc_pay_there_digit}}</td>
										 <td>
											 @if($paper_date <= $staff_charge->stc_date)
												 <button id="{{$staff_charge->stc_id}}" class="padding-button EditStaffCharge_th btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
												 <button id="{{$staff_charge->stc_id}}" class="padding-button deleteStaffCharge_th btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
											 @endif
										 </td>
									 </tr>
								 @endforeach
								 </tbody>
							 </table>

						 </div>
					 </div>
				 </div>

			 </article>

			 <!-- វត្តុ -->
			 <article class="col-sm-12 col-md-12 col-lg-12">

				 <!-- Widget ID (each widget will need unique ID)-->
				 <div class="jarviswidget jarviswidget-color-green" id="wid-id-114" data-widget-editbutton="false">
					 <header>
						 <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
						 <h2>{{ trans('label.staffCharge') }} វត្តុ</h2>
					 </header>
					 <div class="staff_charge_lo">
						 <div class="alert alert-success " style="margin-top:10px;">
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
							 <span aria-hidden="true"></span>
						 </div>
						 <div class="alert alert-danger "  style="margin-top:10px;">
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
							 <span aria-hidden="true"></span>
						 </div>
						 <div class="widget-body no-padding">
							 <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">

								 <thead>
								 <tr>
									 <th>{{ trans('label.id') }}</th>
									 <th>{{ trans('label.date') }}</th>
									 <th>{{ trans('label.priceFor2Digits') }}</th>
									 <th>{{ trans('label.priceFor3Digits') }}</th>
									 <th>លុយសង2ខ្ទង់</th>
								     <th>លុយសង3ខ្ទង់</th>
									 <th>{{ trans('label.action') }}</th>
								 </tr>
								 </thead>
								 <tbody>

								 <tr class="form_add_lo smart-form">
									 <td><button id="btn_add_staff_charge_lo" class="padding-button btn btn-xs btn-primary">{{ trans('label.add') }}</button></td>

									 <td class="show_input_add_lo">
										 <label class="input">
											 {!! Form::text("stc_date_add_lo", $value = null, $attributes = array( 'id' => 'stc_date_add_lo', 'placeholder'=>'YYYY-MM-DD', 'class'=>'required', 'sms'=>'សូមបញ្ចូលកាលបរិច្ឆេទ')) !!}
										 </label>
									 </td>
									 <td class="show_input_add_lo">
										 <label class="input">
										 {!! Form::text("stc_two_digit_charge_add_lo", $value = null, $attributes = array( 'id' => 'stc_two_digit_charge_add_lo','class'=>'required num', 'placeholder'=>trans('label.priceFor2Digits'),'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
									 </label>
									 <td class="show_input_add_lo">
										 <label class="input">
										 {!! Form::text("stc_three_digit_charge_add_lo", $value = null, $attributes = array( 'id' => 'stc_three_digit_charge_add_lo','class'=>'required num', 'placeholder'=>trans('label.priceFor3Digits') ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
									 </label>
									 <td class="show_input_add_lo">
						             	<label class="input">
						             		{!! Form::text("stc_pay_two_digit_charge_add_lo", $value = null, $attributes = array( 'id' => 'stc_pay_two_digit_charge_add_lo','class'=>'required num', 'placeholder'=>"លេខ",'sms'=>trans('validation.custom.required.price2digits'), 'number'=>'true')) !!}</td>
						             	</label>
						             <td class="show_input_add_lo">
						             	<label class="input">
						             		{!! Form::text("stc_pay_there_digit_charge_add_lo", $value = null, $attributes = array( 'id' => 'stc_pay_there_digit_charge_add_lo','class'=>'required num', 'placeholder'=>"លេខ" ,'sms'=>trans('validation.custom.required.price3digits'), 'number'=>'true')) !!}</td>
						             	</label>

									 <td class="show_input_add_lo">
										 <button id="btn_new_staff_charge_lo" type="add" idData='' class="padding-button btn btn-xs btn-success">{{trans('label.save')}}</button>
									 </td>
								 </tr>


								 <?php $i=0;?>
								 @foreach($staff_charges_lo as $staff_charge)
									 <?php $i++;?>
									 <tr class="staff_charge-{{$staff_charge->stc_id}}">
										 <td>{{$i}}</td>
										 <td>{{$staff_charge->stc_date}}</td>
										 <td>{{$staff_charge->stc_two_digit_charge}}</td>
										 <td>{{$staff_charge->stc_three_digit_charge}}</td>
										 <td>{{$staff_charge->stc_pay_two_digit}}</td>
										 <td>{{$staff_charge->stc_pay_there_digit}}</td>
										 <td>
											 @if($paper_date <= $staff_charge->stc_date)
												 <button id="{{$staff_charge->stc_id}}" class="padding-button EditStaffCharge_lo btn btn-xs btn-primary">{{ trans('label.edit') }}</button>
												 <button id="{{$staff_charge->stc_id}}" class="padding-button deleteStaffCharge_lo btn btn-xs btn-danger">{{ trans('label.delete') }}</button>
											 @endif
										 </td>
									 </tr>
								 @endforeach
								 </tbody>
							 </table>

						 </div>
					 </div>
				 </div>

			 </article>

		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script src="{{ asset('/') }}js/plugin/jquery-form/jquery-form.min.js"></script>
	<script type="text/javascript">
	
		function validate_form_main(class_div){
			var num = 0;
			$(class_div + " .required").each(function(index, element) {
	          var id, info, value, number;
	          value = $(element).val();
	          id = $(element).attr('id');
	          number = $(element).attr('number');
	          info = $(element).attr('sms');

	          // pattern_number = /^\d+$/;
	          pattern_number = /^(?:[1-9]\d*|0)?(?:\.\d+)?$/;

	          // alert(id);
	          if (value === '') { //check value empty
	          	validate_by_vannarith(id,info,'add');
	          	num = num + 1;
	          }else if(number === 'true' && !pattern_number.test(value)){ //check value number only
	          	 validate_by_vannarith(id,'សូមបញ្ចូលតំលៃជាលេខ','add');
	          		num = num + 1;
	          }else{ //else value good data
	          	validate_by_vannarith(id,'','remove');
	          }
	        });
	        return num;
		}


		$(document).ready(function() {
		   pageSetUp();

		   var $checkoutForm = $('#checkout-form').validate({
		   // Rules for form validation
		    rules : {
		     
		     s_name : {
		      required : true
		     },
		     s_phone : {
		      required : true
		     },
		     s_start : {
		      required : true
		     }
		    },
		  
		    // Messages for form validation
		    messages : {
		     s_name : {
		      required : 'សូមបញ្ចូលឈ្មោះថ្មី'
		     },
		     s_phone : {
		      required : 'សូមបញ្ចូលលេខទូរស័ព្ទ'
		     },
		     s_start : {
		      required : 'សូមបញ្ចូលកាលបរិច្ឆេទចាប់ផ្តើម'
		     }
		    },
		  
		    // Do not change code below
		    errorPlacement : function(error, element) {
		     error.insertAfter(element.parent());
		    }
		   });

		   

		   // btn controll show hide form insert data
		   $(document).off('click', '#btn_add_staff_charge_main').on('click', '#btn_add_staff_charge_main', function(e){
				
				if($('.show_input_add_main').css('display') == 'none'){
					$('#stc_date_add_main').val('');
					$('#stc_two_digit_charge_add_main').val('');
					$('#stc_three_digit_charge_add_main').val('');
					$('.show_input_add_main').show();
					$("#btn_new_staff_charge_main").attr( "type", "add" );
					$("#btn_new_staff_charge_main").attr( "idData", '' );
					$(this).text("{{ trans('label.delete') }}");
					$('#stc_currency_add_main').val($('#stc_currency_add_main option:first-child').val()).trigger('change');
				}else{
					$('#stc_date_add_main').val('');
					$('#stc_two_digit_charge_add_main').val('');
					$('#stc_three_digit_charge_add_main').val('');
					$('.show_input_add_main').hide();
					$(this).text("{{ trans('label.add') }}");
				}

			});


		   $(document).off('click', '#btn_add_staff_charge_main_kh').on('click', '#btn_add_staff_charge_main_kh', function(e){
				
				if($('.show_input_add_main_kh').css('display') == 'none'){
					$('#stc_date_add_main_kh').val('');
					$('#stc_two_digit_charge_add_main_kh').val('');
					$('#stc_three_digit_charge_add_main_kh').val('');
					$('.show_input_add_main_kh').show();
					$("#btn_new_staff_charge_main_kh").attr( "type", "add" );
					$("#btn_new_staff_charge_main_kh").attr( "idData", '' );
					$(this).text("{{ trans('label.delete') }}");
					$('#stc_currency_add_main_kh').val($('#stc_currency_add_main_kh option:first-child').val()).trigger('change');
				}else{
					$('#stc_date_add_main_kh').val('');
					$('#stc_two_digit_charge_add_main_kh').val('');
					$('#stc_three_digit_charge_add_main_kh').val('');
					$('.show_input_add_main_kh').hide();
					$(this).text("{{ trans('label.add') }}");
				}

			});

		   $(document).off('click', '#btn_add_staff_charge_main_th').on('click', '#btn_add_staff_charge_main_th', function(e){
				
				if($('.show_input_add_main_th').css('display') == 'none'){
					$('#stc_date_add_main_th').val('');
					$('#stc_two_digit_charge_add_main_th').val('');
					$('#stc_three_digit_charge_add_main_th').val('');
					$('.show_input_add_main_th').show();
					$("#btn_new_staff_charge_main_th").attr( "type", "add" );
					$("#btn_new_staff_charge_main_th").attr( "idData", '' );
					$(this).text("{{ trans('label.delete') }}");
					$('#stc_currency_add_main_th').val($('#stc_currency_add_main_th option:first-child').val()).trigger('change');
				}else{
					$('#stc_date_add_main_th').val('');
					$('#stc_two_digit_charge_add_main_th').val('');
					$('#stc_three_digit_charge_add_main_th').val('');
					$('.show_input_add_main_th').hide();
					$(this).text("{{ trans('label.add') }}");
				}

			});

		   // btn controll show hide form insert data
		   $(document).off('click', '#btn_add_staff_charge').on('click', '#btn_add_staff_charge', function(e){
				
				if($('.show_input_add').css('display') == 'none'){
					$('#stc_date_add').val('');
					$('#stc_two_digit_charge_add').val('');
					$('#stc_three_digit_charge_add').val('');
					$('.show_input_add').show();
					$("#btn_new_staff_charge").attr( "type", "add" );
					$("#btn_new_staff_charge").attr( "idData", '' );
					$(this).text("{{ trans('label.delete') }}");
					$('#stc_currency_add').val($('#stc_currency_add option:first-child').val()).trigger('change');
				}else{
					$('#stc_date_add').val('');
					$('#stc_two_digit_charge_add').val('');
					$('#stc_three_digit_charge_add').val('');
					$('.show_input_add').hide();
					$(this).text("{{ trans('label.add') }}");
				}

			});

		   // insert data to database
		   $(document).off('click', '#btn_new_staff_charge').on('click', '#btn_new_staff_charge', function(e){
				$(".staff_charge .alert-success").hide();
				$(".staff_charge .alert-danger").hide();
				// alert("ok");
				if(validate_form_main('.form_add') == 0){
					var stc_date_add = $("#stc_date_add").val();
					var stc_two_digit_charge_add = $("#stc_two_digit_charge_add").val();
					var stc_three_digit_charge_add = $("#stc_three_digit_charge_add").val();

					var stc_pay_two_digit_charge_add = $("#stc_pay_two_digit_charge_add").val();
					var stc_pay_there_digit_charge_add = $("#stc_pay_there_digit_charge_add").val();
					// alert(stc_two_digit_charge_add);

					var staff_id = '{{ $staff->s_id }}';
					var checkType = $(this).attr('type');
					var iddata = $(this).attr('iddata');
					var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
					var stc_type = 1;
					$.ajax({
			              url: '{{URL::to('/')}}/storeCharge',
			              type: 'GET',
			              data: {stc_date_add:stc_date_add,
			              		 stc_two_digit_charge_add:stc_two_digit_charge_add,
			              		 stc_three_digit_charge_add:stc_three_digit_charge_add,
			              		 stc_pay_two_digit_charge_add:stc_pay_two_digit_charge_add,
			              		 stc_pay_there_digit_charge_add:stc_pay_there_digit_charge_add,
			              		 staff_id:staff_id,
			              		 checkType:checkType,
			              		 iddata:iddata,
			              		 idDataTable:idDataTable,
							     stc_type:stc_type
			              },
			              success: function(data) {
			                if(data.status=="success"){
			                  $('tr.form_add.smart-form').after(data.msg);
			                  $(".staff_charge .alert-success").show();
			                  $(".staff_charge .alert-success").find('span').text("{{trans('message.add_success')}}");
			                  $(".staff_charge input").val("");
			                  $("#btn_add_staff_charge").text("{{trans('label.delete')}}");
			                  $("#btn_new_staff_charge").attr('type','add').attr('iddata');
			                }else if(data.status=="updatesuccess"){
			                	$(".staff_charge .alert-success").show();
			                  	$(".staff_charge .alert-success").find('span').text("{{trans('message.update_success')}}");
			                  	$(".staff_charge input").val("");
			                  	$(".staff_charge-"+iddata).html(data.msg);
			                  	$("#btn_add_staff_charge").text("{{trans('label.delete')}}");
			                  	$("#btn_new_staff_charge").attr('type','add').attr('iddata','');
			                }else{
			                	$(".staff_charge .alert-danger").show();
			                  	$(".staff_charge .alert-danger").find('span').text(data.msg);
			                	// alert();
			                  // callPopupLogin();
			                }        
			              }
			        });
				}
				
			});

			$(document).off('click', '.EditStaffCharge').on('click', '.EditStaffCharge', function(e){
				var id = $(this).attr('id');
				$.ajax({
		              url: '{{URL::to('/')}}/getCharge',
		              type: 'GET',
		              data: {id:id},
		              success: function(data) {
		                if(data.status=="success"){

		                	$("#btn_add_staff_charge").text("{{trans('label.delete')}}");
		                	$("#stc_date_add").val(data.msg.stc_date);
							$("#stc_two_digit_charge_add").val(data.msg.stc_two_digit_charge);
							$("#stc_three_digit_charge_add").val(data.msg.stc_three_digit_charge);

							$("#stc_pay_two_digit_charge_add").val(data.msg.stc_pay_two_digit);
							$("#stc_pay_there_digit_charge_add").val(data.msg.stc_pay_there_digit);

							$("#stc_currency_add").val(data.msg.stc_currency).trigger("change");
							$('.show_input_add').show();
							$("#btn_new_staff_charge").attr( "type", "modify" );
							$("#btn_new_staff_charge").attr( "idData", data.msg.stc_id );
							
		                }else{
		                	$("#btn_add_staff_charge").text("{{trans('label.delete')}}");
		                	$("#btn_new_staff_charge").attr( "type", "add" );
		                	$("#btn_new_staff_charge").attr( "idData", '' );
		                	$('.show_input_add').hide();
		                  // callPopupLogin();
		                }        
		              }
		        });
			});

			$(document).off('click', '.deleteStaffCharge').on('click', '.deleteStaffCharge', function(e){
				
				var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
	    		if (r == true) {
	    			var id = $(this).attr('id');
	    			$.ajax({
			              url: '{{action("StaffController@deleteStaffCharge",[""])}}',
			              type: 'GET',
			              data: {id:id},
			              success: function(data) {
			                if(data.status=="success"){
			                  $(".staff_charge-"+id).remove();
			                }else{
			                	alert(data.msg);
			                }        
			              }
			        });
	    		}
			});
			

            // btn controll show hide form insert data Khmer
            $(document).off('click', '#btn_add_staff_charge_kh').on('click', '#btn_add_staff_charge_kh', function(e){

                if($('.show_input_add_kh').css('display') == 'none'){
                    $('#stc_date_add_kh').val('');
                    $('#stc_two_digit_charge_add_kh').val('');
                    $('#stc_three_digit_charge_add_kh').val('');
                    $('.show_input_add_kh').show();
                    $("#btn_new_staff_charge_kh").attr( "type", "add" );
                    $("#btn_new_staff_charge_kh").attr( "idData", '' );
                    $(this).text("{{ trans('label.delete') }}");
                    $('#stc_currency_add_kh').val($('#stc_currency_add_kh option:first-child').val()).trigger('change');
                }else{
                    $('#stc_date_add_kh').val('');
                    $('#stc_two_digit_charge_add_kh').val('');
                    $('#stc_three_digit_charge_add_kh').val('');
                    $('.show_input_add_kh').hide();
                    $(this).text("{{ trans('label.add') }}");
                }

            });

            // insert data to database KH
            $(document).off('click', '#btn_new_staff_charge_kh').on('click', '#btn_new_staff_charge_kh', function(e){
                $(".staff_charge_kh .alert-success").hide();
                $(".staff_charge_kh .alert-danger").hide();
                if(validate_form_main('.form_add_kh') == 0){
                    var stc_date_add = $("#stc_date_add_kh").val();
                    var stc_two_digit_charge_add = $("#stc_two_digit_charge_add_kh").val();
                    var stc_three_digit_charge_add = $("#stc_three_digit_charge_add_kh").val();

                    var stc_pay_two_digit_charge_add_kh = $("#stc_pay_two_digit_charge_add_kh").val();
                    var stc_pay_there_digit_charge_add_kh = $("#stc_pay_there_digit_charge_add_kh").val();

                    var staff_id = '{{ $staff->s_id }}';
                    var checkType = $(this).attr('type');
                    var iddata = $(this).attr('iddata');
                    var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
                    var stc_type = 2;
                    $.ajax({
                        url: '{{URL::to('/')}}/storeCharge',
                        type: 'GET',
                        data: {stc_date_add:stc_date_add,
                            stc_two_digit_charge_add:stc_two_digit_charge_add,
                            stc_three_digit_charge_add:stc_three_digit_charge_add,
                            stc_pay_two_digit_charge_add:stc_pay_two_digit_charge_add_kh,
                            stc_pay_there_digit_charge_add:stc_pay_there_digit_charge_add_kh,
                            staff_id:staff_id,
                            checkType:checkType,
                            iddata:iddata,
                            idDataTable:idDataTable,
                            stc_type:stc_type
                        },
                        success: function(data) {
                            if(data.status=="success"){
                                $('tr.form_add_kh.smart-form').after(data.msg);
                                $(".staff_charge_kh .alert-success").show();
                                $(".staff_charge_kh .alert-success").find('span').text("{{trans('message.add_success')}}");
                                $(".staff_charge_kh input").val("");
                                $("#btn_add_staff_charge_kh").text("{{trans('label.delete')}}");
                                $("#btn_new_staff_charge_kh").attr('type','add').attr('iddata');
                            }else if(data.status=="updatesuccess"){
                                $(".staff_charge_kh .alert-success").show();
                                $(".staff_charge_kh .alert-success").find('span').text("{{trans('message.update_success')}}");
                                $(".staff_charge_kh input").val("");
                                $(".staff_charge-"+iddata).html(data.msg);
                                $("#btn_add_staff_charge_kh").text("{{trans('label.delete')}}");
                                $("#btn_new_staff_charge_kh").attr('type','add').attr('iddata','');
                            }else{
                                $(".staff_charge_kh .alert-danger").show();
                                $(".staff_charge_kh .alert-danger").find('span').text(data.msg);
                                // alert();
                                // callPopupLogin();
                            }
                        }
                    });
                }

            });

            $(document).off('click', '.EditStaffCharge_kh').on('click', '.EditStaffCharge_kh', function(e){
                var id = $(this).attr('id');
                $.ajax({
                    url: '{{URL::to('/')}}/getCharge',
                    type: 'GET',
                    data: {id:id},
                    success: function(data) {
                        if(data.status=="success"){

                            $("#btn_add_staff_charge_kh").text("{{trans('label.delete')}}");
                            $("#stc_date_add_kh").val(data.msg.stc_date);
                            $("#stc_two_digit_charge_add_kh").val(data.msg.stc_two_digit_charge);
                            $("#stc_three_digit_charge_add_kh").val(data.msg.stc_three_digit_charge);

                            $("#stc_pay_two_digit_charge_add_kh").val(data.msg.stc_pay_two_digit);
							$("#stc_pay_there_digit_charge_add_kh").val(data.msg.stc_pay_there_digit);

                            $("#stc_currency_add_kh").val(data.msg.stc_currency).trigger("change");
                            $('.show_input_add_kh').show();
                            $("#btn_new_staff_charge_kh").attr( "type", "modify" );
                            $("#btn_new_staff_charge_kh").attr( "idData", data.msg.stc_id );

                        }else{
                            $("#btn_add_staff_charge_kh").text("{{trans('label.delete')}}");
                            $("#btn_new_staff_charge_kh").attr( "type", "add" );
                            $("#btn_new_staff_charge_kh").attr( "idData", '' );
                            $('.show_input_add_kh').hide();
                            // callPopupLogin();
                        }
                    }
                });
            });

            $(document).off('click', '.deleteStaffCharge_kh').on('click', '.deleteStaffCharge_kh', function(e){

                var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
                if (r == true) {
                    var id = $(this).attr('id');
                    $.ajax({
                        url: '{{action("StaffController@deleteStaffCharge",[""])}}',
                        type: 'GET',
                        data: {id:id},
                        success: function(data) {
                            if(data.status=="success"){
                                $(".staff_charge-"+id).remove();
                            }else{
                                alert(data.msg);
                            }
                        }
                    });
                }
            });
		   


       		// new code main
            // insert data to database
		   $(document).off('click', '#btn_new_staff_charge_main').on('click', '#btn_new_staff_charge_main', function(e){
				$(".staff_charge_edit .alert-success").hide();
				$(".staff_charge_edit .alert-danger").hide();
				if(validate_form_main('.form_add_main') == 0){
					var stc_date_add = $("#stc_date_add_main").val();
					var stc_two_digit_charge_add = $("#stc_two_digit_charge_add_main").val();
					var stc_three_digit_charge_add = $("#stc_three_digit_charge_add_main").val();
					var parent_id_main = $("#parent_id_main").val();

					var staff_id = '{{ $staff->s_id }}';
					var checkType = $(this).attr('type');
					var iddata = $(this).attr('iddata');
					var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
					var stc_type = 1;
					// alert(parent_id_main);
					// return false;
					$.ajax({
			              url: '{{URL::to('/')}}/storeChargeMain',
			              type: 'GET',
			              data: {stc_date_add:stc_date_add,
			              		 stc_two_digit_charge_add:stc_two_digit_charge_add,
			              		 stc_three_digit_charge_add:stc_three_digit_charge_add,
			              		 parent_id_main:parent_id_main,
			              		 staff_id:staff_id,
			              		 checkType:checkType,
			              		 iddata:iddata,
			              		 idDataTable:idDataTable,
							     stc_type:stc_type
			              },
			              success: function(data) {
			                if(data.status=="success"){
			                  $('tr.form_add_main.smart-form').after(data.msg);
			                  $(".staff_charge_main .alert-success").show();
			                  $(".staff_charge_main .alert-success").find('span').text("{{trans('message.add_success')}}");
			                  $(".staff_charge_main input").val("");
			                  $("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
			                  $("#btn_new_staff_charge_main").attr('type','add').attr('iddata');
			                }else if(data.status=="updatesuccess"){
			                	$(".staff_charge_main .alert-success").show();
			                  	$(".staff_charge_main .alert-success").find('span').text("{{trans('message.update_success')}}");
			                  	$(".staff_charge_main input").val("");
			                  	$(".staff_charge_main-"+iddata).html(data.msg);
			                  	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
			                  	$("#btn_new_staff_charge_main").attr('type','add').attr('iddata','');
			                }else{
			                	$(".staff_charge_main .alert-danger").show();
			                  	$(".staff_charge_main .alert-danger").find('span').text(data.msg);
			                	// alert();
			                  // callPopupLogin();
			                }        
			              }
			        });
				}
				
			});

			$(document).off('click', '.EditStaffCharge_main').on('click', '.EditStaffCharge_main', function(e){
				var id = $(this).attr('id');
				var table_main = 'tbl_staff_charge_main';
				$.ajax({
		              url: '{{URL::to('/')}}/getCharge',
		              type: 'GET',
		              data: {id:id,table_main:table_main},
		              success: function(data) {
		                if(data.status=="success"){

		                	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
		                	$("#stc_date_add_main").val(data.msg.stc_date);
							$("#stc_two_digit_charge_add_main").val(data.msg.stc_two_digit_charge);
							$("#stc_three_digit_charge_add_main").val(data.msg.stc_three_digit_charge);
							// alert(data.msg.chail_id);
							// $("#parent_id_main").val(data.msg.chail_id);
							$("#parent_id_main").select2().select2('val',data.msg.chail_id);

							$("#stc_currency_add_main").val(data.msg.stc_currency).trigger("change");
							$('.show_input_add_main').show();
							$("#btn_new_staff_charge_main").attr( "type", "modify" );
							$("#btn_new_staff_charge_main").attr( "idData", data.msg.stc_id );
							
		                }else{
		                	$("#btn_add_staff_charge_main").text("{{trans('label.delete')}}");
		                	$("#btn_new_staff_charge_main").attr( "type", "add" );
		                	$("#btn_new_staff_charge_main").attr( "idData", '' );
		                	$('.show_input_add_main').hide();
		                  // callPopupLogin();
		                }        
		              }
		        });
			});

			$(document).off('click', '.deleteStaffCharge_main').on('click', '.deleteStaffCharge_main', function(e){
				
				var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
	    		if (r == true) {
	    			var id = $(this).attr('id');
	    			$.ajax({
			              url: '{{action("StaffController@deleteStaffCharge_main",[""])}}',
			              type: 'GET',
			              data: {id:id},
			              success: function(data) {
			                if(data.status=="success"){
			                  $(".staff_charge_main-"+id).remove();
			                }else{
			                	alert(data.msg);
			                }        
			              }
			        });
	    		}
			});


			// main KH
			$(document).off('click', '#btn_new_staff_charge_main_kh').on('click', '#btn_new_staff_charge_main_kh', function(e){
				$(".staff_charge_main_kh .alert-success").hide();
				$(".staff_charge_main_kh .alert-danger").hide();
				if(validate_form_main('.form_add_main_kh') == 0){
					var stc_date_add = $("#stc_date_add_main_kh").val();
					var stc_two_digit_charge_add = $("#stc_two_digit_charge_add_main_kh").val();
					var stc_three_digit_charge_add = $("#stc_three_digit_charge_add_main_kh").val();
					var parent_id_main = $("#parent_id_main_kh").val();

					var staff_id = '{{ $staff->s_id }}';
					var checkType = $(this).attr('type');
					var iddata = $(this).attr('iddata');
					var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
					var stc_type = 2;
					// alert(parent_id_main);
					// return false;
					$.ajax({
			              url: '{{URL::to('/')}}/storeChargeMain',
			              type: 'GET',
			              data: {stc_date_add:stc_date_add,
			              		 stc_two_digit_charge_add:stc_two_digit_charge_add,
			              		 stc_three_digit_charge_add:stc_three_digit_charge_add,
			              		 parent_id_main:parent_id_main,
			              		 staff_id:staff_id,
			              		 checkType:checkType,
			              		 iddata:iddata,
			              		 idDataTable:idDataTable,
							     stc_type:stc_type
			              },
			              success: function(data) {
			                if(data.status=="success"){
			                  $('tr.form_add_main_kh.smart-form').after(data.msg);
			                  $(".staff_charge_main_kh .alert-success").show();
			                  $(".staff_charge_main_kh .alert-success").find('span').text("{{trans('message.add_success')}}");
			                  $(".staff_charge_main_kh input").val("");
			                  $("#btn_add_staff_charge_main_kh").text("{{trans('label.delete')}}");
			                  $("#btn_new_staff_charge_main_kh").attr('type','add').attr('iddata');
			                }else if(data.status=="updatesuccess"){
			                	$(".staff_charge_main_kh .alert-success").show();
			                  	$(".staff_charge_main_kh .alert-success").find('span').text("{{trans('message.update_success')}}");
			                  	$(".staff_charge_main_kh input").val("");
			                  	$(".staff_charge_main_kh-"+iddata).html(data.msg);
			                  	$("#btn_add_staff_charge_main_kh").text("{{trans('label.delete')}}");
			                  	$("#btn_new_staff_charge_main_kh").attr('type','add').attr('iddata','');
			                }else{
			                	$(".staff_charge_main_kh .alert-danger").show();
			                  	$(".staff_charge_main_kh .alert-danger").find('span').text(data.msg);
			                	// alert();
			                  // callPopupLogin();
			                }        
			              }
			        });
				}
				
			});

			$(document).off('click', '.EditStaffCharge_main_kh').on('click', '.EditStaffCharge_main_kh', function(e){
				var id = $(this).attr('id');
				var table_main = 'tbl_staff_charge_main';
				$.ajax({
		              url: '{{URL::to('/')}}/getCharge',
		              type: 'GET',
		              data: {id:id,table_main:table_main},
		              success: function(data) {
		                if(data.status=="success"){

		                	$("#btn_add_staff_charge_main_kh").text("{{trans('label.delete')}}");
		                	$("#stc_date_add_main_kh").val(data.msg.stc_date);
							$("#stc_two_digit_charge_add_main_kh").val(data.msg.stc_two_digit_charge);
							$("#stc_three_digit_charge_add_main_kh").val(data.msg.stc_three_digit_charge);
							// alert(data.msg.chail_id);
							// $("#parent_id_main").val(data.msg.chail_id);
							$("#parent_id_main_kh").select2().select2('val',data.msg.chail_id);

							$("#stc_currency_add_main_kh").val(data.msg.stc_currency).trigger("change");
							$('.show_input_add_main_kh').show();
							$("#btn_new_staff_charge_main_kh").attr( "type", "modify" );
							$("#btn_new_staff_charge_main_kh").attr( "idData", data.msg.stc_id );
							
		                }else{
		                	$("#btn_add_staff_charge_main_kh").text("{{trans('label.delete')}}");
		                	$("#btn_new_staff_charge_main_kh").attr( "type", "add" );
		                	$("#btn_new_staff_charge_main_kh").attr( "idData", '' );
		                	$('.show_input_add_main_kh').hide();
		                  // callPopupLogin();
		                }        
		              }
		        });
			});
			$(document).off('click', '.deleteStaffCharge_main_kh').on('click', '.deleteStaffCharge_main_kh', function(e){
				
				var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
	    		if (r == true) {
	    			var id = $(this).attr('id');
	    			$.ajax({
			              url: '{{action("StaffController@deleteStaffCharge_main",[""])}}',
			              type: 'GET',
			              data: {id:id},
			              success: function(data) {
			                if(data.status=="success"){
			                  $(".staff_charge_main_kh-"+id).remove();
			                }else{
			                	alert(data.msg);
			                }        
			              }
			        });
	    		}
			});

			// end main kh



			// code event th
			// main TH
			$(document).off('click', '#btn_new_staff_charge_main_th').on('click', '#btn_new_staff_charge_main_th', function(e){
				$(".staff_charge_main_th .alert-success").hide();
				$(".staff_charge_main_th .alert-danger").hide();
				if(validate_form_main('.form_add_main_th') == 0){
					var stc_date_add = $("#stc_date_add_main_th").val();
					var stc_two_digit_charge_add = $("#stc_two_digit_charge_add_main_th").val();
					var stc_three_digit_charge_add = $("#stc_three_digit_charge_add_main_th").val();
					var parent_id_main = $("#parent_id_main_th").val();

					var staff_id = '{{ $staff->s_id }}';
					var checkType = $(this).attr('type');
					var iddata = $(this).attr('iddata');
					var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
					var stc_type = 3;
					// alert(parent_id_main);
					// return false;
					$.ajax({
			              url: '{{URL::to('/')}}/storeChargeMain',
			              type: 'GET',
			              data: {stc_date_add:stc_date_add,
			              		 stc_two_digit_charge_add:stc_two_digit_charge_add,
			              		 stc_three_digit_charge_add:stc_three_digit_charge_add,
			              		 parent_id_main:parent_id_main,
			              		 staff_id:staff_id,
			              		 checkType:checkType,
			              		 iddata:iddata,
			              		 idDataTable:idDataTable,
							     stc_type:stc_type
			              },
			              success: function(data) {
			                if(data.status=="success"){
			                  $('tr.form_add_main_th.smart-form').after(data.msg);
			                  $(".staff_charge_main_th .alert-success").show();
			                  $(".staff_charge_main_th .alert-success").find('span').text("{{trans('message.add_success')}}");
			                  $(".staff_charge_main_th input").val("");
			                  $("#btn_add_staff_charge_main_th").text("{{trans('label.delete')}}");
			                  $("#btn_new_staff_charge_main_th").attr('type','add').attr('iddata');
			                }else if(data.status=="updatesuccess"){
			                	$(".staff_charge_main_th .alert-success").show();
			                  	$(".staff_charge_main_th .alert-success").find('span').text("{{trans('message.update_success')}}");
			                  	$(".staff_charge_main_th input").val("");
			                  	$(".staff_charge_main_th-"+iddata).html(data.msg);
			                  	$("#btn_add_staff_charge_main_th").text("{{trans('label.delete')}}");
			                  	$("#btn_new_staff_charge_main_th").attr('type','add').attr('iddata','');
			                }else{
			                	$(".staff_charge_main_th .alert-danger").show();
			                  	$(".staff_charge_main_th .alert-danger").find('span').text(data.msg);
			                	// alert();
			                  // callPopupLogin();
			                }        
			              }
			        });
				}
				
			});
			$(document).off('click', '.EditStaffCharge_main_th').on('click', '.EditStaffCharge_main_th', function(e){
				var id = $(this).attr('id');
				var table_main = 'tbl_staff_charge_main';
				$.ajax({
		              url: '{{URL::to('/')}}/getCharge',
		              type: 'GET',
		              data: {id:id,table_main:table_main},
		              success: function(data) {
		                if(data.status=="success"){

		                	$("#btn_add_staff_charge_main_th").text("{{trans('label.delete')}}");
		                	$("#stc_date_add_main_th").val(data.msg.stc_date);
							$("#stc_two_digit_charge_add_main_th").val(data.msg.stc_two_digit_charge);
							$("#stc_three_digit_charge_add_main_th").val(data.msg.stc_three_digit_charge);
							// alert(data.msg.chail_id);
							// $("#parent_id_main").val(data.msg.chail_id);
							$("#parent_id_main_th").select2().select2('val',data.msg.chail_id);

							$("#stc_currency_add_main_th").val(data.msg.stc_currency).trigger("change");
							$('.show_input_add_main_th').show();
							$("#btn_new_staff_charge_main_th").attr( "type", "modify" );
							$("#btn_new_staff_charge_main_th").attr( "idData", data.msg.stc_id );
							
		                }else{
		                	$("#btn_add_staff_charge_main_th").text("{{trans('label.delete')}}");
		                	$("#btn_new_staff_charge_main_th").attr( "type", "add" );
		                	$("#btn_new_staff_charge_main_th").attr( "idData", '' );
		                	$('.show_input_add_main_th').hide();
		                  // callPopupLogin();
		                }        
		              }
		        });
			});
			$(document).off('click', '.deleteStaffCharge_main_th').on('click', '.deleteStaffCharge_main_th', function(e){
				
				var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
	    		if (r == true) {
	    			var id = $(this).attr('id');
	    			$.ajax({
			              url: '{{action("StaffController@deleteStaffCharge_main",[""])}}',
			              type: 'GET',
			              data: {id:id},
			              success: function(data) {
			                if(data.status=="success"){
			                  $(".staff_charge_main_th-"+id).remove();
			                }else{
			                	alert(data.msg);
			                }        
			              }
			        });
	    		}
			});
			 // btn controll show hide form insert data Khmer
            $(document).off('click', '#btn_add_staff_charge_th').on('click', '#btn_add_staff_charge_th', function(e){

                if($('.show_input_add_th').css('display') == 'none'){
                    $('#stc_date_add_th').val('');
                    $('#stc_two_digit_charge_add_th').val('');
                    $('#stc_three_digit_charge_add_th').val('');
                    $('.show_input_add_th').show();
                    $("#btn_new_staff_charge_th").attr( "type", "add" );
                    $("#btn_new_staff_charge_th").attr( "idData", '' );
                    $(this).text("{{ trans('label.delete') }}");
                    $('#stc_currency_add_th').val($('#stc_currency_add_th option:first-child').val()).trigger('change');
                }else{
                    $('#stc_date_add_th').val('');
                    $('#stc_two_digit_charge_add_th').val('');
                    $('#stc_three_digit_charge_add_th').val('');
                    $('.show_input_add_th').hide();
                    $(this).text("{{ trans('label.add') }}");
                }

            });

			// insert data to database TH
            $(document).off('click', '#btn_new_staff_charge_th').on('click', '#btn_new_staff_charge_th', function(e){
                $(".staff_charge_th .alert-success").hide();
                $(".staff_charge_th .alert-danger").hide();
                if(validate_form_main('.form_add_th') == 0){
                    var stc_date_add = $("#stc_date_add_th").val();
                    var stc_two_digit_charge_add = $("#stc_two_digit_charge_add_th").val();
                    var stc_three_digit_charge_add = $("#stc_three_digit_charge_add_th").val();

                    var stc_pay_two_digit_charge_add_th = $("#stc_pay_two_digit_charge_add_th").val();
                    var stc_pay_there_digit_charge_add_th = $("#stc_pay_there_digit_charge_add_th").val();

                    var staff_id = '{{ $staff->s_id }}';
                    var checkType = $(this).attr('type');
                    var iddata = $(this).attr('iddata');
                    var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
                    var stc_type = 3;
                    $.ajax({
                        url: '{{URL::to('/')}}/storeCharge',
                        type: 'GET',
                        data: {stc_date_add:stc_date_add,
                            stc_two_digit_charge_add:stc_two_digit_charge_add,
                            stc_three_digit_charge_add:stc_three_digit_charge_add,
                            stc_pay_two_digit_charge_add:stc_pay_two_digit_charge_add_th,
                            stc_pay_there_digit_charge_add:stc_pay_there_digit_charge_add_th,
                            staff_id:staff_id,
                            checkType:checkType,
                            iddata:iddata,
                            idDataTable:idDataTable,
                            stc_type:stc_type
                        },
                        success: function(data) {
                            if(data.status=="success"){
                                $('tr.form_add_th.smart-form').after(data.msg);
                                $(".staff_charge_th .alert-success").show();
                                $(".staff_charge_th .alert-success").find('span').text("{{trans('message.add_success')}}");
                                $(".staff_charge_th input").val("");
                                $("#btn_add_staff_charge_th").text("{{trans('label.delete')}}");
                                $("#btn_new_staff_charge_th").attr('type','add').attr('iddata');
                            }else if(data.status=="updatesuccess"){
                                $(".staff_charge_th .alert-success").show();
                                $(".staff_charge_th .alert-success").find('span').text("{{trans('message.update_success')}}");
                                $(".staff_charge_th input").val("");
                                $(".staff_charge-"+iddata).html(data.msg);
                                $("#btn_add_staff_charge_th").text("{{trans('label.delete')}}");
                                $("#btn_new_staff_charge_th").attr('type','add').attr('iddata','');
                            }else{
                                $(".staff_charge_th .alert-danger").show();
                                $(".staff_charge_th .alert-danger").find('span').text(data.msg);
                                // alert();
                                // callPopupLogin();
                            }
                        }
                    });
                }

            });

			$(document).off('click', '.EditStaffCharge_th').on('click', '.EditStaffCharge_th', function(e){
                var id = $(this).attr('id');
                $.ajax({
                    url: '{{URL::to('/')}}/getCharge',
                    type: 'GET',
                    data: {id:id},
                    success: function(data) {
                        if(data.status=="success"){

                            $("#btn_add_staff_charge_th").text("{{trans('label.delete')}}");
                            $("#stc_date_add_th").val(data.msg.stc_date);
                            $("#stc_two_digit_charge_add_th").val(data.msg.stc_two_digit_charge);
                            $("#stc_three_digit_charge_add_th").val(data.msg.stc_three_digit_charge);

                            $("#stc_pay_two_digit_charge_add_th").val(data.msg.stc_pay_two_digit);
							$("#stc_pay_there_digit_charge_add_th").val(data.msg.stc_pay_there_digit);

                            $("#stc_currency_add_th").val(data.msg.stc_currency).trigger("change");
                            $('.show_input_add_th').show();
                            $("#btn_new_staff_charge_th").attr( "type", "modify" );
                            $("#btn_new_staff_charge_th").attr( "idData", data.msg.stc_id );

                        }else{
                            $("#btn_add_staff_charge_th").text("{{trans('label.delete')}}");
                            $("#btn_new_staff_charge_th").attr( "type", "add" );
                            $("#btn_new_staff_charge_th").attr( "idData", '' );
                            $('.show_input_add_th').hide();
                            // callPopupLogin();
                        }
                    }
                });
            });

            $(document).off('click', '.deleteStaffCharge_th').on('click', '.deleteStaffCharge_th', function(e){

                var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
                if (r == true) {
                    var id = $(this).attr('id');
                    $.ajax({
                        url: '{{action("StaffController@deleteStaffCharge",[""])}}',
                        type: 'GET',
                        data: {id:id},
                        success: function(data) {
                            if(data.status=="success"){
                                $(".staff_charge-"+id).remove();
                            }else{
                                alert(data.msg);
                            }
                        }
                    });
                }
            });

            // insert data to database LO
             // btn controll show hide form insert data Khmer
            $(document).off('click', '#btn_add_staff_charge_lo').on('click', '#btn_add_staff_charge_lo', function(e){

                if($('.show_input_add_lo').css('display') == 'none'){
                    $('#stc_date_add_lo').val('');
                    $('#stc_two_digit_charge_add_lo').val('');
                    $('#stc_three_digit_charge_add_lo').val('');
                    $('.show_input_add_lo').show();
                    $("#btn_new_staff_charge_lo").attr( "type", "add" );
                    $("#btn_new_staff_charge_lo").attr( "idData", '' );
                    $(this).text("{{ trans('label.delete') }}");
                    $('#stc_currency_add_lo').val($('#stc_currency_add_th option:first-child').val()).trigger('change');
                }else{
                    $('#stc_date_add_lo').val('');
                    $('#stc_two_digit_charge_add_lo').val('');
                    $('#stc_three_digit_charge_add_lo').val('');
                    $('.show_input_add_lo').hide();
                    $(this).text("{{ trans('label.add') }}");
                }

            });

            $(document).off('click', '#btn_new_staff_charge_lo').on('click', '#btn_new_staff_charge_lo', function(e){
                $(".staff_charge_lo .alert-success").hide();
                $(".staff_charge_lo .alert-danger").hide();
                if(validate_form_main('.form_add_lo') == 0){
                    var stc_date_add = $("#stc_date_add_lo").val();
                    var stc_two_digit_charge_add = $("#stc_two_digit_charge_add_lo").val();
                    var stc_three_digit_charge_add = $("#stc_three_digit_charge_add_lo").val();

                    var stc_pay_two_digit_charge_add_lo = $("#stc_pay_two_digit_charge_add_lo").val();
                    var stc_pay_there_digit_charge_add_lo = $("#stc_pay_there_digit_charge_add_lo").val();

                    var staff_id = '{{ $staff->s_id }}';
                    var checkType = $(this).attr('type');
                    var iddata = $(this).attr('iddata');
                    var idDataTable = $('#datatable_tabletools .staff_charge-'+iddata+' > td:nth-child(1)').text();
                    var stc_type = 4;
                    $.ajax({
                        url: '{{URL::to('/')}}/storeCharge',
                        type: 'GET',
                        data: {stc_date_add:stc_date_add,
                            stc_two_digit_charge_add:stc_two_digit_charge_add,
                            stc_three_digit_charge_add:stc_three_digit_charge_add,
                            stc_pay_two_digit_charge_add:stc_pay_two_digit_charge_add_lo,
                            stc_pay_there_digit_charge_add:stc_pay_there_digit_charge_add_lo,
                            staff_id:staff_id,
                            checkType:checkType,
                            iddata:iddata,
                            idDataTable:idDataTable,
                            stc_type:stc_type
                        },
                        success: function(data) {
                            if(data.status=="success"){
                                $('tr.form_add_lo.smart-form').after(data.msg);
                                $(".staff_charge_lo .alert-success").show();
                                $(".staff_charge_lo .alert-success").find('span').text("{{trans('message.add_success')}}");
                                $(".staff_charge_lo input").val("");
                                $("#btn_add_staff_charge_lo").text("{{trans('label.delete')}}");
                                $("#btn_new_staff_charge_lo").attr('type','add').attr('iddata');
                            }else if(data.status=="updatesuccess"){
                                $(".staff_charge_lo .alert-success").show();
                                $(".staff_charge_lo .alert-success").find('span').text("{{trans('message.update_success')}}");
                                $(".staff_charge_lo input").val("");
                                $(".staff_charge-"+iddata).html(data.msg);
                                $("#btn_add_staff_charge_lo").text("{{trans('label.delete')}}");
                                $("#btn_new_staff_charge_lo").attr('type','add').attr('iddata','');
                            }else{
                                $(".staff_charge_lo .alert-danger").show();
                                $(".staff_charge_lo .alert-danger").find('span').text(data.msg);
                                // alert();
                                // callPopupLogin();
                            }
                        }
                    });
                }

            });

			$(document).off('click', '.EditStaffCharge_lo').on('click', '.EditStaffCharge_lo', function(e){
                var id = $(this).attr('id');
                $.ajax({
                    url: '{{URL::to('/')}}/getCharge',
                    type: 'GET',
                    data: {id:id},
                    success: function(data) {
                        if(data.status=="success"){

                            $("#btn_add_staff_charge_lo").text("{{trans('label.delete')}}");
                            $("#stc_date_add_lo").val(data.msg.stc_date);
                            $("#stc_two_digit_charge_add_lo").val(data.msg.stc_two_digit_charge);
                            $("#stc_three_digit_charge_add_lo").val(data.msg.stc_three_digit_charge);

                            $("#stc_pay_two_digit_charge_add_lo").val(data.msg.stc_pay_two_digit);
							$("#stc_pay_there_digit_charge_add_lo").val(data.msg.stc_pay_there_digit);

                            $("#stc_currency_add_lo").val(data.msg.stc_currency).trigger("change");
                            $('.show_input_add_lo').show();
                            $("#btn_new_staff_charge_lo").attr( "type", "modify" );
                            $("#btn_new_staff_charge_lo").attr( "idData", data.msg.stc_id );

                        }else{
                            $("#btn_add_staff_charge_lo").text("{{trans('label.delete')}}");
                            $("#btn_new_staff_charge_lo").attr( "type", "add" );
                            $("#btn_new_staff_charge_lo").attr( "idData", '' );
                            $('.show_input_add_lo').hide();
                            // callPopupLogin();
                        }
                    }
                });
            });

            $(document).off('click', '.deleteStaffCharge_lo').on('click', '.deleteStaffCharge_lo', function(e){

                var r = confirm("តើអ្នកចង់លុបវាឬទេ?");
                if (r == true) {
                    var id = $(this).attr('id');
                    $.ajax({
                        url: '{{action("StaffController@deleteStaffCharge",[""])}}',
                        type: 'GET',
                        data: {id:id},
                        success: function(data) {
                            if(data.status=="success"){
                                $(".staff_charge-"+id).remove();
                            }else{
                                alert(data.msg);
                            }
                        }
                    });
                }
            });



		 
		   // START AND FINISH DATE
		   $('#s_start').datepicker({
			    dateFormat : 'yy-mm-dd',
			    prevText : '<i class="fa fa-chevron-left"></i>',
			    nextText : '<i class="fa fa-chevron-right"></i>',
			    onSelect : function(selectedDate) {
			     $('#s_end').datepicker('option', 'minDate', selectedDate);
			    }
			});

		   $('#s_end').datepicker({
			    dateFormat : 'yy-mm-dd',
			    prevText : '<i class="fa fa-chevron-left"></i>',
			    nextText : '<i class="fa fa-chevron-right"></i>',
			    onSelect : function(selectedDate) {
			     $('#s_start').datepicker('option', 'minDate', selectedDate);
			    }
			});

		   $('#stc_date_add_main').datepicker({
			    dateFormat : 'yy-mm-dd',
			    prevText : '<i class="fa fa-chevron-left"></i>',
			    nextText : '<i class="fa fa-chevron-right"></i>',
			    // onSelect : function(selectedDate) {
			    //  $('#dob').datepicker('option', 'minDate', selectedDate);
			    // }
			});

		   $('#stc_date_add_main_kh').datepicker({
			    dateFormat : 'yy-mm-dd',
			    prevText : '<i class="fa fa-chevron-left"></i>',
			    nextText : '<i class="fa fa-chevron-right"></i>',
			    // onSelect : function(selectedDate) {
			    //  $('#dob').datepicker('option', 'minDate', selectedDate);
			    // }
			});
		   $('#stc_date_add_main_th').datepicker({
			    dateFormat : 'yy-mm-dd',
			    prevText : '<i class="fa fa-chevron-left"></i>',
			    nextText : '<i class="fa fa-chevron-right"></i>',
			    // onSelect : function(selectedDate) {
			    //  $('#dob').datepicker('option', 'minDate', selectedDate);
			    // }
			});

		   $('#stc_date_add').datepicker({
			    dateFormat : 'yy-mm-dd',
			    prevText : '<i class="fa fa-chevron-left"></i>',
			    nextText : '<i class="fa fa-chevron-right"></i>',
			    // onSelect : function(selectedDate) {
			    //  $('#dob').datepicker('option', 'minDate', selectedDate);
			    // }
			});
            $('#stc_date_add_kh').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                // onSelect : function(selectedDate) {
                //  $('#dob').datepicker('option', 'minDate', selectedDate);
                // }
            });
            $('#stc_date_add_th').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                // onSelect : function(selectedDate) {
                //  $('#dob').datepicker('option', 'minDate', selectedDate);
                // }
            });
            $('#stc_date_add_lo').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                // onSelect : function(selectedDate) {
                //  $('#dob').datepicker('option', 'minDate', selectedDate);
                // }
            });
		});
		function btnCancel(){
	   		var r = confirm("តើលោកអ្នកចង់ត្រលប់ក្រោយឬទេ?");
		    if (r == true) {
		        document.location.href='{{URL::to('/')}}/staff';
		    }
	    }
	</script>
@stop