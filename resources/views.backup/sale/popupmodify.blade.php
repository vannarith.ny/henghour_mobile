<style type="text/css">
	#event_add_in_row_modify{
		visibility: visible !important;
	}
	#event_add_in_row_modify .formAddGroup{
		width: 80% !important;
		padding-bottom:10px;
		margin-bottom: 10px;
	}
	#event_add_in_row_modify.row_main{
			background-color: #fff !important;
	}
	#event_add_in_row_modify .lottery_num_select{
		width: 10% !important;
	}
	#event_add_in_row_modify .lottery_num{
		width: 35% !important;
	}
	#event_add_in_row_modify .main_checkbo{
		width: 12% !important;
		margin-left: 10px !important;
	}
	#event_add_in_row_modify .btnSave{
		float: left !important;
		width: 30% !important;
		margin-top: 5px !important;
	}

	#event_add_in_row_modify .onedigitstyle{
	    width: 18% !important;
	    margin-right: 10px;
	}


	

	#event_add_in_row_modify_group{
		visibility: visible !important;
	}
	#event_add_in_row_modify_group .formAddGroup{
		width: 80% !important;
		padding-bottom:10px;
		margin-bottom: 10px;
	}
	#event_add_in_row_modify_group.row_main{
			background-color: #fff !important;
	}
	#event_add_in_row_modify_group .lottery_num_select{
		width: 10% !important;
	}
	#event_add_in_row_modify_group .lottery_num{
		width: 35% !important;
	}
	#event_add_in_row_modify_group .main_checkbo{
		width: 12% !important;
		margin-left: 10px !important;
	}
	#event_add_in_row_modify_group .btnSave{
		float: left !important;
		width: 30% !important;
		margin-top: 5px !important;
	}

	#event_add_in_row_modify_group .onedigitstyle{
	    width: 18% !important;
	    margin-right: 10px;
	}

</style>
<div id="dialogmodifynumber" title="" style="display:none;">
 <div style="width:100%; padding:10px 20px; ">
 	<h1>{{ trans('label.modify_number') }} : <span class="view_number_modify"></span></h1>

 	<div class="modify_number_style">
 		<!-- form add lottery -->
		<div class='row_main custom_padding smart-form' id="event_add_in_row_modify">
			<div class="lottery_num pos_style formAddGroup">
				<select id="pos_id_add" name="pos_id_add" class="formlottery_select eventPostInRow">
					<option value="">{{trans('label.pos')}}</option>
					@foreach($groups as $group)
					<option value="{{ $group->g_id }}">{{ $group->g_name }}</option>
					@endforeach
				</select>
				<input type="hidden" id='pos_id_hidden' name="pos_id_hidden" class="required">
				<input type="hidden" id='pos_id_hidden_old' name="pos_id_hidden_old" class="">
				<input type="hidden" id='r_id_hidden' name="r_id_hidden" value="">
				<input type="hidden" id='p_id_hidden' name="p_id_hidden" value="">
				<input type="hidden" id='num_id_hidden' name="num_id_hidden" value="">
			</div>
			<div class="clear"></div>

			<!-- for list of random -->
			<div class="list_lottery view_lot2">
            </div>

			<div class="clear"></div>
			<!-- for list of random -->

			<div class="lottery_num view_lot2 onedigitstyle">
				{!! Form::text("num_number_add_view2_digit1", $value = null, $attributes = array( 'id' => 'num_number_add_view2_digit1', 'class'=>'form-control formlottery onedigitNew num  eventControlnumber ', 'placeholder'=>'ល1')) !!}
			</div>
			<div class="lottery_num view_lot2 onedigitstyle">
				{!! Form::text("num_number_add_view2_digit2", $value = null, $attributes = array( 'id' => 'num_number_add_view2_digit2', 'class'=>'form-control formlottery onedigitNew num  eventControlnumber ', 'placeholder'=>'ល2')) !!}
			</div>
			<div class="lottery_num view_lot2 onedigitstyle">
				{!! Form::text("num_number_add_view2_digit3", $value = null, $attributes = array( 'id' => 'num_number_add_view2_digit3', 'class'=>'form-control formlottery onedigitNew num  eventControlnumber ', 'placeholder'=>'ល3')) !!}
			</div>
			<div class="btn_add_colume view_lot2 " >
    			<i class="fa fa-fw fa-plus-circle txt-color-green addNewnumlottolist" aria-hidden="true"></i>
    		</div>
			<div class="clear view_lot2"></div>


			<div class="lottery_num view_lot1">
				{!! Form::text("num_number_add", $value = null, $attributes = array( 'id' => 'num_number_add', 'class'=>'form-control formlottery threedigitNew num required eventControlnumber', 'placeholder'=>trans('label.number'))) !!}
			</div>
			<div class="lottery_num_select view_lot1">
				<select id="sym_id_add" name="sym_id_add" class="formlottery_select eventControlSym">
					@foreach($symbols as $symbol)
					<option value="{{ $symbol->pav_id }}">{{ $symbol->pav_value }}</option>
					@endforeach
				</select>
			</div>
			<div class="lottery_num view_lot1">
				{!! Form::text("num_number_end_add", $value = null, $attributes = array( 'id' => 'num_number_end_add','disabled' => 'true', 'class'=>'form-control formlottery threedigitNew num', 'placeholder'=>trans('label.number'))) !!}
			</div>
			<div class="lottery_num main_checkbo view_lot1">
				<span>{{trans('label.multiply')}}</span>
				<input type="checkbox" id="num_reverse_add" name="num_reverse_add"​ class="check_style">
			</div>
			<div class="clear"></div>
			<div class="lottery_num main_price">
				{!! Form::text("num_amount_add", $value = null, $attributes = array( 'id' => 'num_amount_add', 'class'=>'form-control formlottery num_dolla entersubmit required', 'placeholder'=>trans('label.money'))) !!}
			</div>
			<div class="lottery_num_select customStyleCurrency">
				<select id="currentcy_add" name="currentcy_add" class="formlottery_select customCurrency">
					@foreach($currencys as $currency)
					<option value="{{ $currency->pav_id }}">{{ $currency->pav_value }}</option>
					@endforeach
				</select>
			</div>
			<div class="btnSave">
				<button type="button" id="saveLotteryUpdate" colume="" page="" name="saveLottery" class="btn btn-xs btn-primary view_lot1">{{trans('label.save')}}</button>
				<button type="button" id="saveLotteryUpdate_new" colume="" page="" name="saveLottery" class="btn btn-xs btn-primary view_lot2 saveLotteryUpdate_new">{{trans('label.save')}}</button>
			</div>
			<div class="clear"></div>
		</div>
 	</div>
 	
 </div>
</div>

<div id="dialogmodifynumber_group" title="" style="display:none;">
 <div style="width:100%; padding:10px 20px; ">
 	<h1>កែប្រែបុស្តិ៍  : <span class="view_number_modify"></span></h1>

 	<div class="modify_number_style">
 		<!-- form add lottery -->
		<div class='row_main custom_padding smart-form' id="event_add_in_row_modify_group">
			<div class="lottery_num pos_style formAddGroup">
				<select id="pos_id_add_group" name="pos_id_add_group" class="formlottery_select eventPostInRow">
					<option value="">{{trans('label.pos')}}</option>
					@foreach($groups as $group)
					<option value="{{ $group->g_id }}">{{ $group->g_name }}</option>
					@endforeach
				</select>
				<input type="hidden" id='r_id_hidden' name="r_id_hidden" value="">
				<input type="hidden" id='p_id_hidden' name="p_id_hidden" value="">
				<input type="hidden" id='num_id_hidden' name="num_id_hidden" value="">
				<input type="hidden" id='main_id_need' name="main_id_need" value="">
			</div>
			<div class="clear"></div>

			<!-- for list of random -->
			

			<div class="clear"></div>
			<!-- for list of random -->

			
			<div class="btnSave" style="margin:0 auto; float:none !important; width:15% !important;">
				<button type="button" id="saveLotteryUpdateGroup" colume="" page="" name="saveLotteryUpdateGroup" class="btn btn-xs btn-primary view_lot1">{{trans('label.save')}}</button>
			</div>
			<div class="clear"></div>
		</div>
 	</div>
 	
 </div>
</div>

