@extends('master')
@section('title')
<title>របាយការណ៏ ឈ្មួញកណ្តាល</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}
		.backgroundSubTotal{
			background: #eaeaea;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ ឈ្មួញកណ្តាល</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏ ឈ្មួញកណ្តាល</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'filterMain', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('block_id', ([
                                        '' => trans('result.chooseStaff') ]+$block),$staff_filter,['class' => 'required ','id'=>'block_id','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>


							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 @if(isset($report))
                        <style>
                            .displayReport{
                                font-size: 14px;
                            }
                            table.displayReport tr th{
                                font-size: 14px;
                                vertical-align: top;
                                text-align:center !important;
                            }
                        </style>
						 <table id="" class="table table-bordered displayReport" >
                            <thead>
                                <tr>
                                    <th rowspan="2" v-align="top">កូន</th>
                                    <th colspan="2" >លុយកូន</th>
                                    <th colspan="2">លុយទឹក</th>
                                    <th colspan="2">លុយឈ្មួញកណ្ដាល</th>
                                    <th colspan="2" >ភាគរយ ឈ្មួញកណ្ដាល <br> {{$staffMain->percent}} %</th>
                                    <th colspan="3" class="background">ភាគរយ មេ ១ថ្ងៃ<br> {{100 - $staffMain->percent}} % </th>
                                </tr>
                                <tr>
                                    <th>រៀល (៛) </th>
                                    <th>ដុល្លា ($)</th>

                                    <th>រៀល (៛) </th>
                                    <th>ដុល្លា ($)</th>

                                    <th>រៀល (៛) </th>
                                    <th>ដុល្លា ($)</th>

                                    <th>រៀល (៛) </th>
                                    <th>ដុល្លា ($)</th>

                                    <th class="background">រៀល (៛) </th>
                                    <th class="background">ដុល្លា ($)</th>
                                </tr>
                            </thead>
                            <tbody class="display_total_result">
                            <?php
                                $checkStaff = '';
                                $checkTime = '';
                                $count = 0;

                                $total2 = 0;
                                $total3 = 0;

                                $total2DD = 0;
                                $total3DD = 0;

                                $Wtotal2DR = 0;
                                $Wtotal3DR = 0;

                                $Wtotal2DD = 0;
								$Wtotal3DD = 0;
								
								$totalRow1 = 0;
								$totalRow2 = 0;
								$totalRow3 = 0;
								$totalRow4 = 0;
								$totalRow5 = 0;
								$totalRow6 = 0;
								$totalRow7 = 0;
								$totalRow8 = 0;
								$totalRow9 = 0;
								$totalRow10 = 0;

                            ?>
                            @foreach ($report as $key => $row)
                                @php 
                                    $count = $count + 1;
                                @endphp
                                
                                <tr>
                                    <td align="left" style="text-align:left !important; width: 10%;">{{$row->s_name}}</td>
                                    
                                    <!-- // total amount child water -->
                                    <td class=" " align="right" style="width: 5% !important;">{{$row->oneday_price_r}}</td>
                                    <td class=" " align="right" style="width: 5% !important;">{{$row->oneday_price_d}}</td>
									
                                    <!-- // totam amount main water -->
                                    <?php 
                                        $data = DB::table("tbl_sum_by_paper")
                                                ->select(DB::raw('sum(price2digit_r) as totalprice2R, sum(price2digit_d) as totalprice2D, sum(price3digit_r) as totalprice3R, sum(price3digit_d) as totalprice3D'))
                                                ->where('s_id', $row->s_id)
                                                ->where('date', $row->date)
                                                ->first();
                                    if($data){
                                        $totalWater2 = (float) ($row->s_two_digit_charge - $staffMain->s_two_digit_charge);
                                        $totalWater3 = (float) ($row->s_three_digit_charge - $staffMain->s_three_digit_charge);
                                        $amount2R = $data->totalprice2R * ($totalWater2/100);
                                        $amount2D = $data->totalprice2D * ($totalWater2/100);

                                        $amount3R = $data->totalprice3R * ($totalWater3/100);
                                        $amount3D = $data->totalprice3D * ($totalWater3/100);

                                        $totalReal = $amount2R + $amount3R;
                                        $totalDollar = $amount2D + $amount3D;
                                    }else{
                                        $totalReal = 0;
                                        $totalDollar = 0;
									}
									
									$totalRow1 = $totalRow1 + $row->oneday_price_r;
									$totalRow2 = $totalRow2 + $row->oneday_price_d;

                                    $totalReal = $totalReal * -1;
									$totalDollar = $totalDollar * -1;
									
									$totalRow3 = $totalRow3 + $totalReal;
									$totalRow4 = $totalRow4 + $totalDollar;

                                    ?>
                                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($totalReal) }}</td>
                                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($totalDollar, 2) }} $</td>
                                    
                                    <!-- get data main payment -->
                                    <?php 
                                        $mainPaymentReal = (float) ($row->oneday_price_r + $totalReal);
										$mainPaymentDollar = (float) ($row->oneday_price_d + $totalDollar);

										$totalRow5 = $totalRow5 + $mainPaymentReal;
										$totalRow6 = $totalRow6 + $mainPaymentDollar;
										
                                    ?>
                                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($mainPaymentReal) }}</td>
                                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($mainPaymentDollar, 2) }} $</td>

                                    <!-- percent main  -->
                                    <?php 
                                        // $mainPercent = $staffMain->percent;
                                        $mainPaymentPercentReal = (float) ($mainPaymentReal * ($staffMain->percent/100));
										$mainPaymentPercentDollar = (float) ($mainPaymentDollar * ($staffMain->percent/100));
										$totalRow7 = $totalRow7 + $mainPaymentPercentReal;
										$totalRow8 = $totalRow8 + $mainPaymentPercentDollar;
                                    ?>
                                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($mainPaymentPercentReal) }}</td>
                                    <td class=" " align="right" style="width: 5% !important;">{{ number_format($mainPaymentPercentDollar, 2) }} $</td>

                                    <!-- percent boss  -->
                                    <?php 
                                        $bossPaymentReal = $mainPaymentReal - $mainPaymentPercentReal;
                                        $bossPaymentDollar = $mainPaymentDollar - $mainPaymentPercentDollar;
                                        $total2 = $total2 + $bossPaymentReal;
										$total3 = $total3 + $bossPaymentDollar;
										
										$totalRow9 = $totalRow9 + $bossPaymentReal;
										$totalRow10 = $totalRow10 + $bossPaymentDollar;
                                    ?>
                                    <td class="background" align="right" style="width: 5% !important;">{{ number_format($bossPaymentReal) }}</td>
                                    <td class="background" style="width: 5% !important;" colspan="2" align="right">{{ number_format($bossPaymentDollar, 2) }} $</td>
                                </tr>

                            @endforeach
								<tr>
                                    <td class="backgroundSubTotal" style="text-align: right !important;">សរុប</td>

                                    <td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow1) }}</td>
                                    <td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow2, 2) }} $</td>
									<td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow3) }}</td>
                                    <td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow4,2) }} $</td>
									<td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow5) }}</td>
                                    <td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow6, 2) }} $</td>
									<td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow7) }}</td>
                                    <td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow8,2) }} $</td>
									<td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow9) }}</td>
                                    <td class="backgroundSubTotal" style="text-align: right !important;">{{ number_format($totalRow10,2) }} $</td>
                                </tr>
                                <?php 
                                    if($total2 > 0){
                                        $labelR = '<span class="blueColor">មេសុី</span>';
                                        $classR = 'blueColor';
                                    }else{
                                        $labelR = '<span class="redColor">មេខាត</span>';
                                        $classR = 'redColor';
                                    }

                                    if($total3 > 0){
                                        $labelD = '<span class="blueColor">មេសុី</span>';
                                        $classD = 'blueColor';
                                    }else{
                                        $labelD = '<span class="redColor">មេខាត</span>';
                                        $classD = 'redColor';
                                    }
                                ?>
                                <tr>
                                    <td class="" colspan="9" style="text-align: right !important;">{!! $labelR !!}</td>
                                    <td class="" style="text-align: right !important;"><span class="{{$classR}}">{{number_format($total2)}}</span></td>
                                    <td class="" style="text-align: right !important;"><span class="{{$classD}}">{{ number_format($total3, 2) }} $</span></td>
                                </tr>
                            </tbody>
                         </table>
                        <style>
                            .background{
                                background: #c7c7c7;
                            }
                            .blueColor{
                                color: blue;
                                font-size: 16px !important;
                            }
                            .redColor{
                                color: red;
                                font-size: 16px !important;
                            }
                        </style>

					 @endif



		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

	$(document).ready(function() {
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
        });
        
        $(document).off('submit', '#filter').on('submit', '#filter', function(e){
			if (validate_form_main('#filter') == 0) {
				return true;
			}
			return false;
		});
	});
	
	</script>
@stop