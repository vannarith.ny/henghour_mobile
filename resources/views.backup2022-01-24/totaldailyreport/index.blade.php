@extends('master')
<?php
use App\Http\Controllers\SaleController;

// $lottery_pay = DB::('tbl_staff_charge')->where('')
?>
@section('title')
<title>របាយការណ៏</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

	




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         	<!-- widget content -->
		        	<div class="widget-body no-padding">


						 {!! Form::open(['route' => 'reportFilterTotalDaily', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
						 <fieldset>
							 
							 @if(isset($var_dateStart))
								 <?php $startDate_filter = $var_dateStart; ?>
							 @else
								 <?php $startDate_filter = null; ?>
							 @endif

							 @if(isset($var_location_type))
								 <?php $location_type = $var_location_type; ?>
							 @else
								 <?php $location_type = null; ?>
							 @endif

							 <div class="row">

								 <section class="col col-2">
									 <label class="input">
										 <i class="icon-append fa fa-calendar"></i>
										 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
									 </label>

								 </section>
								 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('locations', ([
                                        '' => 'តំបន់' ]+$locations),$location_type,['class' => 'required ','id'=>'type_lottery','sms'=> trans('result.pleaseChooseTypeLottery') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

								
								 <section class="col col-2">
									 <label class="tesxt">
										 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
									 </label>
								 </section>

							 </div>
						 </fieldset>
						 {{ Form::close() }}

					 

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 .table{
							 	/*border: 1px #000 solid;*/
							 }
							 .table>thead>tr>th{
							 	vertical-align: middle;
							 	background: #fff;
							 	/*border: 1px #000 solid;*/
							 }

							 td.lose{
							 	color: red;
							 }
							 
							 table.labelInfo, table.labelInfo th{
							 	border:none !important;
							 	text-align: left !important;
							 }


						 </style>


						 @if(isset($staff_list))
						 <div class="widget-body control_width">
						 			
						 			<table id="" class="table table-bordered summary_result_per_day labelInfo" style="width:90%; margin-left: 15px;" align="lefts">
						 				<thead>
						 					<tr>
						 						 <th style="width: 40%">បញ្ចីប្រចាំថ្ងៃ : {{$startDate_filter}}</th>
												 <th style="width: 10%"></th>
												 <th style="width: 40%">តំបន់ : {{ $getloationName->name }}</th>
												 <th style="width: 10%"></th>
						 					</tr>
						 				</thead>
						 			</table>

									 <table id="" class="table table-bordered summary_result_per_day" style="width:90%; margin-left: 15px;">
										 <thead>
											 <tr>
											 	 <th rowspan="2" valign="middle" style="width: 10%">ល.រ</th>
												 <th rowspan="2" style="width: 10%">ឈ្មោះ</th>
												 <th colspan="4" style="width: 40%">តំបន់ : {{ $getloationName->name }}</th>
												 <!-- <th colspan="4" style="width: 40%">KH</th> -->
											 </tr>
											 <tr>
												 <th>សុី(៛)</th>
												 <th >ខាត(៛)</th>
												 <th>សុី($)</th>
												 <th >ខាត($)</th>

												 <!-- <th>សុី(៛)</th>
												 <th >ខាត(៛)</th>
												 <th>សុី($)</th>
												 <th >ខាត($)</th> -->
											 </tr>
										 
										 </thead>
										 <tbody class="display_total_result">
										 	<?php 
									$money_r_win = 0;
									$money_r_lose = 0;
									$money_d_win = 0;
									$money_d_lose = 0;

									$money_r_win_kh = 0;
									$money_r_lose_kh = 0;
									$money_d_win_kh = 0;
									$money_d_lose_kh = 0;
									if(count($staff_list) > 0){
										 	foreach ($staff_list as $key => $staff) {

										 		// get amount
										 		    $money = DB::table('tbl_staff_transction')
								                      ->where('st_date_diposit',$var_dateStart)
								                      ->where('s_id',$staff->s_id)
								                      ->where('st_type',21)
								                      ->first();

										 	?>
										 	@if($money)
											 	<tr>
											 		<td>{{$key+1}}</td>
											 		<td style="text-transform: uppercase;">{{$staff->s_name}}</td>

											 		<!-- VN -->

											 			@if($money->st_price_r > 0)
											 				@php 
											 					$money_r_win = $money_r_win + $money->st_price_r;
											 				@endphp
												 			<td>{{number_format($money->st_price_r)}}</td>
												 			<td></td>
												 		@else
												 			@php 
												 				$money_r_lose = $money_r_lose + $money->st_price_r;
												 			@endphp
												 			<td></td>
												 			<td>{{number_format($money->st_price_r)}}</td>
												 		@endif
												 		
												 		@if($money->st_price_d > 0)
												 			@php 
												 				$money_d_win = $money_d_win + floatval($money->st_price_d);
												 			@endphp
												 			<td>{{$money->st_price_d}}</td>
												 			<td></td>
												 		@else
												 			@php 
												 				$money_d_lose = $money_d_lose + floatval($money->st_price_d);
												 			@endphp
												 			<td></td>
												 			<td>{{$money->st_price_d}}</td>
												 		@endif

												 		<!-- <td class="lose"></td>
												 		<td class="lose"></td>
												 		<td class="lose"></td>
												 		<td class="lose"></td> -->
												 	
											 	</tr>
											 @endif
										 	<?php 
										 	}
									}
										 	?>
										 		<tr>
										 			<td colspan="2">សរុប</td>
										 			<td >{{number_format($money_r_win)}}</td>
										 			<td class="lose">{{number_format($money_r_lose)}}</td>
										 			<td >{{$money_d_win}}</td>
										 			<td class="lose">{{$money_d_lose}}</td>
										 			<!-- <td >{{number_format($money_r_win_kh)}}</td>
										 			<td class="lose">{{number_format($money_r_lose_kh)}}</td>
										 			<td >{{number_format($money_d_win_kh)}}</td>
										 			<td class="lose">{{number_format($money_d_lose_kh)}}</td> -->
										 		</tr>
										 		@php
										 			
										 			$total_r_win = $money_r_win + $money_r_lose;
										 			if($total_r_win < 0){
										 				$class = 'class="lose"';
										 			}else{
										 				$class = '';
										 			}

										 			$total_d_win = $money_d_win + $money_d_lose;
										 			if($total_d_win < 0){
										 				$class_d = 'class="lose"';
										 			}else{
										 				$class_d = '';
										 			}


										 			$total_r_win_kh = $money_r_win_kh + $money_r_lose_kh;
										 			if($total_r_win_kh < 0){
										 				$class_kh = 'class="lose"';
										 			}else{
										 				$class_kh = '';
										 			}

										 			$total_d_win_kh = $money_d_win_kh + $money_d_lose_kh;
										 			if($total_d_win_kh < 0){
										 				$class_d_kh = 'class="lose"';
										 			}else{
										 				$class_d_kh = '';
										 			}


										 		
										 		@endphp
										 		<tr>
										 			<td colspan="2">ចំណេញ</td>
										 			<td {{$class}} colspan="2">{{number_format($total_r_win)}}</td>
										 			<td {{$class_d}} colspan="2">{{$total_d_win}}</td>

										 			<!-- <td {{$class_kh}} colspan="2">{{number_format($total_r_win_kh)}}</td>
										 			<td {{$class_d_kh}} colspan="2">{{number_format($total_d_win_kh)}}</td> -->
										 		</tr>

										 		@php
										 			$total_r = $total_r_win + $total_r_win_kh;
										 			$total_d = $total_d_win + $total_d_win_kh;
										 		@endphp

										 		<tr>
										 			<td colspan="2">ចំណេញសរុប</td>
										 			<td {{$class}} colspan="2">{{number_format($total_r)}} ៛</td>

										 			<td {{$class_kh}} colspan="2">{{$total_d}} $</td>
										 		</tr>
										 	
										 </tbody>
									 </table>
						 </div>


						 @endif

					



		    
			        </div>
			        <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">



	$(document).ready(function() {
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
	});
	$(document).ready(function(){

		
	});
	</script>
@stop