<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Change Password</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5, user-scalable=no;user-scalable=0;">
	<link href="{{ asset('/') }}mobile_css/jquery-ui.min.css" rel="stylesheet" />
	<link href="{{ asset('/') }}mobile_css/main_phone.css" type="text/css" rel="stylesheet">
	<link href="{{ asset('/') }}mobile_css/style_phone.css" type="text/css" rel="stylesheet">
	<style>

		* { 
				-webkit-touch-callout: none; 
				-webkit-user-select: none; 
				-khtml-user-select: none; 
				-moz-user-select: none; 
				-ms-user-select: none; 
				user-select: none; 
			} 

			
		#menu_tr_id table td { font-size: 50px; font-family: fontHanuman; font-weight: bold; background-image: -webkit-linear-gradient(top, #f9f7f7, #AAA); } #menu_tr_id table td a { text-decoration: none; display: block; color: #000; padding: 25px 50px; }


	</style>
</head>

<body>
	<table width="100%" border="0">
		<tbody>
			<tr id="menu_tr_id" style="display:none">
				<td>
					<table cellpadding="0" cellspacing="5" width="100%">
						<tbody>
							<tr>
								<td><a href="{{URL::to('/')}}/mobile/client"><i class="fa fa-users" aria-hidden="true"></i> ភ្នាក់ងារ</a>
								</td>
								<td><a href="{{URL::to('/')}}/mobile/post"><i class="fa fa-desktop" aria-hidden="true"></i> ប៉ុស្ត៏</a>
								</td>
							</tr>
							<tr>
								<td><a href="{{URL::to('/')}}/mobile/timeclose"><i class="fa fa-times" aria-hidden="true"></i> ម៉ោងបិទ</a>
								</td>
								<td>
								<a href="{{URL::to('/')}}/mobile/changePassword"><i class="fa fa-key" aria-hidden="true"></i> ប្ដូរពាក្យសំងាត់</a>
								</td>
							</tr>
							<tr>
								<td><a href="{{URL::to('/')}}/mobile/checkresult"><i class="fa fa-trophy" aria-hidden="true"></i> ផ្ទៀងលេខត្រូវ </a>
								</td>
								<td><a href="{{URL::to('/')}}/mobile/sary"><i class="fa fa-plug" aria-hidden="true"></i> សារី</a>
								</td>
							</tr>
							<tr>
								<td><a href="{{URL::to('/')}}/mobile/dailyReport"><i class="fa fa-list-ol" aria-hidden="true"></i> ក្បាលបញ្ជី</a>
								</td>
								<td><a href="{{URL::to('/')}}/mobile/checkreportdata"><i class="fa fa-check-square-o" aria-hidden="true"></i> ពិនិត្យបញ្ជី</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href="{{URL::to('/')}}/mobile/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> ចេញ</a>
								</td>
								<td><a href="{{URL::to('/')}}/mobile/transctionstaff"><i class="fa fa-exchange" aria-hidden="true"></i> ស៊ីសង</a>
								</td>
							</tr>
							<tr>
								<td class="btn_show_menu">
									<a href="javascript:void(0);"><i class="fa fa-play" aria-hidden="true"></i> ចាក់ឆ្នោត</a>
								</td>
								<td>
									<a href="{{URL::to('/')}}/mobile/howtoplay"><i class="fa fa-question-circle" aria-hidden="true"></i> របៀបចាក់</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr valign="top">
				<td>
					<table align="left">
						<tbody>
							<tr valign="left">
								<td rowspan="4" width="200" style="vertical-align:top">
									<input type="button" value="បញ្ជី" onclick="show_menus()" class="inputbts">
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<form name="frm_login" method="post">
						<table align="center">
							<tbody>
								<tr>
									<td style="font-size:40px;">ដូរលេខសម្ងាត់ថ្មី:</td>
								</tr>
								<tr>
									<td>
										<input class="input" style="font-size:60px; width:450px; height:65px; border: 1px solid #777;" type="password" name="mdp" readonly="readonly" onfocus="current_focus=0">
									</td>
								</tr>
								<tr>
									<td style="font-size:40px;">លេខសុវវត្ថិភាព:</td>
								</tr>
								<tr>
									<td>
										<input class="input" style="font-size:60px; width:450px; height:65px;border: 1px solid #777;" type="password" name="secure8" value="" readonly="readonly" onfocus="current_focus=1">
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</td>
				<td id="tab_key_l" style="display:none">
					<table cellspacing="0" cellpadding="0" align="center">
						<tbody>
							<tr>
								<td id="keynum_7l">
									<input class="keyb" type="button" value="7" onfocus="focus_at();">
								</td>
								<td id="keynum_8l">
									<input class="keyb" type="button" value="8" onfocus="focus_at();">
								</td>
								<td id="keynum_9l">
									<input class="keyb" type="button" value="9" onfocus="focus_at();">
								</td>
							</tr>
							<tr>
								<td id="keynum_4l">
									<input class="keyb" type="button" value="4" onfocus="focus_at();">
								</td>
								<td id="keynum_5l">
									<input class="keyb" type="button" value="5" onfocus="focus_at();">
								</td>
								<td id="keynum_6l">
									<input class="keyb" type="button" value="6" onfocus="focus_at();">
								</td>
							</tr>
							<tr>
								<td id="keynum_1l">
									<input class="keyb" type="button" value="1" onfocus="focus_at();">
								</td>
								<td id="keynum_2l">
									<input class="keyb" type="button" value="2" onfocus="focus_at();">
								</td>
								<td id="keynum_3l">
									<input class="keyb" type="button" value="3" onfocus="focus_at();">
								</td>
							</tr>
							<tr>
								<td id="keynum_0l">
									<input class="keyb" type="button" value="0" onfocus="focus_at();">
								</td>
								<td id="back_lastl">
									<input class="keyb" type="button" value=" " style="background:url('../img/back_arrow.png') center #FFF no-repeat; border:1px solid #666; background-size: 80px 80px; " onfocus="focus_at();">
								</td>
								<td id="enter_keyl">
									<input class="keyb" type="button" value="↵" onfocus="focus_at();">
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" id="tab_key_v">
					<table cellspacing="0" cellpadding="0" align="center">
						<tbody>
							<tr>
								<td id="keynum_7">
									<input class="keybn" type="button" value="7" onfocus="focus_at();">
								</td>
								<td id="keynum_8">
									<input class="keybn" type="button" value="8" onfocus="focus_at();">
								</td>
								<td id="keynum_9">
									<input class="keybn" type="button" value="9" onfocus="focus_at();">
								</td>
							</tr>
							<tr>
								<td id="keynum_4">
									<input class="keybn" type="button" value="4" onfocus="focus_at();">
								</td>
								<td id="keynum_5">
									<input class="keybn" type="button" value="5" onfocus="focus_at();">
								</td>
								<td id="keynum_6">
									<input class="keybn" type="button" value="6" onfocus="focus_at();">
								</td>
							</tr>
							<tr>
								<td id="keynum_1">
									<input class="keybn" type="button" value="1" onfocus="focus_at();">
								</td>
								<td id="keynum_2">
									<input class="keybn" type="button" value="2" onfocus="focus_at();">
								</td>
								<td id="keynum_3">
									<input class="keybn" type="button" value="3" onfocus="focus_at();">
								</td>
							</tr>
							<tr>
								<td id="keynum_0">
									<input class="keybn" type="button" value="0" onfocus="focus_at();">
								</td>
								<td id="back_last" onmousedown="back_delete_last(window.event);">
									<input class="keybn" type="button" value=" " style="background:url('../img/back_arrow.png') center #FFF no-repeat; border:1px solid #666; background-size: 80px 80px;" onfocus="focus_at();">
								</td>
								<td id="enter_key">
									<input class="keybn" type="button" value="↵" onfocus="focus_at();">
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<div id="updated" style="display:none">ពាក្យសំងាត់បានប្ដូរហើយ</div>
	<div id="error" style="display:none">ពាក្យសំងាត់មិនបានប្ដូរទេ</div>
	<script src="{{ asset('/') }}mobile_js/jquery-1.10.2.min.js"></script>
	<script src="{{ asset('/') }}mobile_js/jquery.validate.js"></script>
	<script src="{{ asset('/') }}mobile_js/jquery-ui.min.js"></script>
	<script src="{{ asset('/') }}mobile_js/changepassword.min.js"></script>
</body>

</html>