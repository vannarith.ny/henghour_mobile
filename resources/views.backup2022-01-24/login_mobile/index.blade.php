
<!-- saved from url=(0034)http://13.250.35.236/Account/Login -->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Login</title>

		<!-- <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5, user-scalable=no;user-scalable=0;"> -->

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link href="{{ asset('/') }}/mobile_css/khmer-font.css" rel="stylesheet" />

		<style> 
			* { 
				-webkit-touch-callout: none; 
				-webkit-user-select: none; 
				-khtml-user-select: none; 
				-moz-user-select: none; 
				-ms-user-select: none; 
				user-select: none; 
			} 
			table#main_id{
				height: auto !important;
			}
			table td{
				font-family: fontBattambong;
				font-size: 22px;
			}

			table.tableKey{
				padding-top: 10px;
			}

			table.tableKey tr{
				height: 94px !important;
			}
		</style>
		<link href="{{ asset('/') }}/Login_files/style_login_phone.css" rel="stylesheet">
	</head>
	<body bgcolor="#0065ae" style="vertical-align:middle; color: #FFF;" onclick="focus_at();" cz-shortcut-listen="true">
		<table width="100%" height="100%" id="main_id" cellspacing="0" cellpadding="0" >
			<tbody>
				<tr>
					<td valign="top" align="center" >
						@include('flash::message')
						<table align="center" style="width: 96%;"> 
							{!! Form::open(['route' => 'login_mobile.store', 'files' => true , 'novalidate' => 'validate', 'id' => 'frm_login', 'name' => 'frm_login', 'class'=>'']) !!}

								<tbody>
									<tr>
										<th height="40" colspan="2" align="center" style="font-size:26px">
											ចូលគណនី
										</th>
									</tr>
									<tr valign="middle">
										<td align="left" nowrap="nowrap" style="font-size:22px; padding-top: 10px;">
											លេខទូរសព័្ទ &nbsp;:&nbsp;
										</td>
										<td align="left" style="width: 70%; padding-top: 10px;"> 
											<input value="{{old('UserName')}}" class="inputl" data-val="true" data-val-required="The UserName field is required." id="UserName" maxlength="64" name="UserName" onfocus="current_focus=0" readonly="readonly" style="width: 100%;" type="text"> 
										</td>
									</tr>
									<tr valign="middle">
										<td align="left" nowrap="nowrap" style="font-size:22px"> 
											លេខសម្ងាត់ &nbsp;:&nbsp;
										</td>
										<td align="left" style="width: 70%;"> 
											<input class="inputl" data-val="true" data-val-required="The Password field is required." value="{{old('Password')}}" id="Password" maxlength="32" name="Password" onfocus="current_focus=1" readonly="readonly" style="width: 100%;" type="password">
										</td>
									</tr>
									<tr style="display:none">
										<td align="right">
											<input type="submit" value="ចូល" onclick="return testclick();">
										</td>
										<td align="center">
											<input type="reset" value="ទៅសភាពដើម" onclick="document.frm_login.reset();">
										</td>
									</tr>
								</tbody> 
							</form>
						</table>
					</td>
					<td width="50%" style="vertical-align: bottom; text-align: right; display: none;" id="tab_key_l">
						<table cellspacing="6" cellpadding="0" align="right" width="100%">
							<tbody>
								<tr id="tr_mnl_1">
									<td width="33%" class="btntd" id="keynum_7l">7</td>
									<td width="33%" class="btntd" id="keynum_8l">8</td>
									<td width="34%" class="btntd" id="keynum_9l">9</td>
								</tr>
								<tr id="tr_mnl_2">
									<td class="btntd" id="keynum_4l">4</td>
									<td class="btntd" id="keynum_5l">5</td>
									<td class="btntd" id="keynum_6l">6</td>
								</tr>
								<tr id="tr_mnl_3">
									<td class="btntd" id="keynum_1l">1</td>
									<td class="btntd" id="keynum_2l">2</td>
									<td class="btntd" id="keynum_3l">3</td>
								</tr>
								<tr id="tr_mnl_4">
									<td class="btntd" id="keynum_0l">0</td>
									<td class="btntd" id="back_lastl">
										<img src="./Login_files/back_arrow.png">
									</td>
									<td class="btntd" id="enter_keyl">↵</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tab_key_v" colspan="2" style="vertical-align:bottom">
						<table cellspacing="10" cellpadding="0" align="right" width="100%" class="tableKey">
							<tbody>
								<tr id="tr_mnv_1" style="height: 139px;">
									<td class="btntd" width="33%" id="keynum_7">7</td>
									<td class="btntd" width="33%" id="keynum_8">8</td>
									<td class="btntd" width="33%" id="keynum_9">9</td>
								</tr>
								<tr id="tr_mnv_2" style="height: 139px;">
									<td class="btntd" id="keynum_4">4</td>
									<td class="btntd" id="keynum_5">5</td>
									<td class="btntd" id="keynum_6">6</td>
								</tr>
								<tr id="tr_mnv_3" style="height: 139px;">
									<td class="btntd" id="keynum_1">1</td>
									<td class="btntd" id="keynum_2">2</td>
									<td class="btntd" id="keynum_3">3</td>
								</tr>
								<tr id="tr_mnv_4" style="height: 139px;">
									<td class="btntd" id="keynum_0">0</td>
									<td class="btntd" id="back_last">
										<img src="./Login_files/back_arrow.png" back_delete_last(window.event)="">
									</td>
									<td class="btntd" id="enter_key">↵</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>

		<script> 
			var current_focus = 0; 
		</script>
		<script src="{{ asset('/') }}/Login_files/login.min.js"></script>
		<!-- <script src="{{ asset('/') }}/Login_files/inspect.min.js"></script> -->
		<script> 
			
		</script>
	</body>
</html>