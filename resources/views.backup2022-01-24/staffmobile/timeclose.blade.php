@extends('master')

@section('title')
<title>{{trans('label.addStaff')}}</title>
@stop

@section('cssStyle')
	<style type="text/css">
    	*:focus {
		    outline: none;
		}
    	input.inputbt {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 24px;
			    padding: 15px 20px;
			    border: 2px #ccc solid;
			    line-height: 24px;
			    border-radius:10px;
    	}

    	input.inputbt.active{
    		background-color: #ccc;

    	}
    	#main_table{
    		padding-top: 20px;
    	}
    	input[type=checkbox], input[type=radio]{
    		zoom:2;
    		margin-top: 5px;
    	}
    	.stylecontent td{
    		font-family: Verdana, Geneva, sans-serif;
    		font-size: 20px;
    	}

    	.timeTableStyle{
    		padding-left: 10px;
    	}

    	.classtd1 {
		    font-size: 13px;
		    padding-left: 7px;
		    padding-right: 7px;
		    border-top: 1px solid rgb(183, 194, 208);
		    border-bottom: 1px solid rgb(183, 194, 208);
		    border-right: 1px solid rgb(183, 194, 208);
		}


    	.timeTableStyle tr:nth-child(even) {background: #CCC}
		.timeTableStyle tr:nth-child(odd) {background: #D0D8FF}

    	.titleTime{
    		background: #006699 !important;
    		color: #FFF;
    		border-color: #FFF;
    	}
    	.titleTime th{
    		padding-top: 10px;
    		padding-bottom: 10px;
    		font-size: 20px !important;
    	}
    	.timeTableStyle tbody td{
    		padding-top: 10px;
    		padding-bottom:10px;
    		font-weight: bold;
    		font-size: 18px !important;
    	}
    	.currentDay{
    		background: #FFC133;
    	}

    	.styleinpiut{
    		width: 120px;
    		border:none;
    		background: none;
    		cursor: pointer;
    	}
    	input.styleinpiut:focus {
    		background:#FFF; 
    	}
    	.alert{
    		display: none;
    	}

    </style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li>{{$staff->s_phone_login}}</li><li>Time Close</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> 
		        {{$staff->s_phone_login}} > Time Close
		      </h1>
		     </div>
		</div>

		<div class="widget-body ">  
			<div class="alert alert-success" role="alert"></div>
			<div class="alert alert-danger" role="alert"></div>
			<table cellpadding="0" cellspacing="0" width="100%" class="timeTableStyle">
				<thead align="center">
					<tr align="center" class="titleTime">
						<th class="classtd1">ព្រឹក</th>
						@foreach ($daly as $day)
							<th class="classtd1">{{$day}}</th>
						@endforeach

					</tr>
				</thead>
				<tbody class="">

					@php 
						$queryAfter = $times->where('sheet_id',23)->groupBy('p_id');
					@endphp
					@foreach ($queryAfter as $querypos)
					<tr>
							
						@foreach ($querypos as $key => $data)
							@if($key == 0)
							<td class="classtd1">{{$data->pos_name}}</td>
							@endif
							<td class="classtd1 @if($key == $currentDay ) currentDay @endif">
								<input type="text" name="timeclose" class="timeCloseControl styleinpiut" value="{{ date('H:i', strtotime($data->t_time)) }}" data-id="{{$data->t_id}}">
							</td>
						@endforeach
					</tr>
					@endforeach

					<!-- night -->
					<tr align="center" class="titleTime">
						<th class="classtd1">ថ្ងៃ</th>
						@foreach ($daly as $day)
							<th class="classtd1">{{$day}}</th>
						@endforeach

					</tr>
					@php 
						$queryAfter = $times->where('sheet_id',24)->groupBy('p_id');
					@endphp
					@foreach ($queryAfter as $querypos)
					<tr>
							
						@foreach ($querypos as $key => $data)
							@if($key == 0)
							<td class="classtd1">{{$data->pos_name}}</td>
							@endif
							<td class="classtd1 @if($key == $currentDay ) currentDay @endif">
								<input type="text" name="timeclose" class="timeCloseControl styleinpiut" value="{{ date('H:i', strtotime($data->t_time)) }}" data-id="{{$data->t_id}}">
							</td>
						@endforeach
					</tr>
					@endforeach


					<tr align="center" class="titleTime">
						<th class="classtd1">ល្ងាច</th>
						@foreach ($daly as $day)
							<th class="classtd1">{{$day}}</th>
						@endforeach

					</tr>

					@php 
						$queryAfter = $times->where('sheet_id',5)->groupBy('p_id');
					@endphp
					@foreach ($queryAfter as $mainKey => $querypos)
					<tr>
							
						@foreach ($querypos as $key => $data)
							@if($key == 0)
							<td class="classtd1" >{{$data->pos_name}}</td>
							@endif
							<td class="classtd1 @if($key == $currentDay ) currentDay @endif" >
								
								<input type="text" name="timeclose" class="timeCloseControl styleinpiut" value="{{ date('H:i', strtotime($data->t_time)) }}" data-id="{{$data->t_id}}" pattern="([01]?[0-9]|2[0-3])(:[0-5][0-9]){2}" 
    							required="required" placeholder="hh:mm">
							</td>
						@endforeach
					</tr>
					@endforeach


					<!-- night -->
					<tr align="center" class="titleTime">
						<th class="classtd1">យប់</th>
						@foreach ($daly as $day)
							<th class="classtd1">{{$day}}</th>
						@endforeach

					</tr>
					@php 
						$queryAfter = $times->where('sheet_id',6)->groupBy('p_id');
					@endphp
					@foreach ($queryAfter as $querypos)
					<tr>
							
						@foreach ($querypos as $key => $data)
							@if($key == 0)
							<td class="classtd1">{{$data->pos_name}}</td>
							@endif
							<td class="classtd1 @if($key == $currentDay ) currentDay @endif">
								<input type="text" name="timeclose" class="timeCloseControl styleinpiut" value="{{ date('H:i', strtotime($data->t_time)) }}" data-id="{{$data->t_id}}">
							</td>
						@endforeach
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>



		
    	

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script src="{{ asset('/') }}js/plugin/jquery-form/jquery-form.min.js"></script>
	<script type="text/javascript">


		$(document).ready(function() {

			function validateTime(value){
				re = /^(\d{1,2}):(\d{2})([ap]m)?$/;

			    if(value != '') {
			      	if(regs = value.match(re)) {
				        if(regs[3]) {
				          // 12-hour value between 1 and 12
				          if(regs[1] < 1 || regs[1] > 12) {
				            alert("Invalid value for hours: " + regs[1]);
				            return false;
				          }
				        } else {
				          // 24-hour value between 0 and 23
				          if(regs[1] > 23) {
				            alert("Invalid value for hours: " + regs[1]);
				           
				            return false;
				          }
				        }
				        // minute value between 0 and 59
				        if(regs[2] > 59) {
				          alert("Invalid value for minutes: " + regs[2]);
				          return false;
				        }
				      } else {
				        alert("Invalid time format: " + value);
				        return false;
				    }
			    }
			    return true;
			}

		  	$(document).on('change', '.timeCloseControl', function() {

		        var value = $(this).val();
		        var id = $(this).attr('data-id');
		        $(".alert").hide();
		        // alert(value);
		        if(validateTime(value)){
		        	$.ajax({
			            type: "get",
			            url: 'updatetimeclose',
			            contentType: "application/json; charset=utf-8",
			            dataType: "json",
			            data: { id: id, value: value },
			            success: function (data) {
			                if (data.status == 'success') {
			                    $(".alert.alert-success").show();
			                    $(".alert.alert-success").text(data.msg);
			                }
			            },
			            error: function (data) { 
			            	$(".alert.alert-danger").show();
			                $(".alert.alert-danger").text(data.msg);
			            }

			        });
		        }else{
		        	$(this).focus();
		        }
		        
		    })
		 
		   
		});
		function btnCancel(){
	   		var r = confirm("តើលោកអ្នកចង់ត្រលប់ក្រោយឬទេ?");
		    if (r == true) {
		        document.location.href='{{URL::to('/')}}/staff';
		    }
	    }
	</script>
@stop