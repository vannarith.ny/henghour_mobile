@extends('master')
<?php
use App\Http\Controllers\SaleController;

// $lottery_pay = DB::('tbl_staff_charge')->where('')
?>
@section('title')
<title>របាយការណ៏</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}
		th.lose, td.lose{
			color: red !important;
		}
		th.blue, td.blue{
			color: blue !important;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'reportphoneFilter', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_page))
							 <?php $page_filter = $var_page; ?>
							 <?php $attr_page = $var_page; ?>
						 @else
							 <?php $page_filter = null; ?>
							 <?php $attr_page = 0; ?>
						 @endif

						 @if(isset($var_sheet))
							 <?php $sheet_filter = $var_sheet; ?>
							 <?php $attr_sheet = $var_page; ?>
						 @else
							 <?php $sheet_filter = null; ?>
							 <?php $attr_sheet = 0; ?>
						 @endif
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>
								 <input type="hidden" name="type_lottery" id="type_lottery" value="1">

							 </section>

							 <!-- <section class="col col-2">
								 <label class="select">
									 {{ Form::select('type_lottery', ([
                                        '' => trans('result.chooseTypeLottery') ]+$type_lottery),$var_type_lottery,['class' => 'required ','id'=>'type_lottery','sms'=> trans('result.pleaseChooseTypeLottery') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section> -->

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('staff', ([
                                        '' => trans('result.chooseStaff') ]+$staff),$staff_filter,['class' => 'required ','id'=>'staff','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('sheet', ([
                                        '' => trans('result.chooseShift') ]+$sheets),$sheet_filter,['class' => ' ','id'=>'sheet','sms'=> trans('result.pleaseChooseShift') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>



							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('page', ([
                                        '' => trans('result.choosePage') ]+$pages),$page_filter,['class' => ' ','id'=>'pages','sms'=> trans('result.pleaseChoosePage') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 @if(isset($report))

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 
							 
						 </style>



						 <div class="widget-body no-padding control_width">

							 <div class="">
								 <div class="">
									 <?php
									 	$page_id = 0;
										 $sum_total_twodigit_r = 0;
										 $sum_total_twodigit_s = 0;
										 $sum_total_threedigit_r = 0;
										 $sum_total_threedigit_s = 0;
										 $controlTotal = count($report)-1;

										 $sum_price_right_twodigit_r = 0;
										 $sum_price_right_twodigit_s = 0;
										 $sum_price_right_threedigit_r = 0;
										 $sum_price_right_threedigit_s = 0;


									 ?>
									 @foreach($report as $key => $rowlist)

										 @if($page_id != $rowlist->p_id)

												 {{--#display total amount by page--}}
											 	@if($key > 0)
													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_2_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_twodigit_s,2)}} $</span></b>
													 </div>
													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_3_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_threedigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_2_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_twodigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_3_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_threedigit_s,2)}} $</span></b>
													 </div>



													<input type="hidden" id="page_{{$key}}" class="report_total"
														   id_staff = "{{$rowlist->s_id}}"
														   time="{{$rowlist->pav_value}}"
														   staff="{{$rowlist->s_name}}"
														   page="{{$rowlist->p_number}}" 
														   page_id="{{$rowlist->p_id}}"

														   total2digitR="{{$sum_total_twodigit_r}}"
														   total2digitS="{{$sum_total_twodigit_s}}"
														   total3digitR="{{$sum_total_threedigit_r}}"
														   total3digitS="{{$sum_total_threedigit_s}}"
														   total2digitRright="{{$sum_price_right_twodigit_r}}"
														   total2digitSright="{{$sum_price_right_twodigit_s}}"
														   total3digitRright="{{$sum_price_right_threedigit_r}}"
														   total3digitSright="{{$sum_price_right_threedigit_s}}" >

												 @endif

												 <?php
												 $sum_total_twodigit_r = 0;
												 $sum_total_twodigit_s = 0;
												 $sum_total_threedigit_r = 0;
												 $sum_total_threedigit_s = 0;

												 $sum_price_right_twodigit_r = 0;
												 $sum_price_right_twodigit_s = 0;
												 $sum_price_right_threedigit_r = 0;
												 $sum_price_right_threedigit_s = 0;
												 ?>


												 </div>
							 				</div>
							 					{{--<button class="btn_print" attr_id="print_page_{{$key}}" attr_date="{{$var_dateStart}}" attr_staff="{{$var_staff}}" attr_sheet="{{$attr_sheet}}" attr_page="{{$attr_page}}">{{trans('label.download_this')}}</button>--}}

											 <div id="print_page_{{$key}}" class="paperStyle" >

													 <div class="col-md-3">
														 {{trans('label.staff_name')}} :
														 <b>{{$rowlist->s_name}} </b>
													 </div>

													 <div class="col-md-2">
														 {{trans('label.date')}} :
														 <b>{{$rowlist->p_date}}</b>
													 </div>

													 <div class="col-md-2">
														 {{trans('label.number_paper')}} :
														 <b>{{$rowlist->p_number}}</b>
													 </div>

													 <div class="col-md-2">
														 {{trans('label.time')}} :
														 <b>
														 	<?php 
														 		$find = array("0","1","2","3");
																$replace = array("");
																echo str_replace($find,$replace,$rowlist->pav_value);
														 	?>
														 </b>
													 </div>
													 <!-- <div class="col-md-2">
														 កែប្រែ :
														 <a href="{{URL::to('/')}}/salephone/{{$rowlist->p_id}}/edit" target="_blank">Click</a>
													 </div> -->

												 <?php 
												 		$find = array("0","1","2","3");
														$replace = array("");
														$timeDisplay =  str_replace($find,$replace,$rowlist->pav_value);
												 ?>
												 <input type="hidden" id="page_info_{{$key}}" class="report_total_info"
														time="{{$timeDisplay}}"
														staff="{{$rowlist->s_name}}"
														id_staff="{{$rowlist->s_id}}"
														page="{{$rowlist->p_number}}"
														page_id="{{$rowlist->p_id}}"
														 >

												 <div class="clearfix"></div>
												 <br>
												 <div class=" controlColume" style="width: 5000px;">
											 <?php
											 	$page_id = $rowlist->p_id;
											 ?>
										 @endif

										 <div class="colume_style" id="Remove_{{$rowlist->r_id}}">
											 <?php
											 $total_twodigit_r = 0;
											 $total_threedigit_r = 0;
											 $total_twodigit_s = 0;
											 $total_threedigit_s = 0;

											 $price_right_twodigit_s = 0;
											 $price_right_twodigit_r = 0;
											 $price_right_threedigit_s = 0;
											 $price_right_threedigit_r = 0;
											 ?>

											 <!-- block list number lottery -->
											 <?php
											 $numberLotterys = DB::table('tbl_number_mobile')
													 ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
													 ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
													 ->where('tbl_number_mobile.r_id',$rowlist->r_id)
													 ->orderBy('tbl_number_mobile.num_sort','ASC')
													 ->orderBy('tbl_number_mobile.num_id','ASC')
													 ->get();
//											 dd($numberLotterys);
											 $storeOrder = 0;
											 ?>
											 @foreach($numberLotterys as $number)


												 @if($storeOrder != $number->num_sort)
														 <?php
														 $storeOrder = $number->num_sort;

														 ?>
														 <div class="pos_style" id="pos_style_{{$number->num_sort}}">{{$number->g_name}}</div>
												 @endif







											 <?php
												 $numberDisplay = '';  //display number to fromte
												 $displayCurrency = '';

												//	Resulte price by row
												 $price_result = App\Model\Report::calculate( $number->num_number,$number->num_end_number,$number->num_sym,$number->num_reverse,$number->num_price,$number->num_currency,$number->g_id,$rowlist->p_time,$rowlist->p_date,$number->num_type);
												 $val_price = explode("-", $price_result);
												 $num_right = "";
												 if($val_price[2] > 0){

													 $actived = 'actived';
													 
													 if($val_price[2]>1){
													 	$num_right = '<div class="right_num"><b>* '.$val_price[2].'</b></div>';
													 }

												 }else{
													 $actived = '';
												 }

												 if($number->num_currency == 2){
													 $currencySym = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
													 $displayCurrency = ' '.$currencySym->pav_value;
													 if($val_price[1]=='2'){
														 $total_twodigit_s = $total_twodigit_s + $val_price['0'];
														 $price_right_twodigit_s = $price_right_twodigit_s + ($number->num_price * $val_price[2]);
													 }else{
														 $total_threedigit_s = $total_threedigit_s + $val_price['0'];
														 $price_right_threedigit_s = $price_right_threedigit_s + ($number->num_price * $val_price[2]);
													 }

												 }else{

													 if($val_price[1]=='2'){
														 $total_twodigit_r = $total_twodigit_r + $val_price['0'];
														 $price_right_twodigit_r = $price_right_twodigit_r + ($number->num_price * $val_price[2]);
													 }else{
														 $total_threedigit_r = $total_threedigit_r + $val_price['0'];
														 $price_right_threedigit_r = $price_right_threedigit_r + ($number->num_price * $val_price[2]);
													 }


												 }



												 if($number->num_sym == 7){ //check sym is -
													 $numberDisplay .='
													<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

														<div class="number_lot lot_int">'.$number->num_number.'</div>';
													 if($number->num_reverse == 1){
														 $numberDisplay .= '<div class="symbol_lot lot_int">x</div>';
													 }else{
														 $numberDisplay .= '<div class="symbol_lot lot_int">-</div>';
													 }

													 $numberDisplay .= '
														<div class="amount_lot lot_int">'.round($number->num_price,2).$displayCurrency.'</div>
														<div class="clear"></div>
														'.$num_right.'
													</div>
													';
												 }else if($number->num_sym == 8){

													 if($number->num_reverse == 1){
														 $end_number_new = '';
														 if($number->num_end_number == ''){
															 $end_number_new = substr($number->num_number, 0, -1).'9';
														 }else{
															 $end_number_new = $number->num_end_number;
														 }
														 $numberDisplay .='
														<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

															<div class="number_lot lot_int">'.$number->num_number.'</div>
															<div class="symbol_lot lot_int">x</div>
															<div class="clear"></div>
															<div class="display_total_number">'.SaleController::calculate_number($number->num_number,$end_number_new,1).'</div>
															<div class="number_lot lot_int clear_margin">
																|
																<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
															</div>
															<div class="clear"></div>
															<div class="number_lot lot_int">'.$end_number_new.'</div>
															<div class="symbol_lot lot_int">x</div>
															<div class="clear"></div>
															'.$num_right.'
														</div>
														';
													 }else{
														 $check = substr($number->num_number, -1);
														 if($check == '0' && $number->num_end_number == ''){
															 $numberDisplay .='
																<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																	<div class="number_lot lot_int">'.$number->num_number.'</div>
																	<div class="symbol_lot lot_int"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class="amount_lot lot_int">'.round($number->num_price,2).$displayCurrency.'</div>
																	<div class="clear"></div>
																	'.$num_right.'
																</div>
																';
														 }else{
															 $end_number_new = '';
															 if($number->num_end_number == ''){
																 $end_number_new = substr($number->num_number, 0, -1).'9';
															 }else{
																 $end_number_new = $number->num_end_number;
															 }
															 $numberDisplay .='
															<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																<div class="number_lot lot_int">'.$number->num_number.'</div>
																<div class="clear"></div>
																<div class="display_total_number">'.SaleController::calculate_number($number->num_number,$end_number_new,0).'</div>
																<div class="number_lot lot_int clear_margin">
																	|
																	<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
																</div>
																<div class="clear"></div>
																<div class="number_lot lot_int">'.$end_number_new.'</div>
																<div class="clear"></div>
																'.$num_right.'
															</div>
															';
														 }

													 }
												}else if($number->num_sym == 10){ //check sym is alot digit to 3 digit
									                $numberDisplay .='
									                <div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">
									                    
									                    <div class="number_lot lot_int newdigitalot">'.$number->num_number.'</div>';
									                    if($number->num_reverse == 1){
									                        $numberDisplay .= '<div class="symbol_lot lot_int ">x</div>';
									                    }else{
									                        $numberDisplay .= '<div class="symbol_lot lot_int ">-</div>';
									                    }
									                    
									                $numberDisplay .= '
									                    <div class="amount_lot lot_int newdigitalot_price">'.round($number->num_price,2).$displayCurrency.'</div>
									                    <div class="clear"></div>
									                    '.$num_right.'
									                </div>
									                '; 
									            }else if($number->num_sym == 11){ //check sym is alot digit to 3 digit
									            	$numberDisplay .='
									                <div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">';
									                $explotData = explode("/", $number->num_number);
									                $leftData = '';
									                $centerData = '';
									                $rightData = '';
									                if(isset($explotData[0])){
									                	$explotData_sub = explode(",", $explotData[0]);
									                	foreach ($explotData_sub as $key0 => $value_sub) {
									                		if($value_sub != ''){
									                			$leftData .='<li>'.$value_sub.'</li>';
									                		}
									                	}
									                }
								                	
									                if(isset($explotData[1])){
									                	$explotData_sub = explode(",", $explotData[1]);
									                	foreach ($explotData_sub as $key1 => $value_sub) {
									                		if($value_sub != ''){
									                			$centerData .='<li>'.$value_sub.'</li>';
									                		}
									                		
									                	}
									                }

									                if(isset($explotData[2])){
									                	$explotData_sub = explode(",", $explotData[2]);
									                	foreach ($explotData_sub as $key2 => $value_sub) {
									                		if($value_sub != ''){
									                			$rightData .='<li>'.$value_sub.'</li>';
									                		}
									                	}
									                }

									                if($leftData == ''){
												        $leftData = $centerData;
												        $centerData = $rightData;
												        $rightData = '';
												    }else if($centerData == ''){
												        $centerData = $rightData;
												        $rightData = '';
												    }

									                $numberDisplay .= '
									                	<div class="row_lottery_list_new">
										                    <div class="txt_left">
										                        <ul>
										                        	'.$leftData.'
										                        </ul>
										                    </div>
										                    <div class="txt_center">
										                        <ul>
										                        	'.$centerData.'
										                        </ul>
										                    </div>
										                    <div class="txt_right">
										                       <ul>
										                        	'.$rightData.'
										                        </ul>
										                    </div>
										                    <div class="clear"></div>
										                </div>
									                ';
									                   
									                    
									                $numberDisplay .= '
									                    <div class="number_lot lot_int newdigitalot_price">'.SaleController::calculate_totalnumber_new($number->num_number).'</div>
									                    <div class="symbol_lot lot_int ">x</div>
									                    <div class="amount_lot lot_int newdigitalot_price">'.round($number->num_price,2).$displayCurrency.'</div>
									                    <div class="clear"></div>
									                    '.$num_right.'
									                </div>
									                '; 

												 }else{
													 if($number->num_reverse == 1){
														 $numberDisplay .='
															<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																<div class="number_lot lot_int">'.$number->num_number.'</div>
																<div class="symbol_lot lot_int">x</div>
																<div class="clear"></div>
																<div class="number_lot lot_int clear_margin">
																	|
																	<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
																</div>
																<div class="clear"></div>
																<div class="number_lot lot_int">'.$number->num_end_number.'</div>
																<div class="symbol_lot lot_int">x</div>
																<div class="clear"></div>
																'.$num_right.'
															</div>
															';
													 }else{
														 $numberDisplay .='
															<div class="row_main '.$actived.'" id="row_main_'.$number->num_id.'">

																<div class="number_lot lot_int">'.$number->num_number.'</div>
																<div class="clear"></div>
																<div class="number_lot lot_int clear_margin">
																	|
																	<div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
																	<div class=" lot_int sym_absolube_amount">'.round($number->num_price,2).$displayCurrency.'</div>
																</div>
																<div class="clear"></div>
																<div class="number_lot lot_int">'.$number->num_end_number.'</div>
																<div class="clear"></div>
																'.$num_right.'
															</div>
															';
													 }
												 }
												 echo $numberDisplay;
												 ?>
											 @endforeach
											 <div class="clearfix"></div>

											 <?php 
											 	$sumamount_r = $total_twodigit_r + $total_threedigit_r;
											 	$sumamount_s = $total_twodigit_s + $total_threedigit_s;
											 ?>

											 <div class="result_price_top" id="row_result_2_{{$rowlist->r_id}}">					{{number_format($total_twodigit_r)}} ៛  
											 	<span class="pull-right">{{round($total_twodigit_s,2)}} $</span>
											 </div>

											 <div class="result_price" id="row_result_3_{{$rowlist->r_id}}">{{number_format($total_threedigit_r)}} ៛  <span class="pull-right">{{round($total_threedigit_s,2)}} $</span></div>

											 <div class="result_price_main" id="row_result_3_{{$rowlist->r_id}}">{{number_format($sumamount_r)}} ៛  <span class="pull-right">{{round($sumamount_s,2)}} $</span></div>


										 </div>
										 		 <?php $sum_total_twodigit_r = $sum_total_twodigit_r + $total_twodigit_r;?>
												 <?php $sum_total_twodigit_s = $sum_total_twodigit_s + $total_twodigit_s;?>
												 <?php $sum_total_threedigit_r = $sum_total_threedigit_r + $total_threedigit_r;?>
												 <?php $sum_total_threedigit_s = $sum_total_threedigit_s + $total_threedigit_s;?>

												 <?php $sum_price_right_twodigit_r = $sum_price_right_twodigit_r + $price_right_twodigit_r;?>
												 <?php $sum_price_right_twodigit_s = $sum_price_right_twodigit_s + $price_right_twodigit_s;?>
												 <?php $sum_price_right_threedigit_r = $sum_price_right_threedigit_r + $price_right_threedigit_r;?>
												 <?php $sum_price_right_threedigit_s = $sum_price_right_threedigit_s + $price_right_threedigit_s;?>



												 {{--#display total amount by page--}}
												 @if($controlTotal == $key)


													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_2_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_twodigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_3_digit')}} :
														 <b><span class="val_r">{{number_format($sum_total_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_total_threedigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_2_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_twodigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_twodigit_s,2)}} $</span></b>
													 </div>

													 <div class="col-md-6 display_result">
														 {{trans('label.total_price_number_3_digit_right')}} :
														 <b><span class="val_r">{{number_format($sum_price_right_threedigit_r)}} ៛</span></b>
														 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														 <b><span class="val_s">{{round($sum_price_right_threedigit_s,2)}} $</span></b>
													 </div>

													 <input type="hidden" id="page_{{$key}}" class="report_total"
													 		id_staff = "{{$rowlist->s_id}}"
													 		time="{{$rowlist->pav_value}}"
														    staff="{{$rowlist->s_name}}"
														    page="{{$rowlist->p_number}}"
														    page_id="{{$rowlist->p_id}}"

															total2digitR="{{$sum_total_twodigit_r}}"
															total2digitS="{{$sum_total_twodigit_s}}"
															total3digitR="{{$sum_total_threedigit_r}}"
															total3digitS="{{$sum_total_threedigit_s}}"
															total2digitRright="{{$sum_price_right_twodigit_r}}"
															total2digitSright="{{$sum_price_right_twodigit_s}}"
															total3digitRright="{{$sum_price_right_threedigit_r}}"
															total3digitSright="{{$sum_price_right_threedigit_s}}">


												 @endif

									 @endforeach

								 </div>

							 </div>

							 <?php
							 $twodigit_charge = 0;
							 $threedigit_charge = 0;
							 $getStaffCharge = DB::table('tbl_staff')
									 ->select('s_id','s_two_digit_charge','s_three_digit_charge','s_two_digit_paid','s_three_digit_paid')
									 ->where('s_id',$var_staff)
									 ->first();
							// dd($getStaffCharge);

							if($var_type_lottery == 1){
								
								$win_two_digit = 70;
                                $win_three_digit = 600;

							}

								 // var_dump($getStaffCharge);
							 if(isset($getStaffCharge)){
								 $twodigit_charge = $getStaffCharge->s_two_digit_charge;
								 $threedigit_charge = $getStaffCharge->s_three_digit_charge;

								 $win_two_digit = $getStaffCharge->s_two_digit_paid;
                                 $win_three_digit = $getStaffCharge->s_three_digit_paid;

								 if($twodigit_charge == $threedigit_charge){
								 	$hidedata = 'hidetwothree_digit';
								 	$sumHide = '';
								 	$colspan = 1;
								 }else{
								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }
							 }

							 ?>
							 @if($var_sheet=='' && $var_page=='')
							 	@if(Session::get('roleLot') == 1 || Session::get('roleLot') == 2)
								 <div id="html-content-holder" class="widget-body paperStyle">
								 	@if(isset($rowlist))
									 <h1>{{trans('label.summary_result_per_day')}} 
									 	@if($var_type_lottery == 1)
									 		VN
									 	@elseif($var_type_lottery == 2)
									 		KH
									 	@else
									 		TH
									 	@endif
									 </h1><br>
									 <div class="col-md-3 staff_name">
										 {{trans('label.staff_name')}} :
										 <b>{{$rowlist->s_name}} </b>
									 </div>

									 <div class="col-md-2">
										 <b>Lottery</b>
									 </div>

									 <div class="col-md-2">
										 {{trans('label.date')}} :
										 <b><?php echo date("d-m-Y", strtotime($rowlist->p_date));?></b>
									 </div>
									 @endif
									 <br><br>
									 <table id="" class="table table-bordered summary_result_per_day" style="width:75%;">
										 <thead>
										 <tr>
										 	 <th></th>
											 <th></th>
											 <th></th>
											 <th colspan="{{$colspan}}">KHR</th>
											 <th>លេខត្រូវ </th>
											 <th>លេខត្រូវ </th>
											 <th colspan="{{$colspan}}">USD</th>
											 <th>លេខត្រូវ</th>
											 <th>លេខត្រូវ</th>
										 </tr>
										 <tr>
										 	 <th>{{trans('label.staff')}}</th>
											 <th>{{trans('label.time')}}</th>
											 <th>page</th>
											 <th class="{{$hidedata}}">2D</th>
											 <th class="{{$hidedata}}">3D</th>
											 <th class="{{$sumHide}}">លុយ</th>
											 <th>2D</th>
											 <th>3D</th>
											 <th class="{{$hidedata}}">2D</th>
											 <th class="{{$hidedata}}">3D</th>
											 <th class="{{$sumHide}}">លុយ</th>
											 <th>2D</th>
											 <th>3D</th>
										 </tr>
										 </thead>
										 <tbody class="display_total_result">
										 </tbody>
									 </table>
								</div>
								@endif

							 @endif
						 </div>




					 @endif



		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		// $(document).off('change', '#type_lottery').on('change', '#type_lottery', function(e){
			
			var id = $('#type_lottery').val();
			var staff_id = $("#staff").val();
			var sheet_id = $("#sheet").val();

			$("#staff").prop( "disabled", true );
			$("#sheet").prop( "disabled", true );
            if(id != ''){
            	$.ajax({
				   url: '{{URL::to('/')}}/reportphone/getstaffbylotterytype',
			       type: 'GET',
			       data: {type:id, staff_id:staff_id, sheet_id:sheet_id},
			       success: function(data) {
			           if(data.status=="success"){

			           		$("#staff").html(data.msg);
			           		$("#sheet").html(data.msg1);

			           		$("#staff").prop( "disabled", false );
			           		$("#sheet").prop( "disabled", false );

			      			console.log(data.msg);

			           }else{
			           }
			        }
			     });
            }else{

            	$("#staff").html('<option value="" selected="selected">រើសបុគ្គលិក</option>');
			    $("#sheet").html('<option value="" selected="selected">ជ្រើសរើសពេល</option>');
            	$("#staff").prop( "disabled", false );
			    $("#sheet").prop( "disabled", false );
            }
			

		// });


		$(document).off('submit', '#filter').on('submit', '#filter', function(e){
			if (validate_form_main('#filter') == 0) {
				return true;
			}
			return false;
		});

@if(isset($report))

		function commaSeparateNumber(val){
			while (/(\d+)(\d{3})/.test(val.toString())){
				val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
			}
			return val;
		}

		function addCommas(nStr)
		{
		    nStr += '';
		    x = nStr.split('.');
		    x1 = x[0];
		    x2 = x.length > 1 ? '.' + x[1] : '';
		    var rgx = /(\d+)(\d{3})/;
		    while (rgx.test(x1)) {
		        x1 = x1.replace(rgx, '$1' + ',' + '$2');
		    }
		    return x1 + x2;
		}



		$(document).ready(function() {
			var total2digitr = '';
			var total2digits = '';
			var total3digitr = '';
			var total3digits = '';
			var total2digitrright = '';
			var total2digitsright = '';
			var total3digitrright = '';
			var total3digitsright = '';
			var time = '';
			var staff_list = '';
			var page = '';
			var tr = '';
			var sum_total2digitrright = 0;
			var sum_total2digitsright = 0;
			var sum_total3digitrright = 0;
			var sum_total3digitsright = 0;

			var num_total2digitrright = 0;
			var num_total2digitsright = 0;
			var num_total3digitrright = 0;
			var num_total3digitsright = 0;


			var twodigit_charge = <?php echo $twodigit_charge;?>;
			var threedigit_charge = <?php echo $threedigit_charge;?>;

			var sum_total2digitr = 0;
			var sum_total2digits = 0;
			var sum_total3digitr = 0;
			var sum_total3digits = 0;

			var data_array = new Array();
			$(".report_total").each(function(index, element) {
				
				var data_list = {};

				var id_staff = $(this).parent().parent().find('.report_total_info').attr('id_staff');

				var time_value = $(this).parent().parent().find('.report_total_info').attr('time');
				var staff_value = $(this).parent().parent().find('.report_total_info').attr('staff');
				var page_value = $(this).parent().parent().find('.report_total_info').attr('page');

				var page_code = $(this).parent().parent().find('.report_total_info').attr('page_code');

				var page_id = $(this).attr('page_id');

				total2digitr = $(this).attr('total2digitr');
				total2digits = $(this).attr('total2digits');
				total3digitr = $(this).attr('total3digitr');
				total3digits = $(this).attr('total3digits');
				total2digitrright = $(this).attr('total2digitrright');
				total2digitsright = $(this).attr('total2digitsright');
				total3digitrright = $(this).attr('total3digitrright');
				total3digitsright = $(this).attr('total3digitsright');

				var item = new Array();
				item.push({'total2digitr':total2digitr,
					       'total2digits':total2digits,
					       'total3digitr':total3digitr,
					       'total3digits':total3digits,
					       'total2digitrright':total2digitrright,
					       'total2digitsright':total2digitsright,
					       'total3digitrright':total3digitrright,
					       'total3digitsright':total3digitsright,
					       'time_value':time_value,
					       'staff_value':staff_value,
					   	   'page_value':page_value,
					   	   'page_id':page_id});
				

				data_list['id']= id_staff;
				data_list['item']= item;

				data_array.push(data_list);
            });
            
            

			data_array = data_array.sort(function (a, b) {
			    return a.id.localeCompare( b.id );
			});
			console.log(data_array);

			// $.each( data_array, function( index, element ) {
			// 	total2digitr = element.item[0].total2digitr;
			// 	total2digits = element.item[0].total2digits;
			// 	total3digitr = element.item[0].total3digitr;
			// 	total3digits = element.item[0].total3digits;
			// 	total2digitrright = element.item[0].total2digitrright;
			// 	total2digitsright = element.item[0].total2digitsright;
			// 	total3digitrright = element.item[0].total3digitrright;
			// 	total3digitsright = element.item[0].total3digitsright;

			// 	time_value = element.item[0].time_value;
			// 	staff_value = element.item[0].staff_value;
			// 	page_value = element.item[0].page_value;
			// 	page_id = element.item[0].page_id;

			// 	if(total2digitr==0){
			// 		numtotal2digitr = 0;
			// 		total2digitr = '';
			// 	}else{
			// 		numtotal2digitr = total2digitr;
			// 		total2digitr = total2digitr;
			// 	}

			// 	if(total2digits==0){
			// 		numtotal2digits = 0;
			// 		total2digits = '';
			// 	}else{
			// 		numtotal2digits = total2digits;
			// 		total2digits = total2digits;
			// 	}

			// 	if(total3digitr==0){
			// 		numtotal3digitr = 0;
			// 		total3digitr = '';
			// 	}else{
			// 		numtotal3digitr = total3digitr;
			// 		total3digitr = total3digitr;
			// 	}

			// 	if(total3digits==0){
			// 		numtotal3digits = 0;
			// 		total3digits = '';
			// 	}else{
			// 		numtotal3digits = total3digits;
			// 		total3digits = total3digits;
			// 	}

			// 	if(total2digitrright==0){
			// 		num_total2digitrright = 0;
			// 		total2digitrright = '';
			// 	}else{
			// 		num_total2digitrright = total2digitrright;
			// 		total2digitrright = total2digitrright;

			// 	}
			// 	if(total2digitsright==0){
			// 		num_total2digitsright = 0;
			// 		total2digitsright = '';
			// 	}else{
			// 		num_total2digitsright = total2digitsright;
			// 		total2digitsright = total2digitsright;
			// 	}
			// 	if(total3digitrright==0){
			// 		num_total3digitrright = 0;
			// 		total3digitrright = '';
			// 	}else{
			// 		num_total3digitrright = total3digitrright;
			// 		total3digitrright = total3digitrright;
			// 	}
			// 	if(total3digitsright==0){
			// 		num_total3digitsright = 0;
			// 		total3digitsright = '';
			// 	}else{
			// 		num_total3digitsright = total3digitsright;
			// 		total3digitsright = total3digitsright;
			// 	}

			// 	sum_total2digitr = sum_total2digitr + parseInt(numtotal2digitr);
			// 	sum_total2digits = sum_total2digits + parseInt(numtotal2digits);
			// 	sum_total3digitr = sum_total3digitr + parseInt(numtotal3digitr);
			// 	sum_total3digits = sum_total3digits + parseInt(numtotal3digits);

			// 	sum_total2digitrright = sum_total2digitrright + parseInt(num_total2digitrright);
			// 	sum_total2digitsright = sum_total2digitsright + parseInt(num_total2digitsright);
			// 	sum_total3digitrright = sum_total3digitrright + parseInt(num_total3digitrright);
			// 	sum_total3digitsright = sum_total3digitsright + parseInt(num_total3digitsright);

			// 	// tr = '<tr><td id="td_time_'+index+'">'+time+'</td><td id="td_page_'+index+'">'+page+'</td><td>'+commaSeparateNumber(total2digitr)+'</td><td>'+commaSeparateNumber(total2digits)+'</td><td>'+commaSeparateNumber(total3digitr)+'</td><td>'+commaSeparateNumber(total3digits)+'</td><td class="result_right">'+commaSeparateNumber(total2digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total2digitsright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitsright)+'</td><tr>';

			// 	tr = '<tr><th id="td_staff_com_'+index+'">'+staff_value+'</th><th id="td_time_com_'+index+'">'+time_value+'</th><th id="td_page_com_'+index+'">'+page_value+'</th><td>'+total2digitr+'</td><td>'+total3digitr+'</td><td>'+total2digitrright+'</td><td>'+total3digitrright+'</td><td>'+total2digits+'</td><td>'+total3digits+'</td><td>'+total2digitsright+'</td><td>'+total3digitsright+'</td><tr>';

			// 	$('.display_total_result').append(tr);
			// });
			var total_page_sum = 0;
			var check_staff = '';
			var check_time = '';
			var row_data = '';
			var date_new = '';
			var type_lottery = '';
			var staff_new = '';
			var total_page = $(".report_total").length;
			$(".report_total").each(function(index, element) {
				total_page_sum = total_page_sum + 1;
				var id_staff = $(this).parent().parent().find('.report_total_info').attr('id_staff');

				var time_value = $(this).parent().parent().find('.report_total_info').attr('time');
				var staff_value = $(this).parent().parent().find('.report_total_info').attr('staff');
				var page_value = $(this).parent().parent().find('.report_total_info').attr('page');

				var page_code = $(this).parent().parent().find('.report_total_info').attr('page_code');

				var page_id = $(this).attr('page_id');
				
                total2digitr = $(this).attr('total2digitr');
                total2digitr = total2digitr;
				total2digits = $(this).attr('total2digits');
                total3digitr = $(this).attr('total3digitr');
                total3digitr = total3digitr;
				total3digits = $(this).attr('total3digits');
                total2digitrright = $(this).attr('total2digitrright');
                total2digitrright = total2digitrright;
				total2digitsright = $(this).attr('total2digitsright');
                total3digitrright = $(this).attr('total3digitrright');
                total3digitrright = total3digitrright;
				total3digitsright = $(this).attr('total3digitsright');
              
				if(total2digitr==0){
					numtotal2digitr = 0;
					total2digitr = '';
				}else{
					numtotal2digitr = total2digitr;
					total2digitr = total2digitr;
				}

				if(total2digits==0){
					numtotal2digits = 0;
					total2digits = '';
				}else{
					numtotal2digits = total2digits;
					total2digits = total2digits;
				}

				if(total3digitr==0){
					numtotal3digitr = 0;
					total3digitr = '';
				}else{
					numtotal3digitr = total3digitr;
					total3digitr = total3digitr;
				}

				var totalsum = parseInt(numtotal2digitr) + parseInt(numtotal3digitr);
				if(totalsum==0){
					totalsum = '';
				}

				if(total3digits==0){
					numtotal3digits = 0;
					total3digits = '';
				}else{
					numtotal3digits = total3digits;
					total3digits = total3digits;
				}

				if(total2digitrright==0){
					num_total2digitrright = 0;
					total2digitrright = '';
				}else{
					num_total2digitrright = total2digitrright;
					total2digitrright = total2digitrright;

				}
				if(total2digitsright==0){
					num_total2digitsright = 0;
					total2digitsright = '';
				}else{
					num_total2digitsright = total2digitsright;
					total2digitsright = total2digitsright;
				}
				if(total3digitrright==0){
					num_total3digitrright = 0;
					total3digitrright = '';
				}else{
					num_total3digitrright = total3digitrright;
					total3digitrright = total3digitrright;
				}
				if(total3digitsright==0){
					num_total3digitsright = 0;
					total3digitsright = '';
				}else{
					num_total3digitsright = total3digitsright;
					total3digitsright = total3digitsright;
				}

				var totalsum_dolla = parseFloat(numtotal2digits) + parseFloat(numtotal3digits);
				if(totalsum_dolla==0){
					totalsum_dolla = '';
				}

				sum_total2digitr = sum_total2digitr + parseInt(numtotal2digitr);
				sum_total2digits = sum_total2digits + parseFloat(numtotal2digits);
				sum_total3digitr = sum_total3digitr + parseInt(numtotal3digitr);
				sum_total3digits = sum_total3digits + parseFloat(numtotal3digits);

				sum_total2digitrright = sum_total2digitrright + parseInt(num_total2digitrright);
				sum_total2digitsright = sum_total2digitsright + parseFloat(num_total2digitsright);
				sum_total3digitrright = sum_total3digitrright + parseInt(num_total3digitrright);
				sum_total3digitsright = sum_total3digitsright + parseFloat(num_total3digitsright);

				// tr = '<tr><td id="td_time_'+index+'">'+time+'</td><td id="td_page_'+index+'">'+page+'</td><td>'+commaSeparateNumber(total2digitr)+'</td><td>'+commaSeparateNumber(total2digits)+'</td><td>'+commaSeparateNumber(total3digitr)+'</td><td>'+commaSeparateNumber(total3digits)+'</td><td class="result_right">'+commaSeparateNumber(total2digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total2digitsright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitrright)+'</td><td class="result_right">'+commaSeparateNumber(total3digitsright)+'</td><tr>';
					
				@if($var_dateStart!='' &&  $var_staff!='' && $var_page=='' && $var_sheet=='' && $staff_boss==0)
                    var date_note = '<?php echo date("d-m-Y", strtotime($rowlist->p_date));?>';
                    var date_db = '<?php echo date("Y-m-d", strtotime($rowlist->p_date));?>';
					var type_lottery = '<?php echo $var_type_lottery;?>';
					// console.log(time_value);
					// console.log(page_value);
					// return false;
					// console.log(total_page_sum);

					if(time_value == 'ថ្ងៃ'){
						var new_time = '24';
					}else if(time_value == 'ល្ងាច'){
						var new_time = '5';
					}else if(time_value == 'យប់'){
						var new_time = '6';
					}else if(time_value == 'ព្រឹក'){
						var new_time = '23';
					}else{
						var new_time = '5';
					}

					row_data = row_data + date_db+','+id_staff+","+new_time+","+page_id+","+total2digitr+","+total3digitr+","+total2digitrright+","+total3digitrright+","+total2digits+","+total3digits+","+total2digitsright+","+total3digitsright+","+total_page_sum+";";

					date_new = date_db;
					type_lottery = type_lottery;
                    staff_new = staff_value;
                    staff_id = '{{$var_staff}}';

					if(total_page_sum == total_page){
                        console.log(row_data);
                        // return false;
						$.ajax({
							url: '/reportphone/summary_lottery_pagePhone',
							type: 'GET',
							data: {row_data:row_data,date_new:date_new,type_lottery:type_lottery,staff_new:staff_new,staff_id:staff_id},
							success: function(data) {
								if(data.status=="success"){
									// console.log(data.msg);
								}else{
		//							console.log(data.msg);
								}
							}
						});
					}
					

				@endif


				if(check_staff != staff_value){
					check_staff = staff_value;
					var new_staff = staff_value;
					check_time = '';
				}else{
					var new_staff = '';
					
				}

				if(check_time != time_value){
					check_time = time_value;
					var new_time = time_value;
				}else{
					
						var new_time = '';
					
				}

				var staff_id_first = "<?php echo $staff_filter;?>";

				
				
				// tr = '<tr><th id="td_staff_'+index+'">'+staff_value+'</th><th id="td_time_'+index+'">'+time_value+'</th><th id="td_page_'+index+'">'+page_value+'</th><td​​ class="{{$hidedata}}">'+total2digitr+'</td><td class="{{$hidedata}}">'+total3digitr+'</td><td class="{{$sumHide}}">'+totalsum+'</td><td>'+total2digitrright+'</td><td>'+total3digitrright+'</td><td class="{{$hidedata}}">'+total2digits+'</td><td class="{{$hidedata}}">'+total3digits+'</td><td class="{{$sumHide}}">'+totalsum_dolla+'</td><td>'+total2digitsright+'</td><td>'+total3digitsright+'</td><tr>';
				tr = '<tr><th id="td_staff_'+index+'">'+new_staff+'</th><th id="td_time_'+index+'">'+new_time+'</th><th id="td_page_'+index+'">'+page_value+'</th><td class="{{$hidedata}}">'+total2digitr+'</td><td class="{{$hidedata}}">'+total3digitr+'</td><td class="{{$sumHide}}">'+totalsum+'</td><td>'+total2digitrright+'</td><td>'+total3digitrright+'</td><td class="{{$hidedata}}">'+total2digits+'</td><td class="{{$hidedata}}">'+total3digits+'</td><td class="{{$sumHide}}">'+totalsum_dolla+'</td><td>'+total2digitsright+'</td><td>'+total3digitsright+'</td></tr>';
				
				$('.display_total_result').append(tr);

				


			});

			var total_income_expense_riel = 0;
			var total_income_expense_dollar = 0;
			<?php 




			if($var_type_lottery == 1){
			?>
				var income_riel = parseInt(Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
			<?php }else{?>
				var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
			<?php }?>
			var total_income_expense_riel = income_riel - expense_riel;
			var check_total = total_income_expense_riel.toString().substr(-2);
            // console.log(total_income_expense_riel);
            // console.log(check_total);
            var staff_id = "<?php echo $staff_filter;?>";

        
			// if(check_total >= 50){
   //              // console.log('1');
   //              if(total_income_expense_riel > 0 ) {

   //                  total_income_expense_riel = total_income_expense_riel + ( 100 - parseInt(check_total));
   //              }else{
   //                  total_income_expense_riel = total_income_expense_riel - ( 100 - parseInt(check_total));
   //              }
			// }else{
   //              // console.log('2');
   //              if(total_income_expense_riel > 0 ){
   //                  total_income_expense_riel = parseInt(total_income_expense_riel) - parseInt(check_total);
   //              }else{
   //                  total_income_expense_riel = parseInt(total_income_expense_riel) + parseInt(check_total);
   //              }

			// }

			
			if(total_income_expense_riel>0){
				total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = total_income_expense_riel;
                
            }else{
            	total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = '('+total_income_expense_riel+')';
                
            }



			var total_income_expense_dollar = income_dollar - expense_dollar;
			var total_income_expense_dollar_noStr = income_dollar - expense_dollar;
            if(total_income_expense_dollar>0){
                total_income_expense_dollar = total_income_expense_dollar.toFixed(2);
            }else{
                total_income_expense_dollar = '('+total_income_expense_dollar.toFixed(2)+')';
            }

            var sum_totalall = sum_total2digitr + sum_total3digitr;
            var sum_totalall_dolla = sum_total2digits + sum_total3digits;
	
			tr = '<tr class="bee_highlight"><th>{{trans('label.total')}}</th><th></th><th >'+total_page_sum+'</th><th class="{{$hidedata}}">'+sum_total2digitr+'</th><th class="{{$hidedata}}">'+sum_total3digitr+'</th><th class="{{$sumHide}}">'+sum_totalall+'</th><th>'+sum_total2digitrright+'</th><th>'+sum_total3digitrright+'</th><th class="{{$hidedata}}">$ '+sum_total2digits+'</th><th class="{{$hidedata}}">$ '+sum_total3digits+'</th><th class="{{$sumHide}}">$ '+sum_totalall_dolla+'</th><th>$ '+sum_total2digitsright+'</th><th>$ '+sum_total3digitsright+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class="bee_highlight"><td></td><td>{{trans('label.total_charge')}}</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)))+'</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitrright * 70)+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitrright * 600)+'</td><td>'+commaSeparateNumber(((parseInt(sum_total2digits) * parseInt(twodigit_charge)) / 100).toFixed(2))+'</td><td>'+commaSeparateNumber(((parseInt(sum_total3digits) * parseInt(threedigit_charge)) / 100).toFixed(2))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitsright * 70 )+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitsright * 600)+'</td><tr>';
			// $('.display_total_result').append(tr);
            var total_rate_thesame = parseInt(sum_total2digitr) + parseInt(sum_total3digitr);
            var total_rate_thesame_dolla = parseFloat(sum_total2digits) + parseFloat(sum_total3digits); 
			tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result').append(tr);
			// 2D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th></th><th></th><th>2D</th><th>'+sum_total2digitr+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100))+'</th><th></th><th>$ '+sum_total2digits+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>$ '+((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100).toFixed(2)+'</th><th></th></tr>';
			$('.display_total_result').append(tr);
			// 3D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th></th><th></th><th>3D</th><th>'+sum_total3digitr+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100))+'</th><th></th><th>$ '+sum_total3digits+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>$ '+((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100).toFixed(2)+'</th><th></th></tr>';
			$('.display_total_result').append(tr);

			tr = '<tr class="bee_highlight {{$sumHide}}"><th></th><th>Total</th><th>'+total_rate_thesame+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+Math.round(((parseFloat(total_rate_thesame) * parseFloat(threedigit_charge)) / 100))+'</th><th></th><th>$ '+total_rate_thesame_dolla+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>$ '+((parseFloat(total_rate_thesame_dolla) * parseFloat(threedigit_charge)) / 100).toFixed(2)+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);




			<?php 
			if($var_type_lottery == 1){
			?>
				// 2D(T)
				tr = '<tr class="bee_highlight"><th></th><th class="{{$hidedata}}"></th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th></th><th>'+sum_total2digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th></th><th>'+sum_total3digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
			<?php }else if($var_type_lottery == 2 || $var_type_lottery == 3 || $var_type_lottery == 4){?>
				// 2D(T)
				tr = '<tr class="bee_highlight"><th></th><th class="{{$hidedata}}"></th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th></th><th>'+sum_total2digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th></th><th>'+sum_total3digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
			<?php }?>




			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);


			// tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th></th><th></th><th class="result_right_total">'+total_income_expense_riel+' </th><th></th><th></th><th></th><th class="result_right_total">$ '+total_income_expense_dollar+'</th><th class="{{$hidedata}}"></th></tr>';
			// $('.display_total_result').append(tr);

			tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result').append(tr);



			// paid or borrow
			var totalRealBefore = total_income_expense_riel_noStr;
			var totalDollaBefore = total_income_expense_dollar_noStr;

			if(totalRealBefore < 0){
				var labelBefore_r = 'មេខាត';
				var colorReal = 'lose';
			}else{
				var labelBefore_r = 'មេសុី';
				var colorReal = 'blue';
			}

			if(totalDollaBefore < 0){
				var labelBefore_d = 'មេខាត';
				var colorDolor = 'lose';
			}else{
				var labelBefore_d = 'មេសុី';
				var colorDolor = 'blue';
			}	

			// price before percent
			tr = '<tr class="bee_highlight vannarithREAL"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">'+labelBefore_r+'</th><th class="result_right_total '+colorReal+'" style="text-align:left !important;">'+addCommas(totalRealBefore)+' </th><th></th><th></th><th>'+labelBefore_d+'</th><th class="result_right_total '+colorDolor+'" style="text-align:left !important;">$ '+totalDollaBefore.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
			$('.display_total_result').append(tr);

			@if($percentStaff > 0)

				var percent = 100 - parseFloat('{{$percentStaff}}');

				var totalReal = parseFloat(totalRealBefore * percent)/100;
				var totalDolla = parseFloat(totalDollaBefore * percent)/100;

				// price after percent
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">%</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(totalReal)+' </th><th></th><th></th><th>%</th><th class="result_right_total" style="text-align:left !important;">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);

			@else

				var totalReal = totalRealBefore;
				var totalDolla = totalDollaBefore;

			@endif

@if($var_type_lottery == 1 || $var_type_lottery == 2 )
			
			
			// end paid or borrow
			

			

			// $(".report_total_info").each(function(index, element) {

			// 	staff_list = $(this).attr('staff');
			// 	id_staff = $(this).attr('id_staff');

			// 	time = $(this).attr('time');
			// 	page = $(this).attr('page');
			// 	$('.display_total_result').find('#td_staff_com_'+index).text(staff_list);
			// 	$('.display_total_result').find('#td_time_com_'+index).text(time);
			// 	$('.display_total_result').find('#td_page_com_'+index).text(page);

			// });

			@if($var_dateStart!='' &&  $var_staff!='' && $var_page=='' && $var_sheet=='')
				var date = '{{$var_dateStart}}';
				var staff = '{{$var_staff}}';
				var var_type_lottery = '{{$var_type_lottery}}';
				var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				<?php 
				if($var_type_lottery == 1){
				?>
					var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
					var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				<?php }else if($var_type_lottery == 2){?>
					var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
					var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				<?php }?>
				console.log(totalReal);
				console.log(totalDolla);
//				console.log(expense_riel);
//				console.log(expense_dollar);
				<?php 
				// if($checkinView == 1 && $var_type_lottery == 1){
				?>
				$.ajax({
					url: '/reportphone/addmoneyphone',
					type: 'GET',
					data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla},
					success: function(data) {
						if(data.status=="success"){
//							console.log(data.msg);
						}else{
//							console.log(data.msg);
						}
					}
				});
				<?php 
				// }
				?>

// 				$.ajax({
// 					url: 'summary_lottery',
// 					type: 'GET',
// 					data: {date:date,staff:staff,income_riel:income_riel,income_dollar:income_dollar,expense_riel:expense_riel,expense_dollar:expense_dollar,var_type_lottery:var_type_lottery},
// 					success: function(data) {
// 						if(data.status=="success"){
// //							console.log(data.msg);
// 						}else{
// //							console.log(data.msg);
// 						}
// 					}
// 				});
				
// 				$.ajax({
// 					url: 'summary_lottery_total',
// 					type: 'GET',
// 					data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:var_type_lottery},
// 					success: function(data) {
// 						if(data.status=="success"){
// //							console.log(data.msg);
// 						}else{
// //							console.log(data.msg);
// 						}
// 					}
// 				});
		    @endif



@endif


		});

@endif
	$(document).ready(function() {
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
		});
	});
	$(document).ready(function(){

		$('.btn_print').click(function(){
			var attr_id = $(this).attr('attr_id');
			var attr_date = $(this).attr('attr_date');
			var attr_staff = $(this).attr('attr_staff');
			var attr_sheet = $(this).attr('attr_sheet');
			if(attr_sheet==''){
				attr_sheet = 0;
			}
			var attr_page = $(this).attr('attr_page');
			if(attr_page==''){
				attr_page = 0;
			}
			var form_upload = $('#frmupload');
			var fram_upload = $('<iframe style="border:0px; width:0px; height:0px; opacity: 0px;" src="http://188.166.234.165/lottery/screenshot/download.php?attr_id='+attr_id+'&attr_date='+attr_date+'&attr_staff='+attr_staff+'&attr_sheet='+attr_sheet+'&attr_page='+attr_page+'" id="framupload" name="framupload"></iframe>');

			$('body').append(fram_upload);

		});
	});
	</script>
@stop