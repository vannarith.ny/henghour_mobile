@extends('mobile.master')

@section('mobile_title')
    <title>ស៊ីសង</title>
@stop

@section('mobile_cssStyle')
    <style type="text/css">
    	*:focus {
		    outline: none;
		}

        * { 
                -webkit-touch-callout: none; 
                -webkit-user-select: none; 
                -khtml-user-select: none; 
                -moz-user-select: none; 
                -ms-user-select: none; 
                user-select: none; 
                touch-action: manipulation;
            } 

    	input.inputbt {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 24px;
			    padding: 15px 20px;
			    border: 2px #ccc solid;
			    line-height: 24px;
			    border-radius:10px;
    	}

    	input.inputbt.active{
    		background-color: #ccc;

    	}
    	#main_table{
    		padding-top: 20px;
      }
      
        .formFiltter{
            display:inline-block;
        }
        .formFiltter label{
            font-family:"Times New Roman", Times, serif;
            font-weight:bold;
            padding-top:10px;
            padding-right:10px;
            padding-left:20px;
        }
        .formdate{
            width: 150px;
        }
        #clientId{
            border: 1px #ccc solid;
            min-width: 100px;
        }
        .container{
            text-align: center;
        }

    </style>

@stop


@section('mobile_content')
<div class="container hideall" style="overflow:scroll;" id="list_menu" >
		
    <div class="row formFiltter">
      <label>ថ្ងៃខែចាម់ផ្តើម</label>
      <input type="datetime" id="fromDate" class="formdate"/>
      <label>បញ្ចប់</label>
      <input type="datetime" id="toDate" class="formdate"/>
      <label>កូន</label>
      <select id="clientId">
        <option value="{{Session::get('iduserlotMobileSec')}}">{{Session::get('usernameLotMobileSec')}}</option>
        @foreach($childs as $child)
            <option value="{{$child->s_id}}">{{$child->s_name}}</option>
        @endforeach
        
      </select>
    </div>
</div>
<div class="container-fluid" style="padding-top:20px;">
        <div id="list_user" class="hideall">
          <table width="100%" class="table-row" align="center" cellspacing="0" cellpadding="0">
            <thead>
              <tr>
                <th class="ctd" >ថ្ងៃខែឆ្នាំ</th>
                <th class="classtd1" >ឈ្មោះ</th>
                <th class="classtd1" >ប្រភេទ</th>
                <th class="classtd1">(៛)</th>
                <th class="classtd1">($)</th>
              </tr>
            </thead>
            <tbody id="dataList">
              
              
            </tbody>
          </table>
          @if(count($childs) > 0)
          <div class="showbtnAddClient">
              <input type="button" value="បញ្ចូលបញ្ជី" onclick="AddClient()" class="inputbt">
          </div>
          @endif
        </div>
        

        <div id="update_info_user" class="hideall">
          <form name="updateUserInfo" id="waitingShowInfo">
                
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">ឈ្មោះ</span>
                  </div>
                  <input type="hidden" id="idhide" name="idhide">
                  <select id="child_id" name="child_id" class="form-control required">
                    @foreach($childs as $child)
                        <option value="{{$child->s_id}}">{{$child->s_name}}</option>
                    @endforeach
                    
                  </select>
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">ចំនួនទឹកប្រាក់ ៛</span>
                  </div>
                  <input type="text" name="moneyR" id="moneyR" onKeypress="KeyValidate(event)" maxlength="12" pattern="[0-9]*" class="form-control required" placeholder="ទឹកប្រាក់ ៛" aria-label="ទឹកប្រាក់ ៛" aria-describedby="basic-addon1" >
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">ចំនួនទឹកប្រាក់ $</span>
                  </div>
                  <input type="text" name="moneyD" id="moneyD" onKeypress="KeyValidate(event)" maxlength="12" pattern="[0-9]*" class="form-control required" placeholder="ទឹកប្រាក់ $" aria-label="ទឹកប្រាក់ $" aria-describedby="basic-addon1" >
                </div>
                
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">ប្រភេទ</span>
                  </div>
                  <select id="s_type" name="s_type" class="form-control required">
                    @foreach($pharams as $pharam)
                        <option value="{{$pharam->pav_id}}">{{$pharam->pav_value}}</option>
                    @endforeach
                    
                  </select>
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">កាលបរិច្ឆេទ(ដកឬដាក់)</span>
                  </div>
                  <input type="datetime" id="date_in" name="date_in" class="form-control formdate required"/>
                </div>


                <table cellspacing="0" cellpadding="5" border="0" align="left" class="tableList">
                       
                        <tr>
                            <td colspan="2" align="center">
                                <input type="button" value="ថយក្រោយ" onclick="ShowClient()" class="inputbt marginright">
                                <input type="button" value="រក្សាទុក" onclick="SaveDataClient()" class="inputbt btncheck update">
                                <input type="button" value="រក្សាទុក" onclick="CreateDataClient()" class="inputbt btncheck create">
                                &nbsp; &nbsp; &nbsp;
                                <input type="button" value="លប់" onclick="DeleteUser()" class="btn-danger inputbt btncheck marginright update">
                            </td>
                        </tr>


                </table>
          </form>
        </div>
      </div>
  </div>

@endsection


@section('mobile_javascript')
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/transctionstaff.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop