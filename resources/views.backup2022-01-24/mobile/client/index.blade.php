@extends('mobile.master')

@section('mobile_title')
    <title>ភ្នាក់ងារ</title>
@stop

@section('mobile_cssStyle')
    <style type="text/css">
    	*:focus {
		    outline: none;
		}

        * { 
                -webkit-touch-callout: none; 
                -webkit-user-select: none; 
                -khtml-user-select: none; 
                -moz-user-select: none; 
                -ms-user-select: none; 
                user-select: none; 
                touch-action: manipulation;
            } 

    	input.inputbt {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 24px;
			    padding: 15px 20px;
			    border: 2px #ccc solid;
			    line-height: 24px;
			    border-radius:10px;
    	}

    	input.inputbt.active{
    		background-color: #ccc;

    	}
    	#main_table{
    		padding-top: 20px;
    	}

    </style>

@stop


@section('mobile_content')
    	<table  cellspacing="0" id="main_table" width="100%">
    		<tbody>
    			
    			<tr valign="top">
    				<td style="padding:10px; text-align:center">
    					<form name="frm_top" onsubmit="return false;" id="frm_btn">  
    						<input class="inputbt" id="ShowClientList" type="button" value="កូនក្រដាស់" onclick="ShowClient()">  
    						<input class="inputbt active" id="voteWater" type="button" value="កាត់ទឹក" onclick="ShowVoteWater()">
    					</form>
    					<br>
    					<table align="center" width="100%">
    						<tbody>
    							<tr valign="top">
    								<td align="left" id="td_users" class="hideall" style="display:none;">
    									<div id="list_user">
    										<table border="0" cellpadding="1" cellspacing="0" width="100%">
    											<thead align="center">
    												<tr align="center">
    													<th class="ctd">ល.រ</th>
    													<th class="ctd">ID</th>
    													<th class="classtd1">ឈ្មោះ</th>
    													<th class="classtd1">លេខទូរស័ព្ទ</th>  
    													<th class="classtd1">គុណទឹក</th>  
    													<th class="classtd1">ផ្អាក</th>
    													<th class="classtd1">SC</th>
    													<th class="classtd1">នៅលក់</th>
    												</tr>
    											</thead>
    											<tbody>
    												
    											</tbody>
    										</table>
    									</div>
                                        @if(Session::get('seccCanCreateChild') == 1)
                                        <div class="showbtnAddClient">
                                            <input type="button" value="បង្កេីតកូន" onclick="AddClient()" class="inputbt">
                                        </div>
                                        @endif
    								</td>
    								<td id="sell_percent" class="hideall">
    									<form name="sell" id="conditions-form">
    										<table cellspacing="0" cellpadding="5" border="1" align="center">
    											<tbody>
    												<tr id="tr-water">
    													<th>ពេល</th>
    													<th>គុណទឹក2លេខ</th>
    													<th>គុណទឹក3លេខ</th>
    												</tr>  
    												<tr>
    													<td colspan="3" align="left">
    														<label>
    															<input name="isUseDollar" type="checkbox" onclick=""> មានចាក់ដុល្លា($)
    														</label>
    													</td>
    												</tr>
    												<tr>
    													<td colspan="3" align="left">
    														<label>
    															<input type="checkbox" name="isPrintKatTek" >
    															<span onclick="">ព្រីនកន្ទុយមានកាត់ទឹក</span>
    														</label>
    														<input type="button" value="រក្សាទុក" onclick="SaveSellPercent()" class="inputbt">
    													</td>
    												</tr>
    											</tbody>
    										</table>
    									</form>
    								</td>

                                    
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    		</tbody>
    	</table>  

        <div id="update_info_user" class="hideall">
            <form name="updateUserInfo" id="waitingShowInfo">
                <input type="hidden" name="s_id" id="s_id" value="">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">ID</span>
                  </div>
                  <span id="idUser" class="pt-2 pl-2"></span>
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">ឈ្មោះ</span>
                  </div>
                  <input type="text" class="form-control required" placeholder="ឈ្មោះ" aria-label="ឈ្មោះ" aria-describedby="basic-addon1" name="s_name" id="s_name">
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">លេខទូរស័ព្ទ</span>
                  </div>
                  <input type="text" name="s_phone" id="s_phone" onKeypress="KeyValidate(event)" maxlength="12" pattern="[0-9]*" class="form-control required" placeholder="លេខទូរស័ព្ទ" aria-label="លេខទូរស័ព្ទ" aria-describedby="basic-addon1" >
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">កំហិតលុយ2លេខ ៛</span>
                  </div>
                  <input type="text" name="LimitMoneyAmount2R" id="LimitMoneyAmount2R" onKeypress="KeyValidate(event)" maxlength="12" pattern="[0-9]*" class="form-control " placeholder="កំហិតលុយ" aria-label="កំហិតលុយ" aria-describedby="basic-addon1" >
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">កំហិតលុយ3លេខ ៛</span>
                  </div>
                  <input type="text" name="LimitMoneyAmount3R" id="LimitMoneyAmount3R" onKeypress="KeyValidate(event)" maxlength="12" pattern="[0-9]*" class="form-control " placeholder="កំហិតលុយ" aria-label="កំហិតលុយ" aria-describedby="basic-addon1" >
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">កំហិតលុយ2លេខ $</span>
                  </div>
                  <input type="text" name="LimitMoneyAmount2D" id="LimitMoneyAmount2D" onKeypress="KeyValidate(event)" maxlength="12" pattern="[0-9]*" class="form-control " placeholder="កំហិតលុយ" aria-label="កំហិតលុយ" aria-describedby="basic-addon1" >
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">កំហិតលុយ3លេខ $</span>
                  </div>
                  <input type="text" name="LimitMoneyAmount3D" id="LimitMoneyAmount3D" onKeypress="KeyValidate(event)" maxlength="12" pattern="[0-9]*" class="form-control " placeholder="កំហិតលុយ" aria-label="កំហិតលុយ" aria-describedby="basic-addon1" >
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">SC</span>
                  </div>
                  <input type="text" name="SCode" id="SCode" class="form-control " placeholder="SC" aria-label="SC" aria-describedby="basic-addon1" >
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">គុណទឹក</span>
                    <span class="input-group-text">2លេខ x</span>
                  </div>
                  <input type="text" name="s_two_digit_charge" id="s_two_digit_charge" onKeypress="KeyValidate(event)" maxlength="5" pattern="[0-9]*" size="4"  class="form-control required" aria-label="ចំនួនទឹកគិតជាភាគរយ">
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">គុណទឹក</span>
                    <span class="input-group-text">3លេខ x</span>
                  </div>
                  <input type="text" name="s_three_digit_charge" id="s_three_digit_charge" onKeypress="KeyValidate(event)" maxlength="5" pattern="[0-9]*" size="4"  class="form-control required" aria-label="ចំនួនទឹកគិតជាភាគរយ">
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">សង</span>
                    <span class="input-group-text">2លេខ x</span>
                  </div>
                  <input type="text" name="s_two_digit_paid" id="s_two_digit_paid" onKeypress="KeyValidate(event)" maxlength="5" pattern="[0-9]*" size="4"  class="form-control required" aria-label="ចំនួនសង">
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">សង</span>
                    <span class="input-group-text">3លេខ x</span>
                  </div>
                  <input type="text" name="s_three_digit_paid" id="s_three_digit_paid" onKeypress="KeyValidate(event)" maxlength="5" pattern="[0-9]*" size="4"  class="form-control required" aria-label="ចំនួនសង">
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">លេខទូរស័ព្ទគណនី</span>
                  </div>
                  <input type="text" name="s_phone_login" id="s_phone_login" class="form-control required" placeholder="លេខទូរស័ព្ទគណនី" aria-label="លេខទូរស័ព្ទគណនី" aria-describedby="basic-addon1" onKeypress="KeyValidate(event)" maxlength="12">
                </div>


                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">លេខសំងាត់</span>
                  </div>
                  <input type="text" name="s_password" id="s_password" onKeypress="KeyValidate(event)" maxlength="6" minlength="6" pattern="[0-9]*" size="6" class="form-control required" placeholder="លេខសំងាត់" aria-label="លេខសំងាត់" aria-describedby="basic-addon1" >
                </div>

                <table cellspacing="0" cellpadding="5" border="0" align="left" class="tableList">
                        <tr>
                            <td align="left" >កូនក្រដាស់ : </td>
                            <td align="left" >
                                អាចបង្កេីតកូន
                                <input type="checkbox" name="CanCreateChild" id="CanCreateChild" value="">

                                &nbsp; &nbsp; &nbsp;
                                មាន $
                                <input type="checkbox" name="s_dollar" id="s_dollar" value="">

                                &nbsp; &nbsp; &nbsp;
                                ផ្អាក
                                <input type="checkbox" name="Active" id="Active" value="">
                            </td>
                        </tr> 
                        <tr>
                            <td colspan="2" align="center">
                                <input type="button" value="ថយក្រោយ" onclick="ShowClient()" class="inputbt marginright">
                                <input type="button" value="រក្សាទុក" onclick="SaveDataClient()" class="inputbt btncheck update">
                                <input type="button" value="រក្សាទុក" onclick="CreateDataClient()" class="inputbt btncheck create">
                                &nbsp; &nbsp; &nbsp;
                                <input type="button" value="លប់" onclick="DeleteUser()" class="btn-danger inputbt btncheck marginright update">
                            </td>
                        </tr>


                </table>
            </form>
        </div>
      




@endsection


@section('mobile_javascript')
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/client.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop