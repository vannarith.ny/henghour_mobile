	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="{{ asset('/') }}/mobile_css/jquery-ui.min.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/khmer-font.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/font-awesome.min.css" rel="stylesheet" />
	<link href="{{ asset('/') }}/mobile_css/sary.css" rel="stylesheet" />
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/post.js?v=2"></script>

	
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/sary.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>

    <style type="text/css">
    	*:focus {
		    outline: none;
		}
    	input#resultDate {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 20px;
			    border: 1px #ccc solid;
			    border-radius:10px;
			    margin-left: 20px;
			    margin-right: 20px;
    	}
		
    	#resultDate{
    		text-align: center;
    	}
    	.table_sary_afternoon td{
    		padding-left: 5px;
    		padding-top: 5px;
    	}
    	.dateTitlesary{
    		padding-left: 10px;
    	}
    	
    	.tableSrey{
    		background-color: #FFF;
    		text-align: center;
    		width: 180px;
    	}
    	.tableSrey td {
		    border: 1px #ccc solid;
		    padding: 2px 10px 2px 2px;
		    font-size: 14px;
		    
		}
		.width200{
			width: 180px;
		}


    </style>

	<div class="container-fluid">
		
		

		<div class="row pt-2 justify-content-md-center" id="saryContent">
				<div class="col-md-12 ">
					<div class="row pt-4 justify-content-md-center">
						@if(isset($resultsAfternoon) and count($resultsAfternoon) > 0 )
							@for ($i = 0; $i < 5; $i++)
		 					<article class="" >
	 						
                                <table cellpadding="0" cellspacing="0" class="tableSrey">
                                    <tr>
                                        <td colspan="2">
                                            <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនថ្ងៃ</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <b>Date : {{$date}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <b style="color: red; font-size: 14px;">4:30 PM</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>A</b>
                                        </td>
                                        <td style="text-align: right; font-size: 20px !important;">
                                             <b>{{$resultsAfternoon[0]->re_num_result}}</b><br>
                                             <b>{{$resultsAfternoon[1]->re_num_result}}</b>
                                        </td>
                                    </tr>
                    			@if(isset($sary[0]))
		                        @php 
		                            $data = $sary[0]->info;
		                            $resultLot = explode(",", $data);
		                        @endphp
                                    <tr>
                                        <td>
                                             <b>200N</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[0]}}</b><br>
                                             <b>{{$resultLot[1]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>500N</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[2]}}</b><br>
                                             <b>{{$resultLot[3]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>1,5Tr</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[4]}}</b><br>
                                             <b>{{$resultLot[5]}}</b><br>
                                             <b>{{$resultLot[6]}}</b><br>
                                             <b>{{$resultLot[7]}}</b><br>
                                             <b>{{$resultLot[8]}}</b><br>
                                             <b>{{$resultLot[9]}}</b><br>
                                             <b>{{$resultLot[10]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>5Tr</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[11]}}</b><br>
                                             <b>{{$resultLot[12]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>8Tr</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[13]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <b>12Tr</b>
                                        </td>
                                        <td style="text-align: right;">
                                             <b>{{$resultLot[14]}}</b>
                                        </td>
                                    </tr>
                    			@endif
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>B</b>
                                        </td>
                                        <td style="text-align: right; font-size: 20px !important;">
                                             <b>{{$resultsAfternoon[2]->re_num_result}}</b> - <b>{{$resultsAfternoon[3]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>C</b>
                                        </td>
                                        <td style="text-align: right; font-size: 20px !important;">
                                             <b>{{$resultsAfternoon[4]->re_num_result}}</b> - <b>{{$resultsAfternoon[5]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>D</b>
                                        </td>
                                        <td style="text-align: right; font-size: 20px !important;">
                                             <b>{{$resultsAfternoon[6]->re_num_result}}</b> - <b>{{$resultsAfternoon[7]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>F</b>
                                        </td>
                                        <td style="text-align: right; font-size: 20px !important;">
                                             <b>{{$resultsAfternoon[8]->re_num_result}}</b> - <b>{{$resultsAfternoon[9]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>I</b>
                                        </td>
                                        <td style="text-align: right; font-size: 20px !important;">
                                             <b>{{$resultsAfternoon[10]->re_num_result}}</b> - <b>{{$resultsAfternoon[11]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>N</b>
                                        </td>
                                        <td style="text-align: right; font-size: 20px !important;">
                                             <b>{{$resultsAfternoon[12]->re_num_result}}</b> - <b>{{$resultsAfternoon[13]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                </table>
                            </article>
	 						@endfor
						@endif

						@if(isset($resultsEvening) and count($resultsEvening) > 0 )
							@for ($i = 0; $i < 5; $i++)
							<article class="" >
								<table cellpadding="0" cellspacing="0" class="tableSrey">
                                    <tr>
                                        <td colspan="3">
                                            <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនយប់</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b>Date : {{$date}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                             <b style="color: red; font-size: 14px;">6:30 PM</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>A</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[0]->re_num_result}}</b><br>
                                             <b>{{$resultsEvening[1]->re_num_result}}</b><br>
                                             <b>{{$resultsEvening[2]->re_num_result}}</b><br>
                                             <b>{{$resultsEvening[3]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[4]->re_num_result}}</b><br>
                                             <b>{{$resultsEvening[5]->re_num_result}}</b><br>
                                             <b>{{$resultsEvening[6]->re_num_result}}</b>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>B</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[7]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[8]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>C</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[9]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[10]->re_num_result}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 20px !important;">
                                             <b>D</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[11]->re_num_result}}</b>
                                        </td>
                                        <td style="text-align: center; font-size: 20px !important;">
                                             <b>{{$resultsEvening[12]->re_num_result}}</b>
                                        </td>
                                    </tr>
            					@if(isset($sary[1]))
		                        	@php 
		                            	$data = $sary[1]->info;
		                            	$resultLotEve = explode(",", $data);
		                        	@endphp
                                    <tr>
                                        <td></td>
                                        <td>
                                             19
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[0]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             18
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[1]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             17
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[2]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             16
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[3]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             15
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[4]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             14
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[5]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             13
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[6]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             12
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[7]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             11
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[8]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             10
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[9]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             9
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[10]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             8
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[11]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             7
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[12]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             6
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[13]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             5
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[14]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             4
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[15]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             3
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[16]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             2
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[17]}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                             1
                                        </td>
                                        <td style="text-align: center;">
                                             <b>{{$resultLotEve[18]}}</b>
                                        </td>
                                    </tr>
            					@endif    
                                </table>
							</article>
							@endfor
						@endif
	 					
					</div>
				</div>
				
				
				
			</div>
		</div>

		
				
	</div>


    	

       
      


	