@extends('mobile.master')

@section('mobile_title')
    <title>សារី</title>
@stop

@section('mobile_cssStyle')
	<link href="{{ asset('/') }}/mobile_css/sary.css" rel="stylesheet" />


    <style type="text/css">
    	*:focus {
		    outline: none;
		}
    	input#resultDate {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 20px;
			    border: 1px #ccc solid;
			    border-radius:10px;
			    margin-left: 20px;
			    margin-right: 20px;
    	}
		
    	#resultDate{
    		text-align: center;
    	}
    	.table_sary_afternoon td{
    		padding-left: 5px;
    		padding-top: 5px;
    	}
    	.dateTitlesary{
    		padding-left: 10px;
    	}
    	
    	.tableSrey{
    		background-color: #FFF;
    		text-align: center;
    		width: 300px;
    	}
    	.tableSrey td {
		    border: 1px #ccc solid;
		    padding: 2px 10px 2px 2px;
		    font-size: 14px;
		    
		}
		.width200{
			width: 300px;
		}


    </style>
@stop


@section('mobile_content')
	<div class="container-fluid">
		
		<div class="row align-items-center bg-light pt-2 pb-2">
			
			<table align="center" cellpadding="0" cellspacing="0" border="0" style="height:100%;">
				<tbody>
					<tr>
						<td>
							<table>
								<tbody>
									<tr>
										<td>
											<img src="{{ asset('/') }}/img/sary/prev.png" id="prev" style="vertical-align: bottom">
										</td>
										<td class="text-center">
											<input type="text" name="date" id="resultDate">
										</td>
										<td>
											<img src="{{ asset('/') }}/img/sary/next.png" id="next" style="vertical-align: bottom">
										</td>
										<td width="20"></td>
										<td width="20"></td>
										<td></td>
										<td width="20"></td>
										<td><a href="{{URL::to('/mobile')}}/printelarger/1" target="_blank">ព្រីន តារាងគុណ 3 ខ្ទង់</a>
										</td>
										<td width="20"></td>
										<td><a href="{{URL::to('/mobile')}}/printelarger/2" target="_blank">ព្រីន តារាងគុណ 4 ខ្ទង់</a>
										</td>
										<td width="20"></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr style="display:none" id="ErrorMessage">
						<td id="error_msg">សូមបញ្ចូល ឆ្នាំ-ខែ-ថ្ងៃ ឱ្យបានត្រឹមត្រូវ</td>
					</tr>
				</tbody>
			</table>
			
		  
		
		</div>

		<div class="row pt-4 justify-content-md-center" id="saryContent">
				<div class="col-md-10 ">
					
					<div class="row pt-2">
						<article class="col-sm-6 col-md-6 col-lg-6 " id="displaySaryMorningv1"></article>
                        <article class="col-sm-6 col-md-6 col-lg-6" id="displaySaryMorning"></article>
	 					<article class="col-sm-6 col-md-6 col-lg-6 " id="displaySaryAfternoom"></article>
                        <article class="col-sm-6 col-md-6 col-lg-6" id="displaySaryEvening"></article>
					</div>
				</div>
				
				
				
			</div>
		</div>

		
				
	</div>


    	

       
      




@endsection


@section('mobile_javascript')
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/post.js?v=2"></script>

	
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/sary.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop