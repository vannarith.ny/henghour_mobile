<table width="100%" cellspacing="0" border="1">
	<tbody>
		@php 
			$totalPost = count($posts);
		@endphp
		@foreach ($posts as $key => $post)

			@if($key == 0)
				<tr align="center" id="post_tr_{{$key}}" style="height: 143px;">
			@endif

			@if( ($key+1) % 4 == 0 )
					<td class="posts_font2" style="color:#000" width="25%" data-id="{{$post['g_id']}}" data-digit="">{{$post['g_name']}}</td>
				</tr>
				<tr align="center" id="post_tr_{{$key}}" style="height: 143px;">
			@else
					<td class="posts_font2" style="color:#000" width="25%" data-id="{{$post['g_id']}}" data-digit="">{{$post['g_name']}}</td>
			@endif
			
			@if( ($key+1) == $totalPost )
				</tr>
			@endif
		
		@endforeach
	</tbody>
</table>