<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		@yield('mobile_title')

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="{{ asset('/') }}/mobile_css/jquery-ui.min.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/khmer-font.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/font-awesome.min.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/main.css?v=1" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/jquery.treetable.css" rel="stylesheet" />
	    <link href="{{ asset('/') }}/mobile_css/client.min.css?v=1" rel="stylesheet" />


		<!-- vannarith style -->

		<link href="{{ asset('/') }}/css/mobile_style.css" rel="stylesheet">

		@yield('mobile_cssStyle')


	</head>

	

	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		@if($page != 'sale' && $page != 'checkresult')
		<header id="header" class="bg-info">
			@include('mobile.header')
			
		</header>
		<div class="space"></div>
		<div class="nav-item">
			<a href="{{URL::to('/')}}/mobile/client" class="btn btn-outline-secondary @if($page == 'Client') active @endif ">ភ្នាក់ងារ</a>
			<a href="{{URL::to('/')}}/mobile/post" class="btn btn-outline-secondary @if($page == 'Post') active @endif ">ប៉ុស្ត៏</a>
			<a href="{{URL::to('/')}}/mobile/timeclose" class="btn btn-outline-secondary @if($page == 'time_close') active @endif ">ម៉ោងបិទ</a>
			<a href="{{URL::to('/')}}/mobile/checkresult" class="btn btn-outline-secondary">ផ្ទៀងលេខត្រូវ</a>
			<a href="{{URL::to('/')}}/mobile/sary" class="btn btn-outline-secondary @if($page == 'sary') active @endif ">សារី</a>
			<a href="{{URL::to('/')}}/mobile/dailyReport" class="btn btn-outline-secondary @if($page == 'dailyreport') active @endif" >ក្បាលបញ្ជី</a>
			<a href="{{URL::to('/')}}/mobile/checkreportdata" class="btn btn-outline-secondary @if($page == 'checkreportdata') active @endif">ពិនិត្យបញ្ជី</a>
			<a href="{{URL::to('/')}}/mobile/changePassword" class="btn btn-outline-secondary ">ប្ដូរពាក្យសំងាត់</a>
			<a href="{{URL::to('/')}}/mobile/transctionstaff" class="btn btn-outline-secondary @if($page == 'TransctionStaff') active @endif">ស៊ីសង</a>
			<a href="{{URL::to('/')}}/mobile/sale" class="btn btn-outline-secondary @if($page == 'sale') active @endif ">ចាក់ឆ្នោត</a>
			<a href="{{URL::to('/')}}/mobile/howtoplay" class="btn btn-outline-secondary">របៀបចាក់</a>
			<a href="{{URL::to('/')}}/mobile/logout" class="btn btn-outline-secondary">ចេញ</a>
		</div>
		
		@endif
		<input type="hidden" value="{{URL::to('/')}}" id="homeURL">
		<!-- END HEADER -->

		
		
		<!-- MAIN PANEL -->
		<div id="main" role="main">

			@yield('mobile_content')

		</div>
		<!-- END MAIN PANEL -->

		@include('mobile.footer')

		

		<script src="{{ asset('/') }}/mobile_js/jquery-1.10.2.min.js"></script>
	    <script src="{{ asset('/') }}/mobile_js/jquery.validate.js"></script>
	    <script src="{{ asset('/') }}/mobile_js/jquery-ui.min.js"></script>
	    <script src="{{ asset('/') }}/mobile_js/bootstrap.min.js"></script>
	    <!-- <script src="{{ asset('/') }}/mobile_js/inspect.min.js"></script> -->

		<script>
			$(document).ready(function($) {
                $( "#header-menu" ).click(function(event) {
                    $("div.nav-item").toggle();
                });
            });
		</script>
		@yield('mobile_javascript')


	</body>

</html>