@extends('mobile.master')

@section('mobile_title')
    <title>ប៉ុស</title>
@stop

@section('mobile_cssStyle')
    <style type="text/css">
    	*:focus {
		    outline: none;
		}
    	input.inputbt {
    		    background-color: rgba(255, 255, 255, 0.5);
			    font-size: 24px;
			    padding: 15px 20px;
			    border: 2px #ccc solid;
			    line-height: 24px;
			    border-radius:10px;
    	}

    	input.inputbt.active{
    		background-color: #ccc;

    	}
    	#main_table{
    		padding-top: 20px;
    	}
    	input[type=checkbox], input[type=radio]{
    		zoom:2;
    		margin-top: 5px;
    	}
    	.stylecontent td{
    		font-family: Verdana, Geneva, sans-serif;
    		font-size: 20px;
    	}


    </style>
@stop


@section('mobile_content')
	<div class="container-fluid">
		<input id="loginUserId" type="hidden" value="{{Session::get('iduserlotMobileSec')}}" />
		<form class="">
			<div class="row align-items-center bg-light pt-2 pb-2">
			
				<div class="col-md-3 ">
					<div class="row ">
					  <label class="col-md-3 pt-1" for="">កូន</label>
					  <select class="custom-select col-md-9" id="clientId" name="clientId" onchange="getPostVote()">
					    <option value="{{Session::get('iduserlotMobileSec')}}" selected>{{Session::get('usernameLotMobileSec')}}</option>
					    @foreach ($childs as $child)
					    	<option value="{{$child->s_id}}">{{$child->s_name}}</option>
						@endforeach
					  </select>
					</div>
				</div>
				<div class="col-md-2 ">
					<div class="row ">
						<label class="col-md-4 pt-1" for="">ពេល</label>
						<select class="custom-select col-md-8" id="timeId" name="timeId" onchange="getPostVote()">
						    <option selected>ជ្រេីសរេីស</option>
						    @foreach ($times as $time)
						    	<option value="{{$time->pav_id}}" @if($sheet_id == $time->pav_id) selected @endif>{{$time->pav_value}}</option>
							@endforeach
						</select>
					</div>
				</div>

				@if(count($staffFollow) > 0)
				<div class="col-md-3 ">
					<div class="row ">
						<label class="col-md-5 text-right pt-1" for="" >យកតាម</label>
						<select class="custom-select col-md-7" id="base_user_id" name="client2">
						    <option selected>ជ្រេីសរេីស</option>
						    @foreach ($staffFollow as $stafflist)
						    	<option value="{{$stafflist->s_id}}">{{$stafflist->s_name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				@endif
				<div class="col-md-4 ">
					<div class="row ">
						<!-- <button type="button" class="btn btn-outline-secondary btn-sm ml-2" onclick="check_all_defult();">យក Default</button> -->
						<button type="button" class=" btn btn-outline-secondary btn-sm ml-2" onclick="check_all();">យកទាំងអស់</button>
				  		<button type="button" class="btn btn-outline-secondary btn-sm ml-2" onclick="uncheck_all();">អត់យកទាំងអស់</button>
					</div>
				</div>
				
			  
			
			</div>
		</form>

		<div class="row pt-4">
			<div class="col-md-12 " id="td_users">
				<table cellpadding="0" cellspacing="0" width="100%">
					<thead align="center">
						<tr align="center" >
							<th class="ctd" width="60">បង្ហាញ</th>
							<th class="classtd1">ឈ្មោះប៉ុស្ត៏បង្ហាញ</th>
							<th class="classtd1">ឈ្មោះប៉ុស្ត៏ដើម</th>
							<th class="classtd1">2លេខ</th>
							<th class="classtd1">3លេខ</th>
						</tr>
					</thead>
					<tbody class="stylecontent">
						<tr>
							<td class="classtd2" align="center">
								<input type="checkbox" id="4" onchange="saveChange(4);">
							</td>
							<td class="classtd2">
								<input size="10" class="form-control" class="input" value="D">
							</td>
							<td class="classtd text-center">D</td>
							<td class="classtd text-center">x1</td>
							<td class="classtd text-center">x1</td>
							<td class="classtd text-center">x0</td>
						</tr>
						<tr>
							<td class="classtd2" align="center">
								<input type="checkbox" id="4" onchange="saveChange(4);">
							</td>
							<td class="classtd2">
								<input size="10" class="form-control" class="input" value="D">
							</td>
							<td class="classtd text-center">D</td>
							<td class="classtd text-center">x1</td>
							<td class="classtd text-center">x1</td>
							<td class="classtd text-center">x0</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		
				
	</div>


    	<!-- <table  cellspacing="0" id="main_table" width="100%">
    		<tbody>
    			
    			<tr valign="top">
    				<td style="padding:10px; text-align:center">
    					
    					
						<table border="0" cellpadding="1" cellspacing="0" width="100%">
							<thead align="center">
								<tr align="center">
									<th class="ctd">បង្ហាញ</th>
									<th class="classtd1">ឈ្មោះប៉ុសបង្ហាញ</th>
									<th class="classtd1">ឈ្មោះប៉ុសដេីម</th>  
									<th class="classtd1">x2</th>  
									<th class="classtd1">x3</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<input type="checkbox" name="" id="checkpost_1" value="1" class="checkboxpost">
									</td>
									<td>
										<input type="text" id="txtpost_1" name="txtpost" value="ABC" class="txtpostname" >
									</td>
									<td>
										A, B, C
									</td>
									<td>
										3
									</td>
									<td>
										3
									</td>
								</tr>
							</tbody>
						</table>
    				</td>
    			</tr>
    		</tbody>
    	</table> -->  

       
      




@endsection


@section('mobile_javascript')
	<script> 
    	var IsAdmin = 'False' == 'True' ? true : false; 
		var IsCanCreateChild = 'False' == 'True' ? true : false; 
		var IsHasDollar = 'True' == 'True' ? true : false; 
	</script>
	<script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/post.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop