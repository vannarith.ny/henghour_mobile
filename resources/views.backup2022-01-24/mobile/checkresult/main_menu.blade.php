<table cellpadding="0" cellspacing="5" width="100%">
												<tbody>
													<tr>
														<td><a href="{{URL::to('/')}}/mobile/client"><i class="fa fa-users" aria-hidden="true"></i> ភ្នាក់ងារ</a>
														</td>
														<td><a href="{{URL::to('/')}}/mobile/post"><i class="fa fa-desktop" aria-hidden="true"></i> ប៉ុស្ត៏</a>
														</td>
													</tr>
													<tr>
														<td><a href="{{URL::to('/')}}/mobile/timeclose"><i class="fa fa-times" aria-hidden="true"></i> ម៉ោងបិទ</a>
														</td>
														<td><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> បង្ហាញសាង</a>
														</td>
													</tr>
													<tr>
														<td><a href="{{URL::to('/')}}/mobile/checkresult"><i class="fa fa-trophy" aria-hidden="true"></i> ផ្ទៀងលេខត្រូវ </a>
														</td>
														<td><a href="{{URL::to('/')}}/mobile/sary"><i class="fa fa-plug" aria-hidden="true"></i> សារី</a>
														</td>
													</tr>
													<tr>
														<td><a href="{{URL::to('/')}}/mobile/dailyReport"><i class="fa fa-list-ol" aria-hidden="true"></i> ក្បាលបញ្ជី</a>
														</td>
														<td><a href="{{URL::to('/')}}/mobile/checkreportdata"><i class="fa fa-check-square-o" aria-hidden="true"></i> ពិនិត្យបញ្ជី</a>
														</td>
													</tr>
													<tr>
														<td><a href="{{URL::to('/')}}/mobile/changePassword"><i class="fa fa-key" aria-hidden="true"></i> ប្ដូរពាក្យសំងាត់</a>
														</td>
														<td><a href="#"><i class="fa fa-exchange" aria-hidden="true"></i> ស៊ីសង</a>
														</td>
													</tr>
													<tr>
														<td class=""><a href="{{URL::to('/')}}/mobile/sale"><i class="fa fa-play" aria-hidden="true"></i> ចាក់ឆ្នោត</a>
														</td>
														<td><a href="{{URL::to('/')}}/mobile/howtoplay"><i class="fa fa-question-circle" aria-hidden="true"></i> របៀបចាក់</a>
														</td>
													</tr>
													<tr>
														<td><a href="{{URL::to('/')}}/mobile/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> ចេញ</a>
														</td>
														<td></td>
													</tr>
												</tbody>
											</table>