@extends('mobile.master')

@section('mobile_title')
    <title>របៀបចាក់ឆ្នោត</title>
@stop

@section('mobile_cssStyle')
    <style type="text/css">
    	*:focus {
		    outline: none;
		}
        #td_howtoplay table{
            background-color:#d9edf8;
            text-align:left !important;
            font-size:20px;
        }
    	


    </style>
@stop


@section('mobile_content')
	<div class="container-fluid">
		

		<div class="row pt-4">
			<div class="col-md-12 " id="td_howtoplay">
            
	
			<table cellspacing="0">
				<tbody>
					<tr style="display:none">
						<td width="100" onclick="document.frm.form_phone.value=0; document.getElementById('image_form_l').style.display=''; document.getElementById('image_form_v').style.display='none';">
							<input type="radio" name="form_phone" &#8203;&#8203;="" value="0" checked="checked">ទំរង់ផ្ដេក</td>
						<td onclick="document.frm.form_phone.value=1; document.getElementById('image_form_l').style.display='none'; document.getElementById('image_form_v').style.display='';">
							<input value="1" type="radio" name="form_phone" &#8203;&#8203;="">ទំរង់បញ្ឈរ</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>បញ្ចូលលេខ : </strong>
										</td>
										<td>ចាក់ឆ្នោតត្រូវចុចម៉ឺនុយ</td>
										<td>
											" ចាក់ឆ្នោត "
										</td>
										<td>បន្ទាប់​មក វាយលេខរួច</td>
										<td><span style="font-size:36px; line-height:18px; font-weight:bold">↵</span>ដើម្បីបញ្ចូលទឹកលុយ រួច&#8203;<span style="font-size:36px; line-height:18px; font-weight:bold">↵</span>
											<br><span style="font-size:36px; line-height:18px; font-weight:bold">+</span>បើទឹកលុយដូចមានស្រាប់</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>ជ្រើសរើសប៉ុស្ត៏: </strong>
										</td>
										<td>ចុចលើ <strong>ប៉ុស៏្ត </strong>
										</td>
										<td>វាយលេខរួច ចុចលើប៉ុស្ត៏ណាមួយដើម្បីរើសយក</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>លេខគុណ: </strong>
										</td>
										<td>វាយលេខរួចចុចលើ <strong style="font-family:Arial, Helvetica, sans-serif">X </strong>
											<br>ចុចលើ <strong style="font-family:Arial, Helvetica, sans-serif">xx3</strong> ចំពោះ X3លេខច្រើន
											<br>ចុចលើ <strong style="font-family:Arial, Helvetica, sans-serif">xx2</strong> ចំពោះ X2លេខច្រើន</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>លេខអូស: </strong>
										</td>
										<td>វាយលេខរួច</td>
										<td>ចុចលើ <span style="font-size:24px; line-height:18px; font-weight:bold">↓ </span> រួចវាយលេខខាងក្រោម រួច&#8203;<span style="font-size:36px; line-height:18px; font-weight:bold">↵</span>
											<br>ចុចលើ <span style="font-size:24px; line-height:18px; font-weight:bold">↑ </span> សំរាប់អូសក្បាលដល់ 9&#8203; រួចវាយទឹកលុយតែម្ដង
											<br>ចុចលើ <span style="font-size:24px; line-height:18px; font-weight:bold"> &gt; </span> សំរាប់អូសកន្ទុយដល់ 9&#8203; រួចវាយទឹកលុយតែម្ដង
											<br>ចុចលើ <span style="font-size:22px; line-height:18px; font-weight:bold"> ← </span> សំរាប់អូសកណ្ដាល3លេខ&#8203;ដល់ 9&#8203; រួចវាយទឹកលុយតែម្ដង
											<br>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>ថយពីលេខអូស: </strong>
										</td>
										<td>ចុចលើ <span style="font-size:24px; line-height:18px; font-weight:bold">↑ </span>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>លេខជឹង: </strong>
										</td>
										<td>ចុចលើ <span style="font-size:24px; line-height:18px; font-weight:bold">→ </span> រួចវាយលេខ&#8203;  រួចវាយលេខបន្ទាប់​ វាមាន២លេខរី៣លេខតាមអ្នកចង់</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>ថយពីលេខជឹង: </strong>
										</td>
										<td>ចុចលើ <span style="font-size:24px; line-height:18px; font-weight:bold">← </span>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tbody>
									<tr valign="top">
										<td><strong>វគ្គថ្មី: </strong>
										</td>
										<td>ចុចលើ <strong>វគ្គថ្មី</strong> សម្រាប់ចូលចាក់កន្ទុយថ្មី</td>
									</tr>
									<tr valign="top">
										<td><strong>ចាក់: </strong>
										</td>
										<td>ចុចលើ <strong>ចាក់</strong> សម្រាប់មើលទឹកលុយបូក និង កាត់ទឹក រឺ ត្រលប់មគចាក់វេញ</td>
									</tr>
									<tr valign="top">
										<td><strong>បង្ហាញ: </strong>
										</td>
										<td>ចុចលើ <strong>បង្ហាញ</strong> សម្រាប់មើលលេខពេញមួយក្រដាស</td>
									</tr>
									<tr valign="top">
										<td><strong>លុប: </strong>
										</td>
										<td>ចុចលើ <strong>លុប</strong> សម្រាប់លុបដែលបញ្ចូលរួចចុងក្រោយគេ</td>
									</tr>
									
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		
			</div>
		</div>

		
				
	</div>

       
      




@endsection


@section('mobile_javascript')
	<!-- <script src="{{ asset('/') }}/mobile_js/jquery.treetable.js"></script>
	<script src="{{ asset('/') }}/mobile_js/dragdrop.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/popup2.min.js"></script>
	<script src="{{ asset('/') }}/mobile_js/post.js?v=2"></script> -->
    <script type="text/javascript">
        $(document).ready(function() {
            

        });
    </script>
@stop