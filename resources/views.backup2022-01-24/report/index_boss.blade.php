@extends('master')
<?php
use App\Http\Controllers\SaleController;
?>
@section('title')
<title>របាយការណ៏</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:25px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:18px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'reportFilter', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 @if(isset($var_page))
							 <?php $page_filter = $var_page; ?>
							 <?php $attr_page = $var_page; ?>
						 @else
							 <?php $page_filter = null; ?>
							 <?php $attr_page = 0; ?>
						 @endif

						 @if(isset($var_sheet))
							 <?php $sheet_filter = $var_sheet; ?>
							 <?php $attr_sheet = $var_page; ?>
						 @else
							 <?php $sheet_filter = null; ?>
							 <?php $attr_sheet = 0; ?>
						 @endif
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('type_lottery', ([
                                        '' => trans('result.chooseTypeLottery') ]+$type_lottery),$var_type_lottery,['class' => 'required ','id'=>'type_lottery','sms'=> trans('result.pleaseChooseTypeLottery') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('staff', ([
                                        '' => trans('result.chooseStaff') ]+$staff),$staff_filter,['class' => 'required ','id'=>'staff','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('sheet', ([
                                        '' => trans('result.chooseShift') ]+$sheets),$sheet_filter,['class' => ' ','id'=>'sheet','sms'=> trans('result.pleaseChooseShift') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>



							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('page', ([
                                        '' => trans('result.choosePage') ]+$pages),$page_filter,['class' => ' ','id'=>'pages','sms'=> trans('result.pleaseChoosePage') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 @if(isset($report))

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 <?php if($staff_boss==1){ ?>
							 .paperStyle{
								 display: none;
							 }
							 .paperStyle:last-child{
								 display: block !important;
							 }
							 .staff_name{
								 display: none;
							 }
							 <?php }?>
							 
						 </style>



						 <div class="widget-body no-padding control_width">

							

							 <?php
							 $twodigit_charge = 0;
							 $threedigit_charge = 0;
							 // var_dump($var_dateStart);
							 $getStaffCharge = DB::table('tbl_staff_charge')
									 ->select('stc_id','stc_three_digit_charge','stc_two_digit_charge','stc_pay_two_digit','stc_pay_there_digit')
									 ->where('s_id',$var_staff)
									 ->where('stc_type',$var_type_lottery)
									 ->where('stc_date','<=',$var_dateStart)
									 ->orderBy('stc_date','DESC')
									 ->first();
								 // dd($getStaffCharge);
							 if(isset($getStaffCharge)){
								 $twodigit_charge = $getStaffCharge->stc_two_digit_charge;
								 $threedigit_charge = $getStaffCharge->stc_three_digit_charge;
								 if($twodigit_charge == $threedigit_charge){
								 	$hidedata = 'hidetwothree_digit';
								 	$sumHide = '';
								 	$colspan = 1;
								 }else{
								 	$hidedata = '';
								 	$sumHide = 'hidetwothree_digit';
								 	$colspan = 2;
								 }

								 if($var_type_lottery == 1 ){
									if($getStaffCharge->stc_pay_two_digit > 0){
										$win_two_digit = $getStaffCharge->stc_pay_two_digit;
									}else{
										$win_two_digit = 70;
									}

									if($getStaffCharge->stc_pay_there_digit > 0){
										$win_three_digit = $getStaffCharge->stc_pay_there_digit;
									}else{
										$win_three_digit = 600;
									}
								 }else if($var_type_lottery == 2 || $var_type_lottery == 3 || $var_type_lottery == 4){

									if($getStaffCharge->stc_pay_two_digit > 0){
										$win_two_digit = $getStaffCharge->stc_pay_two_digit;
									}else{
										$win_two_digit = 90;
									}

									if($getStaffCharge->stc_pay_there_digit > 0){
										$win_three_digit = $getStaffCharge->stc_pay_there_digit;
									}else{
										$win_three_digit = 800;
									}

								 }


							 }else{
							 	if($var_type_lottery == 1){
										$win_two_digit = 70;
										$win_three_digit = 600;
								 }else if($var_type_lottery == 2 || $var_type_lottery == 3 || $var_type_lottery == 4){
										$win_two_digit = 90;
										$win_three_digit = 800;
								 }
							 }






							 ?>
							 @if($var_sheet=='' && $var_page=='')
							 	@if(Session::get('roleLot') == 1)
								 <div id="html-content-holder" class="widget-body paperStyle">
								 	@if(isset($check_staff_main))
									 <h1>
									 	{{trans('label.summary_result_per_day')}}
									 	@if($var_type_lottery == 1)
									 		VN
									 	@elseif($var_type_lottery == 2)
									 		KH
									 	@else
									 		TH
									 	@endif
									 </h1><br>
									 <div class="col-md-3">
										 {{trans('label.staff_name')}} :
										 <b>{{$check_staff_main->s_name}} </b>
									 </div>

									 <div class="col-md-2">
										 <b>Lottery</b>
									 </div>

									 <div class="col-md-2">
										 {{trans('label.date')}} :
										 <b><?php echo date("d-m-Y", strtotime($var_dateStart));?></b>
									 </div>
									 @endif
									 <br><br>
									 <table id="" class="table table-bordered summary_result_per_day" style="width:75%;">
										 <thead>
										 <tr>
										 	 <th></th>
											 <th></th>
											 <th></th>
											 <th colspan="{{$colspan}}">KHR</th>
											 <th>លេខត្រូវ </th>
											 <th>លេខត្រូវ </th>
											 <th colspan="{{$colspan}}">USD</th>
											 <th>លេខត្រូវ</th>
											 <th>លេខត្រូវ</th>
										 </tr>
										 <tr>
										 	 <th>{{trans('label.staff')}}</th>
											 <th>{{trans('label.time')}}</th>
											 <th>page</th>
											 <th class="{{$hidedata}}">2D</th>
											 <th class="{{$hidedata}}">3D</th>
											 <th class="{{$sumHide}}">លុយ</th>
											 <th>2D</th>
											 <th>3D</th>
											 <th class="{{$hidedata}}">2D</th>
											 <th class="{{$hidedata}}">3D</th>
											 <th class="{{$sumHide}}">លុយ</th>
											 <th>2D</th>
											 <th>3D</th>
										 </tr>
										 </thead>
										 <tbody class="display_total_result">
										 	<?php 
										 		$staff_n = '';
										 		$time_n = '';
										 	?>
										 	@foreach($boss_data as $key => $data)
										 		<?php 
											 		if($staff_n != $data->staff_value){
											 			$name_staff_display = $data->staff_value;
											 			$staff_n = $data->staff_value;

											 			$time_n = '';
											 		}else{
											 			$name_staff_display = '';
											 		}

											 		if($time_n != $data->time_value){
											 			$time_display = $data->time_value;

											 			$find = array("0","1","2","3");
														$replace = array("");
														$time_display = str_replace($find,$replace,$time_display);

											 			$time_n = $data->time_value;
											 		}else{
											 			$time_display = '';
											 		}
											 		// var_dump(number_format($data->total2digitr));
											 		// dd($main_new);
											 		if(count($main_new)!=0){
											 			$price_calculate = DB::table('tbl_staff_charge_main')
											 								->where('s_id',$check_staff_main->s_id)
											 								->where('chail_id',$data->id_staff)
											 								->where('stc_type',$var_type_lottery)
											 								->orderBy('stc_date','DESC')
											 								->take(1)
											 								->get();
											 			foreach ($price_calculate as $keyss => $data_ss) {
											 				$cal_2_digit = $data_ss->stc_two_digit_charge;
											 				$cal_3_digit = $data_ss->stc_three_digit_charge;
											 			}
											 			
											 		}else{
											 			$cal_2_digit = $twodigit_charge;
											 			$cal_3_digit = $threedigit_charge;
											 		}
										 		?>
										 		<tr class="many_row">
										 			<td id="td_staff_{{$key}}" id_staff="{{$data->id_staff}}" two="{{$cal_2_digit}}" three="{{$cal_3_digit}}">{{$name_staff_display}}</td>
										 			<td id="td_time_'+index+'">{{$time_display}}</td>
										 			<td id="td_page_'+index+'">{{$data->page_value}}</td>

										 			<td class="{{$hidedata}} total2digitr" >@if($data->total2digitr != 0) {!! floatval($data->total2digitr) !!} @endif</td>
										 			<td class="{{$hidedata}} total3digitr" >@if($data->total3digitr != 0) {!! floatval($data->total3digitr) !!} @endif</td>
										 			<td class="{{$sumHide}} totalsum" >@if($data->totalsum != 0) {{ floatval($data->totalsum) }} @endif</td>
										 			<td class="total2digitrright" style="width:300px;">@if($data->total2digitrright != 0) {!! floatval($data->total2digitrright) !!} @endif</td>
										 			<td class="total3digitrright">@if($data->total3digitrright != 0) {{ floatval($data->total3digitrright) }} @endif</td>
										 			<td class="{{$hidedata}} total2digits">@if($data->total2digits != 0) {{ floatval($data->total2digits) }} @endif</td>
										 			<td class="{{$hidedata}} total3digits">@if($data->total3digits != 0) {{ floatval($data->total3digits) }} @endif</td>
										 			<td class="{{$sumHide}} totalsum_dolla">@if($data->totalsum_dolla != 0) {{ floatval($data->totalsum_dolla) }} @endif</td>
										 			<td class="total2digitsright">@if($data->total2digitsright != 0) {{ floatval($data->total2digitsright) }} @endif</td>
										 			<td class="total3digitsright">@if($data->total3digitsright != 0) {{ floatval($data->total3digitsright) }} @endif</td>

										 		</tr>

										 	@endforeach
										 </tbody>
									 </table>
								</div>
								@endif

							 @endif
						 </div>




					 @endif



		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		$(document).off('change', '#type_lottery').on('change', '#type_lottery', function(e){
			
			var id = $(this).val();
			$("#staff").prop( "disabled", true );
			$("#sheet").prop( "disabled", true );
            if(id != ''){
            	$.ajax({
				   url: 'getstaffbylotterytype',
			       type: 'GET',
			       data: {type:id},
			       success: function(data) {
			           if(data.status=="success"){

			           		$("#staff").html(data.msg);
			           		$("#sheet").html(data.msg1);

			           		$("#staff").prop( "disabled", false );
			           		$("#sheet").prop( "disabled", false );

			      			console.log(data.msg);

			           }else{
			           }
			        }
			     });
            }else{

            	$("#staff").html('<option value="" selected="selected">រើសបុគ្គលិក</option>');
			    $("#sheet").html('<option value="" selected="selected">ជ្រើសរើសពេល</option>');
            	$("#staff").prop( "disabled", false );
			    $("#sheet").prop( "disabled", false );
            }
			

		});


		$(document).off('submit', '#filter').on('submit', '#filter', function(e){
			if (validate_form_main('#filter') == 0) {
				return true;
			}
			return false;
		});

@if(isset($report))

		function commaSeparateNumber(val){
			while (/(\d+)(\d{3})/.test(val.toString())){
				val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
			}
			return val;
		}

		function addCommas(nStr)
		{
		    nStr += '';
		    x = nStr.split('.');
		    x1 = x[0];
		    x2 = x.length > 1 ? '.' + x[1] : '';
		    var rgx = /(\d+)(\d{3})/;
		    while (rgx.test(x1)) {
		        x1 = x1.replace(rgx, '$1' + ',' + '$2');
		    }
		    return x1 + x2;
		}



		$(document).ready(function() {
			var total2digitr = '';
			var total2digits = '';
			var total3digitr = '';
			var total3digits = '';
			var total2digitrright = '';
			var total2digitsright = '';
			var total3digitrright = '';
			var total3digitsright = '';
			var time = '';
			var staff_list = '';
			var page = '';
			var tr = '';
			var sum_total2digitrright = 0;
			var sum_total2digitsright = 0;
			var sum_total3digitrright = 0;
			var sum_total3digitsright = 0;

			var num_total2digitrright = 0;
			var num_total2digitsright = 0;
			var num_total3digitrright = 0;
			var num_total3digitsright = 0;


			var twodigit_charge = <?php echo $twodigit_charge;?>;
			var threedigit_charge = <?php echo $threedigit_charge;?>;

			var sum_total2digitr = 0;
			var sum_total2digits = 0;
			var sum_total3digitr = 0;
			var sum_total3digits = 0;


			var sum_total2digitr_new = 0;
			var sum_total2digits_new = 0;
			var sum_total3digitr_new = 0;
			var sum_total3digits_new = 0;

			var data_array = new Array();

			var total_page_sum = 0;
			$(".many_row").each(function(index, element) {
				total_page_sum = total_page_sum + 1;

				total2digitr = $(this).find('.total2digitr').text();
				total2digits = $(this).find('.total2digits').text();
				total3digitr = $(this).find('.total3digitr').text();
				total3digits = $(this).find('.total3digits').text();
				total2digitrright = $(this).find('.total2digitrright').text();
				total2digitsright = $(this).find('.total2digitsright').text();
				total3digitrright = $(this).find('.total3digitrright').text();
				total3digitsright = $(this).find('.total3digitsright').text();

				var id_staff = $(this).find('#td_staff_'+index).attr('id_staff');
				var twodigit_charge = $(this).find('#td_staff_'+index).attr('two');
				var threedigit_charge = $(this).find('#td_staff_'+index).attr('three');
					// alert(id_staff);

				if(total2digitr==0){
					numtotal2digitr = 0;
					total2digitr = '';
				}else{
					numtotal2digitr = total2digitr;
					total2digitr = total2digitr;
				}

				if(total2digits==0){
					numtotal2digits = 0;
					total2digits = '';
				}else{
					numtotal2digits = total2digits;
					total2digits = total2digits;
				}

				if(total3digitr==0){
					numtotal3digitr = 0;
					total3digitr = '';
				}else{
					numtotal3digitr = total3digitr;
					total3digitr = total3digitr;
				}

				var totalsum = parseFloat(numtotal2digitr) + parseFloat(numtotal3digitr);
				if(totalsum==0){
					totalsum = '';
				}

				if(total3digits==0){
					numtotal3digits = 0;
					total3digits = '';
				}else{
					numtotal3digits = total3digits;
					total3digits = total3digits;
				}

				if(total2digitrright==0){
					num_total2digitrright = 0;
					total2digitrright = '';
				}else{
					num_total2digitrright = total2digitrright;
					total2digitrright = total2digitrright;

				}
				if(total2digitsright==0){
					num_total2digitsright = 0;
					total2digitsright = '';
				}else{
					num_total2digitsright = total2digitsright;
					total2digitsright = total2digitsright;
				}
				if(total3digitrright==0){
					num_total3digitrright = 0;
					total3digitrright = '';
				}else{
					num_total3digitrright = total3digitrright;
					total3digitrright = total3digitrright;
				}
				if(total3digitsright==0){
					num_total3digitsright = 0;
					total3digitsright = '';
				}else{
					num_total3digitsright = total3digitsright;
					total3digitsright = total3digitsright;
				}

				var totalsum_dolla = parseFloat(numtotal2digits) + parseFloat(numtotal3digits);
				if(totalsum_dolla==0){
					totalsum_dolla = '';
				}

				sum_total2digitr = sum_total2digitr + parseFloat(numtotal2digitr);
				sum_total2digits = sum_total2digits + parseFloat(numtotal2digits);
				sum_total3digitr = sum_total3digitr + parseFloat(numtotal3digitr);
				sum_total3digits = sum_total3digits + parseFloat(numtotal3digits);

				sum_total2digitrright = sum_total2digitrright + parseFloat(num_total2digitrright);
				sum_total2digitsright = sum_total2digitsright + parseFloat(num_total2digitsright);
				sum_total3digitrright = sum_total3digitrright + parseFloat(num_total3digitrright);
				sum_total3digitsright = sum_total3digitsright + parseFloat(num_total3digitsright);

				sum_total2digitr_new = sum_total2digitr_new + (
																Math.round(

																		((parseFloat(numtotal2digitr) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digitr) * parseFloat(threedigit_charge)) / 100)
																	
																)
															);


				sum_total2digits_new = sum_total2digits_new + 

															(
																

																		((parseFloat(numtotal2digits) * parseFloat(twodigit_charge)) / 100)
																	
																+
																	
																		((parseFloat(numtotal3digits) * parseFloat(threedigit_charge)) / 100)
																	
																
															);


			});


			var total_income_expense_riel = 0;
			var total_income_expense_dollar = 0;
			<?php 
			if($var_type_lottery == 1){
			?>
				
				@if(count($main_new)!=0)
					var income_riel = sum_total2digitr_new;
					var income_dollar = sum_total2digits_new

				@else
					var income_riel = parseInt(Math.round(((parseInt(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseInt(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
					var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				@endif



				// alert(sum_total2digitr_new);

				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
				
			<?php }else{?>
				var income_riel = parseFloat(Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));
				var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
				var expense_riel = (parseInt(sum_total2digitrright * <?php echo $win_two_digit;?>) + parseInt(sum_total3digitrright * <?php echo $win_three_digit;?>) );
				var expense_dollar = (parseFloat(sum_total2digitsright * <?php echo $win_two_digit;?>) + parseFloat(sum_total3digitsright * <?php echo $win_three_digit;?>));
			<?php }?>
			 // var a = Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100));
			 // var b = Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100));
			 // var aaaa = a + b;
			// alert( income_riel );
			var staff_id = "<?php echo $staff_filter;?>";
			
			if(staff_id == 658 || staff_id == 659){
					var income_riel = parseFloat(Math.round(((parseFloat(sum_total2digitr) * parseFloat(twodigit_charge)) / 100)) + Math.round(((parseFloat(sum_total3digitr) * parseFloat(threedigit_charge)) / 100)));

					var income_dollar = (((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100) + ((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100)).toFixed(2);
					// alert(income_riel);
					var new_price = sum_total2digitr_new;
					var new_price_s = sum_total2digits_new.toFixed(2);
			}else{
				var new_price = income_riel;
				var new_price_s = income_dollar;
			}

			var total_income_expense_riel = income_riel - expense_riel;
			var check_total = total_income_expense_riel.toString().substr(-2);
            // console.log(total_income_expense_riel);
            // console.log(check_total);

            
            
					




		if(staff_id == 492 || staff_id == 491 || staff_id == 490 || staff_id == 363 || staff_id == 266 || staff_id == 389 || staff_id == 274 || staff_id == 276 || staff_id == 275 || staff_id == 271 || staff_id == 316 || staff_id == 315 || staff_id == 269 || staff_id == 277 || staff_id == 307 || staff_id == 309 || staff_id == 534 || staff_id == 564 || staff_id == 598 || staff_id == 599 || staff_id == 604 || staff_id == 603 || staff_id == 602 || staff_id == 605 || staff_id == 606 || staff_id == 609 || staff_id == 618 || staff_id == 619 || staff_id == 624 || staff_id == 623 || staff_id == 630 || staff_id == 633 || staff_id == 634 || staff_id == 658 || staff_id == 659 || staff_id == 619 || staff_id == 637 || staff_id == 597 || staff_id == 636 || staff_id == 668 || staff_id == 672 || staff_id == 749 || staff_id == 760 || staff_id == 767){

			// alert(staff_id);

        }else{
			if(check_total >= 50){
                // console.log('1');
                if(total_income_expense_riel > 0 ) {
                    total_income_expense_riel = total_income_expense_riel + ( 100 - parseInt(check_total));
                }else{
                    total_income_expense_riel = total_income_expense_riel - ( 100 - parseInt(check_total));
                }
			}else{
                // console.log('2');
                if(total_income_expense_riel > 0 ){
                    total_income_expense_riel = parseInt(total_income_expense_riel) - parseInt(check_total);
                }else{
                    total_income_expense_riel = parseInt(total_income_expense_riel) + parseInt(check_total);
                }

			}
		}

			if(total_income_expense_riel>0){
				total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = total_income_expense_riel;
            }else{
            	total_income_expense_riel_noStr = total_income_expense_riel;
                total_income_expense_riel = '('+total_income_expense_riel+')';
            }

			var total_income_expense_dollar = income_dollar - expense_dollar;
			var total_income_expense_dollar_noStr = total_income_expense_dollar;
            if(total_income_expense_dollar>0){
                total_income_expense_dollar = total_income_expense_dollar.toFixed(2);
            }else{
                total_income_expense_dollar = '('+total_income_expense_dollar.toFixed(2)+')';
            }

            var sum_totalall = sum_total2digitr + sum_total3digitr;
            var sum_totalall_dolla = sum_total2digits + sum_total3digits;

            <?php 
            if($var_dateStart <= '2018-05-10'){
            	if($var_staff == 236 || $var_staff == 423 || $var_staff == 297 || $var_staff == 361 || $var_staff == 314){
            ?>
            	total_page_sum = total_page_sum + 36;
			<?php }}?>

			tr = '<tr class="bee_highlight"><th>{{trans('label.total')}}</th><th></th><th >'+total_page_sum+'</th><th class="{{$hidedata}}">'+sum_total2digitr+'</th><th class="{{$hidedata}}">'+sum_total3digitr+'</th><th class="{{$sumHide}}">'+sum_totalall+'</th><th>'+sum_total2digitrright+'</th><th>'+sum_total3digitrright+'</th><th class="{{$hidedata}}">$ '+sum_total2digits+'</th><th class="{{$hidedata}}">$ '+sum_total3digits+'</th><th class="{{$sumHide}}">$ '+sum_totalall_dolla+'</th><th>$ '+sum_total2digitsright+'</th><th>$ '+sum_total3digitsright+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class="bee_highlight"><td></td><td>{{trans('label.total_charge')}}</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total2digitr) * parseInt(twodigit_charge)) / 100)))+'</td><td>'+commaSeparateNumber(Math.round(((parseInt(sum_total3digitr) * parseInt(threedigit_charge)) / 100)))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitrright * 70)+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitrright * 600)+'</td><td>'+commaSeparateNumber(((parseInt(sum_total2digits) * parseInt(twodigit_charge)) / 100).toFixed(2))+'</td><td>'+commaSeparateNumber(((parseInt(sum_total3digits) * parseInt(threedigit_charge)) / 100).toFixed(2))+'</td><td class="result_right">'+commaSeparateNumber(sum_total2digitsright * 70 )+'</td><td class="result_right">'+commaSeparateNumber(sum_total3digitsright * 600)+'</td><tr>';
			// $('.display_total_result').append(tr);
            var total_rate_thesame = parseInt(sum_total2digitr) + parseInt(sum_total3digitr);
            var total_rate_thesame_dolla = parseFloat(sum_total2digits) + parseFloat(sum_total3digits); 
			tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result').append(tr);

			
			// 2D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th></th><th></th><th>2D</th><th>'+sum_total2digitr+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+Math.round(((parseInt(sum_total2digitr) * parseFloat(twodigit_charge)) / 100))+'</th><th></th><th>$ '+sum_total2digits+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>$ '+((parseFloat(sum_total2digits) * parseFloat(twodigit_charge)) / 100).toFixed(2)+'</th><th></th></tr>';
			$('.display_total_result').append(tr);
			// 3D
			tr = '<tr class="bee_highlight {{$hidedata}}"><th></th><th></th><th>3D</th><th>'+sum_total3digitr+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+Math.round(((parseInt(sum_total3digitr) * parseFloat(threedigit_charge)) / 100))+'</th><th></th><th>$ '+sum_total3digits+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>$ '+((parseFloat(sum_total3digits) * parseFloat(threedigit_charge)) / 100).toFixed(2)+'</th><th></th></tr>';
			$('.display_total_result').append(tr);

			tr = '<tr class="bee_highlight {{$sumHide}}"><th></th><th>Total</th><th>'+total_rate_thesame+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+new_price+'</th><th></th><th>$ '+total_rate_thesame_dolla+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>$ '+new_price_s+'</th></tr>';
			$('.display_total_result').append(tr);

			// tr = '<tr class=""><td colspan="11"></td></tr>';
			// $('.display_total_result').append(tr);




			<?php 
			if($var_type_lottery == 1){
			?>
				// 2D(T)
				tr = '<tr class="bee_highlight"><th></th><th class="{{$hidedata}}"></th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th></th><th>'+sum_total2digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th></th><th>'+sum_total3digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
			<?php }else if($var_type_lottery == 2 || $var_type_lottery == 3 || $var_type_lottery == 4){?>
				// 2D(T)
				tr = '<tr class="bee_highlight"><th></th><th class="{{$hidedata}}"></th><th>2D(T)</th><th>'+sum_total2digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitrright * <?php echo $win_two_digit;?>+' </th><th></th><th>'+sum_total2digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total2digitsright * <?php echo $win_two_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
				// 3D(T)
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th>3D(T)</th><th>'+sum_total3digitrright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitrright * <?php echo $win_three_digit;?>+'</th><th></th><th>'+sum_total3digitsright+'</th><th><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></th><th>'+sum_total3digitsright * <?php echo $win_three_digit;?>+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);
			<?php }?>


			



			tr = '<tr class=""><td colspan="11"></td></tr>';
			$('.display_total_result').append(tr);


			// tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th></th><th></th><th class="result_right_total">'+total_income_expense_riel+' </th><th></th><th></th><th></th><th class="result_right_total">$ '+total_income_expense_dollar+'</th><th class="{{$hidedata}}"></th></tr>';
			// $('.display_total_result').append(tr);


			// paid or borrow


			var totalRealBefore = total_income_expense_riel_noStr;
			var totalDollaBefore = total_income_expense_dollar_noStr;

			if(totalRealBefore < 0){
				var labelBefore_r = 'ខាត';
			}else{
				var labelBefore_r = 'សុី';
			}

			if(totalDollaBefore < 0){
				var labelBefore_d = 'ខាត';
			}else{
				var labelBefore_d = 'សុី';
			}	

			// price before percent
			tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">'+labelBefore_r+'</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(totalRealBefore)+' </th><th></th><th></th><th>'+labelBefore_d+'</th><th class="result_right_total" style="text-align:left !important;">$ '+addCommas(totalDollaBefore)+'</th><th class="{{$hidedata}}"></th></tr>';
			$('.display_total_result').append(tr);

			@if($percentStaff > 0)
				var percent_sub = parseFloat('{{$percentStaff}}');
				var percent = 100 - percent_sub;

				var totalReal = parseFloat(totalRealBefore * percent_sub)/100;
				var totalDolla = parseFloat(totalDollaBefore * percent_sub)/100;



				// price after percent 
				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">'+percent_sub+'%</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(totalReal)+' </th><th></th><th></th><th>%</th><th class="result_right_total" style="text-align:left !important;">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);

				// chail_id
				var totalReal_main = parseFloat(totalRealBefore * percent)/100;
				var totalDolla_main = parseFloat(totalDollaBefore * percent)/100;

				tr = '<tr class=""><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">'+percent+'%</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(totalReal_main)+' </th><th></th><th></th><th>%</th><th class="result_right_total" style="text-align:left !important;">$ '+totalDolla_main.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
				$('.display_total_result').append(tr);


			@else

				var totalReal = totalRealBefore;
				var totalDolla = totalDollaBefore;

			@endif

@if($var_type_lottery == 1 || $var_type_lottery == 2)
			var displayMainTotal = 0;
			<?php 
				if (!empty($money_paid) && $var_type_lottery == 1) {
					foreach($money_paid as $key => $value) {
						if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23){
							if($value->st_type == 21 && $value->st_price_r < 0 ){
								$label_r = 'ខាត';
							}else{
								$label_r = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_d < 0 ){
								$label_d = 'ខាត';
							}else{
								$label_d = $value->pav_value;
							}

							if($value->st_type == 21 && $value->st_price_r > 0 && $value->st_price_d > 0 ){
								$label_r = $value->pav_value;
								$label_d = '';
							}
							if($value->st_type == 21 && $value->st_price_r < 0 && $value->st_price_d < 0 ){
								$label_r = 'ខាត';
								$label_d = '';
							}

			?>	
							// console.log(totalDolla);
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							if(subPrice_r != ''){
								totalReal = totalReal + parseFloat(subPrice_r);
							}

							if(subPrice_d != ''){
								totalDolla = totalDolla + parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							var displayPriceR = parseFloat('{{$value->st_price_r}}');
							var displayPriceD = parseFloat('{{$value->st_price_d}}');
							tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">{{ $label_r }}</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(displayPriceR)+'</th><th></th><th></th><th>{{ $label_d }}</th><th class="result_right_total" style="text-align:left !important;" >'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}else{
			?>	
							var subPrice_r = '{{ $value->st_price_r }}';
							var subPrice_d = '{{ $value->st_price_d }}';
							if(subPrice_r > 0){
								totalReal = totalReal - parseFloat(subPrice_r);
							}
							if(subPrice_d > 0){
								totalDolla = totalDolla - parseFloat(subPrice_d);
							}
							// console.log(parseFloat(totalDolla));
							
							var displayPriceR = parseFloat('{{$value->st_price_r * -1}}');
							var displayPriceD = parseFloat('{{$value->st_price_d * -1}}');
							tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">{{ $value->pav_value }}</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(displayPriceR)+'</th><th></th><th></th><th></th><th class="result_right_total" style="text-align:left !important;">'+displayPriceD.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
							$('.display_total_result').append(tr);
							displayMainTotal = displayMainTotal + 1;
			<?php 
						}
					}
				}
			?>
			// end paid or borrow
			if(displayMainTotal > 0){
				// total price
				tr = '<tr class=""><td colspan="11"></td></tr>';
				$('.display_total_result').append(tr);

				if(totalReal > 0){
					var labelTotal = 'សុី'; 
				}else{
					var labelTotal = 'ខាត';
				}

				if(totalDolla > 0){
					var labelTotal_d = 'សុី'; 
				}else{
					var labelTotal_d = 'ខាត';
				}

				if(totalReal >= 0 && totalDolla >= 0){
					var labelTotal = 'សុី'; 
					var labelTotal_d = ''; 
				}

				if(totalReal < 0 && totalDolla < 0){
					var labelTotal = 'ខាត'; 
					var labelTotal_d = ''; 
				}

				tr = '<tr class="bee_highlight"><th class="{{$hidedata}}"></th><th></th><th></th><th colspan="2" style="text-align:left !important;">'+labelTotal+'</th><th class="result_right_total" style="text-align:left !important;">'+addCommas(totalReal)+'</th><th></th><th></th><th>'+labelTotal_d+'</th><th class="result_right_total" style="text-align:left !important;">$ '+totalDolla.toFixed(2)+'</th><th class="{{$hidedata}}"></th></tr>';
								$('.display_total_result').append(tr);
			}

			// $(".report_total_info").each(function(index, element) {

			// 	staff_list = $(this).attr('staff');
			// 	id_staff = $(this).attr('id_staff');

			// 	time = $(this).attr('time');
			// 	page = $(this).attr('page');
			// 	$('.display_total_result').find('#td_staff_com_'+index).text(staff_list);
			// 	$('.display_total_result').find('#td_time_com_'+index).text(time);
			// 	$('.display_total_result').find('#td_page_com_'+index).text(page);

			// });

			var date = '{{$var_dateStart}}';
			var staff = '{{$var_staff}}';
			<?php 
			if($checkinView == 1 && $var_type_lottery == 1){
			?>
			$.ajax({
				url: '../addmoney',
				type: 'GET',
				data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla},
				success: function(data) {
					console.log(data);
					if(data.status=="success"){
//							console.log(data.msg);
					}else{
//							console.log(data.msg);
					}
				}
			});
			
			<?php 
			}
			?>

			var var_type_lottery = '{{$var_type_lottery}}';

			$.ajax({
				url: 'summary_lottery_total',
				type: 'GET',
				data: {date:date,staff:staff,totalReal:totalRealBefore,totalDolla:totalDollaBefore,var_type_lottery:var_type_lottery},
				success: function(data) {
					if(data.status=="success"){
//							console.log(data.msg);
					}else{
//							console.log(data.msg);
					}
				}
			});

@endif



		});

@endif
	$(document).ready(function() {
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
		});
	});
	$(document).ready(function(){

		$('.btn_print').click(function(){
			var attr_id = $(this).attr('attr_id');
			var attr_date = $(this).attr('attr_date');
			var attr_staff = $(this).attr('attr_staff');
			var attr_sheet = $(this).attr('attr_sheet');
			if(attr_sheet==''){
				attr_sheet = 0;
			}
			var attr_page = $(this).attr('attr_page');
			if(attr_page==''){
				attr_page = 0;
			}
			var form_upload = $('#frmupload');
			var fram_upload = $('<iframe style="border:0px; width:0px; height:0px; opacity: 0px;" src="http://188.166.234.165/lottery/screenshot/download.php?attr_id='+attr_id+'&attr_date='+attr_date+'&attr_staff='+attr_staff+'&attr_sheet='+attr_sheet+'&attr_page='+attr_page+'" id="framupload" name="framupload"></iframe>');

			$('body').append(fram_upload);

		});
	});
	</script>
@stop