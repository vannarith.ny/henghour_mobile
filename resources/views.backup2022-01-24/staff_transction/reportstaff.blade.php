@extends('master')

@section('title')
<title>របាយការណ៏ លក់បុគ្គលិក</title>
@stop

@section('cssStyle')
	<style type="text/css">
		

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ លក់បុគ្គលិក</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏ លក់បុគ្គលិក</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'reportstafffilter', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif

                         @if(isset($var_dateStart))
							 <?php $endDate_filter = $var_dateEnd; ?>
						 @else
							 <?php $endDate_filter = null; ?>
						 @endif



						 @if(isset($var_staff))
							 <?php $staff_filter = $var_staff; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 
						 <div class="row">

							 <section class="col col-3">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

                             <section class="col col-3">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateEnd", $value = $endDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateEnd','placeholder'=> trans('result.dateEnd'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

							 <section class="col col-4">
								 <label class="select">
									 {{ Form::select('staff', ([
                                        '' => trans('result.chooseStaff') ]+$staffs),$staff_filter,['class' => 'required ','id'=>'staff','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" id="submitData" name="submit"  class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					
					 <div class="widget-body no-padding">
						<div class="col-12" style="width:90%; margin:0px auto;" id="main_table">
							@if(isset($transctions))
                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
		           
                                <thead>           
                                        <tr>
                                            <th >{{trans('label.id')}}</th>
                                            <th >{{trans('label.staff_name')}}</th>
                                            <th >{{trans('label.date')}}</th>
                                            <th>ទឹកប្រាក់រាល់ថ្ងៃរៀល (៛)</th>
                                            <th >ទឹកប្រាក់រាល់ថ្ងៃដុល្លា ($)</th>
                                            <th >ប្រតិបត្តិការ</th>
                                            <th >ទឹកប្រាក់ចុងគ្រារៀល (៛)</th>
                                            <th >ទឹកប្រាក់ចុងគ្រាដុល្លា ($)</th>
                                            
                                            <!-- <th>{{trans('label.action')}}</th> -->
                                        </tr>
                                </thead>
                                <tbody>
                                    <?php $i=0;?>
                                    @foreach($transctions as $transction)
                                        <?php $i++;?>
                                        <tr class="stafftransction-{{$transction->id}}">
                                            <td>{{$i}}</td>
                                            <td>{{$transction->s_name}}</td>
                                            <td>{{$transction->date}}</td>
                                            <td>
                                                {{  number_format($transction->price_r) }}
                                            </td>
                                            <td>
                                                {{  number_format($transction->price_d, 2) }}
                                            </td>
                                            <td>
                                                <?php 
                                                    $yesturday = date('Y-m-d',strtotime($transction->date . "-1 days"));
                                                    $checktran = DB::table("tbl_staff_transction")
                                                            ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                                            ->where('tbl_staff_transction.st_date_search',$yesturday)
                                                            ->where('tbl_staff_transction.s_id',$transction->s_id)
                                                            ->get();
                                                ?>
                                                @if($checktran)
                                                    <?php 
                                                        $realPriceR = 0;
                                                        $realPriceD = 0;
                                                    ?>
                                                    @foreach($checktran as $key=>$data)
                                                        <?php 
                                                            $displayD = '';
                                                            if($data->st_price_d > 0){
                                                                $displayD .= ' / '.$data->st_price_d.' $';
                                                            }

                                                            if($data->pav_id == 20 || $data->pav_id == 3){
                                                                $realPriceR = $realPriceR - $data->st_price_r;
                                                                $realPriceD = $realPriceD - $data->st_price_d;
                                                            }else if($data->pav_id == 4 || $data->pav_id == 22){
                                                                $realPriceR = $realPriceR + $data->st_price_r;
                                                                $realPriceD = $realPriceD + $data->st_price_d;
                                                            }

                                                        ?>
                                                        <div>{{$data->pav_value}} : {{$data->st_price_r.$displayD}}</div>
                                                    @endforeach
                                                    <?php 
                                                        $displayDTotal = '';
                                                        if($realPriceD > 0 || $realPriceD < 0){
                                                            $displayDTotal .= ' / '.$realPriceD.' $';
                                                        }
                                                    ?>
                                                    <div><b>Total : {{$realPriceR.$displayDTotal}}</b></div>
                                                @endif
                                            </td>
                                            <td>
                                                {{  number_format($transction->c_price_r) }}
                                            </td>
                                            <td>
                                                {{  number_format($transction->c_price_d, 2) }}
                                            </td>

                                            
                                                    
                                            
                                            </tr>
                                    @endforeach            
                                    
                                </tbody>
                                </table>
                            @endif
						</div>
					 </div>

						 


		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		$(document).ready(function() {
			// START AND FINISH DATE
			$('#dateStart').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#dateEnd').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#dateEnd').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#dateStart').datepicker('option', 'mixDate', selectedDate);
				}
			});


			// function GetAmountList() {
			// 	var dateData = $("#dateStart").val();
			// 	var userID = $("#staff").val();
			// 	var sheetID = $("#sheet").val();
			// 	// console.log(userID);
			// 	// return false;
			// 	$.ajax({
			// 		type: "GET",
			// 		url: '{{URL::to('/')}}/reportphone/dailyReportGet',
			// 		data: {
			// 			userID: userID,
			// 			dateData: dateData,
			// 			sheetID: sheetID
			// 		},
			// 		success: function (n) {
			// 			console.log(n);
			// 			$("#main_table").html(n);
			// 		},
			// 		error: function () {}
			// 	})
			// 	return false;
			// }

			// $("#submitData").click(function () {
			// 	GetAmountList();
			// 	return false;
			// });


		});
	
	</script>
@stop