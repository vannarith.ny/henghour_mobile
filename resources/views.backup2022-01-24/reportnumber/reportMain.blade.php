@extends('master')
<?php
use App\Http\Controllers\SaleController;
?>
@section('title')
<title>របាយការណ៏ Main</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:25px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:18px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏ Main</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏ Main</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'reportFilterMain', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($staff_filter))
							 <?php $staff_filter = $staff_filter; ?>
						 @else
							 <?php $staff_filter = null; ?>
						 @endif

						 
						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

							
							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('staff', ([
                                        '' => trans('result.chooseStaff') ]+$staff),$staff_filter,['class' => 'required ','id'=>'staff','sms'=> trans('result.pleaseChooseStaff') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 

							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 @if(isset($childs))

						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 
							 .paperStyle{
								 display: none;
							 }
							 .paperStyle:last-child{
								 display: block !important;
							 }
							 .staff_name{
								 display: none;
							 }
							 
							 
						 </style>

                        <?php 
                            $totalReal = 0;
                            $totalDolla = 0;
                        ?>

						 <div class="widget-body no-padding control_width">
							 	
                            <div id="html-content-holder" class="widget-body paperStyle">
                                
                                    <h1>{{trans('label.summary_result_per_day')}}</h1><br>
                                    <div class="col-md-3">
                                        {{trans('label.staff_name')}} :
                                        <b>{{$staffInfo->s_name}} </b>
                                    </div>

                                    <div class="col-md-2">
                                            <b>ឆ្នោត : វៀតណាម</b>
                                    </div>

                                    <div class="col-md-2">
                                        {{trans('label.date')}} :
                                        <b><?php echo date("d-m-Y", strtotime($var_dateStart));?></b>
                                    </div>

                                    <br><br>
                                    <table id="" class="table table-bordered summary_result_per_day" style="width:75%;">
                                        <thead>
                                        
                                        <tr>
                                            <th style="text-align:left !important;">{{trans('label.staff')}}</th>
                                            <th>លុយខ្មែរ (៛)</th>
                                            <th>លុយដុល្លា ($)</th>
                                        </tr>
                                        </thead>
                                        <tbody class="display_total_result">
                                        <?php
                                            $total2Digit = 0;
                                            $total3Digit = 0;
                                        ?>
                                        @foreach($childs as $key => $child)
                                            <?php 
                                                $money = DB::table("tbl_staff_transction")
                                                         ->where('s_id', $child->s_id)
                                                         ->where('st_type', 21)
                                                         ->where('st_date_diposit', $startDate_filter)
                                                         ->first();
                                                
                                            ?>
                                            @if($money)
                                                <?php 
                                                    $total2Digit = $total2Digit + $money->st_price_r;
                                                    // $total3Digit = $total3Digit + $money->st_price_d;
                                                    $totalDollasub = round($money->st_price_d, 2);
                                                    $total3Digit = $total3Digit + floatval($totalDollasub);
                                                    
                                                ?>
                                            <tr>
                                                <td style="text-align:left !important;">{{ $child->s_name }}</td>
                                                <td>{{$money->st_price_r}}</td>
                                                <td>{{$totalDollasub}}</td>
                                            </tr>
                                            @endif
                                        @endforeach

                                            <?php 
                                                if($total2Digit < 0){
                                                    $priceR = 'ខាត ( '.number_format($total2Digit).' )';
                                                }else{
                                                    $priceR = 'សុី '.number_format($total2Digit);
                                                }

                                                if($total3Digit < 0){
                                                    $priceD = 'ខាត ( '.$total3Digit.' )';
                                                }else{
                                                    $priceD = 'សុី '.$total3Digit;
                                                }

                                                $totalReal = $totalReal + $total2Digit;
                                                $totalDolla = $totalDolla + $total3Digit;
                                            ?>

                                            <tr>
                                                <td style="text-align:right !important;">សរុប</td>
                                                <td>{{$priceR}}</td>
                                                <td>{{$priceD}}</td>
                                            </tr>


                        <?php 

                            // block old money
                            
				            if (!empty($money_paid)) {
                                $tr = '';
					            foreach($money_paid as $key => $value) {
						            if($value->st_type == 4 || $value->st_type == 21 || $value->st_type == 22 || $value->st_type == 23 || $value->st_type == 25){
							            if($value->st_type == 21 && $value->st_price_r < 0 ){
								            $label_r = 'ខាត';
                                        }else{
                                            $label_r = $value->pav_value;
                                        }

                                        if($value->st_type == 21 && $value->st_price_d < 0 ){
                                            $label_d = 'ខាត';
                                        }else{
                                            $label_d = $value->pav_value;
                                        }

                                        if($value->st_type == 21 && $value->st_price_r > 0 && $value->st_price_d > 0 ){
                                            $label_r = $value->pav_value;
                                            $label_d = '';
                                        }
                                        if($value->st_type == 21 && $value->st_price_r < 0 && $value->st_price_d < 0 ){
                                            $label_r = 'ខាត';
                                            $label_d = '';
                                        }
                                        $subPrice_r = $value->st_price_r;
                                        $subPrice_d = $value->st_price_d;
                                        if($subPrice_r != ''){
                                            $totalReal = $totalReal + $subPrice_r;
                                        }
                                        if($subPrice_d != ''){
                                            $totalDolla = $totalDolla + $subPrice_d;
                                        }
                                        $displayPriceR = $value->st_price_r;
                                        $displayPriceD = $value->st_price_d;
                                        $tr .= '<tr>';
                                            $tr .='<td style="text-align:right !important;">'.$label_r.'</td>';
                                            $tr .='<td>'.number_format($displayPriceR).'</td>';
                                            $tr .='<td>'.number_format($displayPriceD,2).'</td>';
                                        $tr .='</tr>';
			           
						            }else{
                                        $subPrice_r = $value->st_price_r;
                                        $subPrice_d = $value->st_price_d;
                                        if($subPrice_r > 0){
                                            $totalReal = $totalReal - $subPrice_r;
                                        }
                                        if($subPrice_d > 0){
                                            $totalDolla = $totalDolla - $subPrice_d;
                                        }
                                        $displayPriceR = $value->st_price_r * -1;
                                        $displayPriceD = $value->st_price_d * -1;
                                        $tr .= '<tr>';
                                            $tr .='<td style="text-align:right !important;">'.$value->pav_value.'</td>';
                                            $tr .='<td>'.number_format($displayPriceR).'</td>';
                                            $tr .='<td>'.number_format($displayPriceD,2).'</td>';
                                        $tr .='</tr>';
			              
						            }
                                }

                                $tr .= '<tr>';
                                    $tr .='<td style="text-align:right !important;">សរុបទាំងអស់</td>';
                                    $tr .='<td>'.number_format($totalReal).'</td>';
                                    $tr .='<td>'.number_format($totalDolla,2).'</td>';
                                $tr .= '</tr>';

                                echo $tr;
				            }
			            ?>    


                                        </tbody>
                                    </table>
                            </div>

						 </div>



					 @endif



		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">
        $(document).ready(function() {
                $('#dateStart').datepicker({
                                        dateFormat : 'yy-mm-dd',
                                        prevText : '<i class="fa fa-chevron-left"></i>',
                                        nextText : '<i class="fa fa-chevron-right"></i>',
                                        onSelect : function(selectedDate) {
                                            $('#dateEnd').datepicker('option', 'minDate', selectedDate);
                                        }
                                    });
                $(document).off('submit', '#filter').on('submit', '#filter', function(e){
                    if (validate_form_main('#filter') == 0) {
                        return true;
                    }
                    return false;
                });

                @if(isset($childs))


                        @if($staffInfo->payment == 1)

                                var date = '{{$startDate_filter}}';
                                var staff = '{{$staffInfo->s_id}}';
                                var totalReal = '{{$totalReal}}';
                                var totalDolla = '{{$totalDolla}}';
                                $.ajax({
                                    url: "{{URL::to('/')}}/addmoney",
                                    type: 'GET',
                                    data: {date:date,staff:staff,totalReal:totalReal,totalDolla:totalDolla},
                                    success: function(data) {
                                        console.log(data);
                                        
                                    }
                                });

                            
                        @endif

                @endif
        });
	</script>
@stop