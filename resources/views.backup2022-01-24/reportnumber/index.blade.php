@extends('master')
<?php
use App\Http\Controllers\SaleController;

// $lottery_pay = DB::('tbl_staff_charge')->where('')
?>
@section('title')
<title>របាយការណ៏</title>
@stop

@section('cssStyle')
	<style type="text/css">
		.colume_style{
			padding-bottom:50px;
			padding-top:15px;
		}
		.colume_style:last-child{
			border-right: 1px solid #CCCCCC;
		}
		.colume_style:nth-child(8){
			border-right: 0px solid #CCCCCC;
		}
		.result_price{
			position: absolute;
			bottom: 25px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_main{
			position: absolute;
			bottom: 0px;
			width: 100%;
			border-top:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
			background: #0000ff;
			color: #FFF;
		}
		.result_price span{
			color:red;
		}
		.result_price_right span{
			 color:red;
		 }

		.result_price_top{
			position: absolute;
			top: 0px;
			width: 100%;
			border-bottom:1px solid #CCCCCC;
			color:blue;
			padding-left:10px;
			padding-right:10px;
			font-weight:bold;
			font-size: 18px;
		}
		.result_price_top span{
			color:red;
		}

		.display_result{
			border-top:1px solid #CCCCCC;
			font-size:22px;
			padding:10px 10px;
		}

		.val_r{
			color: blue;
		}
		.val_s{
			color: red;
		}
		.right_num{
			position: absolute;
			right:5px;
			top:2px;
			color: yellow;
		}
		.result_right{
			color:red;
			font-weight: bold;
		}
		.bee_highlight th{
			background-color:lightyellow;
			font-size: 18px !important;
		}
		.display_total_result td{
			text-align: right;
		}
		.display_total_result tr td:nth-child(1),
		.display_total_result tr td:nth-child(2){
			text-align: left;
		}
		.result_right_total{
			color:blue;
			font-weight: bold;
		}
		.btn_print{
			margin-left: 30px;
		}
		table.summary_result_per_day th,
		table.summary_result_per_day td{
			text-align: center !important;
		}
		table.summary_result_per_day th{
			width: 10%;
			font-size: 20px;
		}
		table.summary_result_per_day td{
			font-size: 18px;
			font-weight: bold;
		}

	</style>
@stop


@section('content')
<!-- RIBBON -->
	<div id="ribbon">

		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><!-- Staffs -->របាយការណ៏</li>
		</ol>
		<!-- end breadcrumb -->


	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <!--  <h1 class="page-title txt-color-blueDark">
		       <i class="fa fa-users fa-fw "></i> របាយការណ៏
		       
		      </h1> -->
		     </div>
		    </div>
		    
		    <!-- widget grid -->
		    <section id="widget-grid" class="">
		    
		     

		     @include('flash::message')

		     <?php if($errors->all()){?>
		     <div class="alert alert-block alert-danger">
		      <a class="close" data-dismiss="alert" href="#">×</a>
		      <h4 class="alert-heading"><i class="fa fa-times"></i> Check not validation!</h4>
		      <p>
		       {{ Html::ul($errors->all(), array('class' => 'alert alert-danger')) }}
		      </p>
		     </div>
		     <?php }?>




		     <!-- row -->
		     <div class="row">
		    
		      <!-- NEW WIDGET START -->
		      <article class="col-sm-12 col-md-12 col-lg-12">

		       <!-- Widget ID (each widget will need unique ID)-->
		       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
		        
		        <header>
		         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
		         <!-- <h2>របាយការណ៏</h2> -->
		         <h2>របាយការណ៏</h2>
		        </header>
		    
		        <!-- widget div-->
		        <div>
		    
		         <!-- widget content -->
		         <div class="widget-body no-padding">


					 {!! Form::open(['route' => 'reportFilterNumber', 'files' => true , 'novalidate' => 'validate', 'id' => 'filter','class'=>'smart-form filter-form']) !!}
					 <fieldset>
						 <?php $attr_sheet =0; ?>
						 <?php $attr_page = 0; ?>
						 @if(isset($var_dateStart))
							 <?php $startDate_filter = $var_dateStart; ?>
						 @else
							 <?php $startDate_filter = null; ?>
						 @endif



						 @if(isset($var_sheet))
							 <?php $sheet_filter = $var_sheet; ?>
						 @else
							 <?php $sheet_filter = null; ?>
						 @endif

						 <?php 
						 // dd($posts);
						 ?>

						 @if(isset($pos_id))
							 <?php $post_filter = $pos_id; ?>
						 @else
							 <?php $post_filter = null; ?>
						 @endif

						 <div class="row">

							 <section class="col col-2">
								 <label class="input">
									 <i class="icon-append fa fa-calendar"></i>
									 {!! Form::text("dateStart", $value = $startDate_filter, $attributes = array('class' => 'form-control required', 'id' => 'dateStart','placeholder'=> trans('result.dateStart'),'sms'=> trans('result.pleaseChooseDate') )) !!}
								 </label>

							 </section>

							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('type_lottery', ([
                                        '' => trans('result.chooseTypeLottery') ]+$type_lottery),$var_type_lottery,['class' => 'required ','id'=>'type_lottery','sms'=> trans('result.pleaseChooseTypeLottery') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>
							 <section class="col col-2">
								 <label class="select">
									 {{ Form::select('sheet', ([
                                        '' => trans('result.chooseShift') ]+$sheets),$sheet_filter,['class' => 'required ','id'=>'sheet','sms'=> trans('result.pleaseChooseShift') ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>
							 <section class="col col-2">
								 <label class="select">

                                     {{ Form::select('pos_id', ([
                                        '' => 'ប៉ុស' ]+$posts),$post_filter,['class' => 'required ','id'=>'pos_id','sms'=> 'ប៉ុស' ]
                                     ) }}
									 <i></i>
								 </label>
							 </section>

							 

							 <section class="col col-2">
								 <label class="tesxt">
									 <button type="submit" name="submit" class="btn btn-primary btn-sm btn-filter">{{ trans('result.filter') }}</button>
								 </label>
							 </section>

						 </div>
					 </fieldset>
					 {{ Form::close() }}

					 @if(isset($allData))
						
						 <style type="text/css">
							 .smart-form select.formlottery_select{
								 box-sizing: border-box !important;
							 }
							 .posInfo{
							 	position: absolute;
							 	left: 15px;
							 	top: 5px;
							 	font-size: 20px;
							 	color: #000;
							 	font-weight: bold;
							 }
							 
						 </style>



						 <div class="widget-body control_width">

						 	<div class="col-sm-12 col-md-4 col-lg-4">
						 		<div class="posInfo">
						 			2 លេខ
						 		</div>

							 	<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
			           
						           <thead>           
							            <tr>
								             <th >ប៉ុស</th>
								             <th >លេខ</th>
								             <th >លុយរៀល(៛)</th>
								             <th >លុយដុល្លា($)</th>
							            </tr>
						           </thead>
						           <tbody>
						           		<?php 
						           		$i=0;
						           		$findPos = DB::table('tbl_pos')->where('pos_time', $sheet_filter)->where('pos_name','A')->first();
						           		// $posA = $allData->where('p_id',$findPos->pos_id)->where('digit',2);
						           		$posData = $allData->where('digit',2);

						           		?>
							           @foreach($posData as $data)
							           		<?php $i++;?>
								            <tr class="sale-{{$data->sm_id}}">
								             <td>{{$data->pos_name}}</td>
								             <td >num_{{ $data->sm_key}}</td>
								             <td >{{ number_format($data->sm_value_r) }}</td>
								             <td >{{ number_format($data->sm_value_d) }}$</td>
								            </tr>
							           @endforeach
							        </tbody>
			          			</table>
		          			</div>
		          			<div class="col-sm-12 col-md-4 col-lg-4">
						 		<div class="posInfo">
						 			3 លេខ
						 		</div>

							 	<table id="datatable_tabletools_3" class="table table-striped table-bordered table-hover" width="100%">
			           
						           <thead>           
							            <tr>
								             <th >ប៉ុស</th>
								             <th >លេខ</th>
								             <th >លុយរៀល(៛)</th>
								             <th >លុយដុល្លា($)</th>
							            </tr>
						           </thead>
						           <tbody>
						           		<?php 
						           		$i=0;
						           		$findPos = DB::table('tbl_pos')->where('pos_time', $sheet_filter)->where('pos_name','A')->first();
						           		// $posA = $allData->where('p_id',$findPos->pos_id)->where('digit',2);
						           		$posData = $allData->where('digit',3);

						           		?>
							           @foreach($posData as $data)
							           		<?php $i++;?>
								            <tr class="sale-{{$data->sm_id}}">
								             <td>{{$data->pos_name}}</td>
								             <td >num_{{ $data->sm_key}}</td>
								             <td >{{ number_format($data->sm_value_r) }}</td>
								             <td >{{ number_format($data->sm_value_d) }}$</td>
								            </tr>
							           @endforeach
							        </tbody>
			          			</table>
		          			</div>
							 
						 </div>




					 @endif



		    
		         </div>
		         <!-- end widget content -->
		    
		        </div>
		        <!-- end widget div -->
		    
		       </div>
		       <!-- end widget -->
		    
		      </article>
		      <!-- WIDGET END -->
		     </div>
		     <!-- end row -->
		    
		    </section>
		    <!-- end widget grid -->

	</div>
<!-- END MAIN CONTENT -->
@endsection


@section('javascript')
	<script type="text/javascript">

		


	$(document).ready(function() {
		@if(isset($posts))
			$("#pos_id").prop( "disabled", false );
		@else
			$("#pos_id").prop( "disabled", true );
		@endif
		// START AND FINISH DATE
		$('#dateStart').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateEnd').datepicker('option', 'minDate', selectedDate);
			}
		});
		$('#dateEnd').datepicker({
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#dateStart').datepicker('option', 'mixDate', selectedDate);
			}
		});


		$(document).off('change', '#sheet').on('change', '#sheet', function(e){
			
			var id = $(this).val();
			$("#pos_id").prop( "disabled", true );
			// $("#sheet").prop( "disabled", true );
            if(id != ''){
            	$.ajax({
				   url: '/reportnumber/getpostBySheet',
			       type: 'GET',
			       data: {sheet:id},
			       success: function(data) {
			           if(data.status=="success"){
			           		$("#pos_id").prop( "disabled", false );
			           		$("#pos_id").html(data.msg);

			      			console.log(data.msg);

			           }else{
			           }
			        }
			     });
            }else{
            	$("#pos_id").prop( "disabled", true );
            	$("#pos_id").html('<option value="" selected="selected">រើសប៉ុស</option>');
            }
			

		});




		var responsiveHelper_datatable_tabletools = undefined;
    
	    var breakpointDefinition = {
	     tablet : 1024,
	     phone : 480
	    };
		$('#datatable_tabletools').dataTable({

		    "lengthChange": false,
		    "pageLength": 20,
		    "info": true,
		    "dom": 'ftipr',
		    "autoWidth" : true,
		    "preDrawCallback" : function() {
		     // Initialize the responsive datatables helper once.
		     if (!responsiveHelper_datatable_tabletools) {
		      responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
		     }
		    },
		    "rowCallback" : function(nRow) {
		     responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		    },
		    "drawCallback" : function(oSettings) {
		     responsiveHelper_datatable_tabletools.respond();
		    }
		});

		$('#datatable_tabletools_3').dataTable({

		    "lengthChange": false,
		    "pageLength": 20,
		    "info": true,
		    "dom": 'ftipr',
		    "autoWidth" : true,
		    "preDrawCallback" : function() {
		     // Initialize the responsive datatables helper once.
		     if (!responsiveHelper_datatable_tabletools) {
		      responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
		     }
		    },
		    "rowCallback" : function(nRow) {
		     responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		    },
		    "drawCallback" : function(oSettings) {
		     responsiveHelper_datatable_tabletools.respond();
		    }
		});
	});
	$(document).ready(function(){

		$('.btn_print').click(function(){
			

		});
	});
	</script>
@stop