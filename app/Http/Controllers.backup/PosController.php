<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class PosController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }elseif (Session::get('roleLot') != 1) {
              Redirect::to('/sale')->send();
           }
            return $next($request);
        });
   
    }

    public function index(Request $request)
    {

        $stc_type = $request->stc_type;
        $pat_key = '';
        if($stc_type=='1'){
            $pat_key = "sheet";
            $page = 'pos';
            $title = 'VN';
        }else if($stc_type=='2'){
            $pat_key = "sheet_khmer";
            $page = 'pos_kh';
            $title = 'KH';
        }else if($stc_type=='3'){
            $pat_key = "sheet_th";
            $page = 'pos_th';
            $title = 'TH';
        }else if($stc_type=='4'){
            $pat_key = "sheet_lo";
            $page = 'pos_lo';
            $title = 'វត្តុ';
        }else{
            $pat_key = "sheet";
            $page = 'pos';
            $title = 'VN';
            $stc_type==1;
        }
     	$poss = DB::table('tbl_pos')
                    ->join('tbl_parameter_value','tbl_pos.pos_time','=','tbl_parameter_value.pav_id')
                    ->join('tbl_parameter_type','tbl_parameter_value.pat_id','=','tbl_parameter_type.pat_id')
                    ->where('tbl_parameter_type.pat_key',$pat_key)
                    ->orderBy('tbl_pos.pos_time','ASC')
                    ->orderBy('tbl_pos.pos_name','ASC')
                    ->get();
     	$times = $this->getTime($stc_type);
     	return view('pos.index', compact('poss','page','times','stc_type','title'));
    }

    public function create(Request $request){
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_pos';
            $title = 'VN';
        }else if($stc_type=='2'){
            $page = 'add_pos_kh';
            $title = 'KH';
        }else if($stc_type=='3'){
            $page = 'add_pos_th';
            $title = 'TH';
        }else if($stc_type=='4'){
            $page = 'add_pos_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'add_pos';
            $stc_type = 1;
            $title = 'VN';
        }

    	$times = $this->getTime($stc_type);
	    return view('pos.create', compact('page','times','stc_type','title'));
    }
    public function store(Request $request)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_pos';
        }else if($stc_type=='2'){
            $page = 'add_pos_kh';
        }else if($stc_type=='3'){
            $page = 'add_pos_th';
        }else if($stc_type=='4'){
            $page = 'add_pos_lo';
        }else{
            $page = 'add_pos';
            $stc_type = 1;
        }

     	$pos_time = $request->pos_time;
	    $pos_name = $request->pos_name;
	    $pos_two_digit = $request->pos_two_digit;
	    $pos_three_digit = $request->pos_three_digit;
	    $pos_info = $request->pos_info;

	    $id_s = 1;
        $draft =  DB::table('tbl_pos')->orderBy('pos_id', 'DESC')->take(1)->get();
        if ($draft){
            foreach ($draft as $id) {
                    $id_s = $id->pos_id + 1;
            }
        }

	    $rule = [
	    	'pos_time' => 'required|unique:tbl_pos,pos_time,null,pos_id,pos_name,'.$pos_name,
	    	'pos_name' => 'required',
	    	'pos_two_digit' => 'required|integer',
	    	'pos_three_digit' => 'required|integer',
	    ];

	    $messages = [
	    	'pos_time.required' => trans('validation.custom.pos.time.required'),
	    	'pos_time.unique' => 'បុស្តិ៍ឈ្មោះ '.$pos_name.' មានរួចហើយ',
	    	'pos_name.required' => trans('validation.custom.pos.name.required'),
	    	'pos_two_digit.required' => trans('validation.custom.pos.number_2_digit.required'),
	    	'pos_two_digit.integer' => trans('validation.custom.pos.number_2_digit.integer'),
	    	'pos_three_digit.required' => trans('validation.custom.pos.number_3_digit.required'),
	    	'pos_three_digit.integer' => trans('validation.custom.pos.number_3_digit.integer')
	    ];

	    $validator = Validator::make($request->all(), $rule, $messages);

	    if ($validator->fails()) {
	        return redirect('pos/create?stc_type='.$stc_type)->withInput($request->all())->withErrors($validator);
	    }else{
	    	

            $check = DB::table('tbl_pos')->insert([
            			'pos_id' => $id_s,
                        'pos_time' => $pos_time,
                        'pos_name' => $pos_name,
                        'pos_two_digit' => $pos_two_digit,
                        'pos_three_digit' => $pos_three_digit,
                        'pos_info' => $pos_info
                       	]); 
            if($check){
                flash()->success(trans('message.add_success_pos'));
                return redirect('/pos?stc_type='.$stc_type);
            }else{
             flash()->error(trans('message.add_error_pos'));
               return redirect('/pos/create?stc_type='.$stc_type)->withInput();
            }

	    }
	      		
    }

    public function edit(Request $request, $id)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_pos';
            $title = 'VN';
        }else if($stc_type=='2'){
            $page = 'add_pos_kh';
            $title = 'KH';
        }else if($stc_type=='3'){
            $page = 'add_pos_th';
            $title = 'TH';
        }else if($stc_type=='4'){
            $page = 'add_pos_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'add_pos';
            $stc_type = 1;
            $title = 'VN';
        }
	    $pos = DB::table('tbl_pos')->where('pos_id',$id)->first();
	    $times = $this->getTime($stc_type);
	    return view('pos.edit', compact('page','pos','times','stc_type','title'));
    }

    public function update(Request $request, $id){
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_pos';
        }else if($stc_type=='2'){
            $page = 'add_pos_kh';
        }else if($stc_type=='3'){
            $page = 'add_pos_th';
        }else if($stc_type=='4'){
            $page = 'add_pos_lo';
        }else{
            $page = 'add_pos';
            $stc_type = 1;
        }

    	$pos_id = $id;
	  	$pos_time = $request->pos_time;
	    $pos_name = $request->pos_name;
	    $pos_two_digit = $request->pos_two_digit;
	    $pos_three_digit = $request->pos_three_digit;
	    $pos_info = $request->pos_info;

	    $rule = [
	    	'pos_time' => 'required|unique:tbl_pos,pos_time,'.$pos_id.',pos_id,pos_name,'.$pos_name,
	    	'pos_name' => 'required',
	    	'pos_two_digit' => 'required|integer',
	    	'pos_three_digit' => 'required|integer',
	    ];

	    $messages = [
	    	'pos_time.required' => trans('validation.custom.pos.time.required'),
	    	'pos_time.unique' => 'បុស្តិ៍ឈ្មោះ '.$pos_name.' មានរួចហើយ',
	    	'pos_name.required' => trans('validation.custom.pos.name.required'),
	    	'pos_two_digit.required' => trans('validation.custom.pos.number_2_digit.required'),
	    	'pos_two_digit.integer' => trans('validation.custom.pos.number_2_digit.integer'),
	    	'pos_three_digit.required' => trans('validation.custom.pos.number_3_digit.required'),
	    	'pos_three_digit.integer' => trans('validation.custom.pos.number_3_digit.integer')
	    ];

	    $validator = Validator::make($request->all(), $rule, $messages);

	    if ($validator->fails()) {
	        return redirect('pos/'.$id.'/edit?stc_type='.$stc_type)->withInput($request->all())->withErrors($validator);
	    }else{
            $check = DB::table('tbl_pos')->where('pos_id', $id)->update([
                        'pos_time' => $pos_time,
                        'pos_name' => $pos_name,
                        'pos_two_digit' => $pos_two_digit,
                        'pos_three_digit' => $pos_three_digit,
                        'pos_info' => $pos_info
                       	]); 
            if($check){
                flash()->success(trans('message.update_success'));
                return redirect('pos/'.$id.'/edit?stc_type='.$stc_type);
            }else{
             flash()->error(trans('message.update_error'));
               return redirect('pos/'.$id.'/edit?stc_type='.$stc_type)->withInput();
            }
	    }
    }


    public function deleteItem(Request $request){
      	$id = $request->id;
      	$checkStcItem = DB::table('tbl_pos')
		      		->where('pos_id','=',$id)
		      		->first();

      	if($checkStcItem){

      		$check = DB::table('tbl_pos_group')
      					->where('pos_id','=',$id)->first();
	      	if($check){
	      		return response(['msg' => trans('message.not_permission_delete'), 'status' => 'error']); 
	      	}else{
	      		$check = DB::table('tbl_pos')->where('pos_id', $id)->delete();
		        if($check){
		          return response(['msg' => $id, 'status' => 'success']);  
		        }else{
		          return response(['msg' => trans('message.fail_delete'), 'status' => 'error']); 
		        } 
	      	}

      	}else{
      		return response(['msg' => trans('message.fail_delete'), 'status' => 'error']); 
      	}
    }



    private function getTime($type){
        if($type=='1'){
            $pat_key = "sheet";
        }else if($type=='2'){
            $pat_key = "sheet_khmer";
        }else if($type=='3'){
            $pat_key = "sheet_th";
        }else if($type=='4'){
            $pat_key = "sheet_lo";
        }else{
            $pat_key = "sheet";
        }
    	$result = DB::table('tbl_parameter_type')
    					->where('tbl_parameter_type.pat_key','=',$pat_key)
    					->join('tbl_parameter_value','tbl_parameter_type.pat_id','=','tbl_parameter_value.pat_id')
    					->pluck('pav_value','pav_id')->all();
    	return $result;
    }

}
