<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

use App\Model\Group;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
              if(!Session::has('iduserlot'))
               {
                    Redirect::to('/login')->send();
               }elseif (Session::get('roleLot') != 1) {
                    Redirect::to('/sale')->send();
               }
            return $next($request);
        });
    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'group';
            $stc_type = 1;
            $title = 'VN';
        }

        $groups = Group::where('stc_type','=',$stc_type)->get();
        return view('group.index', compact('page','groups','title','stc_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'add_group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'add_group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'add_group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'add_group';
            $stc_type = 1;
            $title = 'VN';
        }
        return view('group.create', compact('page','stc_type','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'add_group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'add_group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'add_group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'add_group';
            $stc_type = 1;
            $title = 'VN';
        }

        $g_name = $request->g_name;
        $g_info = $request->g_info;
//        $rule = [
//            'g_name' => 'required|unique:tbl_group,g_name',
//            'g_info' => 'required'
//        ];

        $group_check = Group::where('g_name','=',$g_name)
                        ->where('stc_type','=',$stc_type)
                        ->get()
                        ->first();
        if($group_check){
            flash()->error('ក្រុមឈ្មោះ '.$g_name.' មានរួចហើយ');
            return redirect('group/create?stc_type='.$stc_type)->withInput($request->all());
        }

        $rule = [
            'g_name' => 'required',
            'g_info' => 'required'
        ];

        $messages = [
            'g_name.required' => trans('validation.custom.group.name.required'),
            'g_name.unique' => 'ក្រុមឈ្មោះ '.$g_name.' មានរួចហើយ',
            'g_info.required' => trans('validation.custom.group.info.required'),
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            return redirect('group/create?stc_type='.$stc_type)->withInput($request->all())->withErrors($validator);
        }else{
            $group = new Group();
            $group->g_name = $g_name;
            $group->g_info = $g_info;
            $group->stc_type = $stc_type;
            $group->save();
            if($group){
                flash()->success(trans('message.add_success'));
                return redirect('/group?stc_type='.$stc_type);
            }else{
                flash()->error(trans('message.add_error'));
               return redirect('/group/create?stc_type='.$stc_type)->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $page = 'group';
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'group';
            $stc_type = 1;
            $title = 'VN';
        }
        $group = Group::where('g_id','=',$id)->get()->first();
        return view('group.edit', compact('page','group','title','stc_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'add_group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'add_group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'add_group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'add_group';
            $stc_type = 1;
            $title = 'VN';
        }

        $g_name = $request->g_name;
        $g_info = $request->g_info;


        $group_check = Group::where('g_name','=',$g_name)
            ->where('stc_type','=',$stc_type)
            ->where('g_id','<>',$id)
            ->get()
            ->first();

        if($group_check){
            flash()->error('ក្រុមឈ្មោះ '.$g_name.' មានរួចហើយ');
            return redirect('group/'.$id.'/edit?stc_type='.$stc_type)->withInput($request->all());
        }

        $rule = [
            'g_name' => 'required',
            'g_info' => 'required'
        ];

        $messages = [
            'g_name.required' => trans('validation.custom.group.name.required'),
            'g_name.unique' => 'ក្រុមឈ្មោះ '.$g_name.' មានរួចហើយ',
            'g_info.required' => trans('validation.custom.group.info.required'),
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            return redirect('group/'.$id.'/edit?stc_type='.$stc_type)->withInput($request->all())->withErrors($validator);
        }else{
            $check = Group::find($id);
            if($check){
                $check->g_name = $g_name;
                $check->g_info = $g_info;
                $checkUpdate = $check->save();
                if($checkUpdate){
                    flash()->success(trans('message.update_success'));
                    return redirect('group/'.$id.'/edit?stc_type='.$stc_type);
                }else{
                    flash()->error(trans('message.update_error'));
                    return redirect('group/'.$id.'/edit?stc_type='.$stc_type)->withInput();
                }
            }else{
                flash()->error(trans('message.update_error'));
                return redirect('group/'.$id.'/edit?stc_type='.$stc_type)->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteItem(Request $request){
        $id = $request->id;
        $checkItem = Group::find($id);

        if($checkItem){

            $check = DB::table('tbl_pos_group')
                        ->where('g_id','=',$id)->first();
            if($check){
                return response(['msg' => trans('message.not_permission_delete'), 'status' => 'error']); 
            }else{
                $checkDelete = $checkItem->delete();

                if($checkDelete){
                  return response(['msg' => $id, 'status' => 'success']);  
                }else{
                  return response(['msg' => trans('message.fail_delete'), 'status' => 'error']); 
                }

            }

        }else{
            return response(['msg' => trans('message.fail_delete'), 'status' => 'error']); 
        }
    }

}
