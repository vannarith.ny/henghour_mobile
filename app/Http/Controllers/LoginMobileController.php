<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class LoginMobileController extends Controller
{
   
	public function index(){
	  	$page = "mobile_login";
	    return view("login_mobile.index", compact('page'));
    }

    

    public function confirmloginVerify(Request $request)
    {
        $id = $request->id_verify;
        if($request->VerifyCode){
            // dd(Session::get('idtempo'));
            if((Session::get('idtempo') == $id) && (Session::get('idphoneNumber') == $request->VerifyCode)){
                $check = $check = DB::table('tbl_staff')
                            ->where('s_phone_login', $request->VerifyCode)
                            ->first();
                if($check){
                    $loginAll = (int)$check->s_logined + 1;
                     $ok = DB::table('tbl_staff')->where('s_id',$check->s_id)->update(
                            [
                                's_logined' => $loginAll
                            ]
                     );
                     if($ok){
                        $id_user = $check->s_id;
                         $username = $check->s_name;
                         $name = $check->s_name;
                         $phone = $check->s_phone_login;
                         $role = $check->s_role;
                         $CanCreateChild = $check->CanCreateChild;

                         $IsLimitMoney2DR = $check->IsLimitMoney2DR;
                         $IsLimitMoney3DR = $check->IsLimitMoney3DR;
                         $IsLimitMoney2DD = $check->IsLimitMoney2DD;
                         $IsLimitMoney3DD = $check->IsLimitMoney3DD;

                         $IsLimitMoney2DR_KH = $check->IsLimitMoney2DR_KH;
                         $IsLimitMoney3DR_KH = $check->IsLimitMoney3DR_KH;
                         $IsLimitMoney2DD_KH = $check->IsLimitMoney2DD_KH;
                         $IsLimitMoney3DD_KH = $check->IsLimitMoney3DD_KH;

                         Session::put('iduserlotMobileSec', $id_user);
                         Session::put('usernameLotMobileSec', $username);
                         Session::put('nameLotMobileSec', $name);
                         Session::put('phoneLotMobileSec', $phone);
                         Session::put('roleLotMobileSec', $role);
                         Session::put('cutwaterSec', $check->s_print_cut);
                         Session::put('seccCanCreateChild', $CanCreateChild);
                         Session::put('islogin', $loginAll);

                         Session::put('moneyLimit2DR', $IsLimitMoney2DR);
                         Session::put('moneyLimit3DR', $IsLimitMoney3DR);
                         Session::put('moneyLimit2DD', $IsLimitMoney2DD);
                         Session::put('moneyLimit3DD', $IsLimitMoney3DD);

                         Session::put('moneyLimit2DR_KH', $IsLimitMoney2DR_KH);
                         Session::put('moneyLimit3DR_KH', $IsLimitMoney3DR_KH);
                         Session::put('moneyLimit2DD_KH', $IsLimitMoney2DD_KH);
                         Session::put('moneyLimit3DD_KH', $IsLimitMoney3DD_KH);
                         // dd();
                         return redirect('/mobile');
                     }else{
                        flash()->error("can not update");
                        return redirect('/mobile/confirmlogin/'.$id)->withInput();
                     }
                     


                }else{
                    flash()->error("គណនីរបស់អ្នកមិនត្រឹមត្រូវទេ​! សូមមេត្តាព្យាយាមម្តងទៀត.");
                    return redirect('/mobile/confirmlogin/'.$id)->withInput();
                }
            }else {
                flash()->error("គណនីរបស់អ្នកមិនត្រឹមត្រូវទេ​! សូមមេត្តាព្យាយាមម្តងទៀត.");
                return redirect('/mobile/confirmlogin/'.$id)->withInput();
            }

        }else{
            flash()->error("គណនីរបស់អ្នកមិនត្រឹមត្រូវទេ​! សូមមេត្តាព្យាយាមម្តងទៀត.");
            return redirect('/mobile/confirmlogin/'.$id)->withInput();
        }
    }
    public function confirmlogin(Request $request){
        $page = "confirm_login";
        $id = $request->id;
        return view("login_mobile.confirm", compact('page','id'));
    }
    	

    public function logout(Request $request){
        if(Session::has('iduserlotMobileSec')){
            DB::table('tbl_staff')->where('s_id',Session::get('iduserlotMobileSec'))->update(
                            [
                                's_logined' => 0
                            ]
                    );
           Session::forget('iduserlotMobileSec');
           Session::forget('usernameLotMobileSec');
           Session::forget('nameLotMobileSec');
           Session::forget('phoneLotMobileSec');
           Session::forget('roleLotMobileSec');
           Session::forget('seccCanCreateChild');
           Session::forget('cutwaterSec');
           Session::forget('islogin');
           Session::forget('moneyLimit2DR');
           Session::forget('moneyLimit3DR');
           Session::forget('moneyLimit2DD');
           Session::forget('moneyLimit3DD');

           Session::forget('moneyLimit2DR_KH');
           Session::forget('moneyLimit3DR_KH');
           Session::forget('moneyLimit2DD_KH');
           Session::forget('moneyLimit3DD_KH');

        }

        return redirect('mobile/');
    }

    public function store(Request $request){
	    $username = $request->UserName;
        $password = $request->Password;
        $minutes = 100800;
        $dateCurrent = date('Y-m-d H:i:s');
        $check = DB::table('tbl_staff')
                            ->where('s_phone_login', $username)
                            ->where('s_password', $password)
                            ->first();
        // dd($check);
        if ($check){ // check user have in table

             if($check->Active == 0){
                if($check->s_logined == 0){

                     $id_user = $check->s_id;
                     $username = $check->s_name;
                     $name = $check->s_name;
                     $phone = $check->s_phone_login;
                     $role = $check->s_role;
                     $CanCreateChild = $check->CanCreateChild;
                     
                     $IsLimitMoney2DR = $check->IsLimitMoney2DR;
                     $IsLimitMoney3DR = $check->IsLimitMoney3DR;
                     $IsLimitMoney2DD = $check->IsLimitMoney2DD;
                     $IsLimitMoney3DD = $check->IsLimitMoney3DD;

                     $IsLimitMoney2DR_KH = $check->IsLimitMoney2DR_KH;
                     $IsLimitMoney3DR_KH = $check->IsLimitMoney3DR_KH;
                     $IsLimitMoney2DD_KH = $check->IsLimitMoney2DD_KH;
                     $IsLimitMoney3DD_KH = $check->IsLimitMoney3DD_KH;

                    DB::table('tbl_staff')->where('s_id',$id_user)->update(
                            [
                                's_logined' => 1
                            ]
                    );

                     

                     Session::put('iduserlotMobileSec', $id_user);
                     Session::put('usernameLotMobileSec', $username);
                     Session::put('nameLotMobileSec', $name);
                     Session::put('phoneLotMobileSec', $phone);
                     Session::put('roleLotMobileSec', $role);
                     Session::put('cutwaterSec', $check->s_print_cut);
                     Session::put('seccCanCreateChild', $CanCreateChild);
                     Session::put('islogin', 1);

                     Session::put('moneyLimit2DR', $IsLimitMoney2DR);
                     Session::put('moneyLimit3DR', $IsLimitMoney3DR);
                     Session::put('moneyLimit2DD', $IsLimitMoney2DD);
                     Session::put('moneyLimit3DD', $IsLimitMoney3DD);

                     Session::put('moneyLimit2DR_KH', $IsLimitMoney2DR_KH);
                     Session::put('moneyLimit3DR_KH', $IsLimitMoney3DR_KH);
                     Session::put('moneyLimit2DD_KH', $IsLimitMoney2DD_KH);
                     Session::put('moneyLimit3DD_KH', $IsLimitMoney3DD_KH);

                     return redirect('/mobile');
                }else{
                    $id_user = encrypt($check->s_id);
                    Session::put('idtempo', $id_user);
                    Session::put('idphoneNumber', $username);
                    return redirect('/mobile/confirmlogin/'.$id_user);
                }
                 
             }else{
                flash()->error("គណនីរបស់អ្នកត្រួវបានផ្អាក​. សូមទាក់ទងទៅមេរបស់អ្នក");
                return redirect('/')->withInput();
             }
                
          	 

             
             
             
             

        }else{
        
          flash()->error("គណនីរបស់អ្នកមិនត្រឹមត្រូវទេ​! សូមមេត្តាព្យាយាមម្តងទៀត.");
             return redirect('/')->withInput();
            
        }
	}

}
