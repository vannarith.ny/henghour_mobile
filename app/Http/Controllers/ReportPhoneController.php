<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

use App\Model\ReportPhone;

class ReportPhoneController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }

    public function getstaffbylotterytype(Request $request){
        $type = $request->type;

        $s_id = $request->staff_id;
        $time_id = $request->sheet_id;

        $staff = DB::table('tbl_staff')
            ->select('s_id','s_name')
            ->where('s_role',1)
            ->orderBy('s_name', 'ASC')
            ->groupBy('s_id')
            ->get();

        $option = '<option value="" selected="selected">រើសបុគ្គលិក</option>';
        foreach ($staff as $value) {
            if($s_id == $value->s_id){
                $select = 'selected';
            }else{
                $select = '';
            }
            $option .= '<option value="'.$value->s_id.'" '.$select.'>'.$value->s_name.'</option>';
        }

        $times = $this->getTimeNew($type);
        $option1 = '<option value="" selected="selected">ជ្រើសរើសពេល</option>';
        foreach ($times as $value) {
            if($time_id == $value->pav_id){
                $select = 'selected';
            }else{
                $select = '';
            }
            $option1 .= '<option value="'.$value->pav_id.'" '.$select.'>'.$value->pav_value.'</option>';
        }


        return response(['msg' => $option,'msg1' => $option1, 'status' => 'success']);  
            
    }

    public function index(Request $request)
    {
        $page = 'reportphone';
        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );

        // $staff = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name')
        //     ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
        //     ->orderBy('tbl_staff.s_id', 'ASC')
        //     ->groupBy('tbl_staff.s_id')
        //     ->pluck('s_name','s_id')
        //     ->all();
        if($request->staff != ''){
          $staff = $request->staff;
        }else{
          $staff = array();  
        }
        
        // $pages = DB::table('tbl_paper')
        //     ->select('tbl_paper.p_number')
        //     ->groupBy('tbl_paper.p_number')
        //     ->orderBy('tbl_paper.p_number', 'ASC')
        //     ->pluck('p_number','p_number')
        //     ->all();

        $pages = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_number')
            ->groupBy('tbl_paper_mobile.p_number')
            ->orderBy('tbl_paper_mobile.p_number', 'ASC')
            ->pluck('p_number','p_number')
            ->all();

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();
        return view('reportphone.index', compact('page','sheets','staff','pages','type_lottery','var_type_lottery'));
    }

    public function checkReportPhone(Request $request)
    {
        $page = 'checkreportphone';
        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );

        $staff = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('s_role', 1)
            ->orderBy('tbl_staff.s_id', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
        
        $pages = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_number')
            ->groupBy('tbl_paper_mobile.p_number')
            ->orderBy('tbl_paper_mobile.p_number', 'ASC')
            ->pluck('p_number','p_number')
            ->all();

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();
        return view('reportphone.checkreportphone', compact('page','sheets','staff','pages','type_lottery','var_type_lottery'));
    }


    public function dailyReportGet(Request $request)
    {
        $userID = $request->userID;
        $date = $request->dateData;
        $sheetID = $request->sheetID;
        $chilID = array();


        // check user parent
        // $checkParent = DB::table('tbl_staff')->where('parent_id', $userID)->get();
        $userInfo = DB::table('tbl_staff')->where('s_id', $userID)->first();
        $chilID = $this->loopchail($chilID,$userID);
        // dd($userID,$chilID);
        // if($checkParent){
        //     array_push($chilID,$userID);
        //     foreach ($checkParent as $key => $parent) {
        //         // $chilID .= $parent->s_id.',';
        //         array_push($chilID,$parent->s_id);
        //     }
        //     // if($chilID != ''){
        //         // $chilID .= $userID;
        //         // $chilID = substr($chilID, 0, -1);
        //     // }
            
        // }
        // if( count($chilID) > 0 ){
            // dd($chilID);
            $data = DB::table('tbl_sum_by_paper')
                ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                ->leftjoin('tbl_parameter_value', 'tbl_sum_by_paper.sheet_id','=','tbl_parameter_value.pav_id')
                ->whereIn('tbl_sum_by_paper.s_id', $chilID)
                ->where('tbl_sum_by_paper.date',$date)
                ->where('tbl_sum_by_paper.stc_type',1)
                ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
                ->orderBy('tbl_parameter_value.pav_value', 'ASC')
                ->get();

            // dd($data);
            
            

            return \Response::json(\View::make('reportphone/ajaxReport',compact('data','userInfo','date'))->render());

        
        
    }

    private function loopchail($chilID,$userID){
        $checkParent = DB::table('tbl_staff')->where('parent_id', $userID)->get();
        // var_dump($checkParent);
        array_push($chilID,$userID);
        if(count($checkParent) > 0){
            foreach ($checkParent as $key => $parent) {
                $subCheck = DB::table('tbl_staff')->where('parent_id', $parent->s_id)->first();
                
                if($subCheck){
                    // array_push($chilID,$parent->s_id);
                    $chilIDSub = array();
                    $chilID = $this->loopchail($chilID,$parent->s_id);
                    // array_merge($chilID, $funReturn);
                }
                else{
                    array_push($chilID,$parent->s_id);

                }
                
            }

            return $chilID;
            
        }else{
            return $chilID;
        }
        

    }



    public function filter(Request $request)
    {
        $page = 'reportphone';
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_sheet = $request->sheet;
        $var_staff = $request->staff;
        $var_page = $request->page;
        $var_type_lottery = $request->type_lottery;
        $staff_boss = 0;

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );

        $staff = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('tbl_staff.s_role',1)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
        $pages = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_number')
            ->groupBy('tbl_paper_mobile.p_number')
            ->orderBy('tbl_paper_mobile.p_number', 'ASC')
            ->pluck('p_number','p_number')
            ->all();

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();


        // check paid or borrow and old price
            $parameterMoney =array();
            $checkreport = DB::table('tbl_staff')->where('s_id',$var_staff)->first();
            $checkinView = $checkreport->payment;
            // dd($checkinView);
            // get percent
            $percentStaff = $checkreport->percent_data;

            // dd($checkreport->payment);
            // if($checkreport->payment == 1){

                $take_last = DB::table('tbl_staff_transction')
                ->where('s_id',$var_staff)
                ->where('st_date_search','<',$var_dateStart)
                ->orderBy('st_date_search','DESC')->first();

                // dd($take_last->st_date_search);
                if($take_last){
                    $money_paid = DB::table('tbl_staff_transction')
                              ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                              ->where('s_id',$var_staff)
                              ->where('st_date_search','=',$take_last->st_date_search)
                              ->where('st_date_search','<',$var_dateStart)
                              // ->where('st_type','<>','21')
                              ->get();
                    // dd($money_paid);
                }else{
                   $money_paid = []; 
                }
                // dd($take_last->st_date_diposit);
                // transction money
                


                $old_money = DB::table('tbl_total_everyday')->where('s_id',$var_staff)->orderBy('id', 'desc')->first();

            
   
        $get_val_main = $this->loop_find_staff($var_staff);

        if($get_val_main != ''){
            $mul_staff = substr($get_val_main,0,-1);
        }else{
            $mul_staff = '';
        }

       

        // dd($main_new);
        

        // dd($mul_staff);

        $conditions = '';

        if($var_sheet){
            $conditions .= " tbl_paper_mobile.p_time = '$var_sheet' AND ";
        }

        
           
        if($var_staff){
            $conditions .= " tbl_paper_mobile.s_id = '$var_staff' AND ";
        }

        if($var_page){
            $conditions .= " tbl_paper_mobile.p_number = '$var_page' AND ";
        }

        if($var_type_lottery){
            $conditions .= " tbl_paper_mobile.stc_type = '$var_type_lottery' AND ";
        }

        if($request->dateStart!="" && $request->dateEnd!=""){
            $conditions .= " tbl_paper_mobile.p_date >= '$var_dateStart' AND tbl_paper_mobile.p_date <= '$var_dateEnd' AND ";
        }elseif($request->dateStart!="" && $request->dateEnd==""){
            $conditions .= " tbl_paper_mobile.p_date = '$var_dateStart' AND ";
        }

        $conditions = substr($conditions, 0, -4);
        // var_dump(Session::get('roleLot'));
        // dd($conditions);
        
            if($conditions){
                $report = DB::table('tbl_paper_mobile')
                ->join('tbl_row_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
                ->leftjoin('tbl_staff','tbl_paper_mobile.s_id','=','tbl_staff.s_id')
                ->leftjoin('tbl_parameter_value','tbl_paper_mobile.p_time','=','tbl_parameter_value.pav_id')
                ->whereRaw($conditions)
                ->orderBy('tbl_paper_mobile.p_time','ASC')
                ->orderBy('tbl_paper_mobile.p_number','ASC')
                ->orderBy('tbl_paper_mobile.p_id' ,'ASC')
                ->get();
            }else{
                $report = DB::table('tbl_paper_mobile')
                ->join('tbl_row_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
                ->leftjoin('tbl_staff','tbl_paper_mobile.s_id','=','tbl_staff.s_id')
                ->leftjoin('tbl_parameter_value','tbl_paper_mobile.p_time','=','tbl_parameter_value.pav_id')
                ->orderBy('tbl_paper_mobile.p_time','ASC')
                ->orderBy('tbl_paper_mobile.p_number','ASC')
                ->orderBy('tbl_paper_mobile.p_id' ,'ASC')
                ->get();
            }

            if(count($report)!=0){
                return view('reportphone.index', compact('page','sheets','staff','pages','var_dateStart','var_dateEnd','var_staff','var_sheet','var_page','report','staff_boss','type_lottery','var_type_lottery','money_paid','old_money','checkinView','percentStaff'));
            }else{
                // return view('report.index', compact('page','sheets','staff','pages','type_lottery','var_type_lottery'));
                return view('reportphone.index', compact('page','sheets','staff','pages','var_dateStart','var_staff','var_sheet','var_page','staff_boss','type_lottery','var_type_lottery','money_paid','old_money','checkinView','percentStaff'));
                // return redirect('report/')->withInput()->with('staff', $staff);
            }
        
    }

    public function summary_lottery_total(Request $request){
        $date = $request->date;
        $staff = $request->staff;
        $totalReal = $request->totalReal;
        $totalDolla = $request->totalDolla;
        $var_type_lottery = $request->var_type_lottery;
        $check = DB::table('tbl_story_payment')
            ->where('date',$date)
            ->where('s_id',$staff)
            ->where('type_lottery',$var_type_lottery)
            ->first();
        if($check){
            DB::table('tbl_story_payment')
                ->where('date',$date)
                ->where('s_id',$staff)
                ->where('type_lottery',$var_type_lottery)
                ->delete();

            DB::table('tbl_story_payment')->insert(
                [
                    'money_real' => $totalReal,
                    'money_dolla' => $totalDolla,
                    'date' => $date,
                    's_id' => $staff,
                    'type_lottery' => $var_type_lottery
                ]
            );

            return response(['msg' => 'update', 'status' => 'success']);
        }else{
            DB::table('tbl_story_payment')->insert(
                [
                    'money_real' => $totalReal,
                    'money_dolla' => $totalDolla,
                    'date' => $date,
                    's_id' => $staff,
                    'type_lottery' => $var_type_lottery

                ]
            );
            return response(['msg' => 'insert', 'status' => 'success']);
        }
    }

    public function summary_lottery(Request $request)
    {

        $date = $request->date;
        $staff = $request->staff;
        $income_riel = $request->income_riel;
        $income_dollar = $request->income_dollar;
        $expense_riel = $request->expense_riel;
        $expense_dollar = $request->expense_dollar;
        $var_type_lottery = $request->var_type_lottery;
        $check = DB::table('tbl_summary_lottery')
            ->where('date',$date)
            ->where('s_id',$staff)
            ->where('stc_type',$var_type_lottery)
            ->first();
        if($check){
            DB::table('tbl_summary_lottery')
                ->where('date',$date)
                ->where('s_id',$staff)
                ->where('stc_type',$var_type_lottery)
                ->delete();

            DB::table('tbl_summary_lottery')->insert(
                [
                    'income_dollar' => $income_dollar,
                    'income_riel' => $income_riel,
                    'expense_dollar' => $expense_dollar,
                    'expense_riel' => $expense_riel,
                    'date' => $date,
                    's_id' => $staff,
                    'stc_type' => $var_type_lottery
                ]
            );

            return response(['msg' => 'update', 'status' => 'success']);

        }else{
            DB::table('tbl_summary_lottery')->insert(
                [
                    'income_dollar' => $income_dollar,
                    'income_riel' => $income_riel,
                    'expense_dollar' => $expense_dollar,
                    'expense_riel' => $expense_riel,
                    'date' => $date,
                    's_id' => $staff,
                    'stc_type' => $var_type_lottery

                ]
            );
            return response(['msg' => 'insert', 'status' => 'success']);
        }



    }

    public function summary_lottery_page(Request $request)
    {

        $date_new = $request->date_new;
        $type_lottery = $request->type_lottery;
        $staff_new = $request->staff_new;
        $staff_id = $request->staff_id;
        $row_data = $request->row_data;

        $page_sum = $request->total_page_sum;
        $start_num = $request->start_num;


        $check = DB::table('tbl_sum_by_paper')
            ->where('stc_type', 1)
            ->where('date',$date_new)
            ->where('s_id', $staff_id)
            // ->where('page_value','<=', $page_sum)
            // ->where('page_value','>=', $start_num)
            ->first();
        if($check){

            DB::table('tbl_sum_by_paper')
            ->where('stc_type', 1)
            ->where('date',$date_new)
            ->where('s_id', $staff_id)
            // ->where('page_value','<=', $page_sum)
            // ->where('page_value','>=', $start_num)
            ->delete();

            $row_page = explode(";", $row_data);
            foreach ($row_page as $key => $row) {
                if($row != ''){
                    $colum_data = explode(",", $row);

                    DB::table('tbl_sum_by_paper')->insert(
                        [
                            'stc_type' => 1,
                            'date' => $colum_data[0],
                            's_id' => $colum_data[1],
                            'sheet_id' => $colum_data[2],
                            'p_id' => $colum_data[3],

                            'price2digit_r' => $colum_data[4],
                            'price3digit_r' => $colum_data[5],
                            'win2digit_r' => $colum_data[6],
                            'win3digit_r' => $colum_data[7],

                            'price2digit_d' => $colum_data[8],
                            'price3digit_d' => $colum_data[9],
                            'win2digit_d' => $colum_data[10],
                            'win3digit_d' => $colum_data[11],
                            'lug_r' => $colum_data[13],
                            'lug_s' => $colum_data[14],
                        ]
                    );
                }  

            } #end if
            

            // DB::table('summary_lottery_page')->insert(
            //     [
            //         'type_lottery' => $type_lottery,
            //         'date_lottery' => $date_note,
            //         'staff_value' => $staff_value,
            //         'id_staff' => $id_staff,
            //         'time_value' => $time_value,
            //         'page_value' => $page_value,
            //         'total2digitr' => $total2digitr,
            //         'total3digitr' => $total3digitr,
            //         'totalsum' => $totalsum,
            //         'total2digitrright' => $total2digitrright,
            //         'total3digitrright' => $total3digitrright,
            //         'total2digits' => $total2digits,
            //         'total3digits' => $total3digits,
            //         'totalsum_dolla' => $totalsum_dolla,
            //         'total2digitsright' => $total2digitsright,
            //         'total3digitsright' => $total3digitsright
            //     ]
            // );

            return response(['msg' => 'update', 'status' => 'success']);

        }else{
            $row_page = explode(";", $row_data);
            foreach ($row_page as $key => $row) {
                if($row != ''){
                    $colum_data = explode(",", $row);

                    DB::table('tbl_sum_by_paper')->insert(
                        [
                            'stc_type' => 1,
                            'date' => $colum_data[0],
                            's_id' => $colum_data[1],
                            'sheet_id' => $colum_data[2],
                            'p_id' => $colum_data[3],

                            'price2digit_r' => $colum_data[4],
                            'price3digit_r' => $colum_data[5],
                            'win2digit_r' => $colum_data[6],
                            'win3digit_r' => $colum_data[7],

                            'price2digit_d' => $colum_data[8],
                            'price3digit_d' => $colum_data[9],
                            'win2digit_d' => $colum_data[10],
                            'win3digit_d' => $colum_data[11],
                            'lug_r' => $colum_data[13],
                            'lug_s' => $colum_data[14],
                        ]
                    );
                }  

            } #end if
            return response(['msg' => 'insert', 'status' => 'success']);
        }



    }


    public function addmoney(Request $request){
        $totalReal = $request->totalReal;
        $totalDolla = $request->totalDolla;
        $staff_id = $request->staff;
        $date_new = $request->date;
  
          $check = DB::table('tbl_total_everyday')->where('s_id',$staff_id)->where('date',$date_new)->first();
          if($check){
  
            DB::table('tbl_total_everyday')->where('s_id',$staff_id)->where('date',$date_new)->update([
                                'price_r' => $totalReal,
                                'price_d' => $totalDolla,
                                'oneday_price_r' => $totalReal,
                                'oneday_price_d' => $totalDolla,
                            ]
                        );
  
          }else{
  
            // $id_s = 1 ;
            // $draft =  DB::table('tbl_total_everyday')->orderBy('st_id', 'DESC')->take(1)->get();
            // if ($draft){
            //     foreach ($draft as $id) {
            //             $id_s = $id->st_id + 1;
            //     }
            // }
            
            DB::table('tbl_total_everyday')->insert([
                            'price_r' => $totalReal,
                            'price_d' => $totalDolla,
                            'date' => $date_new,
                            's_id' => $staff_id
                            ]
                        );
          }
  
          return response(['msg' => 'added', 'status' => 'success']);
    }

    private function getTimeNew($type){
        if($type=='1'){
            $pat_key = "sheet";
        }elseif($type=='2'){
            $pat_key = "sheet_khmer";
        }elseif($type=='3'){
            $pat_key = "sheet_th";
        }elseif($type=='4'){
            $pat_key = "sheet_lo";
        }else{
            $pat_key = "sheet";
        }
        $result = DB::table('tbl_parameter_type')
            ->where('tbl_parameter_type.pat_key','=',$pat_key)
            ->join('tbl_parameter_value','tbl_parameter_type.pat_id','=','tbl_parameter_value.pat_id')
            ->get();
        return $result;
    }

    private function loop_find_staff($st_id){
        // var_dump($st_id);
        $get_val = '';

        $check_boss = DB::table('tbl_staff')
            ->where('parent_id','=',$st_id)
            ->get();
        // var_dump($check_boss);
        if($check_boss){
            // dd(count($check_boss));
            if(count($check_boss) >= 1){
                foreach ($check_boss as $key => $val){
                    $get_val .= "'".$val->s_id."',";
                }
                // dd('ok');
                return $get_val;
            }else{

                foreach ($check_boss as $key => $val){
                    return $this->loop_find_staff($val->s_id);
                }
            }
        }else{
            return '';
        }

        // foreach ($check_boss as $key => $val){
        //     if($val->parent_id != ''){
        // $get_val = $this->loop_find_staff($var_staff);
                // foreach ($new_boos as $key_sub => $val_sub){
                //     $get_val .= "'".$val_sub->s_id."',";
                // }
            // }else{
            //     $get_val .= "'".$val->s_id."',";
            // }

        // $check_boss = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name','tbl_staff.parent_id')
        //     ->where('tbl_staff.parent_id','=',$pairent)
        //     ->first();
        // if($check_boss){
        //     $this->loop_find_staff($check_boss->parent_id);
        // }else{
        //     $get_val .= "'".$check_boss->s_id."',";
        //     return $get_val;
        // }

        // $get_boss = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name','tbl_staff.parent_id')
        //     ->where('tbl_staff.parent_id','=',$pairent)
        //     ->get();

        // foreach ($new_boos as $key_sub => $val_sub){
        //             $get_val .= "'".$val_sub->s_id."',";
        // }
        // dd($check_boss);
        // if($check_boss->parent_id == ''){
            
        // }else{
            // $this->loop_find_staff($check_boss->parent_id);
        // }
    }
}
