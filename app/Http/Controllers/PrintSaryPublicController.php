<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class PrintSaryPublicController extends Controller
{
    public function __construct()
    {
        
   
    }

    public function index(Request $request){
        $page = 'sary';

        $date = $request->dateprint;
        $time = $request->time;
        $sary =  DB::table('tbl_sary')->where('date_sary',$date)->where('type',$time)->first();

        if($time == 23){

            $idPosy = [51,52,53,54,55,56,57];
            $resultsMorningVone = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();
            // dd($resultsmorningv1);
        }else{
            $resultsMorningVone = [];
        }

        if($time == 24){

            $idPosy = [47,48,49,50,59,60,61];
            $resultsmorning = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();
            // dd($resultsAfternoon);
        }else{
            $resultsmorning = [];
        }

        if($time == 5){

            $idPosy = [1,2,3,4,5,6,7];
            $resultsAfternoon = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();
            // dd($resultsAfternoon);
        }else{
            $resultsAfternoon = [];
        }

        if($time == 6){

            $idPosyEvening = [10,11,12,13];
            $resultsEvening = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosyEvening)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    ->get();

        }else{
            $resultsEvening = [];
        }


        
        // dd($resultsmorningv1);
        return view("mobile.sary.print", compact('page','resultsAfternoon','resultsEvening','resultsMorningVone','resultsmorning','sary','date'));


    }
}
