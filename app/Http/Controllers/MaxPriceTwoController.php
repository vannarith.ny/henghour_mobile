<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class MaxPriceTwoController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }
            return $next($request);
        });
   
    }

    public function index(Request $request)
    {
     	
        $page = 'max2digit';
        $date = date("Y-m-d");

        $sheet_id = 5;
        $ThatTimeMorning ="10:30:00";
        $ThatTimeAfternoon ="13:30:00";
        $ThatTime ="16:30:00";
        if (time() <= strtotime($ThatTimeMorning)) {
          $sheet_id = 23;
        }else if (time() <= strtotime($ThatTimeAfternoon)) {
          $sheet_id = 24;
        }else if (time() <= strtotime($ThatTime)) {
          $sheet_id = 5;
        }else if (time() >= strtotime($ThatTime)) {
          $sheet_id = 6;
        }


        // $sheet_id = 5;
        // $ThatTime ="16:30:00";
        // if (time() >= strtotime($ThatTime)) {
        //   $sheet_id = 6;
        // }

        // $sheets = DB::table('tbl_parameter_type')
        //     ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
        //     ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
        //     ->where('tbl_parameter_type.pat_key', '=','sheet')
        //     ->orderBy('tbl_parameter_value.pav_id', 'ASC')
        //     ->pluck('pav_value','pav_id')
        //     ->all();

        // dd($sheets);

        
        $posts = DB::table('tbl_pos')
            ->select('pos_id','pos_name')
            ->where('pos_time',$sheet_id)
            ->orderBy('pos_name', 'ASC')
            ->pluck('pos_name','pos_id')
            ->all();
        
        $staffs = DB::table('tbl_staff')
            ->select('s_id','s_name')
            ->where('s_role',1)
            ->orderBy('s_name', 'ASC')
            ->pluck('s_name','s_id')
            ->all();

     	return view('maxprice.2digit', compact('page','posts','staffs','sheet_id'));
    }

    public function maxpricethree(Request $request)
    {
     	
        $page = 'max3digit';
        $date = date("Y-m-d");
        // $sheet_id = 5;
        // $ThatTime ="16:30:00";
        // if (time() >= strtotime($ThatTime)) {
        //   $sheet_id = 6;
        // }

        $sheet_id = 5;
        $ThatTimeMorning ="10:30:00";
        $ThatTimeAfternoon ="13:30:00";
        $ThatTime ="16:30:00";
        if (time() <= strtotime($ThatTimeMorning)) {
          $sheet_id = 5;
        }else if (time() <= strtotime($ThatTimeAfternoon)) {
          $sheet_id = 5;
        }else if (time() <= strtotime($ThatTime)) {
          $sheet_id = 5;
        }else if (time() >= strtotime($ThatTime)) {
          $sheet_id = 6;
        }
        
        $posts = DB::table('tbl_pos')
            ->select('pos_id','pos_name')
            ->where('pos_time',$sheet_id)
            ->orderBy('pos_name', 'ASC')
            ->pluck('pos_name','pos_id')
            ->all();
        
        $staffs = DB::table('tbl_staff')
            ->select('s_id','s_name')
            ->where('s_role',1)
            ->orderBy('s_name', 'ASC')
            ->pluck('s_name','s_id')
            ->all();

     	return view('maxprice.3digit', compact('page','posts', 'staffs','sheet_id'));
    }


    public function maxpricetwofilter(Request $request)
    {
        $page = 'max2digit';
        $date = date("Y-m-d");
        // $sheet_id = 5;
        // $ThatTime ="16:30:00";
        // if (time() >= strtotime($ThatTime)) {
        //   $sheet_id = 6;
        // }

        $sheet_id = 5;
        $ThatTimeMorning ="10:30:00";
        $ThatTimeAfternoon ="13:30:00";
        $ThatTime ="16:30:00";
        if (time() <= strtotime($ThatTimeMorning)) {
          $sheet_id = 5;
        }else if (time() <= strtotime($ThatTimeAfternoon)) {
          $sheet_id = 5;
        }else if (time() <= strtotime($ThatTime)) {
          $sheet_id = 5;
        }else if (time() >= strtotime($ThatTime)) {
          $sheet_id = 6;
        }

        $post_value = $request->pos_id;
        $staff_value = $request->s_id;
        $limitPrice_value = $request->limitPrice;
        $limitPriceDollar_value = $request->limitPriceDollar;
        
        $posts = DB::table('tbl_pos')
            ->select('pos_id','pos_name')
            ->where('pos_time',$sheet_id)
            ->orderBy('pos_name', 'ASC')
            ->pluck('pos_name','pos_id')
            ->all();

        $staffs = DB::table('tbl_staff')
            ->select('s_id','s_name')
            ->where('s_role',1)
            ->orderBy('s_name', 'ASC')
            ->pluck('s_name','s_id')
            ->all();

        // get all number for today
        // dd($staff_value);
        if(isset($staff_value[0]) && $staff_value[0] != ''){
            $allNumber = DB::table('tbl_paper_mobile')
                        ->join('tbl_row_mobile', 'tbl_row_mobile.p_id', '=', 'tbl_paper_mobile.p_id')
                        ->join('tbl_number_mobile', 'tbl_number_mobile.r_id', '=', 'tbl_row_mobile.r_id')
                        ->where('tbl_paper_mobile.p_date',$date)
                        ->where('tbl_paper_mobile.p_time',$sheet_id)
                        ->whereIn('tbl_paper_mobile.s_id', $staff_value)
                        ->where('stc_type', 1)
                        ->get();
        }else{

            $allNumber = DB::table('tbl_paper_mobile')
                        ->join('tbl_row_mobile', 'tbl_row_mobile.p_id', '=', 'tbl_paper_mobile.p_id')
                        ->join('tbl_number_mobile', 'tbl_number_mobile.r_id', '=', 'tbl_row_mobile.r_id')
                        ->where('p_date',$date)
                        ->where('p_time',$sheet_id)
                        ->where('stc_type', 1)
                        ->get();

        }

        // get group id with post id
        $groups = DB::table('tbl_group')
                    ->join('tbl_pos_group', 'tbl_pos_group.g_id', '=', 'tbl_group.g_id')
                    ->where('tbl_group.stc_type','=',1)
                    ->where('tbl_pos_group.pos_id','=',$post_value)
                    ->get();



     	return view('maxprice.2digit', compact('page','posts','post_value','groups', 'limitPrice_value', 'limitPriceDollar_value','date','sheet_id', 'allNumber','staffs','staff_value','sheet_id'));
    }


    public function maxpricethreefilter(Request $request)
    {
        ini_set('max_execution_time', 300);
        $page = 'max3digit';
        $date = date("Y-m-d");
        // $sheet_id = 5;
        // $ThatTime ="16:30:00";
        // if (time() >= strtotime($ThatTime)) {
        //   $sheet_id = 6;
        // }

        $sheet_id = 5;
        $ThatTimeMorning ="10:30:00";
        $ThatTimeAfternoon ="13:30:00";
        $ThatTime ="16:30:00";
        if (time() <= strtotime($ThatTimeMorning)) {
          $sheet_id = 23;
        }else if (time() <= strtotime($ThatTimeAfternoon)) {
          $sheet_id = 24;
        }else if (time() <= strtotime($ThatTime)) {
          $sheet_id = 5;
        }else if (time() >= strtotime($ThatTime)) {
          $sheet_id = 6;
        }

        $post_value = $request->pos_id;
        $staff_value = $request->s_id;
        // dd($staff_value);
        $limitPrice_value = $request->limitPrice;
        $limitPriceDollar_value = $request->limitPriceDollar;
        
        $posts = DB::table('tbl_pos')
            ->select('pos_id','pos_name')
            ->where('pos_time',$sheet_id)
            ->orderBy('pos_name', 'ASC')
            ->pluck('pos_name','pos_id')
            ->all();
        
        $staffs = DB::table('tbl_staff')
            ->select('s_id','s_name')
            ->where('s_role',1)
            ->orderBy('s_name', 'ASC')
            ->pluck('s_name','s_id')
            ->all();

        // get all number for today
        if(isset($staff_value[0]) && $staff_value[0] != ''){
            $allNumber = DB::table('tbl_paper_mobile')
                        ->join('tbl_row_mobile', 'tbl_row_mobile.p_id', '=', 'tbl_paper_mobile.p_id')
                        ->join('tbl_number_mobile', 'tbl_number_mobile.r_id', '=', 'tbl_row_mobile.r_id')
                        ->where('tbl_paper_mobile.p_date',$date)
                        ->where('tbl_paper_mobile.p_time',$sheet_id)
                        ->whereIn('tbl_paper_mobile.s_id', $staff_value)
                        ->where('stc_type', 1)
                        ->get();
        }else{
            $allNumber = DB::table('tbl_paper_mobile')
                        ->join('tbl_row_mobile', 'tbl_row_mobile.p_id', '=', 'tbl_paper_mobile.p_id')
                        ->join('tbl_number_mobile', 'tbl_number_mobile.r_id', '=', 'tbl_row_mobile.r_id')
                        ->where('p_date',$date)
                        ->where('p_time',$sheet_id)
                        ->where('stc_type', 1)
                        ->get();
        }
        // dd($allNumber);

        // get group id with post id
        $groups = DB::table('tbl_group')
                    ->join('tbl_pos_group', 'tbl_pos_group.g_id', '=', 'tbl_group.g_id')
                    ->where('tbl_group.stc_type','=',1)
                    ->where('tbl_pos_group.pos_id','=',$post_value)
                    ->get();



     	return view('maxprice.3digit', compact('page','posts','post_value','groups', 'limitPrice_value', 'limitPriceDollar_value','date','sheet_id', 'allNumber' ,'staffs','staff_value','sheet_id'));
    }

    public static function getpriceOfNumber($allNumber ,$number ,$pos, $date, $sheet_id)
    {
        // dd($allNumber);
        $priceReal = 0;
        $priceDollar = 0;
        $price = array();
        foreach ($pos as $key => $group) {
            $condition = $allNumber->where('g_id', $group->g_id)->where('num_number', $number);
            foreach ($condition as $subkey => $value) {
                if($value->num_currency == 2){
                    $priceDollar = $priceDollar + $value->num_price;
                }else{
                    $priceReal = $priceReal + $value->num_price;
                }
                
            }
        }
        array_push($price,$priceReal,$priceDollar);
        
        return $price; 
    }

    // public static function getpriceOfNumber3digit($allNumber ,$pos, $date, $sheet_id)
    // {
    //     // dd($allNumber);
    //     $priceReal = 0;
    //     $priceDollar = 0;

    //     $list = '';
    //     foreach ($pos as $key => $group) {
    //         $condition = $allNumber
    //                     ->where('g_id', $group->g_id)
    //                     ->groupBy('num_number');
    //         foreach ($condition as $subkey => $value) {
    //             if(strlen($value->num_number) == 3){
    //                 $list .= '<div class="numberList">'.$value->num_number.' : </div>';
    //             }
                
    //         }
    //     }
    //     array_push($price,$priceReal,$priceDollar);
        
    //     return $price; 
    
    // }
}
