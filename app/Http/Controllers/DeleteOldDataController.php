<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class DeleteOldDataController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }

               // if(Session::get('roleLot') == 0)
               // {
               //      if(Session::has('iduserlot')){
               //         Session::forget('iduserlot');
               //         Session::forget('usernameLot');
               //         Session::forget('nameLot');
               //         Session::forget('phoneLot');
               //      }

               //      Redirect::to('/login')->send();
               // }
            return $next($request);
        });
   
    }

    public function index(Request $request)
    {

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );
        $page = 'vannarith';

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();

    	return view('vannarith.index', compact('page','type_lottery','sheets'));
    }
    public function deleteolddata(Request $request)
    {
    	$start_date = $request->dateStart;
    	$end_date = $request->dateEnd;
    	$count = 0;

    	// page table
    	$pages = DB::table('tbl_paper')->where('p_date','>=',$start_date)->where('p_date','<=',$end_date)->get();
    	if(count($pages)){
    		foreach ($pages as $key => $value) {
    			$rows = DB::table('tbl_row')->where('p_id',$value->p_id)->get();
    			if(count($rows)){

    				foreach ($rows as $keyv2 => $valuev2) {
    					DB::table('tbl_number')->where('r_id',$valuev2->r_id)->delete();
    					$count++;
    				}

    				DB::table('tbl_row')->where('p_id',$value->p_id)->delete();

    			}else{
    				var_dump('Row dont have date : '.$value->p_date);
    			}
    		}

    		DB::table('tbl_paper')->where('p_date','>=',$start_date)->where('p_date','<=',$end_date)->delete();
    	}else{
    		dd('Data dont have date : '.$start_date.'->'.$end_date);
    	}
    	
    	dd('Deleted : '.$count);
    }
}
