<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileCheckReportDataController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function getReportData(Request $request)
    {
       $startDate = $request->fromDate;
       $endDate = $request->toDate;
       $userID = $request->userId;

       if($startDate <= $endDate){ //report one day

            $report = DB::table('tbl_total_everyday')
                    ->where('s_id',$userID)
                    ->where('date','>=',$startDate)
                    ->where('date','<=',$endDate)
                    ->orderBy('date', 'ASC')
                    ->groupBy('date')
                    ->get();

            $numRow = count($report);


            if($report){
                $html = '
                    <thead>
                        <tr>
                            <th class="ctd" rowspan="2">ថ្ងៃ</th>
                            <th class="classtd1" colspan="2">លទ្ធផល VN</th>
                            <th class="classtd1" colspan="2">លទ្ធផល KH</th>
                            <th class="classtd1 ondaytotal" colspan="2">លទ្ធផលមូយថ្ងៃ</th>
                            <th class="classtd1" colspan="2">កូនសង</th>
                            <th class="classtd1" colspan="2">កូនខ្ចី</th>
                            <th class="classtd1" colspan="2">តវ៉ា</th>
                            <th class="classtd1" colspan="2">ចាស់</th>
                            <th class="classtd1 lasttotal" colspan="2">ចុងក្រោយ</th>
                        </tr>
                        <tr>
                            <th class="classtd">(៛)</th>
                            <th class="classtd">($)</th>

                            <th class="classtd">(៛)</th>
                            <th class="classtd">($)</th>

                            <th class="classtd ondaytotal">(៛)</th>
                            <th class="classtd ondaytotal">($)</th>

                            <th class="classtd">(៛)</th>
                            <th class="classtd">($)</th>
                            <th class="classtd">(៛)</th>
                            <th class="classtd">($)</th>
                            <th class="classtd">(៛)</th>
                            <th class="classtd">($)</th>
                            <th class="classtd">(៛)</th>
                            <th class="classtd">($)</th>
                            <th class="classtd lasttotal">(៛)</th>
                            <th class="classtd lasttotal">($)</th>
                        </tr>
                    </thead>
                    <tbody >
                ';




                foreach ($report as $key => $value) {

                    $reportVN = DB::table('tbl_total_everyday')
                            ->where('s_id',$userID)
                            ->where('date','=',$value->date)
                            ->where('stc_type',1)
                            ->first();

                    $reportKH = DB::table('tbl_total_everyday')
                            ->where('s_id',$userID)
                            ->where('date','=',$value->date)
                            ->where('stc_type',2)
                            ->first();

                    $totalVN_R = 0;
                    $totalVN_D = 0;
                    if($reportVN){
                        $totalVN_R = $reportVN->price_r;
                        $totalVN_D = $reportVN->price_d;
                    }

                    $totalKH_R = 0;
                    $totalKH_D = 0;
                    if($reportKH){
                        $totalKH_R = $reportKH->price_r;
                        $totalKH_D = $reportKH->price_d;
                    }

                    $totalMain_R = $totalVN_R + $totalKH_R;
                    $totalMain_D = $totalVN_D + $totalKH_D;

                    $sumAmount_R = $totalMain_R;
                    $sumAmount_D = $totalMain_D;

                    $html .= '<tr align="center" class="tr-row no-transac">';
                        $html .= '<td class="classtd">'.date("m/d/Y", strtotime($value->date)).'</td>';
                        $html .= '<td class="classtd">'.number_format($totalVN_R).'</td>'; 
                        $html .= '<td class="classtd">'.$totalVN_D.'$</td>';

                        $html .= '<td class="classtd">'.number_format($totalKH_R).'</td>'; 
                        $html .= '<td class="classtd">'.$totalKH_D.'$</td>';

                        $html .= '<td class="classtd ondaytotal">'.number_format($totalMain_R).'</td>'; 
                        $html .= '<td class="classtd ondaytotal">'.$totalMain_D.'$</td>';

                        // get payment from staff
                        $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                        $pay = DB::table("tbl_staff_transction")
                                ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                ->where('tbl_staff_transction.st_date_search',$yesturday)
                                ->where('tbl_staff_transction.s_id',$value->s_id)
                                ->where('tbl_staff_transction.st_type',3)
                                ->first();
                        if($pay){
                            $html .= '<td class="classtd">'.number_format($pay->st_price_r).'</td>';
                            $html .= '<td class="classtd">'.$pay->st_price_d.'$</td>';

                            $sumAmount_R = $sumAmount_R - $pay->st_price_r;
                            $sumAmount_D = $sumAmount_D - $pay->st_price_d;

                        }else{
                            $html .= '<td class="classtd"></td>';
                            $html .= '<td class="classtd"></td>';
                        }

                        // get payment from boos
                        $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                        $transfar = DB::table("tbl_staff_transction")
                                 ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                 ->where('tbl_staff_transction.st_date_search',$yesturday)
                                 ->where('tbl_staff_transction.s_id',$value->s_id)
                                 ->where('tbl_staff_transction.st_type',4)
                                 ->first();
                        if($transfar){
                            $html .= '<td class="classtd">'.number_format($transfar->st_price_r).'</td>';
                            $html .= '<td class="classtd">'.$transfar->st_price_d.'$</td>';


                            $sumAmount_R = $sumAmount_R + $transfar->st_price_r;
                            $sumAmount_D = $sumAmount_D + $transfar->st_price_d;

                        }else{
                            $html .= '<td class="classtd"></td>';
                            $html .= '<td class="classtd"></td>';
                        }

                        // get payment from boos
                        $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                        $return = DB::table("tbl_staff_transction")
                                 ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                 ->where('tbl_staff_transction.st_date_search',$yesturday)
                                 ->where('tbl_staff_transction.s_id',$value->s_id)
                                 ->where('tbl_staff_transction.st_type',20)
                                 ->first();
                        if($return){
                            $html .= '<td class="classtd">'.number_format($return->st_price_r).'</td>';
                            $html .= '<td class="classtd">'.$return->st_price_d.'$</td>';

                            $sumAmount_R = $sumAmount_R - $return->st_price_r;
                            $sumAmount_D = $sumAmount_D - $return->st_price_d;

                        }else{
                            $html .= '<td class="classtd"></td>';
                            $html .= '<td class="classtd"></td>';
                        }

                        // get old payment
                        $take_last = DB::table('tbl_staff_transction')
                        ->where('s_id',$value->s_id)
                        ->where('st_date_search','<',$value->date)
                        ->orderBy('st_date_search','DESC')->first();
                        if($take_last){
                            $oldPayment = DB::table("tbl_staff_transction")
                                     ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                     ->where('tbl_staff_transction.st_date_search', '<',$value->date)
                                     ->where('tbl_staff_transction.st_date_search','=',$take_last->st_date_search)
                                     ->where('tbl_staff_transction.s_id',$value->s_id)
                                     ->where('tbl_staff_transction.st_type',21)
                                     ->first();
                            if($oldPayment){
                                $html .= '<td class="classtd">'.number_format($oldPayment->st_price_r).'</td>';
                                $html .= '<td class="classtd">'.$oldPayment->st_price_d.'$</td>';

                                $sumAmount_R = $sumAmount_R + $oldPayment->st_price_r;
                                $sumAmount_D = $sumAmount_D + $oldPayment->st_price_d;

                            }else{
                                $html .= '<td class="classtd"></td>';
                                $html .= '<td class="classtd"></td>';
                            }
                        }else{
                            $html .= '<td class="classtd"></td>';
                            $html .= '<td class="classtd"></td>';
                        }

                        $html .= '<td class="classtd lasttotal">'.number_format($sumAmount_R).'</td>';
                        $html .= '<td class="classtd lasttotal">'.$sumAmount_D.'$</td>';
                    $html .= '</tr>';


                    // save data to tbl_staff_transction
                    // $checkTransction = DB::table('tbl_staff_transction')->where('s_id',$value->s_id)->where('st_type','21')->where('st_date_search',$value->date)->first();
                    // if($checkTransction){

                    //     DB::table('tbl_staff_transction')->where('s_id',$value->s_id)->where('st_type','21')->where('st_date_search',$value->date)->update([
                    //                   'st_price_r' => $sumAmount_R,
                    //                   'st_price_d' => $sumAmount_D,
                    //                   'st_remark' => 'Update by user mobile',
                    //                   'st_by' => Session::get('iduserlot')
                    //                   ]
                    //               );

                    // }else{

                     
                    //   DB::table('tbl_staff_transction')->insert([
                    //                   'st_price_r' => $sumAmount_R,
                    //                   'st_price_d' => $sumAmount_D,
                    //                   'st_type' => '21',
                    //                   'st_date_diposit' => $value->date,
                    //                   'st_date_search' => $value->date,
                    //                   'st_remark' => "Insert by user mobile ",
                    //                   's_id' => $value->s_id,
                    //                   'st_by' => Session::get('iduserlot')
                    //                   ]
                    //               );
                    // }

                            
                }
                $html .= '</tbody>';
                return response(['msg' => 'ok','main' => 'no', 'html' => $html, 'status' => 'success']); 
            }else{
                $msg = '<tr align="center" class="tr-row no-transac"><td class="classtd" colspan="13">មិនមានទិន្នន័យ</td></tr>';
                return response(['msg' => $msg,'main' => 'not', 'status' => 'error']); 
            }

       }else{




               if(Session::get('iduserlotMobileSec') == $userID){

                    // $dateEndYesturday = date('Y-m-d',strtotime($endDate . "-1 days"));
                    // $startDate = $dateEndYesturday;

                    $loopStaff = DB::table('tbl_staff')
                                ->where('parent_id', $userID)
                                ->where('Active', 0)
                                ->orderBy('tbl_staff.s_name', 'ASC')
                                ->get();
                    $html = '';

                    if($loopStaff){



                        
                        foreach ($loopStaff as $key => $staffData) {

                            $report = DB::table('tbl_total_everyday')
                                ->where('s_id',$staffData->s_id)
                                ->where('date',$startDate)
                                ->first();

                            if($report){
                                $html .= '<tr align="center" class="tr-row no-transac">';
                                    $html .= '<td class="classtd">'.$staffData->s_name.'</td>';
                                    $html .= '<td class="classtd">'.$report->price_r.'</td>'; 
                                    $html .= '<td class="classtd">'.$report->price_d.'$</td>';

                                    // get payment from staff
                                    $yesturday = date('Y-m-d',strtotime($report->date . "-1 days"));
                                    $pay = DB::table("tbl_staff_transction")
                                            ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                            ->where('tbl_staff_transction.st_date_search',$yesturday)
                                            ->where('tbl_staff_transction.s_id',$report->s_id)
                                            ->where('tbl_staff_transction.st_type',3)
                                            ->first();
                                    if($pay){
                                        $html .= '<td class="classtd">'.$pay->st_price_r.'</td>';
                                        $html .= '<td class="classtd">'.$pay->st_price_d.'$</td>';
                                    }else{
                                        $html .= '<td class="classtd"></td>';
                                        $html .= '<td class="classtd"></td>';
                                    }

                                    // get payment from boos
                                    $yesturday = date('Y-m-d',strtotime($report->date . "-1 days"));
                                    $transfar = DB::table("tbl_staff_transction")
                                             ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                             ->where('tbl_staff_transction.st_date_search',$yesturday)
                                             ->where('tbl_staff_transction.s_id',$report->s_id)
                                             ->where('tbl_staff_transction.st_type',4)
                                             ->first();
                                    if($transfar){
                                        $html .= '<td class="classtd">'.$transfar->st_price_r.'</td>';
                                        $html .= '<td class="classtd">'.$transfar->st_price_d.'$</td>';
                                    }else{
                                        $html .= '<td class="classtd"></td>';
                                        $html .= '<td class="classtd"></td>';
                                    }

                                    // get payment from boos
                                    $yesturday = date('Y-m-d',strtotime($report->date . "-1 days"));
                                    $return = DB::table("tbl_staff_transction")
                                             ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                             ->where('tbl_staff_transction.st_date_search',$yesturday)
                                             ->where('tbl_staff_transction.s_id',$report->s_id)
                                             ->where('tbl_staff_transction.st_type',20)
                                             ->first();
                                    if($return){
                                        $html .= '<td class="classtd">'.$return->st_price_r.'</td>';
                                        $html .= '<td class="classtd">'.$return->st_price_d.'$</td>';
                                    }else{
                                        $html .= '<td class="classtd"></td>';
                                        $html .= '<td class="classtd"></td>';
                                    }

                                    // get old payment
                                    $oldPayment = DB::table("tbl_total_everyday")
                                             ->where('date','<',$report->date)
                                             ->where('s_id',$report->s_id)
                                             ->orderBy('date', 'desc')
                                             ->first();
                                    if($oldPayment){
                                        $html .= '<td class="classtd">'.$oldPayment->c_price_r.'</td>';
                                        $html .= '<td class="classtd">'.$oldPayment->c_price_d.'$</td>';
                                    }else{
                                        $html .= '<td class="classtd"></td>';
                                        $html .= '<td class="classtd"></td>';
                                    }

                                    $html .= '<td class="classtd">'.$report->c_price_r.'</td>';
                                    $html .= '<td class="classtd">'.$report->c_price_d.'$</td>';
                                $html .= '</tr>';
                            }
                                    
                        }


                    }

                    return response(['msg' => $startDate,'main' => 'yes', 'html' => $html, 'status' => 'success']); 

               }else{
                    $report = DB::table('tbl_total_everyday')
                            ->where('s_id',$userID)
                            ->where('date','>=',$startDate)
                            ->where('date','<=',$endDate)
                            ->orderBy('date', 'ASC')
                            ->get();


                    if($report){
                        $html = '

                        ';




                        foreach ($report as $key => $value) {
                            $html .= '<tr align="center" class="tr-row no-transac">';
                                $html .= '<td class="classtd">'.date("m/d/Y", strtotime($value->date)).'</td>';
                                $html .= '<td class="classtd">'.$value->price_r.'</td>'; 
                                $html .= '<td class="classtd">'.$value->price_d.'$</td>';

                                // get payment from staff
                                $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                                $pay = DB::table("tbl_staff_transction")
                                        ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                        ->where('tbl_staff_transction.st_date_search',$yesturday)
                                        ->where('tbl_staff_transction.s_id',$value->s_id)
                                        ->where('tbl_staff_transction.st_type',3)
                                        ->first();
                                if($pay){
                                    $html .= '<td class="classtd">'.$pay->st_price_r.'</td>';
                                    $html .= '<td class="classtd">'.$pay->st_price_d.'$</td>';
                                }else{
                                    $html .= '<td class="classtd"></td>';
                                    $html .= '<td class="classtd"></td>';
                                }

                                // get payment from boos
                                $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                                $transfar = DB::table("tbl_staff_transction")
                                         ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                         ->where('tbl_staff_transction.st_date_search',$yesturday)
                                         ->where('tbl_staff_transction.s_id',$value->s_id)
                                         ->where('tbl_staff_transction.st_type',4)
                                         ->first();
                                if($transfar){
                                    $html .= '<td class="classtd">'.$transfar->st_price_r.'</td>';
                                    $html .= '<td class="classtd">'.$transfar->st_price_d.'$</td>';
                                }else{
                                    $html .= '<td class="classtd"></td>';
                                    $html .= '<td class="classtd"></td>';
                                }

                                // get payment from boos
                                $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                                $return = DB::table("tbl_staff_transction")
                                         ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                                         ->where('tbl_staff_transction.st_date_search',$yesturday)
                                         ->where('tbl_staff_transction.s_id',$value->s_id)
                                         ->where('tbl_staff_transction.st_type',20)
                                         ->first();
                                if($return){
                                    $html .= '<td class="classtd">'.$return->st_price_r.'</td>';
                                    $html .= '<td class="classtd">'.$return->st_price_d.'$</td>';
                                }else{
                                    $html .= '<td class="classtd"></td>';
                                    $html .= '<td class="classtd"></td>';
                                }

                                // get old payment
                                $oldPayment = DB::table("tbl_total_everyday")
                                         ->where('date','<',$value->date)
                                         ->where('s_id',$value->s_id)
                                         ->orderBy('date', 'desc')
                                         ->first();
                                if($oldPayment){
                                    $html .= '<td class="classtd">'.$oldPayment->c_price_r.'</td>';
                                    $html .= '<td class="classtd">'.$oldPayment->c_price_d.'$</td>';
                                }else{
                                    $html .= '<td class="classtd"></td>';
                                    $html .= '<td class="classtd"></td>';
                                }

                                $html .= '<td class="classtd">'.$value->c_price_r.'</td>';
                                $html .= '<td class="classtd">'.$value->c_price_d.'$</td>';
                            $html .= '</tr>';
                                    
                        }
                        return response(['msg' => 'ok','main' => 'no', 'html' => $html, 'status' => 'success']); 
                    }else{
                        $msg = '<tr align="center" class="tr-row no-transac"><td class="classtd" colspan="13">មិនមានទិន្នន័យ</td></tr>';
                        return response(['msg' => $msg,'main' => 'not', 'status' => 'error']); 
                    }


               } //end check main 
        } //end date check

       



    }

    public function index(Request $request){
    	
    	$page = 'checkreportdata';
    	$stc_type = 1;

        
        $userList = DB::table("tbl_staff")
            ->where('tbl_staff.parent_id',Session::get('iduserlotMobileSec'))
            ->orderBy('tbl_staff.s_id','asc')
            ->get()->toArray();

    	// $ThatTime ="16:40:00";
		// if (time() >= strtotime($ThatTime)) {
		//   $sheet_id = 6;
		// }
        // $sheets = DB::table('tbl_parameter_value')->where('pat_id',3)->get();
        // $sheet_id = 23;

        // if (time() <= strtotime('10:40:00')) {
        //     $sheet_id = 23;
        // }elseif (time() <= strtotime('13:40:00')) {
        //     $sheet_id = 24;
        // }elseif (time() <= strtotime('16:40:00')) {
        //     $sheet_id = 5;
        // }else{
        //     $sheet_id = 6;
        // }

		// $sheetName =  DB::table('tbl_parameter_value')->where('pav_id',$sheet_id)->first();

		// $clientId = Session::get('iduserlotMobileSec');

		
        // dd($PostVote);
    	return view("mobile.checkreportdata.index", compact('page', 'userList'));
    }
}
