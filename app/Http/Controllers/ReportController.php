<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

use App\Model\Report;

class ReportController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }


    public function addtotalmoney(Request $request){
        $totalReal = $request->totalReal;
        $totalDolla = $request->totalDolla;
        $staff_id = $request->staff;
        $date_new = $request->date;
        $var_type_lottery = $request->var_type_lottery;
  
          $check = DB::table('tbl_total_everyday')->where('s_id',$staff_id)->where('stc_type',$var_type_lottery)->where('date',$date_new)->first();
          if($check){
  
            DB::table('tbl_total_everyday')->where('s_id',$staff_id)->where('stc_type',$var_type_lottery)->where('date',$date_new)->update([
                                'price_r' => $totalReal,
                                'price_d' => $totalDolla,
                                'c_price_r' => $totalReal,
                                'c_price_d' => $totalDolla,
                                'oneday_price_r' => $totalReal,
                                'oneday_price_d' => $totalDolla,
                                'stc_type' => $var_type_lottery
                            ]
                        );
  
          }else{
  
            // $id_s = 1 ;
            // $draft =  DB::table('tbl_total_everyday')->orderBy('st_id', 'DESC')->take(1)->get();
            // if ($draft){
            //     foreach ($draft as $id) {
            //             $id_s = $id->st_id + 1;
            //     }
            // }
            
            DB::table('tbl_total_everyday')->insert([
                            'price_r' => $totalReal,
                            'price_d' => $totalDolla,
                            'c_price_r' => $totalReal,
                            'c_price_d' => $totalDolla,
                            'oneday_price_r' => $totalReal,
                            'oneday_price_d' => $totalDolla,
                            'date' => $date_new,
                            's_id' => $staff_id,
                            'stc_type' => $var_type_lottery

                            ]
                        );
          }
  
          return response(['msg' => 'added', 'status' => 'success']);
    }


    public function getstaffbylotterytype(Request $request){
        $type = $request->type;

        if($type >= 5){
            $type_staff = 1;
        }else{
            $type_staff = $type;
        }

        $staff = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->get();

        $option = '<option value="" selected="selected">រើសបុគ្គលិក</option>';
        foreach ($staff as $value) {
            $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
        }

        $times = $this->getTimeNew($type);
        $option1 = '<option value="" selected="selected">ជ្រើសរើសពេល</option>';
        foreach ($times as $value) {
            $option1 .= '<option value="'.$value->pav_id.'">'.$value->pav_value.'</option>';
        }


        return response(['msg' => $option,'msg1' => $option1, 'status' => 'success']);  
            
    }

    public function index(Request $request)
    {
        $page = 'report';
        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH');

        // $staff = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name')
        //     ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
        //     ->orderBy('tbl_staff.s_id', 'ASC')
        //     ->groupBy('tbl_staff.s_id')
        //     ->pluck('s_name','s_id')
        //     ->all();
        if($request->staff != ''){
          $staff = $request->staff;
        }else{
          $staff = array();  
        }

        $ownner = array();
        
        $pages = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_number')
            ->groupBy('tbl_paper_mobile.p_number')
            ->orderByRaw('LENGTH(tbl_paper_mobile.p_number)')
            ->orderBy('tbl_paper_mobile.p_number','ASC')
            ->pluck('p_number','p_number')
            ->all();

        $ownner = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_code')
            ->groupBy('tbl_paper_mobile.p_code')
            // ->orderByRaw('LENGTH(tbl_paper.p_code)')
            ->orderBy('tbl_paper_mobile.p_code','ASC')
            ->pluck('p_code','p_code')
            ->all();

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();
        return view('report.index', compact('page','sheets','staff','pages','type_lottery','var_type_lottery','ownner'));
    }

//    public function filter(Request $request)
//    {
//        $page = 'report';
//        $var_dateStart = $request->dateStart;
//        $var_dateEnd = $request->dateEnd;
//        $var_sheet = $request->sheet;
//        $var_staff = $request->staff;
//        $var_page = $request->page;
//
//        return redirect('report/'.$var_dateStart.'/'.$var_dateEnd.'/'.$var_sheet.'/'.$var_staff.'/'.$var_page);
//    }

    public function filter(Request $request)
    {
        $page = 'report';
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_sheet = $request->sheet;
        $var_staff = $request->staff;
        $var_page = $request->page;
        $var_ownner = $request->ownner;
        $var_type_lottery = $request->type_lottery;

        $var_staff = $request->staff;

        // dd( $var_type_lottery);

        $type_lottery = array(1 => 'VN',2 =>'KH');

        if($var_type_lottery >= 5){
            $typeStaff = 1;
        }else{
            $typeStaff = $var_type_lottery;
        }

        $staff = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
        // dd($staff);
        $pages = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_number')
            ->groupBy('tbl_paper_mobile.p_number')
            ->orderByRaw('LENGTH(tbl_paper_mobile.p_number)')
            ->orderBy('tbl_paper_mobile.p_number','ASC')
            ->pluck('p_number','p_number')
            ->all();

        $ownner = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_code')
            ->groupBy('tbl_paper_mobile.p_code')
            // ->orderByRaw('LENGTH(tbl_paper.p_code)')
            ->orderBy('tbl_paper_mobile.p_code','ASC')
            ->pluck('p_code','p_code')
            ->all();

        $pat_key = '';
        if($var_type_lottery=='1'){
            $pat_key = "sheet";
        }elseif($var_type_lottery=='2'){
            $pat_key = "sheet_khmer";
        }else{
            $pat_key = "sheet";
        }    

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=',$pat_key)
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();


        // check paid or borrow and old price
            $parameterMoney =array();
            $checkreport = DB::table('tbl_staff')->where('s_id',$var_staff)->first();
            $checkinView = $checkreport->payment;
            $checkinViewKh = 0;

            // get percent
            $percentStaff = $checkreport->percent_data;
            $staffInfo = $checkreport;

            // dd($checkreport->payment);
            if($checkreport->payment == 1 || $checkinViewKh== 1){

                $take_last = DB::table('tbl_staff_transction')
                ->where('s_id',$var_staff)
                ->where('st_date_search','<',$var_dateStart)
                ->orderBy('st_date_search','DESC')->first();

                // dd($take_last->st_date_search);
                if($take_last){
                    $money_paid = DB::table('tbl_staff_transction')
                              ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                              ->where('s_id',$var_staff)
                              ->where('st_date_search','=',$take_last->st_date_search)
                              ->where('st_date_search','<',$var_dateStart)
                              // ->where('st_type','<>','21')
                              ->get();
                    // dd($money_paid);
                }else{
                   $money_paid = []; 
                }
                // dd($take_last->st_date_diposit);
                // transction money
                // dd($money_paid);


                $old_money = DB::table('tbl_story_payment')->where('s_id',$var_staff)->orderBy('id', 'desc')->first();

            }else{
                $money_paid = [];
                $old_money = [];
            }
          

       //  $check_boss = DB::table('tbl_staff')
       //      ->select('tbl_staff.s_id','tbl_staff.s_name','tbl_staff.parent_id')
       //      ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
       //      ->where('parent_id','=',$var_staff)
       //      ->get();

       // dd($check_boss);


        // $get_val = "";

        // foreach ($check_boss as $key => $val){
        //     if($val->parent_id != ''){
        $get_val_main = $this->loop_find_staff($var_staff);
                // foreach ($new_boos as $key_sub => $val_sub){
                //     $get_val .= "'".$val_sub->s_id."',";
                // }
            // }else{
            //     $get_val .= "'".$val->s_id."',";
            // }
            
        // }
        // dd($get_val_main);
        if($get_val_main != ''){
            $mul_staff = substr($get_val_main,0,-1);
        }else{
            $mul_staff = '';
        }

        $main_new = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('tbl_staff.s_id','=',$var_staff)
            ->get();

        // dd($main_new);
        

        // dd($mul_staff);

        $conditions = '';

        if($var_sheet){
            $conditions .= " tbl_paper_mobile.p_time = '$var_sheet' AND ";
        }

        if($mul_staff){
            $staff_boss = 1;
            if($var_staff){
                $conditions .= " tbl_paper_mobile.s_id IN ($mul_staff) AND ";
            }

            

        }else{
            $staff_boss = 0;
            if($var_staff){
                $conditions .= " tbl_paper_mobile.s_id = '$var_staff' AND ";
            }
        }

        if($var_page){
            $conditions .= " tbl_paper_mobile.p_number = '$var_page' AND ";
        }

        if($var_ownner){
            $conditions .= " tbl_paper_mobile.p_code = '$var_ownner' AND ";
        }

        if($var_type_lottery){
            $conditions .= " tbl_paper_mobile.stc_type = '$var_type_lottery' AND ";
        }

        if($request->dateStart!="" && $request->dateEnd!=""){
            $conditions .= " tbl_paper_mobile.p_date >= '$var_dateStart' AND tbl_paper_mobile.p_date <= '$var_dateEnd' AND ";
        }elseif($request->dateStart!="" && $request->dateEnd==""){
            $conditions .= " tbl_paper_mobile.p_date = '$var_dateStart' AND ";
        }

        $conditions = substr($conditions, 0, -4);
        // var_dump(Session::get('roleLot'));
        // dd($conditions);
        if(Session::get('roleLot') == 1 || Session::get('roleLot') == 2 || Session::get('roleLot') == 3 || Session::get('roleLot') == 0){
            // dd($conditions);
            if($conditions){
                $report = DB::table('tbl_paper_mobile')
                ->join('tbl_row_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
                ->leftjoin('tbl_staff','tbl_paper_mobile.s_id','=','tbl_staff.s_id')
                ->leftjoin('tbl_parameter_value','tbl_paper_mobile.p_time','=','tbl_parameter_value.pav_id')
                ->whereRaw($conditions)
                ->orderBy('tbl_paper_mobile.s_id' ,'ASC')
                ->orderBy('tbl_parameter_value.pav_value' ,'ASC')
                ->orderByRaw('LENGTH(tbl_paper_mobile.p_code)')
                ->orderBy('tbl_paper_mobile.p_code' ,'ASC')
                ->orderByRaw('LENGTH(tbl_paper_mobile.p_number)')
                ->orderBy('tbl_paper_mobile.p_number','ASC')
                ->orderBy('tbl_paper_mobile.p_id' ,'ASC')
                ->get();
                // dd($report);
            }else{
                $report = DB::table('tbl_paper_mobile')
                ->join('tbl_row_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
                ->leftjoin('tbl_staff','tbl_paper_mobile.s_id','=','tbl_staff.s_id')
                ->leftjoin('tbl_parameter_value','tbl_paper_mobile.p_time','=','tbl_parameter_value.pav_id')
                ->orderBy('tbl_paper_mobile.s_id' ,'ASC')
                ->orderBy('tbl_paper_mobile.p_time' ,'ASC')
                ->orderBy('tbl_paper_mobile.p_code' ,'ASC')
                // ->orderBy('tbl_paper.p_number','ASC')
                ->orderByRaw('LENGTH(tbl_paper_mobile.p_number)')
                ->orderBy('tbl_paper_mobile.p_number','ASC')
                // ->orderBy('tbl_paper.p_id' ,'ASC')
                ->get();
            }

        }else{
            if($conditions){

                $report = DB::table('tbl_paper_mobile')
                ->join('tbl_row_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
                ->leftjoin('tbl_staff','tbl_paper_mobile.s_id','=','tbl_staff.s_id')
                ->leftjoin('tbl_parameter_value','tbl_paper_mobile.p_time','=','tbl_parameter_value.pav_id')
                ->where('tbl_paper_mobile.u_id',Session::get('iduserlot'))
                ->whereRaw($conditions)
                ->orderBy('tbl_paper_mobile.s_id' ,'ASC')
                ->orderBy('tbl_paper_mobile.p_time' ,'ASC')
                // ->orderBy('tbl_paper.p_number','ASC')
                ->orderBy('tbl_paper_mobile.p_code' ,'ASC')
                ->orderByRaw('LENGTH(tbl_paper_mobile.p_number)')
                ->orderBy('tbl_paper_mobile.p_number','ASC')
                // ->orderBy('tbl_paper.p_id' ,'ASC')
                ->get();
            }else{
                $report = DB::table('tbl_paper_mobile')
                ->join('tbl_row_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
                ->leftjoin('tbl_staff','tbl_paper_mobile.s_id','=','tbl_staff.s_id')
                ->leftjoin('tbl_parameter_value','tbl_paper_mobile.p_time','=','tbl_parameter_value.pav_id')
                ->where('tbl_paper_mobile.u_id',Session::get('iduserlot'))
                ->orderBy('tbl_paper_mobile.s_id' ,'ASC')
                ->orderBy('tbl_paper_mobile.p_time' ,'ASC')
                ->orderBy('tbl_paper_mobile.p_code' ,'ASC')
                ->orderByRaw('LENGTH(tbl_paper_mobile.p_number)')
                ->orderBy('tbl_paper_mobile.p_number','ASC')
                // ->orderBy('tbl_paper.p_number','ASC')
                // ->orderBy('tbl_paper.p_id' ,'ASC')
                ->get();

            }
        }
        
//        echo $report;
//        dd(count($report));
       // dd($report);

        if(count($report)!=0){
            // dd($staff_boss);
            if($staff_boss != 1 || $var_staff == 611){
                // dd();
                return view('report.index', compact('page','sheets','staff','pages','var_dateStart','var_dateEnd','var_staff','var_sheet','var_page','report','staff_boss','type_lottery','var_type_lottery','money_paid','old_money','checkinView','checkinViewKh','percentStaff', 'staffInfo','ownner','var_ownner'));
            }else{

                $check_staff_main = DB::table('tbl_staff')
                    ->select('s_id','s_name')
                    ->where('s_id','=',$var_staff)
                    ->first();

                // dd($check_staff_main->s_id);

               
                   $boss_data = DB::table('summary_lottery_page')
                        ->where('type_lottery',$var_type_lottery)
                        ->where('date_lottery',date("d-m-Y", strtotime($var_dateStart)))
                        ->whereRaw('id_staff IN ('.$mul_staff.')')
                        ->orderBy('id_staff' ,'ASC')
                        ->orderBy('staff_value' ,'ASC')
                        ->orderBy('time_value' ,'ASC')
                        ->orderByRaw('LENGTH(page_value)')
                        // ->orderBy('page_value','ASC')
                        ->get(); 
                

                

                // $main_new = array();
                // dd($boss_data);
                return view('report.index_boss', compact('page','sheets','staff','pages','var_dateStart','var_dateEnd','var_staff','var_sheet','var_page','report','staff_boss','type_lottery','var_type_lottery','boss_data','check_staff_main','main_new','money_paid','old_money','checkinView','checkinViewKh','percentStaff', 'staffInfo','ownner','var_ownner'));
            }
        
        }else{

            $date_new_delete = date("d-m-Y", strtotime($var_dateStart) );
            $staff_id_delete = $var_staff;

            // DB::table('summary_lottery_page')->where('date_lottery', $date_new_delete)->where('id_staff', $staff_id_delete)->delete();
            // DB::table('tbl_sum_by_paper')->where('date', $var_dateStart)->where('s_id', $staff_id_delete)->delete();
            // DB::table('tbl_summary_lottery')->where('date', $var_dateStart)->where('s_id', $staff_id_delete)->delete();
            // DB::table('tbl_total_everyday')->where('date', $var_dateStart)->where('s_id', $staff_id_delete)->delete();

            // return view('report.index', compact('page','sheets','staff','pages','type_lottery','var_type_lottery'));
            return view('report.index', compact('page','sheets','staff','pages','var_dateStart','var_staff','var_sheet','var_page','staff_boss','type_lottery','var_type_lottery','money_paid','old_money','checkinView','checkinViewKh','percentStaff', 'staffInfo','ownner','var_ownner'));
            // return redirect('report/')->withInput()->with('staff', $staff);
        }
    }

    public function summary_lottery_total(Request $request){
        $date = $request->date;
        $staff = $request->staff;
        $totalReal = $request->totalReal;
        $totalDolla = $request->totalDolla;
        $var_type_lottery = $request->var_type_lottery;
        $check = DB::table('tbl_story_payment')
            ->where('date',$date)
            ->where('s_id',$staff)
            ->where('type_lottery',$var_type_lottery)
            ->first();
        if($check){
            DB::table('tbl_story_payment')
                ->where('date',$date)
                ->where('s_id',$staff)
                ->where('type_lottery',$var_type_lottery)
                ->delete();

            DB::table('tbl_story_payment')->insert(
                [
                    'money_real' => $totalReal,
                    'money_dolla' => $totalDolla,
                    'date' => $date,
                    's_id' => $staff,
                    'type_lottery' => $var_type_lottery
                ]
            );

            return response(['msg' => 'update', 'status' => 'success']);
        }else{
            DB::table('tbl_story_payment')->insert(
                [
                    'money_real' => $totalReal,
                    'money_dolla' => $totalDolla,
                    'date' => $date,
                    's_id' => $staff,
                    'type_lottery' => $var_type_lottery

                ]
            );
            return response(['msg' => 'insert', 'status' => 'success']);
        }
    }

    public function summary_lottery(Request $request)
    {

        $date = $request->date;
        $staff = $request->staff;
        $income_riel = $request->income_riel;
        $income_dollar = $request->income_dollar;
        $expense_riel = $request->expense_riel;
        $expense_dollar = $request->expense_dollar;
        $var_type_lottery = $request->var_type_lottery;
        $check = DB::table('tbl_summary_lottery')
            ->where('date',$date)
            ->where('s_id',$staff)
            ->first();
        if($check){
            DB::table('tbl_summary_lottery')
                ->where('date',$date)
                ->where('s_id',$staff)
                ->delete();

            DB::table('tbl_summary_lottery')->insert(
                [
                    'income_dollar' => $income_dollar,
                    'income_riel' => $income_riel,
                    'expense_dollar' => $expense_dollar,
                    'expense_riel' => $expense_riel,
                    'date' => $date,
                    's_id' => $staff,
                    'stc_type' => $var_type_lottery
                ]
            );

            return response(['msg' => 'update', 'status' => 'success']);

        }else{
            DB::table('tbl_summary_lottery')->insert(
                [
                    'income_dollar' => $income_dollar,
                    'income_riel' => $income_riel,
                    'expense_dollar' => $expense_dollar,
                    'expense_riel' => $expense_riel,
                    'date' => $date,
                    's_id' => $staff,
                    'stc_type' => $var_type_lottery

                ]
            );
            return response(['msg' => 'insert', 'status' => 'success']);
        }



    }

    public function summary_lottery_page(Request $request)
    {

        $date_new = $request->date_new;
        $type_lottery = $request->type_lottery;
        $staff_new = $request->staff_new;
        $id_staff = $request->id_staff;
        $row_data = $request->row_data;

        $page_sum = $request->total_page_sum;
        $start_num = $request->start_num;
        $p_code_data = $request->page_code;


        
            if($row_data != ''){
                $row_page = explode(";", $row_data);
                foreach ($row_page as $key => $row) {
                    // $idNeedOrder = $key+1;
                    if($row != ''){
                        $colum_data = explode(",", $row);

                        $check = DB::table('summary_lottery_page')
                            ->where('p_id',$colum_data[14])
                            ->first();
                        if($check){
                            DB::table('summary_lottery_page')->where('p_id',$colum_data[14])->update(
                                [
                                    'type_lottery' => $type_lottery,
                                    'date_lottery' => $date_new,
                                    'staff_value' => $staff_new,
                                    'id_staff' => $id_staff,
                                    'time_value' => $colum_data[0],
                                    'page_value' => $colum_data[1],
                                    'total2digitr' => $colum_data[2],
                                    'total3digitr' => $colum_data[3],
                                    'totalsum' => $colum_data[4],
                                    'total2digitrright' => $colum_data[5],
                                    'total3digitrright' => $colum_data[6],
                                    'total2digits' => $colum_data[7],
                                    'total3digits' => $colum_data[8],
                                    'totalsum_dolla' => $colum_data[9],
                                    'total2digitsright' => $colum_data[10],
                                    'total3digitsright' => $colum_data[11],
                                    'p_code' => $colum_data[13],
                                    // 'p_id' => $colum_data[14],
                                    'p_code_p' => $colum_data[15],
                                    'id_order' => $key+1,
                                    'total1digitrright' => $colum_data[16],
                                    'total1digitsright' => $colum_data[17]
                                ]
                            );
                        }else{
                            DB::table('summary_lottery_page')->insert(
                                [
                                    'type_lottery' => $type_lottery,
                                    'date_lottery' => $date_new,
                                    'staff_value' => $staff_new,
                                    'id_staff' => $id_staff,
                                    'time_value' => $colum_data[0],
                                    'page_value' => $colum_data[1],
                                    'total2digitr' => $colum_data[2],
                                    'total3digitr' => $colum_data[3],
                                    'totalsum' => $colum_data[4],
                                    'total2digitrright' => $colum_data[5],
                                    'total3digitrright' => $colum_data[6],
                                    'total2digits' => $colum_data[7],
                                    'total3digits' => $colum_data[8],
                                    'totalsum_dolla' => $colum_data[9],
                                    'total2digitsright' => $colum_data[10],
                                    'total3digitsright' => $colum_data[11],
                                    'p_code' => $colum_data[13],
                                    'p_id' => $colum_data[14],
                                    'p_code_p' => $colum_data[15],
                                    'id_order' => $key+1,
                                    'total1digitrright' => $colum_data[16],
                                    'total1digitsright' => $colum_data[17]
                                ]
                            );
                        }

                        
                    }  

                } #end foreach
            }else{

                $page_value = $request->page_value;
                $new_time = $request->new_time;
                $ownner = $request->ownner;

                $conditions = '';
                if($page_value != ''){
                    $conditions .= " page_value = '$page_value' AND ";
                }
                if($new_time != ''){
                    $vowels = array(0, 1, 2, 3, 4,' :5', ' 6:', ' 7:5', ' :', '8');
                    $onlyconsonants = str_replace($vowels, "", $new_time);
                    // dd($new_time);
                    $conditions .= " time_value like '%$onlyconsonants' AND ";
                }
                if($ownner != ''){
                    $conditions .= " p_code = '$ownner' AND ";
                }

                $conditions = substr($conditions, 0, -4);
                // dd($conditions);
                if($conditions){
                    $query = DB::table('summary_lottery_page')
                        ->where('date_lottery',date("d-m-Y", strtotime($date_new)))
                        ->where('type_lottery', $type_lottery)
                        ->where('id_staff', $id_staff)
                        ->whereRaw($conditions)
                        ->delete();
                }else{
                    $query = DB::table('summary_lottery_page')
                        ->where('date_lottery',date("d-m-Y", strtotime($date_new)))
                        ->where('type_lottery', $type_lottery)
                        ->where('id_staff', $id_staff)
                        ->delete();
                }   

            }

            return response(['msg' => 'update', 'status' => 'success']);
            




    }

    public function summary_lottery_page_by_page(Request $request)
    {

        $date_new = $request->date_new;
        $type_lottery = $request->type_lottery;
        $id_staff = $request->id_staff;
        $row_data = $request->row_data;

        $page_sum = $request->total_page_sum;
        $start_num = $request->start_num;


        if($row_data != ''){
            $row_page = explode(";", $row_data);
            foreach ($row_page as $key => $row) {
                if($row != ''){
                    $colum_data = explode(",", $row);

                    $check = DB::table('tbl_sum_by_paper')
                        ->where('p_id',$colum_data[14])
                        ->first();
                    if($check){
                        DB::table('tbl_sum_by_paper')->where('p_id',$colum_data[14])->update(
                            [
                                'price2digit_r' => $colum_data[2],
                                'price3digit_r' => $colum_data[3],
                                'price2digit_d' => $colum_data[7],
                                'price3digit_d' => $colum_data[8],
                                'win2digit_r' => $colum_data[5],
                                'win3digit_r' => $colum_data[6],
                                'win2digit_d' => $colum_data[10],
                                'win3digit_d' => $colum_data[11],
                                's_id' => $id_staff,
                                'date' => $date_new,
                                // 'p_id' => $colum_data[1],
                                'sheet_id' => $colum_data[0],
                                'page_value' => $colum_data[1],
                                'stc_type' => $type_lottery,
                                'win1digit_r' => $colum_data[16],
                                'win1digit_d' => $colum_data[17]
                            ]
                        );
                    }else{
                        DB::table('tbl_sum_by_paper')->insert(
                            [

                                'price2digit_r' => $colum_data[2],
                                'price3digit_r' => $colum_data[3],
                                'price2digit_d' => $colum_data[7],
                                'price3digit_d' => $colum_data[8],
                                'win2digit_r' => $colum_data[5],
                                'win3digit_r' => $colum_data[6],
                                'win2digit_d' => $colum_data[10],
                                'win3digit_d' => $colum_data[11],
                                's_id' => $id_staff,
                                'date' => $date_new,
                                'p_id' => $colum_data[14],
                                'sheet_id' => $colum_data[0],
                                'page_value' => $colum_data[1],
                                'stc_type' => $type_lottery,
                                'win1digit_r' => $colum_data[16],
                                'win1digit_d' => $colum_data[17]
                                
                            ]
                        );
                    }

                    
                }  

            } #end foreach
        }else{

            $page_value = $request->page_value;
            $new_time = $request->new_time;
            $ownner = $request->ownner;

            $conditions = '';
            if($page_value != ''){
                $conditions .= " page_value = '$page_value' AND ";
            }
            if($new_time != ''){
                // $vowels = array(0, 1, 2, 3, 4);
                // $onlyconsonants = str_replace($vowels, "", $new_time);
                $conditions .= " sheet_id = '$new_time' AND ";
            }
            // if($ownner != ''){
            //     $conditions .= " p_code = '$ownner' AND ";
            // }

            $conditions = substr($conditions, 0, -4);
            // dd($conditions);
            if($conditions){
                $query = DB::table('tbl_sum_by_paper')
                    ->where('date',$date_new)
                    ->where('stc_type', $type_lottery)
                    ->where('s_id', $id_staff)
                    ->whereRaw($conditions)
                    ->delete();
            }else{
                $query = DB::table('tbl_sum_by_paper')
                    ->where('date',$date_new)
                    ->where('stc_type', $type_lottery)
                    ->where('s_id', $id_staff)
                    ->delete();
            }   

        }

        return response(['msg' => 'update', 'status' => 'success']);


        // $check = DB::table('tbl_sum_by_paper')
        //     ->where('date',$date_new)
        //     ->where('s_id', $id_staff)
        //     // ->where('page_value','<=', $page_sum)
        //     // ->where('page_value','>=', $start_num)
        //     ->first();
        // if($check){

        //     DB::table('tbl_sum_by_paper')
        //     ->where('date',$date_new)
        //     ->where('s_id', $id_staff)
        //     // ->where('page_value','<=', $page_sum)
        //     // ->where('page_value','>=', $start_num)
        //     ->delete();

        //     $row_page = explode(";", $row_data);
        //     foreach ($row_page as $key => $row) {
        //         if($row != ''){
        //             $colum_data = explode(",", $row);

        //             DB::table('tbl_sum_by_paper')->insert(
        //                 [

        //                     'price2digit_r' => $colum_data[2],
        //                     'price3digit_r' => $colum_data[3],
        //                     'price2digit_d' => $colum_data[7],
        //                     'price3digit_d' => $colum_data[8],
        //                     'win2digit_r' => $colum_data[5],
        //                     'win3digit_r' => $colum_data[6],
        //                     'win2digit_d' => $colum_data[10],
        //                     'win3digit_d' => $colum_data[11],
        //                     's_id' => $id_staff,
        //                     'date' => $date_new,
        //                     'p_id' => $colum_data[1],
        //                     'sheet_id' => $colum_data[0],
        //                     'stc_type' => $type_lottery
                            
        //                 ]
        //             );
        //         }  

        //     } #end if
            

        //     // DB::table('summary_lottery_page')->insert(
        //     //     [
        //     //         'type_lottery' => $type_lottery,
        //     //         'date_lottery' => $date_note,
        //     //         'staff_value' => $staff_value,
        //     //         'id_staff' => $id_staff,
        //     //         'time_value' => $time_value,
        //     //         'page_value' => $page_value,
        //     //         'total2digitr' => $total2digitr,
        //     //         'total3digitr' => $total3digitr,
        //     //         'totalsum' => $totalsum,
        //     //         'total2digitrright' => $total2digitrright,
        //     //         'total3digitrright' => $total3digitrright,
        //     //         'total2digits' => $total2digits,
        //     //         'total3digits' => $total3digits,
        //     //         'totalsum_dolla' => $totalsum_dolla,
        //     //         'total2digitsright' => $total2digitsright,
        //     //         'total3digitsright' => $total3digitsright
        //     //     ]
        //     // );

        //     return response(['msg' => 'update', 'status' => 'success']);

        // }else{
        //     $row_page = explode(";", $row_data);
        //     foreach ($row_page as $key => $row) {
        //         if($row != ''){
        //             $colum_data = explode(",", $row);

        //             // DB::table('tbl_sum_by_paper')->insert(
        //             //     [
        //             //         'price2digit_r' => $colum_data[6],
        //             //         'price3digit_r' => $colum_data[7],
        //             //         'price2digit_d' => $colum_data[11],
        //             //         'price3digit_d' => $colum_data[12],
        //             //         'win2digit_r' => $colum_data[9],
        //             //         'win3digit_r' => $colum_data[10],
        //             //         'win2digit_d' => $colum_data[14],
        //             //         'win3digit_d' => $colum_data[15],
        //             //         's_id' => $colum_data[2],
        //             //         'date' => $date_new,
        //             //         'p_id' => $colum_data[5],
        //             //         'sheet_id' => $colum_data[4],
        //             //         'stc_type' => $colum_data[0]
        //             //     ]
        //             // );

        //             DB::table('tbl_sum_by_paper')->insert(
        //                 [

        //                     'price2digit_r' => $colum_data[2],
        //                     'price3digit_r' => $colum_data[3],
        //                     'price2digit_d' => $colum_data[7],
        //                     'price3digit_d' => $colum_data[8],
        //                     'win2digit_r' => $colum_data[5],
        //                     'win3digit_r' => $colum_data[6],
        //                     'win2digit_d' => $colum_data[10],
        //                     'win3digit_d' => $colum_data[11],
        //                     's_id' => $id_staff,
        //                     'date' => $date_new,
        //                     'p_id' => $colum_data[1],
        //                     'sheet_id' => $colum_data[0],
        //                     'stc_type' => $type_lottery
                            
        //                 ]
        //             );
        //         }  

        //     } #end if
        //     return response(['msg' => 'insert', 'status' => 'success']);
        // }



    }


    private function getTimeNew($type){
        if($type=='1'){
            $pat_key = "sheet";
        }elseif($type=='2'){
            $pat_key = "sheet_khmer";
        }elseif($type=='3'){
            $pat_key = "sheet_th";
        }elseif($type=='4'){
            $pat_key = "sheet_lo";
        }elseif($type=='5'){
            $pat_key = "sheet_hoso";
        }elseif($type=='6'){
            $pat_key = "sheet_minhgoc";
        }elseif($type=='7'){
            $pat_key = "sheet_thinhnam";
        }else{
            $pat_key = "sheet";
        }
        $result = DB::table('tbl_parameter_type')
            ->where('tbl_parameter_type.pat_key','=',$pat_key)
            ->join('tbl_parameter_value','tbl_parameter_type.pat_id','=','tbl_parameter_value.pat_id')
            ->orderBy('tbl_parameter_value.pav_value')
            ->get();
        return $result;
    }

    private function loop_find_staff($st_id){
        // var_dump($st_id);
        $get_val = '';

        $check_boss = DB::table('tbl_staff')
            ->where('parent_id','=',$st_id)
            ->get();
        // var_dump($check_boss);
        if($check_boss){
            // dd(count($check_boss));
            if(count($check_boss) >= 1){
                foreach ($check_boss as $key => $val){
                    $get_val .= "'".$val->s_id."',";
                }
                // dd('ok');
                return $get_val;
            }else{

                foreach ($check_boss as $key => $val){
                    return $this->loop_find_staff($val->s_id);
                }
            }
        }else{
            return '';
        }

        // foreach ($check_boss as $key => $val){
        //     if($val->parent_id != ''){
        // $get_val = $this->loop_find_staff($var_staff);
                // foreach ($new_boos as $key_sub => $val_sub){
                //     $get_val .= "'".$val_sub->s_id."',";
                // }
            // }else{
            //     $get_val .= "'".$val->s_id."',";
            // }

        // $check_boss = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name','tbl_staff.parent_id')
        //     ->where('tbl_staff.parent_id','=',$pairent)
        //     ->first();
        // if($check_boss){
        //     $this->loop_find_staff($check_boss->parent_id);
        // }else{
        //     $get_val .= "'".$check_boss->s_id."',";
        //     return $get_val;
        // }

        // $get_boss = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name','tbl_staff.parent_id')
        //     ->where('tbl_staff.parent_id','=',$pairent)
        //     ->get();

        // foreach ($new_boos as $key_sub => $val_sub){
        //             $get_val .= "'".$val_sub->s_id."',";
        // }
        // dd($check_boss);
        // if($check_boss->parent_id == ''){
            
        // }else{
            // $this->loop_find_staff($check_boss->parent_id);
        // }
    }






}
