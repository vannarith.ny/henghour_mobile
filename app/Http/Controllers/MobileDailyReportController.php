<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;
use App\Model\Pos;
use App\Model\Group;
use App\Model\PosGroup;
use App\Model\Report as Report;

class MobileDailyReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function index(Request $request){

        $stc_type = $request->type;
        if($stc_type == 2){
            $patID = 7;
            $displaylabel = 'kh';
        }else{
            $patID = 3;
            $displaylabel = 'vn';
        }
    	
    	$page = 'dailyreport'.$displaylabel;
    	// $stc_type = 1;

    	$sheets = DB::table('tbl_parameter_value')->where('pat_id',$patID)->get();
        $sheetsGet = DB::table('tbl_parameter_value')->where('pat_id',$patID)->orderby('pav_value','asc')->first();
        $sheet_id = $sheetsGet->pav_id;
        
        $userList = DB::table("tbl_staff")
            ->where('tbl_staff.parent_id',Session::get('iduserlotMobileSec'))
            ->orderBy('tbl_staff.s_id','asc')
            ->get()->toArray();

    	// $ThatTime ="16:40:00";
		// if (time() >= strtotime($ThatTime)) {
		//   $sheet_id = 6;
		// }

		// $sheetName =  DB::table('tbl_parameter_value')->where('pav_id',$sheet_id)->first();

		// $clientId = Session::get('iduserlotMobileSec');

		
        // dd($PostVote);
    	return view("mobile.dailyreport.index", compact('page','sheet_id','sheets', 'userList','stc_type'));
    }

    public function dailyReportGet(Request $request)
    {
        $userID = $request->userID;
        $date = $request->dateData;
        $sheetID = $request->sheetID;
        $stc_type = $request->stcType;

        $chilID = array();
        // check user parent
        $checkParent = DB::table('tbl_staff')->where('parent_id', $userID)->get();
        $userInfo = DB::table('tbl_staff')->where('s_id', $userID)->first();
        if($checkParent){
            array_push($chilID,$userID);
            foreach ($checkParent as $key => $parent) {
                // $chilID .= $parent->s_id.',';
                array_push($chilID,$parent->s_id);
            }
            // if($chilID != ''){
                // $chilID .= $userID;
                // $chilID = substr($chilID, 0, -1);
            // }
            
        }
        // if( count($chilID) > 0 ){
            // dd($chilID);
            $data = DB::table('tbl_sum_by_paper')
                ->leftjoin('tbl_parameter_value', 'tbl_sum_by_paper.sheet_id','=','tbl_parameter_value.pav_id')
                ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                ->whereIn('tbl_sum_by_paper.s_id', $chilID)
                ->where('tbl_sum_by_paper.date',$date)
                ->where('tbl_sum_by_paper.stc_type',$stc_type)
                ->groupBy('tbl_sum_by_paper.p_id')
                ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
                ->orderBy('tbl_parameter_value.pav_value', 'ASC')
                ->orderBy('tbl_sum_by_paper.page_value', 'ASC')
                ->get();

                // dd($data);
            return \Response::json(\View::make('mobile/dailyreport/boos',compact('data','userInfo','date','stc_type'))->render());

        // }else{
        //     $data = DB::table('tbl_sum_by_paper')
        //         ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
        //         ->where('tbl_sum_by_paper.s_id',$userID)
        //         ->where('tbl_sum_by_paper.date',$date)
        //         ->where('tbl_sum_by_paper.stc_type',1)
        //         ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
        //         ->orderBy('tbl_sum_by_paper.sheet_id', 'ASC')
        //         ->get();
        //     return \Response::json(\View::make('mobile/dailyreport/lastchild',compact('data','userInfo','date'))->render());
        // }

        
        
        
    }
}
