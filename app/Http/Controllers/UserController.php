<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }elseif (Session::get('roleLot') != 1) {
              Redirect::to('/sale')->send();
           }
            return $next($request);
        });
   
    }




	public function index()
    {
     	$page = 'user';
     	$users = DB::table('tbl_user')->orderBy('u_name','ASC')->get();
     	return view('user.index', compact('users','page'));
    }

    public function create(){
    	$page = 'add_user';
	    return view('user.create', compact('page'));
    }

    public function deleteItem(Request $request){
        $id = $request->id;
        $check = DB::table('tbl_paper')->where('u_id',$id)->first();
        if($check){
          return response(['msg' => "This user have process on other page can't delete", 'status' => 'error']); 
        }else{
          $checkUser = DB::table('tbl_user')->where('u_id', $id)->delete();
          if($checkUser){
            return response(['msg' => $id, 'status' => 'success']);  
          }else{
            return response(['msg' => '', 'status' => 'error']); 
          }
        }
        
    }


    public function store(Request $request)
    {
     	$u_name = $request->u_name;
	    $u_phone = $request->u_phone;
	    $u_line = $request->u_line;
	    $role = $request->role;
	    $u_username = $request->u_username;
        $u_password = $request->u_password;
        $u_unlock = $request->u_unlock;
        
        if($u_unlock == 'on'){
            $u_unlock = 1;
        }else{
            $u_unlock = 0;
        }

	     $check = DB::table('tbl_user')->where('u_username', $u_username)->first();
       $checkphone = DB::table('tbl_user')->where('u_phone', $u_phone)->first();
	     if ($check){
	        flash()->error("User ".$u_username." have already exit. please try other username");
	        return redirect('user/create')->withInput();
	     }
       else if ($checkphone){
          flash()->error("The phone number ".$u_phone." have already exit. please try other number");
          return redirect('user/create')->withInput();
       } else{

	      		$id_u = 1;
	      
	            $draft =  DB::table('tbl_user')->orderBy('u_id', 'DESC')->take(1)->get();
	            if ($draft){
	                foreach ($draft as $id) {
	                        $id_u = $id->u_id + 1;
	                }
	            }


	            $check = DB::table('tbl_user')->insert([
	                        'u_id' => $id_u,
	                        'u_name' => $u_name,
	                        'u_phone' => $u_phone,
	                        'u_line' => $u_line,
	                        'role' => $role,
	                        'u_username' => $u_username,
                            'u_password' => $u_password,
                            'u_unlock' => $u_unlock,
	                        ]
	                    ); 
	            if($check){
	                flash()->success(trans('message.add_success'));
	                return redirect('/user');
	            }else{
	               flash()->error(trans('message.add_error'));
	               return redirect('/user/create')->withInput(); 
	            }
	        }
    }


    public function deleteUserStaff(Request $request){
        $id = $request->id;
        

          
            $check = DB::table('tbl_user_staff')->where('us_id', $id)->delete();
            if($check){
              return response(['msg' => $id, 'status' => 'success']);  
            }else{
              return response(['msg' => '', 'status' => 'error']); 
            }

    }

    public function getUserStaff(Request $request){

      $id = $request->id;

      
      $staffCharge = DB::table('tbl_user_staff')->where('us_id',$id)->first();
      
      return response(['msg' => $staffCharge, 'status' => 'success']);

    }

    public function storeAddStaffToUser(Request $request){

        $s_id = $request->parent_id_main;
        $u_id = $request->user_id;
        $iddata = $request->iddata;
        $idDataTable = $request->idDataTable;
        $checkType = $request->checkType;

        $class = 'EditStaffCharge_main';
        $class1 = 'deleteStaffCharge_main';

        if($checkType == 'add'){

            $checkDate = DB::table('tbl_user_staff')->where('u_id','=',$u_id)
                                    ->where('s_id','=',$s_id)
                                    ->exists();
            if($checkDate){
                return response(['msg' => "កូនមានរួចហើយ", 'status' => 'error']);
            }
            

             $check = DB::table('tbl_user_staff')->insertGetId([
                                's_id' => $s_id,
                                'u_id' => $u_id
                                ]
                            );
            if($check){
                $staffCharge = DB::table('tbl_user_staff')
                                ->leftjoin('tbl_staff','tbl_user_staff.s_id','tbl_staff.s_id')
                                ->where('tbl_user_staff.us_id',$check)->first();

                $msg = '<tr class="staff_charge_main-'.$staffCharge->us_id.'">
                         <td>'.$idDataTable.'</td>
                         
                         <td>'.$staffCharge->s_name.'</td>
                         <td>
                            <button id="'.$staffCharge->us_id.'" class="padding-button '.$class.' btn btn-xs btn-primary">'.trans('label.edit').'</button>
                            <button id="'.$staffCharge->us_id.'" class="padding-button '.$class1.' btn btn-xs btn-danger">'.trans('label.delete').'</button>
                         </td>
                    </tr> ';
                return response(['msg' => $msg, 'status' => 'success']);
            }else{
                return response(['msg' => "ការរក្សាទុកកំនែប្រែមិនដំណើរការ", 'status' => 'error']);
            }
        }else{

            $checkDate = DB::table('tbl_user_staff')->where('s_id','=',$s_id)
                                        ->where('us_id','<>',$iddata)
                                        ->where('u_id','=',$u_id)
                                        ->exists();
            if($checkDate){
                return response(['msg' => "កូនមានរួចហើយ", 'status' => 'error']);
            }

//            $check = DB::table('tbl_paper')
//                ->where('s_id','=',$s_id)
//                ->where('p_date','>=',$stc_date)
//                ->first();
//
//            if($check){
//
//                return response(['msg' => 'លេខរៀងមួយនេះមិនអាចលុបបានទេ ព្រោះវាកំពុងដំណើរការ', 'status' => 'error']);
//            }

//            $check2 = DB::table('tbl_paper')
//                ->where('s_id','=',$s_id)
//                ->where('p_date','<=',$stc_date)
//                ->first();
//
//            $checkstffdate = StaffCharge::where('stc_date','>',$stc_date)
//                ->where('s_id','=',$s_id)
//                ->first();
//
//            if(isset($check2) && isset($checkstffdate)){
//
//                return response(['msg' => 'លេខរៀងមួយនេះមិនអាចលុបបានទេ ព្រោះវាកំពុងដំណើរការ', 'status' => 'error']);
//            }



            $check = DB::table('tbl_user_staff')->where('us_id',$iddata)->update([
                                    's_id' => $s_id,
                                    'u_id' => $u_id
                                    ]
                                );
            if($check){
                $staffCharge = DB::table('tbl_user_staff')
                              ->leftjoin('tbl_staff','tbl_user_staff.s_id','tbl_staff.s_id')
                              ->where('tbl_user_staff.us_id',$iddata)->first();
                $msg = '<td>'.$idDataTable.'</td>
                         <td>'.$staffCharge->s_name.'</td>
                         <td>
                            <button id="'.$staffCharge->us_id.'" class="padding-button '.$class.' btn btn-xs btn-primary">'.trans('label.edit').'</button>
                            <button id="'.$staffCharge->us_id.'" class="padding-button '.$class1.' btn btn-xs btn-danger">'.trans('label.delete').'</button>
                         </td>';
              return response(['msg' => $msg, 'status' => 'updatesuccess']);
            }else{
              return response(['msg' => "ទិន្នន័យមិនបានកែប្រែ", 'status' => 'error']);
            }
        }

    }



    public function edit($id)
    {
	    $page = 'user';  
	    $user = DB::table('tbl_user')->where('u_id',$id)->first();  
        $staffs = DB::table('tbl_staff')
                        ->pluck('s_name','s_id')->all();


        $user_staffs = DB::table('tbl_user_staff')
                        ->leftjoin('tbl_staff','tbl_user_staff.s_id','tbl_staff.s_id')
                        ->where('tbl_user_staff.u_id',$id)
                        ->orderBy('tbl_staff.s_name','ASC')
                        ->get();

	    return view('user.edit', compact('page','user','user_staffs','staffs','id'));
    }

    public function update(Request $request, $id){     
	  	$u_name = $request->u_name;
        $u_phone = $request->u_phone;
        $u_line = $request->u_line;
        $role = $request->role;
        $u_username = $request->u_username;
        $u_password = $request->new_password;
        $u_unlock = $request->u_unlock;

        if($u_unlock == 'on'){
            $u_unlock = 1;
        }else{
            $u_unlock = 0;
        }

        $check = DB::table('tbl_user')->where('u_username', $u_username)->where('u_id','<>', $id)->first();
        if ($check){
            flash()->error("Your username ".$u_username." have already exit. please try other user");
           
            return redirect('user/'.$id.'/edit');
        }else{
        	if($u_password == ''){
        		$check = DB::table('tbl_user')->where('u_id', $id)->update(
                        [
                          'u_name' => $u_name,
                          'u_phone' => $u_phone,
                          'u_line' => $u_line,
                          'role' => $role,
                          'u_username' => $u_username,
                          'u_unlock' => $u_unlock,
                        ]
                    );
        	}else{
        		$check = DB::table('tbl_user')->where('u_id', $id)->update(
                        [
                          'u_name' => $u_name,
                          'u_phone' => $u_phone,
                          'u_line' => $u_line,
                          'role' => $role,
                          'u_username' => $u_username,
                          'u_password' => $u_password,
                          'u_unlock' => $u_unlock,
                        ]
                    );
        	}
             
            if($check){
                flash()->success(trans('message.update_success'));
                return redirect('user/'.$id.'/edit');
            }else{
                flash()->error(trans('message.update_error'));
                return redirect('user/'.$id.'/edit')->withInput();
            }
        }
    }


}
