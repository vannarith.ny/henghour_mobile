<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileClientController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {

	 		  if(!Session::has('iduserlotMobileSec'))
		       {
		            Redirect::to('/')->send();
		       }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    // dd($check);
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    private function ganerateTimeClose(){

        // Pos Khmer
        $pos0 = DB::table('tbl_pos')->whereIn('pos_time',[36])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $pos1 = DB::table('tbl_pos')->whereIn('pos_time',[17])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $pos2 = DB::table('tbl_pos')->whereIn('pos_time',[12])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $pos3 = DB::table('tbl_pos')->whereIn('pos_time',[13])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $pos4 = DB::table('tbl_pos')->whereIn('pos_time',[24])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $pos5 = DB::table('tbl_pos')->whereIn('pos_time',[14])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();

        // Pos VN
        $posAFternoon = DB::table('tbl_pos')->whereIn('pos_time',[5])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $posEvening = DB::table('tbl_pos')->whereIn('pos_time',[6])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        // dd($posAFternoon);
        $daly = array(
                        '1' => 'ច័ន្ទ',
                        '2' => 'អង្គារ',
                        '3' => 'ពុធ',
                        '4' => 'ព្រហស្បត៍',
                        '5' => 'សុក្រ',
                        '6' => 'សៅរ៍',
                        '7' => 'អាទិត្យ' 
                        );
        // return $pos;

        // time close
        $timeDay = array();
        foreach ($posAFternoon as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:25";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:25";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:25";

                    }
                    
                }
            }else if($key == 4){ // F
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            }else if($key == 5){ // I
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            }else if($key == 6){ // II
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            }else if($key == 7){ // III
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            }else if($key == 8){ // IV
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            }else if($key == 9){ // K
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            }else if($key == 10){ // LO 23 + 15
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:13";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:14";

                    }
                    
                }
            
            }else if($key == 11){ // N
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:25";

                    }
                    
                }
            }else if($key == 12){ // P
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:25";

                    }
                    
                }
            }
            
        }

        // evening loop
        foreach ($posEvening as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:25";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:25";

                    }
                    
                }
            
            }else if($key == 1){ // A1 Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:19";

                    }
                    
                }
            }else if($key == 2){ // A2 Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:19";

                    }
                    
                }
            }else if($key == 3){ // A3 Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:19";

                    }
                    
                }
            }else if($key == 4){ // A4 Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:19";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:19";

                    }
                    
                }
            }else if($key == 5){ // B Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:20";

                    }
                    
                }
            }else if($key == 6){ // C Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:20";

                    }
                    
                }
            }else if($key == 7){ // D Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:20";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:20";

                    }
                    
                }
            }else if($key == 8){ // LO Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:10";

                    }
                    
                }
            
            }
            
        }


        // start pos kh
        foreach ($pos0 as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "08:42";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "08:42";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "08:42";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "08:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "08:42";

                    }
                    
                }
            
            }
            
        }

        foreach ($pos1 as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:42";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:42";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:42";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:42";

                    }
                    
                }
            
            }
            
        }

        foreach ($pos2 as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "12:57";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "12:57";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "12:57";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "12:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "12:57";

                    }
                    
                }
            
            }
            
        }

        foreach ($pos3 as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "15:42";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "15:42";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "15:42";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "15:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "15:42";

                    }
                    
                }
            
            }
            
        }

        foreach ($pos4 as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "17:57";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "17:57";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "17:57";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "17:57";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "17:57";

                    }
                    
                }
            
            }
            
        }

        foreach ($pos5 as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "19:42";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "19:42";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "19:42";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "19:42";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "19:42";

                    }
                    
                }
            
            }
            
        }

        return $timeDay;


    }


    public function index(){
    	$page = 'Client';
    	// $page = 'profit-loss';
    	$staff = DB::table("tbl_staff")->where('s_id',Session::get('iduserlotMobileSec'))->get();
    	// dd($staffid);
    	// $arr_staffid = array();
    	// foreach ($staffid as $s) {
    	// 	array_push($arr_staffid, $s->s_id);
    	// }
        // $posTime = $this->ganerateTimeClose();
        // foreach ($posTime as $key => $timeClose) {
        //     $data = explode("//",$key);
        //     // $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;

        //     foreach ($timeClose as $key => $value) {
        //         DB::table('tbl_time_close')
        //         ->insert([
        //             's_id' => 1,
        //             't_time' => $value,
        //             't_day' => $key,
        //             'p_id' => $data[0],
        //             'sheet_id' => $data[2]
        //             ]);
        //     }      
        // }
        // dd();
    	// dd($staffid);
    	return view("mobile.client.index", compact('page','staff'));
    }

    public function activeclient(Request $request){
        $userId = $request->userId;

        $check = DB::table('tbl_staff')
            ->where('s_id', $userId)
            ->first();
        if($check->Active == 1){
            DB::table('tbl_staff')
                ->where('s_id', $userId)
                ->update([
                        'Active' => 0
                        ]);
            return response(['msg' => 'រក្សាទុកបានជោគជ័យ', 's_id' => $userId, 'status' => 'success']);
        }else{
            DB::table('tbl_staff')
                ->where('s_id', $userId)
                ->update([
                        'Active' => 1
                        ]);
            return response(['msg' => 'រក្សាទុកបានជោគជ័យ', 's_id' => $userId, 'status' => 'success']);
        }
        



        
    }

    public function deleteuser(Request $request){

        $id = $request->userId;
        $check = DB::table("tbl_staff")->where('parent_id',$id)->first();

        if($check){

            return response(['msg' => 'ភ្នាក់ងារមិនអាចលុបបានទេពីព្រោះមានកូន',  'status' => 'error']); 
            
        }else{

            $control = DB::table("tbl_staff")->where('s_id',$id)->delete();
            if($control){

                DB::table("tbl_water")->where('s_id',$id)->delete();
                DB::table("tbl_staff_charge")->where('s_id',$id)->delete();
                DB::table("tbl_time_close")->where('s_id',$id)->delete();

                return response(['msg' => 'លុបភ្នាក់ងារបានសំរេច', 'status' => 'success']); 
            }else{
                return response(['msg' => "Can't delete this user , please contact admin",  'status' => 'error']); 
            }

            

        }

        
    }

    public function getwater(Request $request){

    	$water = DB::table("tbl_water")
                ->leftjoin('tbl_parameter_value','tbl_parameter_value.pav_id','tbl_water.time')
    			->where('s_id',Session::get('iduserlotMobileSec'))
    			->orderBy('tbl_parameter_value.pat_id','asc')
                ->orderBy('tbl_parameter_value.pav_value','asc')
    			->get()->toArray(); 

        $infoUser = DB::table("tbl_staff")->where('s_id',Session::get('iduserlotMobileSec'))->first();

    	return response(['msg' => $water, 'users' => $infoUser, 'water'=>$water, 'status' => 'success']);

    }

    public function createuserinfo(Request $request){

        $s_name = $request->s_name;
        $s_phone = $request->s_phone;
        $LimitMoneyAmount2R = $request->LimitMoneyAmount2R;
        $LimitMoneyAmount3R = $request->LimitMoneyAmount3R;
        $LimitMoneyAmount2D = $request->LimitMoneyAmount2D;
        $LimitMoneyAmount3D = $request->LimitMoneyAmount3D;
        $SCode = $request->SCode;
        $s_two_digit_charge = $request->s_two_digit_charge;
        $s_three_digit_charge = $request->s_three_digit_charge;

        $s_two_digit_paid = $request->s_two_digit_paid;
        $s_three_digit_paid = $request->s_three_digit_paid;

        $s_phone_login = $request->s_phone_login;
        $s_password = $request->s_password;

        $CanCreateChild = 0;
        if($request->CanCreateChild == 'true'){
            $CanCreateChild = 1;
        }

        $s_dollar = 0;
        if($request->s_dollar == 'true'){
            $s_dollar = 1;
        }

        $Active = 0;
        if($request->Active == 'true'){
            $Active = 1;
        }
        // dd($request->Active);
        
        $check = DB::table('tbl_staff')->where('s_phone_login', $s_phone_login)->first();

        if($check){
            return response(['msg' => 'លេខទូរស័ព្ទគណនីមានរួចហេីយ', 'status' => 'error']);
        }else{
            $id_user = DB::table('tbl_staff')
                ->insertGetId([
                    's_name' => $s_name,
                    's_phone' => $s_phone,
                    'IsLimitMoney2DR' => $LimitMoneyAmount2R,
                    'IsLimitMoney3DR' => $LimitMoneyAmount3R,
                    'IsLimitMoney2DD' => $LimitMoneyAmount2D,
                    'IsLimitMoney3DD' => $LimitMoneyAmount3D,
                    'SCode' => $SCode,
                    's_two_digit_charge' => $s_two_digit_charge,
                    's_three_digit_charge' => $s_three_digit_charge,
                    's_two_digit_paid' => $s_two_digit_paid,
                    's_three_digit_paid' => $s_three_digit_paid,
                    'CanCreateChild' => $CanCreateChild,
                    's_dollar' => $s_dollar,
                    's_phone_login' => $s_phone_login,
                    's_password' => $s_password,
                    'Active' => $Active,
                    's_role' => 1,
                    'parent_id' => Session::get('iduserlotMobileSec')
                    ]);

                // auto add to water with any user
                $waterCut = $s_two_digit_charge + 5;
                DB::table('tbl_water')
                ->insert([
                    's_id' => $id_user,
                    'w_2digit' => $waterCut,
                    'w_3digit' => $waterCut,
                    'time' => 5
                    ]);
                DB::table('tbl_water')
                ->insert([
                    's_id' => $id_user,
                    'w_2digit' => $waterCut,
                    'w_3digit' => $waterCut,
                    'time' => 6
                    ]);

                DB::table('tbl_water')
                ->insert([
                    's_id' => $id_user,
                    'w_2digit' => $waterCut,
                    'w_3digit' => $waterCut,
                    'time' => 23
                    ]);
                DB::table('tbl_water')
                ->insert([
                    's_id' => $id_user,
                    'w_2digit' => $waterCut,
                    'w_3digit' => $waterCut,
                    'time' => 24
                    ]);

                // add defult post
                    // loop afternoon
                    $groupNames = ["A","B","C","D","F","I","N","Lo(23+19)","Lo+FIN","Lo+F","4P","5P","6P","7P","BCD","BC","BD","CD","FIN","FI"];
                    $getPostAfternoon = DB::table('tbl_pos_group')
                                    ->leftjoin('tbl_group','tbl_pos_group.g_id','tbl_group.g_id')
                                    ->where("tbl_pos_group.sheet_id",5)
                                    ->whereIn("tbl_group.g_name",$groupNames)
                                    ->orderBy('tbl_group.g_name', 'ASC')
                                    ->groupBy('tbl_pos_group.g_id')
                                    ->get();
                    foreach ($getPostAfternoon as $key => $valuePost) {
                      DB::table('tbl_group_user_disable')->insert([
                          'g_id'  =>  $valuePost->g_id,
                          's_id' => $id_user
                          ]
                      );
                    }

                    // // loop Evening
                    $groupNames = ["Lo(32+25)"];
                    $getPostEvening = DB::table('tbl_pos_group')
                                    ->leftjoin('tbl_group','tbl_pos_group.g_id','tbl_group.g_id')
                                    ->where("tbl_pos_group.sheet_id",6)
                                    ->whereIn("tbl_group.g_name",$groupNames)
                                    ->orderBy('tbl_group.g_name', 'ASC')
                                    ->groupBy('tbl_pos_group.g_id')
                                    ->get();
                    foreach ($getPostEvening as $key => $valuePostEv) {
                      DB::table('tbl_group_user_disable')->insert([
                          'g_id'  =>  $valuePostEv->g_id,
                          's_id' => $id_user
                          ]
                      );
                    }

                // auto add to time close by user
                $posTime = $this->ganerateTimeClose();
                foreach ($posTime as $key => $timeClose) {
                    $data = explode("//",$key);
                    // $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;

                    foreach ($timeClose as $key => $value) {
                        DB::table('tbl_time_close')
                        ->insert([
                            's_id' => $id_user,
                            't_time' => $value,
                            't_day' => $key,
                            'p_id' => $data[0],
                            'sheet_id' => $data[2]
                            ]);
                    }      
                }
                


            return response(['msg' => 'រក្សាទុកបានជោគជ័យ', 'status' => 'success']);
        }

        

    }

    public function saveuserinfo(Request $request){
        $s_id = $request->s_id;
        $s_name = $request->s_name;
        $s_phone = $request->s_phone;
        $LimitMoneyAmount2R = $request->LimitMoneyAmount2R;
        $LimitMoneyAmount3R = $request->LimitMoneyAmount3R;
        $LimitMoneyAmount2D = $request->LimitMoneyAmount2D;
        $LimitMoneyAmount3D = $request->LimitMoneyAmount3D;
        $SCode = $request->SCode;
        $s_two_digit_charge = $request->s_two_digit_charge;
        $s_three_digit_charge = $request->s_three_digit_charge;

        $s_two_digit_paid = $request->s_two_digit_paid;
        $s_three_digit_paid = $request->s_three_digit_paid;

        $s_phone_login = $request->s_phone_login;
        $s_password = $request->s_password;

        $CanCreateChild = 0;
        if($request->CanCreateChild == 'true'){
            $CanCreateChild = 1;
        }

        $s_dollar = 0;
        if($request->s_dollar == 'true'){
            $s_dollar = 1;
        }

        $Active = 0;
        if($request->Active == 'true'){
            $Active = 1;
        }
        // dd($request->Active);
                
        $check = DB::table('tbl_staff')->where('s_phone_login', $s_phone_login)->where('s_id','<>', $s_id)->first();

        if($check){
            return response(['msg' => 'លេខទូរស័ព្ទគណនីមានរួចហេីយ', 'status' => 'error']);
        }else{
            DB::table('tbl_staff')
            ->where('s_id', $s_id)
            ->update([
                    's_name' => $s_name,
                    's_phone' => $s_phone,
                    'IsLimitMoney2DR' => $LimitMoneyAmount2R,
                    'IsLimitMoney3DR' => $LimitMoneyAmount3R,
                    'IsLimitMoney2DD' => $LimitMoneyAmount2D,
                    'IsLimitMoney3DD' => $LimitMoneyAmount3D,
                    'SCode' => $SCode,
                    's_two_digit_charge' => $s_two_digit_charge,
                    's_three_digit_charge' => $s_three_digit_charge,
                    's_two_digit_paid' => $s_two_digit_paid,
                    's_three_digit_paid' => $s_three_digit_paid,
                    'CanCreateChild' => $CanCreateChild,
                    's_dollar' => $s_dollar,
                    's_phone_login' => $s_phone_login,
                    's_password' => $s_password,
                    'Active' => $Active,
                    's_role' => 1,
                    ]);

            return response(['msg' => 'រក្សាទុកបានជោគជ័យ', 's_id' => $s_id, 'status' => 'success']);
        }

    }

    public function savewater(Request $request){
    	$voteLotteryWaters = $request->voteLotteryWaters;

        $isUseDollar = 0;
        if($request->isUseDollar == 'true'){
            $isUseDollar = 1;
        }

        $isPrintKatTek = 0;
        if($request->isPrintKatTek == 'true'){
            $isPrintKatTek = 1;
        }

        if(count($voteLotteryWaters) > 0){
            foreach ($voteLotteryWaters as $key => $value) {
                DB::table('tbl_water')
                ->where('w_id', $value['LotteryTimeId'])
                ->update([
                        'w_2digit' => $value['TwoDigit'],
                        'w_3digit' => $value['ThreeDigit']
                        ]);
            }
        }

        DB::table('tbl_staff')
                ->where('s_id', Session::get('iduserlotMobileSec'))
                ->update([
                        's_dollar' => $isUseDollar,
                        's_print_cut' => $isPrintKatTek
                        ]);

        return response(['msg' => 'រក្សាទុកបានជោគជ័យ', 's_id' => Session::get('iduserlotMobileSec'), 'status' => 'success']);

    }

    public function getlist(Request $request){
        $userList = DB::table("tbl_staff")

                ->where('tbl_staff.parent_id',Session::get('iduserlotMobileSec'))
                ->orderBy('tbl_staff.s_id','asc')
                ->get()->toArray(); 

        return response(['msg' => 'ok', 'Clients'=>$userList, 'status' => 'success']);
    }


}
