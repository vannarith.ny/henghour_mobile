<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

use App\Model\Report;

class ReportNumberController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }

    public function reportMain(Request $request)
    {
        $page = 'reportMain';
        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );

        $staff = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('tbl_staff.total_payment', 1)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->pluck('s_name','s_id')
            ->all();
        
        
       
        return view('reportnumber.reportMain', compact('page','staff'));
    }

    public function getpostBySheet(Request $request){

        $sheet = $request->sheet;

        
            
            $staff = DB::table('tbl_pos')
                ->where('pos_time',$sheet)
                ->orderBy('pos_name', 'ASC')
                ->get();
            
            $option = '<option value="" selected="selected">រើសប៉ុស</option>';
            foreach ($staff as $value) {
                $option .= '<option value="'.$value->pos_id.'">'.$value->pos_name.'</option>';
            }
        

        
        return response(['msg' => $option,'msg1' => '', 'status' => 'success']);  

    }


    public function getstaffbylotterytype(Request $request){
        $type = $request->type;

        if(Session::get('iduserlot') == 65481){

            $staff = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                ->where('tbl_staff_charge.stc_type',$type)
                ->where('tbl_staff.parent_id',761)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->get();

            $chilID = array(761,1049);
            $staffMain = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                ->where('tbl_staff_charge.stc_type',$type)
                ->whereIn('tbl_staff.s_id',$chilID)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->get();

            $option = '<option value="" selected="selected">រើសបុគ្គលិក</option>';
            foreach ($staff as $value) {
                $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
            }
        
        }elseif(Session::get('roleLot') == 3){
            $childs = DB::table('tbl_user_staffs')
                        ->where('u_id',Session::get('iduserlot'))
                        ->orderBy('s_id', 'ASC')
                        ->get();
            
            $option = '<option value="" selected="selected">រើសបុគ្គលិក</option>';
            foreach ($childs as $child) {
                
                $staff = DB::table('tbl_staff')
                    ->where('s_id',$child->s_id)
                    ->first();

                $option .= '<option value="'.$staff->s_id.'">'.$staff->s_name.'</option>';


                $checkMain = DB::table('tbl_staff')
                            ->where('parent_id', $staff->s_id)
                            ->get();

                if(count($checkMain) > 0){
                    foreach ($checkMain as $moreStaff) {
                        $option .= '<option value="'.$moreStaff->s_id.'">'.$moreStaff->s_name.'</option>';
                    }
                }else{
                    $moreStaffs = DB::table('tbl_staff_charge_main')
                            ->where('s_id', $staff->s_id)
                            ->get();
                    if($moreStaffs){
                        foreach ($moreStaffs as $moreStaff) {
                            $staffSub = DB::table('tbl_staff')
                                    ->where('s_id',$moreStaff->chail_id)
                                    ->first();
                            $option .= '<option value="'.$staffSub->s_id.'">'.$staffSub->s_name.'</option>';
                        }
                    }
                }

                

            }

            

            

        }else{
            
            $staff = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                ->where('tbl_staff_charge.stc_type',$type)
                ->where('tbl_staff.from_external',0)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->get();
            
            $option = '<option value="" selected="selected">រើសបុគ្គលិក</option>';
            foreach ($staff as $value) {
                $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
            }
        }

        

        if(isset($staffMain)){
            foreach ($staffMain as $value) {
                $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
            }
        }

        $times = $this->getTimeNew($type);
        $option1 = '<option value="" selected="selected">ជ្រើសរើសពេល</option>';
        foreach ($times as $value) {
            $option1 .= '<option value="'.$value->pav_id.'">'.$value->pav_value.'</option>';
        }


        return response(['msg' => $option,'msg1' => $option1, 'status' => 'success']);  
            
    }

    public function index(Request $request)
    {
        $page = 'reportnumber';
        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 => 'KH');

        // $staff = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name')
        //     ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
        //     ->orderBy('tbl_staff.s_id', 'ASC')
        //     ->groupBy('tbl_staff.s_id')
        //     ->pluck('s_name','s_id')
        //     ->all();
        if($request->staff != ''){
          $staff = $request->staff;
        }else{
          $staff = array();  
        }
        
        $pages = DB::table('tbl_paper_mobile')
            ->select('tbl_paper_mobile.p_number')
            ->groupBy('tbl_paper_mobile.p_number')
            ->orderBy('tbl_paper_mobile.p_number', 'ASC')
            ->pluck('p_number','p_number')
            ->all();

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();

        $posts = DB::table('tbl_pos')
                ->orderBy('pos_name', 'ASC')
                ->pluck('pos_name','pos_id')
                ->all();
        $posts = array();

        return view('reportnumber.index', compact('page','sheets','posts','staff','pages','type_lottery','var_type_lottery'));
    }

    public function filter(Request $request)
    {
        $page = 'reportnumber';
        $var_dateStart = $request->dateStart;
        $var_sheet = $request->sheet;
        $var_type_lottery = $request->type_lottery;
        $pos_id = $request->pos_id;

        // $priceChild = 0;

        // dd( $var_type_lottery);
        // dd($var_type_lottery);

        $type_lottery = array(1 => 'VN',2 => 'KH' );


        if($var_type_lottery == 1){
            $sheetCheck = 'sheet';
        }else{
            $sheetCheck = 'sheet_khmer';
        }
        

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=',$sheetCheck)
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();

        $posts = DB::table('tbl_pos')
                ->where('pos_time',$var_sheet)
                ->orderBy('pos_name', 'ASC')
                ->pluck('pos_name','pos_id')
                ->all();

        // dd(date("Y-m-d", strtotime($var_dateStart)));
                // dd($posts);
        // get paper not check
        $papers = DB::table('tbl_number_mobile')
                    ->select('tbl_number_mobile.num_id','tbl_number_mobile.num_number','tbl_number_mobile.num_end_number', 'tbl_number_mobile.num_sym','tbl_number_mobile.num_reverse','tbl_number_mobile.num_price','tbl_number_mobile.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row_mobile','tbl_number_mobile.r_id','=','tbl_row_mobile.r_id')
                    ->join('tbl_paper_mobile','tbl_paper_mobile.p_id','=','tbl_row_mobile.p_id')
                    ->join('tbl_pos_group','tbl_number_mobile.g_id','=','tbl_pos_group.g_id')
                    ->where('tbl_paper_mobile.p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('tbl_paper_mobile.p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number_mobile.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            // $data = array();
            // foreach ($result as $key => $pos) {
            //     foreach ($pos as $keyPos => $arrNum) {

            //     }
            // }


            $deleted = DB::table('tbl_smart')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number
                    // var_dump($arrNum);
                    foreach ($arrNum as $numValue => $num) {
                        
                        $check = DB::table('tbl_smart')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        // var_dump($num[0],$numValue,$check);
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // var_dump($num,'=>');
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else if($num[1] == 2){
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }
            // dd();

            // foreach ($result as $keyMain => $main) { //foreach pos
            //     foreach ($main as $key => $pos) { //foreach pos

            //         foreach ($pos as $keyPos => $num) { //foreach number
            //             $check = DB::table('tbl_smart')
            //                     ->where('sm_date',$var_dateStart)
            //                     ->where('p_id',$key)
            //                     ->where('sm_key',$keyPos)
            //                     ->first();

                        
                        
            //             if($check){

            //                 $newPriceR = $check->sm_value_r;
            //                 $newPriceD = $check->sm_value_d;
            //                 if($num[1] == 1){
            //                     $newPriceR = $check->sm_value_r + $num[0];
            //                 }else{
            //                     $newPriceD = $check->sm_value_d + $num[0];
            //                 }

            //                 $updated = DB::table('tbl_smart')->where('sm_id', $check->sm_id)->update([
            //                         'sm_value_r' => $newPriceR,
            //                         'sm_value_d' => $newPriceD,
            //                         ]);
            //                 if($updated){
            //                     $count++;
            //                 } 
                            
            //             }else{
            //                 // dd($num);
            //                 $newPriceR = 0;
            //                 $newPriceD = 0;
            //                 if($num[1] == 1){
            //                     $newPriceR = $num[0];
            //                 }else{
            //                     $newPriceD = $num[0];
            //                 }

            //                 $insertID = DB::table('tbl_smart')->insertGetId(
            //                     [
            //                         'digit' => strlen($keyPos),
            //                         'sm_key' => $keyPos,
            //                         'sm_value_r' => $newPriceR,
            //                         'sm_value_d' => $newPriceD,
            //                         'p_id' => $key,
            //                         'sm_date' => $var_dateStart

            //                     ]
            //                 );
            //                 if($insertID){
            //                     $count++;
            //                 } 

            //             }


            //         } //end foreach num
            //     } //end foreach pos
            // } //end foreach main
            // dd($result);
        }else{
            DB::table('tbl_smart')->delete();
        }
        // dd();
        // dd($result);

        $allData = DB::table('tbl_smart')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$pos_id)
                    ->where('sm_date',$var_dateStart)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
       
        // dd($posts);
         return view('reportnumber.index', compact('page','sheets','posts','var_dateStart','var_sheet','type_lottery','var_type_lottery','allData','pos_id'));
            

        
    }

    




    public function summary_lottery(Request $request)
    {

        $date = $request->date;
        $staff = $request->staff;
        $income_riel = $request->income_riel;
        $income_dollar = $request->income_dollar;
        $expense_riel = $request->expense_riel;
        $expense_dollar = $request->expense_dollar;
        $var_type_lottery = $request->var_type_lottery;
        $check = DB::table('tbl_summary_lottery')
            ->where('date',$date)
            ->where('s_id',$staff)
            ->where('stc_type',$var_type_lottery)
            ->first();
        if($check){
            DB::table('tbl_summary_lottery')
                ->where('date',$date)
                ->where('s_id',$staff)
                ->where('stc_type',$var_type_lottery)
                ->delete();

            DB::table('tbl_summary_lottery')->insert(
                [
                    'income_dollar' => $income_dollar,
                    'income_riel' => $income_riel,
                    'expense_dollar' => $expense_dollar,
                    'expense_riel' => $expense_riel,
                    'date' => $date,
                    's_id' => $staff,
                    'stc_type' => $var_type_lottery
                ]
            );

            return response(['msg' => 'update', 'status' => 'success']);

        }else{
            DB::table('tbl_summary_lottery')->insert(
                [
                    'income_dollar' => $income_dollar,
                    'income_riel' => $income_riel,
                    'expense_dollar' => $expense_dollar,
                    'expense_riel' => $expense_riel,
                    'date' => $date,
                    's_id' => $staff,
                    'stc_type' => $var_type_lottery

                ]
            );
            return response(['msg' => 'insert', 'status' => 'success']);
        }



    }

    public function summary_lottery_page(Request $request)
    {

        $date_new = $request->date_new;
        $type_lottery = $request->type_lottery;
        $staff_new = $request->staff_new;
        $row_data = $request->row_data;

        $page_sum = $request->total_page_sum;
        $start_num = $request->start_num;


        $check = DB::table('summary_lottery_page')
            ->where('type_lottery', $type_lottery)
            ->where('date_lottery',$date_new)
            ->where('staff_value', $staff_new)
            // ->where('page_value','<=', $page_sum)
            // ->where('page_value','>=', $start_num)
            ->first();
        if($check){

            DB::table('summary_lottery_page')
            ->where('type_lottery', $type_lottery)
            ->where('date_lottery',$date_new)
            ->where('staff_value', $staff_new)
            // ->where('page_value','<=', $page_sum)
            // ->where('page_value','>=', $start_num)
            ->delete();

            $row_page = explode(";", $row_data);
            foreach ($row_page as $key => $row) {
                if($row != ''){
                    $colum_data = explode(",", $row);

                    // DB::table('summary_lottery_page')->insert(
                    //     [
                    //         'type_lottery' => $colum_data[0],
                    //         'date_lottery' => $colum_data[1],
                    //         'staff_value' => $colum_data[3],
                    //         'id_staff' => $colum_data[2],
                    //         'time_value' => $colum_data[4],
                    //         'page_value' => $colum_data[5],
                    //         'total2digitr' => $colum_data[6],
                    //         'total3digitr' => $colum_data[7],
                    //         'totalsum' => $colum_data[8],
                    //         'total2digitrright' => $colum_data[9],
                    //         'total3digitrright' => $colum_data[10],
                    //         'total2digits' => $colum_data[11],
                    //         'total3digits' => $colum_data[12],
                    //         'totalsum_dolla' => $colum_data[13],
                    //         'total2digitsright' => $colum_data[14],
                    //         'total3digitsright' => $colum_data[15],
                    //         'page_code' => $colum_data[17]
                    //     ]
                    // );
                    DB::table('summary_lottery_page')->insert(
                        [
                            'type_lottery' => $type_lottery,
                            'date_lottery' => $date_new,
                            'staff_value' => $staff_new,
                            'id_staff' => $colum_data[0],
                            'time_value' => $colum_data[1],
                            'page_value' => $colum_data[2],
                            'total2digitr' => $colum_data[3],
                            'total3digitr' => $colum_data[4],
                            'totalsum' => $colum_data[5],
                            'total2digitrright' => $colum_data[6],
                            'total3digitrright' => $colum_data[7],
                            'total2digits' => $colum_data[8],
                            'total3digits' => $colum_data[9],
                            'totalsum_dolla' => $colum_data[10],
                            'total2digitsright' => $colum_data[11],
                            'total3digitsright' => $colum_data[12],
                            'page_code' => $colum_data[13]
                        ]
                    );
                }  

            } #end if
            

            // DB::table('summary_lottery_page')->insert(
            //     [
            //         'type_lottery' => $type_lottery,
            //         'date_lottery' => $date_note,
            //         'staff_value' => $staff_value,
            //         'id_staff' => $id_staff,
            //         'time_value' => $time_value,
            //         'page_value' => $page_value,
            //         'total2digitr' => $total2digitr,
            //         'total3digitr' => $total3digitr,
            //         'totalsum' => $totalsum,
            //         'total2digitrright' => $total2digitrright,
            //         'total3digitrright' => $total3digitrright,
            //         'total2digits' => $total2digits,
            //         'total3digits' => $total3digits,
            //         'totalsum_dolla' => $totalsum_dolla,
            //         'total2digitsright' => $total2digitsright,
            //         'total3digitsright' => $total3digitsright
            //     ]
            // );

            return response(['msg' => 'update', 'status' => 'success']);

        }else{
            $row_page = explode(";", $row_data);
            foreach ($row_page as $key => $row) {
                if($row != ''){
                    $colum_data = explode(",", $row);

                    DB::table('summary_lottery_page')->insert(
                        [
                            'type_lottery' => $type_lottery,
                            'date_lottery' => $date_new,
                            'staff_value' => $staff_new,
                            'id_staff' => $colum_data[0],
                            'time_value' => $colum_data[1],
                            'page_value' => $colum_data[2],
                            'total2digitr' => $colum_data[3],
                            'total3digitr' => $colum_data[4],
                            'totalsum' => $colum_data[5],
                            'total2digitrright' => $colum_data[6],
                            'total3digitrright' => $colum_data[7],
                            'total2digits' => $colum_data[8],
                            'total3digits' => $colum_data[9],
                            'totalsum_dolla' => $colum_data[10],
                            'total2digitsright' => $colum_data[11],
                            'total3digitsright' => $colum_data[12],
                            'page_code' => $colum_data[13]
                        ]
                    );
                }  

            } #end if
            return response(['msg' => 'insert', 'status' => 'success']);
        }



    }


    private function getTimeNew($type){
        if($type=='1'){
            $pat_key = "sheet";
        }elseif($type=='2'){
            $pat_key = "sheet_khmer";
        }elseif($type=='3'){
            $pat_key = "sheet_th";
        }elseif($type=='4'){
            $pat_key = "sheet_lo";
        }else{
            $pat_key = "sheet";
        }
        $result = DB::table('tbl_parameter_type')
            ->where('tbl_parameter_type.pat_key','=',$pat_key)
            ->join('tbl_parameter_value','tbl_parameter_type.pat_id','=','tbl_parameter_value.pat_id')
            ->get();
        return $result;
    }

    private function loop_find_staff($st_id){
        // var_dump($st_id);
        $get_val = '';

        $check_boss = DB::table('tbl_staff')
            ->where('parent_id','=',$st_id)
            ->get();
        // var_dump($check_boss);
        if($check_boss){
            // dd(count($check_boss));
            if(count($check_boss) >= 1){
                foreach ($check_boss as $key => $val){
                    $get_val .= "'".$val->s_id."',";
                }
                // dd('ok');
                return $get_val;
            }else{

                foreach ($check_boss as $key => $val){
                    return $this->loop_find_staff($val->s_id);
                }
            }
        }else{
            return '';
        }

        // foreach ($check_boss as $key => $val){
        //     if($val->parent_id != ''){
        // $get_val = $this->loop_find_staff($var_staff);
                // foreach ($new_boos as $key_sub => $val_sub){
                //     $get_val .= "'".$val_sub->s_id."',";
                // }
            // }else{
            //     $get_val .= "'".$val->s_id."',";
            // }

        // $check_boss = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name','tbl_staff.parent_id')
        //     ->where('tbl_staff.parent_id','=',$pairent)
        //     ->first();
        // if($check_boss){
        //     $this->loop_find_staff($check_boss->parent_id);
        // }else{
        //     $get_val .= "'".$check_boss->s_id."',";
        //     return $get_val;
        // }

        // $get_boss = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name','tbl_staff.parent_id')
        //     ->where('tbl_staff.parent_id','=',$pairent)
        //     ->get();

        // foreach ($new_boos as $key_sub => $val_sub){
        //             $get_val .= "'".$val_sub->s_id."',";
        // }
        // dd($check_boss);
        // if($check_boss->parent_id == ''){
            
        // }else{
            // $this->loop_find_staff($check_boss->parent_id);
        // }
    }
}
