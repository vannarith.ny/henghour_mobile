<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

use App\Model\Staff;
use App\Model\StaffTransaction;
use App\Model\SummaryLottery;

class MobileDashboardController extends Controller
{
    public function __construct()
    {
      $this->middleware(function ($request, $next) {

        if(!Session::has('iduserlotMobileSec'))
           {
                Redirect::to('/')->send();
           }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }
    public function index(){
    	$page = 'dashboard';
    	// $page = 'profit-loss';
    	$staffid = DB::table("tbl_staff")->where('s_id',Session::get('iduserlotMobileSec'))->get();
    	// dd($staffid);
    	// $arr_staffid = array();
    	// foreach ($staffid as $s) {
    	// 	array_push($arr_staffid, $s->s_id);
    	// }

    	dd($staffid);
    	return view("mobile.dashboard.index", compact('page'));
    }
}
