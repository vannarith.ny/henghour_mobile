<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileTimeCloseController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function index(){
    	$page = 'time_close';
    	// $page = 'profit-loss';
    	$daly = array(
                        '1' => 'ច័ន្ទ',
                        '2' => 'អង្គារ',
                        '3' => 'ពុធ',
                        '4' => 'ព្រហស្បត៍',
                        '5' => 'សុក្រ',
                        '6' => 'សៅរ៍',
                        '7' => 'អាទិត្យ' 
                        );
    	$times = DB::table("tbl_time_close")
    			 ->join('tbl_pos','tbl_time_close.p_id','tbl_pos.pos_id')
    			 ->where('s_id',Session::get('iduserlotMobileSec'))->get();
    	$currentDay = date("N");
    	$currentDay = (int)$currentDay-1;
    	// $queryAfter = $times->where('sheet_id',5)->groupBy('p_id');
    	// dd();
    	return view("mobile.timeclose.index", compact('page','times','daly','currentDay'));
    }
}
