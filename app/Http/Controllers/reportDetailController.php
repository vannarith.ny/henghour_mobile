<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class reportDetailController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }

    public function index(Request $request)
    {
        $page = 'reportdetail';
        
        // $block = DB::table('tbl_block')
        //     ->orderBy('name', 'ASC')
        //     ->pluck('name','id')
        //     ->all();

        // dd(Session::get('roleLot') );
        $staffMain = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('parent_id', 0)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
            $staffMain['all'] = 'All';

        $chilStaffs = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('parent_id','<>', 0)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();

        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ',5 =>'Hoso',6 =>'Minhgoc',7 =>'Thinhnam' );
        // dd($chilStaffs);

        return view('reportdetail.index', compact('page','staffMain','type_lottery','var_type_lottery','chilStaffs'));
    }

    public function filterMain(Request $request)
    {
        $page = 'reportdetail';
        
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_staff = $request->s_id_man;
        $var_staff_chail = $request->s_id;


        if($var_staff == '' && $var_staff_chail == ''){
            
            $staffMain = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('parent_id', 0)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();
                $staffMain['all'] = 'All';

            $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('parent_id','<>', 0)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();

            $var_type_lottery = null;

            $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ',5 =>'Hoso',6 =>'Minhgoc',7 =>'Thinhnam' );
            // dd($chilStaffs);

            return view('reportdetail.index', compact('page','staffMain','type_lottery','var_type_lottery','chilStaffs'));


        }else{

            $staffMain = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('parent_id', 0)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();


            $staffMain['all'] = 'All';

            // dd(Session::get('roleLot'));

            if($var_staff == 'all'){
                $chilStaffs[''] = 'កូនក្រុម';

                $mainNameStaffInfo = [];


            }else if($var_staff != '' && $var_staff_chail == ''){

                $chilStaffs = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->where('parent_id', $var_staff)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->pluck('s_name','s_id')
                    ->all();

                $chilStaffsData = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->where('parent_id', $var_staff)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->pluck('s_name','s_id')
                    ->all();

                $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff)
                        ->first();

            }else if($var_staff != '' && $var_staff_chail != ''){

                $chilStaffs = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->where('parent_id', $var_staff)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->pluck('s_name','s_id')
                    ->all();

                $chilStaffsData = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->where('s_id', $var_staff_chail)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->pluck('s_name','s_id')
                    ->all();
                    // dd($var_staff,'first');

                $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff_chail)
                        ->first();

            }else if($var_staff == '' && $var_staff_chail != ''){

                $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('parent_id','<>', 0)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();

                $chilStaffsData = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->where('s_id', $var_staff_chail)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->pluck('s_name','s_id')
                    ->all();
                    // dd($var_staff,'first');

                $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff_chail)
                        ->first();
            
            }else{

                $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('parent_id','<>', 0)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();
                // dd($chilStaffs,'ok');

                $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff)
                        ->first();
                
            }

            
            if($var_staff == 'all'){



                $reports = DB::table('tbl_sum_by_paper')
                            ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                            ->where('tbl_sum_by_paper.date','=',$var_dateStart)
                            ->orderBy('tbl_sum_by_paper.date', 'ASC')
                            // ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();

                return view('reportdetail.index', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports'));

            }else if($var_dateEnd != '' && $var_dateEnd > $var_dateStart){

                $reports = DB::table('tbl_sum_by_paper')
                        ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                        ->where('tbl_sum_by_paper.s_id', $var_staff_chail)
                        ->where('tbl_sum_by_paper.date','>=',$var_dateStart)
                        ->where('tbl_sum_by_paper.date','<=',$var_dateEnd)
                        ->orderBy('tbl_sum_by_paper.date', 'ASC')
                        // ->orderBy('tbl_sum_by_paper.p_id', 'ASC')
                        ->orderBy('tbl_staff.s_name', 'asc')
                        ->orderByRaw('LENGTH(tbl_staff.s_name)')
                        ->get();

                return view('reportdetail.index_enddate', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports'));

            }else{

                $chilID = array();
                if($chilStaffsData){
                    
                    // array_push($chilID,$var_staff_chail);
                    foreach ($chilStaffsData as $key => $value) {
                        array_push($chilID,$key);
                    }
                }
                $reports = DB::table('tbl_sum_by_paper')
                            ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                            ->where('tbl_sum_by_paper.date','=',$var_dateStart)
                            ->whereIn('tbl_sum_by_paper.s_id', $chilID)
                            ->orderBy('tbl_sum_by_paper.date', 'ASC')
                            // ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();

                return view('reportdetail.index', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports'));

            }
                

                

                // return view('reportmain.index_all_staff_oneday', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports'));
        }
        
        

        
        // dd($mainNameStaffInfo);
                    
        
        
        
    }
}
