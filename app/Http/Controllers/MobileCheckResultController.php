<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;
use App\Model\Pos;
use App\Model\Group;
use App\Model\PosGroup;
use App\Model\Report as Report;

class MobileCheckResultController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
    }

    public function payVote(Request $request)
    {
        $id = $request->idLot;
        $check = DB::table('tbl_number_mobile')
                    ->where('num_id',$id)
                    ->update([
                        'f_pay' => 1
                    ]);
        if($check){
            return response([
                'msg' => 'ok',
                'status' => 'success'
            ]);
        }else{
            return response([
                'msg' => 'error',
                'status' => 'error'
            ]);
        }
        
    }

    public function index(Request $request){
    	
    	$page = 'checkresult';
    	$stc_type = $request->type;
        $idType = $stc_type;
        // dd($stc_type);

        if($stc_type == 2){
            $patID = 7;
        }else{
            $patID = 3;
        }

    	// $sheets = DB::table('tbl_parameter_value')->where('pat_id',$patID)->get();

        $sheets = DB::table('tbl_parameter_value')->where('pat_id',$patID)->orderby('pav_value','asc')->get();
        $sheetsGet = DB::table('tbl_parameter_value')->where('pat_id',$patID)->orderby('pav_value','asc')->first();
    	$sheet_id = $sheetsGet->pav_id;

  //   	$ThatTime ="16:40:00";
		// if (time() >= strtotime($ThatTime)) {
		//   $sheet_id = 6;
		// }
        if($stc_type == 2){

            if (time() <= strtotime('08:45:00')) {
                $sheet_id = 36;
            }elseif (time() <= strtotime('10:30:00')) {
                $sheet_id = 17;
            }elseif (time() <= strtotime('13:00:00')) {
                $sheet_id = 12;
            }elseif (time() <= strtotime('15:45:00')) {
                $sheet_id = 13;
            }elseif (time() <= strtotime('18:00:00')) {
                $sheet_id = 24;
            }else{
                $sheet_id = 14;
            }

        }else{

            if (time() <= strtotime('16:40:00')) {
                $sheet_id = 5;
            }else{
                $sheet_id = 6;
            }
        }

		$sheetName =  DB::table('tbl_parameter_value')->where('pav_id',$sheet_id)->first();

		$clientId = Session::get('iduserlotMobileSec');

		

    	
        // dd($PostVote);
    	return view("mobile.checkresult.index", compact('page','sheets','sheetName','sheet_id','clientId','stc_type','idType'));
    }
}
