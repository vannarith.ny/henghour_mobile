<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

use App\Model\Staff;
use App\Model\StaffTransaction;
use App\Model\SummaryLottery;

class DashboardController extends Controller
{
        public function __construct()
    {
        $this->middleware(function ($request, $next) {
                          if(!Session::has('iduserlot'))
                       {
                            Redirect::to('/login')->send();
                       
                       }
            return $next($request);
        });
   
    }
    public function index(){
        $page = 'dashboard';
        // $page = 'profit-loss';
        $staffid = DB::table("tbl_staff_transction")->get();
        $arr_staffid = array();
        foreach ($staffid as $s) {
                array_push($arr_staffid, $s->s_id);
        }



        $page = 'Sary';
        $date = date("Y-m-d");

        if(isset($_GET['date'])){
                $date = $_GET['date'];
        }

        $dateVar = $date;
        $idPosy = [41,42,43,44,51,52,53,54];
        $resultsMorning = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $date)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        if(count($resultsMorning) == 0){
                $date = date("Y-m-d",strtotime("-1 days"));
                $resultsMorning = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $date)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();

        }
        // dd($resultsMorning);


        $idPosy = [1,2,3,4,5,6,7,33];
        $resultsAfternoon = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $date)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        if(count($resultsAfternoon) == 0){
                $date = date("Y-m-d",strtotime("-1 days"));
                $resultsAfternoon = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $date)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();

        }
        // dd($resultsAfternoon);

        $idPosyEvening = [10,11,12,13];
        $resultsEvening = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $date)
                ->whereIn('tbl_pos.pos_id',$idPosyEvening)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                ->get();

         // dd($resultsEvening);
        $saryAfternoon =  DB::table('tbl_sary')->where('date_sary',$date)->where('type',18)->first();
        $saryEvening =  DB::table('tbl_sary')->where('date_sary',$date)->where('type',5)->first();
        $saryNight =  DB::table('tbl_sary')->where('date_sary',$date)->where('type',6)->first();
        // dd($sary);
        

        return view("dashboard.index", compact('page','resultsMorning','resultsAfternoon','resultsEvening','saryAfternoon','saryEvening','saryNight','date','dateVar'));
        // return view("dashboard.index", compact('page','summaryDatas','tr','sumary_lottery','sumary_lottery_kh','totalGainsAndLosses_dollar','totalGainsAndLosses_riel'));
        // return view("dashboard.index", compact('page'));
    }
    public function getBalance($income,$expense){
        $result = 0;
        $balanceIncome = explode(" ", $income);
                $balanceExpence = explode(" ", $expense);
                $result = $balanceIncome[0] - $balanceExpence[0];
                return $result;
    }
    public function replaceCurency($str){
        $str = str_replace('R', '', $str);
        $getstr = str_replace('$', '', $str);
        return $getstr;

    }
    public function currencyFormat($val){
         $val = explode(" ", $val);
        $getval = number_format($val[0]).' '.$val[1];
        return $getval;
    }

    public function editlogout(){

                if(Session::has('iduserlot')){
                   Session::forget('iduserlot');
                   Session::forget('usernameLot');
                   Session::forget('nameLot');
                   Session::forget('phoneLot');
                }

                return redirect('/login');
        }
}
