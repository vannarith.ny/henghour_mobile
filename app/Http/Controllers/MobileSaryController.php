<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileSaryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function index(){
    	$page = 'sary';

        $date = date('Y-m-d');
        // $date = '2019-05-04';
        // dd($date);

        

    	// $times = DB::table("tbl_time_close")
    	// 		 ->join('tbl_pos','tbl_time_close.p_id','tbl_pos.pos_id')
    	// 		 ->where('s_id',Session::get('iduserlotMobileSec'))->get();
    	// $currentDay = date("N");
    	// $currentDay = (int)$currentDay-1;
    	// $queryAfter = $times->where('sheet_id',5)->groupBy('p_id');
    	// dd();
    	return view("mobile.sary.index", compact('page','date'));
    }

    public function printelarger(Request $request){
        $page = 'sary';
        $type = $request->type;
        
        return view("mobile.sary.printelarger", compact('page','type'));
    }

    public function printsary(Request $request){
        $page = 'sary';

        $date = $request->dateprint;
        $time = $request->time;
        $sary =  DB::table('tbl_sary')->where('date_sary',$date)->where('type',$time)->first();

        if($time == 36){

            $idPosy = [157,158,159,160];
            $resultsKH = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();
            // dd($resultsmorningv1);
        }else if($time == 17){

            $idPosy = [35,36,37,38];
            $resultsKH = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();

        }else if($time == 12){

            $idPosy = [15,16,17,18];
            $resultsKH = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();

        }else if($time == 13){

            $idPosy = [23,24,25,26];
            $resultsKH = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();

        }else if($time == 24){

            $idPosy = [47,48,49,50];
            $resultsKH = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();

        }else if($time == 14){

            $idPosy = [19,20,21,22];
            $resultsKH = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();
        }else{
            $resultsKH = [];
        }

        

        if($time == 5){

            $idPosy = [1,2,3,4,5,6,7];
            $resultsAfternoon = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();
            // dd($resultsAfternoon);
        }else{
            $resultsAfternoon = [];
        }

        if($time == 6){

            $idPosyEvening = [10,11,12,13];
            $resultsEvening = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosyEvening)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    ->get();

        }else{
            $resultsEvening = [];
        }


        
        // dd($resultsmorningv1);
        return view("mobile.sary.print", compact('page','resultsAfternoon','resultsEvening','resultsKH','sary','time','date'));


    }

    public function GetResultSary(Request $request){
        $date = $request->date;

        $newDate = date("Y-m-d", strtotime($date));

        
     //    if(count($sary) == 0){
     //        $newDate = date('Y-m-d',strtotime($date . "-1 days"));
     //        $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->get();
     //    }

        // dd($newDate);
        // VN Afternoon
        $idPosy = [1,2,3,4,5,6,7];
        $resultsAfternoon = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsAfternoon);

        // VN Evening
        $idPosyEvening = [10,11,12,13];
        $resultsEvening = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosyEvening)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                ->get();

        // dd($resultsEvening);


        // sary afternoon
        $htmlAfternoon = '';

        if(isset($resultsAfternoon) and count($resultsAfternoon) > 0 ){

            $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 5)->first();

            // dd($sary);
            $htmlAfternoon .= '<div class="width200 text-center mb-2 ">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/5\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="2">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនល្ងាច</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <b style="color: red; font-size: 14px;">04:30 AM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[0]->re_num_result.'</b><br>
                         <b>'.$resultsAfternoon[1]->re_num_result.'</b>
                    </td>
                </tr>';

            if(isset($sary)){
    
                $data = $sary->info;
                $resultLot = explode(",", $data);
            

                $htmlAfternoon .='<tr>
                        <td>
                             <b>Giải sáu</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[0].'</b><br>
                             <b>'.$resultLot[1].'</b><br>
                             <b>'.$resultLot[2].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải năm</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[3].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải tư</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[4].'</b><br>
                             <b>'.$resultLot[5].'</b><br>
                             <b>'.$resultLot[6].'</b><br>
                             <b>'.$resultLot[7].'</b><br>
                             <b>'.$resultLot[8].'</b><br>
                             <b>'.$resultLot[9].'</b><br>
                             <b>'.$resultLot[10].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải ba</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[11].'</b><br>
                             <b>'.$resultLot[12].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải nhì</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[13].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải nhất</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[14].'</b>
                        </td>
                    </tr>';
            }

            $htmlAfternoon .='<tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[2]->re_num_result.'</b> - <b>'.$resultsAfternoon[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[4]->re_num_result.'</b> - <b>'.$resultsAfternoon[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[6]->re_num_result.'</b> - <b>'.$resultsAfternoon[7]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>F</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[8]->re_num_result.'</b> - <b>'.$resultsAfternoon[9]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>I</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[10]->re_num_result.'</b> - <b>'.$resultsAfternoon[11]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>N</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[12]->re_num_result.'</b> - <b>'.$resultsAfternoon[13]->re_num_result.'</b>
                    </td>
                </tr>';

           $htmlAfternoon .= ' </table>';

        }

        // sary evening
        $htmlEvening = '';
        if(isset($resultsEvening) and count($resultsEvening) > 0 ){
            $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 6)->first();

            // dd($sary);
            $htmlEvening .= '<div class="width200 text-center mb-2">
                    <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/6\')">Print Sary</button>
                </div>
                <table cellpadding="0" cellspacing="0" class="tableSrey">
                    <tr>
                        <td colspan="3">
                            <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនយប់</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Date : '.$newDate.'</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                             <b style="color: red; font-size: 14px;">6:30 PM</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 20px !important;">
                             <b>A</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[0]->re_num_result.'</b><br>
                             <b>'.$resultsEvening[1]->re_num_result.'</b><br>
                             <b>'.$resultsEvening[2]->re_num_result.'</b><br>
                             <b>'.$resultsEvening[3]->re_num_result.'</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[4]->re_num_result.'</b><br>
                             <b>'.$resultsEvening[5]->re_num_result.'</b><br>
                             <b>'.$resultsEvening[6]->re_num_result.'</b>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-size: 20px !important;">
                             <b>B</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[7]->re_num_result.'</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[8]->re_num_result.'</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 20px !important;">
                             <b>C</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[9]->re_num_result.'</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[10]->re_num_result.'</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 20px !important;">
                             <b>D</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[11]->re_num_result.'</b>
                        </td>
                        <td style="text-align: center; font-size: 20px !important;">
                             <b>'.$resultsEvening[12]->re_num_result.'</b>
                        </td>
                    </tr>';

            if(isset($sary)){
    
                $data = $sary->info;
                $resultLot = explode(",", $data);
            

                $htmlEvening .='<tr>
                        <td>
                             <b>Giải sáu</b>
                        </td>
                        <td style="text-align: right;" colspan="2">
                             <b>'.$resultLot[0].'</b><br>
                             <b>'.$resultLot[1].'</b><br>
                             <b>'.$resultLot[2].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải năm</b>
                        </td>
                        <td style="text-align: right;" colspan="2">
                             <b>'.$resultLot[3].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải tư</b>
                        </td>
                        <td style="text-align: right;" colspan="2">
                             <b>'.$resultLot[4].'</b><br>
                             <b>'.$resultLot[5].'</b><br>
                             <b>'.$resultLot[6].'</b><br>
                             <b>'.$resultLot[7].'</b><br>
                             <b>'.$resultLot[8].'</b><br>
                             <b>'.$resultLot[9].'</b><br>
                             <b>'.$resultLot[10].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải ba</b>
                        </td>
                        <td style="text-align: right;" colspan="2">
                             <b>'.$resultLot[11].'</b><br>
                             <b>'.$resultLot[12].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải nhì</b>
                        </td>
                        <td style="text-align: right;" colspan="2">
                             <b>'.$resultLot[13].'</b><br>
                             <b>'.$resultLot[14].'</b><br>
                             <b>'.$resultLot[15].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Giải nhất</b>
                        </td>
                        <td style="text-align: right;" colspan="2">
                            <b>'.$resultLot[16].'</b><br>
                            <b>'.$resultLot[17].'</b><br>
                            <b>'.$resultLot[18].'</b>
                        </td>
                    </tr>';
            }

            // $htmlEvening .='<tr>
            //         <td style="font-size: 20px !important;">
            //              <b>B</b>
            //         </td>
            //         <td style="text-align: right; font-size: 20px !important;">
            //              <b>'.$resultsEvening[2]->re_num_result.'</b> - <b>'.$resultsEvening[3]->re_num_result.'</b>
            //         </td>
            //     </tr>
            //     <tr>
            //         <td style="font-size: 20px !important;">
            //              <b>C</b>
            //         </td>
            //         <td style="text-align: right; font-size: 20px !important;">
            //              <b>'.$resultsEvening[4]->re_num_result.'</b> - <b>'.$resultsEvening[5]->re_num_result.'</b>
            //         </td>
            //     </tr>
            //     <tr>
            //         <td style="font-size: 20px !important;">
            //              <b>D</b>
            //         </td>
            //         <td style="text-align: right; font-size: 20px !important;">
            //              <b>'.$resultsEvening[6]->re_num_result.'</b> - <b>'.$resultsEvening[7]->re_num_result.'</b>
            //         </td>
            //     </tr>
                
            //     </tr>';

           $htmlEvening .= ' </table>';
        }




        // end block VN


        // KH 0ព្រលឹម 8:45
        $idPosy = [157,158,159,160];
        $resultsKH1 = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsKH1);

        // sary morningv1
        $htmlKH1 = '';

        if(isset($resultsKH1) and count($resultsKH1) > 0 ){

            // $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 23)->first();
            // dd($sary);
            $htmlKH1 .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/36\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="3">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតខ្មែរ ព្រលឹម</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                         <b style="color: red; font-size: 14px;">08:45 AM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[0]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[1]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[2]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[4]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[6]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH1[7]->re_num_result.'</b>
                    </td>
                </tr>';

            



           $htmlKH1 .= ' </table>';

        }

        // KH 1ព្រឹក 10:35
        $idPosy = [35,36,37,38];
        $resultsKH2 = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsKH1);

        // sary morningv1
        $htmlKH2 = '';

        if(isset($resultsKH2) and count($resultsKH2) > 0 ){

            // $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 23)->first();
            // dd($sary);
            $htmlKH2 .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/17\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="3">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតខ្មែរ ព្រឹក</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                         <b style="color: red; font-size: 14px;">10:35 AM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[0]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[1]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[2]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[4]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[6]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH2[7]->re_num_result.'</b>
                    </td>
                </tr>';

            



           $htmlKH2 .= ' </table>';

        }

        // KH 2ថ្ងៃ 1:00
        $idPosy = [15,16,17,18];
        $resultsKH3 = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsKH1);

        // sary morningv1
        $htmlKH3 = '';

        if(isset($resultsKH3) and count($resultsKH3) > 0 ){

            // $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 23)->first();
            // dd($sary);
            $htmlKH3 .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/12\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="3">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតខ្មែរ ថ្ងៃ</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                         <b style="color: red; font-size: 14px;">01:00 PM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[0]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[1]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[2]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[4]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[6]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH3[7]->re_num_result.'</b>
                    </td>
                </tr>';

            



           $htmlKH3 .= ' </table>';

        }

        // KH 3រសៀល 3:45
        $idPosy = [23,24,25,26];
        $resultsKH4 = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsKH1);

        // sary morningv1
        $htmlKH4 = '';

        if(isset($resultsKH4) and count($resultsKH4) > 0 ){

            // $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 23)->first();
            // dd($sary);
            $htmlKH4 .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/13\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="3">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតខ្មែរ រសៀល</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                         <b style="color: red; font-size: 14px;">03:45 PM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[0]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[1]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[2]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[4]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[6]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH4[7]->re_num_result.'</b>
                    </td>
                </tr>';

            



           $htmlKH4 .= ' </table>';

        }


        // KH 4ល្ងាច 6:00
        $idPosy = [47,48,49,50];
        $resultsKH5 = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsKH1);

        // sary morningv1
        $htmlKH5 = '';

        if(isset($resultsKH5) and count($resultsKH5) > 0 ){

            // $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 23)->first();
            // dd($sary);
            $htmlKH5 .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/24\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="3">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតខ្មែរ ល្ងាច</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                         <b style="color: red; font-size: 14px;">06:00 PM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[0]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[1]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[2]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[4]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[6]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH5[7]->re_num_result.'</b>
                    </td>
                </tr>';

            



           $htmlKH5 .= ' </table>';

        }


        // KH 5យប់ 7:45
        $idPosy = [19,20,21,22];
        $resultsKH6 = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsKH1);

        // sary morningv1
        $htmlKH6 = '';

        if(isset($resultsKH6) and count($resultsKH6) > 0 ){

            // $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 23)->first();
            // dd($sary);
            $htmlKH6 .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/14\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="3">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតខ្មែរ យប់</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                         <b style="color: red; font-size: 14px;">06:00 PM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[0]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[1]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[2]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[4]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[6]->re_num_result.'</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsKH6[7]->re_num_result.'</b>
                    </td>
                </tr>';

            



           $htmlKH6 .= ' </table>';

        }

        


        


        return response(['msg' => 'insert','htmlKH1' => $htmlKH1,'htmlKH2' => $htmlKH2,'htmlKH3' => $htmlKH3,'htmlKH4' => $htmlKH4,'htmlKH5' => $htmlKH5,'htmlKH6' => $htmlKH6, 'htmlAfternoon' => $htmlAfternoon, 'htmlEvening' => $htmlEvening, 'date' => $newDate, 'status' => 'success']);
    }
}
