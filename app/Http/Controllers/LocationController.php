<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class LocationController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }elseif (Session::get('roleLot') != 1) {
		       		Redirect::to('/sale')->send();
		       }
            return $next($request);
        });
   
    }

    public function getCharge(Request $request){
    	$id = $request->id;

      
        $staffCharge = DB::table('tbl_block_staff')->where('id',$id)->first();
      
    	
    	return response(['msg' => $staffCharge, 'status' => 'success']);
    }

    public function storestafflocation(Request $request){
      $s_id = $request->s_id;
      $location_id = $request->location_id;
      $iddata = $request->iddata;
      $idDataTable = $request->idDataTable;
      $checkType = $request->checkType;

       
        $class = 'EditStaffCharge_main';
        $class1 = 'deleteStaffCharge_main';


        if($checkType == 'add'){

            $checkDate = DB::table('tbl_block_staff')->where('b_id','=',$location_id)
                                    ->where('s_id','=',$s_id)
                                    ->exists();

            if($checkDate){
                return response(['msg' => "បុគ្គលិកមានរួចហេីយក្និងតំបន់នេះ", 'status' => 'error']);
            }
            // check id max
            $id_stc = 1;

            $draft =  DB::table('tbl_block_staff')->select('id')->orderBy('id', 'DESC')->take(1)->get();
            if ($draft){
                foreach ($draft as $id) {
                        $id_stc = $id->id + 1;
                }
            }
            // dd($id_stc);
             $check = DB::table('tbl_block_staff')->insert([
                                'id' => $id_stc,
                                's_id' => $s_id,
                                'b_id' => $location_id
                                ]
                            );
            if($check){
                $staffCharge = DB::table('tbl_block_staff')
                               ->leftjoin('tbl_staff','tbl_block_staff.s_id','tbl_staff.s_id')
                               ->where('tbl_block_staff.id',$id_stc)->first();

                $msg = '<tr class="staff_charge-'.$staffCharge->id.'">
                         <td>'.$staffCharge->id.'</td>
                         <td>'.$staffCharge->s_name.'</td>
                         <td>
                            <button id="'.$staffCharge->id.'" class="padding-button '.$class.' btn btn-xs btn-primary">'.trans('label.edit').'</button>
                            <button id="'.$staffCharge->id.'" class="padding-button '.$class1.' btn btn-xs btn-danger">'.trans('label.delete').'</button>
                         </td>
                    </tr> ';
                return response(['msg' => $msg, 'status' => 'success']);
            }else{
                return response(['msg' => "ការរក្សាទុកកំនែប្រែមិនដំណើរការ", 'status' => 'error']);
            }
        }else{

            $checkDate = DB::table('tbl_block_staff')->where('b_id','=',$location_id)
                                        ->where('s_id','=',$s_id)
                                        ->where('id','<>',$iddata)
                                        ->exists();
            if($checkDate){
                return response(['msg' => "បុគ្គលិកមានរួចហេីយក្និងតំបន់នេះ", 'status' => 'error']);
            }




            $check = DB::table('tbl_block_staff')->where('id',$iddata)->update([
                                    's_id' => $s_id
                                    ]
                                );
            if($check){
                // $staffCharge = DB::table('tbl_staff_charge')->where('stc_id',$iddata)->first();
                $staffCharge = DB::table('tbl_block_staff')
                               ->leftjoin('tbl_staff','tbl_block_staff.s_id','tbl_staff.s_id')
                               ->where('tbl_block_staff.id',$iddata)->first();
                $msg = '<td>'.$idDataTable.'</td>
                         <td>'.$staffCharge->s_name.'</td>
                         <td>
                            <button id="'.$staffCharge->id.'" class="padding-button '.$class.' btn btn-xs btn-primary">'.trans('label.edit').'</button>
                            <button id="'.$staffCharge->id.'" class="padding-button '.$class1.' btn btn-xs btn-danger">'.trans('label.delete').'</button>
                         </td>';
              return response(['msg' => $msg, 'status' => 'updatesuccess']);
            }else{
              return response(['msg' => "ទិន្នន័យមិនបានកែប្រែ", 'status' => 'error']);
            }
        }

    }

    public function deleteStaffCharge_main(Request $request){
        $id = $request->id;
        

            $check = DB::table('tbl_block_staff')->where('id', $id)->delete();
            if($check){
              return response(['msg' => $id, 'status' => 'success']);  
            }else{
              return response(['msg' => '', 'status' => 'error']); 
            }

    }





    public function index()
    {
     	$page = 'location';
         $locations = DB::table('tbl_block')
                    ->leftjoin('tbl_staff', 'tbl_block.s_id','=','tbl_staff.s_id')
                    ->orderBy('tbl_block.name','ASC')->get();
     	return view('location.index', compact('locations','page'));
    }
    public function create(){
        $page = 'add_location';
        $staffs = DB::table('tbl_staff')
                    ->where('s_role', 1)
                    ->where('Active', 0)
                    ->where('parent_id', 0)
                    ->pluck('s_name','s_id')->all();

	    return view('location.create', compact('page', 'staffs'));
    }

    public function store(Request $request)
    {
         $name = $request->name;
         $s_id = $request->s_id;
         $percent = $request->percent;
	    $rule = [
	    	'name' => 'required|unique:tbl_block'
	    ];

	    $messages = [
	    	'name.required' => 'សូមបញ្ចូលឈ្មោះ',
	    	'name.unique' => 'ឈ្មោះគណនី​ '.$name.' មានរួចហើយ។​ សូមបញ្ចូលឈ្មោះថ្មី'
	    ];

	    $validator = Validator::make($request->all(), $rule, $messages);

	    if ($validator->fails()) {
	        return redirect('location/create')->withInput($request->all())->withErrors($validator);
	    }else{

            $check = DB::table('tbl_block')->insertGetId([
                        'name' => $name,
                        's_id' => $s_id,
                        'percent' => $percent
                        ]
                    );

            if($check){
                flash()->success(trans('message.add_success'));
                return redirect('/location/'.$check.'/edit');
            }else{
             flash()->error(trans('message.add_error'));
               return redirect('/location/create')->withInput(); 
            }
	    }
    }

    public function edit($id)
    {
	    $page = 'location';
        
	   

	    $location = DB::table('tbl_block')
                        ->where('id',$id)
                        ->first();

        $staffMains = DB::table('tbl_staff')
                        ->where('s_role', 1)
                        ->where('Active', 0)
                        ->where('parent_id', 0)
                        ->pluck('s_name','s_id')->all();

     

        $staffs = DB::table('tbl_staff')
                        
                        ->pluck('s_name','s_id')->all();

        $staffs_location = DB::table('tbl_block_staff')
                        ->leftjoin('tbl_staff','tbl_block_staff.s_id','tbl_staff.s_id')
                        ->where('tbl_block_staff.b_id',$id)
                        ->orderBy('tbl_staff.s_name','ASC')
                        ->get();

	    return view('location.edit', compact('page','location','staffs','staffMains','staffs_location'));
    }

    public function update(Request $request, $id){     
        $name = $request->name;
        $s_id = $request->s_id;
        $percent = $request->percent;

	    $rule = [
	    	'name' => 'required|unique:tbl_block,name,'.$id.',id'
	    ];

	    $messages = [
	    	'name.required' => 'សូមបញ្ចូលឈ្មោះ',
	    	'name.unique' => 'ឈ្មោះគណនី​ '.$name.' មានរួចហើយ។​ សូមបញ្ចូលឈ្មោះថ្មី'
	    ];

	    $validator = Validator::make($request->all(), $rule, $messages);

	    if ($validator->fails()) {
	        return redirect('location/'.$id.'/edit')->withInput($request->all())->withErrors($validator);
	    }else{

    		$check = DB::table('tbl_block')->where('id', $id)->update(
                    [
                        'name' => $name,
                        's_id' => $s_id,
                        'percent' => $percent
                    ]
                );
             
            if($check){
                flash()->success("ទិន្នន័យត្រូវបានកែប្រែ");
                return redirect('location/'.$id.'/edit');
            }else{
             flash()->error("ទិន្នន័យមិនត្រូវបានកែប្រែ");
                return redirect('location/'.$id.'/edit')->withInput();
            }
        }
    }

    public function deleteItem(Request $request){
      	$id = $request->id;
      	$check = DB::table('tbl_block_staff')->where('b_id',$id)->first();
      	if($check){
      		return response(['msg' => 'តំបន់នេះមិនអាចលុបបានទេព្រោះកំពុងដំណើរការ', 'status' => 'error']); 
      	}else{
      		$check = DB::table('tbl_block')->where('id', $id)->delete();
	        if($check){
	          return response(['msg' => $id, 'status' => 'success']);  
	        }else{
	          return response(['msg' => '', 'status' => 'error']); 
	        }
      	}
      	   
    }
}
