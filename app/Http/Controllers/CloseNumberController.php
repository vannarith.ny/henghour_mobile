<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class CloseNumberController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }elseif (Session::get('roleLot') != 1 && Session::get('roleLot') != 3) {
                    Redirect::to('/salephone?type=1')->send();
                }
            return $next($request);
        });
   
    }

    public function deleteItem(Request $request){
        $id = $request->id;
        
        $check = DB::table('tbl_close_number')->where('c_id', $id)->delete();
        if($check){
            return response(['msg' => $id, 'status' => 'success']);  
        }else{
            return response(['msg' => '', 'status' => 'error']); 
        } 
           
  }

    public function index()
    {
         $page = 'close_number';

        //  $times = DB::table('tbl_parameter_value')
        //         ->join('tbl_parameter_type', 'tbl_parameter_value.pat_id','=','tbl_parameter_type.pat_id')
        //         ->where('pat_key','sheet')
        //         ->orderBy('pav_value','ASC')
        //         ->pluck('pav_value', 'pav_id')->all();

        //  $posts = DB::table('tbl_pos')
        //             ->select('pos_id','pos_name')
        //             ->where('pos_time',$sheet_id)
        //             ->orderBy('pos_name', 'ASC')
        //             ->pluck('pos_name','pos_id')
        //             ->all();

     	$closeNumbers = DB::table('tbl_close_number')
                          ->leftjoin('tbl_parameter_value', 'tbl_close_number.sheet_id','=','tbl_parameter_value.pav_id')
                          ->leftjoin('tbl_pos', 'tbl_close_number.post_id','=','tbl_pos.pos_id')
                         ->orderBy('tbl_close_number.c_date','DESC')
                         ->orderBy('tbl_close_number.sheet_id','DESC')
                         ->get();
     	return view('closenumber.index', compact('closeNumbers','page'));
    }

    public function getpost(Request $request)
    {
        $type = $request->type;

        //  $posts = DB::table('tbl_pos')
        //             ->select('pos_id','pos_name')
        //             ->where('pos_time',$sheet_id)
        //             ->orderBy('pos_name', 'ASC')
        //             ->pluck('pos_name','pos_id')
        //             ->all();

        $pots = DB::table('tbl_pos')
            ->select('pos_id','pos_name')
            ->where('pos_time',$type)
            ->orderBy('pos_name', 'ASC')
            ->get();

        $option = '<option value="" selected="selected">រើសប៉ុស</option>';
        foreach ($pots as $value) {
            $option .= '<option value="'.$value->pos_id.'">'.$value->pos_name.'</option>';
        }

        return response(['msg' => $option, 'status' => 'success']);
    }

    public function create(){
        $page = 'add_close_number';
        
         $times = DB::table('tbl_parameter_value')
                ->join('tbl_parameter_type', 'tbl_parameter_value.pat_id','=','tbl_parameter_type.pat_id')
                ->where('pat_key','sheet')
                ->orderBy('pav_value','ASC')
                ->pluck('pav_value', 'pav_id')->all();
        
         $posts = DB::table('tbl_pos')
                    ->select('pos_id','pos_name')
                    ->where('pos_time',old('sheet_id'))
                    ->orderBy('pos_name', 'ASC')
                    ->pluck('pos_name','pos_id')
                    ->all();
        
	    return view('closenumber.create', compact('page','times','posts'));
    }

    public function store(Request $request)
    {
     	$sheet_id = $request->sheet_id;
	    $post_id = $request->post_id;
        $c_number = $request->c_number;
        $price_limit_r = $request->price_limit_r;
        $price_limit_d = $request->price_limit_d;


        $check = DB::table('tbl_close_number')
                    ->where('c_number', $c_number)
                    ->where('sheet_id', $sheet_id)
                    ->where('post_id', $post_id)
                    ->first();
                    
        
        if($check){
            flash()->error("លេខ".$c_number."មានបិទរួចទៅហើយអ្នកអាចរកមើលនូវ Page list");
            return redirect('/closenumber/create')->withInput();
        }else{

            $checkv2 = DB::table('tbl_close_number')->insert([
                        'c_number' => $c_number,
                        'post_id' => $post_id,
                        'sheet_id' => $sheet_id,
                        'price_limit_r' => $price_limit_r,
                        'price_limit_d' => $price_limit_d
                        ]
                    ); 
            if($checkv2){
                flash()->success("លេខត្រូវបានបិទ");
                return redirect('/closenumber');
            }else{
                flash()->error("លេខមិនត្រូវបានរក្សាទុក");
                return redirect('/closenumber/create')->withInput(); 
            }
        }
    }
    public function edit($id)
    {
        $page = 'close_number';

        $closeNumber = DB::table('tbl_close_number')->where('c_id', $id)->first();
        
        $times = DB::table('tbl_parameter_value')
                ->join('tbl_parameter_type', 'tbl_parameter_value.pat_id','=','tbl_parameter_type.pat_id')
                ->where('pat_key','sheet')
                ->orderBy('pav_value','ASC')
                ->pluck('pav_value', 'pav_id')->all();
        
        $posts = DB::table('tbl_pos')
                    ->select('pos_id','pos_name')
                    ->where('pos_time',$closeNumber->sheet_id)
                    ->orderBy('pos_name', 'ASC')
                    ->pluck('pos_name','pos_id')
                    ->all();

	    return view('closenumber.edit', compact('page','closeNumber','times','posts'));
    }

    public function update(Request $request, $id){   

        $sheet_id = $request->sheet_id;
	    $post_id = $request->post_id;
        $c_number = $request->c_number;
        $price_limit_r = $request->price_limit_r;
        $price_limit_d = $request->price_limit_d;

        $check = DB::table('tbl_close_number')
                    ->where('c_id','<>', $id)
                    ->where('c_number', $c_number)
                    ->where('sheet_id', $sheet_id)
                    ->where('post_id', $post_id)
                    ->first();
                    
        
        if($check){
            flash()->error("លេខ".$c_number."មានបិទរួចទៅហើយអ្នកអាចរកមើលនូវ Page list");
            return redirect('/closenumber/'.$id.'/edit')->withInput();
        }else{

            $checkv2 = DB::table('tbl_close_number')->where('c_id', $id)->update([
                'c_number' => $c_number,
                'post_id' => $post_id,
                'sheet_id' => $sheet_id,
                'price_limit_r' => $price_limit_r,
                'price_limit_d' => $price_limit_d
                ]
            ); 
            if($checkv2){
                flash()->success("លេខបិទត្រូវបានផ្លាសប្តូរ");
                return redirect('/closenumber');
            }else{
                flash()->error("លេខមិនត្រូវបានរក្សាទុក");
                return redirect('/closenumber/'.$id.'/edit')->withInput(); 
            }

            
        }

        
    }
}
