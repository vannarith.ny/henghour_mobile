<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class ReportMainStaffController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }

    public function updatecodep(Request $request){

        $pIDList = $request->pIDList;
        $codeVal = $request->codeVal;
        $pDate = $request->pDate;
        $p_id = $request->p_id;

        // if($codeVal != ''){
            // $pDate = date("d-m-Y", strtotime($pDate));
            $check = DB::table('summary_lottery_page')->where('id',$pIDList)->update([
                                'p_code_p' => $codeVal
                            ]
                        );
            if($check){
               

                DB::table('tbl_paper_mobile')->where('p_id',$p_id)
                                    ->update([
                                            'p_code_p' => $codeVal
                                    ]);

                return response(['msg' => $pIDList, 'data'=> $codeVal, 'status' => 'success']);
            }else{
                return response(['msg' => 'Database error', 'status' => 'error']);    
            }
            

        // }else{
        //     return response(['msg' => 'code not found', 'status' => 'error']); 
        // }

        

    }

    public function updatecode(Request $request){

        $pIDList = $request->pIDList;
        $codeVal = $request->codeVal;
        $pDate = $request->pDate;
        $p_id = $request->p_id;

        // if($codeVal != ''){
            // $pDate = date("d-m-Y", strtotime($pDate));
            $check = DB::table('summary_lottery_page')->where('id',$pIDList)->update([
                                'p_code' => $codeVal
                            ]
                        );
            if($check){
               

                DB::table('tbl_paper_mobile')->where('p_id',$p_id)
                                    ->update([
                                            'p_code' => $codeVal
                                    ]);


                // insert to tracking
                DB::table('tbl_tracking')->insert([
                            't_name' => $codeVal,
                            'p_id' => $p_id,
                            'u_id' => Session::get('iduserlot')

                            ]
                        );           

                return response(['msg' => $pIDList, 'data'=> $codeVal, 'status' => 'success']);
            }else{
                return response(['msg' => 'Database error', 'status' => 'error']);    
            }
            

        // }else{
        //     return response(['msg' => 'code not found', 'status' => 'error']); 
        // }

        

    }

    public function updatenumber(Request $request){

        $pIDList = $request->pIDList;
        $codeVal = $request->codeVal;
        $pDate = $request->pDate;
        $p_id = $request->p_id;

        if($codeVal != ''){
            // $pDate = date("d-m-Y", strtotime($pDate));
            $check = DB::table('summary_lottery_page')->where('id',$pIDList)->update([
                                'page_value' => $codeVal
                            ]
                        );
            if($check){
                

                DB::table('tbl_paper_mobile')->where('p_id',$p_id)
                                    ->update([
                                            'p_number' => $codeVal
                                    ]);

                return response(['msg' => $pIDList, 'data'=> $codeVal, 'status' => 'success']);
            }else{
                return response(['msg' => 'Database error', 'status' => 'error']);    
            }
            

        }else{
            return response(['msg' => 'code not found', 'status' => 'error']); 
        }

        

    }

    public function getstaffchild(Request $request)
    {
        $s_id = $request->s_id;

        if($s_id != ''){

            $getMain = DB::table('tbl_staff')
                        ->select('tbl_staff.s_id','tbl_staff.s_name')
                        ->where('tbl_staff.parent_id',$s_id)
                        ->first();
            // dd($getMain);

            if($getMain){

                $mainS = DB::table('tbl_staff')
                        ->select('tbl_staff.s_id','tbl_staff.s_name')
                        ->where('tbl_staff.parent_id',$s_id)
                        ->orderBy('tbl_staff.s_name', 'ASC')
                        ->groupBy('tbl_staff.s_id')
                        ->pluck('s_name','s_id')
                        ->all();

                $chilID = array();
                if($mainS){
                    

                    foreach ($mainS as $key => $value) {
                        array_push($chilID,$key);
                    }
                }


                $staff = DB::table('tbl_staff_charge_main')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->leftjoin('tbl_staff','tbl_staff_charge_main.chail_id','tbl_staff.s_id')
                    // ->where('tbl_staff_charge_main.s_id',$s_id)
                    ->whereIn('tbl_staff_charge_main.s_id', $chilID)
                    ->where('tbl_staff.s_role', 1)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->get();

            }else{
                $staff = DB::table('tbl_staff_charge_main')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->leftjoin('tbl_staff','tbl_staff_charge_main.chail_id','tbl_staff.s_id')
                    ->where('tbl_staff_charge_main.s_id',$s_id)
                    ->where('tbl_staff.s_role', 1)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->get();
            }
            

            $option = '<option value="" selected="selected">កូនក្រុម</option>';
            foreach ($staff as $value) {
                $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
            }
        
        }else{

            if(Session::get('roleLot') == 100){
                // $staff = DB::table('tbl_staff')
                //     ->select('tbl_staff.s_id','tbl_staff.s_name')
                //     ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                //     ->orderBy('tbl_staff.s_name', 'ASC')
                //     ->get();

                    $staff = DB::table('tbl_staff')
                        ->select('tbl_staff.s_id','tbl_staff.s_name')
                        ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
                        ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')
                        ->where('u_id',Session::get('iduserlot'))
                        ->where('tbl_staff.s_role', 1)
                        ->orderBy('tbl_staff.s_name', 'ASC')
                        ->groupBy('tbl_staff.s_id')
                        ->get();

                    $staffOther = DB::table('tbl_staff')
                            ->select('tbl_staff.s_id','tbl_staff.s_name')
                            ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                            ->join('tbl_user_staff','tbl_staff_charge.s_id','tbl_user_staff.s_id')
                            ->where('u_id',Session::get('iduserlot'))
                            ->where('tbl_staff.s_role', 1)
                            ->orderBy('tbl_staff.s_name', 'ASC')
                            ->groupBy('tbl_staff.s_id')
                            ->get();
                    $staff = $staff->merge($staffOther);
            }else{
                $staff = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->where('s_role',1)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->get();
            }
            

            // dd($staff);
            $option = '';
            // $option = '<option value="" selected="selected">កូនក្រុម</option>';
            foreach ($staff as $value) {
                $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
            }
        }

        


        return response(['msg' => $option, 'status' => 'success']);  
    }

    public function printdata(Request $request){
        $page = 'reportMain';
        
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_staff = $request->s_id_man;
        $var_staff_chail = $request->s_id;
        // dd($var_staff_chail);


        
        $staffMain = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();

        if(Session::get('roleLot') == 100){
            $staffMain = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->join('tbl_user_staff','tbl_staff.s_id','tbl_user_staff.s_id')
            ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
            ->where('u_id',Session::get('iduserlot'))
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
        }


        $staffMain['all'] = 'All';

        if($var_staff == 'all'){
            if(Session::get('roleLot') == 100){
                // $chilStaffs = DB::table('tbl_staff')
                //         ->select('tbl_staff.s_id','tbl_staff.s_name')
                //         ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
                //         ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')
                //         ->where('u_id',Session::get('iduserlot'))
                //         ->orderBy('tbl_staff.s_name', 'ASC')
                //         ->groupBy('tbl_staff.s_id')
                //         ->pluck('s_name','s_id')
                //         ->all();

                
                    // $chilStaffsOther = DB::table('tbl_staff')
                    // ->select('tbl_staff.s_id','tbl_staff.s_name')
                    // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                    // ->join('tbl_user_staff','tbl_staff_charge.s_id','tbl_user_staff.s_id')
                    // ->where('u_id',Session::get('iduserlot'))
                    // ->orderBy('tbl_staff.s_name', 'ASC')
                    // ->groupBy('tbl_staff.s_id')
                    // ->pluck('s_name','s_id')
                    // ->all();
                    // $chilStaffs = array_merge($chilStaffs, $chilStaffsOther);
                $chilStaffs[''] = 'កូនក្រុម';
            }else{
                // $chilStaffs = DB::table('tbl_staff')
                //     ->select('tbl_staff.s_id','tbl_staff.s_name')
                //     ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                //     ->orderBy('tbl_staff.s_name', 'ASC')
                //     ->groupBy('tbl_staff.s_id')
                //     ->pluck('s_name','s_id')
                //     ->all();
                $chilStaffs[''] = 'កូនក្រុម';
            }

        }else if($var_staff != ''){
            $chilStaffs = DB::table('tbl_staff_charge_main')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->leftjoin('tbl_staff','tbl_staff_charge_main.chail_id','tbl_staff.s_id')
            ->where('tbl_staff_charge_main.s_id',$var_staff)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->pluck('s_name','s_id')
            ->all();
        
        }else{
            $chilStaffs = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();

            if(Session::get('roleLot') == 100){
                // $chilStaffs = DB::table('tbl_staff')
                // ->select('tbl_staff.s_id','tbl_staff.s_name')
                // ->join('tbl_user_staff','tbl_staff.s_id','tbl_user_staff.s_id')
                // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                // ->where('u_id',Session::get('iduserlot'))
                // ->orderBy('tbl_staff.s_name', 'ASC')
                // ->groupBy('tbl_staff.s_id')
                // ->pluck('s_name','s_id')
                // ->all();

                $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
                ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')
                ->where('u_id',Session::get('iduserlot'))
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();

                // if(count($chilStaffs) == 0){
                    $chilStaffsOther = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                    ->join('tbl_user_staff','tbl_staff_charge.s_id','tbl_user_staff.s_id')
                    ->where('u_id',Session::get('iduserlot'))
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->pluck('s_name','s_id')
                    ->all();
                // }
                $chilStaffs = $chilStaffs+$chilStaffsOther;
                // $chilStaffs = array_merge($chilStaffs, $chilStaffsOther);
            }
        }


        
                
                $mainNameStaffInfo = DB::table('tbl_staff')
                    ->where('s_id', $var_staff_chail)
                    ->first();
                // dd($var_staff_chail);
            // check paid or borrow and old price
                $parameterMoney =array();
                $checkreport = DB::table('tbl_staff')->where('s_id',$var_staff_chail)->first();
                $checkinView = $checkreport->payment;
                $checkinViewKh = 0;

                // get percent
                $percentStaff = $checkreport->percent_data;
                $staffInfo = $checkreport;

                // dd($checkreport->payment);
                if($checkreport->payment == 1 || $checkinViewKh == 1){

                    $take_last = DB::table('tbl_staff_transction')
                    ->where('s_id',$var_staff_chail)
                    ->where('st_date_search','<',$var_dateStart)
                    ->orderBy('st_date_search','DESC')->first();

                    // dd($take_last->st_date_search);
                    if($take_last){
                        $money_paid = DB::table('tbl_staff_transction')
                                ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                                ->where('s_id',$var_staff_chail)
                                ->where('st_date_search','=',$take_last->st_date_search)
                                ->where('st_date_search','<',$var_dateStart)
                                // ->where('st_type','<>','21')
                                ->get();
                        // dd($money_paid);
                    }else{
                    $money_paid = []; 
                    }
                    // dd($take_last->st_date_diposit);
                    // transction money
                    // dd($money_paid);


                    $old_money = DB::table('tbl_story_payment')->where('s_id',$var_staff_chail)->orderBy('id', 'desc')->first();

                }else{
                    $money_paid = [];
                    $old_money = [];
                }
            // check paid or borrow and old price
            //dd($money_paid,$old_money);
          
            $boss_data = DB::table('summary_lottery_page')
                    // ->select(DB::raw('p_code, CAST(p_code as SIGNED) AS casted_column, id,type_lottery,date_lottery,staff_value,id_staff,time_value,page_value,total2digitr,total3digitr,totalsum,total2digitrright,total3digitrright,total2digits,total3digits,totalsum_dolla,total2digitsright,total3digitsright,total1digitrright,total4digitrright,total1digitsright,total4digitsright,p_id'))
                    ->where('date_lottery',date("d-m-Y", strtotime($var_dateStart)))
                    ->whereRaw('id_staff IN ('.$var_staff_chail.')')

                    ->orderBy('time_value' ,'ASC')
                    ->orderBy('id_staff' ,'ASC')
                    // ->orderBy('p_id' ,'ASC')
                    ->orderBy('id_order' ,'ASC')

                    // ->orderBy('id_staff' ,'ASC')
                    // ->orderBy('staff_value' ,'ASC')
                    // ->orderBy('time_value' ,'ASC')
                    // ->orderBy('casted_column' ,'ASC')
                    // ->orderByRaw('LENGTH(p_code)')
                    // ->orderBy('p_code')
                    // ->orderByRaw('LENGTH(page_value)')
                    // ->orderBy('page_value','ASC')
                    ->get(); 
                    
            return view('reportmain.printdata', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','boss_data','money_paid','old_money','checkinView','checkinViewKh','percentStaff'));
        
        
    }

    public function printdataall(Request $request){
        $page = 'reportMain';
        
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_staff = $request->s_id_man;
        $var_staff_chail = $request->s_id;
        $type_lottery = $request->type_lottery;
        // dd($var_staff);


        if($var_staff == 'all'){
            if(Session::get('roleLot') == 100){
                $chilStaffs = DB::table('tbl_staff')
                        ->select('tbl_staff.s_id','tbl_staff.s_name')
                        // ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
                        ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')
                        ->where('u_id',Session::get('iduserlot'))
                        ->orderBy('tbl_staff.s_name', 'ASC')
                        ->groupBy('tbl_staff.s_id')
                        ->get();

            }else{
                $chilStaffs = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->get();
            }
        }else if($var_staff_chail != ''){

            $chilStaffs = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                    ->where('tbl_staff.s_id',$var_staff_chail)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->get();
                

        }else if($var_staff != ''){
            $chilStaffs = DB::table('tbl_staff_charge_main')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->leftjoin('tbl_staff','tbl_staff_charge_main.chail_id','tbl_staff.s_id')
            ->where('tbl_staff_charge_main.s_id',$var_staff)
            ->groupBy('tbl_staff.s_id')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->get();
            // dd($chilStaffs);
        }

        // dd($type_lottery);


        if($type_lottery){
            return view('reportmain.printdataall', compact('page','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','type_lottery'));
        }else{
            return view('reportmain.printdataalltype', compact('page','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','type_lottery'));
        }
          
                    
            
        
        
    }

    public function index(Request $request)
    {
        $page = 'reportMain';
        
        // $block = DB::table('tbl_block')
        //     ->orderBy('name', 'ASC')
        //     ->pluck('name','id')
        //     ->all();
        
        if(Session::get('roleLot') != 100){
            $staffMain = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();


            $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('s_role', 1)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();
        }else{

            // $idGetStaff = DB:table('tbl_user_staff')->where('u_id',Session::get('iduserlot'))->get();

            $staffMain = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_user_staff','tbl_staff.s_id','tbl_user_staff.s_id')
                ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
                ->where('u_id',Session::get('iduserlot'))
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();

            $staffsOther = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_user_staff','tbl_staff.parent_id','tbl_user_staff.s_id')
                ->where('tbl_user_staff.u_id',Session::get('iduserlot'))
                // ->where('tbl_staff.s_role', 1)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();
            // dd($staffsOther);
            $staffMain = $staffMain+$staffsOther;
            
    

            $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
                ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')
                ->where('u_id',Session::get('iduserlot'))
                ->where('tbl_staff.s_role', 1)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();
                // dd($chilStaffs);

            // if(count($chilStaffs) == 0){
                $chilStaffsOther = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                ->join('tbl_user_staff','tbl_staff_charge.s_id','tbl_user_staff.s_id')
                ->where('u_id',Session::get('iduserlot'))
                ->where('tbl_staff.s_role', 1)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();
                // dd($chilStaffs,$chilStaffsOther);
            // }
            $chilStaffs = $chilStaffs+$chilStaffsOther;
            // dd($chilStaffs);
        }


        // dd(Session::get('iduserlot'));
        if(Session::get('roleLot') == 100){

            $getMain = DB::table('tbl_staff')
                        ->join('tbl_user_staff','tbl_staff.parent_id','tbl_user_staff.s_id')
                        ->where('tbl_staff.parent_id','<>',0)
                        ->where('tbl_user_staff.u_id',Session::get('iduserlot'))
                        ->groupBy('tbl_staff.parent_id')
                        ->get();
            // dd($getMain);

        }else{
            $getMain = DB::table('tbl_staff')
                        ->where('tbl_staff.parent_id','<>',0)
                        ->groupBy('tbl_staff.parent_id')
                        ->get();
        }
        
        if($getMain){
            foreach ($getMain as $keyMain => $valueMain) {
                $getName = DB::table('tbl_staff')->where('s_id',$valueMain->parent_id)->first();
                if($getName){
                     $staffMain[$getName->s_id] = $getName->s_name;
                }
               
            }
        }

        if(Session::get('roleLot') == 1){
            $staffMain['all'] = 'All';
        }


        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH' );

        // dd($staffMain);

        return view('reportmain.index', compact('page','staffMain','type_lottery','var_type_lottery','chilStaffs'));
    }

    public function filterMain(Request $request)
    {
        $page = 'reportMain';
        
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_staff = $request->s_id_man;
        $var_staff_chail = $request->s_id;
        $var_type_lottery = $request->type_lottery;

        $type_lottery = array(1 => 'VN',2 =>'KH');


        
        $staffMain = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();

        if(Session::get('roleLot') == 100){

            // $staffMain = DB::table('tbl_staff')
            // ->select('tbl_staff.s_id','tbl_staff.s_name')
            // ->join('tbl_user_staff','tbl_staff.s_id','tbl_user_staff.s_id')
            // ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
            // ->where('u_id',Session::get('iduserlot'))
            // ->orderBy('tbl_staff.s_name', 'ASC')
            // ->groupBy('tbl_staff.s_id')
            // ->pluck('s_name','s_id')
            // ->all();
            // $staffMain['all'] = 'All';

            $staffMain = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_user_staff','tbl_staff.s_id','tbl_user_staff.s_id')
                ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
                ->where('u_id',Session::get('iduserlot'))
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();

            $staffsOther = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_user_staff','tbl_staff.parent_id','tbl_user_staff.s_id')
                ->where('tbl_user_staff.u_id',Session::get('iduserlot'))
                // ->where('tbl_staff.s_role', 1)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();
            // dd($staffsOther);
            $staffMain = $staffMain+$staffsOther;
        }

        if(Session::get('roleLot') == 1){
            $staffMain['all'] = 'All';
        }

        // $staffMain['15'] = '888';

        // $getMain = DB::table('tbl_staff')
        //                 ->where('tbl_staff.parent_id','<>',0)
        //                 ->groupBy('tbl_staff.parent_id')
        //                 ->get();
        // if($getMain){
        //     foreach ($getMain as $keyMain => $valueMain) {
        //         $getName = DB::table('tbl_staff')->where('s_id',$valueMain->parent_id)->first();
        //         $staffMain[$getName->s_id] = $getName->s_name;
        //     }
        // }

        if(Session::get('roleLot') == 100){

            $getMain = DB::table('tbl_staff')
                        ->join('tbl_user_staff','tbl_staff.parent_id','tbl_user_staff.s_id')
                        ->where('tbl_staff.parent_id','<>',0)
                        ->where('tbl_user_staff.u_id',Session::get('iduserlot'))
                        ->groupBy('tbl_staff.parent_id')
                        ->get();
            // dd($getMain);

        }else{
            $getMain = DB::table('tbl_staff')
                        ->where('tbl_staff.parent_id','<>',0)
                        ->groupBy('tbl_staff.parent_id')
                        ->get();
        }
        
        if($getMain){
            foreach ($getMain as $keyMain => $valueMain) {
                $getName = DB::table('tbl_staff')->where('s_id',$valueMain->parent_id)->first();
                if($getName){
                     $staffMain[$getName->s_id] = $getName->s_name;
                }
               
            }
        }

        // dd(Session::get('roleLot'));

        if($var_staff == 'all'){
            if(Session::get('roleLot') == 100){

                // $chilStaffs = DB::table('tbl_staff')
                //         ->select('tbl_staff.s_id','tbl_staff.s_name')
                //         ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
                //         ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')
                //         ->where('u_id',Session::get('iduserlot'))
                //         ->orderBy('tbl_staff.s_name', 'ASC')
                //         ->groupBy('tbl_staff.s_id')
                //         ->pluck('s_name','s_id')
                //         ->all();

                
                    // $chilStaffsOther = DB::table('tbl_staff')
                    // ->select('tbl_staff.s_id','tbl_staff.s_name')
                    // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                    // ->join('tbl_user_staff','tbl_staff_charge.s_id','tbl_user_staff.s_id')
                    // ->where('u_id',Session::get('iduserlot'))
                    // ->orderBy('tbl_staff.s_name', 'ASC')
                    // ->groupBy('tbl_staff.s_id')
                    // ->pluck('s_name','s_id')
                    // ->all();
                    // $chilStaffs = array_merge($chilStaffs, $chilStaffsOther);
                $chilStaffs[''] = 'កូនក្រុម';
            }else{
                // $chilStaffs = DB::table('tbl_staff')
                //     ->select('tbl_staff.s_id','tbl_staff.s_name')
                //     ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                //     ->orderBy('tbl_staff.s_name', 'ASC')
                //     ->groupBy('tbl_staff.s_id')
                //     ->pluck('s_name','s_id')
                //     ->all();
                $chilStaffs[''] = 'កូនក្រុម';
            }

        }else if($var_staff != ''){

            $getData = DB::table('tbl_staff')
                        ->where('tbl_staff.parent_id',$var_staff)
                        ->groupBy('tbl_staff.parent_id')
                        ->first();

            if($getData){

                $mainS = DB::table('tbl_staff')
                        ->select('tbl_staff.s_id','tbl_staff.s_name')
                        ->where('tbl_staff.parent_id',$var_staff)
                        ->orderBy('tbl_staff.s_name', 'ASC')
                        ->groupBy('tbl_staff.s_id')
                        ->pluck('s_name','s_id')
                        ->all();

                $chilNeed = array();
                if($mainS){
                    

                    foreach ($mainS as $keys => $value) {
                        array_push($chilNeed,$keys);
                    }
                }

                $chilStaffs = DB::table('tbl_staff_charge_main')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->leftjoin('tbl_staff','tbl_staff_charge_main.chail_id','tbl_staff.s_id')
                    // ->where('tbl_staff_charge_main.s_id',$var_staff)
                    ->whereIn('tbl_staff_charge_main.s_id', $chilNeed)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->pluck('s_name','s_id')
                    ->all();

            }else{
                $chilStaffs = DB::table('tbl_staff_charge_main')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->leftjoin('tbl_staff','tbl_staff_charge_main.chail_id','tbl_staff.s_id')
                    ->where('tbl_staff_charge_main.s_id',$var_staff)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->pluck('s_name','s_id')
                    ->all();
            }

            
        
        }else{
            $chilStaffs = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('s_role',1)
            // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();

            if(Session::get('roleLot') == 100){
                // $chilStaffs = DB::table('tbl_staff')
                // ->select('tbl_staff.s_id','tbl_staff.s_name')
                // ->join('tbl_user_staff','tbl_staff.s_id','tbl_user_staff.s_id')
                // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                // ->where('u_id',Session::get('iduserlot'))
                // ->orderBy('tbl_staff.s_name', 'ASC')
                // ->groupBy('tbl_staff.s_id')
                // ->pluck('s_name','s_id')
                // ->all();


                $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
                ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')
                ->where('u_id',Session::get('iduserlot'))
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();

                // if(count($chilStaffs) == 0){
                    $chilStaffsOther = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                    ->join('tbl_user_staff','tbl_staff_charge.s_id','tbl_user_staff.s_id')
                    ->where('u_id',Session::get('iduserlot'))
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->groupBy('tbl_staff.s_id')
                    ->pluck('s_name','s_id')
                    ->all();
                // }
                $chilStaffs = $chilStaffs+$chilStaffsOther;
                // $chilStaffs = array_merge($chilStaffs, $chilStaffsOther);
            }
        }


        // block check type report 
        if($var_staff_chail){

            $mainNameStaffInfo = DB::table('tbl_staff')
                    ->where('s_id', $var_staff_chail)
                    ->first();

        }else if($var_staff != ''){

            $mainNameStaffInfo = DB::table('tbl_staff')
                    ->where('s_id', $var_staff)
                    ->first();

        }else{

        }
        // dd($mainNameStaffInfo);


        // if old style report
        if(isset($mainNameStaffInfo) && $mainNameStaffInfo->type_report == 0 && $var_type_lottery != null){

            $staffInfo = $mainNameStaffInfo;
            // start block transction old payment
            $parameterMoney =array();
            $checkreport = DB::table('tbl_staff')->where('s_id',$mainNameStaffInfo->s_id )->first();
            $checkinView = $checkreport->payment;
            $checkinViewKh = 0;

            // get percent
            $percentStaff = $checkreport->percent_data;
            $staffInfo = $checkreport;

            // dd($checkreport->payment);
            if(($checkreport->payment == 1 || $checkinViewKh == 1) && $var_staff != null && $var_staff_chail != null){

                if($var_staff_chail != ''){
                    $take_last = DB::table('tbl_staff_transction')
                                ->where('s_id',$var_staff_chail)
                                ->where('st_date_search','<',$var_dateStart)
                                ->orderBy('st_date_search','DESC')->first();

                    // dd($take_last->st_date_search);
                    if($take_last){
                        $money_paid = DB::table('tbl_staff_transction')
                                ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                                ->where('s_id',$var_staff_chail)
                                ->where('st_date_search','=',$take_last->st_date_search)
                                ->where('st_date_search','<',$var_dateStart)
                                // ->where('st_type','<>','21')
                                ->get();
                        // dd($money_paid);
                    }else{
                        $money_paid = []; 
                    }
                }else if($var_staff != ''){

                    $take_last = DB::table('tbl_staff_transction')
                                ->where('s_id',$var_staff)
                                ->where('st_date_search','<',$var_dateStart)
                                ->orderBy('st_date_search','DESC')->first();

                    // dd($take_last->st_date_search);
                    if($take_last){
                        $money_paid = DB::table('tbl_staff_transction')
                                ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                                ->where('s_id',$var_staff)
                                ->where('st_date_search','=',$take_last->st_date_search)
                                ->where('st_date_search','<',$var_dateStart)
                                // ->where('st_type','<>','21')
                                ->get();
                        // dd($money_paid);
                    }else{
                        $money_paid = []; 
                    }

                }else{
                    $money_paid = []; 
                }
                


                $old_money = DB::table('tbl_story_payment')->where('s_id',$var_staff_chail)->orderBy('id', 'desc')->first();

            }else{
                $money_paid = [];
                $old_money = [];
            }

            // dd($money_paid);
            // end block transction old payment
            
            // dd($mainNameStaffInfo->s_id);
            $get_val_main = $this->loop_find_staff($mainNameStaffInfo->s_id);
            
            if($get_val_main != ''){
                $mul_staff = substr($get_val_main,0,-1);
                $main_new = 1;
            }else{
                $mul_staff = $mainNameStaffInfo->s_id;
                $main_new = 0;
            }
            // dd($mul_staff);
            ///////////block get reports///////////


            $boss_data = DB::table('summary_lottery_page')
                            ->where('date_lottery',date("d-m-Y", strtotime($var_dateStart)))
                            ->where('type_lottery',$var_type_lottery)
                            ->whereRaw('id_staff IN ('.$mul_staff.')')
                            // ->orderBy('id_staff' ,'ASC')
                            ->orderBy('staff_value' ,'ASC')
                            ->orderBy('time_value' ,'ASC')
                            ->orderBy('page_value','ASC')
                            ->orderByRaw('LENGTH(page_value)')
                            ->get();
            // dd($boss_data);


            return view('reportmain.report0Style', compact('page','boss_data','staffMain','chilStaffs','var_staff_chail','var_dateStart','var_dateEnd','var_staff','money_paid','old_money','checkinView','checkinViewKh','percentStaff', 'staffInfo','main_new','mainNameStaffInfo','var_type_lottery','type_lottery'));


            ///////////end block get reports///////////



        




        }else{ // style new report


        
            if(
                ($var_staff == '' && $var_staff_chail != '' && $var_dateEnd =='' && $var_type_lottery != '') ||
                ($var_staff != '' && $var_staff_chail != '' && $var_dateEnd =='' && $var_type_lottery != '')
                
                ){ //get report chail
                    
                    $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff_chail)
                        ->first();
                    // dd($var_staff_chail);
                // check paid or borrow and old price
                    $parameterMoney =array();
                    $checkreport = DB::table('tbl_staff')->where('s_id',$var_staff_chail)->first();
                    $checkinView = $checkreport->payment;
                    $checkinViewKh = 0;

                    // get percent
                    $percentStaff = $checkreport->percent_data;
                    $staffInfo = $checkreport;

                    // dd($checkreport->payment);
                    if($checkreport->payment == 1 || $checkinViewKh == 1){

                        $take_last = DB::table('tbl_staff_transction')
                        ->where('s_id',$var_staff_chail)
                        ->where('st_date_search','<',$var_dateStart)
                        ->orderBy('st_date_search','DESC')->first();

                        // dd($take_last->st_date_search);
                        if($take_last){
                            $money_paid = DB::table('tbl_staff_transction')
                                    ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                                    ->where('s_id',$var_staff_chail)
                                    ->where('st_date_search','=',$take_last->st_date_search)
                                    ->where('st_date_search','<',$var_dateStart)
                                    // ->where('st_type','<>','21')
                                    ->get();
                            // dd($money_paid);
                        }else{
                        $money_paid = []; 
                        }
                        // dd($take_last->st_date_diposit);
                        // transction money
                        // dd($money_paid);


                        $old_money = DB::table('tbl_story_payment')->where('s_id',$var_staff_chail)->orderBy('id', 'desc')->first();

                    }else{
                        $money_paid = [];
                        $old_money = [];
                    }
                // check paid or borrow and old price
                //dd($money_paid,$old_money);
              
                $boss_data = DB::table('summary_lottery_page')
                        // ->select(DB::raw('p_code, CAST(p_code as SIGNED) AS casted_column, id,type_lottery,date_lottery,staff_value,id_staff,time_value,page_value,total2digitr,total3digitr,totalsum,total2digitrright,total3digitrright,total2digits,total3digits,totalsum_dolla,total2digitsright,total3digitsright,total1digitrright,total4digitrright,total1digitsright,total4digitsright,p_id'))
                        ->where('date_lottery',date("d-m-Y", strtotime($var_dateStart)))
                        ->where('type_lottery',$var_type_lottery)
                        ->whereRaw('id_staff IN ('.$var_staff_chail.')')
                        ->orderBy('time_value' ,'ASC')
                        ->orderBy('id_staff' ,'ASC')
                        // ->orderBy('p_id' ,'ASC')
                        ->orderBy('id_order' ,'ASC')

                        // ->orderBy('staff_value' ,'ASC')
                        // ->orderBy('time_value' ,'ASC')
                        // ->orderBy('casted_column' ,'ASC')
                        // ->orderByRaw('LENGTH(p_code)')
                        // ->orderBy('p_code')
                        // ->orderByRaw('LENGTH(page_value)')
                        // ->orderBy('page_value','ASC')
                        ->get(); 
                // dd($boss_data);
                        
                return view('reportmain.report', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','boss_data','money_paid','old_money','checkinView','checkinViewKh','percentStaff','type_lottery','var_type_lottery'));

            }else if(
                ($var_staff == '' && $var_staff_chail != '' && $var_dateEnd =='' && $var_type_lottery == '') ||
                ($var_staff != '' && $var_staff_chail != '' && $var_dateEnd =='' && $var_type_lottery == '') ||
                ($var_staff == '' && $var_staff_chail != '' && $var_dateEnd !='' && $var_type_lottery == '' && $var_dateStart == $var_dateEnd) ||
                ($var_staff != '' && $var_staff_chail != '' && $var_dateEnd !='' && $var_type_lottery == '' && $var_dateStart == $var_dateEnd)
                
                ){ //get report chail
                    
                    
                // dd('here');
                $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff_chail)
                        ->first();

                
                if($var_staff_chail){

                    
                    // dd($var_staff_chail);
                    $reports = DB::table('tbl_total_everyday')
                            ->leftjoin('tbl_staff', 'tbl_total_everyday.s_id','=','tbl_staff.s_id')
                            ->where('tbl_total_everyday.s_id', $var_staff_chail)
                            ->where('tbl_total_everyday.date',$var_dateStart)
                            ->where('tbl_total_everyday.stc_type','<>',0)
                            // ->orderBy('tbl_total_everyday.s_id', 'ASC')
                            // ->orderByRaw('LENGTH(tbl_staff.s_name)', 'asc')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();
                    
                    // dd($reports);
                        $take_last = DB::table('tbl_staff_transction')
                        ->where('s_id',$var_staff_chail)
                        ->where('st_date_search','<',$var_dateStart)
                        ->orderBy('st_date_search','DESC')->first();
                        if($take_last){
                            $money_paid = DB::table('tbl_staff_transction')
                                        ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                                        ->where('s_id',$var_staff_chail)
                                        ->where('st_date_search','=',$take_last->st_date_search)
                                        ->where('st_date_search','<',$var_dateStart)
                                        // ->where('st_type','<>','21')
                                        ->get();
                        }else{
                            $money_paid = []; 
                        }
                        
                        // dd($money_paid);
                        return view('reportmain.onedayonstaff', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports','money_paid','type_lottery','var_type_lottery'));
                            
                    
                  
                    
                }else{
                    flash()->error("ឈ្មួញកណ្តាលមិនមានកូន");
                    $money_paid = []; 
                    return view('reportmain.index', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','money_paid','type_lottery','var_type_lottery'));
                }



            
            }else if(

                ($var_staff == '' && $var_staff_chail != '' && $var_dateEnd !='') || 
                ($var_staff != '' && $var_staff_chail != '' && $var_dateEnd !='')

                ){ //get report chail with start end date
                
                $mainNameStaffInfo = DB::table('tbl_staff')
                    ->where('s_id', $var_staff_chail)
                    ->first();
                
                $reports = DB::table('tbl_sum_by_paper')
                            ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                            ->where('tbl_sum_by_paper.s_id', $var_staff_chail)
                            ->where('tbl_sum_by_paper.date','>=',$var_dateStart)
                            ->where('tbl_sum_by_paper.date','<=',$var_dateEnd)
                            ->orderBy('tbl_sum_by_paper.date', 'ASC')
                            // ->orderBy('tbl_sum_by_paper.p_id', 'ASC')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();
                //dd($report);
                return view('reportmain.report_date_with_chail', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports'));
                
            }else if(

                ($var_staff != '' && $var_staff_chail == '' && $var_dateEnd !='' && $var_staff != 'all')

                ){ // get report by boss with start date end date
                    $chilID = array();
                    if($chilStaffs){
                        

                        foreach ($chilStaffs as $key => $value) {
                            array_push($chilID,$key);
                        }
                        
                        
                        

                        $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff)
                        ->first();

                        $reports = DB::table('tbl_total_everyday')
                            ->leftjoin('tbl_staff', 'tbl_total_everyday.s_id','=','tbl_staff.s_id')
                            ->whereIn('tbl_total_everyday.s_id', $chilID)
                            ->where('tbl_total_everyday.date','>=',$var_dateStart)
                            ->where('tbl_total_everyday.date','<=',$var_dateEnd)
                            ->orderBy('tbl_total_everyday.date', 'ASC')
                            // ->orderBy('tbl_total_everyday.s_id', 'ASC')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();

                        return view('reportmain.index_boss', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports'));
                    
                            
                            
                    
                        
                    }else{
                        flash()->error("ឈ្មួញកណ្តាលមិនមានកូន");
                        $money_paid = []; 
                        return view('reportmain.index', compact('page','block','var_dateStart','var_staff_chail','var_dateEnd','var_staff','type_lottery','var_type_lottery','money_paid','mainNameStaffInfo','chilStaffs','type_lottery','var_type_lottery'));
                    }
                        dd('Boss');
            }else if(

                ($var_staff == 'all' && $var_staff_chail == '' && $var_dateStart !='')

                ){ //report all staff


                    $mainNameStaffInfo = [];
                    if(Session::get('roleLot') == 100){
                        
                        $reports = DB::table('tbl_sum_by_paper')
                            ->join('tbl_staff_charge_main','tbl_sum_by_paper.s_id','tbl_staff_charge_main.chail_id')
                            ->join('tbl_user_staff','tbl_staff_charge_main.s_id','tbl_user_staff.s_id')

                            ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                            
                            ->where('tbl_sum_by_paper.date','=',$var_dateStart)
                            ->where('tbl_user_staff.u_id',Session::get('iduserlot'))
                            ->orderBy('tbl_sum_by_paper.date', 'ASC')
                            // ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();
                        // if(count($reports) == 0){
                            $reportsOther = DB::table('tbl_sum_by_paper')
                            ->join('tbl_staff_charge','tbl_sum_by_paper.s_id','tbl_staff_charge.s_id')
                            ->join('tbl_user_staff','tbl_staff_charge.s_id','tbl_user_staff.s_id')

                            ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                            
                            ->where('tbl_sum_by_paper.date','=',$var_dateStart)
                            ->where('tbl_user_staff.u_id',Session::get('iduserlot'))
                            ->orderBy('tbl_sum_by_paper.date', 'ASC')
                            // ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();
                            // dd($reports);
                        // }
                        $reports = $reports->merge($reportsOther);
                    }else{
                        $reports = DB::table('tbl_sum_by_paper')
                            ->leftjoin('tbl_staff', 'tbl_sum_by_paper.s_id','=','tbl_staff.s_id')
                            ->where('tbl_sum_by_paper.date','=',$var_dateStart)
                            ->orderBy('tbl_sum_by_paper.date', 'ASC')
                            // ->orderBy('tbl_sum_by_paper.s_id', 'ASC')
                            ->orderBy('tbl_staff.s_name', 'asc')
                            ->orderByRaw('LENGTH(tbl_staff.s_name)')
                            ->get();
                    }
                    // dd($reports);
                    return view('reportmain.index_all_staff_oneday', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports','type_lottery','var_type_lottery'));
                
          
            }else{
                // dd($chilStaffs);
                $mainNameStaffInfo = DB::table('tbl_staff')
                        ->where('s_id', $var_staff)
                        ->first();

                $chilID = array();
                if($chilStaffs){

                    
                    if($var_staff != 'all'){
                        foreach ($chilStaffs as $key => $value) {
                            array_push($chilID,$key);
                        }
                        // dd($chilID);
                        $reports = DB::table('tbl_total_everyday')
                                ->leftjoin('tbl_staff', 'tbl_total_everyday.s_id','=','tbl_staff.s_id')
                                ->whereIn('tbl_total_everyday.s_id', $chilID)
                                ->where('tbl_total_everyday.date',$var_dateStart)
                                ->where('tbl_total_everyday.stc_type','<>',0)
                                // ->orderBy('tbl_total_everyday.s_id', 'ASC')
                                // ->orderByRaw('LENGTH(tbl_staff.s_name)', 'asc')
                                ->orderBy('tbl_staff.s_name', 'asc')
                                ->orderByRaw('LENGTH(tbl_staff.s_name)')
                                ->get();
                        
                        // dd($reports);
                            $take_last = DB::table('tbl_staff_transction')
                            ->where('s_id',$var_staff)
                            ->where('st_date_search','<',$var_dateStart)
                            ->orderBy('st_date_search','DESC')->first();
                            if($take_last){
                                $money_paid = DB::table('tbl_staff_transction')
                                            ->leftjoin('tbl_parameter_value', 'tbl_parameter_value.pav_id', '=', 'tbl_staff_transction.st_type')
                                            ->where('s_id',$var_staff)
                                            ->where('st_date_search','=',$take_last->st_date_search)
                                            ->where('st_date_search','<',$var_dateStart)
                                            // ->where('st_type','<>','21')
                                            ->get();
                            }else{
                                $money_paid = []; 
                            }
                            
                            // dd($money_paid);
                            return view('reportmain.index', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','reports','money_paid','type_lottery', 'var_type_lottery'));
                            // return view('reportmain.index', compact('page','staffChild','var_staff_chail','var_dateStart','var_dateEnd','var_staff','staffMain','report','type_lottery','var_type_lottery','money_paid','mainNameStaffInfo','chilStaffs'));
                    }
                  
                    
                }else{
                    flash()->error("ឈ្មួញកណ្តាលមិនមានកូន");
                    $money_paid = []; 
                    return view('reportmain.index', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_staff','var_staff_chail','var_dateStart','var_dateEnd','money_paid','type_lottery','var_type_lottery'));
                }
                
            }

        } //end check type report
        
        
    }

    private function getTimeNew($type){
        if($type=='1'){
            $pat_key = "sheet";
        }elseif($type=='2'){
            $pat_key = "sheet_khmer";
        }elseif($type=='3'){
            $pat_key = "sheet_th";
        }elseif($type=='4'){
            $pat_key = "sheet_lo";
        }else{
            $pat_key = "sheet";
        }
        $result = DB::table('tbl_parameter_type')
            ->where('tbl_parameter_type.pat_key','=',$pat_key)
            ->join('tbl_parameter_value','tbl_parameter_type.pat_id','=','tbl_parameter_value.pat_id')
            ->get();
        return $result;
    }

    private function loop_find_staff($st_id){
        // var_dump($st_id);
        $get_val = '';

        $check_boss = DB::table('tbl_staff_charge_main')
            ->where('s_id','=',$st_id)
            ->get();
        // var_dump($check_boss);
        if($check_boss){
            // dd(count($check_boss));
            if(count($check_boss) >= 1){
                foreach ($check_boss as $key => $val){
                    $get_val .= "'".$val->chail_id."',";
                }
                // dd('ok');
                return $get_val;
            }else{

                foreach ($check_boss as $key => $val){
                    return $this->loop_find_staff($val->chail_id);
                }
            }
        }else{
            return '';
        }

    }

}
