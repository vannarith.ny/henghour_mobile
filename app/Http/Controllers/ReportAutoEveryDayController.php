<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;
use Mail;

use App\Model\Report;

class ReportAutoEveryDayController extends Controller
{
     public function step1(Request $request)
    {
        // step report index

        if($request->date){
            $dateCall = $request->date;
        }else{
            $dateCall = date("Y-m-d");
        }
        $page = 'auto_step1';
        // dd($dateCall);

        return view('reportautoeveryday.step1', compact('page','dateCall'));

    }

     public function step2(Request $request)
    {
        // step report

        if($request->date){
            $dateCall = $request->date;
        }else{
            $dateCall = date("Y-m-d");
        }
        $page = 'auto_step2';
        // dd($dateCall);

        return view('reportautoeveryday.step2', compact('page','dateCall'));

    }

    public function step3(Request $request)
    {
        // step onedayonstaff
    
        if($request->date){
            $dateCall = $request->date;
        }else{
            $dateCall = date("Y-m-d");
        }
        $page = 'auto_step3';
        // dd($dateCall);

        return view('reportautoeveryday.step3', compact('page','dateCall'));

    }

    public function step4(Request $request)
    {
        // step boss staff reportmain.index
    
        if($request->date){
            $dateCall = $request->date;
        }else{
            $dateCall = date("Y-m-d");
        }
        $page = 'auto_step4';
        // dd($dateCall);

        return view('reportautoeveryday.step4', compact('page','dateCall'));

    }
}
