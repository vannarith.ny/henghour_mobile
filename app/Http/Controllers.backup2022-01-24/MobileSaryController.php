<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileSaryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function index(){
    	$page = 'sary';

        $date = date('Y-m-d');
        // $date = '2019-05-04';
        // dd($date);

        

    	// $times = DB::table("tbl_time_close")
    	// 		 ->join('tbl_pos','tbl_time_close.p_id','tbl_pos.pos_id')
    	// 		 ->where('s_id',Session::get('iduserlotMobileSec'))->get();
    	// $currentDay = date("N");
    	// $currentDay = (int)$currentDay-1;
    	// $queryAfter = $times->where('sheet_id',5)->groupBy('p_id');
    	// dd();
    	return view("mobile.sary.index", compact('page','date'));
    }

    public function printelarger(Request $request){
        $page = 'sary';
        $type = $request->type;
        
        return view("mobile.sary.printelarger", compact('page','type'));
    }

    public function printsary(Request $request){
        $page = 'sary';

        $date = $request->dateprint;
        $time = $request->time;
        $sary =  DB::table('tbl_sary')->where('date_sary',$date)->get();

        if($time == 5){

            $idPosy = [1,2,3,4,5,6,7];
            $resultsAfternoon = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosy)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                    ->get();
            // dd($resultsAfternoon);
        }else{
            $resultsAfternoon = [];
        }

        if($time == 6){

            $idPosyEvening = [10,11,12,13];
            $resultsEvening = DB::table('tbl_result')
                    ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                    ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                    ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                    ->where('tbl_result.re_date', '=', $date)
                    ->whereIn('tbl_pos.pos_id',$idPosyEvening)
                    ->orderBy('tbl_pos.pos_id','ASC')
                    ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                    // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                    ->get();

        }else{
            $resultsEvening = [];
        }


        

        return view("mobile.sary.print", compact('page','resultsAfternoon','resultsEvening','sary','date'));


    }

    public function GetResultSary(Request $request){
        $date = $request->date;

        $newDate = date("Y-m-d", strtotime($date));

        
     //    if(count($sary) == 0){
     //        $newDate = date('Y-m-d',strtotime($date . "-1 days"));
     //        $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->get();
     //    }

        // dd($sary);
        $idPosy = [1,2,3,4,5,6,7];
        $resultsAfternoon = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsAfternoon);

        $idPosyEvening = [10,11,12,13];
        $resultsEvening = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosyEvening)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                ->get();

        
        $idPosy = [51,52,53,54,55,56,57];
        $resultsMorningv1 = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($resultsMorningv1);

        // sary morningv1
        $htmlMorningv1 = '';

        if(isset($resultsMorningv1) and count($resultsMorningv1) > 0 ){

            $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 23)->first();
            // dd($sary);
            $htmlMorningv1 .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/5\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="2">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនព្រឹក</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <b style="color: red; font-size: 14px;">10:30 AM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorningv1[0]->re_num_result.'</b><br>
                         <b>'.$resultsMorningv1[1]->re_num_result.'</b>
                    </td>
                </tr>';

            if(isset($sary)){
    
                $data = $sary->info;
                $resultLot = explode(",", $data);
            

                $htmlMorningv1 .='<tr>
                        <td>
                             <b>200N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[0].'</b><br>
                             <b>'.$resultLot[1].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>500N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[2].'</b><br>
                             <b>'.$resultLot[3].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>1,5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[4].'</b><br>
                             <b>'.$resultLot[5].'</b><br>
                             <b>'.$resultLot[6].'</b><br>
                             <b>'.$resultLot[7].'</b><br>
                             <b>'.$resultLot[8].'</b><br>
                             <b>'.$resultLot[9].'</b><br>
                             <b>'.$resultLot[10].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[11].'</b><br>
                             <b>'.$resultLot[12].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>8Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[13].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>12Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[14].'</b>
                        </td>
                    </tr>';
            }

            $htmlMorningv1 .='<tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorningv1[2]->re_num_result.'</b> - <b>'.$resultsMorningv1[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorningv1[4]->re_num_result.'</b> - <b>'.$resultsMorningv1[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorningv1[6]->re_num_result.'</b> - <b>'.$resultsMorningv1[7]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>F</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorningv1[8]->re_num_result.'</b> - <b>'.$resultsMorningv1[9]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>I</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorningv1[10]->re_num_result.'</b> - <b>'.$resultsMorningv1[11]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>N</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorningv1[12]->re_num_result.'</b> - <b>'.$resultsMorningv1[13]->re_num_result.'</b>
                    </td>
                </tr>';

           $htmlMorningv1 .= ' </table>';

        }

        // start sary morning
        $idPosy = [47,48,49,50,59,60,61];
        $resultsMorning = DB::table('tbl_result')
                ->select('tbl_pos.pos_name', 'tbl_pos.pos_id', 'tbl_pos.pos_two_digit', 'tbl_pos.pos_three_digit', 'tbl_result.re_id', 'tbl_result.re_num_result', 'tbl_result.pos_id', 'tbl_result.re_date')
                ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
                ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
                ->where('tbl_result.re_date', '=', $newDate)
                ->whereIn('tbl_pos.pos_id',$idPosy)
                ->orderBy('tbl_pos.pos_id','ASC')
                ->orderByRaw('LENGTH(tbl_result.re_num_result)', 'ASC')->orderBy('tbl_result.re_num_result', 'ASC')
                
                // ->orderBy(DB::raw('CAST(SUBSTR(tbl_result.re_num_result FROM 3) AS UNSIGNED)'))
                // ->orderBy('tbl_result.re_num_result',SORT_NATURAL,'DESC')
                ->get();
        // dd($sary[3]);

        // sary morning
        $htmlMorning = '';

        if(isset($resultsMorning) and count($resultsMorning) > 0 ){

            $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 24)->first();

            $htmlMorning .= '<div class="width200 text-center mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/5\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="2">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនថ្ងៃ</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <b style="color: red; font-size: 14px;">01:30 PM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorning[0]->re_num_result.'</b><br>
                         <b>'.$resultsMorning[1]->re_num_result.'</b>
                    </td>
                </tr>';

            if(isset($sary)){
    
                $data = $sary->info;
                $resultLot = explode(",", $data);
            

                $htmlMorning .='<tr>
                        <td>
                             <b>200N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[0].'</b><br>
                             <b>'.$resultLot[1].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>500N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[2].'</b><br>
                             <b>'.$resultLot[3].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>1,5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[4].'</b><br>
                             <b>'.$resultLot[5].'</b><br>
                             <b>'.$resultLot[6].'</b><br>
                             <b>'.$resultLot[7].'</b><br>
                             <b>'.$resultLot[8].'</b><br>
                             <b>'.$resultLot[9].'</b><br>
                             <b>'.$resultLot[10].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[11].'</b>
                        </td>
                    </tr>';
            }

            $htmlMorning .='<tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorning[2]->re_num_result.'</b> - <b>'.$resultsMorning[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorning[4]->re_num_result.'</b> - <b>'.$resultsMorning[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorning[6]->re_num_result.'</b> - <b>'.$resultsMorning[7]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>F</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorning[8]->re_num_result.'</b> - <b>'.$resultsMorning[9]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>I</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorning[10]->re_num_result.'</b> - <b>'.$resultsMorning[11]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>N</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsMorning[12]->re_num_result.'</b> - <b>'.$resultsMorning[13]->re_num_result.'</b>
                    </td>
                </tr>';

           $htmlMorning .= ' </table>';

        }



        // sary afternoon
        $htmlAfternoon = '';

        if(isset($resultsAfternoon) and count($resultsAfternoon) > 0 ){

            $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 5)->first();

            // dd($sary);
            $htmlAfternoon .= '<div class="width200 text-center mb-2 mt-4">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/5\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="2">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនល្ងាច</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <b style="color: red; font-size: 14px;">04:30 AM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[0]->re_num_result.'</b><br>
                         <b>'.$resultsAfternoon[1]->re_num_result.'</b>
                    </td>
                </tr>';

            if(isset($sary)){
    
                $data = $sary->info;
                $resultLot = explode(",", $data);
            

                $htmlAfternoon .='<tr>
                        <td>
                             <b>200N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[0].'</b><br>
                             <b>'.$resultLot[1].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>500N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[2].'</b><br>
                             <b>'.$resultLot[3].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>1,5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[4].'</b><br>
                             <b>'.$resultLot[5].'</b><br>
                             <b>'.$resultLot[6].'</b><br>
                             <b>'.$resultLot[7].'</b><br>
                             <b>'.$resultLot[8].'</b><br>
                             <b>'.$resultLot[9].'</b><br>
                             <b>'.$resultLot[10].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[11].'</b><br>
                             <b>'.$resultLot[12].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>8Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[13].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>12Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[14].'</b>
                        </td>
                    </tr>';
            }

            $htmlAfternoon .='<tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[2]->re_num_result.'</b> - <b>'.$resultsAfternoon[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[4]->re_num_result.'</b> - <b>'.$resultsAfternoon[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[6]->re_num_result.'</b> - <b>'.$resultsAfternoon[7]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>F</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[8]->re_num_result.'</b> - <b>'.$resultsAfternoon[9]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>I</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[10]->re_num_result.'</b> - <b>'.$resultsAfternoon[11]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>N</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsAfternoon[12]->re_num_result.'</b> - <b>'.$resultsAfternoon[13]->re_num_result.'</b>
                    </td>
                </tr>';

           $htmlAfternoon .= ' </table>';

        }

        // sary evening
        $htmlEvening = '';
        if(isset($resultsEvening) and count($resultsEvening) > 0 ){
            $sary =  DB::table('tbl_sary')->where('date_sary',$newDate)->where('type', 6)->first();

            // dd($sary);
            $htmlEvening .= '<div class="width200 text-center mt-4 mb-2">
                <button class="btn btn-danger btn-sm" type="submit" onclick="window.open(\''.URL::to('/').'/mobile/printsary/'.$newDate.'/5\')">Print Sary</button>
            </div>
            <table cellpadding="0" cellspacing="0" class="tableSrey">
                <tr>
                    <td colspan="2">
                        <b style="color: red; font-size: 16px;">លទ្ធផលឆ្នោតយួនយប់</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Date : '.$newDate.'</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <b style="color: red; font-size: 14px;">06:30 AM</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>A</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsEvening[0]->re_num_result.'</b><br>
                         <b>'.$resultsEvening[1]->re_num_result.'</b>
                    </td>
                </tr>';

            if(isset($sary)){
    
                $data = $sary->info;
                $resultLot = explode(",", $data);
            

                $htmlEvening .='<tr>
                        <td>
                             <b>200N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[0].'</b><br>
                             <b>'.$resultLot[1].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>500N</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[2].'</b><br>
                             <b>'.$resultLot[3].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>1,5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[4].'</b><br>
                             <b>'.$resultLot[5].'</b><br>
                             <b>'.$resultLot[6].'</b><br>
                             <b>'.$resultLot[7].'</b><br>
                             <b>'.$resultLot[8].'</b><br>
                             <b>'.$resultLot[9].'</b><br>
                             <b>'.$resultLot[10].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>5Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[11].'</b><br>
                             <b>'.$resultLot[12].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>8Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[13].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>12Tr</b>
                        </td>
                        <td style="text-align: right;">
                             <b>'.$resultLot[14].'</b>
                        </td>
                    </tr>';
            }

            $htmlEvening .='<tr>
                    <td style="font-size: 20px !important;">
                         <b>B</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsEvening[2]->re_num_result.'</b> - <b>'.$resultsEvening[3]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>C</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsEvening[4]->re_num_result.'</b> - <b>'.$resultsEvening[5]->re_num_result.'</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px !important;">
                         <b>D</b>
                    </td>
                    <td style="text-align: right; font-size: 20px !important;">
                         <b>'.$resultsEvening[6]->re_num_result.'</b> - <b>'.$resultsEvening[7]->re_num_result.'</b>
                    </td>
                </tr>
                
                </tr>';

           $htmlEvening .= ' </table>';
        }


        return response(['msg' => 'insert','htmlMorningv1' => $htmlMorningv1,'htmlMorning' => $htmlMorning, 'htmlAfternoon' => $htmlAfternoon, 'htmlEvening' => $htmlEvening, 'date' => $newDate, 'status' => 'success']);
    }
}
