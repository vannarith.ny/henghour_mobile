<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;
use App\Model\Pos;
use App\Model\Group;
use App\Model\PosGroup;

class PosGroupController extends Controller
{


    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }elseif (Session::get('roleLot') != 1) {
                Redirect::to('/sale')->send();
            }
            return $next($request);
        });

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

//        $page = 'pos_group';
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'pos_group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'pos_group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'pos_group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'pos_group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'pos_group';
            $stc_type = 1;
            $title = 'VN';
        }

//        $getGroups = Group::with('poss')->get();
        $getGroups = Group::with('poss')->where('stc_type','=',$stc_type)->get();
        $posGroups = array();
//        dd($getGroups);
        foreach ($getGroups as $key => $getGroup) {
            $posNames = array();
            $first = true;
            foreach ($getGroup->poss as $key_pos => $pos) {
                $posTimeName = $this->getTimeName($pos->pos_time);
                $posNames[$posTimeName][$pos->pos_time][] = $pos->pos_name;
            }
//             dd($posNames);

            if(!empty($posNames)){
                foreach ($posNames as $key_posName => $posName) {
                    foreach ($posName as $key_sheet => $sheet) {
                        $newGroup = array();
                        $newGroup['g_id'] = $getGroup->g_id;
                        $newGroup['g_name'] = $getGroup->g_name;
                        $newGroup['g_time'] = $key_sheet;
                        $newGroup['list_pos'] = rtrim(implode(',', $sheet), ',');
                        $newGroup['g_time_name'] = $key_posName;
                        $newGroup['g_info'] = $getGroup->g_info;
                        array_push($posGroups,  $newGroup);
                    }
                }
                // dd($posGroups);
            }
        }

        return view('pos_group.index', compact('page','posGroups','stc_type','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

//        $page = 'add_pos_group';
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'add_pos_group';
            $title = 'VN';
            $pat_key = "sheet";
        }elseif($stc_type=='2'){
            $page = 'add_pos_group_kh';
            $title = 'KH';
            $pat_key = "sheet_khmer";
        }elseif($stc_type=='3'){
            $page = 'add_pos_group_th';
            $title = 'TH';
            $pat_key = "sheet_th";
        }elseif($stc_type=='4'){
            $page = 'add_pos_group_lo';
            $title = 'វត្តុ';
            $pat_key = "sheet_lo";

        }else{
            $page = 'add_pos_group';
            $stc_type = 1;
            $title = 'VN';
            $pat_key = "sheet";
        }

        $listGroup =  Group::where('stc_type','=',$stc_type)->pluck('g_name','g_id')->all();

        $times = $this->getTime($stc_type);
        $firstTime = array_keys($times)[0];
        $listPoss = DB::table('tbl_pos')
            ->join('tbl_parameter_value','tbl_pos.pos_time','=','tbl_parameter_value.pav_id')
            ->join('tbl_parameter_type','tbl_parameter_value.pat_id','=','tbl_parameter_type.pat_id')
            ->where('tbl_pos.pos_time','=',$firstTime)
            ->where('tbl_parameter_type.pat_key',$pat_key)
            ->pluck('pos_name','pos_id')->all();

        return view('pos_group.create', compact('page','listPoss','listGroup','times','title','stc_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $title = 'VN';
        }elseif($stc_type=='2'){
            $title = 'KH';
        }elseif($stc_type=='3'){
            $title = 'TH';
        }elseif($stc_type=='4'){
            $title = 'វត្តុ';
        }else{
            $stc_type = 1;
            $title = 'VN';
        }

        $g_id = $request->g_id;
        $pos_ids = $request->pos_id;
        $sheet_id = $request->pos_time;

        $rule = [
            'g_id' => 'required|unique:tbl_pos_group,g_id,null,pg_id,sheet_id,'.$sheet_id,
            'pos_id' => 'required'
        ];



        $messages = [
            'g_id.required' => trans('validation.custom.group.name.required'),
            'g_id.unique' => trans('message.pos_group_exist'),
            'pos_id.required' => trans('validation.custom.pos.name.check')
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

//        dd($validator);

        if($validator->fails()) {
            return redirect('posgroup/create?stc_type='.$stc_type)->withInput($request->all())->withErrors($validator);
        }else{

            $datas = array();
            foreach ($pos_ids as $key_pos => $pos_id) {
                array_push($datas , ['g_id'=>$g_id,'pos_id'=>$pos_id,'sheet_id'=>$sheet_id]);
            }

            $checkInsert = PosGroup::insert($datas);
            if($checkInsert){
                flash()->success(trans('message.add_success'));
                return redirect('posgroup?stc_type='.$stc_type);
            }else{
                flash()->error(trans('message.add_error'));
                return redirect('posgroup?stc_type='.$stc_type);
            }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $sheet, Request $request)
    {
//        $page = 'pos_group';

        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'pos_group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'pos_group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'pos_group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'pos_group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'pos_group';
            $stc_type = 1;
            $title = 'VN';
        }

        $listGroup = Group::pluck('g_name','g_id')->all();
        $posGoup = Group::with('poss')->find($id);
        $timeID = $sheet;
        if($posGoup){
            $posCheckeds = $posGoup->poss->where('pos_time','=',$sheet)->pluck('pos_name','pos_id');
            if($posCheckeds->count() > 0){
                $times = $this->getTime($stc_type);
                $listPoss = Pos::where('pos_time','=',$timeID)->pluck('pos_name','pos_id')->all();
                return view('pos_group.edit', compact('page','listPoss','listGroup','posGoup','posCheckeds','times','timeID','stc_type','title'));
            }
            return redirect('posgroup?stc_type='.$stc_type);
        }else{
            return redirect('posgroup?stc_type='.$stc_type);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stc_type = $request->stc_type;
        if($stc_type=='1'){
            $page = 'pos_group';
            $title = 'VN';
        }elseif($stc_type=='2'){
            $page = 'pos_group_kh';
            $title = 'KH';
        }elseif($stc_type=='3'){
            $page = 'pos_group_th';
            $title = 'TH';
        }elseif($stc_type=='4'){
            $page = 'pos_group_lo';
            $title = 'វត្តុ';
        }else{
            $page = 'pos_group';
            $stc_type = 1;
            $title = 'VN';
        }

        $g_id = $request->g_id;
        $pos_ids = $request->pos_id;
        $timeID = $request->pos_time;

        if($g_id == $id){
            $rule = [
                'g_id' => 'required',
                'pos_id' => 'required'
            ];
        }else{
            $rule = [
                'g_id' => 'required|unique:tbl_pos_group,g_id',
                'pos_id' => 'required'
            ];
        }

        $messages = [
            'g_id.required' => trans('validation.custom.group.name.required'),
            'g_id.unique' => trans('message.pos_group_exist'),
            'pos_id.required' => trans('validation.custom.pos.name.check')
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            return redirect('posgroup/'.$id.'/edit/'.$timeID.'?stc_type='.$stc_type)->withInput($request->all())->withErrors($validator);
        }else{

            $deleteCheck = PosGroup::where('g_id','=',$id)->where('sheet_id','=',$timeID)->delete();
            if($deleteCheck){

                $datas = array();
                foreach ($pos_ids as $key_pos => $pos_id) {
                    array_push($datas , ['g_id'=>$g_id,'pos_id'=>$pos_id,'sheet_id'=>$timeID]);
                }

                $checkUpdate = PosGroup::insert($datas);
                if($checkUpdate){
                    flash()->success(trans('message.update_success'));
                    return redirect('posgroup/'.$id.'/edit/'.$timeID.'?stc_type='.$stc_type);
                }else{
                    flash()->error(trans('message.update_error'));
                    return redirect('posgroup/'.$id.'/edit/'.$timeID.'?stc_type='.$stc_type);
                }

            }else{
                flash()->error(trans('message.update_error'));
                return redirect('posgroup/'.$id.'/edit/'.$timeID.'?stc_type='.$stc_type);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteItem(Request $request){
        $id = $request->id;
        $checkGroup = Group::find($id);
        $pos_time = $request->pos_time;

        if($checkGroup){
            $checkPermission = DB::table('tbl_number')->where('g_id', $id)->first();
            if($checkPermission){
                return response(['msg' => trans('message.pos_group_error_delete') , 'status' => 'error']);
            }else{
                $check = PosGroup::where('g_id','=',$id)->where('sheet_id','=',$pos_time)->delete();
                if($check){
                    return response(['msg' => $check, 'status' => 'success']);
                }else{
                    return response(['msg' => trans('message.delete_error'), 'status' => 'error']);
                }


            }
        }else{
            return response(['msg' => trans('message.delete_error') , 'status' => 'error']);
        }
    }
    public function requestPos(Request $request){
        $timeID = $request->pos_time;
        $g_id = $request->g_id;
        $message = '';
        if($g_id != ''){
            $posGoup = Group::with('poss')->find($g_id);
            $posCheckeds = $posGoup->poss->pluck('pos_name','pos_id');
        }else{
            $posCheckeds = [];
        }
        $listPoss = Pos::where('pos_time','=',$timeID)->pluck('pos_name','pos_id')->all();
        if($listPoss){
            $message .= '<div class="row">';
            $q=0;
            foreach($listPoss as $keyPos => $listPos){
                $q++;
                if($q%3 != 0){
                    $message .= '<div class="col col-4"><label class="checkbox"><input type="checkbox" name="pos_id[]" value="'.$keyPos.'" '.((isset($posCheckeds[$keyPos]) == true) ? "checked" : "").'><i></i>'.$listPos.'</label></div>';
                }else{
                    $message .='<div class="col col-4"><label class="checkbox"><input type="checkbox" name="pos_id[]" value="'.$keyPos.'" '.((isset($posCheckeds[$keyPos]) == true) ? "checked" : "").'><i></i>'.$listPos.'</label></div></div><div class="row">';
                }
            }
            return response(['msg' => $message , 'status' => 'success']);
        }else{
            return response(['msg' => $message , 'status' => 'error']);
        }
    }

    private function getTime($type){
        if($type=='1'){
            $pat_key = "sheet";
        }elseif($type=='2'){
            $pat_key = "sheet_khmer";
        }elseif($type=='3'){
            $pat_key = "sheet_th";
        }elseif($type=='4'){
            $pat_key = "sheet_lo";
        }else{
            $pat_key = "sheet";
        }
        $result = DB::table('tbl_parameter_type')
            ->where('tbl_parameter_type.pat_key','=',$pat_key)
            ->join('tbl_parameter_value','tbl_parameter_type.pat_id','=','tbl_parameter_value.pat_id')
            ->pluck('pav_value','pav_id')->all();
        return $result;
    }

    private function getTimeName($timeID){
        $result = '';
        $check = DB::table('tbl_parameter_value')->where('pav_id','=',$timeID)->first();
        if($check){
            $result = $check->pav_value;
        }
        return $result;
    }
}