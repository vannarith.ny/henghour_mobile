<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class moneyTotalReportController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }

    public function index(Request $request)
    {
        $page = 'money_total_report';

        $locations = DB::table('tbl_block')
                    ->pluck('name','id')->all();

        return view('moneytotalreport.index', compact('page','locations'));
    }

    public function filter(Request $request)
    {
        $page = 'money_total_report';
        $var_dateStart = $request->dateStart;
        $var_location_type = $request->locations;
        $get_val = '';

        $locations = DB::table('tbl_block')
                    ->pluck('name','id')->all();

        // dd($var_location_type);

        if($var_location_type != ''){

            $staff_list = DB::table('tbl_block_staff')
            ->leftjoin('tbl_staff', 'tbl_block_staff.s_id', '=', 'tbl_staff.s_id')
            ->where('b_id',$var_location_type)
            ->orderBy('tbl_staff.s_name','asc')
            ->get();


            $getloationName = DB::table('tbl_block')->where('id',$var_location_type)->first();
            // dd($staff_list);

           
                
                



            
        }else{

            $staff_list = [];
            $getloationName = [];

        }
        
                return view('moneytotalreport.index', compact('page','var_dateStart','var_location_type','staff_list','locations','getloationName'));
            
        
    }
}
