<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class StaffMobileController extends Controller
{
     public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }elseif (Session::get('roleLot') != 1) {
		       		Redirect::to('/sale')->send();
		       }
            return $next($request);
        });
   
    }

    private function ganerateTimeClose(){
        $posMorning = DB::table('tbl_pos')->whereIn('pos_time',[23])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $posTime1 = DB::table('tbl_pos')->whereIn('pos_time',[24])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();

        $posAFternoon = DB::table('tbl_pos')->whereIn('pos_time',[5])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        $posEvening = DB::table('tbl_pos')->whereIn('pos_time',[6])->orderBy('pos_time','ASC')->orderBy('pos_name','ASC')->get();
        // dd($posTime1);
        $daly = array(
                        '1' => 'ច័ន្ទ',
                        '2' => 'អង្គារ',
                        '3' => 'ពុធ',
                        '4' => 'ព្រហស្បត៍',
                        '5' => 'សុក្រ',
                        '6' => 'សៅរ៍',
                        '7' => 'អាទិត្យ' 
                        );
        // return $pos;

        // time close
        $timeDay = array();
        foreach ($posAFternoon as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:15";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:16";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:09";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:18";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:16";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:30";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:29";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:24";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:30";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:30";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:29";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:24";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:30";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:30";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:29";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:24";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:30";

                    }
                    
                }
            }else if($key == 4){ // F
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:15";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:15";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:14";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:09";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:19";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:16";

                    }
                    
                }
            }else if($key == 5){ // I
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:16";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:12";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:16";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:15";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:19";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:16";

                    }
                    
                }
            }else if($key == 6){ // K
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:15";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:16";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:09";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:11";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:18";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:16";

                    }
                    
                }
            }else if($key == 7){ // LO 23 + 15
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:17";

                    }
                    
                }
            }else if($key == 8){ // LO
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:17";

                    }
                    
                }
            }else if($key == 9){ // LO AB
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:17";

                    }
                    
                }
            }else if($key == 10){ // N
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:30";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:29";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:27";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:30";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:30";

                    }
                    
                }
            }else if($key == 11){ // P
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "16:30";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "16:29";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "16:27";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "16:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "16:33";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "16:30";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "16:30";

                    }
                    
                }
            }
            
        }

        // evening loop
        foreach ($posEvening as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:17";

                    }
                    
                }
            }else if($key == 1){ // A 1
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:17";

                    }
                    
                }
            }else if($key == 2){ // A 2
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:17";

                    }
                    
                }
            }else if($key == 3){ // A 3
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:17";

                    }
                    
                }
            }else if($key == 4){ // A 4
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:17";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:17";

                    }
                    
                }
            }else if($key == 5){ // B Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:23";

                    }
                    
                }
            }else if($key == 6){ // C Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:23";

                    }
                    
                }
            }else if($key == 7){ // D Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:23";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:23";

                    }
                    
                }
            }else if($key == 8){ // LO Night
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "18:10";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "18:10";

                    }
                    
                }
            
            }
            
        }


        foreach ($posMorning as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:07";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:20";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:20";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:20";

                    }
                    
                }
            }else if($key == 4){ // F
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:07";

                    }
                    
                }
            }else if($key == 5){ // I
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:07";

                    }
                    
                }
            }else if($key == 6){ // LO 23 + 19
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:07";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:07";

                    }
                    
                }
            
            }else if($key == 7){ // N
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "10:20";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "10:20";

                    }
                    
                }
            
            }
            
        }

        foreach ($posTime1 as $key => $posValue) {
            $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;
            if($key == 0){ // A
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:10";

                    }
                    
                }
            }else if($key == 1){ // B
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:25";

                    }
                    
                }
            }else if($key == 2){ // C
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:25";

                    }
                    
                }
            }else if($key == 3){ // D
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:25";

                    }
                    
                }
            }else if($key == 4){ // F
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:10";

                    }
                    
                }
            }else if($key == 5){ // I
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:10";

                    }
                    
                }
            }else if($key == 6){ // LO 23 + 19
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:10";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:10";

                    }
                    
                }
            
            }else if($key == 7){ // N
                foreach ($daly as $day => $value) {
                    if($day == 1){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 2){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 3){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 4){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 5){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 6){
                        $timeDay[$poscheck][$day] = "13:25";
                    }else if($day == 7){
                        $timeDay[$poscheck][$day] = "13:25";

                    }
                    
                }
            
            }
            
        }

        return $timeDay;


    }

    public function index()
    {
     	$page = 'staff_mobile';
     	$staffs = DB::table('tbl_staff')
                   ->where('s_role',1)
                   ->orderBy('s_name','ASC')->get();
     	return view('staffmobile.index', compact('staffs','page'));
    }
    public function create(){
    	$page = 'add_staff_mobile';
        $staffs = DB::table('tbl_staff')
            ->where('s_role',0)
            ->where('tbl_staff.parent_id','0')
            ->pluck('s_name','s_id')->all();
	    return view('staffmobile.create', compact('page','staffs'));
    }

    public function store(Request $request)
    {
        $posTime = $this->ganerateTimeClose();
        // dd($posTime);
    	$s_name = $request->s_name;
        $s_phone = $request->s_phone;

        $IsLimitMoney2DR = $request->IsLimitMoney2DR;
        $IsLimitMoney3DR = $request->IsLimitMoney3DR;
        $IsLimitMoney2DD = $request->IsLimitMoney2DD;
        $IsLimitMoney3DD = $request->IsLimitMoney3DD;

        $SCode = $request->SCode;
        $s_two_digit_charge = $request->s_two_digit_charge;
        $s_three_digit_charge = $request->s_three_digit_charge;

        $s_two_digit_paid = $request->s_two_digit_paid;
        $s_three_digit_paid = $request->s_three_digit_paid;

        $s_phone_login = $request->s_phone_login;
        $s_password = $request->s_password;

	     // $check = DB::table('tbl_staff')->where('s_name', $s_name)->first();

	    $rule = [
	    	's_name' => 'required',
            's_phone_login' => 'required|unique:tbl_staff',
            's_password' => 'required'
	    ];

	    $messages = [
	    	's_name.required' => 'សូមបញ្ចូលឈ្មោះ',
	    	's_phone_login.required' => 'សូមបញ្ចូលលេខទូរស័ព្ទ',
	    	's_phone_login.unique' => 'ឈ្មោះគណនី​ '.$s_phone_login.' មានរួចហើយ។​ សូមបញ្ចូលឈ្មោះថ្មី',
	    	's_password.required' => 'សូមបញ្ចូលpassword 4 ឬ 6 ខ្ទង់'
	    ];

	    $validator = Validator::make($request->all(), $rule, $messages);

	    if ($validator->fails()) {
	        return redirect('staffmobile/create')->withInput($request->all())->withErrors($validator);
	    }else{


	    	

	        $CanCreateChild = 0;
	        if($request->CanCreateChild == 'on'){
	            $CanCreateChild = 1;
	        }

	        $s_dollar = 0;
	        if($request->s_dollar == 'on'){
	            $s_dollar = 1;
	        }

	        $Active = 0;
	        if($request->Active == 'on'){
	            $Active = 1;
	        }
	        // dd($request->s_dollar);
	        
	        
	        $id_user = DB::table('tbl_staff')
	                ->insertGetId([
	                    's_name' => $s_name,
	                    's_phone' => $s_phone,
	                    'IsLimitMoney2DR' => $IsLimitMoney2DR,
                        'IsLimitMoney3DR' => $IsLimitMoney3DR,
                        'IsLimitMoney2DD' => $IsLimitMoney2DD,
                        'IsLimitMoney3DD' => $IsLimitMoney3DD,
	                    'SCode' => $SCode,
	                    's_two_digit_charge' => $s_two_digit_charge,
                        's_three_digit_charge' => $s_three_digit_charge,
                        's_two_digit_paid' => $s_two_digit_paid,
                        's_three_digit_paid' => $s_three_digit_paid,
	                    'CanCreateChild' => $CanCreateChild,
	                    's_dollar' => $s_dollar,
	                    's_phone_login' => $s_phone_login,
	                    's_password' => $s_password,
	                    'Active' => $Active,
	                    's_role' => 1
	                    ]);

                    // auto add to water with any user
                    $waterDigit = $s_two_digit_paid + 5;
	                DB::table('tbl_water')
	                ->insert([
	                    's_id' => $id_user,
	                    'w_2digit' => $waterDigit,
	                    'w_3digit' => $waterDigit,
	                    'time' => 5
	                    ]);
	                DB::table('tbl_water')
	                ->insert([
	                    's_id' => $id_user,
	                    'w_2digit' => $waterDigit,
	                    'w_3digit' => $waterDigit,
	                    'time' => 6
	                    ]);

	                // auto add to time close by user
	                $posTime = $this->ganerateTimeClose();
	                foreach ($posTime as $key => $timeClose) {
	                    $data = explode("//",$key);
	                    // $poscheck = $posValue->pos_id.'//'.$posValue->pos_name.'//'.$posValue->pos_time;

	                    foreach ($timeClose as $key => $value) {
	                        DB::table('tbl_time_close')
	                        ->insert([
	                            's_id' => $id_user,
	                            't_time' => $value,
	                            't_day' => $key,
	                            'p_id' => $data[0],
	                            'sheet_id' => $data[2]
	                            ]);
	                    }      
	                }

                // add defult post

                    // loop morning
                    // $groupNames = ["A","B","C","D","F","I","N","Lo(23+19)","Lo+FIN","Lo+F","4P","5P","6P","7P","BCD","BC","BD","CD","FIN","FI"];
                    // $getPostAfternoon = DB::table('tbl_pos_group')
                    //                 ->leftjoin('tbl_group','tbl_pos_group.g_id','tbl_group.g_id')
                    //                 ->where("tbl_pos_group.sheet_id",23)
                    //                 ->whereIn("tbl_group.g_name",$groupNames)
                    //                 ->orderBy('tbl_group.g_name', 'ASC')
                    //                 ->groupBy('tbl_pos_group.g_id')
                    //                 ->get();
                    // foreach ($getPostAfternoon as $key => $valuePost) {
                    //   DB::table('tbl_group_user_disable')->insert([
                    //       'g_id'  =>  $valuePost->g_id,
                    //       's_id' => $id_user
                    //       ]
                    //   );
                    // }

                    // loop morning 1
                    $groupNames = ["Lo 16+20"];
                    $getPostAfternoon = DB::table('tbl_pos_group')
                                    ->leftjoin('tbl_group','tbl_pos_group.g_id','tbl_group.g_id')
                                    ->where("tbl_pos_group.sheet_id",24)
                                    ->whereIn("tbl_group.g_name",$groupNames)
                                    ->orderBy('tbl_group.g_name', 'ASC')
                                    ->groupBy('tbl_pos_group.g_id')
                                    ->get();
                    foreach ($getPostAfternoon as $key => $valuePost) {
                      DB::table('tbl_group_user_disable')->insert([
                          'g_id'  =>  $valuePost->g_id,
                          's_id' => $id_user
                          ]
                      );
                    }

                    // loop afternoon
                    $groupNames = ["A","B","C","D","F","I","N","Lo(23+19)","Lo+FIN","Lo+F","4P","5P","6P","7P","BCD","BC","BD","CD","FIN","FI"];
                    $getPostAfternoon = DB::table('tbl_pos_group')
                                    ->leftjoin('tbl_group','tbl_pos_group.g_id','tbl_group.g_id')
                                    ->where("tbl_pos_group.sheet_id",5)
                                    ->whereIn("tbl_group.g_name",$groupNames)
                                    ->orderBy('tbl_group.g_name', 'ASC')
                                    ->groupBy('tbl_pos_group.g_id')
                                    ->get();
                    foreach ($getPostAfternoon as $key => $valuePost) {
                      DB::table('tbl_group_user_disable')->insert([
                          'g_id'  =>  $valuePost->g_id,
                          's_id' => $id_user
                          ]
                      );
                    }

                    // // loop Evening
                    $groupNames = ["Lo(32+25)"];
                    $getPostEvening = DB::table('tbl_pos_group')
                                    ->leftjoin('tbl_group','tbl_pos_group.g_id','tbl_group.g_id')
                                    ->where("tbl_pos_group.sheet_id",6)
                                    ->whereIn("tbl_group.g_name",$groupNames)
                                    ->orderBy('tbl_group.g_name', 'ASC')
                                    ->groupBy('tbl_pos_group.g_id')
                                    ->get();
                    foreach ($getPostEvening as $key => $valuePostEv) {
                      DB::table('tbl_group_user_disable')->insert([
                          'g_id'  =>  $valuePostEv->g_id,
                          's_id' => $id_user
                          ]
                      );
                    }
	                

                flash()->success(trans('message.add_success'));
                return redirect('/staffmobile/'.$id_user.'/edit');
            
	    }
    }

    public function edit($id)
    {
	    $page = 'staff_mobile';
	    $staff = DB::table('tbl_staff')
                        ->where('s_id',$id)
                        ->first();

	    return view('staffmobile.edit', compact('page','staff'));
    }

    public function update(Request $request, $id){     
	  	
	  	$s_name = $request->s_name;
        $s_phone = $request->s_phone;

        $IsLimitMoney2DR = $request->IsLimitMoney2DR;
        $IsLimitMoney3DR = $request->IsLimitMoney3DR;
        $IsLimitMoney2DD = $request->IsLimitMoney2DD;
        $IsLimitMoney3DD = $request->IsLimitMoney3DD;

        $SCode = $request->SCode;
        $s_two_digit_charge = $request->s_two_digit_charge;
        $s_three_digit_charge = $request->s_three_digit_charge;

        $s_two_digit_paid = $request->s_two_digit_paid;
        $s_three_digit_paid = $request->s_three_digit_paid;

        $s_phone_login = $request->s_phone_login;
        $s_password = $request->s_password;

        $rule = [
	    	's_name' => 'required',
            's_phone_login' => 'required|unique:tbl_staff,s_phone_login,'.$id.',s_id',
            's_password' => 'required'
	    ];

	    $messages = [
	    	's_name.required' => 'សូមបញ្ចូលឈ្មោះ',
	    	's_phone_login.required' => 'សូមបញ្ចូលលេខទូរស័ព្ទ',
	    	's_phone_login.unique' => 'ឈ្មោះគណនី​ '.$s_phone_login.' មានរួចហើយ។​ សូមបញ្ចូលឈ្មោះថ្មី',
	    	's_password.required' => 'សូមបញ្ចូលpassword 4 ឬ 6 ខ្ទង់'
	    ];

	    $validator = Validator::make($request->all(), $rule, $messages);

	    if ($validator->fails()) {
	        return redirect('staffmobile/'.$id.'/edit')->withInput($request->all())->withErrors($validator);
	    }else{

	    	$CanCreateChild = 0;
	        if($request->CanCreateChild == 'on'){
	            $CanCreateChild = 1;
	        }

	        $s_dollar = 0;
	        if($request->s_dollar == 'on'){
	            $s_dollar = 1;
	        }

	        $Active = 0;
	        if($request->Active == 'on'){
	            $Active = 1;
	        }

   			DB::table('tbl_staff')
            ->where('s_id', $id)
            ->update([
                    's_name' => $s_name,
                    's_phone' => $s_phone,
                    
                    'IsLimitMoney2DR' => $IsLimitMoney2DR,
                    'IsLimitMoney3DR' => $IsLimitMoney3DR,
                    'IsLimitMoney2DD' => $IsLimitMoney2DD,
                    'IsLimitMoney3DD' => $IsLimitMoney3DD,

                    'SCode' => $SCode,
                    's_two_digit_charge' => $s_two_digit_charge,
                    's_three_digit_charge' => $s_three_digit_charge,
                    's_two_digit_paid' => $s_two_digit_paid,
                    's_three_digit_paid' => $s_three_digit_paid,
                    'CanCreateChild' => $CanCreateChild,
                    's_dollar' => $s_dollar,
                    's_phone_login' => $s_phone_login,
                    's_password' => $s_password,
                    'Active' => $Active,
                    's_role' => 1,
                    ]);
	        
            flash()->success("ទិន្នន័យត្រូវបានកែប្រែ");
            return redirect('staffmobile/'.$id.'/edit');
            
        }
    }

    public function deleteItem(Request $request){
      	$id = $request->id;
      	
      		$check = DB::table('tbl_staff')->where('s_id', $id)->delete();
	        if($check){
	          	DB::table("tbl_water")->where('s_id',$id)->delete();
	            DB::table("tbl_staff_charge")->where('s_id',$id)->delete();
	            DB::table("tbl_time_close")->where('s_id',$id)->delete();
                DB::table("tbl_group_user_disable")->where('s_id',$id)->delete();
	          	return response(['msg' => $id, 'status' => 'success']);  
	        }else{
	          	return response(['msg' => '', 'status' => 'error']); 
	        }
      	   
    }


    public function timeclose(Request $request){
        $id = $request->id;
        $page = 'time_close';
        // $page = 'profit-loss';
        $daly = array(
                        '1' => 'ច័ន្ទ',
                        '2' => 'អង្គារ',
                        '3' => 'ពុធ',
                        '4' => 'ព្រហស្បត៍',
                        '5' => 'សុក្រ',
                        '6' => 'សៅរ៍',
                        '7' => 'អាទិត្យ' 
                        );
        $staff = DB::table('tbl_staff')
                        ->where('s_id',$id)
                        ->first();
        $times = DB::table("tbl_time_close")
                 ->join('tbl_pos','tbl_time_close.p_id','tbl_pos.pos_id')
                 ->where('s_id',$id)->get();
        $currentDay = date("N");
        $currentDay = (int)$currentDay-1;
        return view('staffmobile.timeclose', compact('page','staff','times','daly','currentDay'));
    }

    public function updatetimeclose(Request $request){
        $id = $request->id;
        $value = $request->value;

        
        $check = DB::table('tbl_time_close')
                ->where('t_id', $id)
                ->update([
                    't_time' => $value
                    ]);

        if($check){
            return response(['msg' => 'Update success', 'status' => 'success']);  
        }else{
            return response(['msg' => $check, 'status' => 'error']); 
        }
    }

}
