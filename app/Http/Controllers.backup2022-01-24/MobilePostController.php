<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;
use App\Model\Pos;
use App\Model\Group;
use App\Model\PosGroup;

class MobilePostController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    private function getTimeName($timeID){
        $result = '';
        $check = DB::table('tbl_parameter_value')->where('pav_id','=',$timeID)->first();
        if($check){
            $result = $check->pav_value;
        }
        return $result;
    }



    public function index(Request $request){
    	$page = 'Post';
    	$stc_type = 1;

    	$sheet_id = 5;
    	if($request->sheet_id){
    		$sheet_id = $request->sheet_id;
    	}
    	$user_id = Session::get('iduserlotMobileSec');
    	if($request->user_id){
    		$user_id = $request->user_id;
    	}

    	

        $childs = DB::table('tbl_staff')->where('parent_id',Session::get('iduserlotMobileSec'))->get();
        $times = DB::table('tbl_parameter_value')->where('pat_id',3)->get();

        $myID = DB::table('tbl_staff')->where('s_id',Session::get('iduserlotMobileSec'))->first();
        if($myID->parent_id == 0){
            $staffFollow = DB::table('tbl_staff')->where('parent_id',$myID->s_id)->where('s_id','<>',Session::get('iduserlotMobileSec'))->get();
        }else{
            $staffFollow = DB::table('tbl_staff')->where('parent_id',$myID->parent_id)->where('s_id','<>',Session::get('iduserlotMobileSec'))->get();
        }
        


    	// dd($staffid);
    	return view("mobile.post.index", compact('page', 'childs','times','sheet_id','staffFollow'));
    }

    public function UpdateClientPostVote(Request $request){
        $clientId = $request->clientId;
        $postVoteId = $request->postVoteId;

        if($request->checkValue == 'true'){
            DB::table('tbl_group_user_disable')->insert([
                        'g_id'  =>  $postVoteId,
                        's_id' => $clientId
                        ]
                    );
            return response(['msg' => 'insert', 'check' => 'checked', 'status' => 'success']);
        }else{
            DB::table('tbl_group_user_disable')->where('g_id',$postVoteId)->where('s_id',$clientId)->delete();
            return response(['msg' => 'delete', 'check' => 'checked', 'status' => 'success']);
        }
        
    }

    public function UpdateNameVoteVote(Request $request){
        $clientId = $request->clientId;
        $postVoteId = $request->postVoteId;
        $namePost = $request->namePost;

        if($request->checkValue == 'true'){

            DB::table('tbl_group_user_disable')->where('g_id',$postVoteId)->where('s_id',$clientId)->update(
                    [
                        'gu_name' => $namePost
                    ]
            );
            return response(['msg' => 'update', 'check' => 'checked', 'status' => 'success']);

        }else{
            

            DB::table('tbl_group_user_disable')->insert([
                        'g_id'  =>  $postVoteId,
                        's_id' => $clientId,
                        'gu_name' => $namePost
                        ]
                    );
            return response(['msg' => 'insert', 'check' => 'checked', 'status' => 'success']);
        }
    }

    public function GetPostVoteIdsClient(Request $request){
        $clientId = $request->clientId;
        $clientIdFollow = $request->clientIdFollow;
        $allPost = DB::table('tbl_group_user_disable')->where('s_id', $clientIdFollow)->get();
        if(count($allPost) > 0){
            foreach ($allPost as $key => $value) {
                DB::table('tbl_group_user_disable')->insert([
                        'g_id'  =>  $value->g_id,
                        'gu_name'  =>  $value->gu_name,
                        's_id' => $clientId
                        ]
                    );
            }
            return response(['msg' => 'ok', 'check' => $allPost, 'status' => 'success']);
        }else{
            return response(['msg' => 'គណនីមួយនេះមិនទាន់មានប៉ុសទេ', 'check' => 'checked', 'status' => 'error']);
        }
    }

    public function getpostbyuser(Request $request){
        $clientId = $request->clientId;
        $timeId = $request->timeId;
        $stc_type = 1;

        $getGroups = Group::with('poss')->where('stc_type','=',$stc_type)->orderBy('g_name', 'ASC')->get();
        $posGroups = array();

        foreach ($getGroups as $key => $getGroup) {
            $posNames = array();
            $first = true;
            foreach ($getGroup->poss as $key_pos => $pos) {
                $posTimeName = $this->getTimeName($pos->pos_time);
                $posNames[$posTimeName][$pos->pos_time][] = $pos->pos_name;
            }
//             dd($posNames);

            if(!empty($posNames)){
                foreach ($posNames as $key_posName => $posName) {
                    foreach ($posName as $key_sheet => $sheet) {
                        if($key_sheet == $timeId){
                            $newGroup = array();
                            $newGroup['g_id'] = $getGroup->g_id;
                            $newGroup['g_name'] = $getGroup->g_name;
                            $newGroup['g_time'] = $key_sheet;
                            $newGroup['list_pos'] = rtrim(implode(',', $sheet), ',');
                            $newGroup['g_time_name'] = $key_posName;
                            $newGroup['g_info'] = $getGroup->g_info;
                            array_push($posGroups,  $newGroup);
                        }
                        
                    }
                }
                // dd($posGroups);
            }
        }
        // dd($posGroups);

        // check data user
        $userUses = DB::table('tbl_group_user_disable')->where('s_id',$clientId)->get();
        // dd($userUses);
        $data = '';

        foreach ($posGroups as $key => $value) {
            $checked = '';
            $guName = $value['g_name'];
            foreach ($userUses as $key => $userUse) {
               if($userUse->g_id == $value['g_id']){
                    $checked ='checked';
                    if($userUse->gu_name != ''){
                       $guName = $userUse->gu_name; 
                    }
                    
               }
            }
            $numberPost = explode(",",$value['g_info']);
            if(isset($numberPost[0])){
                $num2Digit = $numberPost[0];
            }else{
                $num2Digit = 1;
            }

            if(isset($numberPost[1])){
                $num3Digit = $numberPost[1];
            }else{
                $num3Digit = 1;
            }

            $data .= '<tr>';
                $data .= '<td class="classtd'.$value['g_id'].'" align="center">';
                    $data .= '<input type="checkbox" '.$checked.' id="'.$value['g_id'].'" onchange="saveChange('.$value['g_id'].', $(this));">';
                $data .= '</td>';
                $data .= '<td class="classtd2">';
                    $data .= '<input size="10" class="form-control checkUpdateName" class="input" idPost="'.$value['g_id'].'" value="'.$guName.'" >';
                $data .= '</td>';
                $data .= '<td class="classtd text-left">'.$value['list_pos'].'</td>';
                $data .= '<td class="classtd text-center">x'.$num2Digit.'</td>';
                $data .= '<td class="classtd text-center">x'.$num3Digit.'</td>';
            $data .= '</tr>';
        }
        return response(['msg' => 'ok', 'data' => $data, 'status' => 'success']);


    }
}
