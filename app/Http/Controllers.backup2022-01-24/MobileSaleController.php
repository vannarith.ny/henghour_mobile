<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;
use App\Model\Pos;
use App\Model\Group;
use App\Model\PosGroup;
use App\Model\Report as Report;

class MobileSaleController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    private function getTimeName($timeID){
        $result = '';
        $check = DB::table('tbl_parameter_value')->where('pav_id','=',$timeID)->first();
        if($check){
            $result = $check->pav_value;
        }
        return $result;
    }

    private function getpost($clientId,$timeId){

    	
        $stc_type = 1;

        $getGroups = Group::with('poss')->where('stc_type','=',$stc_type)->orderBy('g_name', 'ASC')->get();
        $posGroups = array();

        foreach ($getGroups as $key => $getGroup) {
            $posNames = array();
            $first = true;
            foreach ($getGroup->poss as $key_pos => $pos) {
                $posTimeName = $this->getTimeName($pos->pos_time);
                $posNames[$posTimeName][$pos->pos_time][] = $pos->pos_name;
            }
//             dd($posNames);

            if(!empty($posNames)){
                foreach ($posNames as $key_posName => $posName) {
                    foreach ($posName as $key_sheet => $sheet) {
                        if($key_sheet == $timeId){
                            $newGroup = array();
                            $newGroup['g_id'] = $getGroup->g_id;
                            $newGroup['g_name'] = $getGroup->g_name;
                            $newGroup['g_time'] = $key_sheet;
                            $newGroup['list_pos'] = rtrim(implode(',', $sheet), ',');
                            $newGroup['g_time_name'] = $key_posName;
                            $newGroup['g_info'] = $getGroup->g_info;
                            array_push($posGroups,  $newGroup);
                        }
                        
                    }
                }
                // dd($posGroups);
            }
        }
        // dd($posGroups);

        // check data user
        $userUses = DB::table('tbl_group_user_disable')->where('s_id',$clientId)->get();
        // dd($userUses);
        $data = array();

        foreach ($posGroups as $key => $value) {
            $checked = '';
            $guName = $value['g_name'];
            foreach ($userUses as $key => $userUse) {
               if($userUse->g_id == $value['g_id']){

                    $newGroup = array();
                    $newGroup['g_id'] = $value['g_id'];
                    $newGroup['g_name'] = $value['g_name'];
                    array_push($data,  $newGroup);
                    
               }
            }
        }

        return $data;


    
    }

    public function VotePrint(Request $request)
    {
        $datelottery = $request->vote['DateCreated'];
        $r_number = $request->vote['Level'];
        $sheet_id = $request->vote['LotteryTimeId'];
        if($sheet_id == 5){
            $timePrint = 'Evening.';
        }else{
            $timePrint = 'Night.';
        }

        $lotterylists = DB::table('tbl_row_mobile')
                    ->leftjoin('tbl_paper_mobile','tbl_row_mobile.p_id','tbl_paper_mobile.p_id')
                    ->where('tbl_paper_mobile.stc_type',1)
                    ->where('tbl_paper_mobile.s_id',Session::get('iduserlotMobileSec'))
                    ->where('tbl_paper_mobile.p_date',$datelottery)
                    ->where('tbl_paper_mobile.p_time',$sheet_id)
                    ->where('tbl_paper_mobile.p_number',1)
                    ->where('tbl_row_mobile.r_number', $r_number)
                    ->get();
        // if row has
        if($lotterylists){

            $oneNum = '';
            $timeShow = date("h:i A d/m/Y", time());
            $oneNum .= 'com.thetinydev.printerdriver://#DH## '; 
            $oneNum .= $timeShow.' : '.Session::get('nameLotMobileSec').'   ##BR##DWDH##DWDH##';

            $totalPriceReal2 = 0;
            $totalPriceReal3 = 0;
            $totalPriceDollar2 = 0;
            $totalPriceDollar3 = 0;

            foreach ($lotterylists as $key => $row) {

                $numberLotterys = DB::table('tbl_number_mobile')
                               ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
                               ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
                               ->where('tbl_number_mobile.r_id',$row->r_id)
                               ->where('tbl_number_mobile.f_print',0)
                               ->orderBy('tbl_number_mobile.num_sort','ASC')
                               ->orderBy('tbl_number_mobile.num_id','ASC')
                               ->get();

                

                

                // if number has
                if(count($numberLotterys) > 0){
                    
                    $checkNewGroup = '';
                    
                    

                    foreach ($numberLotterys as $key => $list) {

                        if($list->num_sort != $checkNewGroup ){
                            $newgroup = 1;
                            $checkNewGroup = $list->num_sort;
                        }else{
                            $newgroup = 0;
                        }

                        // start calculate print and check winner
                        $price_result = Report::calculate( $list->num_number,$list->num_end_number,$list->num_sym,$list->num_reverse,$list->num_price,$list->num_currency,$list->g_id,$row->p_time,$row->p_date,$list->num_type);
                        $val_price = explode("-", $price_result);

                        $oneNum .= $this->getNumberLotteryPrint($list->num_id,$list->g_id,$list->num_number,$list->num_end_number,$list->num_sym,$list->num_reverse,$list->num_price,$list->num_currency,$list->r_id,$row->p_id, $list->num_sort, $newgroup, $list->g_name, $list->num_type );

                        // update print
                        DB::table('tbl_number_mobile')->where('num_id',$list->num_id)->update(['f_print' => 1]);
                        

                        if($list->num_currency == 2){
                             $currencySym = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
                             $displayCurrency = ' '.$currencySym->pav_value;
                             if($val_price[1]=='2'){
                                 $totalPriceDollar2 = $totalPriceDollar2 + $val_price['0'];
                             }else{

                                 $totalPriceDollar3 = $totalPriceDollar3 + $val_price['0'];
                             }

                         }else{

                             if($val_price[1]=='2'){
                                 $totalPriceReal2 = $totalPriceReal2 + $val_price['0'];
                             }else{
                                 $totalPriceReal3 = $totalPriceReal3 + $val_price['0'];
                             }

                        }

                    }  //end loop number

                } //end if r_number



            }


            $totalAmount = ($totalPriceReal2 + $totalPriceReal3);
            $totalAmountDollar = floatval($totalPriceDollar2 + $totalPriceDollar3);
            
            $displayDollar = '';
           

            if($sheet_id == 23 || $sheet_id == 24){
                $sheet_id = 5;
            }
            $water = DB::table('tbl_water')
                    ->where('s_id',Session::get('iduserlotMobileSec'))
                    ->where('time',$sheet_id)
                    ->first();

            $totalPrice2R = round( ($totalPriceReal2*$water->w_2digit)/100 ,2);
            $totalPrice3R = round( ($totalPriceReal3*$water->w_3digit)/100 ,2);
            
            $totalPrice2D = round( ($totalPriceDollar2*$water->w_2digit)/100 ,2);
            $totalPrice3D = round( ($totalPriceDollar3*$water->w_3digit)/100 ,2);

            $totalAmountAfterWaterR = $totalPrice2R + $totalPrice3R;
            $totalAmountAfterWaterD = $totalPrice2D + $totalPrice3D;
            // if($totalAmountDollar > 0){
            //     $oneNum .= 'DWDH##T: '.$totalAmount.':'.$totalAmountDollar.'$##BR##REGULAR##FontB##'.$water->w_2digit.'@PercentSign:##FontA##DWDH##'.$totalAmountAfterWaterR.':'.$totalAmountAfterWaterD.'$##BR##DWDH##FontA##('.$r_number.')##BR##REGULAR##FontB##'.date("h:i A", time()).'##BR##BR##CutFull#';
            // }else{
            //     $oneNum .= 'DWDH##T: '.$totalAmount.'##BR##FontB##'.$water->w_2digit.'@PercentSign:##FontA##DWDH##'.$totalAmountAfterWaterR.'##BR##DWDH##FontA##('.$r_number.')##BR##REGULAR##FontB##'.date("h:i A", time()).'##BR##BR##CutFull#';
            // }

            if($totalAmountDollar > 0 && $totalAmount > 0){
                $oneNum .= 'DWDH##T: '.$totalAmount.':'.$totalAmountDollar.'$##BR##REGULAR##FontB##'.$water->w_2digit.'@PercentSign:##FontA##DWDH##'.$totalAmountAfterWaterR.':'.$totalAmountAfterWaterD.'$##BR##DWDH##FontA##('.$r_number.')##BR##REGULAR##FontB##'.$timePrint.'##BR##BR##CutFull#';
            }else if($totalAmountDollar > 0){
                $oneNum .= 'DWDH##T: '.$totalAmountDollar.'$##BR##REGULAR##FontB##'.$water->w_2digit.'@PercentSign:##FontA##DWDH##'.$totalAmountAfterWaterD.'$##BR##DWDH##FontA##('.$r_number.')##BR##REGULAR##FontB##'.$timePrint.'##BR##BR##CutFull#';

            }else{
                $oneNum .= 'DWDH##T: '.$totalAmount.'##BR##FontB##'.$water->w_2digit.'@PercentSign:##FontA##DWDH##'.$totalAmountAfterWaterR.'##BR##DWDH##FontA##('.$r_number.')##BR##REGULAR##FontB##'.$timePrint.'##BR##BR##CutFull#';
            }
            

            return response([
                'msg' => 'ok',
                'htmlData' => $oneNum,
                'status' => 'success'
            ]);
        }



    }

    public function ChangePost(Request $request)
    {
        $newGID = $request->newGID;

        $pageID = $request->vote['pageID'];
        $rowID = $request->vote['rowID'];
        $numberSort = $request->vote['numberSort'];
        $oldGID = $request->vote['oldGID'];
        $timeID = $request->vote['timeID'];
        $newDate = $request->DateCreate;

        $currentDate = date("Y-m-d");

        $day_of_week = date('N');
        // dd($oldGID);
        $getMinTimePost =  DB::table('tbl_pos_group')
                           ->join('tbl_time_close','tbl_pos_group.pos_id','tbl_time_close.p_id')
                           ->where('tbl_time_close.s_id', Session::get('iduserlotMobileSec'))
                           ->where('tbl_time_close.t_day', $day_of_week)
                           ->where('tbl_time_close.sheet_id', $timeID)
                           ->where(function ($query) use ($oldGID, $newGID) {
                                $query->where('tbl_pos_group.g_id', '=', $oldGID)
                                      ->orWhere('tbl_pos_group.g_id', '=', $newGID);
                            })
                           ->min('t_time');
        if (time() >= strtotime($getMinTimePost)) {
            return response(['msg' => 'ហួសម៉ោង​ មិនអាចលប់បានទេ...','timeNeed' => $getMinTimePost ,'NumSort' => 1, 'status' => 'error']);
        }

        if (strtotime($currentDate) > strtotime($newDate)) {
            return response(['msg' => 'ហួសម៉ោង...1','timeNeed' => $getMinTimePost ,'NumSort' => 1, 'status' => 'error']);
        }

        if($numberSort){

            $check = DB::table('tbl_number_mobile')
                            ->where('r_id', $rowID)
                            ->where('g_id', $oldGID)
                            ->where('num_sort', $numberSort)
                            ->update(
                                ['g_id' => $newGID]
                            );

            return response(['msg' => 'Update success','data' => $check , 'status' => 'success']);
        }


    }

    public function DeleteNumber(Request $request){

        $pageID = $request->pageID;
        $rowID = $request->rowID;
        $numberID = $request->numberID;
        $numberSort = $request->numberSort;
        $gID = $request->gID;
        $timeID = $request->timeID;
        $day_of_week = date('N');

        $getMinTimePost =  DB::table('tbl_pos_group')
                           ->join('tbl_time_close','tbl_pos_group.pos_id','tbl_time_close.p_id')
                           ->where('tbl_time_close.s_id', Session::get('iduserlotMobileSec'))
                           ->where('tbl_time_close.t_day', $day_of_week)
                           ->where('tbl_time_close.sheet_id', $timeID)
                           ->where('tbl_pos_group.g_id', $gID)
                           ->min('t_time');
        if (time() >= strtotime($getMinTimePost)) {
            return response(['msg' => 'ហួសម៉ោង​ មិនអាចលប់បានទេ...','timeNeed' => $getMinTimePost ,'NumSort' => 1, 'status' => 'error']);
        }

        if($numberID){

            $checkprint = DB::table('tbl_number_mobile')->where('num_id', $numberID)->where('f_print', 1)->first();
            if($checkprint){
                return response(['msg' => 'ព្រិនរួច មិនអាចលប់បានទេ...','timeNeed' => $numberID ,'NumSort' => 1, 'status' => 'error']);
            }else{
                $check = DB::table('tbl_number_mobile')->where('num_id', $numberID)->delete();
                return response(['msg' => 'Delete number success','data' => $check , 'status' => 'success']);
            }
            

            
        }else if($numberSort){

            $checkprint = DB::table('tbl_number_mobile')
                        ->where('r_id', $rowID)
                        ->where('g_id', $gID)
                        ->where('num_sort', $numberSort)
                        ->where('f_print', 1)->first();
            if($checkprint){
                return response(['msg' => 'ព្រិនរួច មិនអាចលប់បានទេ...','timeNeed' => $numberSort ,'NumSort' => 1, 'status' => 'error']);
            }else{
                $check = DB::table('tbl_number_mobile')
                            ->where('r_id', $rowID)
                            ->where('g_id', $gID)
                            ->where('num_sort', $numberSort)
                            ->delete();

                return response(['msg' => 'Delete number by sort success','data' => $check , 'status' => 'success']);
            }
            
        }

        



    }

    public function AddNumber(Request $request)
    {
        $data = $request->obj;
        $newDate = $request->vote['DateCreated'];
        $newLavel = $request->vote['Level'];

        $currentDate = date("Y-m-d");
        // get time()
        $day_of_week = date('N');
        $getMinTimePost =  DB::table('tbl_pos_group')
                           ->join('tbl_time_close','tbl_pos_group.pos_id','tbl_time_close.p_id')
                           ->where('tbl_time_close.s_id', Session::get('iduserlotMobileSec'))
                           ->where('tbl_time_close.t_day', $day_of_week)
                           ->where('tbl_time_close.sheet_id', $data['timeID'])
                           ->where('tbl_pos_group.g_id', $data['posGId'])
                           ->min('t_time');
        if (time() >= strtotime($getMinTimePost)) {
            return response(['msg' => 'ហួសម៉ោង...','timeNeed' => $getMinTimePost ,'NumSort' => 1, 'status' => 'error']);
        }
        if (strtotime($currentDate) > strtotime($newDate)) {
            return response(['msg' => 'ហួសម៉ោង...','timeNeed' => $getMinTimePost ,'NumSort' => 1, 'status' => 'error']);
        }

        // check close number
        $getListNumber = $this->get_number_list($data['numStart'],$data['numEnd'],$data['symID'],$data['reverseID']);
        if(count($getListNumber) > 0){ // check number has

            foreach($getListNumber as $key => $listNumber){
                $closeNumber = DB::table('tbl_close_number')
                            ->join('tbl_pos_group','tbl_pos_group.pos_id','tbl_close_number.post_id')
                            ->where('tbl_close_number.sheet_id', $data['timeID'])
                            ->where('tbl_pos_group.g_id', $data['posGId'])
                            ->where('tbl_close_number.c_number', $listNumber)
                            ->first();

                if($closeNumber){

                    $getTotalPrice = $this->get_sum_price_number($listNumber,$closeNumber->post_id,$data['timeID'],$currentDate);

                    // dd($getTotalPrice);

                    if($data['currency'] == 2){  // check Dollar
                        $countPriceIn = floatval($getTotalPrice[1]) + floatval($data['price']);
                        if($closeNumber->price_limit_d == 0){
                            return response(['msg' => 'លេខ'.$listNumber.'ត្រូវបានបិទ...','timeNeed' => $listNumber ,'NumSort' => 1, 'status' => 'error']);
                        }else if(floatval($closeNumber->price_limit_d) < floatval($countPriceIn)){
                            return response(['msg' => 'ទឹកលុយពេញហើយ ('.strval($getTotalPrice[1]).' $)','timeNeed' => $listNumber ,'NumSort' => 1, 'status' => 'error']);
                        // }else if($closeNumber->price_limit_d < (float)$data['price']){
                        //     return response(['msg' => 'ទឹកលុយហួសការកំណត់ ('.$closeNumber->price_limit_d.' $)','timeNeed' => $listNumber ,'NumSort' => 1, 'status' => 'error']);
                        }
                    }else{ /// check Real
                        // dd( floatval($getTotalPrice[0]).'//'.floatval($data['price']));
                        $countPriceIn = floatval($getTotalPrice[0]) + floatval($data['price']);
                        if($closeNumber->price_limit_r == 0){
                            return response(['msg' => 'លេខ'.$listNumber.'ត្រូវបានបិទ...','timeNeed' => $listNumber ,'NumSort' => 1, 'status' => 'error']);
                        }else if(floatval($closeNumber->price_limit_r) < $countPriceIn){
                            return response(['msg' => 'ទឹកលុយពេញហើយ ('.strval($closeNumber->price_limit_r).')','timeNeed' => $listNumber ,'NumSort' => 1, 'status' => 'error']);
                        // }else if($closeNumber->price_limit_r < (float)$data['price']){
                        //     return response(['msg' => 'ទឹកលុយហួសការកំណត់ ('.$closeNumber->price_limit_r.')','timeNeed' => $listNumber ,'NumSort' => 1, 'status' => 'error']);
                        }
                    }
                    
                }
            }
        }
        // dd();

        // check limit price
        // dd($data['price']*100);
        // dd(Session::get('moneyLimit'));
        // if( ((float)Session::get('moneyLimit') > 0) && (float)Session::get('moneyLimit') < (float)$data['price'] ){
        //     return response(['msg' => 'ទឹកប្រាក់មិនត្រូវលេីស '.Session::get('moneyLimit').' ទេ.','timeNeed' => $getMinTimePost ,'NumSort' => 1, 'status' => 'error']);
        // }

       if($data['numStart']){

            // block check limit price
            $countDigit = 2;
            $limitMoney = 0;
            if($data['currency'] == 2){ /// price dollar

                if($data['symID'] == 10){
                    $countDigit = 3;
                }elseif($data['symID'] == 11){
                    $explotData = explode("/", $data['numStart']);
                    if(count($explotData) == '3'){
                        $countDigit = 3;
                    }else{
                        $countDigit = 2;
                    }
                }else{
                    // dd($countDigit);
                    if(strlen($data['numStart']) == '3'){
                        $countDigit = 3;
                    }else{
                        $countDigit = 2;
                    }
                }

                if($countDigit == 3){
                    $limitMoney = (float)Session::get('moneyLimit3DD');
                }else{
                    $limitMoney = (float)Session::get('moneyLimit2DD');
                }

            }else{

                if($data['symID'] == 10){
                    $countDigit = 3;
                }elseif($data['symID'] == 11){
                    $explotData = explode("/", $data['numStart']);
                    if(count($explotData) == '3'){
                        $countDigit = 3;
                    }else{
                        $countDigit = 2;
                    }
                }else{
                    if(strlen($data['numStart']) == '3'){
                        $countDigit = 3;
                    }else{
                        $countDigit = 2;
                    }
                }

                if($countDigit == 3){
                    $limitMoney = (float)Session::get('moneyLimit3DR');
                }else{
                    $limitMoney = (float)Session::get('moneyLimit2DR');
                }
                // dd($limitMoney);

            }

            if( ($limitMoney > 0) && $limitMoney < (float)$data['price'] ){
                return response(['msg' => 'ទឹកប្រាក់មិនត្រូវលេីស '.$limitMoney.' ទេ.','timeNeed' => $getMinTimePost ,'NumSort' => 1, 'status' => 'error']);
            }

            // end block check limit price

            // check page 
            $getPageRowInfo = DB::table('tbl_paper_mobile')
                    ->join('tbl_row_mobile','tbl_paper_mobile.p_id','tbl_row_mobile.p_id')
                    ->where('tbl_paper_mobile.stc_type',1)
                    ->where('tbl_paper_mobile.s_id',Session::get('iduserlotMobileSec'))
                    ->where('tbl_paper_mobile.p_date',$newDate)
                    ->where('tbl_paper_mobile.p_time',$data['timeID'])
                    ->first();

            if($getPageRowInfo == null){
                // insert page
                $pageID = DB::table('tbl_paper_mobile')->insertGetId([
                                'p_date' => $newDate,
                                'p_time' => $data['timeID'],
                                'p_number' => 1,
                                's_id' => Session::get('iduserlotMobileSec'),
                                'stc_type' => 1
                            ]);

                // insert row
                $rowID = DB::table('tbl_row_mobile')->insertGetId([
                                'p_id' => $pageID,
                                'r_number' => 1
                            ]);
            }else{
                // check Lavel
                // var_dump($getPageRowInfo->p_id);
                // var_dump($newLavel);
                $checkLavel = DB::table('tbl_row_mobile')
                                ->where('p_id', $getPageRowInfo->p_id)
                                ->where('r_number', $newLavel)
                                ->first();
                // dd($checkLavel);
                if($checkLavel == null){
                    $rowID = DB::table('tbl_row_mobile')->insertGetId([
                                'p_id' => $getPageRowInfo->p_id,
                                'r_number' => $newLavel
                            ]);
                }else{
                    $rowID = $checkLavel->r_id;
                }
                
            }


            

            $result = DB::table('tbl_number_mobile')->insertGetId([
                            'num_number' => $data['numStart'],
                            'num_sym' => $data['symID'],
                            'num_reverse' => $data['reverseID'],
                            'num_end_number' => $data['numEnd'],
                            'num_price' => $data['price'],
                            'num_currency' => $data['currency'],
                            'g_id' => $data['posGId'],
                            'r_id' => $rowID,
                            'num_sort' => $data['numSort']
                            ,
                            'num_type' => $data['numbertype']
                        ]);

            return response(['msg' => 'ok','data' => $result ,'NumSort' => $data['numSort'], 'status' => 'success']);

       }

        
        
    }

    public function GetMaxLevel(Request $request){

        $date = $request->vote['DateCreated'];
        $sheet_id = 5;
        $ThatTimeMorning ="10:30:00";
        $ThatTimeAfternoon ="13:30:00";
        $ThatTime ="16:30:00";
        if (time() <= strtotime($ThatTimeMorning)) {
          $sheet_id = 23;
        }else if (time() <= strtotime($ThatTimeAfternoon)) {
          $sheet_id = 24;
        }else if (time() <= strtotime($ThatTime)) {
          $sheet_id = 5;
        }else if (time() >= strtotime($ThatTime)) {
          $sheet_id = 6;
        }

        if(isset($request->vote['LotteryTimeId'])){
            $sheet_id = $request->vote['LotteryTimeId'];
        }

        // dd($sheet_id);

        if($date){
            $newDate = date("Y-m-d", strtotime($date));
        }else{
            $newDate = date("Y-m-d");
        }
        
        

        $maxLavel = DB::table('tbl_row_mobile')
                    ->leftjoin('tbl_paper_mobile','tbl_row_mobile.p_id','tbl_paper_mobile.p_id')
                    ->where('tbl_paper_mobile.stc_type',1)
                    ->where('tbl_paper_mobile.s_id',Session::get('iduserlotMobileSec'))
                    ->where('tbl_paper_mobile.p_date',$newDate)
                    ->where('tbl_paper_mobile.p_time',$sheet_id)
                    ->max('tbl_row_mobile.r_number');
        // dd($request->vote);

        $maxNumsort = DB::table('tbl_paper_mobile')
                    ->join('tbl_row_mobile','tbl_paper_mobile.p_id','tbl_row_mobile.p_id')
                    ->join('tbl_number_mobile','tbl_row_mobile.r_id','tbl_number_mobile.r_id')
                    ->where('tbl_paper_mobile.stc_type',1)
                    ->where('tbl_paper_mobile.s_id',Session::get('iduserlotMobileSec'))
                    ->where('tbl_paper_mobile.p_date',$newDate)
                    ->where('tbl_paper_mobile.p_time',$sheet_id)
                    ->max('tbl_number_mobile.num_sort');



        


        if($maxLavel){
            $maxLavel = $maxLavel + 1;
            $maxNumsort = $maxNumsort + 1;
            return response(['msg' => 'ok','Level' => $maxLavel,'NumSort' => $maxNumsort, 'status' => 'success']);
        }else{
            return response(['msg' => 'ok','Level' => 1,'NumSort' => 1, 'status' => 'success']);
        }

        

    }

    public function GetPageList(Request $request)
    {
        $p_number = $request->page;
        $date = $request->date;
        $sheet_id = $request->timeId;

        // win
        $totalWin2DigitReal = 0;
        $totalWin3DigitReal = 0;

        $totalWin2DigitDollar = 0;
        $totalWin3DigitDollar = 0;

        // amount
        $totalPrice2DigitReal = 0;
        $totalPrice3DigitReal = 0;

        $totalPrice2DigitDollar = 0;
        $totalPrice3DigitDollar = 0; 

        //limit rowview
        $limitRow = 15;

        $p_idNeed = '';
        $r_idNeed = '';

        $lotterylists = DB::table('tbl_row_mobile')
                    ->leftjoin('tbl_paper_mobile','tbl_row_mobile.p_id','tbl_paper_mobile.p_id')
                    ->where('tbl_paper_mobile.stc_type',1)
                    ->where('tbl_paper_mobile.s_id',Session::get('iduserlotMobileSec'))
                    ->where('tbl_paper_mobile.p_date',$date)
                    ->where('tbl_paper_mobile.p_time',$sheet_id)
                    ->where('tbl_paper_mobile.p_number',$p_number)
                    ->orderBy('tbl_row_mobile.r_number', 'ASC')
                    ->get();
        // dd($lotterylists);

        // if row has
        if($lotterylists){

            $dataDisplay = '<div class="rowview">';
            $countCheck = 0;
            
            foreach ($lotterylists as $key => $row) {
                
                $numberLotterys = DB::table('tbl_number_mobile')
                               ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
                               ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
                               ->where('tbl_number_mobile.r_id',$row->r_id)
                               ->orderBy('tbl_number_mobile.num_sort','ASC')
                               ->orderBy('tbl_number_mobile.num_id','ASC')
                               ->get();

                

                

                // if number has
                if(count($numberLotterys) > 0){
                    $oneNum = '';
                    $checkNewGroup = '';
                    
                    $totalPriceReal2 = 0;
                    $totalPriceReal3 = 0;
                    $totalPriceDollar2 = 0;
                    $totalPriceDollar3 = 0;
                    $total2N3DigitReal = 0;
                    $total2N3DigitDollar = 0;

                    foreach ($numberLotterys as $key => $list) {
                        $countCheck++;
                        $p_idNeed = $row->p_id;
                        $r_idNeed = $row->r_id;
                        if($list->num_sort != $checkNewGroup ){
                            $newgroup = 1;
                            $checkNewGroup = $list->num_sort;
                        }else{
                            $newgroup = 0;
                        }

                        // start calculate print and check winner
                        $price_result = Report::calculate( $list->num_number,$list->num_end_number,$list->num_sym,$list->num_reverse,$list->num_price,$list->num_currency,$list->g_id,$row->p_time,$row->p_date,$list->num_type);
                        $val_price = explode("-", $price_result);

                        $oneNum .= $this->getNumberLottery($list->num_id,$list->g_id,$list->num_number,$list->num_end_number,$list->num_sym,$list->num_reverse,$list->num_price,$list->num_currency,$list->r_id,$row->p_id, $list->num_sort, $newgroup, $val_price[2], $list->f_pay,$list->num_type );


                        

                        if($list->num_currency == 2){
                             $currencySym = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
                             $displayCurrency = ' '.$currencySym->pav_value;
                             if($val_price[1]=='2'){
                                 $totalPriceDollar2 = $totalPriceDollar2 + $val_price['0'];
                                 $totalWin2DigitDollar = $totalWin2DigitDollar + ($list->num_price * $val_price[2]);
                             }else{

                                 $totalPriceDollar3 = $totalPriceDollar3 + $val_price['0'];
                                 $totalWin3DigitDollar = $totalWin3DigitDollar + ($list->num_price * $val_price[2]);
                             }

                         }else{

                             if($val_price[1]=='2'){
                                 $totalPriceReal2 = $totalPriceReal2 + $val_price['0'];
                                 $totalWin2DigitReal = $totalWin2DigitReal + ($list->num_price * $val_price[2]);
                             }else{
                                 $totalPriceReal3 = $totalPriceReal3 + $val_price['0'];
                                 $totalWin3DigitReal = $totalWin3DigitReal + ($list->num_price * $val_price[2]);
                             }

                        }

                    }  //end loop number

                    $total2N3DigitReal = ($totalPriceReal2 + $totalPriceReal3);
                    $total2N3DigitDollar = floatval($totalPriceDollar2 + $totalPriceDollar3);
                    $displayDollar = '';
                    if($total2N3DigitDollar > 0){
                        $displayDollar = ' / '.$total2N3DigitDollar.'$';
                    }

                    // amount count
                    $totalPrice2DigitReal = $totalPrice2DigitReal + $totalPriceReal2;
                    $totalPrice3DigitReal = $totalPrice3DigitReal + $totalPriceReal3;

                    $totalPrice2DigitDollar = $totalPrice2DigitDollar + $totalPriceDollar2;
                    $totalPrice3DigitDollar = $totalPrice3DigitDollar + $totalPriceDollar3;



                    // block display total by row number
                    $oneNum .= '<div class="totalprice needPadding">
                                    <div class="labeltitle">'.$row->r_number.' = </div>
                                    <div class="valuedisplay">'.$total2N3DigitReal.$displayDollar.'</div>
                                    <div class="clear"></div>
                               </div>';

                    $dataDisplay .=  $oneNum;

                    if($countCheck >= $limitRow ){
                        $countCheck = 0;
                        $dataDisplay .=  '</div><div class="rowview">';
                    }

                    
                } //end if r_number



            }

            $dataDisplay .=  '</div><div class="clear"></div>'; 

            $totalAmount = ($totalPrice2DigitReal + $totalPrice3DigitReal);
            $totalAmountDollar = $totalPrice2DigitDollar + $totalPrice3DigitDollar;
            $displayDollar = '';
            if($totalAmountDollar > 0){
                $displayDollar = ' / '.$totalAmountDollar.'$';
            }
            $displayDollar2Dollar = ''; 
            if($totalPrice2DigitDollar > 0){
                $displayDollar2Dollar = ' / '.$totalPrice2DigitDollar.'$';
            }
            $displayDollar3Dollar = '';
            if($totalPrice3DigitDollar > 0){
                $displayDollar3Dollar = ' / '.$totalPrice3DigitDollar.'$';
            }


            $totalAmount = '
                <div class="totalfooter">
                    <div class="rowLeft">សរុប : '.$totalAmount.$displayDollar.'</div>
                    <div class="rowLeft">
                        <div>2លេខ : '.($totalPrice2DigitReal).$displayDollar2Dollar.'</div>
                        <div>3លេខ : '.($totalPrice3DigitReal).$displayDollar3Dollar.'</div>
                    </div>
                    <div class="rowLeft">
                        <div class="winCSS">ត្រូវ2លេខ : '.($totalWin2DigitReal).' / '.$totalWin2DigitDollar.'$</div>
                        <div class="winCSS">ត្រូវ3លេខ : '.($totalWin3DigitReal).' / '.$totalWin3DigitDollar.'$</div>
                    </div>
                    <div class="clear"></div>
                </div>
            ';
            $dataDisplay .= $totalAmount;


            // store to db for result
            if($p_idNeed != '' && $r_idNeed != ''){

                $price2digit_r = $totalPrice2DigitReal;
                $price2digit_d = $totalPrice2DigitDollar;
                $price3digit_r = $totalPrice3DigitReal;
                $price3digit_d = $totalPrice3DigitDollar;

                $win2digit_r = $totalWin2DigitReal;
                $win2digit_d = $totalWin2DigitDollar;
                $win3digit_r = $totalWin3DigitReal;
                $win3digit_d = $totalWin3DigitDollar;

                $s_id = Session::get('iduserlotMobileSec');
                $stc_type = 1;

                $checkRecord = DB::table("tbl_sum_by_paper")
                                ->where('stc_type', $stc_type)
                                ->where('s_id', $s_id)
                                ->where('date', $date)
                                ->where('sheet_id', $sheet_id)
                                // ->where('p_id', $p_idNeed)
                                ->delete();
                if($checkRecord){
                    // $totalStore = DB::table("tbl_sum_by_paper")
                    //     ->where('id', $checkRecord->id)
                    //     ->update([

                    //         'price2digit_r' => $price2digit_r,
                    //         'price2digit_d' => $price2digit_d,
                    //         'price3digit_r' => $price3digit_r,
                    //         'price3digit_d' => $price3digit_d,

                    //         'win2digit_r' => $win2digit_r,
                    //         'win2digit_d' => $win2digit_d,
                    //         'win3digit_r' => $win3digit_r,
                    //         'win3digit_d' => $win3digit_d,
                    //     ]);
                    $totalStore = DB::table("tbl_sum_by_paper")
                        ->insertGetId([

                            'price2digit_r' => $price2digit_r,
                            'price2digit_d' => $price2digit_d,
                            'price3digit_r' => $price3digit_r,
                            'price3digit_d' => $price3digit_d,

                            'win2digit_r' => $win2digit_r,
                            'win2digit_d' => $win2digit_d,
                            'win3digit_r' => $win3digit_r,
                            'win3digit_d' => $win3digit_d,

                            's_id' => $s_id,
                            'stc_type' => $stc_type,
                            'date' => $date,
                            'sheet_id' => $sheet_id,
                            'p_id' => $p_idNeed
                        ]);
                }else{
                    $totalStore = DB::table("tbl_sum_by_paper")
                        ->insertGetId([

                            'price2digit_r' => $price2digit_r,
                            'price2digit_d' => $price2digit_d,
                            'price3digit_r' => $price3digit_r,
                            'price3digit_d' => $price3digit_d,

                            'win2digit_r' => $win2digit_r,
                            'win2digit_d' => $win2digit_d,
                            'win3digit_r' => $win3digit_r,
                            'win3digit_d' => $win3digit_d,

                            's_id' => $s_id,
                            'stc_type' => $stc_type,
                            'date' => $date,
                            'sheet_id' => $sheet_id,
                            'p_id' => $p_idNeed
                        ]);
                }

                
            }
            


            return response([
                'msg' => 'ok',
                'htmlData' => $dataDisplay,
                'tottotalPriceReal2' => $totalPrice2DigitDollar,
                'tottotalPriceReal3' => $totalPrice3DigitReal,
                'tottotalPriceDollar2' => $totalPrice2DigitDollar,
                'tottotalPriceDollar3' => $totalPrice3DigitDollar,

                'totalWin2Real' => $totalWin2DigitReal,
                'totalWin3Real' => $totalWin3DigitReal,
                'totalWin2Dollar' => $totalWin2DigitDollar,
                'totalWin3Dollar' => $totalWin3DigitDollar,

                'status' => 'success'
            ]);
        }
        
    }

    public function GetList(Request $request){
    	$maxLavel = $request->vote['Level'];
    	$date = $request->vote['DateCreated'];
        $sheet_id = $request->vote['LotteryTimeId'];
        $newDate = date("Y-m-d", strtotime($date));

        // $maxLavel = DB::table('tbl_paper_mobile')
        //             ->join('tbl_row_mobile','tbl_paper_mobile.p_id','tbl_row_mobile.p_id')
        //             ->where('tbl_paper_mobile.stc_type',1)
        //             ->where('tbl_paper_mobile.s_id',Session::get('iduserlotMobileSec'))
        //             ->where('tbl_paper_mobile.p_date',$newDate)
        //             ->where('tbl_paper_mobile.p_time',$sheet_id)
        //             ->max('tbl_row_mobile.r_number');

        $lotterylists = DB::table('tbl_paper_mobile')
                    ->leftjoin('tbl_row_mobile','tbl_paper_mobile.p_id','tbl_row_mobile.p_id')
                    ->where('tbl_paper_mobile.stc_type',1)
                    ->where('tbl_paper_mobile.s_id',Session::get('iduserlotMobileSec'))
                    ->where('tbl_paper_mobile.p_date',$newDate)
                    ->where('tbl_paper_mobile.p_time',$sheet_id)
                    ->where('tbl_row_mobile.r_number',$maxLavel)
                    ->first();
                    // dd($lotterylists);

        $htmlData = array();

        $total_twodigit_r = 0;
        $total_twodigit_s = 0;
        $total_threedigit_r = 0;
        $total_threedigit_s = 0;

        $price_right_twodigit_r = 0;
        $price_right_threedigit_r = 0;
        $price_right_twodigit_s = 0;
        $price_right_threedigit_s = 0;

        $WaterCustomer = [];

        if(isset($lotterylists->r_id)){

        
            $numberLotterys = DB::table('tbl_number_mobile')
                               ->leftjoin('tbl_parameter_value', 'tbl_number_mobile.num_sym','=','tbl_parameter_value.pav_id')
                               ->leftjoin('tbl_group', 'tbl_number_mobile.g_id','=','tbl_group.g_id')
                               ->where('tbl_number_mobile.r_id',$lotterylists->r_id)
                               ->orderBy('tbl_number_mobile.num_sort','DESC')
                               ->orderBy('tbl_number_mobile.num_id','DESC')
                               ->get();

            // dd($numberLotterys);
            if(count($numberLotterys) > 0){
                $checkNewGroup = '';

                

                foreach ($numberLotterys as $key => $list) {
                    if($list->num_sort != $checkNewGroup ){
                        $newgroup = 1;
                        $checkNewGroup = $list->num_sort;
                    }else{
                        $newgroup = 0;
                    }
                    $htmlData[] = $this->getNumberLottery($list->num_id,$list->g_id,$list->num_number,$list->num_end_number,$list->num_sym,$list->num_reverse,$list->num_price,$list->num_currency,$list->r_id,$lotterylists->p_id, $list->num_sort, $newgroup, 0, $list->f_pay,$list->num_type );


                    // start calculate print and check winner
                    $price_result = Report::calculate( $list->num_number,$list->num_end_number,$list->num_sym,$list->num_reverse,$list->num_price,$list->num_currency,$list->g_id,$lotterylists->p_time,$lotterylists->p_date,$list->num_type);
                    // dd($price_result);
                    $val_price = explode("-", $price_result);

                    if($list->num_currency == 2){
                         $currencySym = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
                         $displayCurrency = ' '.$currencySym->pav_value;
                         if($val_price[1]=='2'){
                             $total_twodigit_s = $total_twodigit_s + $val_price['0'];
                             $price_right_twodigit_s = $price_right_twodigit_s + ($list->num_price * $val_price[2]);
                         }else{
                             $total_threedigit_s = $total_threedigit_s + $val_price['0'];
                             $price_right_threedigit_s = $price_right_threedigit_s + ($list->num_price * $val_price[2]);
                         }

                     }else{

                         if($val_price[1]=='2'){
                             $total_twodigit_r = $total_twodigit_r + $val_price['0'];
                             $price_right_twodigit_r = $price_right_twodigit_r + ($list->num_price * $val_price[2]);
                         }else{
                             $total_threedigit_r = $total_threedigit_r + $val_price['0'];
                             $price_right_threedigit_r = $price_right_threedigit_r + ($list->num_price * $val_price[2]);
                         }

                    }
                    // start calculate print and check winner

                }  //end loop number

                // start get water
                if(Session::get('cutwaterSec') == 1){
                    $water = DB::table('tbl_water')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('time',$sheet_id)
                            ->first();
                    $WaterCustomer = $water;
                }
                
                
                
            }
        }

        $infoTotalPrice = [
                        'TWDigitKhmer' => $total_twodigit_r,
                        'THDigitKhmer' => $total_threedigit_r,
                        'TWDigitDollar' => $total_twodigit_s,
                        'THDigitDollar' => $total_threedigit_s];
        $infoPriceWinner = [
                        'Win2DigitKhmer' => $price_right_twodigit_r,
                        'Win3DigitKhmer' => $price_right_threedigit_r,
                        'Win2DigitDollar' => $price_right_twodigit_s,
                        'Win3DigitDollar' => $price_right_threedigit_s
                      ];

        // dd($htmlData);
    	return response([
                'msg' => 'ok',
                'maxLavel' => $maxLavel,
                'htmlData' => $htmlData,
                'infoTotalPrice' => $infoTotalPrice,
                'infoPriceWinner' => $infoPriceWinner,
                'waterCustomer' => $WaterCustomer,
                'displayCutWater' => Session::get('cutwaterSec'),
                'status' => 'success'
            ]);
    }

    public function SetDefaultTime(Request $request){
        $clientId = Session::get('iduserlotMobileSec');
        $sheet_id = $request->vote['LotteryTimeId'];
        $date = $request->vote['DateCreated'];
        $level = $request->vote['Level'];

        $posts = $this->getpost($clientId,$sheet_id);

        $Html = '';
        $PostVote = []; 
        $totalPost = count($posts);
        foreach ($posts as $key => $post) {
            if($key == 0){
                $Html .= '<tr align="center" id="post_tr_'.$key.'" style="height: 143px;">';
                $PostVote['Name'] = $post['g_name'];
                $PostVote['Id'] = $post['g_id'];
            }

            if( ($key+1) % 4 == 0 ){
                    $Html .= '<td class="posts_font2" style="color:#000" width="25%" data-id="'.$post['g_id'].'" data-digit="">'.$post['g_name'].'</td>
                </tr>
                <tr align="center" id="post_tr_{{$key}}" style="height: 143px;">';
            }else{
                    $Html .= '<td class="posts_font2" style="color:#000" width="25%" data-id="'.$post['g_id'].'" data-digit="">'.$post['g_name'].'</td>';
            }
            
            if( ($key+1) == $totalPost ){
                $Html .= '</tr>';
            }
        }

        return response(['msg' => 'ok','Html' => $Html,'PostVote' => (object) $PostVote, 'Vote' => $request->vote, 'status' => 'success']);
    }

    public function index(Request $request){
    	
    	$page = 'sale';
    	$stc_type = 1;

    	$sheets = DB::table('tbl_parameter_value')->where('pat_id',3)->get();
    	$sheet_id = 23;

  //   	$ThatTime ="16:40:00";
		// if (time() >= strtotime($ThatTime)) {
		//   $sheet_id = 6;
		// }

        if (time() <= strtotime('10:40:00')) {
            $sheet_id = 23;
        }elseif (time() <= strtotime('13:40:00')) {
            $sheet_id = 24;
        }elseif (time() <= strtotime('16:40:00')) {
            $sheet_id = 5;
        }else{
            $sheet_id = 6;
        }

		$sheetName =  DB::table('tbl_parameter_value')->where('pav_id',$sheet_id)->first();

		$clientId = Session::get('iduserlotMobileSec');

		$posts = $this->getpost($clientId,$sheet_id);
        $PostVote = [];
    	foreach ($posts as $key => $post) {
            if($key == 0){
                $PostVote['Name'] = $post['g_name'];
                $PostVote['Id'] = $post['g_id'];
            }
        }
    	
        $PostVote =  (object) $PostVote;
        

        if(isset($PostVote->Id)){
            
        }else{
            $PostVote->Id = '';
            $PostVote->Name = '';
        }
        // dd($PostVote);
    	return view("mobile.sale.index", compact('page','sheets','sheetName','sheet_id','posts','PostVote'));
    }

    private function getNumberLotteryPrint($num_id,$g_id,$number_start,$number_end,$sym_id,$num_reverse,$num_price,$num_currency,$r_id,$page_id, $num_sort, $checkNewGroup, $grounName,$numType)
    {
        $num_number = $number_start;
        $count_num_start = strlen($num_number);
        if($count_num_start <= 3){
            $num_end_number = $number_end;
        }else{
            $num_end_number = '';
        }
        $num_sym = $sym_id;
        $num_price = $num_price;

            $numberDisplay = '';  //display number to fromte
            $displayCurrency = '';

            

            if($num_currency == 2){
                $currencyDisplay = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
                $displayCurrency = $currencyDisplay->pav_value;
                $num_price = $num_price;
            }

            if($checkNewGroup == 1){
                $numberDisplay .= '     '.$grounName.'##BR##';
                // $groupNameDisplay = DB::table('tbl_group')->where('g_id',$g_id)->first();
                // $numberDisplay = '<div class="pos_style" id="pos_style_'.$num_sort.'" colume="'.$r_id.'" page="'.$page_id.'" numberSort="'.$num_sort.'" g-id="'.$g_id.'">
                //                     <span class="pos_style_post"  colume="'.$r_id.'" page="'.$page_id.'" numberSort="'.$num_sort.'" g-id="'.$g_id.'">'.$groupNameDisplay->g_name.'</span>';

                // $numberDisplay .= '<div class="optionNumberMain" colume="'.$r_id.'" page="'.$page_id.'" numberSort="'.$num_sort.'" g-id="'.$g_id.'">
                //         <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                //     </div>';

                // $numberDisplay .='</div>';
            }

            // $numberDisplay .= $checkGroupDisplay.'////'.$g_id.'///'.$r_id.'//'.$num_sort;

            if($num_sym == 7){ //check sym is -

                if($num_reverse == 1){
                    $numberDisplay .= '       '.$num_number.'X'.round($num_price,2).$displayCurrency.'##BR##';
                }else{
                    $numberDisplay .= '       '.$num_number.':'.round($num_price,2).$displayCurrency.'##BR##';
                }

            }else if($num_sym == 8){

                if($num_reverse == 1){
                    $end_number_new = '';
                    if($num_end_number == ''){
                        $end_number_new = substr($num_number, 0, -1).'9';
                    }else{
                        $end_number_new = $num_end_number;
                    }

                    $numberDisplay .= '       '.$num_number.'X'.round($num_price,2).$displayCurrency.'##BR##REGULAR##FontB##              ('.$this->calculate_number($num_number,$end_number_new,1,$numType).')##DWDH##FontA##@StraightLine##BR##DWDH##FontA##       '.$end_number_new.'X'.round($num_price,2).$displayCurrency.'##BR##DWDH##';

                   
                }else{
                    $check = substr($num_number, -1);
                    if($check == '0' && $num_end_number == ''){
                        $numberDisplay .= '       '.$num_number.'>'.round($num_price,2).$displayCurrency.'##BR##DWDH##';
                    }else{
                        $end_number_new = '';
                        if($num_end_number == ''){
                            $end_number_new = substr($num_number, 0, -1).'9';
                        }else{
                            $end_number_new = $num_end_number;
                        }

                        $numberDisplay .= '       '.$num_number.':'.round($num_price,2).$displayCurrency.'##BR##REGULAR##FontB##              ('.$this->calculate_number($num_number,$end_number_new,0,$numType).')##DWDH##FontA##@StraightLine##BR##DWDH##FontA##       '.$end_number_new.':'.round($num_price,2).$displayCurrency.'##BR##DWDH##';
                    }
                     
                }
            }else if($num_sym == 10){
                $numberDisplay .= '       '.$num_number.'X'.round($num_price,2).$displayCurrency.'##BR##DWDH##';

            }else if($num_sym == 11){ //check sym is alot digit to 3 digit

                $numberDisplay .= '       '.$num_number.'X'.round($num_price,2).$displayCurrency.'##BR##DWDH##';

                $numberDisplay .='
                <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                    <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                        <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                    </div>';
                $explotData = explode("/", $num_number);
                $leftData = '';
                $centerData = '';
                $rightData = '';
                if(isset($explotData[0])){
                    $leftData =$explotData[0];
                    $numberDisplay .= '       '.$leftData.'##BR##DWDH##';
                }
                
                if(isset($explotData[1])){
                    $centerData = $explotData[1];
                    $numberDisplay .= '       '.$centerData.'##BR##DWDH##';
                }

                if(isset($explotData[2])){
                    $rightData = $explotData[2];
                    $numberDisplay .= '       '.$rightData.'##BR##DWDH##';
                }

                $numberDisplay .= '##DWDH##FontA##@StraightLine##BR##DWDH##FontA##       '.$this->calculate_totalnumber_new($num_number, 0).'X'.round($num_price,2).$displayCurrency.'##BR##DWDH##';

            }else{
                if($num_reverse == 1){

                    $numberDisplay .= '       '.$num_number.'X'.round($num_price,2).$displayCurrency.'##BR##REGULAR##FontB##              ('.$this->calculate_number($num_number,$num_end_number,1,$numType).')##DWDH##FontA##@StraightLine##BR##DWDH##FontA##       '.$num_end_number.'X'.round($num_price,2).$displayCurrency.'##BR##DWDH##';

                }else{
                    $numberDisplay .= '       '.$num_number.':'.round($num_price,2).$displayCurrency.'##BR##REGULAR##FontB##              ('.$this->calculate_number($num_number,$num_end_number,0,$numType).')##DWDH##FontA##@StraightLine##BR##DWDH##FontA##       '.$num_end_number.':'.round($num_price,2).$displayCurrency.'##BR##DWDH##';
                    
                }
            }

            

        return $numberDisplay;  
    }

    private function getNumberLottery($num_id,$g_id,$number_start,$number_end,$sym_id,$num_reverse,$num_price,$num_currency,$r_id,$page_id, $num_sort, $checkNewGroup, $numWinner, $payed, $numType){

        // $g_id = $request->pos_id;
        // $checkNewGroup = 0;
        $num_number = $number_start;
        $count_num_start = strlen($num_number);
        if($count_num_start <= 3){
            $num_end_number = $number_end;
        }else{
            $num_end_number = '';
        }
        $num_sym = $sym_id;

        $num_right = "";
        if($payed == 1){
            $actived = 'payed';

            if($numWinner>1){
                $num_right = '<div class="right_num"><b>* '.$numWinner.'</b></div>';
             }
        }else if($numWinner > 0){

             $actived = 'actived';
             
             if($numWinner>1){
                $num_right = '<div class="right_num"><b>* '.$numWinner.'</b></div>';
             }

        }else{
             $actived = '';
        }



            $numberDisplay = '';  //display number to fromte
            $displayCurrency = '';
            if($num_currency == 2){
                $currencyDisplay = DB::table('tbl_parameter_value')->where('pav_id',2)->first();
                $displayCurrency = $currencyDisplay->pav_value;
            }

            if($checkNewGroup == 1){
                $groupNameDisplay = DB::table('tbl_group')->where('g_id',$g_id)->first();
                $numberDisplay = '<div class="pos_style" id="pos_style_'.$num_sort.'" colume="'.$r_id.'" page="'.$page_id.'" numberSort="'.$num_sort.'" g-id="'.$g_id.'">
                                    <span class="pos_style_post"  colume="'.$r_id.'" page="'.$page_id.'" numberSort="'.$num_sort.'" g-id="'.$g_id.'">'.$groupNameDisplay->g_name.'</span>';

                $numberDisplay .= '<div class="optionNumberMain" colume="'.$r_id.'" page="'.$page_id.'" numberSort="'.$num_sort.'" g-id="'.$g_id.'">
                        <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                    </div>';

                $numberDisplay .='</div>';
            // }else{
            //     $checkGroupDisplay =  DB::table('tbl_number_mobile')
            //                             ->where('r_id',$r_id)
            //                             ->where('g_id',$g_id)
            //                             ->where('num_sort',$num_sort)
            //                             ->count();
            //     if($checkGroupDisplay <= 1 ){
            //         $groupNameDisplay = DB::table('tbl_group')->where('g_id',$g_id)->first();
            //         $numberDisplay = '<div class="pos_style" id="pos_style_'.$num_sort.'">'.$groupNameDisplay->g_name.'</div>';
            //     }
            }

            // $numberDisplay .= $checkGroupDisplay.'////'.$g_id.'///'.$r_id.'//'.$num_sort;
            if($num_sym == 7){ //check sym is -
                $numberDisplay .='
                <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                    <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                        <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                    </div>
                    <div class="number_lot lot_int">'.$num_number.'</div>';
                    if($num_reverse == 1){
                        $numberDisplay .= '<div class="symbol_lot lot_int">x</div>';
                    }else{
                        $numberDisplay .= '<div class="symbol_lot lot_int">-</div>';
                    }
                    
                $numberDisplay .= '
                    <div class="amount_lot lot_int">'.round($num_price,2).$displayCurrency.'</div>
                    <div class="clear"></div>
                    '.$num_right.'
                </div>
                '; 
            }else if($num_sym == 8){

                if($num_reverse == 1){
                    $end_number_new = '';
                    if($num_end_number == ''){
                        $end_number_new = substr($num_number, 0, -1).'9';
                    }else{
                        $end_number_new = $num_end_number;
                    }
                    $numberDisplay .='
                    <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                        <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                            <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                        </div>
                        <div class="number_lot lot_int">'.$num_number.'</div>
                        <div class="symbol_lot lot_int">x</div>
                        <div class="clear"></div>
                        <div class="display_total_number">'.$this->calculate_number($num_number,$end_number_new,1,$numType).'</div>
                        <div class="number_lot lot_int clear_margin">
                            |
                            <div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                            <div class=" lot_int sym_absolube_amount">'.round($num_price,2).$displayCurrency.'</div>
                        </div>
                        <div class="clear"></div>
                        <div class="number_lot lot_int">'.$end_number_new.'</div>
                        <div class="symbol_lot lot_int">x</div>
                        <div class="clear"></div>
                        '.$num_right.'
                    </div>
                    '; 
                }else{
                    $check = substr($num_number, -1);
                    if($check == '0' && $num_end_number == ''){
                        $numberDisplay .='
                            <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                                <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                                    <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                                </div>
                                <div class="number_lot lot_int">'.$num_number.'</div>
                                <div class="symbol_lot lot_int"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                                <div class="amount_lot lot_int">'.round($num_price,2).$displayCurrency.'</div>
                                <div class="clear"></div>
                                '.$num_right.'
                            </div>
                            ';
                    }else{
                        $end_number_new = '';
                        if($num_end_number == ''){
                            $end_number_new = substr($num_number, 0, -1).'9';
                        }else{
                            $end_number_new = $num_end_number;
                        }
                        $numberDisplay .='
                        <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                            <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                                <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                            </div>
                            <div class="number_lot lot_int">'.$num_number.'</div>
                            <div class="clear"></div>
                            <div class="display_total_number">'.$this->calculate_number($num_number,$end_number_new,0,$numType).'</div>
                            <div class="number_lot lot_int clear_margin">
                                |
                                <div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                                <div class=" lot_int sym_absolube_amount">'.round($num_price,2).$displayCurrency.'</div>
                            </div>
                            <div class="clear"></div>
                            <div class="number_lot lot_int">'.$end_number_new.'</div>
                            <div class="clear"></div>
                            '.$num_right.'
                        </div>
                        '; 
                    }
                     
                }
            }else if($num_sym == 10){
                $numberDisplay .='
                <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                    <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                        <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                    </div>
                    <div class="number_lot lot_int newdigitalot">'.$num_number.'</div>';
                    if($num_reverse == 1){
                        $numberDisplay .= '<div class="symbol_lot lot_int">x</div>';
                    }else{
                        $numberDisplay .= '<div class="symbol_lot lot_int">-</div>';
                    }
                    
                $numberDisplay .= '
                    <div class="amount_lot lot_int newdigitalot_price">'.round($num_price,2).$displayCurrency.'</div>
                    <div class="clear"></div>
                    '.$num_right.'
                </div>
                ';
            }else if($num_sym == 11){ //check sym is alot digit to 3 digit
                $numberDisplay .='
                <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                    <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                        <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                    </div>';
                $explotData = explode("/", $num_number);
                $leftData = '';
                $centerData = '';
                $rightData = '';
                if(isset($explotData[0])){
                    $explotData_sub = explode(",", $explotData[0]);
                    foreach ($explotData_sub as $key => $value_sub) {
                        if($value_sub != ''){
                            $leftData .='<li>'.$value_sub.'</li>';
                        }
                    }
                }
                
                if(isset($explotData[1])){
                    $explotData_sub = explode(",", $explotData[1]);
                    foreach ($explotData_sub as $key => $value_sub) {
                        if($value_sub != ''){
                            $centerData .='<li>'.$value_sub.'</li>';
                        }
                        
                    }
                }

                if(isset($explotData[2])){
                    $explotData_sub = explode(",", $explotData[2]);
                    foreach ($explotData_sub as $key => $value_sub) {
                        if($value_sub != ''){
                            $rightData .='<li>'.$value_sub.'</li>';
                        }
                    }
                }

                if($leftData == ''){
                    $leftData = $centerData;
                    $centerData = $rightData;
                    $rightData = '';
                }else if($centerData == ''){
                    $centerData = $rightData;
                    $rightData = '';
                }

                $labelChink = '';

                if($numType == 1){
                    $labelChink = '<div class="display_chink">ដកឌុប</div>';
                }

                $numberDisplay .= '
                    <div class="row_lottery_list_new">
                        <div class="txt_left">
                            <ul>
                                '.$leftData.'
                            </ul>
                        </div>
                        <div class="txt_center">
                            <ul>
                                '.$centerData.'
                            </ul>
                        </div>
                        <div class="txt_right">
                           <ul>
                                '.$rightData.'
                            </ul>
                        </div>
                        <div class="clear"></div>
                        '.$labelChink.'
                    </div>
                ';
                   
                    
                $numberDisplay .= '
                    <div class="number_lot lot_int newdigitalot_price">'.$this->calculate_totalnumber_new($num_number,$numType).'</div>
                    <div class="symbol_lot lot_int ">x</div>
                    <div class="amount_lot lot_int newdigitalot_price">'.round($num_price,2).$displayCurrency.'</div>
                    <div class="clear"></div>
                    '.$num_right.'
                </div>
                '; 
            }else{
                if($num_reverse == 1){
                    $numberDisplay .='
                        <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                            <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                                <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                            </div>
                            <div class="number_lot lot_int">'.$num_number.'</div>
                            <div class="symbol_lot lot_int">x</div>
                            <div class="clear"></div>
                            <div class="number_lot lot_int clear_margin">
                                |
                                <div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                                <div class=" lot_int sym_absolube_amount">'.round($num_price,2).$displayCurrency.'</div>
                            </div>
                            <div class="clear"></div>
                            <div class="number_lot lot_int">'.$num_end_number.'</div>
                            <div class="symbol_lot lot_int">x</div>
                            <div class="clear"></div>
                            '.$num_right.'
                        </div>
                        '; 
                }else{
                    $numberDisplay .='
                        <div order="'.$num_sort.'" class="row_main '.$actived.' needgetgroup_'.$num_sort.'" number="'.$num_id.'" id="row_main_'.$num_id.'">
                            <div class="optionNumber" colume="'.$r_id.'" page="'.$page_id.'" number="'.$num_id.'" g-id="'.$g_id.'">
                                <i class="fa fa-times txt-color-red eventDeletenumberAny" aria-hidden="true"></i>
                            </div>
                            <div class="number_lot lot_int">'.$num_number.'</div>
                            <div class="clear"></div>
                            <div class="number_lot lot_int clear_margin">
                                |
                                <div class="sym_absolube"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                                <div class=" lot_int sym_absolube_amount">'.round($num_price,2).$displayCurrency.'</div>
                            </div>
                            <div class="clear"></div>
                            <div class="number_lot lot_int">'.$num_end_number.'</div>
                            <div class="clear"></div>
                            '.$num_right.'
                        </div>
                        '; 
                }
            }

            

        return $numberDisplay;  
        
    }

    private function calculate_totalnumber_new($num_start, $type) {
        $totalNumber = 0;
        // $list3Digit = '';
        $number = explode("/", $num_start);
        // var_dump($number);
        if($number[0] != '' && $number[1] != '' && $number[2] != ''){
            $left = explode(",", $number[0]);
            $center = explode(",", $number[1]);
            $right = explode(",", $number[2]);
            foreach ($left as $key => $value1) {
                if($value1 != ''){
                    foreach ($center as $key => $value2) {
                        if($value2 != ''){
                            foreach ($right as $key => $value3) {
                                if($value3 != ''){
                                    if($type == 1 && $value1 == $value2 && $value2 == $value3){

                                    }else if($type == 2 && ( 
                                                        ($value1 == $value2 && $value1 != $value3) || 
                                                        ($value1 != $value2 && $value1 == $value3) ||
                                                        ($value1 != $value2 && $value1 != $value3)
                                            ) ){

                                    }else{
                                        $totalNumber++;
                                    }
                                    // $totalNumber++;
                                }
                            }
                        }
                        
                    }
                }
                
            }
        }else if($number[0] != '' && $number[1] != '' && $number[2] == ''){
            $left = explode(",", $number[0]);
            $center = explode(",", $number[1]);
            foreach ($left as $key => $value1) {
                if($value1 != ''){
                    foreach ($center as $key => $value2) {
                        if($value2 != ''){
                            if($type == 1 && $value1 == $value2){

                            }else if($type == 2 && $value1 != $value2){

                            }else{
                                $totalNumber++;
                            }
                        }
                        
                    }
                }
                
            }
        }else if($number[0] != '' && $number[1] == '' && $number[2] != ''){
            $left = explode(",", $number[0]);
            $right = explode(",", $number[2]);
            foreach ($left as $key => $value1) {
                if($value1 != ''){
                    foreach ($right as $key => $value2) {
                        if($value2 != ''){
                            $totalNumber++;
                        }
                        
                    }
                }
                
            }
        }else if($number[0] == '' && $number[1] != '' && $number[2] != ''){
            $center = explode(",", $number[1]);
            $right = explode(",", $number[2]);
            foreach ($center as $key => $value1) {
                if($value1 != ''){
                    foreach ($right as $key => $value2) {
                        if($value2 != ''){
                            $totalNumber++;
                        }
                        
                    }
                }
                
            }
        }
        // var_dump($totalNumber);
        return $totalNumber;
    }

    private function calculate_number($num_start,$num_end,$num_reverse,$typeNum) {
        $str = strlen($num_start);
        $total_number = 0;
        if($str == 2){ // if number 2 digit
            if($num_reverse == 1){
                $start = str_split($num_start);
                $end = str_split($num_end);

                if($start[0]==$end[0] && $start[1]!=$end[1] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i++){
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal + 2;
                        }
                    }
                    $total_number = $total_number + $subTotal;
                // reverse number 10 -> 90 *
                }elseif($start[0]!=$end[0] && $start[1]==$end[1] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+10){
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal + 2;
                        }
                    }
                    $total_number = $total_number + $subTotal;

                }elseif($start[0]==$start[1] && $end[0]==$end[1] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i = $i + 11){
                        
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal + 2;
                        }
                    }
                    $total_number = $total_number + $subTotal;

                }else{
                    $subTotal = 0;
                     for($i=$num_start; $i<=$num_end; $i++){
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal + 2;
                        }
                    }
                    $total_number = $total_number + $subTotal;
                }
            }else{

                $start = str_split($num_start);
                $end = str_split($num_end);

                if($start[0]==$end[0] && $start[1]!=$end[1] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i++){
                            $subTotal = $subTotal + 1;
                    }
                    $total_number = $total_number + $subTotal;
                // reverse number 10 -> 90 *
                }elseif($start[0]!=$end[0] && $start[1]==$end[1] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i = $i + 10){
                        
                            $subTotal = $subTotal + 1;
                    }
                    $total_number = $total_number + $subTotal;
                }elseif($start[0]==$start[1] && $end[0]==$end[1] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i = $i + 11){
                        
                            $subTotal = $subTotal + 1;
                    }
                    $total_number = $total_number + $subTotal;
                }else{
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i++){
                            $subTotal = $subTotal + 1;
                    }
                    $total_number = $total_number + $subTotal;
                }
            }
        }else{ // if number 3 digit
            if($num_reverse== 1){
                $start = str_split($num_start);
                $end = str_split($num_end);
                // var_dump($start);
                // var_dump($end);

                if($start[0]==$end[0] && $start[1]==$end[1] && $start[2]!=$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i++){
                        $i = intval($i);
                        if($i <= 9 && $i >=0){
                            $i = '00'.$i;
                        }else if($i <= 99){
                            $i = '0'.$i;
                        }
                        
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal +  3;
                        }elseif(count($result)=='3'){
                            $subTotal = $subTotal +  6;
                        }
                    }
                    $total_number = $total_number + $subTotal;
                // reverse number 10 -> 90 *
                }elseif($start[0]==$end[0] && $start[1]!=$end[1] && $start[2]==$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+10){
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal +  3;
                        }elseif(count($result)=='3'){
                            $subTotal = $subTotal +  6;
                        }
                    }
                    $total_number = $total_number + $subTotal;
                }elseif($start[0]!=$end[0] && $start[1]==$end[1] && $start[2]==$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+100){
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal +  3;
                        }elseif(count($result)=='3'){
                            $subTotal = $subTotal +  6;
                        }
                    }
                    $total_number = $total_number + $subTotal;
                }elseif($start[0]==$start[1] && $start[1]==$start[2] && $end[0]==$end[1] && $end[1]==$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i = $i + 111){
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal +  3;
                        }elseif(count($result)=='3'){
                            $subTotal = $subTotal +  6;
                        }
                    }
                    $total_number = $total_number + $subTotal;

                }else{
                    $subTotal = 0;
                     for($i=$num_start; $i<=$num_end; $i++){
                        $result = array_unique( str_split( $i ) );
                        if(count($result)=='1'){
                            $subTotal = $subTotal +  1;
                        }elseif(count($result)=='2'){
                            $subTotal = $subTotal + 3;
                        }elseif(count($result)=='3'){
                            $subTotal = $subTotal + 6;
                        }
                    }
                    $total_number = $total_number + $subTotal;
                }
            }else{
                $start = str_split($num_start);
                $end = str_split($num_end);

                if($start[0]==$end[0] && $start[1]==$end[1] && $start[2]!=$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i++){
                        $subTotal = $subTotal +  1;
                    }
                    $total_number = $total_number + $subTotal;
                // reverse number 10 -> 90 *
                }elseif($start[0]==$end[0] && $start[1]!=$end[1] && $start[2]==$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+10){
                        $subTotal = $subTotal +  1;
                    }
                    $total_number = $total_number + $subTotal;

                }elseif($start[0]==$end[0] && $start[1]==$start[2] && $end[1]==$end[2] && $typeNum == 0){ // check 100>999

                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+11){
                        $subTotal = $subTotal +  1;
                    }
                    $total_number = $total_number + $subTotal;

                }elseif($start[0]==$start[1] && $end[0]==$end[1] && $start[2]==$end[2] ){ // check 001>991
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+110){
                        $subTotal = $subTotal +  1;
                    }
                    $total_number = $total_number + $subTotal;

                }elseif($start[0]!=$end[0] && $start[1]==$end[1] && $start[2]==$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+100){
                        $subTotal = $subTotal +  1;
                    }
                    $total_number = $total_number + $subTotal;
                }elseif($start[0]==$start[1] && $start[1]==$start[2] && $end[0]==$end[1] && $end[1]==$end[2] && $typeNum == 0){
                    $subTotal = 0;
                    for($i=$num_start; $i<=$num_end; $i=$i+111){
                        $subTotal = $subTotal +  1;
                    }
                    $total_number = $total_number + $subTotal;

                }else{
                    $subTotal = 0;
                     for($i=$num_start; $i<=$num_end; $i++){
                        $subTotal = $subTotal +  1;
                    }
                    $total_number = $total_number + $subTotal;
                } 
            }
        }
        return $total_number;
    }

    private function get_sum_price_number($num_number,$post_id,$time,$datecheck) {

        // only real number
        $numbers = DB::table('tbl_number_mobile')
                            ->join('tbl_row_mobile','tbl_number_mobile.r_id','tbl_row_mobile.r_id')
                            ->join('tbl_paper_mobile','tbl_row_mobile.p_id','tbl_paper_mobile.p_id')
                            ->join('tbl_pos_group','tbl_number_mobile.g_id','tbl_pos_group.g_id')
                            ->where('tbl_paper_mobile.p_time', $time)
                            ->where('tbl_paper_mobile.p_date', $datecheck)
                            ->where('tbl_paper_mobile.stc_type', 1)
                            ->where('tbl_pos_group.pos_id', $post_id)
                            ->where('tbl_number_mobile.num_sym', 7)
                            ->where('tbl_number_mobile.num_reverse', 0)
                            ->where('tbl_number_mobile.num_number', $num_number)
                            ->get();
        
        // dd($time.'/'.$datecheck.'/'.$g_id.'/'.$num_number);
        // dd($numbers);
        $sumR = 0;
        $sumD = 0;
        foreach ($numbers as $key => $value) {

            if($value->num_number == strval($num_number)){
                if($value->num_currency == 1){
                    $sumR = $sumR + floatval($value->num_price);
                }else{
                    $sumD = $sumD + floatval($value->num_price);
                }
            }
        }

        //  number x
        $numbers = DB::table('tbl_number_mobile')
                            ->join('tbl_row_mobile','tbl_number_mobile.r_id','tbl_row_mobile.r_id')
                            ->join('tbl_paper_mobile','tbl_row_mobile.p_id','tbl_paper_mobile.p_id')
                            ->join('tbl_pos_group','tbl_number_mobile.g_id','tbl_pos_group.g_id')
                            ->where('tbl_paper_mobile.p_time', $time)
                            ->where('tbl_paper_mobile.p_date', $datecheck)
                            ->where('tbl_paper_mobile.stc_type', 1)
                            ->where('tbl_pos_group.pos_id', $post_id)
                            ->where('tbl_number_mobile.num_sym', 7)
                            ->where('tbl_number_mobile.num_reverse', 1)
                            ->get();

        foreach ($numbers as $key => $mainNum) {
            $loopNum = $this->get_number_list($mainNum->num_number,$mainNum->num_end_number,$mainNum->num_sym,$mainNum->num_reverse);
            foreach ($loopNum as $keySub => $numReal) {

                if($num_number == strval($numReal)){
                    if($mainNum->num_currency == 1){
                        $sumR = $sumR + floatval($mainNum->num_price);
                    }else{
                        $sumD = $sumD + floatval($mainNum->num_price);
                    }
                }
            }
        }

        //  number > with x
        $numbers = DB::table('tbl_number_mobile')
                            ->join('tbl_row_mobile','tbl_number_mobile.r_id','tbl_row_mobile.r_id')
                            ->join('tbl_paper_mobile','tbl_row_mobile.p_id','tbl_paper_mobile.p_id')
                            ->join('tbl_pos_group','tbl_number_mobile.g_id','tbl_pos_group.g_id')
                            ->where('tbl_paper_mobile.p_time', $time)
                            ->where('tbl_paper_mobile.p_date', $datecheck)
                            ->where('tbl_paper_mobile.stc_type', 1)
                            ->where('tbl_pos_group.pos_id', $post_id)
                            ->where('tbl_number_mobile.num_sym', 8)
                            ->get();

        foreach ($numbers as $key => $mainNum) {
            $loopNum = $this->get_number_list($mainNum->num_number,$mainNum->num_end_number,$mainNum->num_sym,$mainNum->num_reverse);
            foreach ($loopNum as $keySub => $numReal) {

                if($num_number == strval($numReal)){
                    if($mainNum->num_currency == 1){
                        $sumR = $sumR + floatval($mainNum->num_price);
                    }else{
                        $sumD = $sumD + floatval($mainNum->num_price);
                    }
                }
            }
        }


        $price = [floatval($sumR),floatval($sumD)];

        return $price;

    }

    private function get_number_list($num_number,$num_end_number,$num_sym,$num_reverse) {
        $numberArray = array();
        $str = strlen($num_number);
        // $num_reverse = 1;
        // condition 2 or 3 digit
        if($str=='2'){ // condition 2 digit
            if($num_sym=='7'){

                if($num_reverse=='1'){

                    $array = str_split($num_number);
                    $number_re = $this->sampling($array,2);
                    foreach ($number_re as $number){
                        
                        array_push($numberArray,$number);

                    }

                }else {
                    array_push($numberArray,$num_number);
                }
            }elseif($num_sym=='8'){

                if($num_reverse=='1'){

                    if($num_end_number){

                        $start = str_split($num_number);
                        $end = str_split($num_end_number);
                        // reverse number 20 -> 29 *
                        if($start[0]==$end[0] && $start[1]!=$end[1]){

                            for($i=$num_number; $i<=$num_end_number; $i++){
                                if($i <= 9){
                                    $i = '0'.$i;
                                }

                                $array = str_split($i);
                                $number_re = $this->sampling($array,2);
                                foreach ($number_re as $number){
                                    $number = (integer)$number;
                                    if($number == 0) {

                                    }else if($number <= 9){
                                        $number = '0'.$number;
                                    }
                                    array_push($numberArray,$number);
                                }
                            }
                            // reverse number 10 -> 90 *
                        }elseif($start[0]!=$end[0] && $start[1]==$end[1]){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+10){
                                if($i <= 9){
                                    $i = '0'.$i;
                                }

                                $array = str_split($i);
                                $number_re = $this->sampling($array,2);
                                foreach ($number_re as $number){
                                    $number = (integer)$number;
                                    if($number == 0) {

                                    }else if($number <= 9){
                                        $number = '0'.$number;
                                    }
                                    array_push($numberArray,$number);

                                }
                            }

                        }elseif( $start[0]==$start[1] && $end[0]==$end[1] ){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+11){
                                if($i == 0) {

                                }else if($i <= 9){
                                    $i = '0'.$i;
                                }
                                array_push($numberArray,$i);
                            }

                        }
                    }else{
                        //  reverse number 23 -> *
                        // NO number have number end
                        $start = str_split($num_number);

                        for($i=$start[1]; $i<=9; $i++){

                            $array = str_split($num_number);
                            $number_re = $this->sampling($array,2);
                            foreach ($number_re as $number){
                                $number = (integer)$number;
                                if($number == 0) {

                                }else if($number <= 9){
                                    $number = '0'.$number;
                                }
                                array_push($numberArray,$number);

                            }

                            $num_number = $num_number + 1;
                        }
                    }

                }else{

                    if($num_end_number){
                        $start = str_split($num_number);
                        $end = str_split($num_end_number);
                        if($start[0]==$end[0] && $start[1]!=$end[1]){
                            $num_number = (integer)$num_number;
                            for($i=$num_number; $i<=$num_end_number; $i++){
                                // var_dump($i);
                                if($i <= 9){
                                    $i = '0'.$i;
                                }
                                // var_dump($i);
                                // var_dump("ok1");
                                array_push($numberArray,$i);

                            }

                        }elseif($start[0]!=$end[0] && $start[1]==$end[1]){

                            for($i=$num_number; $i<=$num_end_number; $i=$i+10){
                                array_push($numberArray,$i);

                            }
                        }elseif( $start[0]==$start[1] && $end[0]==$end[1] ){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+11){
                                if($i == 0) {

                                }else if($i <= 9){
                                    $i = '0'.$i;
                                }
                                array_push($numberArray,$i);
                            }

                        }else{

                            for($i=$num_number; $i<=$num_end_number; $i++){
                                if($i == 0) {

                                }else if($i <= 9){
                                    $i = '0'.$i;
                                }
                                // var_dump($i);
                                // var_dump("ok4");
                                array_push($numberArray,$i);
                            }

                        }
                    }else{
                        $last_num = substr($num_number, -1);
                        for($i=$last_num; $i<=9; $i++){
                            $num_number = (integer)$num_number;
                            if($num_number <= 9){
                                $num_number = '0'.$num_number;
                            }
                            // var_dump($num_number);
                            // var_dump("ok5");
                            array_push($numberArray,$num_number);
                            $num_number = $num_number + 1;

                        }

                    }
                }

            }elseif($num_sym=='9'){

                if($num_reverse=='1'){

                    if($num_end_number){

                        for($i=(integer)$num_number; $i<=(integer)$num_end_number; $i++){
                            if($i <= 9){
                                $i = '0'.$i;
                            }
                            $array = str_split($i);
                            $number_re = $this->sampling($array,2);
                            foreach ($number_re as $number){
                                $number = (integer)$number;
                                if($number <= 9){
                                    $number = '0'.$number;
                                }
                                array_push($numberArray,$number);

                            }

                        }
                    }

                }else{

                    if($num_end_number){
                        for($i=(integer)$num_number; $i<=(integer)$num_end_number; $i++){
                            if($i <= 9){
                                $i = '0'.$i;
                            }
                            array_push($numberArray,$i);
                        }
                    }

                }

            }
        }elseif($str=='3'){ // condition 3 digit
            if($num_sym=='7'){
                if($num_reverse=='1'){

                    $array = str_split($num_number);
                    $number_re = $this->sampling($array,3);
                   // var_dump($number_re);

                    foreach ($number_re as $number){
                        array_push($numberArray,$number);
                    }

                }else{
                    
                    array_push($numberArray,$num_number);
                }
//                dd($count_win);
            }elseif($num_sym=='8'){

                if($num_reverse=='1'){
//                  number have number end
                    if($num_end_number){
                        $start = str_split($num_number);
                        $end = str_split($num_end_number);

                        // reverse number 220 -> 229 *
                        if($start[0]==$end[0] && $start[1]==$end[1] && $start[2]!=$end[2]){
                            for($i=$num_number; $i<=$num_end_number; $i++){
                                $i = intval($i);
                                // var_dump($i);
                                if($i <= 9 && $i >=0){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = '0'.$i;
                                }

                                $array = str_split($i);
                                
                                $number_re = $this->sampling($array,3);
                                foreach ($number_re as $number){
                                    $number = (integer)$number;
                                    if($number == 0) {

                                    }else if($number <= 9){
                                        $number = '00'.$number;
                                    }else if($number <= 99){
                                        $number = '0'.$number;
                                    }
                                    // var_dump($number);
                                    array_push($numberArray,$number);
                                }

                            }
                            // reverse number 230 -> 290 *
                        }elseif($start[0]==$end[0] && $start[1]!=$end[1] && $start[2]==$end[2]){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+10){

                                if($i == 0) {

                                }else if($i <= 9){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = '0'.$i;
                                }

                                $array = str_split($i);
                                

                                $number_re = $this->sampling($array,3);

                                foreach ($number_re as $number){
                                    $number = (integer)$number;
                                    if($number == 0) {

                                    }else if($number <= 9){
                                        $number = '00'.$number;
                                    }else if($number <= 99){
                                        $number = '0'.$number;
                                    }
                                    array_push($numberArray,$number);
                                }

                            }
                            // reverse number 330 -> 390 *
                        }elseif($start[0]!=$end[0] && $start[1]==$end[1] && $start[2]==$end[2]){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+100){

                                $array = str_split($i);
                                $number_re = $this->sampling($array,3);
                                foreach ($number_re as $number){
                                    $number = (integer)$number;
                                    if($number == 0) {

                                    }else if($number <= 9){
                                        $number = '00'.$number;
                                    }else if($number <= 99){
                                        $number = '0'.$number;
                                    }
                                    array_push($numberArray,$number);
                                }

                            }
                        }elseif( ($start[0]==$start[1] && $start[1]==$start[2]) && ($end[0]==$end[1] && $end[1]==$end[2]) ){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+111){
                                // $num_number = (integer)$num_number;
                                if($i == 0) {

                                }else if($i <= 9){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = '0'.$i;
                                }
                                array_push($numberArray,$i);
                            }
                        }

                    }else{
                        //  NO reverse number 222 -> *
                        //  NO number have number end
                        $start = str_split($num_number);

                        for($i=$start[2]; $i<=9; $i++){

                            $array = str_split($num_number);
                            $number_re = $this->sampling($array,3);
                            foreach ($number_re as $number){
                                $number = (integer)$number;
                                if($number == 0) {

                                }else if($number <= 9){
                                    $number = '00'.$number;
                                }else if($number <= 99){
                                    $number = '0'.$number;
                                }
                                array_push($numberArray,$number);
                            }

                            $num_number = $num_number + 1;
                        }

                    }

                }else{
                    //  reverse number 222 ->
                    //  number have number end
                    // var_dump($num_end_number);
                    if($num_end_number){
                        $start = str_split($num_number);
                        $end = str_split($num_end_number);
                        if($start[0]==$end[0] && $start[1]==$end[1] && $start[2]!=$end[2]){

                            for($i=$num_number; $i<=$num_end_number; $i++){
                                $i = (integer)$i;
                                if($i == 0) {
                                    $num_number = '000';
                                }else if($i <= 9){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = '0'.$i;
                                }
                                // var_dump($i);
                                array_push($numberArray,$i);
                            }

                        }elseif($start[0]==$end[0] && $start[1]!=$end[1] && $start[2]==$end[2]){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+10){
                                // $num_number = (integer)$num_number;
                                if($i == 0) {
                                    $num_number = '000';
                                }else if($i <= 9){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = '0'.$i;
                                }
                                array_push($numberArray,$i);
                            }
                        }elseif($start[0]!=$end[0] && $start[1]==$end[1] && $start[2]==$end[2]){
                            for($i=$num_number; $i<=$num_end_number; $i=$i+100){
                                // $num_number = (integer)$num_number;
                                if($i == 0) {
                                    $num_number = '000';
                                }else if($i <= 9){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = $i;
                                }
                                // var_dump($i);
                                array_push($numberArray,$i);
                            }
                        }elseif( ($start[0]==$start[1] && $start[1]==$start[2]) && ($end[0]==$end[1] && $end[1]==$end[2]) ){
//                            var_dump($check_pos);
                            for($i=$num_number; $i<=$num_end_number; $i=$i+111) {
                                // $num_number = (integer)$num_number;
                                if($i == 0){
                                    $num_number = '000';
                                }else if($i <= 9){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = '0'.$i;
                                }

                                array_push($numberArray,$i);
                            }
                        }else{
                            for($i=$num_number; $i<=$num_end_number; $i++){
                                // $num_number = (integer)$num_number;
                                if($i == 0) {
                                    $num_number = '000';
                                }else if($i <= 9){
                                    $i = '00'.$i;
                                }else if($i <= 99){
                                    $i = '0'.$i;
                                }
                                array_push($numberArray,$i);
                            }
                        }

                    }else{

                        $last_num = substr($num_number, -1);
                        for($i=$last_num; $i<=9; $i++){
                            
                            $num_number = (integer)$num_number;
                            // var_dump($num_number);
                            if($num_number == 0) {
                                $num_number = '000';
                            }else if($num_number <= 9){

                                $num_number = '00'.$num_number;
                            }else if($num_number <= 99){
                                
                                $num_number = '0'.$num_number;
                            }
                            // var_dump($num_number);

                            array_push($numberArray,$num_number);
                            $num_number = $num_number + 1;

                        }

                    }
                }

            }elseif($num_sym=='9'){

                if($num_reverse=='1'){

                    if($num_end_number){

                        for($i=$num_number; $i<=$num_end_number; $i++){

                            $array = str_split($i);
                            $number_re = $this->sampling($array,3);
                            foreach ($number_re as $number){
                                // if($number == 0) {

                                // }else if($number <= 9){
                                //     $number = '00'.$number;
                                // }else if($number <= 99){
                                //     $number = '0'.$number;
                                // }
                                array_push($numberArray,$number);
                            }

                        }

                    }

                }else{

                    if($num_end_number){

                        for($i=$num_number; $i<=$num_end_number; $i++){

                            if($i == 0) {

                            }else if($i <= 9){
                                $i = '00'.$i;
                            }else if($i <= 99){
                                $i = '0'.$i;
                            }
                            // var_dump($i);
                            array_push($numberArray,$i);

                        }

                    }

                }

            }
        }elseif($num_sym=='10'){ // condition alot of digit

            $data_number = array();
            $data = Report::reverse_number($num_number,$data_number);

            foreach ($data as $value) {
                $number_re = $this->sampling($value,3);
                foreach ($number_re as $number){
                    // if($number == 0) {

                    // }else if($number <= 9){
                    //     $number = '00'.$number;
                    // }else if($number <= 99){
                    //     $number = '0'.$number;
                    // }
                    array_push($numberArray,$number);
                }
            }
            
            // dd($data);
        }elseif($num_sym=='11'){ // condition alot of digit

            $data = $this->count_number_new_old_style($num_number);
            
            if($data[1] == '3'){
                foreach ($data[0] as $number){
                    // if($number == 0) {

                    // }else if($number <= 9){
                    //     $number = '00'.$number;
                    // }else if($number <= 99){
                    //     $number = '0'.$number;
                    // }
                    array_push($numberArray,$number);
                }
            }else{
                foreach ($data[0] as $number){
                    // if($number <= 9){
                    //     $number = '0'.$number;
                    // }
                    array_push($numberArray,$number);

                }
            }

        }

        return $numberArray;
    }

    private function sampling($chars, $size, $combinations = array()) {
        $resultDigit = array();
        if($size == 2){
            $resultDigit[] = $chars[0].$chars[1];
            $resultDigit[] = $chars[1].$chars[0];
        }else{
            $resultDigit[] = $chars[0].$chars[1].$chars[2];
            $resultDigit[] = $chars[0].$chars[2].$chars[1];
            $resultDigit[] = $chars[1].$chars[2].$chars[0];
            $resultDigit[] = $chars[1].$chars[0].$chars[2];
            $resultDigit[] = $chars[2].$chars[0].$chars[1];
            $resultDigit[] = $chars[2].$chars[1].$chars[0];
        }
        return  array_unique($resultDigit);
    }

    private function count_number_new_old_style($num_start) {
        $data_number_list = array();
        // $list3Digit = '';
        $number = explode("/", $num_start);
        // var_dump($number);
        $checkDgit = '';
        if($number[0] != '' && $number[1] != '' && $number[2] != ''){
            $left = explode(",", $number[0]);
            $center = explode(",", $number[1]);
            $right = explode(",", $number[2]);
            foreach ($left as $key1 => $value1) {
                if($value1 != ''){
                    foreach ($center as $key2 => $value2) {
                        if($value2 != ''){
                            foreach ($right as $key3 => $value3) {
                                if($value3 != ''){
                                    $num_need = $value1.$value2.$value3;
                                    $data_number_list[$num_need]=$num_need;
                                }
                            }
                        }
                        
                    }
                }
                
            }
            $checkDgit = 3;
        }else if($number[0] != '' && $number[1] != '' && $number[2] == ''){
            $left = explode(",", $number[0]);
            $center = explode(",", $number[1]);
            foreach ($left as $key1 => $value1) {
                if($value1 != ''){
                    foreach ($center as $key2 => $value2) {
                        if($value2 != ''){
                            $num_need= $value1.$value2;
                            $data_number_list[$num_need]=$num_need;
                        }
                        
                    }
                }
                
            }
            $checkDgit = 2;
        }else if($number[0] != '' && $number[1] == '' && $number[2] != ''){
            $left = explode(",", $number[0]);
            $right = explode(",", $number[2]);
            foreach ($left as $key => $value1) {
                if($value1 != ''){
                    foreach ($right as $key => $value2) {
                        if($value2 != ''){
                            $num_need= $value1.$value2;
                            $data_number_list[$num_need]=$num_need;
                        }
                        
                    }
                }
                
            }
            $checkDgit = 2;
        }else if($number[0] == '' && $number[1] != '' && $number[2] != ''){
            $center = explode(",", $number[1]);
            $right = explode(",", $number[2]);
            foreach ($center as $key => $value1) {
                if($value1 != ''){
                    foreach ($right as $key => $value2) {
                        if($value2 != ''){
                            $num_need= $value1.$value2;
                            $data_number_list[$num_need]=$num_need;
                        }
                        
                    }
                }
                
            }
            $checkDgit = 2;
        }
        // var_dump($totalNumber);

        $totalValue = array($data_number_list,$checkDgit);
        return $totalValue;
    }
}
