<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

use Illuminate\Http\Request;

class resultReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }elseif (Session::get('roleLot') != 1) {
                    Redirect::to('/sale')->send();
               }
            return $next($request);
        });
        // dd("ok1");

    }
    public function index(Request $request)
    {
    	$page = 'resultreport';
        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );

       $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();
        

        return view('resultreport.index', compact('page','sheets','type_lottery','var_type_lottery'));
    }


    public function filter(Request $request)
    {
        $page = 'reportResultFillter';
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_type_lottery = $request->type_lottery;
        $stc_type = $request->type_lottery;

        $var_sheet = $request->sheet;

        // dd( $var_type_lottery);

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );


        if($stc_type=='1'){
            $pat_key = "sheet";
        }elseif($stc_type=='2'){
            $pat_key = "sheet_khmer";
        }elseif($stc_type=='3'){
            $pat_key = "sheet_th";
        }elseif($stc_type=='4'){
            $pat_key = "sheet_lo";
        }else{
            $pat_key = "sheet";
        }
        

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();
      
        

        // dd($mul_staff);

        $conditions = '';

        

        if($request->dateStart!="" && $request->dateEnd!=""){
            $conditions .= " tbl_result.re_date >= '$var_dateStart' AND tbl_result.re_date <= '$var_dateEnd' AND ";
        }elseif($request->dateStart!="" && $request->dateEnd==""){
            $conditions .= " tbl_result.re_date = '$var_dateStart' AND ";
        }

        if($var_sheet){
            $conditions .= " tbl_pos.pos_time = '$var_sheet' AND ";
        }

        $conditions = substr($conditions, 0, -4);
        // var_dump(Session::get('roleLot'));
        // dd($conditions);


                $report = DB::table('tbl_result')
			            ->distinct()
			            ->select('tbl_parameter_value.pav_value','tbl_result.re_date','tbl_result.re_id','tbl_parameter_value.pav_id','tbl_result.re_num_result','tbl_pos.pos_name','tbl_pos.pos_time')
			            ->leftjoin('tbl_pos', 'tbl_result.pos_id', '=', 'tbl_pos.pos_id')
			            ->leftjoin('tbl_parameter_value', 'tbl_pos.pos_time', '=', 'tbl_parameter_value.pav_id')
			            ->leftjoin('tbl_parameter_type','tbl_parameter_value.pat_id','=','tbl_parameter_type.pat_id')
			            ->where('tbl_parameter_type.pat_key',$pat_key)
			            ->whereRaw($conditions)
			            ->whereRaw(" tbl_pos.pos_name in ('A','B','C','D','F','I','N')")
			            ->orderBy('tbl_result.re_date','DESC')
			            ->orderBy('tbl_pos.pos_id','ASC')

			            ->get();


		// dd($report);

        if(count($report)!=0){
                // dd($boss_data);

        		$resulttwo = array();

            	$mainDate = array();

            	$checkDate = '';

        		foreach($report as $key => $val){



        			if(strlen($val->re_num_result)==2){
	                    $resulttwo[$val->re_date][$val->pos_name][2][] = $val->re_num_result;

	                }else{
	                    $resulttwo[$val->re_date][$val->pos_name][3][] = $val->re_num_result;
	                }

	                if($checkDate != $val->re_date){
        				$checkDate = $val->re_date;
        				$mainDate[] = $val->re_date;
        			}
        		}

        		// dd($resulttwo);
                return view('resultreport.index', compact('page','sheets','var_sheet','var_dateStart','var_dateEnd','report','type_lottery','var_type_lottery','resulttwo','mainDate'));
        
            
        }else{
            // return view('report.index', compact('page','sheets','staff','pages','type_lottery','var_type_lottery'));
            return view('resultreport.index', compact('page','sheets','var_sheet','var_dateStart','var_dateEnd','type_lottery','var_type_lottery'));
            // return redirect('report/')->withInput()->with('staff', $staff);
        }
    }



}
