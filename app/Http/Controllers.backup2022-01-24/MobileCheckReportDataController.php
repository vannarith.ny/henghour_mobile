<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileCheckReportDataController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

              if(!Session::has('iduserlotMobileSec'))
               {
                    Redirect::to('/')->send();
               }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function getReportData(Request $request)
    {
       $startDate = $request->fromDate;
       $endDate = $request->toDate;
       $userID = $request->userId;

       $report = DB::table('tbl_total_everyday')
                    ->where('s_id',$userID)
                    ->where('date','>=',$startDate)
                    ->where('date','<=',$endDate)
                    ->orderBy('date', 'ASC')
                    ->get();


        if($report){
            $html = '';
            foreach ($report as $key => $value) {
                $html .= '<tr align="center" class="tr-row no-transac">';
                    $html .= '<td class="classtd">'.date("m/d/Y", strtotime($value->date)).'</td>';
                    $html .= '<td class="classtd">'.$value->price_r.'</td>'; 
                    $html .= '<td class="classtd">'.$value->price_d.'$</td>';

                    // get payment from staff
                    $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                    $pay = DB::table("tbl_staff_transction")
                            ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                            ->where('tbl_staff_transction.st_date_search',$yesturday)
                            ->where('tbl_staff_transction.s_id',$value->s_id)
                            ->where('tbl_staff_transction.st_type',3)
                            ->first();
                    if($pay){
                        $html .= '<td class="classtd">'.$pay->st_price_r.'</td>';
                        $html .= '<td class="classtd">'.$pay->st_price_d.'$</td>';
                    }else{
                        $html .= '<td class="classtd"></td>';
                        $html .= '<td class="classtd"></td>';
                    }

                    // get payment from boos
                    $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                    $transfar = DB::table("tbl_staff_transction")
                             ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                             ->where('tbl_staff_transction.st_date_search',$yesturday)
                             ->where('tbl_staff_transction.s_id',$value->s_id)
                             ->where('tbl_staff_transction.st_type',4)
                             ->first();
                    if($transfar){
                        $html .= '<td class="classtd">'.$transfar->st_price_r.'</td>';
                        $html .= '<td class="classtd">'.$transfar->st_price_d.'$</td>';
                    }else{
                        $html .= '<td class="classtd"></td>';
                        $html .= '<td class="classtd"></td>';
                    }

                    // get payment from boos
                    $yesturday = date('Y-m-d',strtotime($value->date . "-1 days"));
                    $return = DB::table("tbl_staff_transction")
                             ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                             ->where('tbl_staff_transction.st_date_search',$yesturday)
                             ->where('tbl_staff_transction.s_id',$value->s_id)
                             ->where('tbl_staff_transction.st_type',20)
                             ->first();
                    if($return){
                        $html .= '<td class="classtd">'.$return->st_price_r.'</td>';
                        $html .= '<td class="classtd">'.$return->st_price_d.'$</td>';
                    }else{
                        $html .= '<td class="classtd"></td>';
                        $html .= '<td class="classtd"></td>';
                    }

                    // get old payment
                    $oldPayment = DB::table("tbl_total_everyday")
                             ->where('date','<',$value->date)
                             ->where('s_id',$value->s_id)
                             ->orderBy('date', 'desc')
                             ->first();
                    if($oldPayment){
                        $html .= '<td class="classtd">'.$oldPayment->c_price_r.'</td>';
                        $html .= '<td class="classtd">'.$oldPayment->c_price_d.'$</td>';
                    }else{
                        $html .= '<td class="classtd"></td>';
                        $html .= '<td class="classtd"></td>';
                    }

                    $html .= '<td class="classtd">'.$value->c_price_r.'</td>';
                    $html .= '<td class="classtd">'.$value->c_price_d.'$</td>';
                $html .= '</tr>';
						
            }
            return response(['msg' => 'ok', 'html' => $html, 'status' => 'success']); 
        }else{
            $msg = '<tr align="center" class="tr-row no-transac"><td class="classtd" colspan="13">មិនមានទិន្នន័យ</td></tr>';
            return response(['msg' => $msg, 'status' => 'error']); 
        }



    }

    public function index(Request $request){
    	
    	$page = 'checkreportdata';
    	$stc_type = 1;

        
        $userList = DB::table("tbl_staff")
            ->where('tbl_staff.parent_id',Session::get('iduserlotMobileSec'))
            ->orderBy('tbl_staff.s_id','asc')
            ->get()->toArray();

    	// $ThatTime ="16:40:00";
		// if (time() >= strtotime($ThatTime)) {
		//   $sheet_id = 6;
		// }
        $sheets = DB::table('tbl_parameter_value')->where('pat_id',3)->get();
        $sheet_id = 23;

        if (time() <= strtotime('10:40:00')) {
            $sheet_id = 23;
        }elseif (time() <= strtotime('13:40:00')) {
            $sheet_id = 24;
        }elseif (time() <= strtotime('16:40:00')) {
            $sheet_id = 5;
        }else{
            $sheet_id = 6;
        }

		// $sheetName =  DB::table('tbl_parameter_value')->where('pav_id',$sheet_id)->first();

		// $clientId = Session::get('iduserlotMobileSec');

		
        // dd($PostVote);
    	return view("mobile.checkreportdata.index", compact('page','sheet_id','sheets', 'userList'));
    }
}
