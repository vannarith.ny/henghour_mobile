<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class ReportMainStaffController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }

    public function getstaffchild(Request $request)
    {
        $s_id = $request->s_id;

        if($s_id != ''){
            $staff = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('tbl_staff.parent_id',$s_id)
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->get();

            $option = '<option value="" selected="selected">កូនក្រុម</option>';
            foreach ($staff as $value) {
                $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
            }
        }else{


            $staff = DB::table('tbl_staff')
                    ->select('tbl_staff.s_id','tbl_staff.s_name')
                    ->where('s_role',1)
                    ->where('parent_id',"<>", 0)
                    ->orderBy('tbl_staff.s_name', 'ASC')
                    ->get();
            

            // dd($staff);
            $option = '';
            // $option = '<option value="" selected="selected">កូនក្រុម</option>';
            foreach ($staff as $value) {
                $option .= '<option value="'.$value->s_id.'">'.$value->s_name.'</option>';
            }
        }

        


        return response(['msg' => $option, 'status' => 'success']);  
    }

    public function index(Request $request)
    {
        $page = 'reportMain';
        
        $block = DB::table('tbl_block')
            ->orderBy('name', 'ASC')
            ->pluck('name','id')
            ->all();
        return view('reportmain.index', compact('page','block'));
    }

    public function filterMain(Request $request)
    {
        $page = 'reportMain';
        $var_dateStart = $request->dateStart;
        $var_staff = $request->block_id;

        $block = DB::table('tbl_block')
            ->orderBy('name', 'ASC')
            ->pluck('name','id')
            ->all();

        $staffChild = DB::table('tbl_block_staff')
            ->where('b_id',$var_staff)
            ->get();
        
        $chilID = array();
        if($staffChild){

            foreach ($staffChild as $key => $value) {
                array_push($chilID,$value->s_id);
            }

            $report = DB::table('tbl_total_everyday')
                    ->leftjoin('tbl_staff', 'tbl_total_everyday.s_id','=','tbl_staff.s_id')
                    ->whereIn('tbl_total_everyday.s_id', $chilID)
                    ->where('tbl_total_everyday.date',$var_dateStart)
                    ->orderBy('tbl_total_everyday.s_id', 'ASC')
                    ->get();
            
            $staffMain = DB::table('tbl_block')
                            ->leftjoin('tbl_staff', 'tbl_block.s_id','=','tbl_staff.s_id')
                            ->where('tbl_block.id', $var_staff)
                            ->first();
            return view('reportmain.index', compact('page','block','var_dateStart','var_staff','staffMain','report'));
        }else{
            flash()->error("ឈ្មួញកណ្តាលមិនមានកូន");
            return view('reportmain.index', compact('page','block','var_dateStart','var_staff'));
        }
    }

}
