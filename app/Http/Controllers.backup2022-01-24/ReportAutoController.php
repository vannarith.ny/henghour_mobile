<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

use App\Model\Report;

class ReportAutoController extends Controller
{
    // block update cuongnam result
    public function updatealomorning(Request $request)
    {
        $page = 'report_update';
        $var_dateStart = date("Y-m-d");
        // $var_dateStart = '2021-10-14';
        $var_sheet = 6; // Morning
        $var_type_lottery = 1; // VN lottery
        $pos_id = 10; // group id = A
        // $numFix = 00; // number result 2D
        // $numFix3D = 000; // number result 3D
        $numFix = rand(10, 99); // number result 2D
        $numFix3D = rand(100, 999); // number result 3D
        


        // block check pos A
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }

        $digit = 2; 
        $x = 1; // for add random
        $numberResult = $this->checknumberavailable($numFix,$pos_id,$var_dateStart,$digit);
        var_dump($numberResult,'2D'); 
        if($numberResult){
            $table_type = 4;
            $category_id = 1;
            $name = 'first_price';
            $set_value = $numberResult;
            $finish = 0;

            DB::connection('mysql2')->table('lotteries')
                    ->where('table_type',$table_type)
                    ->where('category_id',$category_id)
                    ->where('name',$name)
                    ->where('finish',0)
                    ->update([
                        'set_value' => $set_value,
                    ]);

        }

        // block 3D A
        $digit = 3;
        $x = 2;
        $numberResult = $this->checknumberavailable($numFix3D,$pos_id,$var_dateStart,$digit);
        var_dump($numberResult,'3D'); 
        if($numberResult){
            $table_type = 4;
            $category_id = 1;
            $name = 'second_price';
            $set_value = $numberResult;
            $finish = 0;

            DB::connection('mysql2')->table('lotteries')
                    ->where('table_type',$table_type)
                    ->where('category_id',$category_id)
                    ->where('name',$name)
                    ->where('finish',0)
                    ->update([
                        'set_value' => $set_value,
                    ]);

        }
        // end block A morning





        // block lo 
        $pos_id = 14; // group id = Lo
        $labelLoName = array('third_price_1','third_price_2','third_price_3','fourth_price','fifth_price_1','fifth_price_2','fifth_price_3','fifth_price_4','fifth_price_5','fifth_price_6','fifth_price_7','sixth_price_1','sixth_price_2','seventh_price','eighth_price'); // number result 2D
        // $numFix3D = array('000','000','000','000','000','000','000','000','000','000','000','000','000','000','000'); // number result 3D
        $numFix3D = array(rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999)); // number result 3D
        
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }

        foreach ($numFix3D as $key => $value) {
            $digit = 2;
            $numCheck = substr($value, -2);
            $twoResult = $this->checknumberavailable($numCheck,$pos_id,$var_dateStart,$digit);
            $digit = 3;
            $numCheck3 = substr($value, 0, 1).$twoResult;
            $threeResult = $this->checknumberavailableLo($numCheck3,$pos_id,$var_dateStart,$digit);
            var_dump($threeResult.'=>'.$twoResult.'=>data'.$key); 

            if($threeResult){
                $table_type = 4;
                $category_id = 1;
                $name = $labelLoName[$key];
                $set_value = $threeResult;
                $finish = 0;

                if($name == 'third_price_1' || $name == 'third_price_2' || $name == 'third_price_3'){
                    $set_value = rand(0, 9).$threeResult;
                }else{
                    $rand = rand(0, 99);
                    $mainD = '';
                    if($rand <= 9){
                        $mainD = '0'.$rand;
                    }else{
                        $mainD = $rand;
                    }
                    $set_value = $mainD.$threeResult;
                }

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);
            }
            

        }

        //end block lo 
        
        



        
    }

    public function updatebcdmorning(Request $request)
    {
        $page = 'report_update';
        $var_dateStart = date("Y-m-d");
        // $var_dateStart = '2021-10-14';
        $var_sheet = 6; // Morning
        $var_type_lottery = 1; // VN lottery
        $pos_id = 11; // group id = B
        // $numFix = 00; // number result 2D
        // $numFix3D = 000; // number result 3D
        $numFix = rand(10, 99); // number result 2D
        $numFix3D = rand(100, 999); // number result 3D
        
        


        // block check pos B
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }




        // block check pos C
        // get paper not check
        $pos_id = 12;
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }



        // block check pos D
        // get paper not check
        $pos_id = 13;
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }






        $digit = 2; 
        $fullNum = $numFix.$numFix3D;
        $numberResultBCD = $this->checknumberBCD($fullNum,$pos_id,$var_dateStart,$digit);
        // dd($numberResultBCD);
        var_dump($numberResultBCD,'BCD'); 
        if($numberResultBCD){

            $table_type = 4;
            $category_id = 1;
            $name = 'grand_price';
            $set_value = $numberResultBCD;
            $finish = 0;

            DB::connection('mysql2')->table('lotteries')
                    ->where('table_type',$table_type)
                    ->where('category_id',$category_id)
                    ->where('name',$name)
                    ->where('finish',0)
                    ->update([
                        'set_value' => $set_value,
                    ]);

        }

        
        // end block BCD morning


        
    }



    // block update cuongnam result Afternoon

    // AFI LO
    public function updateafiloafternoon(Request $request)
    {
        $page = 'report_update';
        $var_dateStart = date("Y-m-d");
        // $var_dateStart = '2021-10-14';
        $var_sheet = 18; // Morning
        $var_type_lottery = 1; // VN lottery
        $pos_id = 41; // group id = A
        // $numFixA = 00; // number result 2D
        // $numFixA3D = '000'; // number result 3D
        $numFixA = rand(10, 99); // number result 2D
        $numFixA3D = rand(100, 999); // number result 3D
        
        


        // block check pos A
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }


        // block check pos F
        $pos_id = 51; // group id = F
        // $numFixF = 70; // number result 2D
        // $numFixF3D = 000; // number result 3D
        $numFixF = rand(10, 99); // number result 2D
        $numFixF3D = rand(100, 999); // number result 3D
        
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }


        // block check pos I
        $pos_id = 52; // group id = I
        // $numFixI = 74; // number result 2D
        // $numFixI3D = 836; // number result 3D
        $numFixI = rand(10, 99); // number result 2D
        $numFixI3D = rand(100, 999); // number result 3D
        
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }


        $dateLottery = date("Y-m-d");
        $current_day = date("D", strtotime($dateLottery));

        if($current_day!='Fri'){
            // Random A 2D
            $digit = 2; 
            $x = 1; // for add random
            $numberResult = $this->checknumberavailable($numFixA,41,$var_dateStart,$digit);
            var_dump($numberResult,'2D A'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 1;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random A 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixA3D,41,$var_dateStart,$digit);
            var_dump($numberResult,'3D A'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 1;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }
        }

        if($current_day=='Sat' || $current_day=='Sun' || $current_day=='Mon'){

            // Random FI 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailableFI($numFixF,51,52,$var_dateStart,$digit);
            var_dump($numberResult,'2D FI'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 2;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random FI 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailableFI($numFixF3D,51,52,$var_dateStart,$digit);
            var_dump($numberResult,'3D FI'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 2;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }
        }
        if( $current_day=='Wed' || $current_day=='Thu'){
           

            // Random F 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixF,51,$var_dateStart,$digit);
            var_dump($numberResult,'2D F'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 3;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random F 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixF3D,51,$var_dateStart,$digit);
            var_dump($numberResult,'3D F'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 3;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixI,52,$var_dateStart,$digit);
            var_dump($numberResult,'2D I'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 2;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixI3D,52,$var_dateStart,$digit);
            var_dump($numberResult,'3D I'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 2;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

        }
        if($current_day=='Fri'){
           

            // Random AFI 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailableAFI($numFixA,41,51,52,$var_dateStart,$digit);
            var_dump($numberResult,'2D AFI'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 1;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random AFI 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailableAFI($numFixA3D,41,51,52,$var_dateStart,$digit);
            var_dump($numberResult,'3D AFI'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 1;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }


        }
        if($current_day=='Tue'){
           
            // Random F 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixF,51,$var_dateStart,$digit);
            var_dump($numberResult,'2D F'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 2;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random F 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixF3D,51,$var_dateStart,$digit);
            var_dump($numberResult,'3D F'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 2;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixI,52,$var_dateStart,$digit);
            var_dump($numberResult,'2D I'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 3;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixI3D,52,$var_dateStart,$digit);
            var_dump($numberResult,'3D I'); 
            if($numberResult){
                $table_type = 1;
                $category_id = 3;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }


        }



        // start block lo 
        // block lo  Afternoon
        $pos_id = 45; // group id = Lo
        $labelLoName = array('third_price_1','third_price_2','third_price_3','fourth_price','fifth_price_1','fifth_price_2','fifth_price_3','fifth_price_4','fifth_price_5','fifth_price_6','fifth_price_7','sixth_price_1','sixth_price_2','seventh_price','eighth_price'); // number result 2D
        // $numFix3D = array('562','783','612','043','064','468','512','403','318','402','063','573','317','382','412'); // number result 3D
        $numFix3D = array(rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999)); // number result 3D
        
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }

        foreach ($numFix3D as $key => $value) {
            $digit = 2;
            $numCheck = substr($value, -2);
            $twoResult = $this->checknumberavailable($numCheck,$pos_id,$var_dateStart,$digit);
            $digit = 3;
            $numCheck3 = substr($value, 0, 1).$twoResult;
            $threeResult = $this->checknumberavailableLo($numCheck3,$pos_id,$var_dateStart,$digit);
            var_dump($threeResult.'=>'.$twoResult.'=>data'.$key); 

            if($threeResult){
                $table_type = 1;
                $category_id = 1;
                $name = $labelLoName[$key];
                $set_value = $threeResult;
                $finish = 0;

                if($name == 'third_price_1' || $name == 'third_price_2' || $name == 'third_price_3'){
                    $set_value = rand(0, 9).$threeResult;
                }else{
                    $rand = rand(0, 99);
                    $mainD = '';
                    if($rand <= 9){
                        $mainD = '0'.$rand;
                    }else{
                        $mainD = $rand;
                    }
                    $set_value = $mainD.$threeResult;
                }

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);
            }
            

        }
        //end block lo 
        


        
    }

    //BCD Afternoon
    public function updatebcdafternoon(Request $request)
    {
        $page = 'report_update';
        $var_dateStart = date("Y-m-d");
        // $var_dateStart = '2021-10-14';
        $var_sheet = 18; // Morning
        $var_type_lottery = 1; // VN lottery
        $pos_id = 42; // group id = B
        // $numFix = 00; // number result 2D
        // $numFix3D = 000; // number result 3D
        $numFix = rand(10, 99); // number result 2D
        $numFix3D = rand(100, 999); // number result 3D
        
        


        // block check pos B
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }




        // block check pos C
        // get paper not check
        $pos_id = 43;
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }



        // block check pos D
        // get paper not check
        $pos_id = 44;
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }


        $dateLottery = date("Y-m-d");
        $current_day = date("D", strtotime($dateLottery));

        $digit = 2; 
        $count = 0;
        $fullNum = $numFix.$numFix3D;
        if($current_day=='Fri'){
            $numberResultBCD = $this->checknumberBCDNAfternoon($fullNum,42,43,44,53,$var_dateStart,$digit,$count);

            var_dump($numberResultBCD,'BCDN Afternoon'); 
        }else{
            $numberResultBCD = $this->checknumberBCDAfternoon($fullNum,42,43,44,$var_dateStart,$digit,$count);
        
            var_dump($numberResultBCD,'BCD Afternoon'); 


            // start N Afternoon
            $pos_id = 53; // group id = N
            // $numFixN = 64; // number result 2D
            // $numFixN3D = 374; // number result 3D
            $numFixN = rand(10, 99); // number result 2D
            $numFixN3D = rand(100, 999); // number result 3D
            

            // get paper not check
            $papers = DB::table('tbl_number')
                        ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                        ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                        ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                        ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                        ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                        ->where('p_time',$var_sheet)
                        ->where('tbl_pos_group.pos_id',$pos_id)
                        ->groupBy('tbl_number.num_id')
                        // ->toSql();
                        ->get();

           // dd($papers);
            $result = array();
            foreach ($papers as $key => $paper) {
                // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
                $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
                // $result[] =$data[$paper->pos_id];
            }
            // dd($result);

            $count = 0;

            if(count($result) > 0){

                $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
                foreach ($result as $key => $pos) {

                    foreach ($pos as $keyPos => $arrNum) { //foreach number

                        foreach ($arrNum as $numValue => $num) {
                        
                            $check = DB::table('tbl_smart_auto')
                                    ->where('sm_date',$var_dateStart)
                                    ->where('p_id',$keyPos)
                                    ->where('sm_key',strval($numValue))
                                    ->first();

                            
                            if($check){

                                $newPriceR = $check->sm_value_r;
                                $newPriceD = $check->sm_value_d;
                                if($num[1] == 1){
                                    $newPriceR = $check->sm_value_r + $num[0];
                                }else{
                                    $newPriceD = $check->sm_value_d + $num[0];
                                }

                                $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                        'sm_value_r' => $newPriceR,
                                        'sm_value_d' => $newPriceD,
                                        ]);
                                if($updated){
                                    $count++;
                                } 
                                
                            }else{
                            
                            
                                // dd($num);
                                $newPriceR = 0;
                                $newPriceD = 0;
                                if($num[1] == 1){
                                    $newPriceR = $num[0];
                                }else{
                                    $newPriceD = $num[0];
                                }

                                $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                    [
                                        'digit' => strlen($numValue),
                                        'sm_key' => strval($numValue),
                                        'sm_value_r' => $newPriceR,
                                        'sm_value_d' => $newPriceD,
                                        'p_id' => $keyPos,
                                        'sm_date' => $var_dateStart

                                    ]
                                );
                                if($insertID){
                                    $count++;
                                } 
                            }

                        }


                    } //end foreach num
                }

                
            }else{
                DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            }

            $dateLottery = date("Y-m-d");
            $current_day = date("D", strtotime($dateLottery));

            if($current_day=='Sat' || $current_day=='Sun' || $current_day=='Mon'){

                // Random N 2D & 3D
                $digit = 2; 
                $numberResult2D = $this->checknumberavailable($numFixN,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult2D,'2D N Afternoon'); 
                $digit = 3; 
                $numberResult3D = $this->checknumberavailable($numFixN3D,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult3D,'3D N Afternoon'); 
                if($numberResult3D){
                    $table_type = 1;
                    $category_id = 2;
                    $name = 'grand_price';
                    $set_value = rand(0, 9).$numberResult2D.$numberResult3D;
                    $finish = 0;
                    DB::connection('mysql2')->table('lotteries')
                            ->where('table_type',$table_type)
                            ->where('category_id',$category_id)
                            ->where('name',$name)
                            ->where('finish',0)
                            ->update([
                                'set_value' => $set_value,
                            ]);

                }

            }

            if( $current_day=='Wed' || $current_day=='Thu'){

                // Random N 2D & 3D
                $digit = 2; 
                $numberResult2D = $this->checknumberavailable($numFixN,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult2D,'2D N Afternoon'); 
                $digit = 3; 
                $numberResult3D = $this->checknumberavailable($numFixN3D,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult3D,'3D N Afternoon'); 
                if($numberResult3D){
                    $table_type = 1;
                    $category_id = 3;
                    $name = 'grand_price';
                    $set_value = rand(0, 9).$numberResult2D.$numberResult3D;
                    $finish = 0;
                    DB::connection('mysql2')->table('lotteries')
                            ->where('table_type',$table_type)
                            ->where('category_id',$category_id)
                            ->where('name',$name)
                            ->where('finish',0)
                            ->update([
                                'set_value' => $set_value,
                            ]);

                }

            }

            if($current_day=='Fri'){

            }

            if($current_day=='Tue'){
               
                // Random N 2D & 3D
                $digit = 2; 
                $numberResult2D = $this->checknumberavailable($numFixN,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult2D,'2D N Afternoon'); 
                $digit = 3; 
                $numberResult3D = $this->checknumberavailable($numFixN3D,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult3D,'3D N Afternoon'); 
                if($numberResult3D){
                    $table_type = 1;
                    $category_id = 2;
                    $name = 'grand_price';
                    $set_value = rand(0, 9).$numberResult2D.$numberResult3D;
                    $finish = 0;
                    DB::connection('mysql2')->table('lotteries')
                            ->where('table_type',$table_type)
                            ->where('category_id',$category_id)
                            ->where('name',$name)
                            ->where('finish',0)
                            ->update([
                                'set_value' => $set_value,
                            ]);

                }

            }


            // end N Afternoon



        } //end if no friday

        if($numberResultBCD){

            $table_type = 1;
            $category_id = 1;
            $name = 'grand_price';
            $set_value = $numberResultBCD;
            $finish = 0;

            DB::connection('mysql2')->table('lotteries')
                    ->where('table_type',$table_type)
                    ->where('category_id',$category_id)
                    ->where('name',$name)
                    ->where('finish',0)
                    ->update([
                        'set_value' => $set_value,
                    ]);

        }

        
        // end block BCD afternoon 


        

        
    }
    // End block Afternoon


    // start block Evening
    // AFI LO
    public function updateafiloevening(Request $request)
    {
        $page = 'report_update';
        $var_dateStart = date("Y-m-d");
        // $var_dateStart = '2021-10-14';
        $var_sheet = 5; // Morning
        $var_type_lottery = 1; // VN lottery
        $pos_id = 1; // group id = A
        // $numFixA = 00; // number result 2D
        // $numFixA3D = '000'; // number result 3D
        $numFixA = rand(10, 99); // number result 2D
        $numFixA3D = rand(100, 999); // number result 3D
        
        


        // block check pos A
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){
            // dd($pos);
            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }
        // dd('ok');

        // block check pos F
        $pos_id = 5; // group id = F
        // $numFixF = 00; // number result 2D
        // $numFixF3D = 000; // number result 3D
        $numFixF = rand(10, 99); // number result 2D
        $numFixF3D = rand(100, 999); // number result 3D
        
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }

        // dd('ok F');

        // block check pos I
        $pos_id = 6; // group id = I
        $numFixI = 00; // number result 2D
        // $numFixI3D = '000'; // number result 3D
        $numFixI = rand(10, 99); // number result 2D
        $numFixI3D = rand(100, 999); // number result 3D
        
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }


        $dateLottery = date("Y-m-d");
        $current_day = date("D", strtotime($dateLottery));

        if($current_day!='Fri'){
            // Random A 2D
            $digit = 2; 
            $x = 1; // for add random
            $numberResult = $this->checknumberavailable($numFixA,1,$var_dateStart,$digit);
            var_dump($numberResult,'2D A Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 1;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random A 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixA3D,1,$var_dateStart,$digit);
            var_dump($numberResult,'3D A Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 1;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }
        }

        if($current_day=='Sat' || $current_day=='Sun' || $current_day=='Mon'){

            // Random FI 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailableFI($numFixF,5,6,$var_dateStart,$digit);
            var_dump($numberResult,'2D FI Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 2;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random FI 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailableFI($numFixF3D,5,6,$var_dateStart,$digit);
            var_dump($numberResult,'3D FI Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 2;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }
        }
        if( $current_day=='Wed' || $current_day=='Thu'){
           

            // Random F 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixF,5,$var_dateStart,$digit);
            var_dump($numberResult,'2D F Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 3;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random F 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixF3D,5,$var_dateStart,$digit);
            var_dump($numberResult,'3D F Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 3;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixI,6,$var_dateStart,$digit);
            var_dump($numberResult,'2D I Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 2;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixI3D,6,$var_dateStart,$digit);
            var_dump($numberResult,'3D I Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 2;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

        }
        if($current_day=='Fri'){
           

            // Random AFI 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailableAFI($numFixA,1,5,6,$var_dateStart,$digit);
            var_dump($numberResult,'2D AFI Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 1;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random AFI 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailableAFI($numFixA3D,1,5,6,$var_dateStart,$digit);
            var_dump($numberResult,'3D AFI Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 1;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }


        }
        if($current_day=='Tue'){
           
            // Random F 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixF,5,$var_dateStart,$digit);
            var_dump($numberResult,'2D F Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 2;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random F 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixF3D,5,$var_dateStart,$digit);
            var_dump($numberResult,'3D F Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 2;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 2D
            $digit = 2; 
            $numberResult = $this->checknumberavailable($numFixI,6,$var_dateStart,$digit);
            var_dump($numberResult,'2D I Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 3;
                $name = 'first_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }

            // Random I 3D
            $digit = 3; 
            $numberResult = $this->checknumberavailable($numFixI3D,6,$var_dateStart,$digit);
            var_dump($numberResult,'3D I Evening'); 
            if($numberResult){
                $table_type = 2;
                $category_id = 3;
                $name = 'second_price';
                $set_value = $numberResult;
                $finish = 0;

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);

            }


        }



        // start block lo 
        // block lo  Afternoon
        $pos_id = 8; // group id = Lo
        $labelLoName = array('third_price_1','third_price_2','third_price_3','fourth_price','fifth_price_1','fifth_price_2','fifth_price_3','fifth_price_4','fifth_price_5','fifth_price_6','fifth_price_7','sixth_price_1','sixth_price_2','seventh_price','eighth_price'); // number result 2D
        // $numFix3D = array('000','000','000','000','000','000','000','000','000','000','000','000','000','000','000'); // number result 3D
        $numFix3D = array(rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999),rand(100, 999)); // number result 3D
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }

        foreach ($numFix3D as $key => $value) {
            $digit = 2;
            $numCheck = substr($value, -2);
            $twoResult = $this->checknumberavailable($numCheck,$pos_id,$var_dateStart,$digit);
            $digit = 3;
            $numCheck3 = substr($value, 0, 1).$twoResult;
            $threeResult = $this->checknumberavailableLo($numCheck3,$pos_id,$var_dateStart,$digit);
            var_dump($threeResult.'=>'.$twoResult.'=>data'.$key.'=>Evening'); 

            if($threeResult){
                $table_type = 2;
                $category_id = 1;
                $name = $labelLoName[$key];
                $set_value = $threeResult;
                $finish = 0;

                if($name == 'third_price_1' || $name == 'third_price_2' || $name == 'third_price_3'){
                    $set_value = rand(0, 9).$threeResult;
                }else{
                    $rand = rand(0, 99);
                    $mainD = '';
                    if($rand <= 9){
                        $mainD = '0'.$rand;
                    }else{
                        $mainD = $rand;
                    }
                    $set_value = $mainD.$threeResult;
                }

                DB::connection('mysql2')->table('lotteries')
                        ->where('table_type',$table_type)
                        ->where('category_id',$category_id)
                        ->where('name',$name)
                        ->where('finish',0)
                        ->update([
                            'set_value' => $set_value,
                        ]);
            }
            

        }
        //end block lo 
        


        
    } 


    //BCD Afternoon
    public function updatebcdevening(Request $request)
    {
        $page = 'report_update';
        $var_dateStart = date("Y-m-d");
        // $var_dateStart = '2021-10-14';
        $var_sheet = 5; // Morning
        $var_type_lottery = 1; // VN lottery
        $pos_id = 2; // group id = B
        // $numFix = 00; // number result 2D
        // $numFix3D = 000; // number result 3D
        $numFix = rand(10, 99); // number result 2D
        $numFix3D = rand(100, 999); // number result 3D
        
        


        // block check pos B
        // get paper not check
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }




        // block check pos C
        // get paper not check
        $pos_id = 3;
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }



        // block check pos D
        // get paper not check
        $pos_id = 4;
        $papers = DB::table('tbl_number')
                    ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                    ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                    ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                    ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                    ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                    ->where('p_time',$var_sheet)
                    ->where('tbl_pos_group.pos_id',$pos_id)
                    ->groupBy('tbl_number.num_id')
                    // ->toSql();
                    ->get();

       // dd($papers);
        $result = array();
        foreach ($papers as $key => $paper) {
            // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
            $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
            // $result[] =$data[$paper->pos_id];
        }
        // dd($result);

        $count = 0;

        if(count($result) > 0){

            $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            foreach ($result as $key => $pos) {

                foreach ($pos as $keyPos => $arrNum) { //foreach number

                    foreach ($arrNum as $numValue => $num) {
                    
                        $check = DB::table('tbl_smart_auto')
                                ->where('sm_date',$var_dateStart)
                                ->where('p_id',$keyPos)
                                ->where('sm_key',strval($numValue))
                                ->first();

                        
                        if($check){

                            $newPriceR = $check->sm_value_r;
                            $newPriceD = $check->sm_value_d;
                            if($num[1] == 1){
                                $newPriceR = $check->sm_value_r + $num[0];
                            }else{
                                $newPriceD = $check->sm_value_d + $num[0];
                            }

                            $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    ]);
                            if($updated){
                                $count++;
                            } 
                            
                        }else{
                        
                        
                            // dd($num);
                            $newPriceR = 0;
                            $newPriceD = 0;
                            if($num[1] == 1){
                                $newPriceR = $num[0];
                            }else{
                                $newPriceD = $num[0];
                            }

                            $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                [
                                    'digit' => strlen($numValue),
                                    'sm_key' => strval($numValue),
                                    'sm_value_r' => $newPriceR,
                                    'sm_value_d' => $newPriceD,
                                    'p_id' => $keyPos,
                                    'sm_date' => $var_dateStart

                                ]
                            );
                            if($insertID){
                                $count++;
                            } 
                        }

                    }


                } //end foreach num
            }

            
        }else{
            DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
        }



        $dateLottery = date("Y-m-d");
        $current_day = date("D", strtotime($dateLottery));
        // $current_day ='Fri';

        $digit = 2; 
        $count= 0;
        $fullNum = $numFix.$numFix3D;
        if($current_day=='Fri'){
            $numberResultBCD = $this->checknumberBCDNAfternoon($fullNum,2,3,4,7,$var_dateStart,$digit,$count);

            var_dump($numberResultBCD,'BCDN Afternoon'); 
        }else{
            $numberResultBCD = $this->checknumberBCDAfternoon($fullNum,2,3,4,$var_dateStart,$digit,$count);
        
            var_dump($numberResultBCD,'BCD Afternoon'); 


            // start N Afternoon
            $pos_id = 7; // group id = N
            // $numFixN = 00; // number result 2D
            // $numFixN3D = '000'; // number result 3D
            $numFixN = rand(10, 99); // number result 2D
            $numFixN3D = rand(100, 999); // number result 3D
            

            // get paper not check
            $papers = DB::table('tbl_number')
                        ->select('tbl_number.num_id','tbl_number.num_number','tbl_number.num_end_number', 'tbl_number.num_sym','tbl_number.num_reverse','tbl_number.num_price','tbl_number.num_currency','tbl_pos_group.pos_id','tbl_pos_group.g_id')
                        ->join('tbl_row','tbl_number.r_id','=','tbl_row.r_id')
                        ->join('tbl_paper','tbl_paper.p_id','=','tbl_row.p_id')
                        ->join('tbl_pos_group','tbl_number.g_id','=','tbl_pos_group.g_id')
                        ->where('p_date',date("Y-m-d", strtotime($var_dateStart)))
                        ->where('p_time',$var_sheet)
                        ->where('tbl_pos_group.pos_id',$pos_id)
                        ->groupBy('tbl_number.num_id')
                        // ->toSql();
                        ->get();

           // dd($papers);
            $result = array();
            foreach ($papers as $key => $paper) {
                // $result[] = Report::findingNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency,$paper->g_id,$var_sheet,$var_dateStart,$pos_id);
                $result[$paper->num_id][$paper->pos_id] = Report::loopNumber($paper->num_number,$paper->num_end_number,$paper->num_sym,$paper->num_reverse,$paper->num_price,$paper->num_currency);
                // $result[] =$data[$paper->pos_id];
            }
            // dd($result);

            $count = 0;

            if(count($result) > 0){

                $deleted = DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
                foreach ($result as $key => $pos) {

                    foreach ($pos as $keyPos => $arrNum) { //foreach number

                        foreach ($arrNum as $numValue => $num) {
                        
                            $check = DB::table('tbl_smart_auto')
                                    ->where('sm_date',$var_dateStart)
                                    ->where('p_id',$keyPos)
                                    ->where('sm_key',strval($numValue))
                                    ->first();

                            
                            if($check){

                                $newPriceR = $check->sm_value_r;
                                $newPriceD = $check->sm_value_d;
                                if($num[1] == 1){
                                    $newPriceR = $check->sm_value_r + $num[0];
                                }else{
                                    $newPriceD = $check->sm_value_d + $num[0];
                                }

                                $updated = DB::table('tbl_smart_auto')->where('sm_id', $check->sm_id)->update([
                                        'sm_value_r' => $newPriceR,
                                        'sm_value_d' => $newPriceD,
                                        ]);
                                if($updated){
                                    $count++;
                                } 
                                
                            }else{
                            
                            
                                // dd($num);
                                $newPriceR = 0;
                                $newPriceD = 0;
                                if($num[1] == 1){
                                    $newPriceR = $num[0];
                                }else{
                                    $newPriceD = $num[0];
                                }

                                $insertID = DB::table('tbl_smart_auto')->insertGetId(
                                    [
                                        'digit' => strlen($numValue),
                                        'sm_key' => strval($numValue),
                                        'sm_value_r' => $newPriceR,
                                        'sm_value_d' => $newPriceD,
                                        'p_id' => $keyPos,
                                        'sm_date' => $var_dateStart

                                    ]
                                );
                                if($insertID){
                                    $count++;
                                } 
                            }

                        }


                    } //end foreach num
                }

                
            }else{
                DB::table('tbl_smart_auto')->where('sm_date',$var_dateStart)->where('p_id',$pos_id)->delete();
            }

            $dateLottery = date("Y-m-d");
            $current_day = date("D", strtotime($dateLottery));

            if($current_day=='Sat' || $current_day=='Sun' || $current_day=='Mon'){

                // Random N 2D & 3D
                $digit = 2; 
                $numberResult2D = $this->checknumberavailable($numFixN,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult2D,'2D N Afternoon'); 
                $digit = 3; 
                $numberResult3D = $this->checknumberavailable($numFixN3D,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult3D,'3D N Afternoon'); 
                if($numberResult3D){
                    $table_type = 2;
                    $category_id = 2;
                    $name = 'grand_price';
                    $set_value = rand(0, 9).$numberResult2D.$numberResult3D;
                    $finish = 0;
                    DB::connection('mysql2')->table('lotteries')
                            ->where('table_type',$table_type)
                            ->where('category_id',$category_id)
                            ->where('name',$name)
                            ->where('finish',0)
                            ->update([
                                'set_value' => $set_value,
                            ]);

                }

            }

            if( $current_day=='Wed' || $current_day=='Thu'){

                // Random N 2D & 3D
                $digit = 2; 
                $numberResult2D = $this->checknumberavailable($numFixN,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult2D,'2D N Afternoon'); 
                $digit = 3; 
                $numberResult3D = $this->checknumberavailable($numFixN3D,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult3D,'3D N Afternoon'); 
                if($numberResult3D){
                    $table_type = 2;
                    $category_id = 3;
                    $name = 'grand_price';
                    $set_value = rand(0, 9).$numberResult2D.$numberResult3D;
                    $finish = 0;
                    DB::connection('mysql2')->table('lotteries')
                            ->where('table_type',$table_type)
                            ->where('category_id',$category_id)
                            ->where('name',$name)
                            ->where('finish',0)
                            ->update([
                                'set_value' => $set_value,
                            ]);

                }

            }

            if($current_day=='Fri'){

            }

            if($current_day=='Tue'){
               
                // Random N 2D & 3D
                $digit = 2; 
                $numberResult2D = $this->checknumberavailable($numFixN,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult2D,'2D N Afternoon'); 
                $digit = 3; 
                $numberResult3D = $this->checknumberavailable($numFixN3D,$pos_id,$var_dateStart,$digit);
                var_dump($numberResult3D,'3D N Afternoon'); 
                if($numberResult3D){
                    $table_type = 2;
                    $category_id = 2;
                    $name = 'grand_price';
                    $set_value = rand(0, 9).$numberResult2D.$numberResult3D;
                    $finish = 0;
                    DB::connection('mysql2')->table('lotteries')
                            ->where('table_type',$table_type)
                            ->where('category_id',$category_id)
                            ->where('name',$name)
                            ->where('finish',0)
                            ->update([
                                'set_value' => $set_value,
                            ]);

                }

            }


            // end N Afternoon



        } //end if no friday

        if($numberResultBCD){

            $table_type = 2;
            $category_id = 1;
            $name = 'grand_price';
            $set_value = $numberResultBCD;
            $finish = 0;

            DB::connection('mysql2')->table('lotteries')
                    ->where('table_type',$table_type)
                    ->where('category_id',$category_id)
                    ->where('name',$name)
                    ->where('finish',0)
                    ->update([
                        'set_value' => $set_value,
                    ]);

        }

        
        // end block BCD evening 

        
    }


    // end block Evening




    private function checknumberavailableAFI($number,$postIDA,$postIDF,$postIDI,$date,$digit){


        $maxPrice= 0;
        if($digit == 2){
            $maxPrice = 10;
            $x = 1;
        }else{
            $x = 2;
            $maxPrice = 5;
        }

        
        // A
        $allDataA = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDA)
                    ->where('sm_date',$date)
                    ->where('digit',$digit)
                    ->where('sm_key',strval($number))
                    ->where('sm_value_r','>',$maxPrice)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allDataF = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDF)
                    ->where('sm_date',$date)
                    ->where('digit',$digit)
                    ->where('sm_key',strval($number))
                    ->where('sm_value_r','>',$maxPrice)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        $allDataI = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDI)
                    ->where('sm_date',$date)
                    ->where('digit',$digit)
                    ->where('sm_key',strval($number))
                    ->where('sm_value_r','>',$maxPrice)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        // var_dump($allData2B,$allData3B,$allData2C,$allData3C,$allData2D,$allData3D);
        if(count($allDataA) > 0 || count($allDataF) > 0 || count($allDataI) > 0 ){
            
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);

            if($digit == 2){
                if($randomNumer <= 9){
                    $randomNumer = '0'.$randomNumer;
                }
            }else{
                if($randomNumer <= 9){
                    $randomNumer = '00'.$randomNumer;
                }else if($randomNumer <= 99){
                    $randomNumer = '0'.$randomNumer;
                }
            }
            return $this->checknumberavailableAFI($randomNumer,$postIDA,$postIDF,$postIDI,$date,$digit);
        }else{
            return $number;
        }

    }


    private function checknumberBCD($number,$postID,$date,$digit){


        $twoDB = substr($number, 0,2);
        $threeDB = substr($number, -3);
        $twoDC = substr($number, -2);
        $threeDC = substr($number, 0,3);
        $twoDD = substr($number,0,1).substr($number,-1);
        $threeDD = substr($number,1,3);
        // var_dump($twoDB,$threeDB,$twoDC,$threeDC,$twoDD,$threeDD);
        // var_dump($number);

        if(substr($twoDB, 0,1) == substr($twoDC, 0,1)){
            $x = 4;
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);
            
            if($randomNumer <= 9){
                $randomNumer = '0000'.$randomNumer;
            }else if($randomNumer <= 99){
                $randomNumer = '000'.$randomNumer;
            }else if($randomNumer <= 999){
                $randomNumer = '00'.$randomNumer;
            }else if($randomNumer <= 9999){
                $randomNumer = '0'.$randomNumer;
            }
            return $this->checknumberBCD($randomNumer,$postID,$date,$digit);
        }


        $maxPrice2 = 10;
        $maxPrice3 = 5;
        
        // B
        $allData2B = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',11)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDB))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3B = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',11)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDB))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        // C
        $allData2C = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',12)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDC))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3C = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',12)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDC))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        // D
        $allData2D = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',13)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDD))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3D = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',13)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDD))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        // var_dump($allData2B,$allData3B,$allData2C,$allData3C,$allData2D,$allData3D);
        if(count($allData2B) > 0 || count($allData3B) > 0 || 
            count($allData2C) > 0 || count($allData3C) > 0 || 
            count($allData2D) > 0 || count($allData3D) > 0){

            $x = 4;
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);
            
            if($randomNumer <= 9){
                $randomNumer = '0000'.$randomNumer;
            }else if($randomNumer <= 99){
                $randomNumer = '000'.$randomNumer;
            }else if($randomNumer <= 999){
                $randomNumer = '00'.$randomNumer;
            }else if($randomNumer <= 9999){
                $randomNumer = '0'.$randomNumer;
            }
            return $this->checknumberBCD($randomNumer,$postID,$date,$digit);
        }else{
            return rand(0, 9).$number;
        }

    }

    private function checknumberBCDAfternoon($number,$postIDB,$postIDC,$postIDD,$date,$digit,$count){

        $count++;
        $twoDB = substr($number, 0,2);
        $threeDB = substr($number, -3);
        $twoDC = substr($number, -2);
        $threeDC = substr($number, 0,3);
        $twoDD = substr($number,0,1).substr($number,-1);
        $threeDD = substr($number,1,3);
        // var_dump($twoDB,$threeDB,$twoDC,$threeDC,$twoDD,$threeDD);
        
        if(substr($twoDB, 0,1) == substr($twoDC, 0,1)){
            $x = 4;
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);
            // var_dump($randomNumer);
            
            if($randomNumer <= 9){
                $randomNumer = '0000'.$randomNumer;
            }else if($randomNumer <= 99){
                $randomNumer = '000'.$randomNumer;
            }else if($randomNumer <= 999){
                $randomNumer = '00'.$randomNumer;
            }else if($randomNumer <= 9999){
                $randomNumer = '0'.$randomNumer;
            }
            return $this->checknumberBCDAfternoon($randomNumer,$postIDB,$postIDC,$postIDD,$date,$digit,$count);
        }

        // if($count <= 500){
        //     $maxPrice2 = 10;
        // }else if($count <= 1000){
        //     $maxPrice2 = 20;
        // }else if($count <= 1500){
        //     $maxPrice2 = 30;
        // }else if($count <= 2000){
        //     $maxPrice2 = 40;
        // }else if($count <= 2500){
        //     $maxPrice2 = 50;
        // }else if($count <= 3000){
        //     $maxPrice2 = 60;
        // }else if($count <= 3500){
        //     $maxPrice2 = 70;
        // }else if($count <= 4000){
        //     $maxPrice2 = 80;
        // }else if($count <= 4500){
        //     $maxPrice2 = 90;
        // }else if($count <= 5000){
        //     $maxPrice2 = 100;
        // }else{
            $maxPrice2 = 10;
        // }
        $maxPrice3 = 5;
        // var_dump($maxPrice2);
        
        // B
        $allData2B = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDB)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDB))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3B = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDB)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDB))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        // C
        $allData2C = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDC)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDC))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3C = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDC)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDC))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        // D
        $allData2D = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDD)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDD))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3D = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDD)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDD))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        // var_dump($allData2B,$allData3B,$allData2C,$allData3C,$allData2D,$allData3D);
        if(count($allData2B) > 0 || count($allData3B) > 0 || 
            count($allData2C) > 0 || count($allData3C) > 0 || 
            count($allData2D) > 0 || count($allData3D) > 0){

            $x = 4;
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);
            // var_dump($randomNumer);
            
            if($randomNumer <= 9){
                $randomNumer = '0000'.$randomNumer;
            }else if($randomNumer <= 99){
                $randomNumer = '000'.$randomNumer;
            }else if($randomNumer <= 999){
                $randomNumer = '00'.$randomNumer;
            }else if($randomNumer <= 9999){
                $randomNumer = '0'.$randomNumer;
            }
            return $this->checknumberBCDAfternoon($randomNumer,$postIDB,$postIDC,$postIDD,$date,$digit,$count);
        }else{
            return rand(0, 9).$number;
        }

    }

    private function checknumberBCDNAfternoon($number,$postIDB,$postIDC,$postIDD,$postIDN,$date,$digit,$count){

        $count++;
        $twoDB = substr($number, 0,2);
        $threeDB = substr($number, -3);
        $twoDC = substr($number, -2);
        $threeDC = substr($number, 0,3);
        $twoDD = substr($number,0,1).substr($number,-1);
        $threeDD = substr($number,1,3);

        if(substr($twoDB, 0,1) == substr($twoDC, 0,1)){
            $x = 4;
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);
            // var_dump($randomNumer);
            
            if($randomNumer <= 9){
                $randomNumer = '0000'.$randomNumer;
            }else if($randomNumer <= 99){
                $randomNumer = '000'.$randomNumer;
            }else if($randomNumer <= 999){
                $randomNumer = '00'.$randomNumer;
            }else if($randomNumer <= 9999){
                $randomNumer = '0'.$randomNumer;
            }
            return $this->checknumberBCDNAfternoon($randomNumer,$postIDB,$postIDC,$postIDD,$postIDN,$date,$digit,$count);
        }
        // var_dump($twoDB,$threeDB,$twoDC,$threeDC,$twoDD,$threeDD);
        // var_dump($number);

        // if($count <= 500){
        //     $maxPrice2 = 10;
        // }else if($count <= 1000){
        //     $maxPrice2 = 20;
        // }else if($count <= 1500){
        //     $maxPrice2 = 30;
        // }else if($count <= 2000){
        //     $maxPrice2 = 40;
        // }else if($count <= 2500){
        //     $maxPrice2 = 50;
        // }else if($count <= 3000){
        //     $maxPrice2 = 60;
        // }else if($count <= 3500){
        //     $maxPrice2 = 70;
        // }else if($count <= 4000){
        //     $maxPrice2 = 80;
        // }else if($count <= 4500){
        //     $maxPrice2 = 90;
        // }else if($count <= 5000){
        //     $maxPrice2 = 100;
        // }else{
            $maxPrice2 = 10;
        // }
        $maxPrice3 = 5;

        // var_dump($maxPrice2);
        
        // B
        $allData2B = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDB)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDB))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3B = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDB)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDB))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        // C
        $allData2C = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDC)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDC))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3C = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDC)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDC))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        // D
        $allData2D = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDD)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDD))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3D = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDD)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDD))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        // N
        $allData2N = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDN)
                    ->where('sm_date',$date)
                    ->where('digit',2)
                    ->where('sm_key',strval($twoDB))
                    ->where('sm_value_r','>',$maxPrice2)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        $allData3N = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDN)
                    ->where('sm_date',$date)
                    ->where('digit',3)
                    ->where('sm_key',strval($threeDB))
                    ->where('sm_value_r','>',$maxPrice3)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        // var_dump($allData2B,$allData3B,$allData2C,$allData3C,$allData2D,$allData3D);
        if(count($allData2B) > 0 || count($allData3B) > 0 || 
            count($allData2C) > 0 || count($allData3C) > 0 || 
            count($allData2D) > 0 || count($allData3D) > 0 || 
            count($allData2N) > 0 || count($allData3N) > 0){

            $x = 4;
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);
            
            if($randomNumer <= 9){
                $randomNumer = '0000'.$randomNumer;
            }else if($randomNumer <= 99){
                $randomNumer = '000'.$randomNumer;
            }else if($randomNumer <= 999){
                $randomNumer = '00'.$randomNumer;
            }else if($randomNumer <= 9999){
                $randomNumer = '0'.$randomNumer;
            }
            return $this->checknumberBCDNAfternoon($randomNumer,$postIDB,$postIDC,$postIDD,$postIDN,$date,$digit,$count);
        }else{
            return rand(0, 9).$number;
        }

    }

    private function checknumberavailable($number,$postID,$date,$digit){
        $maxPrice= 0;
        if($digit == 2){
            $maxPrice = 10;
            $x = 1;
        }else{
            $x = 2;
            $maxPrice = 5;
        }
        
        // var_dump($number,$x);
        $allData = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postID)
                    ->where('sm_date',$date)
                    ->where('digit',$digit)
                    ->where('sm_key',strval($number))
                    ->where('sm_value_r','>',$maxPrice)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        // var_dump($allData);
        if(count($allData) > 0){
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);

            if($digit == 2){
                if($randomNumer <= 9){
                    $randomNumer = '0'.$randomNumer;
                }
            }else{
                if($randomNumer <= 9){
                    $randomNumer = '00'.$randomNumer;
                }else if($randomNumer <= 99){
                    $randomNumer = '0'.$randomNumer;
                }
            }
            return $this->checknumberavailable($randomNumer,$postID,$date,$digit);
        }else{
            return $number;
        }

    }

    private function checknumberavailableFI($number,$postIDF,$postIDI,$date,$digit){
        $maxPrice= 0;
        if($digit == 2){
            $maxPrice = 10;
            $x = 1;
        }else{
            $x = 2;
            $maxPrice = 5;
        }
        
        // var_dump($number,$x);
        $allDataF = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDF)
                    ->where('sm_date',$date)
                    ->where('digit',$digit)
                    ->where('sm_key',strval($number))
                    ->where('sm_value_r','>',$maxPrice)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();

        $allDataI = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postIDI)
                    ->where('sm_date',$date)
                    ->where('digit',$digit)
                    ->where('sm_key',strval($number))
                    ->where('sm_value_r','>',$maxPrice)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        // var_dump($allData);
        if(count($allDataF) > 0 || count($allDataI) > 0){
            $min = pow(0,$x);
            $max = pow(10,$x+1)-1;
            $randomNumer = rand($min, $max);

            if($digit == 2){
                if($randomNumer <= 9){
                    $randomNumer = '0'.$randomNumer;
                }
            }else{
                if($randomNumer <= 9){
                    $randomNumer = '00'.$randomNumer;
                }else if($randomNumer <= 99){
                    $randomNumer = '0'.$randomNumer;
                }
            }
            return $this->checknumberavailableFI($randomNumer,$postIDF,$postIDI,$date,$digit);
        }else{
            return $number;
        }

    }

    private function checknumberavailableLo($number,$postID,$date,$digit){
        
        
        $maxPrice = 5;
        
        $allData = DB::table('tbl_smart_auto')
                    ->select([DB::raw("SUM(sm_value_r) as sm_value_r"), DB::raw("SUM(sm_value_d) as sm_value_d"), 'sm_id','digit', 'sm_key', 'p_id', 'pos_name','pos_time', 'pos_id'])
                    ->leftjoin('tbl_pos', 'tbl_smart_auto.p_id', '=', 'tbl_pos.pos_id')
                    ->where('p_id',$postID)
                    ->where('sm_date',$date)
                    ->where('digit',$digit)
                    ->where('sm_key',strval($number))
                    ->where('sm_value_r','>',$maxPrice)
                    ->orderBy('sm_value_r', 'DESC')
                    ->orderBy('sm_value_d', 'DESC')
                    ->groupBy('sm_key')
                    ->get();
        // var_dump($allData);
        if(count($allData) > 0){
            // var_dump($number);
            $random1D = rand(0, 9);
            $randomNumer = $random1D.substr($number, -2);
            return $this->checknumberavailable($randomNumer,$postID,$date,$digit);
        }else{
            return $number;
        }

    }
}
