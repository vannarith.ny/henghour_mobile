<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class dailyReportController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // ini_set('max_execution_time', 3600);
        // ini_set('max_execution_time', 300); 
        // set_time_limit(300);
        $this->middleware(function ($request, $next) {
            if(!Session::has('iduserlot'))
            {
                Redirect::to('/login')->send();
            }
            return $next($request);
        });

    }

    public function index(Request $request)
    {
        $page = 'daily_report';

        $locations = DB::table('tbl_block')
                    ->pluck('name','id')->all();

        return view('dailyreport.index', compact('page','locations'));
    }


    public function filter(Request $request)
    {
        $page = 'daily_report';
        $var_dateStart = $request->dateStart;
        $var_location_type = $request->locations;
        $get_val = '';

        $locations = DB::table('tbl_block')
                    ->pluck('name','id')->all();

        // dd($var_location_type);

        if($var_location_type != ''){

            $staff_list = DB::table('tbl_block_staff')
            ->leftjoin('tbl_staff', 'tbl_block_staff.s_id', '=', 'tbl_staff.s_id')
            ->where('b_id',$var_location_type)
            ->orderBy('tbl_staff.s_name','asc')
            ->get();


            $getloationName = DB::table('tbl_block')->where('id',$var_location_type)->first();
            // dd($staff_list);

           

                // foreach ($staff_list as $key => $val){
                //     $get_val .= "'".$val->id."',";
                // }
                
                // if($get_val != ''){
                //     $mul_staff = substr($get_val,0,-1);
                // }else{
                //     $mul_staff = '';
                // }

                // // dd($mul_staff);

                // if($mul_staff != ''){
                //     $money = DB::table('tbl_story_payment')
                //       ->leftjoin('tbl_staff', 'tbl_story_payment.s_id', '=', 'tbl_staff.s_id')
                //       ->where('tbl_story_payment.date',$var_dateStart)
                //       ->whereRaw('tbl_story_payment.s_id IN ('.$mul_staff.')')
                //       ->orderBy('tbl_staff.s_name','ASC')
                //       ->orderBy('tbl_story_payment.type_lottery','ASC')
                //       ->get();
                // }
                
                



            
        }else{

            $staff_list = [];
            $getloationName = [];

        }
        
                return view('dailyreport.index', compact('page','var_dateStart','var_location_type','staff_list','locations','getloationName'));
            
        
    }


}
