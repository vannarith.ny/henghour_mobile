<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileChangePasswordDataController extends Controller
{
	public function __construct()
    {
    	$this->middleware(function ($request, $next) {

	 		  if(!Session::has('iduserlotMobileSec'))
		       {
		            Redirect::to('/')->send();
		       }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function changePassword(Request $request)
    {
        $page = "change_password";
        return view("login_mobile.changepassword", compact('page'));
    }

    public function changepasswordData(Request $request)
    {


        $id = Session::get('iduserlotMobileSec');
        $newpassword = $request->Password;
        $check = DB::table('tbl_staff')->where('s_id',$id)->update(
                            [
                                's_password' => $newpassword,
                                's_logined' => 0
                            ]
                     );
        if($check){

               Session::forget('iduserlotMobileSec');
               Session::forget('usernameLotMobileSec');
               Session::forget('nameLotMobileSec');
               Session::forget('phoneLotMobileSec');
               Session::forget('roleLotMobileSec');
               Session::forget('seccCanCreateChild');
               Session::forget('cutwaterSec');
               Session::forget('islogin');

            return response(['msg' => '','url' => URL::to('/').'/mobile/logout', 'status' => 'success']);
         }else{
            return response(['msg' => '', 'status' => 'error']);
         }
    }
}
