<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;
use DateTime;
use DateInterval;
use DatePeriod;

class TransctionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
              if(!Session::has('iduserlot'))
               {
                    Redirect::to('/login')->send();
               }elseif (Session::get('roleLot') != 1) {
              Redirect::to('/sale')->send();
           }
            return $next($request);
        });
   
    }


    public function index(Request $request)
    {
        $page = 'transction';

        $var_dateStart = Date('Y-m-d', strtotime('-10 days'));
        $var_dateEnd = date("Y-m-d");
        $var_staff = null;
        

        $staffMain = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('s_role',1)
            ->where('parent_id', 0)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
        // $staffMain['all'] = 'All';

        $chilStaffs = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('s_role',1)
            ->where('parent_id',"<>", 0)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();

        $sInfo = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('s_role',1)
            ->where('parent_id',"<>", 0)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->get()->first();

        // dd($sInfo->s_id);
        $var_staff_chail = $sInfo->s_id;
        $mainNameStaffInfo = $sInfo;

        return view('transction.index', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_dateStart','var_dateEnd','var_staff','var_staff_chail'));
    }

    public function updatemoneydata(Request $request){

        $value = $request->value;
        $typepayment = $request->typeID;
        $datepay = $request->typeDate;
        $currency = $request->crcy;
        $dateID = $request->valDateID;
        $s_id = $request->staffID;
        // if($typepayment == 20){
        //     $dateSearch = $datepay;
        // }else{
            $dateSearch = date('Y-m-d', strtotime($datepay. ' -1 days'));
        // }
        

        // DB::table('tbl_staff_transction')
        //             ->where('st_date_diposit',$datepay)
        //             ->where('s_id',$s_id)
        //             ->where('st_type',$typepayment)
        //             ->delete();

        

        if($value != '' && $value >=0){

            $checkRow = DB::table('tbl_staff_transction')
                    ->where('st_date_diposit',$datepay)
                    ->where('s_id',$s_id)
                    ->where('st_type',$typepayment)
                    ->first();

            if($checkRow){
                if($currency == 'r'){
                    DB::table('tbl_staff_transction')->where('st_id',$checkRow->st_id)->update([
                          'st_price_r'  =>  $value,
                            'st_type' => $typepayment,
                            'st_date_diposit'  => $datepay,
                            'st_date_search' => $dateSearch,
                            'st_remark'  =>  'from ajax real',
                            's_id' => $s_id
                          ]
                      );
                    $money = number_format($value);
                }else{
                    DB::table('tbl_staff_transction')->where('st_id',$checkRow->st_id)->update([
                          'st_price_d'  =>  $value,
                            'st_type' => $typepayment,
                            'st_date_diposit'  => $datepay,
                            'st_date_search' => $dateSearch,
                            'st_remark'  =>  'from ajax dollar',
                            's_id' => $s_id
                        ]
                      );
                    $money = number_format($value,2);

                }

                $idTran= $checkRow->st_id;

            }else{
                if($currency == 'r'){
                    $idTran= DB::table('tbl_staff_transction')->insertGetId([
                            'st_price_r'  =>  $value,
                            'st_type' => $typepayment,
                            'st_date_diposit'  => $datepay,
                            'st_date_search' => $dateSearch,
                            'st_remark'  =>  'from ajax real',
                            's_id' => $s_id
                            ]
                        ); 
                    $money = number_format($value);
                }else{
                    $idTran= DB::table('tbl_staff_transction')->insertGetId([
                            'st_price_d'  =>  $value,
                            'st_type' => $typepayment,
                            'st_date_diposit'  => $datepay,
                            'st_date_search' => $dateSearch,
                            'st_remark'  =>  'from ajax dollar',
                            's_id' => $s_id
                            ]
                        );  
                    $money = number_format($value,2);
                }
            }

            
        }else{
            $idTran = 0;
            $money = 0;
        }
        


        if($idTran){


            // need createfunction calculate
            

            $endDate_filter = date('Y-m-d');
            $begin = new DateTime($datepay);
            $end = new DateTime($endDate_filter);
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $dateDB = $dt->format("Y-m-d");
                $checkMoney = $this->calculatemoney($dateDB, $s_id);
                // var_dump($checkMoney,$dateDB);
                if(isset($checkMoney)){

                    $checksubRow = DB::table('tbl_staff_transction')
                    ->where('st_date_diposit',$dateDB)
                    ->where('s_id',$s_id)
                    ->where('st_type',21)
                    ->first();
                    
                    if($checksubRow){
                        DB::table('tbl_staff_transction')->where('st_id',$checksubRow->st_id)->update([
                                'st_price_r'  =>  $checkMoney[2],
                                'st_price_d'  =>  $checkMoney[3],
                                'st_remark'  =>  'from ajax',
                              ]
                          );
                    }else{
                        DB::table('tbl_staff_transction')->insertGetId([
                            'st_price_r'  =>  $checkMoney[2],
                            'st_price_d'  =>  $checkMoney[3],
                            'st_type' => 21,
                            'st_date_diposit'  => $dateDB,
                            'st_date_search' => $dateDB,
                            'st_remark'  =>  'from ajax check',
                            's_id' => $s_id
                            ]
                        );  

                    }

                    


                }
            }
            $checkMoney = $this->calculatemoney($datepay, $s_id);


            return response(['msg' => $idTran,'id' => $s_id, 'valueDisplay'=>$money, 'date'=>$datepay, 'finalPayment'=>$checkMoney, 'status' => 'success']);  
        
        }else{

            // need createfunction calculate
            $checkMoney = $this->calculatemoney($datepay, $s_id);
            return response(['msg' => $idTran,'id' => $s_id, 'valueDisplay'=>$money, 'date'=>$datepay, 'finalPayment'=>$checkMoney, 'status' => 'success']);

        } 
    }

    public static function calculatemoney($datepay, $s_id){
        $finalPayment = 0;
        $finalPaymentD = 0;
        // get money from staff
        $moneyTotal = DB::table('tbl_staff_transction')
                ->where('s_id',$s_id)
                ->where('st_type',21)
                ->where('st_date_diposit',$datepay)
                ->first();

        if($moneyTotal){
            $finalPayment = $finalPayment + $moneyTotal->price_one_day_r;
            $finalPaymentD = $finalPaymentD + $moneyTotal->price_one_day_d;
        }else{
            DB::table('tbl_staff_transction')->insertGetId([
                'st_price_r'  =>  0,
                'st_price_d'  =>  0,
                'st_type' => 21,
                'st_date_diposit'  => $datepay,
                'st_date_search' => $datepay,
                'st_remark'  =>  'from ajax auto empty',
                's_id' => $s_id
                ]
            );  
        }

        $moneyTotalOld = DB::table('tbl_staff_transction')
                ->where('s_id',$s_id)
                ->where('st_type',21)
                ->where('st_date_diposit',date('Y-m-d', strtotime($datepay. ' -1 days')))
                ->first();
        if($moneyTotalOld){
            // old payment
            $finalPayment = $finalPayment + $moneyTotalOld->st_price_r;
            $finalPaymentD = $finalPaymentD + $moneyTotalOld->st_price_d;
        }

        // get money from staff
        $moneyFrom = DB::table('tbl_staff_transction')
                ->where('s_id',$s_id)
                ->where('st_type',3)
                ->where('st_date_diposit',$datepay)
                ->first();
        if($moneyFrom){
            $finalPayment = $finalPayment - $moneyFrom->st_price_r;
            $finalPaymentD = $finalPaymentD - $moneyFrom->st_price_d;
        }

        // get money to staff
        $moneyTo = DB::table('tbl_staff_transction')
                ->where('s_id',$s_id)
                ->where('st_type',4)
                ->where('st_date_diposit',$datepay)
                ->first();
        if($moneyTo){
            $finalPayment = $finalPayment + $moneyTo->st_price_r;
            $finalPaymentD = $finalPaymentD + $moneyTo->st_price_d;
        }

        // get money to staff
        $moneyCheckAgian = DB::table('tbl_staff_transction')
                ->where('s_id',$s_id)
                ->where('st_type',20)
                ->where('st_date_diposit',$datepay)
                ->first();
        if($moneyCheckAgian){
            $finalPayment = $finalPayment - $moneyCheckAgian->st_price_r;
            $finalPaymentD = $finalPaymentD - $moneyCheckAgian->st_price_d;
        }

        DB::table('tbl_staff_transction')
                ->where('s_id',$s_id)
                ->where('st_type',21)
                ->where('st_date_diposit',$datepay)
                ->update([
                        'st_price_r' => $finalPayment,
                        'st_price_d' => $finalPaymentD
                        ]
                    ); 

        
        return [number_format($finalPayment), number_format($finalPaymentD,2), $finalPayment, $finalPaymentD];
    }


    public function filtermaintransction(Request $request)
    {
        $page = 'transction';
        
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd; 
        $var_staff = $request->s_id_man;
        $var_staff_chail = $request->s_id;

        $today = date("Y-m-d");
        if($var_dateEnd >= $today){
            $var_dateEnd = $today;
        }  

        if($var_dateEnd == ''){
            $var_dateEnd = $today;
        } 

        
        $staffMain = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            // ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.s_id')
            ->where('s_role',1)
            ->where('parent_id', 0)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();

        if($var_staff == 'all'){

            $chilStaffs = DB::table('tbl_staff')
                ->select('tbl_staff.s_id','tbl_staff.s_name')
                ->where('s_role',1)
                // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
                ->orderBy('tbl_staff.s_name', 'ASC')
                ->groupBy('tbl_staff.s_id')
                ->pluck('s_name','s_id')
                ->all();

        }else if($var_staff != ''){
            $chilStaffs = DB::table('tbl_staff')
            ->where('parent_id',$var_staff)
            ->orderBy('s_name', 'ASC')
            ->pluck('s_name','s_id')
            ->all();
        
        }else{
            $chilStaffs = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->where('s_role',1)
            // ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
        }


        if($var_staff_chail == '' && $var_staff !=''){
            
            $mainNameStaffInfo = DB::table('tbl_staff')
                    ->where('s_id', $var_staff)
                    ->first();
            // dd($mainNameStaffInfo);

        }else{
            $mainNameStaffInfo = DB::table('tbl_staff')
                    ->where('s_id', $var_staff_chail)
                    ->first();
        }



        return view('transction.index', compact('page','mainNameStaffInfo','staffMain','chilStaffs','var_dateStart','var_dateEnd','var_staff','var_staff_chail'));

    }

}
