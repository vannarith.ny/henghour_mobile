<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use Flash;
use Redirect;
use Hash;
use Session;

class ReportErrorController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
	 		  if(!Session::has('iduserlot'))
		       {
		            Redirect::to('/login')->send();
		       }

               // if(Session::get('roleLot') == 0)
               // {
               //      if(Session::has('iduserlot')){
               //         Session::forget('iduserlot');
               //         Session::forget('usernameLot');
               //         Session::forget('nameLot');
               //         Session::forget('phoneLot');
               //      }

               //      Redirect::to('/login')->send();
               // }
            return $next($request);
        });
   
    }

    public function index(Request $request)
    {
     	

        
        $page = 'reporterror';
        $var_type_lottery = null;

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );

        // $staff = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name')
        //     ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
        //     ->orderBy('tbl_staff.s_id', 'ASC')
        //     ->groupBy('tbl_staff.s_id')
        //     ->pluck('s_name','s_id')
        //     ->all();
        if($request->staff != ''){
          $staff = $request->staff;
        }else{
          $staff = array();  
        }
        
        $pages = DB::table('tbl_paper')
            ->select('tbl_paper.p_number')
            ->groupBy('tbl_paper.p_number')
            ->orderBy('tbl_paper.p_number', 'ASC')
            ->pluck('p_number','p_number')
            ->all();

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();
        return view('reporterror.index', compact('page','sheets','staff','pages','type_lottery','var_type_lottery'));
    }

    public function filter(Request $request)
    {
        $page = 'reporterror';
        $var_dateStart = $request->dateStart;
        $var_dateEnd = $request->dateEnd;
        $var_sheet = $request->sheet;
        $var_staff = $request->staff;
        $var_page = $request->page;
        $var_type_lottery = $request->type_lottery;

        // dd( $var_type_lottery);

        $type_lottery = array(1 => 'VN',2 =>'KH',3 =>'TH',4 =>'វត្តុ' );

        $staff = DB::table('tbl_staff')
            ->select('tbl_staff.s_id','tbl_staff.s_name')
            ->join('tbl_staff_charge','tbl_staff.s_id','tbl_staff_charge.s_id')
            ->where('tbl_staff_charge.stc_type',$var_type_lottery)
            ->orderBy('tbl_staff.s_name', 'ASC')
            ->groupBy('tbl_staff.s_id')
            ->pluck('s_name','s_id')
            ->all();
        $pages = DB::table('tbl_paper')
            ->select('tbl_paper.p_number')
            ->groupBy('tbl_paper.p_number')
            ->orderBy('tbl_paper.p_number', 'ASC')
            ->pluck('p_number','p_number')
            ->all();

        $sheets = DB::table('tbl_parameter_type')
            ->select('tbl_parameter_type.pat_id','tbl_parameter_value.pav_id','tbl_parameter_value.pav_value')
            ->join('tbl_parameter_value', 'tbl_parameter_type.pat_id', '=', 'tbl_parameter_value.pat_id')
            ->where('tbl_parameter_type.pat_key', '=','sheet')
            ->orderBy('tbl_parameter_value.pav_id', 'ASC')
            ->pluck('pav_value','pav_id')
            ->all();


        $staff_name = DB::table('tbl_staff')
            ->select('tbl_staff.s_name')
            ->where('tbl_staff.s_id', $var_staff)
            ->first();
       

        // $main_new = DB::table('tbl_staff')
        //     ->select('tbl_staff.s_id','tbl_staff.s_name')
        //     ->join('tbl_staff_charge_main','tbl_staff.s_id','tbl_staff_charge_main.chail_id')
        //     ->where('tbl_staff_charge_main.s_id','=',$var_staff)
        //     ->get();

        $paper_row_table = DB::table('summary_lottery_page');

            if($var_dateStart != ''){
            	$paper_row_table = $paper_row_table->where('date_lottery','=',$var_dateStart);
            }
            if($var_type_lottery != ''){
            	$paper_row_table = $paper_row_table->where('type_lottery','=',$var_type_lottery);
            }
            if($staff_name->s_name != ''){
            	$paper_row_table = $paper_row_table->where('staff_value','=',$staff_name->s_name);
            }
            
            
            $paper_row_table = $paper_row_table->get();

            
 


            // return view('report.index', compact('page','sheets','staff','pages','type_lottery','var_type_lottery'));
            return view('reporterror.index', compact('page','sheets','staff','pages','var_dateStart','var_staff','var_sheet','var_page','paper_row_table','var_type_lottery','type_lottery'));
            // return redirect('report/')->withInput()->with('staff', $staff);
        
    }

    public function deleteItem(Request $request){
        $id = $request->id;
        $checkStcItem = DB::table('summary_lottery_page')
                    ->where('id','=',$id)
                    ->first();
        if($checkStcItem){
            
            
            $check = DB::table('summary_lottery_page')
                        ->where('id','=',$id)->delete();
            if($check){
                return response(['msg' => $id, 'status' => 'success']); 
            }else{
               
                  return response(['msg' => trans('message.fail_delete'), 'status' => 'error']); 
            } 
        }else{
           return response(['msg' => trans('message.not_permission_delete'), 'status' => 'error']); 
        }
    }
}
