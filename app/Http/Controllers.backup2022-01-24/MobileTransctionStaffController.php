<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Flash;
use Input;
use Redirect;
use Blade;
use DB;

class MobileTransctionStaffController extends Controller
{
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {

	 		  if(!Session::has('iduserlotMobileSec'))
		       {
		            Redirect::to('/')->send();
		       }else{
                    $check = DB::table('tbl_staff')
                            ->where('s_id',Session::get('iduserlotMobileSec'))
                            ->where('s_logined',Session::get('islogin'))
                            ->first();
                    // dd($check);
                    if(isset($check->Active) && $check->Active == 0){
                        
                    }else{
                       flash()->error("គណនីរបស់អ្នកត្រូវបានគេចូលហើយ");
                       Session::forget('iduserlotMobileSec');
                       Session::forget('usernameLotMobileSec');
                       Session::forget('nameLotMobileSec');
                       Session::forget('phoneLotMobileSec');
                       Session::forget('roleLotMobileSec');
                       Session::forget('seccCanCreateChild');
                        return redirect('mobile/');
                    }
                    
               }
            return $next($request);
        });
   
    }

    public function getTransctionList(Request $request)
    {
        $userID = $request->userId;
        $startDate = $request->fromDate;
        $endDate = $request->toDate;

        $sIDArray = array();
        array_push($sIDArray,$userID);

        // find child
        $get_val_main = $this->loop_find_staff($userID);
        if($get_val_main != ''){
            $mul_staff = substr($get_val_main,0,-1);
            $pieces = explode(",", $mul_staff);
            if(count($pieces) > 0){
                foreach ($pieces as $key => $id) {
                    array_push($sIDArray,$id);
                }
            }
            
        }
        

        // dd($sIDArray);


        $yesturdayStart = date('Y-m-d',strtotime($startDate . "-1 days"));
        $yesturdayEnd = date('Y-m-d',strtotime($endDate . "-1 days"));
        $report = DB::table("tbl_staff_transction")
                ->leftjoin('tbl_parameter_value', 'tbl_staff_transction.st_type','=','tbl_parameter_value.pav_id')
                ->leftjoin('tbl_staff', 'tbl_staff_transction.s_id','=','tbl_staff.s_id')
                ->where('tbl_staff_transction.st_date_search','>=',$yesturdayStart)
                ->where('tbl_staff_transction.st_date_search','<=',$yesturdayEnd)
                ->whereIn('tbl_staff_transction.s_id', $sIDArray)
                ->get();

        // dd($report);

        if($report){
            $html = '';
            $total1R = 0;
            $total1D = 0;
            foreach ($report as $key => $value) {
                $class="";
                if($value->s_id != Session::get('iduserlotMobileSec')){
                    $class='onclick="SaveUpdate(' .$value->st_id. ', event);"';
                }
                $html .= '<tr align="center" class="tr-row no-transac " '.$class.'>';
                    $html .= '<td class="classtd">'.date("m/d/Y", strtotime($value->st_date_diposit)).'</td>';
                    $html .= '<td class="classtd">'.$value->s_name.'</td>'; 

                    // get payment អោយ
                    $html .= '<td class="classtd">'.$value->pav_value.'</td>';
                    $html .= '<td class="classtd">'.number_format($value->st_price_r).'</td>';
                    $html .= '<td class="classtd">'.$value->st_price_d.'$</td>';
                    $total1R = $total1R + $value->st_price_r;
                    $total1D = $total1D + $value->st_price_d;
                    

                    
                $html .= '</tr>';    
            }

            $userList = $html.'<tr>
                            <td class="classtd" colspan="3" align="right">សរុប</td>
                            <td class="classtd" align="right">'.number_format($total1R).' ៛</td>
                            <td class="classtd" align="right">'.$total1D.' $</td>
                        </tr>';
            return response(['msg' => 'ok', 'Clients'=>$userList, 'status' => 'success']);
        }else{
            $msg = '<tr align="center" class="tr-row no-transac"><td class="classtd" colspan="8">មិនមានទិន្នន័យ</td></tr>';
            return response(['msg' => $msg, 'status' => 'error']);
        }

        
    }

    public function gettransctionone(Request $request)
    {
        $tranID = $request->clientId;
        $data = DB::table('tbl_staff_transction')
                    ->where('st_id',$tranID)
                    ->first();
        if($data){
            return response(['msg' => 'ok', 'Clients'=>$data, 'status' => 'success']);
        }else{
            return response(['msg' => 'not found', 'status' => 'error']);
        }
                   
    }

    public function index(){
        $page = 'TransctionStaff';

        $pharams = DB::table('tbl_parameter_value')
                    ->where('pat_id', 2)
                    ->whereIn('pav_id', array(3, 4, 20, 22))->get();


        $staff = DB::table("tbl_staff")->where('s_id',Session::get('iduserlotMobileSec'))->first();
        $childs = DB::table("tbl_staff")->where('parent_id',Session::get('iduserlotMobileSec'))->get();
    	return view("mobile.transctionstaff.index", compact('page','staff','childs','pharams'));
    }

    public function updateData(Request $request)
    {
        $idData = $request->idData;
        $s_id = $request->s_id;
        $moneyR = $request->moneyR;
        $moneyD = $request->moneyD;
        $s_type = $request->s_type;
        $date_in = $request->date_in;
        if($s_type == 3 || $s_type == 4){
            $yesturday = date('Y-m-d',strtotime($date_in . "-1 days"));
        }else{
            $yesturday = $date_in;
        }

        $check = DB::table('tbl_staff_transction')
                    ->where('st_id','<>', $idData)
                    ->where('s_id', $s_id)
                    ->where('st_date_search', $date_in)
                    ->where('st_type', $s_type)
                    ->first();
        if($check){
            return response(['msg' => 'ទិន្នន័យជាន់គ្នា', 'status' => 'error']);
        }else{
            DB::table('tbl_staff_transction')->where('st_id', $idData)
                ->Update([
                    'st_price_r' => $moneyR,
                    'st_price_d' => $moneyD,
                    'st_type' => $s_type,
                    'st_date_diposit' => $date_in,
                    'st_date_search' => $yesturday,
                    's_id' => $s_id,
                    'st_by' => Session::get('iduserlotMobileSec')
                    ]);
            return response(['msg' => 'រក្សាទុកបានជោគជ័យ', 's_id' => $s_id, 'status' => 'success']);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->userId;
        

        $control = DB::table("tbl_staff_transction")->where('st_id',$id)->delete();
           

        return response(['msg' => 'លុបបានសំរេច', 'status' => 'success']); 
    }

    public function saveTransction(Request $request)
    {
        $s_id = $request->s_id;
        $moneyR = $request->moneyR;
        $moneyD = $request->moneyD;
        $s_type = $request->s_type;
        $date_in = $request->date_in;
        if($s_type == 3 || $s_type == 4){
            $yesturday = date('Y-m-d',strtotime($date_in . "-1 days"));
        }else{
            $yesturday = $date_in;
        }

        $check = DB::table('tbl_staff_transction')
                    ->where('s_id', $s_id)
                    ->where('st_date_search', $date_in)
                    ->where('st_type', $s_type)
                    ->first();
        if($check){
            return response(['msg' => 'ទិន្នន័យជាន់គ្នា', 'status' => 'error']);
        }else{
            $id = DB::table('tbl_staff_transction')
                ->insertGetId([
                    'st_price_r' => $moneyR,
                    'st_price_d' => $moneyD,
                    'st_type' => $s_type,
                    'st_date_diposit' => $date_in,
                    'st_date_search' => $yesturday,
                    's_id' => $s_id,
                    'st_by' => Session::get('iduserlotMobileSec')
                    ]);
            return response(['msg' => 'រក្សាទុកបានជោគជ័យ', 's_id' => $s_id, 'status' => 'success']);
        }
    }

    private function loop_find_staff($st_id){
        // var_dump($st_id);
        $get_val = '';

        $check_boss = DB::table('tbl_staff')
            ->where('parent_id','=',$st_id)
            ->get();
        // var_dump($check_boss);
        if($check_boss){
            // dd(count($check_boss));
            if(count($check_boss) >= 1){
                foreach ($check_boss as $key => $val){
                    $get_val .= $val->s_id.",";
                }
                // dd('ok');
                return $get_val;
            }else{

                foreach ($check_boss as $key => $val){
                    return $this->loop_find_staff($val->s_id);
                }
            }
        }else{
            return '';
        }
    }
}
